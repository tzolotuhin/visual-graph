package vg.web.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VgWebGatewayApplication {
    public static void main(String[] args) {
        SpringApplication.run(VgWebGatewayApplication.class, args);
    }
}
