package vg.web.gateway.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class GatewayConfig {
    @Value("${vg-web-gateway.route.vg-web-auth-service}")
    private String authServiceUri;

    @Value("${vg-web-gateway.route.vg-web-api-service}")
    private String apiServiceUri;

    private final AuthenticationFilter filter;

    @Bean
    public RouteLocator routes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route("auth-service", r -> r.path("/v1/user/**", "/v1/token/**").uri(authServiceUri))
                .route("api-health-check-service", r -> r.path("/v1/api/health-check").uri(apiServiceUri))
                .route("api-service", r -> r.path("/v1/api/**")
                        .filters(f -> f.filter(filter))
                        .uri(apiServiceUri))
                .build();
    }
}
