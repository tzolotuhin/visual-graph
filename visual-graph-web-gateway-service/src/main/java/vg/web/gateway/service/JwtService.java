package vg.web.gateway.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class JwtService {
//    @Value("${auth.jwt.expiration}")
//    private String expirationTime;
//
//    private final RSAPrivateKey rsaPrivateKey;
//
//    private final RSAPublicKey rsaPublicKey;
//
//    public Claims getAllClaimsFromToken(String token) {
//        return Jwts.parserBuilder().setSigningKey(rsaPublicKey).build().parseClaimsJws(token).getBody();
//    }
//
//    public Date getExpirationDateFromToken(String token) {
//        return getAllClaimsFromToken(token).getExpiration();
//    }
//
//    private Boolean isTokenExpired(String token) {
//        final Date expiration = getExpirationDateFromToken(token);
//        return expiration.before(new Date());
//    }
//
//    public String generate(String login, String type) {
//        Map<String, Object> claims = new HashMap<>();
//        claims.put("login", login);
//        claims.put("type", type);
//        return doGenerateToken(claims, login, type);
//    }
//
//    private String doGenerateToken(Map<String, Object> claims, String username, String type) {
//        long expirationTimeLong;
//        if ("ACCESS".equals(type)) {
//            expirationTimeLong = Long.parseLong(expirationTime) * 1000;
//        } else {
//            expirationTimeLong = Long.parseLong(expirationTime) * 1000 * 5;
//        }
//        final Date createdDate = new Date();
//        final Date expirationDate = new Date(createdDate.getTime() + expirationTimeLong);
//
//        return Jwts.builder()
//                .setClaims(claims)
//                .setSubject(username)
//                .setIssuedAt(createdDate)
//                .setExpiration(expirationDate)
//                .signWith(rsaPrivateKey)
//                .compact();
//    }
//
//    public Boolean validateToken(String token) {
//        return !isTokenExpired(token);
//    }
}
