package vg.plugin.glayout.hierarchical.data;

import vg.lib.layout.hierarchical.data.HierarchicalGraph;

import java.awt.Point;
import java.util.UUID;

public class HierarchicalGraphTest {
    public static HierarchicalGraph generateSimpleBackwardGraph() {
        HierarchicalGraph graph = new HierarchicalGraph(new Point(20, 20), new Point(10, 10), new Point(10, 10), false, false);

        var in = graph.addInputPort(UUID.randomUUID(), 0, "in", 0, new Point(30, 20));

        var v2 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "v2", 1, new Point(30, 20));
        var v1 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "v1", 2, new Point(30, 20));
        var v3 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "v3", 3, new Point(30, 20));

        var out = graph.addOutputPort(UUID.randomUUID(), 0, "out", 4, new Point(30, 20));

        graph.addEdge(in, v1);

        graph.addEdge(v2, v1);
        graph.addEdge(v2, v3);
        graph.addEdge(v1, v3);
        graph.addEdge(v1, v2);
        graph.addEdge(v3, v1);

        graph.addEdge(v1, out);

        graph.build(null);

        return graph;
    }
}