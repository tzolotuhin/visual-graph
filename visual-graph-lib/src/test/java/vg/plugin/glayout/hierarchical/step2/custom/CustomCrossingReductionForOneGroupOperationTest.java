package vg.plugin.glayout.hierarchical.step2.custom;

import org.junit.jupiter.api.Test;
import vg.lib.layout.hierarchical.step2.custom.CustomCrossingReductionForOneGroupOperation;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CustomCrossingReductionForOneGroupOperationTest {
    @Test
    void testExecute() {
        // arrange
        var inputPort1 = UUID.randomUUID();

        var outputPort1 = UUID.randomUUID();

        var vertex1 = UUID.randomUUID();
        var dummyVertex21 = UUID.randomUUID();
        var vertex22 = UUID.randomUUID();
        var vertex23 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            // 0
            inputPort1,

            // 1
            vertex1,

            // 2 3 4
            dummyVertex21, vertex22, vertex23,

            // 5
            vertex3,

            // 6
            outputPort1
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort1, vertex1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, dummyVertex21));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, vertex22));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex21, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex22, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex23, vertex3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, outputPort1));

        var vertexIdToLayer = Map.of(
            inputPort1, 0,
            outputPort1, 4,
            vertex1, 1,
            dummyVertex21, 2,
            vertex22, 2,
            vertex23, 2,
            vertex3, 3
        );

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            Collections.emptyMap(),
            vertexIdToLayer,
            List.of(dummyVertex21),
            Collections.emptyMap(),
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        var expectedCycleVertexIndexToParentVertexIndex = new int[7];
        Arrays.fill(expectedCycleVertexIndexToParentVertexIndex, -1);

        assertArrayEquals(expectedCycleVertexIndexToParentVertexIndex, re.cycleVertexIndexToParentVertexIndex);

        assertArrayEquals(new int[] {
            0b010000,
            0b001111, 0b010000,
            0b001111, 0b010000, 0b010000,
            0b010000
        }, re.edgeIndexToType);

        assertArrayEquals(new int[] { 0, 1, 1, 4, 5 }, re.chainIndexToStartVertexIndex);
        assertArrayEquals(new int[] { 1, 5, 5, 5, 6 }, re.chainIndexToFinishVertexIndex);

        assertArrayEquals(new int[] { }, re.layerToTopTrapVertexIndices[0]);
        assertArrayEquals(new int[] { 1 }, re.layerToTopTrapVertexIndices[1]);
        assertArrayEquals(new int[] { 1 }, re.layerToTopTrapVertexIndices[2]);
        assertArrayEquals(new int[] { 1 }, re.layerToTopTrapVertexIndices[3]);
        assertArrayEquals(new int[] { 1 }, re.layerToTopTrapVertexIndices[4]);

        assertArrayEquals(new int[] { 5 }, re.layerToBottomTrapVertexIndices[0]);
        assertArrayEquals(new int[] { 5 }, re.layerToBottomTrapVertexIndices[1]);
        assertArrayEquals(new int[] { 5 }, re.layerToBottomTrapVertexIndices[2]);
        assertArrayEquals(new int[] { 5 }, re.layerToBottomTrapVertexIndices[3]);
        assertArrayEquals(new int[] { }, re.layerToBottomTrapVertexIndices[4]);

        // assert initial order and metrics before some actions.
        assertEquals(0, re.numberOfCrossesBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        // assert final data.
        assertEquals(0, re.numberOfCrosses);
    }

    @Test
    void testExecute_two_layers() {
        // arrange
        var vertex11 = UUID.randomUUID();
        var vertex12 = UUID.randomUUID();
        var vertex21 = UUID.randomUUID();
        var vertex22 = UUID.randomUUID();
        var vertex23 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            // 0 1
            vertex11, vertex12,

            // 2 3 4
            vertex21, vertex22, vertex23
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex11, vertex22));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex12, vertex21));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex12, vertex23));

        var vertexIdToLayer = Map.of(
            vertex11, 0,
            vertex12, 0,
            vertex21, 1,
            vertex22, 1,
            vertex23, 1
        );

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            Collections.emptyMap(),
            vertexIdToLayer,
            Collections.emptyList(),
            Collections.emptyMap(),
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        var expectedCycleVertexIndexToParentVertexIndex = new int[5];
        Arrays.fill(expectedCycleVertexIndexToParentVertexIndex, -1);

        assertArrayEquals(expectedCycleVertexIndexToParentVertexIndex, re.cycleVertexIndexToParentVertexIndex);

        assertArrayEquals(new int[] {
            0b010000, 0b010000, 0b010000
        }, re.edgeIndexToType);

        assertArrayEquals(new int[] { 0, 1, 1 }, re.chainIndexToStartVertexIndex);
        assertArrayEquals(new int[] { 3, 2, 4 }, re.chainIndexToFinishVertexIndex);

        // assert initial order and metrics before some actions.
        assertEquals(1, re.numberOfCrossesBefore);

        assertArrayEquals(
            new long[] { 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 1 },
            re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        // assert final data.
        assertArrayEquals(new int[] {
            0, 1,
            1, 0, 2
        }, re.vertexIndexToOrder);

        assertEquals(0, re.numberOfCrosses);
    }

    @Test
    void testExecute_two_layers_with_vertices_which_have_ports() {
        // arrange
        var vertex11 = UUID.randomUUID();
        var vertex12 = UUID.randomUUID();
        var vertex21 = UUID.randomUUID();
        var vertex22 = UUID.randomUUID();
        var vertex23 = UUID.randomUUID();

        var vertex11ToVertex22 = UUID.randomUUID();
        var vertex11ToVertex21 = UUID.randomUUID();
        var vertex11ToVertex23 = UUID.randomUUID();
        var vertex12ToVertex23 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            vertex11,
            vertex12,
            vertex21,
            vertex22,
            vertex23
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(vertex11ToVertex22, Map.entry(vertex11, vertex22));
        edgeIdToNodeIds.put(vertex11ToVertex21, Map.entry(vertex11, vertex21));
        edgeIdToNodeIds.put(vertex11ToVertex23, Map.entry(vertex11, vertex23));
        edgeIdToNodeIds.put(vertex12ToVertex23, Map.entry(vertex12, vertex23));

        var vertexIdToLayer = Map.of(
            vertex11, 0,
            vertex12, 0,
            vertex21, 1,
            vertex22, 1,
            vertex23, 1
        );

        var edgeIdToPortIndices = Map.of(
            vertex11ToVertex22, Map.entry(0, -1),
            vertex11ToVertex21, Map.entry(1, -1),
            vertex11ToVertex23, Map.entry(-1, 1),
            vertex12ToVertex23, Map.entry(-1, 0)
        );

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            edgeIdToPortIndices,
            vertexIdToLayer,
            Collections.emptyList(),
            Collections.emptyMap(),
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        assertEquals(2, re.numberOfCrossesBefore);

        var expectedCycleVertexIndexToParentVertexIndex = new int[5];
        Arrays.fill(expectedCycleVertexIndexToParentVertexIndex, -1);

        assertArrayEquals(expectedCycleVertexIndexToParentVertexIndex, re.cycleVertexIndexToParentVertexIndex);

        assertArrayEquals(new int[] {
            0b010000, 0b010000, 0b110000, 0b010000
        }, re.edgeIndexToType);

        assertArrayEquals(new int[] { 0, 0, 1 }, re.chainIndexToStartVertexIndex);
        assertArrayEquals(new int[] { 3, 2, 4 }, re.chainIndexToFinishVertexIndex);

        // assert initial order and metrics before some actions.
        assertEquals(2, re.numberOfCrossesBefore);

        // TODO: problem with different ports in one vertex.
        assertArrayEquals(
            new long[] { 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 0 },
            re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        // assert final data.
        assertArrayEquals(new int[] {
            0, 1,
            1, 0, 2
        }, re.vertexIndexToOrder);

        assertEquals(1, re.numberOfCrosses);
    }

    @Test
    void testExecute_two_cycles_and_one_long_edge() {
        // arrange
        var topBeacon = UUID.randomUUID();
        var bottomBeacon = UUID.randomUUID();

        var vertex1 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();

        var long1Dummy1 = UUID.randomUUID();

        var trgComp1Vertex1 = UUID.randomUUID();
        var trgComp2Vertex1 = UUID.randomUUID();

        var srcComp1Vertex3 = UUID.randomUUID();
        var srcComp2Vertex3 = UUID.randomUUID();

        var cycle1Dummy1 = UUID.randomUUID();
        var cycle1Dummy2 = UUID.randomUUID();
        var cycle1Dummy3 = UUID.randomUUID();

        var cycle2Dummy1 = UUID.randomUUID();
        var cycle2Dummy2 = UUID.randomUUID();
        var cycle2Dummy3 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            // 0 1 2
            trgComp1Vertex1, trgComp2Vertex1, topBeacon,

            // 3 4 5
            cycle1Dummy1, cycle2Dummy1, vertex1,

            // 6 7 8 9
            cycle1Dummy2, cycle2Dummy2, long1Dummy1, vertex2,

            // 10 11 12
            cycle1Dummy3, cycle2Dummy3, vertex3,

            // 13 14 15
            srcComp1Vertex3, srcComp2Vertex3, bottomBeacon,
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(topBeacon, vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgComp1Vertex1, vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgComp2Vertex1, vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgComp1Vertex1, cycle1Dummy1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgComp2Vertex1, cycle2Dummy1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, long1Dummy1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, vertex2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy1, cycle1Dummy2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy1, cycle2Dummy2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy1, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy2, cycle1Dummy3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy2, cycle2Dummy3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, bottomBeacon));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, srcComp1Vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, srcComp2Vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy3, srcComp1Vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy3, srcComp2Vertex3));

        var vertexIdToLayer = new LinkedHashMap<UUID, Integer>();
        vertexIdToLayer.put(topBeacon, 0);
        vertexIdToLayer.put(trgComp1Vertex1, 0);
        vertexIdToLayer.put(trgComp2Vertex1, 0);

        vertexIdToLayer.put(vertex1, 1);
        vertexIdToLayer.put(cycle1Dummy1, 1);
        vertexIdToLayer.put(cycle2Dummy1, 1);

        vertexIdToLayer.put(long1Dummy1, 2);
        vertexIdToLayer.put(vertex2, 2);
        vertexIdToLayer.put(cycle1Dummy2, 2);
        vertexIdToLayer.put(cycle2Dummy2, 2);

        vertexIdToLayer.put(vertex3, 3);
        vertexIdToLayer.put(cycle1Dummy3, 3);
        vertexIdToLayer.put(cycle2Dummy3, 3);

        vertexIdToLayer.put(bottomBeacon, 4);
        vertexIdToLayer.put(srcComp1Vertex3, 4);
        vertexIdToLayer.put(srcComp2Vertex3, 4);

        var dummyVertices = List.of(
            topBeacon,
            bottomBeacon,

            trgComp1Vertex1,
            srcComp1Vertex3,

            trgComp2Vertex1,
            srcComp2Vertex3,

            long1Dummy1,

            cycle1Dummy1,
            cycle1Dummy2,
            cycle1Dummy3,

            cycle2Dummy1,
            cycle2Dummy2,
            cycle2Dummy3);

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            Collections.emptyMap(),
            vertexIdToLayer,
            dummyVertices,
            Collections.emptyMap(),
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        assertEquals(0, re.numberOfCrossesBefore);

        var expectedCycleVertexIndexToParentVertexIndex = new int[16];
        Arrays.fill(expectedCycleVertexIndexToParentVertexIndex, -1);
        expectedCycleVertexIndexToParentVertexIndex[0] = 5;
        expectedCycleVertexIndexToParentVertexIndex[1] = 5;
        expectedCycleVertexIndexToParentVertexIndex[13] = 12;
        expectedCycleVertexIndexToParentVertexIndex[14] = 12;

        assertArrayEquals(expectedCycleVertexIndexToParentVertexIndex, re.cycleVertexIndexToParentVertexIndex);

        assertArrayEquals(new int[] {
            0b000010, 0b001000, 0b001000, 0b001111, 0b001111,
            0b001111, 0b010000, 0b001111, 0b001111,
            0b001111, 0b010000, 0b001111, 0b001111,
            0b000001, 0b000100, 0b000100, 0b001111, 0b001111
        }, re.edgeIndexToType);

        assertArrayEquals(new int[] { 0, 1, 5, 5 }, re.chainIndexToStartVertexIndex);
        assertArrayEquals(new int[] { 13, 14, 12, 12 }, re.chainIndexToFinishVertexIndex);

        // assert initial order and metrics before some actions.
        assertEquals(0, re.numberOfCrossesBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        // assert final data.
        assertArrayEquals(new int[] {
            1, 2, 0,
            1, 2, 0,
            2, 3, 0, 1,
            1, 2, 0,
            1, 2, 0
        }, re.vertexIndexToOrder);

        assertEquals(0, re.numberOfCrosses);
    }

    @Test
    void testExecute_two_cycles_and_input_port_and_output_port() {
        // arrange
        var inputPort = UUID.randomUUID();
        var outputPort = UUID.randomUUID();

        var vertex1 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();

        var dummyVertex2Vertex3 = UUID.randomUUID();

        var trgComp1Vertex2 = UUID.randomUUID();
        var trgComp2Vertex3 = UUID.randomUUID();

        var srcComp1Vertex1 = UUID.randomUUID();
        var srcComp2Vertex1 = UUID.randomUUID();

        var long1Dummy1 = UUID.randomUUID();
        var long1Dummy2 = UUID.randomUUID();

        var long2Dummy1 = UUID.randomUUID();
        var long2Dummy2 = UUID.randomUUID();
        var long2Dummy3 = UUID.randomUUID();
        var long2Dummy4 = UUID.randomUUID();

        var long3Dummy1 = UUID.randomUUID();

        var cycle1Dummy1 = UUID.randomUUID();
        var cycle1Dummy2 = UUID.randomUUID();
        var cycle1Dummy3 = UUID.randomUUID();
        var cycle1Dummy4 = UUID.randomUUID();

        var cycle2Dummy1 = UUID.randomUUID();
        var cycle2Dummy2 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            inputPort,

            trgComp1Vertex2, long2Dummy1,

            cycle1Dummy1, vertex2, long2Dummy2,

            cycle1Dummy2, long1Dummy1, dummyVertex2Vertex3, long2Dummy3, trgComp2Vertex3,

            cycle1Dummy3, long1Dummy2, vertex3, long2Dummy4, cycle2Dummy1,

            cycle1Dummy4, vertex1, cycle2Dummy2,

            srcComp1Vertex1, long3Dummy1, srcComp2Vertex1,

            outputPort
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort, long2Dummy1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgComp1Vertex2, vertex2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgComp1Vertex2, cycle1Dummy1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long2Dummy1, long2Dummy2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy1, cycle1Dummy2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, long1Dummy1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, dummyVertex2Vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long2Dummy2, long2Dummy3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy2, cycle1Dummy3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy1, long1Dummy2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex2Vertex3, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long2Dummy3, long2Dummy4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgComp2Vertex3, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgComp2Vertex3, cycle2Dummy1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy3, cycle1Dummy4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy2, vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long2Dummy4, vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy1, cycle2Dummy2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy4, srcComp1Vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, srcComp1Vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, long3Dummy1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, srcComp2Vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy2, srcComp2Vertex1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long3Dummy1, outputPort));

        var vertexIdToLayer = new LinkedHashMap<UUID, Integer>();
        vertexIdToLayer.put(inputPort, 0);

        vertexIdToLayer.put(trgComp1Vertex2, 1);
        vertexIdToLayer.put(long2Dummy1, 1);

        vertexIdToLayer.put(cycle1Dummy1, 2);
        vertexIdToLayer.put(vertex2, 2);
        vertexIdToLayer.put(long2Dummy2, 2);

        vertexIdToLayer.put(cycle1Dummy2, 3);
        vertexIdToLayer.put(long1Dummy1, 3);
        vertexIdToLayer.put(dummyVertex2Vertex3, 3);
        vertexIdToLayer.put(long2Dummy3, 3);
        vertexIdToLayer.put(trgComp2Vertex3, 3);

        vertexIdToLayer.put(cycle1Dummy3, 4);
        vertexIdToLayer.put(long1Dummy2, 4);
        vertexIdToLayer.put(vertex3, 4);
        vertexIdToLayer.put(long2Dummy4, 4);
        vertexIdToLayer.put(cycle2Dummy1, 4);

        vertexIdToLayer.put(cycle1Dummy4, 5);
        vertexIdToLayer.put(vertex1, 5);
        vertexIdToLayer.put(cycle2Dummy2, 5);

        vertexIdToLayer.put(srcComp1Vertex1, 6);
        vertexIdToLayer.put(long3Dummy1, 6);
        vertexIdToLayer.put(srcComp2Vertex1, 6);

        vertexIdToLayer.put(outputPort, 7);

        var dummyVertices = List.of(
            trgComp1Vertex2, long2Dummy1,

            cycle1Dummy1, long2Dummy2,

            cycle1Dummy2, long1Dummy1, dummyVertex2Vertex3, long2Dummy3, trgComp2Vertex3,

            cycle1Dummy3, long1Dummy2, long2Dummy4, cycle2Dummy1,

            cycle1Dummy4, cycle2Dummy2,

            srcComp1Vertex1, long3Dummy1, srcComp2Vertex1
        );

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            Collections.emptyMap(),
            vertexIdToLayer,
            dummyVertices,
            Collections.emptyMap(),
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        // assert initial order and metrics before some actions.
        assertEquals(1, re.numberOfCrossesBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 1, 0, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 1, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        // assert final data.
        assertArrayEquals(new int[] {
            0,
            1, 0,
            2, 1, 0,
            4, 1, 2, 0, 3,
            4, 1, 2, 0, 3,
            2, 0, 1,
            2, 0, 1,
            0
        }, re.vertexIndexToOrder);

        assertEquals(0, re.numberOfCrosses);
    }

    @Test
    void testExecute_fixed_ports() {
        // arrange
        var inputPort1 = UUID.randomUUID();
        var inputPort2 = UUID.randomUUID();
        var inputPort3 = UUID.randomUUID();
        var outputPort1 = UUID.randomUUID();
        var outputPort2 = UUID.randomUUID();
        var outputPort3 = UUID.randomUUID();

        var vertex11 = UUID.randomUUID();
        var vertex12 = UUID.randomUUID();
        var vertex13 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();

        var dummyVertex11Vertex3 = UUID.randomUUID();
        var dummyVertex13Vertex3 = UUID.randomUUID();

        var dummyVertex2Output1 = UUID.randomUUID();
        var dummyVertex2Output3 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            // 0 1 2
            inputPort1, inputPort2, inputPort3,

            // 3 4 5
            vertex11, vertex12, vertex13,

            // 6 7 8
            dummyVertex11Vertex3, vertex2, dummyVertex13Vertex3,

            // 9 10 11
            dummyVertex2Output1, vertex3, dummyVertex2Output3,

            // 12 13 14
            outputPort1, outputPort2, outputPort3
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort2, vertex11));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort1, vertex12));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort3, vertex12));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort2, vertex13));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex11, dummyVertex11Vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex12, vertex2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex13, dummyVertex13Vertex3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex11Vertex3, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, dummyVertex2Output1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, dummyVertex2Output3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex13Vertex3, vertex3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex2Output1, outputPort1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex2Output3, outputPort3));

        var vertexIdToLayer = new LinkedHashMap<UUID, Integer>();
        vertexIdToLayer.put(inputPort1, 0);
        vertexIdToLayer.put(inputPort2, 0);
        vertexIdToLayer.put(inputPort3, 0);

        vertexIdToLayer.put(vertex11, 1);
        vertexIdToLayer.put(vertex12, 1);
        vertexIdToLayer.put(vertex13, 1);

        vertexIdToLayer.put(dummyVertex11Vertex3, 2);
        vertexIdToLayer.put(vertex2, 2);
        vertexIdToLayer.put(dummyVertex13Vertex3, 2);

        vertexIdToLayer.put(dummyVertex2Output1, 3);
        vertexIdToLayer.put(vertex3, 3);
        vertexIdToLayer.put(dummyVertex2Output3, 3);

        vertexIdToLayer.put(outputPort1, 4);
        vertexIdToLayer.put(outputPort2, 4);
        vertexIdToLayer.put(outputPort3, 4);

        var dummyVertices = List.of(
            dummyVertex11Vertex3, dummyVertex13Vertex3,

            dummyVertex2Output1, dummyVertex2Output3
        );

        var vertexIdToInitialOrder = Map.of(
            inputPort1, 0,
            inputPort2, 1,
            inputPort3, 2,

            outputPort1, 0,
            outputPort2, 1,
            outputPort3, 2
        );

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            Collections.emptyMap(),
            vertexIdToLayer,
            dummyVertices,
            vertexIdToInitialOrder,
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        var expectedCycleVertexIndexToParentVertexIndex = new int[15];
        Arrays.fill(expectedCycleVertexIndexToParentVertexIndex, -1);

        assertArrayEquals(expectedCycleVertexIndexToParentVertexIndex, re.cycleVertexIndexToParentVertexIndex);

        // assert initial order and metrics before some actions.
        assertEquals(4, re.numberOfCrossesBefore);

        assertArrayEquals(
                new long[] { 0, 2, 0, 2, 0 },
                re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        assertArrayEquals(
            new long[] { 2, 0, 2, 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        // assert final data.
        assertEquals(2, re.numberOfCrosses);
    }

    @Test
    void testExecute_two_cycles_part1_of_phase_63_graph() {
        // arrange
        var vertex1 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex31 = UUID.randomUUID();
        var vertex32 = UUID.randomUUID();
        var vertex41 = UUID.randomUUID();
        var vertex42 = UUID.randomUUID();

        var dummyVertex1Vertex2 = UUID.randomUUID();
        var dummyVertex31Vertex41 = UUID.randomUUID();
        var dummyVertex32Vertex42 = UUID.randomUUID();

        var trgCompVertex1 = UUID.randomUUID();
        var trgCompVertex2 = UUID.randomUUID();

        var srcCompVertex31 = UUID.randomUUID();
        var srcCompVertex41 = UUID.randomUUID();

        var cycle1Dummy1 = UUID.randomUUID();
        var cycle1Dummy2 = UUID.randomUUID();
        var cycle1Dummy3 = UUID.randomUUID();
        var cycle1Dummy4 = UUID.randomUUID();
        var cycle1Dummy5 = UUID.randomUUID();
        var cycle1Dummy6 = UUID.randomUUID();

        var cycle2Dummy1 = UUID.randomUUID();
        var cycle2Dummy2 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            trgCompVertex1,

            cycle1Dummy1, vertex1,

            cycle1Dummy2, dummyVertex1Vertex2, trgCompVertex2,

            cycle1Dummy3, vertex2, cycle2Dummy1,

            cycle1Dummy4, vertex31, vertex32, cycle2Dummy2,

            cycle1Dummy5, dummyVertex31Vertex41, srcCompVertex31, dummyVertex32Vertex42,

            cycle1Dummy6, vertex41, vertex42,

            srcCompVertex41
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex1, cycle1Dummy1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex1, vertex1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy1, cycle1Dummy2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, dummyVertex1Vertex2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy2, cycle1Dummy3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex1Vertex2, vertex2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex2, vertex2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex2, cycle2Dummy1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy3, cycle1Dummy4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, vertex31));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, vertex32));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy1, cycle2Dummy2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy4, cycle1Dummy5));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex31, dummyVertex31Vertex41));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex32, dummyVertex32Vertex42));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex31, srcCompVertex31));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy2, srcCompVertex31));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy5, cycle1Dummy6));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex31Vertex41, vertex41));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex32Vertex42, vertex42));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy6, srcCompVertex41));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex41, srcCompVertex41));

        var vertexIdToLayer = new LinkedHashMap<UUID, Integer>();
        vertexIdToLayer.put(trgCompVertex1, 0);

        vertexIdToLayer.put(cycle1Dummy1, 1);
        vertexIdToLayer.put(vertex1, 1);

        vertexIdToLayer.put(cycle1Dummy2, 2);
        vertexIdToLayer.put(dummyVertex1Vertex2, 2);
        vertexIdToLayer.put(trgCompVertex2, 2);

        vertexIdToLayer.put(cycle1Dummy3, 3);
        vertexIdToLayer.put(vertex2, 3);
        vertexIdToLayer.put(cycle2Dummy1, 3);

        vertexIdToLayer.put(cycle1Dummy4, 4);
        vertexIdToLayer.put(vertex31, 4);
        vertexIdToLayer.put(vertex32, 4);
        vertexIdToLayer.put(cycle2Dummy2, 4);

        vertexIdToLayer.put(cycle1Dummy5, 5);
        vertexIdToLayer.put(dummyVertex31Vertex41, 5);
        vertexIdToLayer.put(srcCompVertex31, 5);
        vertexIdToLayer.put(dummyVertex32Vertex42, 5);

        vertexIdToLayer.put(cycle1Dummy6, 6);
        vertexIdToLayer.put(vertex41, 6);
        vertexIdToLayer.put(vertex42, 6);

        vertexIdToLayer.put(srcCompVertex41, 7);

        var dummyVertices = List.of(
            trgCompVertex1,

            cycle1Dummy1,

            cycle1Dummy2, dummyVertex1Vertex2, trgCompVertex2,

            cycle1Dummy3, cycle2Dummy1,

            cycle1Dummy4, cycle2Dummy2,

            cycle1Dummy5, dummyVertex31Vertex41, srcCompVertex31, dummyVertex32Vertex42,

            cycle1Dummy6,

            srcCompVertex41
        );

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            Collections.emptyMap(),
            vertexIdToLayer,
            dummyVertices,
            Collections.emptyMap(),
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        var expectedCycleVertexIndexToParentVertexIndex = new int[21];
        Arrays.fill(expectedCycleVertexIndexToParentVertexIndex, -1);
        expectedCycleVertexIndexToParentVertexIndex[0] = 2;
        expectedCycleVertexIndexToParentVertexIndex[5] = 7;
        expectedCycleVertexIndexToParentVertexIndex[15] = 10;
        expectedCycleVertexIndexToParentVertexIndex[20] = 18;

        assertArrayEquals(expectedCycleVertexIndexToParentVertexIndex, re.cycleVertexIndexToParentVertexIndex);

        assertArrayEquals(new int[] {  0,  5,  7 }, re.chainIndexToStartVertexIndex);
        assertArrayEquals(new int[] { 20, 15, 19 }, re.chainIndexToFinishVertexIndex);

        // assert initial order and metrics before some actions.
        assertEquals(1, re.numberOfCrossesBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 1, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 0, 1, 0, 0 },
            re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        // assert final data.
        assertEquals(0, re.numberOfCrosses);
    }

    @Test
    void testExecute_two_cycles_part2_of_phase_63_graph() {
        // arrange
        var vertex1 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();
        var vertex4 = UUID.randomUUID();
        var vertex51 = UUID.randomUUID();
        var vertex52 = UUID.randomUUID();
        var vertex61 = UUID.randomUUID();
        var vertex62 = UUID.randomUUID();
        var vertex7 = UUID.randomUUID();

        var dummyVertex2Vertex3 = UUID.randomUUID();
        var dummyVertex3Vertex4 = UUID.randomUUID();
        var dummyVertex51Vertex61 = UUID.randomUUID();
        var dummyVertex52Vertex62 = UUID.randomUUID();
        var dummyVertex61Vertex7 = UUID.randomUUID();

        var trgCompVertex3 = UUID.randomUUID();
        var trgCompVertex4 = UUID.randomUUID();

        var srcCompVertex52 = UUID.randomUUID();
        var srcCompVertex62 = UUID.randomUUID();

        var long1Dummy1 = UUID.randomUUID();
        var long1Dummy2 = UUID.randomUUID();
        var long1Dummy3 = UUID.randomUUID();
        var long1Dummy4 = UUID.randomUUID();
        var long1Dummy5 = UUID.randomUUID();
        var long1Dummy6 = UUID.randomUUID();
        var long1Dummy7 = UUID.randomUUID();

        var cycle1Dummy1 = UUID.randomUUID();
        var cycle1Dummy2 = UUID.randomUUID();
        var cycle1Dummy3 = UUID.randomUUID();
        var cycle1Dummy4 = UUID.randomUUID();
        var cycle1Dummy5 = UUID.randomUUID();
        var cycle1Dummy6 = UUID.randomUUID();

        var cycle2Dummy1 = UUID.randomUUID();
        var cycle2Dummy2 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            vertex1,

            long1Dummy1, vertex2,

            long1Dummy2, dummyVertex2Vertex3, trgCompVertex3,

            long1Dummy3, vertex3, cycle1Dummy1,

            long1Dummy4, dummyVertex3Vertex4, cycle1Dummy2, trgCompVertex4,

            long1Dummy5, vertex4, cycle1Dummy3, cycle2Dummy1,

            long1Dummy6, vertex51, vertex52, cycle1Dummy4, cycle2Dummy2,

            long1Dummy7, dummyVertex51Vertex61, dummyVertex52Vertex62, cycle1Dummy5, srcCompVertex52,

            vertex61, vertex62, cycle1Dummy6,

            dummyVertex61Vertex7, srcCompVertex62,

            vertex7
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, long1Dummy1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, vertex2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy1, long1Dummy2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, dummyVertex2Vertex3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy2, long1Dummy3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex2Vertex3, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex3, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex3, cycle1Dummy1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy3, long1Dummy4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, dummyVertex3Vertex4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy1, cycle1Dummy2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy4, long1Dummy5));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex3Vertex4, vertex4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy2, cycle1Dummy3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex4, vertex4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex4, cycle2Dummy1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy5, long1Dummy6));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex4, vertex51));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex4, vertex52));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy3, cycle1Dummy4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy1, cycle2Dummy2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy6, long1Dummy7));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex51, dummyVertex51Vertex61));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex52, dummyVertex52Vertex62));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy4, cycle1Dummy5));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex52, srcCompVertex52));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy2, srcCompVertex52));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy7, vertex61));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex51Vertex61, vertex61));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex52Vertex62, vertex62));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy5, cycle1Dummy6));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex61, dummyVertex61Vertex7));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex62, srcCompVertex62));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle1Dummy6, srcCompVertex62));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex61Vertex7, vertex7));

        var vertexIdToLayer = new LinkedHashMap<UUID, Integer>();
        vertexIdToLayer.put(vertex1, 0);

        vertexIdToLayer.put(long1Dummy1, 1);
        vertexIdToLayer.put(vertex2, 1);

        vertexIdToLayer.put(long1Dummy2, 2);
        vertexIdToLayer.put(dummyVertex2Vertex3, 2);
        vertexIdToLayer.put(trgCompVertex3, 2);

        vertexIdToLayer.put(long1Dummy3, 3);
        vertexIdToLayer.put(vertex3, 3);
        vertexIdToLayer.put(cycle1Dummy1, 3);

        vertexIdToLayer.put(long1Dummy4, 4);
        vertexIdToLayer.put(dummyVertex3Vertex4, 4);
        vertexIdToLayer.put(cycle1Dummy2, 4);
        vertexIdToLayer.put(trgCompVertex4, 4);

        vertexIdToLayer.put(long1Dummy5, 5);
        vertexIdToLayer.put(vertex4, 5);
        vertexIdToLayer.put(cycle1Dummy3, 5);
        vertexIdToLayer.put(cycle2Dummy1, 5);

        vertexIdToLayer.put(long1Dummy6, 6);
        vertexIdToLayer.put(vertex51, 6);
        vertexIdToLayer.put(vertex52, 6);
        vertexIdToLayer.put(cycle1Dummy4, 6);
        vertexIdToLayer.put(cycle2Dummy2, 6);

        vertexIdToLayer.put(long1Dummy7, 7);
        vertexIdToLayer.put(dummyVertex51Vertex61, 7);
        vertexIdToLayer.put(dummyVertex52Vertex62, 7);
        vertexIdToLayer.put(cycle1Dummy5, 7);
        vertexIdToLayer.put(srcCompVertex52, 7);

        vertexIdToLayer.put(vertex61, 8);
        vertexIdToLayer.put(vertex62, 8);
        vertexIdToLayer.put(cycle1Dummy6, 8);

        vertexIdToLayer.put(dummyVertex61Vertex7, 9);
        vertexIdToLayer.put(srcCompVertex62, 9);

        vertexIdToLayer.put(vertex7, 10);

        var dummyVertices = List.of(
            long1Dummy1,

            long1Dummy2, dummyVertex2Vertex3, trgCompVertex3,

            long1Dummy3, cycle1Dummy1,

            long1Dummy4, dummyVertex3Vertex4, cycle1Dummy2, trgCompVertex4,

            long1Dummy5, cycle1Dummy3, cycle2Dummy1,

            long1Dummy6, cycle1Dummy4, cycle2Dummy2,

            long1Dummy7, dummyVertex51Vertex61, dummyVertex52Vertex62, cycle1Dummy5, srcCompVertex52,

            cycle1Dummy6,

            dummyVertex61Vertex7, srcCompVertex62
        );

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            Collections.emptyMap(),
            vertexIdToLayer,
            dummyVertices,
            Collections.emptyMap(),
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        var expectedCycleVertexIndexToParentVertexIndex = new int[33];
        Arrays.fill(expectedCycleVertexIndexToParentVertexIndex, -1);

        expectedCycleVertexIndexToParentVertexIndex[5] = 7;
        expectedCycleVertexIndexToParentVertexIndex[12] = 14;
        expectedCycleVertexIndexToParentVertexIndex[26] = 19;
        expectedCycleVertexIndexToParentVertexIndex[31] = 28;

        assertArrayEquals(expectedCycleVertexIndexToParentVertexIndex, re.cycleVertexIndexToParentVertexIndex);

        assertArrayEquals(new int[] {  0, 0,  5, 12, 14 }, re.chainIndexToStartVertexIndex);
        assertArrayEquals(new int[] { 27, 7, 31, 26, 27 }, re.chainIndexToFinishVertexIndex);

        // assert initial order and metrics before some actions.
        assertEquals(2, re.numberOfCrossesBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0 },
            re.layerToNumberOfCrossesFromTopToBottomBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0 },
            re.layerToNumberOfCrossesFromBottomToTopBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        // assert final data.
        assertEquals(0, re.numberOfCrosses);
    }

    @Test
    void testExecute_of_phase_63_part3_graph() {
        // arrange
        var vertex1 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();
        var vertex41 = UUID.randomUUID();
        var vertex42 = UUID.randomUUID();
        var vertex51 = UUID.randomUUID();
        var vertex52 = UUID.randomUUID();
        var vertex6 = UUID.randomUUID();
        var vertex7 = UUID.randomUUID();
        var vertex8 = UUID.randomUUID();

        var dummyVertex2Vertex3 = UUID.randomUUID();
        var dummyVertex41Vertex51 = UUID.randomUUID();
        var dummyVertex42Vertex52 = UUID.randomUUID();
        var dummyVertex6Vertex8 = UUID.randomUUID();

        var trgCompVertex3 = UUID.randomUUID();

        var srcCompVertex41 = UUID.randomUUID();

        var long1Dummy1 = UUID.randomUUID();
        var long1Dummy2 = UUID.randomUUID();
        var long1Dummy3 = UUID.randomUUID();
        var long1Dummy4 = UUID.randomUUID();
        var long1Dummy5 = UUID.randomUUID();
        var long1Dummy6 = UUID.randomUUID();

        var long2Dummy1 = UUID.randomUUID();
        var long2Dummy2 = UUID.randomUUID();

        var cycle2Dummy1 = UUID.randomUUID();
        var cycle2Dummy2 = UUID.randomUUID();

        var arrayOfVertices = new UUID[]{
                // 0
                vertex1,

                // 1, 2
                long1Dummy1, vertex2,

                // 3, 4, 5
                long1Dummy2, dummyVertex2Vertex3, trgCompVertex3,

                // 6, 7, 8
                long1Dummy3, vertex3, cycle2Dummy1,

                // 9, 10, 11, 12
                long1Dummy4, vertex41, cycle2Dummy2, vertex42,

                // 13, 14, 15, 16, 17
                long1Dummy5, dummyVertex41Vertex51, srcCompVertex41, dummyVertex42Vertex52, long2Dummy1,

                // 18, 19, 20, 21
                long1Dummy6, vertex51, vertex52, long2Dummy2,

                // 22
                vertex6,

                // 23, 24
                dummyVertex6Vertex8, vertex7,

                // 25
                vertex8
        };
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, long1Dummy1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, vertex2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy1, long1Dummy2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, dummyVertex2Vertex3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy2, long1Dummy3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex2Vertex3, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex3, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(trgCompVertex3, cycle2Dummy1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy3, long1Dummy4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, vertex41));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, vertex42));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy1, cycle2Dummy2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy4, long1Dummy5));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex41, dummyVertex41Vertex51));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex42, dummyVertex42Vertex52));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex41, srcCompVertex41));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(cycle2Dummy2, srcCompVertex41));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex42, long2Dummy1));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy5, long1Dummy6));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex41Vertex51, vertex51));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex42Vertex52, vertex52));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long2Dummy1, long2Dummy2));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long1Dummy6, vertex6));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(long2Dummy2, vertex6));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex51, vertex6));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex6, dummyVertex6Vertex8));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex6, vertex7));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex6Vertex8, vertex8));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex7, vertex8));

        var vertexIdToLayer = new LinkedHashMap<UUID, Integer>();
        vertexIdToLayer.put(vertex1, 0);

        vertexIdToLayer.put(long1Dummy1, 1);
        vertexIdToLayer.put(vertex2, 1);

        vertexIdToLayer.put(long1Dummy2, 2);
        vertexIdToLayer.put(dummyVertex2Vertex3, 2);
        vertexIdToLayer.put(trgCompVertex3, 2);

        vertexIdToLayer.put(long1Dummy3, 3);
        vertexIdToLayer.put(vertex3, 3);
        vertexIdToLayer.put(cycle2Dummy1, 3);

        vertexIdToLayer.put(long1Dummy4, 4);
        vertexIdToLayer.put(vertex41, 4);
        vertexIdToLayer.put(vertex42, 4);
        vertexIdToLayer.put(cycle2Dummy2, 4);

        vertexIdToLayer.put(long1Dummy5, 5);
        vertexIdToLayer.put(dummyVertex41Vertex51, 5);
        vertexIdToLayer.put(srcCompVertex41, 5);
        vertexIdToLayer.put(long2Dummy1, 5);
        vertexIdToLayer.put(dummyVertex42Vertex52, 5);

        vertexIdToLayer.put(long1Dummy6, 6);
        vertexIdToLayer.put(vertex51, 6);
        vertexIdToLayer.put(vertex52, 6);
        vertexIdToLayer.put(long2Dummy2, 6);

        vertexIdToLayer.put(vertex6, 7);

        vertexIdToLayer.put(dummyVertex6Vertex8, 8);
        vertexIdToLayer.put(vertex7, 8);

        vertexIdToLayer.put(vertex8, 9);

        var dummyVertices = List.of(
            long1Dummy1,

            long1Dummy2, dummyVertex2Vertex3, trgCompVertex3,

            long1Dummy3, cycle2Dummy1,

            long1Dummy4, cycle2Dummy2,

            long1Dummy5, dummyVertex41Vertex51, srcCompVertex41, long2Dummy1, dummyVertex42Vertex52,

            long1Dummy6, long2Dummy2,

            dummyVertex6Vertex8
        );

        // act
        var re = new CustomCrossingReductionForOneGroupOperation(
            vertices,
            edgeIdToNodeIds,
            Collections.emptyMap(),
            vertexIdToLayer,
            dummyVertices,
            Collections.emptyMap(),
            Collections.emptyMap()).execute();

        // assert prepared data.
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertexIndexToId);

        var expectedCycleVertexIndexToParentVertexIndex = new int[26];
        Arrays.fill(expectedCycleVertexIndexToParentVertexIndex, -1);

        expectedCycleVertexIndexToParentVertexIndex[5] = 7;
        expectedCycleVertexIndexToParentVertexIndex[15] = 10;

        assertArrayEquals(expectedCycleVertexIndexToParentVertexIndex, re.cycleVertexIndexToParentVertexIndex);

        assertArrayEquals(new int[] {  0, 0,  5, 10, 12, 22, 22 }, re.chainIndexToStartVertexIndex);
        assertArrayEquals(new int[] { 22, 7, 15, 22, 22, 25, 25 }, re.chainIndexToFinishVertexIndex);
        assertArrayEquals(new int[] {
                4,
                1, 4,
                1, 4, 2,
                1, 4, 2,
                1, 4, 2, 4,
                1, 4, 2, 0, 3,
                1, 4, 0, 3,
                4,
                4, 4,
                4 }, re.vertexIndexToWeight);

        assertArrayEquals(new int[] { 16, 20 }, re.weightToVertexIndices[0]);
        assertArrayEquals(new int[] { 1, 3, 6, 9, 13, 18 }, re.weightToVertexIndices[1]);
        assertArrayEquals(new int[] { 5, 8, 11, 15 }, re.weightToVertexIndices[2]);
        assertArrayEquals(new int[] { 17, 21 }, re.weightToVertexIndices[3]);
        assertArrayEquals(new int[] { 0, 2, 4, 7, 10, 12, 14, 19, 22, 23, 24, 25 }, re.weightToVertexIndices[4]);

        assertArrayEquals(new int[] { 0 }, re.layerToTopTrapVertexIndices[0]);
        assertArrayEquals(new int[] { 0 }, re.layerToTopTrapVertexIndices[1]);
        assertArrayEquals(new int[] { 5, 0 }, re.layerToTopTrapVertexIndices[2]);
        assertArrayEquals(new int[] { 7, 5, 0 }, re.layerToTopTrapVertexIndices[3]);
        assertArrayEquals(new int[] { 10, 12, 7, 5, 0 }, re.layerToTopTrapVertexIndices[4]);
        assertArrayEquals(new int[] { 10, 12, 7, 5, 0 }, re.layerToTopTrapVertexIndices[5]);
        assertArrayEquals(new int[] { 10, 12, 7, 5, 0 }, re.layerToTopTrapVertexIndices[6]);
        assertArrayEquals(new int[] { 22, 10, 12, 7, 5, 0 }, re.layerToTopTrapVertexIndices[7]);
        assertArrayEquals(new int[] { 22, 10, 12, 7, 5, 0 }, re.layerToTopTrapVertexIndices[8]);
        assertArrayEquals(new int[] { 22, 10, 12, 7, 5, 0 }, re.layerToTopTrapVertexIndices[9]);

        assertArrayEquals(new int[] { 7, 15, 22, 25 }, re.layerToBottomTrapVertexIndices[0]);
        assertArrayEquals(new int[] { 7, 15, 22, 25 }, re.layerToBottomTrapVertexIndices[1]);
        assertArrayEquals(new int[] { 7, 15, 22, 25 }, re.layerToBottomTrapVertexIndices[2]);
        assertArrayEquals(new int[] { 7, 15, 22, 25 }, re.layerToBottomTrapVertexIndices[3]);
        assertArrayEquals(new int[] { 15, 22, 25 }, re.layerToBottomTrapVertexIndices[4]);
        assertArrayEquals(new int[] { 15, 22, 25 }, re.layerToBottomTrapVertexIndices[5]);
        assertArrayEquals(new int[] { 22, 25 }, re.layerToBottomTrapVertexIndices[6]);
        assertArrayEquals(new int[] { 22, 25 }, re.layerToBottomTrapVertexIndices[7]);
        assertArrayEquals(new int[] { 25 }, re.layerToBottomTrapVertexIndices[8]);
        assertArrayEquals(new int[] { 25 }, re.layerToBottomTrapVertexIndices[9]);

        // assert initial order and metrics before some actions.
        assertEquals(1, re.numberOfCrossesBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 1, 0, 0, 0, 0, 0, 0 },
            re.layerToNumberOfCrossesFromBottomToTopBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
            re.layerToNumberOfCrossesFromTopToBottomBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithBottomTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 1, 1, 0, 0, 0, 0 },
            re.layerToNumberOfChainCrossesWithTopTrapsBefore);

        assertArrayEquals(
            new long[] { 0, 0, 0, 0, 1, 0, 1, 0, 0, 0 },
            re.layerToNumberOfNeighborhoodAnomaliesBefore);

        // assert final data.
        assertEquals(0, re.numberOfCrosses);
    }
}
