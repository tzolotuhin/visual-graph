package vg.plugin.glayout.hierarchical.step2.fine;

import org.junit.jupiter.api.Test;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.step2.legacy.EstCrossesOperation;
import vg.lib.layout.hierarchical.step2.legacy.data.CrossingReductionTable;
import vg.lib.layout.hierarchical.step2.legacy.data.Edge;

import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

// TODO: допроверить остальные уровни, чтобы зафиксировать решение, лучше перенести в CrossingReductionRowProcedureTest.
class EstCrossesOperationTest {
    @Test
    void execute1() {
        var graph = new HierarchicalGraph(new Point(20, 20), new Point(10, 10), new Point(10, 10), false, false);

        var n16 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n16", 0, new Point(30, 20));
        var n9 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n9", 0, new Point(30, 20));

        var n28 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n28", 1, new Point(30, 20));
        var n29 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n29", 1, new Point(30, 20));
        var n37 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n37", 1, new Point(30, 20));
        var n39 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n39", 1, new Point(30, 20));

        var n32 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n32", 2, new Point(30, 20));
        var n31 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n31", 2, new Point(30, 20));
        var n38 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n38", 2, new Point(30, 20));

        var n25 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n25", 3, new Point(30, 20));
        var n35 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n35", 3, new Point(30, 20));
        var n36 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n36", 3, new Point(30, 20));

        graph.addEdge(n16, n28);
        graph.addFakeEdge(n16, n25);
        graph.addEdge(n16, n29);
        graph.addEdge(n9, n37);
        graph.addEdge(n9, n39);

        graph.addEdge(n28, n32);
        graph.addEdge(n28, n31);
        graph.addEdge(n37, n31);
        graph.addEdge(n37, n38);

        graph.addEdge(n31, n25);
        graph.addEdge(n31, n35);
        graph.addEdge(n31, n36);

        graph.build(null);

        graph.applyOrders(0, Arrays.asList("n16", "n9"));
        graph.applyOrders(1, Arrays.asList("n28", "n16 <-> n25 -- F", "n29", "n37", "n39"));
        graph.applyOrders(2, Arrays.asList("n32", "n16 <-> n25 -- F", "n31", "n38"));
        graph.applyOrders(3, Arrays.asList("n25", "n35", "n36"));

        var table = new CrossingReductionTable(graph.getDefaultDetailedGroup(), graph);

        {
            var row = table.rows[1];
            long re = new EstCrossesOperation(row, Direction.ALL).execute();
            assertEquals(0, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(0, re);
        }

        {
            var row = table.rows[2];
            long re = new EstCrossesOperation(row, Direction.ALL).execute();
            assertEquals(2, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(Edge.GENERAL_CROSS_W, re);
        }

        {
            var row = table.rows[3];
            long re = new EstCrossesOperation(row, Direction.ALL).execute();
            assertEquals(4, row.topGroups.length);
            assertEquals(1, row.bottomGroups.length);
            assertEquals(Edge.GENERAL_CROSS_W, re);
        }

        {
            var row = table.rows[4];
            long re = new EstCrossesOperation(row, Direction.BOTTOM).execute();
            assertEquals(5, row.topGroups.length);
            assertEquals(0, row.bottomGroups.length);
            assertEquals(0, re);
        }
    }

    @Test
    void execute2() {
        var graph = new HierarchicalGraph(new Point(20, 20), new Point(10, 10), new Point(10, 10), false, false);

        var n10 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n10", 0, new Point(30, 20));

        var n11 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n11", 1, new Point(30, 20));

        var n1 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n1", 2, new Point(30, 20));

        var n3 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n3", 3, new Point(30, 20));

        var n4 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n4", 4, new Point(30, 20));

        var n8 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n8", 5, new Point(30, 20));
        var n5 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n5", 5, new Point(30, 20));

        var n2 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n2", 6, new Point(30, 20));
        var n6 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n6", 6, new Point(30, 20));

        var n7 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n7", 7, new Point(30, 20));
        var n12 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n12", 7, new Point(30, 20));

        var n13 = graph.addRealVertexWithDefaultGroupId(UUID.randomUUID(), "n13", 8, new Point(30, 20));

        graph.addEdge(n10, n11);
        graph.addFakeEdge(n11, n1);
        graph.addFakeEdge(n11, n12);
        graph.addEdge(n1, n3);
        graph.addEdge(n3, n4);
        graph.addEdge(n4, n8);
        graph.addEdge(n4, n5);
        graph.addEdge(n8, n2);
        graph.addEdge(n5, n6);
        graph.addEdge(n2, n12);
        graph.addEdge(n6, n7);
        graph.addEdge(n12, n13);

        graph.addEdge(n7, n4);
        graph.addEdge(n6, n4);

        graph.build(null);

        graph.applyOrders(0, Collections.singletonList("n10"));
        graph.applyOrders(1, Collections.singletonList("n11"));
        graph.applyOrders(2, Arrays.asList("n1", "n11 <-> n12 -- F"));
        graph.applyOrders(3, Arrays.asList("n3", "n11 <-> n12 -- F"));
        graph.applyOrders(4, Arrays.asList("TC: n4 [n7]", "n3 <-> n4 -- F", "TC: n4 [n6]", "n11 <-> n12 -- F"));
        graph.applyOrders(5, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n4", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(6, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n8", "n5", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(7, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n2", "n6", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(8, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n2 <-> n12 -- F", "n6 <-> n7 -- F", "SC: n6 [n4]", "n11 <-> n12 -- F"));
        graph.applyOrders(9, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n7", "n12"));
        graph.applyOrders(10, Arrays.asList("SC: n7 [n4]", "BB: n7", "n12 <-> n13 -- F"));
        graph.applyOrders(11, Collections.singletonList("n13"));

        var table = new CrossingReductionTable(graph.getDefaultDetailedGroup(), graph);

        {
            var row = table.rows[9];
            long re = new EstCrossesOperation(row, Direction.BOTTOM).execute();
            assertEquals(5, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(Edge.GENERAL_CROSS_W, re);
        }
        {
            var row = table.rows[10];
            long re = new EstCrossesOperation(row, Direction.BOTTOM).execute();
            assertEquals(4, row.topGroups.length);
            assertEquals(1, row.bottomGroups.length);
            assertEquals(0, re);
        }

        graph.applyOrders(0, Collections.singletonList("n10"));
        graph.applyOrders(1, Collections.singletonList("n11"));
        graph.applyOrders(2, Arrays.asList("n1", "n11 <-> n12 -- F"));
        graph.applyOrders(3, Arrays.asList("n3", "n11 <-> n12 -- F"));
        graph.applyOrders(4, Arrays.asList("TC: n4 [n7]", "n3 <-> n4 -- F", "TC: n4 [n6]", "n11 <-> n12 -- F"));
        graph.applyOrders(5, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n4", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(6, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n5", "n8", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(7, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n6", "n2", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(8, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n6 <-> n7 -- F", "n2 <-> n12 -- F", "SC: n6 [n4]", "n11 <-> n12 -- F"));
        graph.applyOrders(9, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n7", "n12"));
        graph.applyOrders(10, Arrays.asList("SC: n7 [n4]", "BB: n7", "n12 <-> n13 -- F"));
        graph.applyOrders(11, Collections.singletonList("n13"));

        table = new CrossingReductionTable(graph.getDefaultDetailedGroup(), graph);
        {
            var row = table.rows[9];
            long re = new EstCrossesOperation(row, Direction.ALL).execute();
            assertEquals(5, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(Edge.MAIN_BACK_FAKE_CROSS_W, re);
        }

        //
        graph.applyOrders(0, Collections.singletonList("n10"));
        graph.applyOrders(1, Collections.singletonList("n11"));
        graph.applyOrders(2, Arrays.asList( "n11 <-> n12 -- F", "n1"));
        graph.applyOrders(3, Arrays.asList("n11 <-> n12 -- F", "n3"));
        graph.applyOrders(4, Arrays.asList("n11 <-> n12 -- F", "n3 <-> n4 -- F", "TC: n4 [n6]", "TC: n4 [n7]"));
        graph.applyOrders(5, Arrays.asList("n11 <-> n12 -- F", "n4", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "TC: n4 [n7] <-> SC: n7 [n4] -- F"));
        graph.applyOrders(6, Arrays.asList("n11 <-> n12 -- F", "n8", "n5", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "TC: n4 [n7] <-> SC: n7 [n4] -- F"));
        graph.applyOrders(7, Arrays.asList("n11 <-> n12 -- F", "n2", "n6", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "TC: n4 [n7] <-> SC: n7 [n4] -- F"));
        graph.applyOrders(8, Arrays.asList("n11 <-> n12 -- F", "n2 <-> n12 -- F", "n6 <-> n7 -- F", "SC: n6 [n4]", "TC: n4 [n7] <-> SC: n7 [n4] -- F"));
        graph.applyOrders(9, Arrays.asList("n12", "n7", "TC: n4 [n7] <-> SC: n7 [n4] -- F"));
        graph.applyOrders(10, Arrays.asList("n12 <-> n13 -- F", "BB: n7", "SC: n7 [n4]"));
        graph.applyOrders(11, Collections.singletonList("n13"));

        table = new CrossingReductionTable(graph.getDefaultDetailedGroup(), graph);

        {
            var row = table.rows[9];
            long re = new EstCrossesOperation(row, Direction.BOTTOM).execute();
            assertEquals(5, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(0, re);
        }

        // back edge between n6 and n2.
        graph.applyOrders(0, Collections.singletonList("n10"));
        graph.applyOrders(1, Collections.singletonList("n11"));
        graph.applyOrders(2, Arrays.asList("n1", "n11 <-> n12 -- F"));
        graph.applyOrders(3, Arrays.asList("n3", "n11 <-> n12 -- F"));
        graph.applyOrders(4, Arrays.asList("TC: n4 [n7]", "n3 <-> n4 -- F", "TC: n4 [n6]", "n11 <-> n12 -- F"));
        graph.applyOrders(5, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n4", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(6, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n5", "n8", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(7, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n6", "n2", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(8, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n6 <-> n7 -- F", "SC: n6 [n4]", "n2 <-> n12 -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(9, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n7", "n12"));
        graph.applyOrders(10, Arrays.asList("SC: n7 [n4]", "BB: n7", "n12 <-> n13 -- F"));
        graph.applyOrders(11, Collections.singletonList("n13"));

        table = new CrossingReductionTable(graph.getDefaultDetailedGroup(), graph);
        {
            var row = table.rows[9];
            long re = new EstCrossesOperation(row, Direction.BOTTOM).execute();
            assertEquals(5, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(Edge.MAIN_BACK_FAKE_CROSS_W, re);
        }

        // back edge between n7 -> n4 and n6.
        graph.applyOrders(0, Collections.singletonList("n10"));
        graph.applyOrders(1, Collections.singletonList("n11"));
        graph.applyOrders(2, Arrays.asList("n1", "n11 <-> n12 -- F"));
        graph.applyOrders(3, Arrays.asList("n3", "n11 <-> n12 -- F"));
        graph.applyOrders(4, Arrays.asList("TC: n4 [n7]", "n3 <-> n4 -- F", "TC: n4 [n6]", "n11 <-> n12 -- F"));
        graph.applyOrders(5, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n4", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(6, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n5", "n8", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(7, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n6", "n2", "TC: n4 [n6] <-> SC: n6 [n4] -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(8, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "SC: n6 [n4]", "n6 <-> n7 -- F", "n2 <-> n12 -- F", "n11 <-> n12 -- F"));
        graph.applyOrders(9, Arrays.asList("TC: n4 [n7] <-> SC: n7 [n4] -- F", "n7", "n12"));
        graph.applyOrders(10, Arrays.asList("SC: n7 [n4]", "BB: n7", "n12 <-> n13 -- F"));
        graph.applyOrders(11, Collections.singletonList("n13"));

        table = new CrossingReductionTable(graph.getDefaultDetailedGroup(), graph);
        {
            var row = table.rows[9];
            long re = new EstCrossesOperation(row, Direction.BOTTOM).execute();
            assertEquals(5, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(0, re);
        }
    }
}