package vg.plugin.glayout.hierarchical.step1;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import vg.lib.layout.hierarchical.step1.CustomLayeringOperation;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CustomLayeringOperationTest {
    @Test
    void testExecute() {
        // arrange
        var inputPort1 = UUID.randomUUID();
        var inputPort2 = UUID.randomUUID();
        var inputPort3 = UUID.randomUUID();

        var outputPort1 = UUID.randomUUID();
        var outputPort2 = UUID.randomUUID();

        var vertex1 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();
        var vertex4 = UUID.randomUUID();
        var vertex5 = UUID.randomUUID();
        var vertex6 = UUID.randomUUID();
        var vertex7 = UUID.randomUUID();
        var vertex8 = UUID.randomUUID();
        var vertex9 = UUID.randomUUID();

        var inputPorts = List.of(
            inputPort1,
            inputPort2,
            inputPort3
        );

        var outputPorts = List.of(
            outputPort1,
            outputPort2
        );

        var arrayOfVertices = new UUID[] {
            inputPort1,
            inputPort2,
            inputPort3,
            outputPort1,
            outputPort2,
            vertex1,
            vertex2,
            vertex3,
            vertex4,
            vertex5,
            vertex6,
            vertex7,
            vertex8,
            vertex9
        } ;
        var vertices = List.of(arrayOfVertices);

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort1, vertex1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort3, vertex6));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort2, vertex7));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, vertex2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, vertex4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex1, vertex4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex3, vertex4));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex5, vertex6));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex4, outputPort1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex9, vertex8));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex9, outputPort2));

        var portIdToSpecifiedOrder = Map.of(
            inputPort1, 0,
            inputPort2, 1,
            inputPort3, 2,

            outputPort1, 0,
            outputPort2, 1
        );

        // act
        var re = new CustomLayeringOperation(
            vertices, inputPorts, outputPorts, edgeIdToNodeIds, portIdToSpecifiedOrder).execute();

        // assert
        assertNotNull(re);

        assertArrayEquals(arrayOfVertices, re.vertices);

        assertArrayEquals(new int[] { 1, 2, 3, 4, 0, 1, 1, 4, 4, 5, 5, 2, 0, 0 }, re.vertexIndexToGroupIdBeforeMerge);

        assertArrayEquals(new int[] { 0, 0, 0, 2, 1, 1, 2, 0, 1, 0, 1, 1, 1, 0 }, re.vertexIndexToLayerBeforeMerge);

        assertArrayEquals(new int[] { 0, 0, 0, 4, 1, 1, 2, 2, 3, 0, 1, 1, 1, 0 }, re.vertexIndexToLayerAfterMerge);

        assertArrayEquals(new int[] { 0, 0, 0, 4, 4, 1, 2, 2, 3, 1, 2, 1, 3, 2 }, re.vertexIndexToLayer);

        assertArrayEquals(new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, re.vertexIndexToGroupId);
    }

    @Test
    void testExecute_arithmetic_1() {
        // arrange
        var inputPort1 = UUID.randomUUID();
        var inputPort2 = UUID.randomUUID();
        var inputPort3 = UUID.randomUUID();

        var outputPort1 = UUID.randomUUID();
        var outputPort2 = UUID.randomUUID();

        var vertex1 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();
        var vertex4 = UUID.randomUUID();

        var inputPort1ToVertex1 = UUID.randomUUID();
        var inputPort1ToVertex2 = UUID.randomUUID();
        var inputPort2ToVertex1 = UUID.randomUUID();
        var inputPort2ToVertex2 = UUID.randomUUID();
        var inputPort3ToVertex3 = UUID.randomUUID();
        var inputPort3ToVertex4 = UUID.randomUUID();

        var vertex1ToVertex3 = UUID.randomUUID();
        var vertex2ToVertex4 = UUID.randomUUID();

        var vertex3ToOutputPort1 = UUID.randomUUID();
        var vertex4ToOutputPort2 = UUID.randomUUID();

        var inputPorts = List.of(
            inputPort1,
            inputPort2,
            inputPort3
        );

        var outputPorts = List.of(
            outputPort1,
            outputPort2
        );

        var arrayOfVertices = new UUID[] {
            inputPort1,
            inputPort2,
            inputPort3,
            outputPort1,
            outputPort2,
            vertex1,
            vertex2,
            vertex3,
            vertex4
        } ;
        var vertices = List.of(arrayOfVertices);

        var edges = Map.of(
            inputPort1ToVertex1, Map.entry(inputPort1, vertex1),
            inputPort1ToVertex2, Map.entry(inputPort1, vertex2),
            inputPort2ToVertex1, Map.entry(inputPort2, vertex1),
            inputPort2ToVertex2, Map.entry(inputPort2, vertex2),
            inputPort3ToVertex3, Map.entry(inputPort3, vertex3),
            inputPort3ToVertex4, Map.entry(inputPort3, vertex4),

            vertex1ToVertex3, Map.entry(vertex1, vertex3),
            vertex2ToVertex4, Map.entry(vertex2, vertex4),

            vertex3ToOutputPort1, Map.entry(vertex3, outputPort1),
            vertex4ToOutputPort2, Map.entry(vertex4, outputPort2)
        );

        var portIdToSpecifiedOrder = Map.of(
            inputPort1, 0,
            inputPort2, 1,
            inputPort3, 2,

            outputPort1, 0,
            outputPort2, 1
        );

        // act
        var re = new CustomLayeringOperation(
            vertices, inputPorts, outputPorts, edges, portIdToSpecifiedOrder).execute();

        // assert
        assertNotNull(re);

        assertNotNull(re.vertices);
        assertArrayEquals(arrayOfVertices, re.vertices);

        var expectedVertexIndexToLayer = new int[] {
            0, 0, 0, 3, 3,
            1, 1, 2, 2
        };
        assertNotNull(re.vertexIndexToLayer);
        assertArrayEquals(expectedVertexIndexToLayer, re.vertexIndexToLayer);

        var expectedVertexIndexToGroupId = new int[] {
            0, 0, 0, 0, 0,
            0, 0, 0, 0
        };
        assertNotNull(re.vertexIndexToGroupId);
        assertArrayEquals(expectedVertexIndexToGroupId, re.vertexIndexToGroupId);
    }

    @Test
    void testExecute_main_cluster_15_2() {
        // arrange
        var inputPort1 = UUID.randomUUID();

        var outputPort1 = UUID.randomUUID();

        var vertex1 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();

        var inputPort1ToVertex1 = UUID.randomUUID();

        var vertex1ToVertex2 = UUID.randomUUID();
        var vertex2ToVertex1 = UUID.randomUUID();

        var vertex2ToOutputPort1 = UUID.randomUUID();

        var inputPorts = List.of(
            inputPort1
        );

        var outputPorts = List.of(
            outputPort1
        );

        var arrayOfVertices = new UUID[] {
            inputPort1,
            outputPort1,
            vertex1,
            vertex2,
        } ;
        var vertices = List.of(arrayOfVertices);

        var edges = Map.of(
            inputPort1ToVertex1, Map.entry(inputPort1, vertex1),

            vertex1ToVertex2, Map.entry(vertex1, vertex2),
            vertex2ToVertex1, Map.entry(vertex2, vertex1),

            vertex2ToOutputPort1, Map.entry(vertex2, outputPort1)
        );

        var portIdToSpecifiedOrder = Map.of(
            inputPort1, 0,

            outputPort1, 0
        );

        // act
        var re = new CustomLayeringOperation(
            vertices, inputPorts, outputPorts, edges, portIdToSpecifiedOrder).execute();

        // assert
        assertNotNull(re);

        assertNotNull(re.vertices);
        assertArrayEquals(arrayOfVertices, re.vertices);

        var expectedVertexIndexToLayer = new int[] {
            0, 3,
            1, 2
        };
        assertNotNull(re.vertexIndexToLayer);
        assertArrayEquals(expectedVertexIndexToLayer, re.vertexIndexToLayer);

        var expectedVertexIndexToGroupId = new int[] {
            0, 0, 0, 0
        };
        assertNotNull(re.vertexIndexToGroupId);
        assertArrayEquals(expectedVertexIndexToGroupId, re.vertexIndexToGroupId);
    }

    @Test
    void testExecute_loop_only() {
        // arrange
        var inputPort1 = UUID.randomUUID();

        var outputPort1 = UUID.randomUUID();

        var vertex1 = UUID.randomUUID();

        var vertex1ToVertex1 = UUID.randomUUID();

        var inputPorts = List.of(
            inputPort1
        );

        var outputPorts = List.of(
            outputPort1
        );

        var arrayOfVertices = new UUID[] {
            inputPort1,
            outputPort1,
            vertex1
        } ;
        var vertices = List.of(arrayOfVertices);

        var edges = Map.of(
            vertex1ToVertex1, Map.entry(vertex1, vertex1)
        );

        var portIdToSpecifiedOrder = Map.of(
            inputPort1, 0,

            outputPort1, 0
        );

        // act
        var re = new CustomLayeringOperation(
            vertices, inputPorts, outputPorts, edges, portIdToSpecifiedOrder).execute();

        // assert
        assertNotNull(re);

        assertNotNull(re.vertices);
        assertArrayEquals(arrayOfVertices, re.vertices);

        assertArrayEquals(new int[] { 0, 2, 1 }, re.vertexIndexToLayer);

        assertNotNull(re.vertexIndexToGroupId);
        assertArrayEquals(new int[] { 0, 0, 2 }, re.vertexIndexToGroupId);
    }

    @Test
    void testExecute_fixed_ports() {
        // arrange
        var inputPort1 = UUID.randomUUID();
        var inputPort2 = UUID.randomUUID();
        var inputPort3 = UUID.randomUUID();
        var outputPort1 = UUID.randomUUID();
        var outputPort2 = UUID.randomUUID();
        var outputPort3 = UUID.randomUUID();

        var vertex11 = UUID.randomUUID();
        var vertex12 = UUID.randomUUID();
        var vertex13 = UUID.randomUUID();
        var vertex2 = UUID.randomUUID();
        var vertex3 = UUID.randomUUID();

        var dummyVertex11Vertex3 = UUID.randomUUID();
        var dummyVertex13Vertex3 = UUID.randomUUID();

        var dummyVertex2Output1 = UUID.randomUUID();
        var dummyVertex2Output3 = UUID.randomUUID();

        var arrayOfVertices = new UUID[] {
            inputPort1, inputPort2, inputPort3,

            vertex11, vertex12, vertex13,

            dummyVertex11Vertex3, vertex2, dummyVertex13Vertex3,

            dummyVertex2Output1, vertex3, dummyVertex2Output3,

            outputPort1, outputPort2, outputPort3
        };
        var vertices = List.of(arrayOfVertices);

        var inputPorts = List.of(
            inputPort1,
            inputPort2,
            inputPort3
        );

        var outputPorts = List.of(
            outputPort1,
            outputPort2,
            outputPort3
        );

        var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort2, vertex11));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort1, vertex12));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort3, vertex12));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(inputPort2, vertex13));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex11, dummyVertex11Vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex12, vertex2));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex13, dummyVertex13Vertex3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex11Vertex3, vertex3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, dummyVertex2Output1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(vertex2, dummyVertex2Output3));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex13Vertex3, vertex3));

        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex2Output1, outputPort1));
        edgeIdToNodeIds.put(UUID.randomUUID(), Map.entry(dummyVertex2Output3, outputPort3));

        // act
        var re = new CustomLayeringOperation(
            vertices, inputPorts, outputPorts, edgeIdToNodeIds, Collections.emptyMap()).execute();

        // assert
        assertNotNull(re);
    }
}
