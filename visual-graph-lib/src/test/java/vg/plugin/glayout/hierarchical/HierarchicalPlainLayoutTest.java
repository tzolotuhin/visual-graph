package vg.plugin.glayout.hierarchical;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.junit.jupiter.api.Test;
import vg.lib.layout.hierarchical.HierarchicalLayoutSettings;

import java.awt.*;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

class HierarchicalPlainLayoutTest {
    @Test
    void testSugiyamaAlgorithm() {
        var in1 = UUID.randomUUID();
        var in2 = UUID.randomUUID();
        var v11 = UUID.randomUUID();
        var v12 = UUID.randomUUID();
        var v2 = UUID.randomUUID();
        var v3 = UUID.randomUUID();
        var out1 = UUID.randomUUID();

        List<UUID> vertices = Lists.newArrayList();
        vertices.add(in1);
        vertices.add(in2);
        vertices.add(v11);
        vertices.add(v12);
        vertices.add(v2);
        vertices.add(v3);
        vertices.add(out1);

        List<UUID> inputPorts = Lists.newArrayList();
        inputPorts.add(in1);
        inputPorts.add(in2);

        List<UUID> outputPorts = Lists.newArrayList();
        outputPorts.add(out1);

        Map<UUID, Map.Entry<UUID, UUID>> edgeToNodes = Maps.newLinkedHashMap();
        var in1ToV11 = UUID.randomUUID();
        var in2ToV12 = UUID.randomUUID();
        edgeToNodes.put(in1ToV11, new AbstractMap.SimpleEntry<>(in1, v11));
        edgeToNodes.put(in2ToV12, new AbstractMap.SimpleEntry<>(in2, v12));
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v11, v2));
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v12, v2));
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v2, v3));
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v3, out1));

        Map<UUID, UUID> edgeToSrcPort = Maps.newLinkedHashMap();

        Map<UUID, UUID> edgeToTrgPort = Maps.newLinkedHashMap();

        Map<UUID, Integer> portIdToIndex = Maps.newLinkedHashMap();
        portIdToIndex.put(in1, 0);
        portIdToIndex.put(in2, 1);
        portIdToIndex.put(out1, 0);

        Map<UUID, Point> portIdToPos = Maps.newLinkedHashMap();

        Map<UUID, Point> portIdToSize = Maps.newLinkedHashMap();

        Map<UUID, String> vertexIdToName = Maps.newLinkedHashMap();
        vertexIdToName.put(in1, "in1");
        vertexIdToName.put(in2, "in2");
        vertexIdToName.put(v11, "v11");
        vertexIdToName.put(v12, "v12");
        vertexIdToName.put(v2, "v2");
        vertexIdToName.put(v3, "v3");
        vertexIdToName.put(out1, "out1");

        Map<UUID, Point> vertexIdToFragmentTextSize = Maps.newLinkedHashMap();

        Map<UUID, Point> vertexIdToSize = Maps.newLinkedHashMap();
        vertexIdToSize.put(in1, new Point(20, 10));
        vertexIdToSize.put(in2, new Point(20, 10));
        vertexIdToSize.put(out1, new Point(20, 10));
        vertexIdToSize.put(v11, new Point(40, 20));
        vertexIdToSize.put(v12, new Point(40, 20));
        vertexIdToSize.put(v2, new Point(200, 200));
        vertexIdToSize.put(v3, new Point(10, 10));

        Point fakeVertexSize = new Point(10, 10);
        int crossingReductionAlgorithm = HierarchicalLayoutSettings.GENERAL_CROSSING_REDUCTION_ALGORITHM;
        int alignment = HierarchicalLayoutSettings.LEGACY_ALIGNMENT_ALGORITHM;

//        var sugiyamaAlgorithm = new HierarchicalPlainLayout(
//                vertices,
//                inputPorts,
//                outputPorts,
//                edgeToNodes,
//                edgeToSrcPort,
//                edgeToTrgPort,
//                portIdToIndex,
//                portIdToPos,
//                portIdToSize,
//                vertexIdToName,
//                vertexIdToSize,
//                vertexIdToFragmentTextSize,
//                new HierarchicalLayoutSettings());
//
//        var output = sugiyamaAlgorithm.execute();
//
//        assertNotNull(output);

//        assertEquals(200, output.getGraphSize().x);
//        assertEquals(250, output.getGraphSize().y);
//
//        var outVertices = output.getVertices();
//        assertEquals(7, outVertices.size());
//        assertEquals(80, outVertices.get(in1).getPosX());
//        assertEquals(100, outVertices.get(in2).getPosX());
//        assertEquals(60, outVertices.get(v11).getPosX());
//        assertEquals(100, outVertices.get(v12).getPosX());
//        assertEquals(0, outVertices.get(v2).getPosX());
//        assertEquals(95, outVertices.get(v3).getPosX());
//        assertEquals(90, outVertices.get(out1).getPosX());
    }

    @Test
    void testSugiyamaAlgorithm_childWithMultipleParents() {
        var v11 = UUID.randomUUID();
        var v12 = UUID.randomUUID();
        var v2 = UUID.randomUUID();
        var v3 = UUID.randomUUID();

        List<UUID> vertices = Lists.newArrayList();
        vertices.add(v11);
        vertices.add(v12);
        vertices.add(v2);
        vertices.add(v3);

        List<UUID> inputPorts = Lists.newArrayList();

        List<UUID> outputPorts = Lists.newArrayList();

        Map<UUID, Map.Entry<UUID, UUID>> edgeToNodes = Maps.newLinkedHashMap();
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v11, v2));
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v12, v3));
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v2, v3));

        Map<UUID, UUID> edgeToSrcPort = Maps.newLinkedHashMap();

        Map<UUID, UUID> edgeToTrgPort = Maps.newLinkedHashMap();

        Map<UUID, Integer> portIdToIndex = Maps.newLinkedHashMap();

        Map<UUID, Point> portIdToPos = Maps.newLinkedHashMap();

        Map<UUID, Point> portIdToSize = Maps.newLinkedHashMap();

        Map<UUID, String> vertexIdToName = Maps.newLinkedHashMap();
        vertexIdToName.put(v11, "v11");
        vertexIdToName.put(v12, "v12");
        vertexIdToName.put(v2, "v2");
        vertexIdToName.put(v3, "v3");

        Map<UUID, Point> vertexIdToFragmentTextSize = Maps.newLinkedHashMap();

        Map<UUID, Point> vertexIdToSize = Maps.newLinkedHashMap();
        vertexIdToSize.put(v11, new Point(40, 20));
        vertexIdToSize.put(v12, new Point(40, 20));
        vertexIdToSize.put(v2, new Point(40, 20));
        vertexIdToSize.put(v3, new Point(10, 10));

        Point fakeVertexSize = new Point(10, 10);
        int crossingReductionAlgorithm = HierarchicalLayoutSettings.GENERAL_CROSSING_REDUCTION_ALGORITHM;
        int alignment = HierarchicalLayoutSettings.LEGACY_ALIGNMENT_ALGORITHM;

//        var sugiyamaAlgorithm = new HierarchicalPlainLayout(
//                vertices,
//                inputPorts,
//                outputPorts,
//                edgeToNodes,
//                edgeToSrcPort,
//                edgeToTrgPort,
//                portIdToIndex,
//                portIdToPos,
//                portIdToSize,
//                vertexIdToName,
//                vertexIdToSize,
//                vertexIdToFragmentTextSize,
//                new HierarchicalLayoutSettings());

//        assertNotNull(sugiyamaAlgorithm.execute());

//        assertEquals(80, output.getGraphSize().x);
//        assertEquals(50, output.getGraphSize().y);
//
//        var outVertices = output.getVertices();
//        assertEquals(4, outVertices.size());
//        assertEquals(0, outVertices.get(v11).getPosX());
//        assertEquals(40, outVertices.get(v12).getPosX());
//        assertEquals(0, outVertices.get(v2).getPosX());
//        assertEquals(15, outVertices.get(v3).getPosX());
    }

    @Test
    void testSugiyamaAlgorithm_onlyPorts() {
        var in1 = UUID.randomUUID();
        var in2 = UUID.randomUUID();
        var out1 = UUID.randomUUID();

        List<UUID> vertices = Lists.newArrayList();
        vertices.add(in1);
        vertices.add(in2);
        vertices.add(out1);

        List<UUID> inputPorts = Lists.newArrayList();
        inputPorts.add(in1);
        inputPorts.add(in2);

        List<UUID> outputPorts = Lists.newArrayList();
        outputPorts.add(out1);

        Map<UUID, Map.Entry<UUID, UUID>> edgeToNodes = Maps.newLinkedHashMap();

        Map<UUID, UUID> edgeToSrcPort = Maps.newLinkedHashMap();

        Map<UUID, UUID> edgeToTrgPort = Maps.newLinkedHashMap();

        Map<UUID, Integer> portIdToIndex = Maps.newLinkedHashMap();
        portIdToIndex.put(in1, 0);
        portIdToIndex.put(in2, 1);
        portIdToIndex.put(out1, 0);

        Map<UUID, Point> portIdToPos = Maps.newLinkedHashMap();

        Map<UUID, Point> portIdToSize = Maps.newLinkedHashMap();

        Map<UUID, String> vertexIdToName = Maps.newLinkedHashMap();
        vertexIdToName.put(in1, "in1");
        vertexIdToName.put(in2, "in2");
        vertexIdToName.put(out1, "out1");

        Map<UUID, Point> vertexIdToFragmentTextSize = Maps.newLinkedHashMap();

        Map<UUID, Point> vertexIdToSize = Maps.newLinkedHashMap();
        vertexIdToSize.put(in1, new Point(20, 10));
        vertexIdToSize.put(in2, new Point(20, 10));
        vertexIdToSize.put(out1, new Point(60, 10));

        Point fakeVertexSize = new Point(10, 10);
        int crossingReductionAlgorithm = HierarchicalLayoutSettings.GENERAL_CROSSING_REDUCTION_ALGORITHM;
        int alignment = HierarchicalLayoutSettings.LEGACY_ALIGNMENT_ALGORITHM;

//        var sugiyamaAlgorithm = new HierarchicalPlainLayout(
//                vertices,
//                inputPorts,
//                outputPorts,
//                edgeToNodes,
//                edgeToSrcPort,
//                edgeToTrgPort,
//                portIdToIndex,
//                portIdToPos,
//                portIdToSize,
//                vertexIdToName,
//                vertexIdToSize,
//                vertexIdToFragmentTextSize,
//                new HierarchicalLayoutSettings());
//
//        assertNotNull(sugiyamaAlgorithm.execute());

//        assertEquals(60, output.getGraphSize().x);
//        assertEquals(20, output.getGraphSize().y);
//
//        var outVertices = output.getVertices();
//        assertEquals(3, outVertices.size());

//        assertEquals(10, outVertices.get(in1).getPosX());
//        assertEquals(0, outVertices.get(in1).getPosY());
//
//        assertEquals(30, outVertices.get(in2).getPosX());
//        assertEquals(0, outVertices.get(in2).getPosY());
//
//        assertEquals(0, outVertices.get(out1).getPosX());
//        assertEquals(10, outVertices.get(out1).getPosY());
    }

    @Test
    void testSugiyamaAlgorithm_backwardEdge() {
        var v1 = UUID.randomUUID();
        var v2 = UUID.randomUUID();
        var v3 = UUID.randomUUID();

        List<UUID> vertices = Lists.newArrayList();
        vertices.add(v1);
        vertices.add(v2);
        vertices.add(v3);

        List<UUID> inputPorts = Lists.newArrayList();

        List<UUID> outputPorts = Lists.newArrayList();

        Map<UUID, Map.Entry<UUID, UUID>> edgeToNodes = Maps.newLinkedHashMap();
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v1, v2));
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v2, v3));
        edgeToNodes.put(UUID.randomUUID(), new AbstractMap.SimpleEntry<>(v1, v3));

        var backwardEdgeId = UUID.randomUUID();
        edgeToNodes.put(backwardEdgeId, new AbstractMap.SimpleEntry<>(v3, v1));

        Map<UUID, UUID> edgeToSrcPort = Maps.newLinkedHashMap();

        Map<UUID, UUID> edgeToTrgPort = Maps.newLinkedHashMap();

        Map<UUID, Integer> portIdToIndex = Maps.newLinkedHashMap();

        Map<UUID, Point> portIdToPos = Maps.newLinkedHashMap();

        Map<UUID, Point> portIdToSize = Maps.newLinkedHashMap();

        Map<UUID, String> vertexIdToName = Maps.newLinkedHashMap();
        vertexIdToName.put(v1, "v1");
        vertexIdToName.put(v2, "v2");
        vertexIdToName.put(v3, "v3");

        Map<UUID, Point> vertexIdToFragmentTextSize = Maps.newLinkedHashMap();

        Map<UUID, Point> vertexIdToSize = Maps.newLinkedHashMap();
        vertexIdToSize.put(v1, new Point(400, 100));
        vertexIdToSize.put(v2, new Point(200, 100));
        vertexIdToSize.put(v3, new Point(200, 100));

        Point fakeVertexSize = new Point(10, 10);
        int crossingReductionAlgorithm = HierarchicalLayoutSettings.GENERAL_CROSSING_REDUCTION_ALGORITHM;
        int alignment = HierarchicalLayoutSettings.LEGACY_ALIGNMENT_ALGORITHM;

//        var sugiyamaAlgorithm = new HierarchicalPlainLayout(
//                vertices,
//                inputPorts,
//                outputPorts,
//                edgeToNodes,
//                edgeToSrcPort,
//                edgeToTrgPort,
//                portIdToIndex,
//                portIdToPos,
//                portIdToSize,
//                vertexIdToName,
//                vertexIdToSize,
//                vertexIdToFragmentTextSize,
//                new HierarchicalLayoutSettings());
//
//        assertNotNull(sugiyamaAlgorithm.execute());

//        assertEquals(400, output.getGraphSize().x);
//        assertEquals(300, output.getGraphSize().y);
//
//        var outVertices = output.getVertices();
//        assertNotNull(outVertices);
//        assertEquals(3, outVertices.size());
//        assertEquals(0, outVertices.get(v1).getPosX());
//        assertEquals(100, outVertices.get(v2).getPosX());
//        assertEquals(100, outVertices.get(v3).getPosX());

//        var outEdges = output.getEdges();
//        assertNotNull(outEdges);
//        assertEquals(4, outEdges.size());
//
//        var outBackwardEdge = outEdges.get(backwardEdgeId);
//        assertTrue(outBackwardEdge.isBackward());
    }
}
