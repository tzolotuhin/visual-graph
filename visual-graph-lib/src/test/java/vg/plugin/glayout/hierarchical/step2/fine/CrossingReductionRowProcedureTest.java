package vg.plugin.glayout.hierarchical.step2.fine;

import org.junit.jupiter.api.Test;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.step2.legacy.CalcCrossesOperation;
import vg.lib.layout.hierarchical.step2.legacy.EstCrossesOperation;
import vg.lib.layout.hierarchical.step2.legacy.data.CrossingReductionTable;
import vg.lib.layout.hierarchical.step2.legacy.data.Edge;
import vg.plugin.glayout.hierarchical.data.HierarchicalGraphTest;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CrossingReductionRowProcedureTest {
    @Test
    void execute_simpleBackwardGraph_example1() {
        var graph = HierarchicalGraphTest.generateSimpleBackwardGraph();

        graph.applyOrders(0, Collections.singletonList("in"));
        graph.applyOrders(1, Arrays.asList("TC: v2 [v1]", "TB: v2", "in <-> v1 -- F"));
        graph.applyOrders(2, Arrays.asList("TC: v2 [v1] <-> SC: v1 [v2] -- F", "v2", "in <-> v1 -- F"));
        graph.applyOrders(3, Arrays.asList("TC: v2 [v1] <-> SC: v1 [v2] -- F", "v2 <-> v3 -- F", "v2 <-> v1 -- F", "in <-> v1 -- F", "TC: v1 [v3]"));
        graph.applyOrders(4, Arrays.asList("TC: v2 [v1] <-> SC: v1 [v2] -- F", "v2 <-> v3 -- F", "v1", "TC: v1 [v3] <-> SC: v3 [v1] -- F"));
        graph.applyOrders(5, Arrays.asList("v2 <-> v3 -- F", "SC: v1 [v2]", "v1 <-> v3 -- F", "v1 <-> out -- F", "TC: v1 [v3] <-> SC: v3 [v1] -- F"));
        graph.applyOrders(6, Arrays.asList("v3", "v1 <-> out -- F", "TC: v1 [v3] <-> SC: v3 [v1] -- F"));
        graph.applyOrders(7, Arrays.asList("BB: v3", "v1 <-> out -- F", "SC: v3 [v1]"));
        graph.applyOrders(8, Collections.singletonList("out"));

        var table = new CrossingReductionTable(graph.getDefaultDetailedGroup(), graph);

        {
            var row = table.rows[5];
            long re = new EstCrossesOperation(row, Direction.BOTTOM).execute();
            assertEquals(4, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(2 * Edge.TOP_MAIN_BACK_FAKE_CROSS_W, re);

            var crosses = new CalcCrossesOperation(table.edges[4], Direction.ALL).execute();
            assertEquals(Edge.DIFF_LEVEL_GROUP_CROSS_W + Edge.ONE_FAKE_CROSS_W, crosses.w);
            assertEquals(1, crosses.amount);
        }
    }

    @Test
    void execute_simpleBackwardGraph_example2() {
        var graph = HierarchicalGraphTest.generateSimpleBackwardGraph();

        graph.applyOrders(0, Collections.singletonList("in"));
        graph.applyOrders(1, Arrays.asList("TC: v2 [v1]", "TB: v2", "in <-> v1 -- F"));
        graph.applyOrders(2, Arrays.asList("TC: v2 [v1] <-> SC: v1 [v2] -- F", "v2", "in <-> v1 -- F"));
        graph.applyOrders(3, Arrays.asList("TC: v2 [v1] <-> SC: v1 [v2] -- F", "v2 <-> v3 -- F", "v2 <-> v1 -- F", "in <-> v1 -- F", "TC: v1 [v3]"));
        graph.applyOrders(4, Arrays.asList("TC: v2 [v1] <-> SC: v1 [v2] -- F", "v2 <-> v3 -- F", "v1", "TC: v1 [v3] <-> SC: v3 [v1] -- F"));
        graph.applyOrders(5, Arrays.asList("SC: v1 [v2]", "v2 <-> v3 -- F", "v1 <-> v3 -- F", "v1 <-> out -- F", "TC: v1 [v3] <-> SC: v3 [v1] -- F"));
        graph.applyOrders(6, Arrays.asList("v3", "v1 <-> out -- F", "TC: v1 [v3] <-> SC: v3 [v1] -- F"));
        graph.applyOrders(7, Arrays.asList("BB: v3", "v1 <-> out -- F", "SC: v3 [v1]"));
        graph.applyOrders(8, Collections.singletonList("out"));

        var table = new CrossingReductionTable(graph.getDefaultDetailedGroup(), graph);

        {
            var row = table.rows[5];
            long re = new EstCrossesOperation(row, Direction.ALL).execute();
            assertEquals(4, row.topGroups.length);
            assertEquals(2, row.bottomGroups.length);
            assertEquals(3 * Edge.MAIN_BACK_FAKE_CROSS_W, re);

            var crosses = new CalcCrossesOperation(table.edges[4], Direction.ALL).execute();
            assertEquals(Edge.DIFF_LEVEL_GROUP_CROSS_W + Edge.BOTTOM_BACK_FAKE_CROSS_W, crosses.w);
            assertEquals(1, crosses.amount);
        }
    }
}