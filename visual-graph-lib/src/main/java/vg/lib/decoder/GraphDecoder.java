package vg.lib.decoder;

import java.io.InputStream;
import org.apache.commons.lang3.StringUtils;
import vg.lib.progress.Progress;
import vg.lib.progress.ProgressImpl;
import vg.lib.storage.GraphStorage;

/**
 * Interface for all graph decoders.
 *
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public abstract class GraphDecoder {
    protected InputStream inputStream;
    protected String graphName;
    protected GraphStorage graphStorage;

    protected final ProgressImpl progress;

    public GraphDecoder(InputStream inputStream,
                        String graphName,
                        GraphStorage graphStorage) {
        this.inputStream = inputStream;
        this.graphName = graphName;
        this.graphStorage = graphStorage;

        progress = new ProgressImpl("Decode " + graphStorage);
    }

    /**
     * Returns progress of decoding.
     */
    public Progress getProgress() {
        return progress;
    }

    /**
     * Returns root graph id.
     */
    public abstract int decode() throws GraphDecoderException;

    public static class GraphDecoderException extends Exception {
        public GraphDecoderException(String message) {
            super(message);
        }

        public GraphDecoderException(String message, Exception exception) {
            super(message, exception);
        }

        public GraphDecoderException(int lineNumber, String token, String expectedToken) {
            super("Can't parse following token: '" + token + "', expected token: '" + expectedToken + "', line number: " + lineNumber + ".");
        }

        public GraphDecoderException(Exception exception) {
            super(
                String.format("Can't decode input content. Original error: '%s'.", exception.getMessage()),
                exception);
        }
    }

    protected String readID(String token) {
        if (StringUtils.isBlank(token)) {
            return token;
        }

        if (token.startsWith("\"") && token.endsWith("\"")) {
            token = token.substring(1).substring(0, token.length() - 2);
        }

        if (token.startsWith("'") && token.endsWith("'")) {
            token = token.substring(1).substring(0, token.length() - 2);
        }

        return token;
    }
}
