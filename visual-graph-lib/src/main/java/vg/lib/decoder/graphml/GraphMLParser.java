package vg.lib.decoder.graphml;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.text.StringEscapeUtils;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import vg.lib.model.record.AttributeRecordType;
import vg.lib.model.record.VertexRecord;
import vg.lib.storage.GraphStorage;

@Slf4j
class GraphMLParser extends DefaultHandler {
    // Constants
    private static final String ID = "id";
    private static final String GRAPH = "graph";
    private static final String EDGEDEF = "edgedefault";
    private static final String DIRECTED = "directed";
    private static final String UNDIRECTED = "undirected";
    private static final String PORT = "port";
    private static final String NAME = "name";
    //private static final String HYPEREDGE = "hyperedge";
    //private static final String ENDPOINT = "endpoint";
    private static final String KEY = "key";
    private static final String FOR = "for";
    private static final String ALL = "all";
    private static final String ATTRNAME = "attr.name";
    private static final String ATTRTYPE = "attr.type";
    private static final String DEFAULT = "default";
    private static final String NODE = "node";
    private static final String EDGE = "edge";
    private static final String SOURCE = "source";
    private static final String SOURCE_PORT = "sourceport";
    private static final String TARGET = "target";
    private static final String TARGET_PORT = "targetport";
    private static final String DATA = "data";
    private static final String GRAPHML = "graphml";

    private static final String INT = "int";
    private static final String INTEGER = "integer";
    private static final String LONG = "long";
    private static final String FLOAT = "float";
    private static final String DOUBLE = "double";
    private static final String REAL = "real";
    private static final String BOOLEAN = "boolean";
    private static final String STRING = "string";

    // Main data
    private final GraphStorage graphStorage;

    private final int graphModelId;

    // schema parsing
    private String m_key;
    private String m_id;
    private String m_for;
    private String m_name;
    private String m_type;
    private String m_default;

    private final StringBuffer stringBuffer = new StringBuffer();

    private final List<GraphMlAttribute> vertexData = Lists.newArrayList();
    private final List<GraphMlAttribute> edgeData = Lists.newArrayList();
    private final List<GraphMlAttribute> graphData = Lists.newArrayList();

    private GraphMlNode currNode, rootNode;

    GraphMLParser(int graphModelId, GraphStorage graphStorage) {
        this.graphModelId = graphModelId;
        this.graphStorage = graphStorage;
    }

    @Override
    public void startDocument() {
        rootNode = currNode = new GraphMlNode();
    }

    @Override
    public void endDocument() {
        // if we don't reach the end of document, delete graph from model
    }

    @Override
    public void startElement(String namespaceURI, String localName, String qName, Attributes attributes) {
        // first clear the character buffer.
        stringBuffer.setLength(0);

        switch (qName) {
            case GRAPH: {
                String edgeDef = attributes.getValue(EDGEDEF);
                if (edgeDef != null && !edgeDef.equalsIgnoreCase(DIRECTED) && !edgeDef.equalsIgnoreCase(UNDIRECTED)) {
                    throw new RuntimeException(String.format("Unknown token '%s'.", attributes));
                }

                boolean directed = StringUtils.equalsIgnoreCase(DIRECTED, edgeDef) | edgeDef == null;

                // graph id is optional parameter
                String graphMlId = attributes.getValue(ID);

                if (StringUtils.isBlank(graphMlId)) {
                    graphMlId = "";
                }

                switch (currNode.getCurrentSection()) {
                    case GraphMlNode.GRAPH_ML_SECTION:
                        currNode = currNode.addChild(
                            graphStorage.createExtendedVertex(graphModelId, VertexRecord.NO_PARENT_ID),
                            graphMlId,
                            directed);

                        currNode.beginGraphSection();

                        break;
                    case GraphMlNode.NODE_SECTION:
                        currNode.beginGraphSection();
                        currNode.setDirected(directed);

                        break;
                    default:
                        throw new IllegalArgumentException("Section 'graph' places in wrong section. Please check your file.");
                }

                graphStorage.setInnerGraphIdAttribute(currNode.getId(), graphMlId);

                currNode.mergeAttributes(graphData);

                break;
            }

            case KEY: {
                Validate.isTrue(rootNode == currNode, "Section 'key' should place in 'graphml' section. Please check your file.");

                m_for = attributes.getValue(FOR);
                m_id = attributes.getValue(ID);
                m_name = attributes.getValue(ATTRNAME);
                m_type = attributes.getValue(ATTRTYPE);

                break;
            }

            case NODE: {
                String graphMlNodeId = attributes.getValue(ID);

                Validate.notEmpty(graphMlNodeId, "Node is empty. Please check your file.");

                int vertexId = graphStorage.createExtendedVertex(graphModelId, currNode.getId());

                currNode = currNode.addChild(vertexId, graphMlNodeId, currNode.isDirected());
                currNode.beginNodeSection();

                graphStorage.setVertexIdAttribute(currNode.getId(), currNode.getGraphMlId());

                currNode.mergeAttributes(vertexData);

                break;
            }

            case PORT: {
                String graphMLPortId = attributes.getValue(NAME);
                if (graphMLPortId == null) {
                    graphMLPortId = attributes.getValue(ID);
                }

                Validate.notEmpty(graphMLPortId, "Parameter 'name' or 'id' is required for port element.");

                int portId = graphStorage.createPort(graphModelId, currNode.getId(), false, currNode.getPorts().size());
                graphStorage.setVertexIdAttribute(portId, graphMLPortId);

                currNode.addPort(graphMLPortId, portId);

                // additionally insert the port's attributes.
                for (int i = 0; i < attributes.getLength(); i++) {
                    var value = attributes.getValue(i);
                    var name = attributes.getQName(i);
                    if (value == null || name == null || name.equalsIgnoreCase(NAME) || name.equalsIgnoreCase(ID)) {
                        continue;
                    }

                    graphStorage.createVertexAttribute(portId, name, value, AttributeRecordType.STRING, true);
                }

                break;
            }

            case EDGE: {
                String graphMLEdgeId = attributes.getValue(ID);

                // edge id is optional parameter
                if (StringUtils.isBlank(graphMLEdgeId))
                    graphMLEdgeId = "";

                String srcGraphMLId = attributes.getValue(SOURCE);
                String trgGraphMLId = attributes.getValue(TARGET);
                String srcPortGraphMLId = attributes.getValue(SOURCE_PORT);
                String trgPortGraphMLId = attributes.getValue(TARGET_PORT);

                log.debug(
                    "Parse edge with id '{}', src id '{}', trg id '{}', src port id : '{}', trg port id '{}'.",
                    graphMLEdgeId,
                    srcGraphMLId,
                    trgGraphMLId,
                    srcPortGraphMLId,
                    trgPortGraphMLId);

                GraphMlNode srcGraphMlVertex = rootNode.getNodeByGraphMlId(srcGraphMLId, currNode);
                GraphMlNode trgGraphMlVertex = rootNode.getNodeByGraphMlId(trgGraphMLId, currNode);

                Validate.isTrue(srcGraphMlVertex != null && trgGraphMlVertex != null,
                        String.format(
                                "Can't find source or target node. \n" +
                                        "Edge id '%s', source id '%s', target id '%s'. \n" +
                                        "Please check your file.",
                                graphMLEdgeId,
                                srcGraphMLId,
                                trgGraphMLId));

                Integer srcId = srcGraphMlVertex.getId();
                Integer trgId = trgGraphMlVertex.getId();
                Integer srcPortId = null, trgPortId = null;
                if (srcPortGraphMLId != null) {
                    srcPortId = srcGraphMlVertex.getPorts().get(srcPortGraphMLId);
                }
                if (trgPortGraphMLId != null) {
                    trgPortId = trgGraphMlVertex.getPorts().get(trgPortGraphMLId);
                }

                List<Integer> edgesIds = graphStorage.createCompositeEdge(
                        graphModelId,
                        srcId,
                        trgId,
                        srcPortId == null ? -1 : srcPortId,
                        trgPortId == null ? -1 : trgPortId,
                        currNode.isDirected());

                int edgeId = edgesIds.get(0);

                currNode.beginEdgeSection(edgeId, graphMLEdgeId);

                currNode.mergeEdgeAttributes(edgeData);

                break;
            }

            case DATA: {
                m_key = attributes.getValue(KEY);
                Validate.notEmpty(m_key, "Key must not be null or empty.");
                break;
            }

            case GRAPHML: {
                currNode.beginGraphMlSection();
                break;
            }

            default: {
                // graphml format can contain unknown token - it's OK, and should be skipped.
                log.error("Unknown token '{}'.", qName);
            }
        }
    }

    @Override
    public void endElement(String namespaceURI, String localName, String qName) {
        switch (qName) {
            case DEFAULT: {
                m_default = stringBuffer.toString();
                break;
            }

            case KEY: {
                addToSchema();
                break;
            }

            case GRAPH: {
                currNode.endGraphSection();

                GraphMlNode parent = rootNode.getParent(currNode);
                if (parent == rootNode)
                    currNode = parent;

                break;
            }

            case NODE: {
                currNode.getAttributes().forEach(x -> {
                    if (x.value != null) {
                        graphStorage.createVertexAttribute(currNode.getId(), x.getNameOrId(), x.getValue(), x.getType(), true);
                    }
                });

                currNode.endNodeSection();
                currNode = rootNode.getParent(currNode);

                break;
            }

            case EDGE: {
                currNode.getEdgeAttributes().forEach(x -> {
                    if (x.getValue() != null) {
                        graphStorage.createEdgeAttribute(currNode.getEdgeId(), x.getNameOrId(), x.getValue(), x.getType(), true);
                    }
                });

                if(!StringUtils.isBlank(currNode.getEdgeGraphMlId())) {
                    graphStorage.setEdgeIdAttribute(currNode.getEdgeId(), currNode.getEdgeGraphMlId());
                }

                currNode.endEdgeSection();

                break;
            }

            case DATA: {
                GraphMlAttribute graphMLAttribute = new GraphMlAttribute(m_key, m_key, "", graphMLAttributeType2VisualGraphAttributeType(STRING));
                switch (currNode.getCurrentSection()) {
                    case GraphMlNode.GRAPH_SECTION:
                    case GraphMlNode.NODE_SECTION: {
                        var list = currNode.getAttributes().stream().filter(x -> StringUtils.equals(x.getId(), m_key)).collect(Collectors.toList());
                        if (list.size() > 0) {
                            graphMLAttribute = list.get(0);
                        }
                        graphMLAttribute.setValue(StringEscapeUtils.unescapeJava(stringBuffer.toString()));
                        currNode.mergeAttributes(Collections.singletonList(graphMLAttribute));
                        break;
                    }
                    case GraphMlNode.EDGE_SECTION: {
                        List<GraphMlAttribute> list = currNode.getEdgeAttributes().stream().filter(x -> StringUtils.equals(x.getId(), m_key)).collect(Collectors.toList());
                        if (list.size() > 0) {
                            graphMLAttribute = list.get(0);
                        }
                        graphMLAttribute.setValue(StringEscapeUtils.unescapeJava(stringBuffer.toString()));
                        currNode.mergeEdgeAttributes(Collections.singletonList(graphMLAttribute));
                        break;
                    }
                }

                break;
            }

            case GRAPHML: {
                currNode.endGraphMlSection();
                break;
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        stringBuffer.append(ch, start, length);
    }

    private void addToSchema() {
        Validate.notEmpty(m_id, "Id is empty in 'key' section.");

        var type = graphMLAttributeType2VisualGraphAttributeType(m_type);
        String strValue = m_default;

        if (m_for == null || m_for.equalsIgnoreCase(ALL)) {
            vertexData.add(new GraphMlAttribute(m_id, m_name, strValue, type));
            edgeData.add(new GraphMlAttribute(m_id, m_name, strValue, type));
            graphData.add(new GraphMlAttribute(m_id, m_name, strValue, type));
        } else if (m_for.equalsIgnoreCase(NODE) || m_for.equalsIgnoreCase(PORT)) {
            vertexData.add(new GraphMlAttribute(m_id, m_name, strValue, type));
        } else if (m_for.equalsIgnoreCase(EDGE)) {
            edgeData.add(new GraphMlAttribute(m_id, m_name, strValue, type));
        } else if (m_for.equalsIgnoreCase(GRAPH) || m_for.equalsIgnoreCase(GRAPHML)) {
            graphData.add(new GraphMlAttribute(m_id, m_name, strValue, type));
        } else {
            throw new IllegalArgumentException(String.format("Unrecognized group '%s' in 'key' section, id = '%s'.", m_for, m_id));
        }

        m_default = null;
    }

    private static AttributeRecordType graphMLAttributeType2VisualGraphAttributeType(String type) {
        if (type == null || type.equals(STRING)) {
            return AttributeRecordType.STRING;
        }

        if (type.equals(BOOLEAN)) {
            return AttributeRecordType.BOOLEAN;
        }

        if (type.equals(DOUBLE) || type.equals(FLOAT) || type.equals(REAL)) {
            return AttributeRecordType.DOUBLE;
        }

        if (type.equals(INT) || type.equals(INTEGER) || type.equals(LONG)) {
            return AttributeRecordType.INTEGER;
        }

        return AttributeRecordType.STRING;
    }

    private static class GraphMlNode {
        private static final int GRAPH_ML_SECTION = 0;
        private static final int GRAPH_SECTION = 1;
        private static final int NODE_SECTION = 2;
        private static final int EDGE_SECTION = 3;
        private static final int ATTR_SECTION = 4;

        private Stack<Integer> currentSection = new Stack<>();

        private final int id;
        private final String graphMlId;
        private boolean directed;

        private Map.Entry<Integer, String> currEdge;
        private List<GraphMlAttribute> edgeAttributes = Lists.newArrayList();

        private final Map<String, Integer> ports = Maps.newLinkedHashMap();

        private final Stack<GraphMlNode> nodes = new Stack<>();

        private final List<GraphMlAttribute> attributes = Lists.newArrayList();

        GraphMlNode() {
            this(-1, null, false);
        }

        GraphMlNode(int id, String graphMlId, boolean directed) {
            this.id = id;
            this.graphMlId = graphMlId;
            this.directed = directed;
        }

        void beginEdgeSection(int edgeId, String graphMlEdgeId) {
            Validate.notEmpty(currentSection);
            Validate.isTrue(currentSection.peek() == GRAPH_SECTION, "Tag <edges ...> should be inside in tag <graph ...>.");
            Validate.isTrue(edgeId >= 0);
            Validate.notNull(graphMlEdgeId);

            currEdge = new AbstractMap.SimpleEntry<>(edgeId, graphMlEdgeId);
            currentSection.add(EDGE_SECTION);
        }

        void endEdgeSection() {
            Validate.notEmpty(currentSection);
            Validate.isTrue(currentSection.peek() == EDGE_SECTION);
            currEdge = null;
            edgeAttributes.clear();
            currentSection.pop();
        }

        void beginGraphSection() {
            if (!currentSection.isEmpty()) {
                Validate.isTrue(currentSection.peek() == NODE_SECTION);
            }
            currentSection.add(GRAPH_SECTION);
        }

        void endGraphSection() {
            Validate.notEmpty(currentSection);
            Validate.isTrue(currentSection.peek() == GRAPH_SECTION);
            currentSection.pop();
        }

        void beginNodeSection() {
            Validate.isTrue(currentSection.isEmpty());
            currentSection.add(NODE_SECTION);
        }

        void endNodeSection() {
            Validate.notEmpty(currentSection);
            Validate.isTrue(currentSection.peek() == NODE_SECTION);
            currentSection.pop();
        }

        void beginGraphMlSection() {
            Validate.isTrue(currentSection.isEmpty());
            currentSection.add(GRAPH_ML_SECTION);
        }

        void endGraphMlSection() {
            Validate.notEmpty(currentSection);
            Validate.isTrue(currentSection.peek() == GRAPH_ML_SECTION);
            currentSection.pop();
        }

        void mergeEdgeAttributes(List<GraphMlAttribute> attributes) {
            Validate.notEmpty(currentSection);
            Validate.isTrue(currentSection.peek() == EDGE_SECTION);

            attributes.forEach(x -> {
                List<GraphMlAttribute> attrs = this.edgeAttributes.stream().filter(y -> StringUtils.equals(x.getId(), y.getId())).collect(Collectors.toList());
                Validate.isTrue(attrs.size() <= 1);

                if (attrs.size() == 1) {
                    attrs.get(0).setValue(x.getValue());
                } else {
                    this.edgeAttributes.add(new GraphMlAttribute(x.getId(), x.getName(), x.getValue(), x.getType()));
                }
            });
        }

        List<GraphMlAttribute> getEdgeAttributes() {
            return edgeAttributes;
        }

        void mergeAttributes(List<GraphMlAttribute> attributes) {
            attributes.forEach(x -> {
                List<GraphMlAttribute> attrs = this.attributes.stream().filter(y -> StringUtils.equals(x.getId(), y.getId())).collect(Collectors.toList());
                Validate.isTrue(attrs.size() <= 1);

                if (attrs.size() == 1) {
                    attrs.get(0).setValue(x.getValue());
                } else {
                    this.attributes.add(new GraphMlAttribute(x.getId(), x.getName(), x.getValue(), x.getType()));
                }
            });
        }

        List<GraphMlAttribute> getAttributes() {
            return attributes;
        }

        int getCurrentSection () {
            return currentSection.peek();
        }

        GraphMlNode addChild(int id, String graphMLId, boolean directed) {
            GraphMlNode node = new GraphMlNode(id, graphMLId, directed);
            nodes.add(node);
            return node;
        }

        GraphMlNode getParent(GraphMlNode child) {
            if (nodes.contains(child)) {
                return this;
            }

            for (GraphMlNode node : nodes) {
                GraphMlNode parent = node.getParent(child);
                if (parent != null)
                    return parent;
            }

            return null;
        }

        GraphMlNode getLastNode() {
            if (!nodes.isEmpty()) {
                return nodes.peek();
            }
            return null;
        }

        GraphMlNode getNodeByGraphMlId(String graphMlId, GraphMlNode currNode) {
            List<GraphMlNode> visited = Lists.newArrayList();

            // first find node in children of currNode
            if (currNode != null) {
                for (GraphMlNode node : currNode.getNodes()) {
                    if (StringUtils.equals(node.getGraphMlId(), graphMlId)) {
                        return node;
                    }
                }
            }

            // otherwise find anywhere
            while (currNode != null) {
                Queue<GraphMlNode> queue = Queues.newArrayDeque();
                queue.add(currNode);
                while (!queue.isEmpty()) {
                    GraphMlNode node = queue.poll();

                    if (StringUtils.equals(node.getGraphMlId(), graphMlId)) {
                        return node;
                    }

                    visited.add(node);
                    node.getNodes().forEach(x -> {
                        if (!visited.contains(x)) {
                            queue.add(x);
                        }
                    });
                }

                currNode = getParent(currNode);
            }

            return null;
        }

        int getEdgeId() {
            Validate.notNull(currEdge);

            return currEdge.getKey();
        }

        String getEdgeGraphMlId() {
            Validate.notNull(currEdge);

            return currEdge.getValue();
        }

        String getGraphMlId() {
            return graphMlId;
        }

        int getId() {
            return id;
        }

        Stack<GraphMlNode> getNodes() {
            return nodes;
        }

        boolean isDirected() {
            return directed;
        }

        void setDirected(boolean directed) {
            this.directed = directed;
        }

        void addPort(String graphMLId, int portId) {
            ports.put(graphMLId, portId);
        }

        Map<String, Integer> getPorts() {
            return ports;
        }
    }

    private static class GraphMlAttribute {
        @Getter
        private final String id;
        @Getter
        private final String name;
        @Getter @Setter
        private String value;
        @Getter
        private final AttributeRecordType type;

        GraphMlAttribute(String id, String name, String value, AttributeRecordType type) {
            Validate.notEmpty(id);

            this.id = id;
            this.name = name;
            this.type = type;
            this.value = value;
        }

        String getNameOrId() {
            if (StringUtils.isBlank(name)) {
                return id;
            }
            return name;
        }
    }
}
