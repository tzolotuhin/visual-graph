package vg.lib.decoder.graphml;

import java.io.InputStream;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXParseException;
import vg.lib.decoder.GraphDecoder;
import vg.lib.storage.GraphStorage;

/**
 * GraphML decoder.
 * <p>
 * GraphML specification: <a href="http://graphml.graphdrawing.org/specification/schema_element.xsd.htm">...</a>
 * Examples: <a href="http://graphml.graphdrawing.org/primer/graphml-primer.html">...</a>
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class GraphMLDecoder extends GraphDecoder {
    public GraphMLDecoder(InputStream inputStream,
                          String graphName,
                          GraphStorage graphStorage) {
        super(inputStream, graphName, graphStorage);
    }

    @Override
    public int decode() throws GraphDecoderException {
        try {
            var saxParser = SAXParserFactory.newInstance().newSAXParser();

            var graphId = graphStorage.createGraphModel(graphName);

            var handler = new GraphMLParser(graphId, graphStorage);

            saxParser.parse(inputStream, handler);

            return graphId;
        } catch (SAXParseException ex) {
            // original message of SAXParseException class doesn't contain column number and row number.
            throw new GraphDecoderException(String.format(
                "%s Line number: %s, column number:%s.",
                ex.getMessage(),
                ex.getLineNumber(),
                ex.getColumnNumber()), ex);
        } catch (Exception ex) {
            throw new GraphDecoderException(ex);
        }
    }
}
