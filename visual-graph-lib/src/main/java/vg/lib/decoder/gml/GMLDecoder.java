package vg.lib.decoder.gml;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import vg.lib.decoder.GraphDecoder;
import vg.lib.model.record.AttributeRecordType;
import vg.lib.model.record.VertexRecord;
import vg.lib.progress.Progress;
import vg.lib.storage.GraphStorage;

@Slf4j
public class GMLDecoder extends GraphDecoder {
    // Constants
    private static final String GRAPH = "graph";
    private static final String NODE = "node";
    private static final String EDGE = "edge";
    private static final String SOURCE = "source";
    private static final String TARGET = "target";

    private static final String BEGIN_STMT = "[";
    private static final String END_STMT = "]";

    private static final String ID = "id";

    // Main data
    private GMLTokenizer currGMLTokenizer;
    private Map<String, Integer> nodes;

    public GMLDecoder(InputStream inputStream,
                      String graphName,
                      GraphStorage graphStorage) {
        super(inputStream, graphName, graphStorage);
    }

    @Override
    public Progress getProgress() {
        return progress;
    }

    @Override
    public int decode() throws GraphDecoderException {
        try {
            currGMLTokenizer = new GMLTokenizer(inputStream);

            int graphModelId = graphStorage.createGraphModel(graphName);
            int graphId = graphStorage.createExtendedVertex(graphModelId, VertexRecord.NO_PARENT_ID);

            nodes = Maps.newHashMap();

            while (currGMLTokenizer.peekToken() != null) {
                if (isNextAttr())
                    parseAttr();
                else if (isNextGraph())
                    parseGraph(graphModelId, graphId);
                else {
                    throw new GMLDecoderException(currGMLTokenizer.getCurrentLineNumber(), currGMLTokenizer.peekToken(), GRAPH + "|ID ID");
                }
            }

            return graphModelId;
        } catch (IOException ex) {
            throw new GraphDecoderException(ex);
        }
    }

    private void parseGraph(int graphModelId, int graphId) throws IOException {
        if (!isNextGraph()) {
            throw new GMLDecoderException(currGMLTokenizer.getCurrentLineNumber(), currGMLTokenizer.peekToken(), GRAPH);
        }
        currGMLTokenizer.nextToken();
        currGMLTokenizer.nextToken();

        parseStmtList(graphModelId, graphId);

        if (currGMLTokenizer.peekToken() == null || !currGMLTokenizer.peekToken().equalsIgnoreCase(END_STMT))
            throw new GMLDecoderException(currGMLTokenizer.getCurrentLineNumber(), currGMLTokenizer.peekToken(), END_STMT);
        currGMLTokenizer.nextToken();
    }

    private void parseStmtList(int graphModelId, int graphId) throws IOException {
        while (isNextStmt()) {
            parseStmt(graphModelId, graphId);
        }
    }

    private void parseStmt(int graphModelId, int graphId) throws IOException {
        if (!isNextStmt())
            return;

        if (isNextNode()) {
            parseNode(graphModelId, graphId);
        } else if (isNextEdge()) {
            parseEdge(graphModelId);
        } else if (isNextAttr()) {
            AbstractMap.SimpleEntry<String, String> pair = parseAttr();
            //MainService.graphDataBaseService.createGraphAttributeHeader(graphId, pair.getKey(), pair.getValue(), AttributeRecord.STRING_ATTRIBUTE_TYPE);
        }
    }

    private void parseNode(int graphModelId, int graphId) throws IOException {
        currGMLTokenizer.nextToken();
        currGMLTokenizer.nextToken();

        int vertexId = graphStorage.createExtendedVertex(graphModelId, graphId);
        while (isNextAttr()) {
            AbstractMap.SimpleEntry<String, String> pair = parseAttr();
            if (pair.getKey().equalsIgnoreCase(ID)) {
                if (nodes.containsKey(pair.getValue())) {
                    throw new GMLDecoderException(currGMLTokenizer.getCurrentLineNumber(), "The graph contains nodes with same id: " + pair.getValue());
                }
                nodes.put(pair.getValue(), vertexId);
                graphStorage.setVertexIdAttribute(vertexId, pair.getValue());
            } else {
                graphStorage.createVertexAttribute(
                    vertexId,
                    pair.getKey(),
                    pair.getValue(),
                    AttributeRecordType.STRING,
                    true);
            }
        }

        currGMLTokenizer.nextToken();
    }

    private void parseEdge(int graphId) throws IOException {
        currGMLTokenizer.nextToken();
        currGMLTokenizer.nextToken();

        String sourceIdStr = null, targetIdStr = null;
        Map<String, String> attributes = Maps.newHashMap();
        while (isNextAttr()) {
            AbstractMap.SimpleEntry<String, String> pair = parseAttr();
            if (pair.getKey().equalsIgnoreCase(SOURCE))
                sourceIdStr = pair.getValue();
            else if (pair.getKey().equalsIgnoreCase(TARGET))
                targetIdStr = pair.getValue();
            else
                attributes.put(pair.getKey(), pair.getValue());
        }

        if (sourceIdStr == null || targetIdStr == null)
            throw new GMLDecoderException(currGMLTokenizer.getCurrentLineNumber(), String.format("Can't find source id (%s) and target id (%s) in the edge statement", sourceIdStr, targetIdStr));

        if (!nodes.containsKey(sourceIdStr) || !nodes.containsKey(targetIdStr))
            throw new GMLDecoderException(currGMLTokenizer.getCurrentLineNumber(), String.format("Can't find source id (%s) and target id (%s) for the edge", sourceIdStr, targetIdStr));

        int edgeId = graphStorage.createEdgeForVerticesWithSameParents(graphId, nodes.get(sourceIdStr), nodes.get(targetIdStr));
        for (String name : attributes.keySet()) {
            graphStorage.createEdgeAttribute(
                edgeId,
                name,
                attributes.get(name),
                AttributeRecordType.STRING,
                true);
        }

        currGMLTokenizer.nextToken();
    }

    private AbstractMap.SimpleEntry<String, String> parseAttr() throws IOException {
        if (!isNextAttr())
            throw new GMLDecoderException(currGMLTokenizer.getCurrentLineNumber(), currGMLTokenizer.peekToken(), "ID ID");

        String key = readID(currGMLTokenizer.nextToken());
        String value = readID(currGMLTokenizer.nextToken());
        return new AbstractMap.SimpleEntry<>(key, value);
    }

    private boolean isNextGraph() throws IOException {
        return currGMLTokenizer.peekToken() != null && currGMLTokenizer.peekToken(1) != null &&
                currGMLTokenizer.peekToken().equalsIgnoreCase(GRAPH) && currGMLTokenizer.peekToken(1).equalsIgnoreCase(BEGIN_STMT);
    }

    private boolean isNextNode() throws IOException {
        return currGMLTokenizer.peekToken() != null && currGMLTokenizer.peekToken().equals(NODE);
    }

    private boolean isNextEdge() throws IOException {
        return currGMLTokenizer.peekToken() != null && currGMLTokenizer.peekToken().equals(EDGE);
    }

    private boolean isNextStmt() throws IOException {
        return currGMLTokenizer.peekToken() != null && !currGMLTokenizer.peekToken().equals(END_STMT);
    }

    private boolean isNextAttr() throws IOException {
        return !(currGMLTokenizer.peekToken() == null || currGMLTokenizer.peekToken(1) == null) &&
                (isTokenID(currGMLTokenizer.peekToken())  && isTokenID(currGMLTokenizer.peekToken(1)));
    }

    private static boolean isTokenID(String token) {
        if (token == null) return false;

        if (token.startsWith("\"") && token.endsWith("\""))
            return true;
        if (token.startsWith("'") && token.endsWith("'"))
            return true;
        if (token.startsWith("<") && token.endsWith(">"))
            return true;

        Pattern idPattern = Pattern.compile("^[a-zA-Z_\\x200-\\x377][a-zA-Z0-9_\\x200-\\x377]+$");
        Pattern numberPattern = Pattern.compile("^[0-9]+|[0-9]+.[0-9]+");

        return idPattern.matcher(token).matches() || numberPattern.matcher(token).matches();
    }

   private static class GMLTokenizer {
        private String prevToken = null;
        private StreamTokenizer tokenizer;
        private List<String> tokens;
        private List<Integer> lineNumbers;

        public GMLTokenizer(InputStream inputStream) throws IOException {
            var bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            tokenizer = new StreamTokenizer(bufferedReader);
            tokenizer.slashSlashComments(true);
            tokenizer.slashStarComments(true);

            tokens = Lists.newArrayList();
            lineNumbers = Lists.newArrayList();
            readTokens(100);
        }

        public boolean hasNextToken() throws IOException {
            if (tokens.size() == 0) {
                readTokens(100);
            }

            return tokens.size() != 0;
        }

        public String nextToken() throws IOException {
            if (hasNextToken()) {
                String value = tokens.get(0);
                tokens.remove(0);
                lineNumbers.remove(0);
                return value;
            } else {
                return null;
            }
        }

        public String peekToken() throws IOException {
            return peekToken(0);
        }

        public String peekToken(int number) throws IOException {
            if (tokens.size() <= number) {
                readTokens(number - tokens.size() + 5);
                if (tokens.size() > number)
                    return tokens.get(number);
            } else {
                return tokens.get(number);
            }
            return null;
        }

        private void readTokens(int count) throws IOException {
            while (count > 0) {
                int tokenValueType;
                String value = null;
                if ((tokenValueType = tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
                    switch (tokenValueType) {
                        case StreamTokenizer.TT_WORD:
                            value = tokenizer.sval;
                            break;

                        case StreamTokenizer.TT_NUMBER:
                            if ((int)tokenizer.nval == tokenizer.nval)
                                value = Integer.toString((int)tokenizer.nval);
                            else
                                value = Double.toString(tokenizer.nval);
                            break;

                        default:
                            if ((char)tokenValueType == '\"' || (char)tokenValueType == '\'') {
                                value = Character.toString((char)tokenValueType) + tokenizer.sval + Character.toString((char)tokenValueType);
                            } else {
                                value = Character.toString((char)tokenValueType);
                            }
                            break;
                    }
                }

                if (value != null) {
                    tokens.add(value);
                    lineNumbers.add(tokenizer.lineno());

                    prevToken = value;
                }
                count--;
            }
        }

        private int getCurrentLineNumber() {
            return lineNumbers.size() == 0 ? -1 : lineNumbers.get(0);
        }
    }

    public static class GMLDecoderException extends RuntimeException {
        public GMLDecoderException(int lineNumber, String message) {
            super(message);
        }

        public GMLDecoderException(int lineNumber, String token, String expectedToken) {
            super("Can't parse following token: " + token + ", expected token: " + expectedToken + ", line number: " + lineNumber);
        }
    }
}
