package vg.lib.decoder.dot;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import vg.lib.decoder.GraphDecoder;
import vg.lib.model.record.AttributeRecordType;
import vg.lib.model.record.VertexRecord;
import vg.lib.storage.GraphStorage;

/**
 * Dot decoder.
 * <p>
 * http://www.graphviz.org/content/dot-language
 * <p>
 *
 * GCC compiler:
 * https://gcc.gnu.org/onlinedocs/gcc/Developer-Options.html
 * Call graph:
 * 1. gcc test.c -fdump-rtl-expand-graph-lineno
 * Control Flow Graph (or cfg):
 * 1. gcc test.c -fdump-tree-cfg-graph-lineno
 * GIMPLE:
 * 1. gcc test.c -fdump-tree-ssa-graph-lineno
 *    gcc test.c -fdump-tree-optimized-graph-lineno
 * Python:
 * 1. python3 ./pycfg/pycfg.py ./find_max_in_array.py -d
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
@Slf4j
public class DotDecoder extends GraphDecoder {
    // Constants
    private static final String STRICT = "strict";
    private static final String UNDIRECTED_GRAPH = "graph";
    private static final String DIRECTED_GRAPH = "digraph";
    private static final String GRAPH = "graph";
    private static final String NODE = "node";
    private static final String EDGE = "edge";
    private static final String SUBGRAPH = "subgraph";

    private static final String BEGIN_ATTR = "[";
    private static final String END_ATTR = "]";
    private static final String BEGIN_STMT = "{";
    private static final String END_STMT = "}";
    private static final String DELIMITER = ";";
    private static final String EQUAL = "=";
    private static final String COMMA = ",";
    private static final String DIRECTED_EDGE = "->";
    private static final String UNDIRECTED_EDGE = "--";
    private static final String COLON = ":";

    // Main data
    private DotTokenizer currDotTokenizer;
    private DotGraph rootDotGraph, currDotGraph;

    public DotDecoder(InputStream inputStream,
                      String graphName,
                      GraphStorage graphStorage) {
        super(inputStream, graphName, graphStorage);
    }

    @Override
    public int decode() throws GraphDecoderException {
        rootDotGraph = currDotGraph = new DotGraph();

        try {
            currDotTokenizer = new DotTokenizer(inputStream);
        } catch (IOException ex) {
            throw new GraphDecoderException(ex);
        }

        int graphId = graphStorage.createGraphModel(graphName);

        while (currDotTokenizer.peekTokenString() != null) {
            if (isNextGraph()) {
                parseGraph(graphId);
            } else {
                throw new GraphDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekTokenString(), GRAPH);
            }
        }

        return graphId;
    }

    private void parseGraph(int graphId) throws GraphDecoderException {
        log.debug("Method 'parseGraph' was called with graphId '{}'.", graphId);

        String id = null;

        if (isStrictToken(currDotTokenizer.peekTokenString())) {
            currDotTokenizer.nextToken();
        }

        boolean directed = isDirectedGraphToken(currDotTokenizer.peekTokenString());
        if (isDirectedGraphToken(currDotTokenizer.peekTokenString()) | isUndirectedGraphToken(currDotTokenizer.peekTokenString())) {
            currDotTokenizer.nextToken();
        }

        if (isNextId()) {
            id = readID(currDotTokenizer.peekTokenString());
            currDotTokenizer.nextToken();
        }

        if (!isBeginStmtToken(currDotTokenizer.peekTokenString())) {
            throw new GraphDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekTokenString(), BEGIN_STMT);
        } else {
            currDotTokenizer.nextToken();

            int vertexId = graphStorage.createExtendedVertex(graphId, VertexRecord.NO_PARENT_ID);

            if (id != null) {
                graphStorage.setInnerGraphIdAttribute(vertexId, id);
            }

            currDotGraph = currDotGraph.addChild();
            parseStmtList(graphId, vertexId, directed, id);
            currDotGraph = rootDotGraph.getParent(currDotGraph);

            if (!isEndStmtToken(currDotTokenizer.nextToken())) {
                throw new GraphDecoderException(currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekTokenString(), END_STMT);
            }
        }
    }

    private void parseStmtList(int graphId,
                               int vertexOwnerId,
                               boolean directed,
                               String subGraphId) throws GraphDecoderException {
        log.debug(
            "Method 'parseStmtList' was called with graphId '{}', vertexOwnerId '{}', directed '{}', subGraphId '{}'.",
            graphId,
            vertexOwnerId,
            directed,
            subGraphId);

        while (isNextStmt()) {
            if (isDelimiterToken(currDotTokenizer.peekTokenString())) {
                currDotTokenizer.nextToken();
            }
            parseStmt(graphId, vertexOwnerId, directed, subGraphId);
        }

        log.debug(
            "Method 'parseStmtList' was finished with graphId '{}', vertexOwnerId '{}', directed '{}', subGraphId '{}'.",
            graphId,
            vertexOwnerId,
            directed,
            subGraphId);
    }

    private void parseStmt(int graphId,
                           int vertexOwnerId,
                           boolean directed,
                           String subGraphId) throws GraphDecoderException {
        log.debug(
            "Method 'parseStmt' was called with graphId '{}', vertexOwnerId '{}', directed '{}', subGraphId '{}'.",
            graphId,
            vertexOwnerId,
            directed,
            subGraphId);

        if (!isNextStmt()) {
            return;
        }

        if (isNextAttrStmt()) {
            parseAttrStmt(graphId);
        } else if (isNextSubGraph()) {
            parseSubGraph(graphId, vertexOwnerId, directed, subGraphId);
        } else if (isNextAttr()) {
            var pair = parseAttr();
            graphStorage.createVertexAttribute(
                vertexOwnerId, pair.getKey(), pair.getValue(), AttributeRecordType.STRING, true);
        } else {
            parseNodeAndEdgeStmt(graphId, vertexOwnerId, directed, subGraphId);
        }
    }

    private String parseNodeId() throws GraphDecoderException {
        var id = readID(currDotTokenizer.nextToken());
        if (isNextPort()) {
            parsePort();
        }
        return id;
    }

    private void parsePort() throws GraphDecoderException {
        if (isNextCompassPT()) {
            parseCompassPT();
        } else {
            if (!isColonToken(currDotTokenizer.nextToken())) {
                throw new GraphDecoderException(
                    currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekTokenString(), COLON);
            }

            if (isNextId()) {
                readID(currDotTokenizer.nextToken());
            }

            if (isNextCompassPT()) {
                parseCompassPT();
            }
        }
    }

    private void parseCompassPT() {
        currDotTokenizer.nextToken();
        currDotTokenizer.nextToken();
    }

    private void parseNodeAndEdgeStmt(int graphId,
                                      int vertexOwnerId,
                                      boolean directed,
                                      String subGraphId) throws GraphDecoderException {
        log.debug(
            "'parseNodeAndEdgeStmt' was called with graphId '{}', vertexOwnerId '{}', directed '{}', subGraphId '{}'.",
            graphId,
            vertexOwnerId,
            directed,
            subGraphId);

        int sourceVertexId;
        if (isNextSubGraph()) {
            sourceVertexId = parseSubGraph(graphId, vertexOwnerId, directed, subGraphId);
            log.debug("Source subgraph id '{}'.", sourceVertexId);
        } else {
            var nodeId = parseNodeId();

            log.debug("Source node id '{}'.", nodeId);

            sourceVertexId = currDotGraph.getVertexDataBaseId(nodeId);
            if (sourceVertexId < 0) {
                sourceVertexId = graphStorage.createExtendedVertex(graphId, vertexOwnerId);

                log.debug("Source node id '{}' was not found. New source vertex id '{}'.", nodeId, sourceVertexId);

                currDotGraph.addVertex(nodeId, sourceVertexId);

                graphStorage.setVertexIdAttribute(sourceVertexId, nodeId);
            }

            if (!isNextEdgeOp(currDotTokenizer.peekTokenString())) {
                // note: it's node statement and next parse it as node statement
                if (isBeginNextAttrList()) {
                    var result = parseAttrList();
                    for (var entry : result.entrySet()) {
                        var value = entry.getValue();
                        if (entry.getKey().equalsIgnoreCase("label")) {
                            value = value.replace("l\n", "\n");
                            value = value.replace("|", "");
                        }

                        graphStorage.createVertexAttribute(
                            sourceVertexId, entry.getKey(), value, AttributeRecordType.STRING, true);
                    }
                    return;
                }
            }
        }

        var dataBaseEdgeIds = parseEdgeRHS(
            graphId, vertexOwnerId, sourceVertexId, new ArrayList<>(), directed, subGraphId);

        if (isBeginNextAttrList()) {
            var result = parseAttrList();
            for (var dataBaseEdgeId : dataBaseEdgeIds) {
                for (var entry : result.entrySet()) {
                    var key = entry.getKey();
                    var value = entry.getValue();
                    if (key.equalsIgnoreCase("style") && value.equalsIgnoreCase("invis")) {
                        graphStorage.removeEdgeRecord(dataBaseEdgeId);
                        break;
                    }

                    graphStorage.createEdgeAttribute(
                        dataBaseEdgeId, key, result.get(key), AttributeRecordType.STRING, true);
                }
            }
        }
    }

    private List<Integer> parseEdgeRHS(int graphId,
                                       int vertexOwnerId,
                                       int sourceVertexId,
                                       List<Integer> edgeIds,
                                       boolean directed,
                                       String subGraphId) throws GraphDecoderException {
        log.debug(
            "'parseEdgeRHS' was called with graphId {}, vertexOwnerId {}, "
                + "sourceVertexId {}, directed {}, subGraphId '{}'.",
            graphId,
            vertexOwnerId,
            sourceVertexId,
            directed,
            subGraphId);

        parseEdgeOP();

        int targetVertexId;
        if (isNextSubGraph()) {
            targetVertexId = parseSubGraph(graphId, vertexOwnerId, directed, subGraphId);
        } else {
            String nodeId = parseNodeId();

            targetVertexId = currDotGraph.getVertexDataBaseId(nodeId);
            if (targetVertexId < 0) {
                targetVertexId = graphStorage.createExtendedVertex(graphId, vertexOwnerId);

                log.debug("Target node id '{}' was not found. New target vertex id '{}'.", nodeId, targetVertexId);

                currDotGraph.addVertex(nodeId, targetVertexId);

                graphStorage.setVertexIdAttribute(targetVertexId, nodeId);
            }
        }

        // create edge in graph storage.
        try {
            var createdEdgeIds = graphStorage
                .createCompositeEdge(graphId, sourceVertexId, targetVertexId, -1, -1, directed);
            edgeIds.addAll(createdEdgeIds);
        } catch (IllegalArgumentException ex) {
            log.error(ex.getMessage());
        }

        if (isNextEdgeRHS()) {
            parseEdgeRHS(graphId, vertexOwnerId, targetVertexId, edgeIds, directed, subGraphId);
        }

        return edgeIds;
    }

    private void parseEdgeOP() throws GraphDecoderException {
        if (!isNextEdgeRHS()) {
            throw new GraphDecoderException(
                currDotTokenizer.getCurrentLineNumber(),
                currDotTokenizer.peekTokenString(),
                DIRECTED_EDGE + "|" + UNDIRECTED_EDGE);
        }

        var token = currDotTokenizer.nextToken();
        Validate.isTrue(StringUtils.equalsIgnoreCase(token, DIRECTED_EDGE));
    }

    private void parseAttrStmt(int graphId) throws GraphDecoderException {
        log.debug("'parseAttrStmt' was called with graphId {}.", graphId);

        var token = currDotTokenizer.nextToken();

        if (token == null) {
            throw new GraphDecoderException(
                currDotTokenizer.getCurrentLineNumber(), null, GRAPH + "|" + NODE + "|" + EDGE);
        }

        switch (token) {
            case GRAPH:
            case NODE:
            case EDGE:
                var attrList = parseAttrList();

                var skippedAttributes = attrList.entrySet().stream()
                    .map(entry -> entry.getKey() + " = " + entry.getValue())
                    .collect(Collectors.joining(", "));
                log.debug("Skip following attributes: {}.", skippedAttributes);

                break;
            default:
                throw new GraphDecoderException(
                    currDotTokenizer.getCurrentLineNumber(), token, GRAPH + "|" + NODE + "|" + EDGE);
        }
    }

    private LinkedHashMap<String, String> parseAttrList() throws GraphDecoderException {
        log.debug("'parseAttrList' was called.");

        if (!isBeginNextAttrList()) {
            throw new GraphDecoderException(
                currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekTokenString(), BEGIN_ATTR);
        }

        var result = new LinkedHashMap<String, String>();
        currDotTokenizer.nextToken();
        while (!isNextEndAttrList()) {
            var pair = parseAttr();

            result.put(pair.getKey(), pair.getValue());

            if (isDelimiterToken(currDotTokenizer.peekTokenString()) || isCommaToken(currDotTokenizer.peekTokenString())) {
                currDotTokenizer.nextToken();
            }

            if (isNextAttr()) {
                continue;
            }

            if (currDotTokenizer.peekTokenString() == null || !currDotTokenizer.peekTokenString().equals(END_ATTR)) {
                throw new GraphDecoderException(
                    currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekTokenString(), END_ATTR);
            }
        }
        currDotTokenizer.nextToken();
        return result;
    }

    private Map.Entry<String, String> parseAttr() throws GraphDecoderException {
        if (!isNextAttr()) {
            throw new GraphDecoderException(
                currDotTokenizer.getCurrentLineNumber(), currDotTokenizer.peekTokenString(), "ID" + EQUAL + "ID");
        }

        var key = readID(currDotTokenizer.nextToken());
        currDotTokenizer.nextToken();
        var value = readID(currDotTokenizer.nextToken());
        return new AbstractMap.SimpleEntry<>(key, value);
    }

    private int parseSubGraph(int graphId,
                              int vertexOwnerId,
                              boolean directed,
                              String subGraphId) throws GraphDecoderException {
        log.debug(
            "'parseSubGraph' with graphId '{}', vertexOwnerId '{}', directed '{}', subGraphId '{}'.",
            graphId,
            vertexOwnerId,
            directed,
            subGraphId);

        int dbSubGraphId = -1;

        if (currDotTokenizer.hasNextToken()) {
            var token = currDotTokenizer.nextToken();

            if (Objects.equals(token, SUBGRAPH)) {
                token = currDotTokenizer.nextToken();
            }

            if (token != null && !token.equals(BEGIN_STMT)) {
                subGraphId = token;
                token = currDotTokenizer.nextToken();
            }

            if (Objects.equals(token, BEGIN_STMT)) {
                dbSubGraphId = graphStorage.createExtendedVertex(graphId, vertexOwnerId);

                if (subGraphId != null) {
                    graphStorage.setVertexIdAttribute(dbSubGraphId, subGraphId);
                    graphStorage.setInnerGraphIdAttribute(dbSubGraphId, subGraphId);
                }

                currDotGraph = currDotGraph.addChild();
                parseStmtList(graphId, dbSubGraphId, directed, subGraphId);
                currDotGraph = rootDotGraph.getParent(currDotGraph);
            }

            token = currDotTokenizer.nextToken();
            if (token == null || !token.equals(END_STMT)) {
                throw new GraphDecoderException(currDotTokenizer.getCurrentLineNumber(), token, END_STMT);
            }
        }

        return dbSubGraphId;
    }

    private boolean isNextStmt() {
        return currDotTokenizer.peekTokenString() != null && !currDotTokenizer.peekTokenString().equals(END_STMT);
    }

    private boolean isNextSubGraph() {
        return currDotTokenizer.peekTokenString() != null && (currDotTokenizer.peekTokenString().equals(SUBGRAPH) || currDotTokenizer.peekTokenString().equals(BEGIN_STMT));
    }

    private boolean isNextPort() {
        String token = currDotTokenizer.peekTokenString();

        return token != null && token.equals(COLON);
    }

    private boolean isNextCompassPT() {
        if (!isColonToken(currDotTokenizer.peekTokenString())) return false;

        Pattern pattern = Pattern.compile("^n$|^ne$|^e$|^se$|^s$|^sw$|^w$|^nw$|^c$|^_$");
        return currDotTokenizer.peekTokenString(1) != null && pattern.matcher(currDotTokenizer.peekTokenString(1)).matches();
    }

    private boolean isNextGraph() {
        return currDotTokenizer.hasNextToken() &&
                (isStrictToken(currDotTokenizer.peekTokenString()) ||
                        isDirectedGraphToken(currDotTokenizer.peekTokenString()) ||
                        isUndirectedGraphToken(currDotTokenizer.peekTokenString()));
    }

    private boolean isNextAttrStmt() {
        var token = currDotTokenizer.peekTokenString();
        var nextToken = currDotTokenizer.peekTokenString(1);

        if (token == null || nextToken == null) {
            return false;
        }

        return (token.equals(UNDIRECTED_GRAPH) ||
                token.equals(NODE) ||
                token.equals(EDGE)) && nextToken.equals(BEGIN_ATTR);
    }

    private boolean isBeginNextAttrList() {
        return currDotTokenizer.peekTokenString() != null && currDotTokenizer.peekTokenString().equals(BEGIN_ATTR);
    }

    private boolean isNextEndAttrList() {
        return currDotTokenizer.peekTokenString() != null && currDotTokenizer.peekTokenString().equals(END_ATTR);
    }

    private boolean isNextAttr() {
        return !(currDotTokenizer.peekTokenString() == null || currDotTokenizer.peekTokenString(1) == null || currDotTokenizer.peekTokenString(2) == null) &&
                (isTokenId(currDotTokenizer.peekTokenString()) && currDotTokenizer.peekTokenString(1).equals(EQUAL) && isTokenId(currDotTokenizer.peekTokenString(2)));
    }

    private boolean isNextEdgeRHS() {
        return isNextEdgeOp(currDotTokenizer.peekTokenString());
    }

    private boolean isNextId() {
        return isTokenId(currDotTokenizer.peekTokenString());
    }

    private static boolean isTokenId(String token) {
        if (token == null) return false;

        if (token.startsWith("\"") && token.endsWith("\""))
            return true;
        if (token.startsWith("'") && token.endsWith("'"))
            return true;
        if (token.startsWith("<") && token.endsWith(">"))
            return true;

        if (token.contains(" "))
            return false;

        Pattern idPattern = Pattern.compile("^[a-zA-Z0-9_\\x200-\\x377]+$");
        Pattern numberPattern = Pattern.compile("^[0-9]+|[0-9]+.[0-9]+");

        return idPattern.matcher(token).matches() || numberPattern.matcher(token).matches();
    }

    private static boolean isBeginStmtToken(String token) {
        return token != null && token.equalsIgnoreCase(BEGIN_STMT);
    }

    private static boolean isEndStmtToken(String token) {
        return token != null && token.equalsIgnoreCase(END_STMT);
    }

    private static boolean isDirectedGraphToken(String token) {
        return token != null && token.equalsIgnoreCase(DIRECTED_GRAPH);
    }

    private static boolean isUndirectedGraphToken(String token) {
        return token != null && token.equalsIgnoreCase(UNDIRECTED_GRAPH);
    }

    private static boolean isStrictToken(String token) {
        return token != null && token.equalsIgnoreCase(STRICT);
    }

    private static boolean isDelimiterToken(String token) {
        return token != null && token.equalsIgnoreCase(DELIMITER);
    }

    private static boolean isCommaToken(String token) {
        return token != null && token.equalsIgnoreCase(COMMA);
    }

    private static boolean isColonToken(String token) {
        return token != null && token.equalsIgnoreCase(COLON);
    }

    private static boolean isNextEdgeOp(String token) {
        return token != null && (token.equals(DIRECTED_EDGE) || token.equals(UNDIRECTED_EDGE));
    }

    /**
     * Use the class in the DotDecoder context only.
     * Thread safe only for progress bar.
     */
    private static class DotTokenizer {
        private String content;
        private List<Token> tokens;

        DotTokenizer(InputStream inputStream) throws IOException {
            content = new String(inputStream.readAllBytes(), StandardCharsets.UTF_8);

            char[] charContent = content.toCharArray();
            tokens = Lists.newArrayList();
            int pos = 0;
            int line = 0;

            StringBuilder token = new StringBuilder();
            for (int i = 0; i < charContent.length; i++) {
                char currByte = charContent[i];

                if (token.length() == 0) {
                    pos = i;
                }

                if (currByte == '\n') {
                    line++;
                }

                if (currByte == '\"'
                        || currByte == '\''
                        || currByte == '\n'
                        || currByte == '\r'
                        || currByte == ' '
                        || currByte == '\t'
                        || currByte == '{'
                        || currByte == '}'
                        || currByte == '['
                        || currByte == ']'
                        || currByte == '-'
                        || currByte == '>'
                        || currByte == '='
                        || currByte == ','
                        || currByte == ';'
                        || currByte == ':') {
                    if (token.length() != 0) {
                        if (token.length() == 1 && token.toString().getBytes()[0] == '-' && (currByte == '-' || currByte == '>')) {
                            token.append(currByte);
                        }

                        tokens.add(new Token(token.toString(), pos, line));
                        token.setLength(0);

                        if (currByte == '{'
                                || currByte == '}'
                                || currByte == '['
                                || currByte == ']'
                                || currByte == '='
                                || currByte == ','
                                || currByte == ';'
                                || currByte == ':') {
                            token.append(currByte);
                            tokens.add(new Token(token.toString(), pos, line));
                            token.setLength(0);
                        }
                    } else {
                        if (currByte == '-') {
                            token.append(currByte);
                        }

                        // handle long attribute's values.
                        if (currByte == '\'' || currByte == '\"') {
                            token.append(currByte);
                            for (i = i + 1; i < charContent.length; i++) {
                                char nextByte = charContent[i];
                                token.append(nextByte);

                                // expect same byte for end of long attribute's value.
                                if (nextByte == currByte) {
                                    if (!token.toString().endsWith("\\" + nextByte)) {
                                        break;
                                    }
                                }
                            }

                            // clean slashes
                            String comment = token.toString()
                                    .replace("\\l", "")
                                    .replace("\\", "");

                            tokens.add(new Token(comment, pos, line));
                            token.setLength(0);
                        }
                        if (currByte == '{'
                                || currByte == '}'
                                || currByte == '['
                                || currByte == ']'
                                || currByte == '='
                                || currByte == ','
                                || currByte == ';'
                                || currByte == ':') {
                            token.append(currByte);
                            tokens.add(new Token(token.toString(), pos, line));
                            token.setLength(0);
                        }
                    }
                } else {
                    token.append(currByte);
                }
            }
        }

        synchronized boolean hasNextToken() {
            return tokens.size() != 0;
        }

        synchronized String nextToken() {
            if (hasNextToken()) {
                return tokens.remove(0).getToken();
            } else {
                return null;
            }
        }

        synchronized int getContentLength() {
            return content.length();
        }

        synchronized Token peekToken() {
            if (tokens.size() > 0) {
                return tokens.get(0);
            }
            return null;
        }

        synchronized String peekTokenString() {
            return peekTokenString(0);
        }

        synchronized String peekTokenString(int number) {
            if (tokens.size() > number) {
                return tokens.get(number).getToken();
            }
            return null;
        }

        synchronized private int getCurrentLineNumber() {
            if (tokens.size() > 0) {
                return tokens.get(0).getLine() + 1;
            }

            return -1;
        }

        private static class Token {
            private String token;
            private int pos;
            private int line;

            private Token(String token, int pos, int line) {
                this.token = token;
                this.pos = pos;
                this.line = line;
            }

            private String getToken() {
                return token;
            }

            private int getPos() {
                return pos;
            }

            private int getLine() {
                return line;
            }

            @Override
            public String toString() {
                return token;
            }
        }
    }

    private static class DotGraph {
        private List<DotGraph> graphs = Lists.newArrayList();

        private Map<String, Integer> vertexDotIdToDataBaseId = Maps.newLinkedHashMap();

        private DotGraph getParent(DotGraph child) {
            if (graphs.contains(child)) {
                return this;
            }

            for (DotGraph graph : graphs) {
                DotGraph parent = graph.getParent(child);
                if (parent != null)
                    return parent;
            }

            return null;
        }

        private DotGraph addChild() {
            DotGraph child = new DotGraph();
            graphs.add(child);
            return child;
        }

        /**
         * Returns id (for the node) in data base, -1 - otherwise.
         */
        private int getVertexDataBaseId(String nodeId) {
            Integer id = vertexDotIdToDataBaseId.get(nodeId);
            if (id == null) {
                for (DotGraph graph : graphs) {
                    id = graph.getVertexDataBaseId(nodeId);
                    if (id != -1) {
                        return id;
                    }
                }
                return -1;
            }
            return id;
        }

        private void addVertex(String dotVertexId, int vertexId) {
            vertexDotIdToDataBaseId.put(dotVertexId, vertexId);
        }
    }
}
