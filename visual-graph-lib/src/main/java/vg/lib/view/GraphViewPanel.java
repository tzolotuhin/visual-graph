package vg.lib.view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import javax.swing.JPanel;
import lombok.extern.slf4j.Slf4j;
import vg.lib.config.VGConfigSettings;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVObject;
import vg.lib.view.elements.GVVertex;
import vg.lib.view.elements.GVSelectionFrame;

@Slf4j
public class GraphViewPanel extends JPanel {
    // Data
    private final GraphViewModel graphViewModel;
    private GraphViewBufferedImage sharedImage;

    public GraphViewPanel(GraphViewModel graphViewModel) {
        this.graphViewModel = graphViewModel;
    }

    public GraphViewBufferedImage getSharedImage() {
        synchronized (this) {
            return sharedImage;
        }
    }

    public void update() {
        log.debug("Start rendering...");

        graphViewModel.calculate();

        // get preferred size and viewport
        Point size = graphViewModel.getAbsoluteSize();
        Rectangle viewport = graphViewModel.getViewport();
        var gridType = graphViewModel.getGridType();
        var visibilityOfMinimap = graphViewModel.getVisibilityOfMinimap();
        var absoluteGridSize = graphViewModel.getAbsoluteGridSize();
        var selectionFrame = graphViewModel.getSelectionFrame();
        var searchBarResultsObject = graphViewModel.getSearchBarResultsObject();

        if (size.x == 0 || size.y == 0 || viewport.width <= 0 || viewport.height <= 0) {
            setSharedImage(null);
            log.debug("Finish rendering (with null)...");
            return;
        }

        Rectangle viewportWithCapacity = getViewportWithCapacity(viewport, graphViewModel.getViewportCapacity());

        var image = new GraphViewBufferedImage(
            viewportWithCapacity.width, viewportWithCapacity.height, BufferedImage.TYPE_INT_ARGB);
        image.setPreferredSize(new Dimension(size.x, size.y));
        image.setViewport(viewport);
        image.setViewportWithCapacity(viewportWithCapacity);

        Graphics2D g2 = (Graphics2D)image.getGraphics();

        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        List<GVVertex> vertices = graphViewModel.getVertices();
        List<GVEdge> edges = graphViewModel.getEdges();

        // draw background.
        g2.setColor(Color.WHITE);
        g2.fillRect(0, 0, viewportWithCapacity.width, viewportWithCapacity.height);

        // draw grid if need...
        if (gridType == VGConfigSettings.UNDER_THE_GRAPH_GRID_TYPE) {
            drawGrid(g2, viewportWithCapacity, size, absoluteGridSize);
        }

        long vertexRendering = 0, edgeRendering = 0;
        for (int level = 0; level < graphViewModel.getLevelSize(); level++) {
            // draw edges.
            long startEdgeRendering = System.currentTimeMillis();
            for (var edge : edges) {
                if (edge.getLevel() != level) {
                    continue;
                }

                drawEdge(g2, viewportWithCapacity, edge);
            }
            long finishEdgeRendering = System.currentTimeMillis();

            // draw vertices.
            long startVertexRendering = System.currentTimeMillis();
            for (var vertex : vertices) {
                if (vertex.getLevel() != level) {
                    continue;
                }

                drawVertex(g2, viewportWithCapacity, vertex);
            }
            long finishVertexRendering = System.currentTimeMillis();

            vertexRendering += (finishVertexRendering - startVertexRendering);
            edgeRendering += (finishEdgeRendering - startEdgeRendering);
        }

        // draw grid if need...
        if (gridType == VGConfigSettings.ABOVE_THE_GRAPH_GRID_TYPE) {
            drawGrid(g2, viewportWithCapacity, size, absoluteGridSize);
        }

        // draw a focus over the element which is the result of the search bar tool.
        drawFocus(g2, viewportWithCapacity, searchBarResultsObject);

        // draw selection frame.
        drawSelectionFrame(g2, viewportWithCapacity, selectionFrame);

        // draw minimap.
        long minimapRendering = 0;
        if (visibilityOfMinimap) {
            minimapRendering = drawMinimap(g2, viewportWithCapacity, size, vertices, edges);
        }

        setSharedImage(image);

        log.debug(
            String.format("\nTime for rendering vertices: %s ms\n" +
                    "Time for rendering edges: %s ms\n" +
                    "Time for rendering minimap: %s ms\n" +
                    "Finish rendering...",
                vertexRendering,
                edgeRendering,
                minimapRendering));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        var image = getSharedImage();
        if (image == null) {
            return;
        }

        try {
            int x1 = image.getViewport().x - image.getViewportWithCapacity().x;
            int y1 = image.getViewport().y - image.getViewportWithCapacity().y;
            int width = image.getViewportWithCapacity().x + image.getViewportWithCapacity().width - image.getViewport().x;
            int height = image.getViewportWithCapacity().y + image.getViewportWithCapacity().height - image.getViewport().y;

            Graphics2D g2 = (Graphics2D)g;

            g2.setColor(Color.WHITE);
            g2.fillRect(image.getViewport().x, image.getViewport().y, image.getViewport().width, image.getViewport().height);

            g2.drawImage(
                    image.getSubimage(x1, y1, width, height),
                    image.getViewport().x,
                    image.getViewport().y,
                    this);
        } catch (Throwable ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        var image = getSharedImage();
        if (image != null) {
            return image.getPreferredSize();
        }
        return super.getPreferredSize();
    }

    private Rectangle getViewportWithCapacity(Rectangle currViewport, int capacity) {
        int x1 = currViewport.x - capacity;
        int y1 = currViewport.y - capacity;
        int x2 = currViewport.x + currViewport.width + capacity;
        int y2 = currViewport.y + currViewport.height + capacity;

        if (x1 < 0) {
            x1 = 0;
        }
        if (y1 < 0) {
            y1 = 0;
        }

        return new Rectangle(x1, y1, x2 - x1, y2 - y1);
    }

    private void setSharedImage(GraphViewBufferedImage image) {
        synchronized (this) {
            this.sharedImage = image;
        }
    }

    private void drawGrid(Graphics2D g2, Rectangle viewportWithCapacity, Point absoluteSize, Point2D.Float absoluteGridSize) {
        g2.setColor(VGConfigSettings.GRID_COLOR);
        g2.setStroke(new BasicStroke(1f));

        int startIndex = (int)(viewportWithCapacity.y / absoluteGridSize.y);
        int finishIndex = (int)((viewportWithCapacity.y + viewportWithCapacity.height) / absoluteGridSize.y);
        for (int i = startIndex; i <= finishIndex; i++) {
            g2.drawLine(
                    0,
                    Math.round(i * absoluteGridSize.y - viewportWithCapacity.y),
                    Math.max(absoluteSize.x, viewportWithCapacity.width),
                    Math.round(i * absoluteGridSize.y - viewportWithCapacity.y));
        }

        startIndex = (int)(viewportWithCapacity.x / absoluteGridSize.x);
        finishIndex = (int)((viewportWithCapacity.x + viewportWithCapacity.width) / absoluteGridSize.x);
        for (int i = startIndex; i <= finishIndex; i++) {
            g2.drawLine(
                    Math.round(i * absoluteGridSize.x - viewportWithCapacity.x),
                    0,
                    Math.round(i * absoluteGridSize.x - viewportWithCapacity.x),
                    Math.max(absoluteSize.y, viewportWithCapacity.height));
        }
    }

    private void drawFocus(Graphics2D g2, Rectangle viewportWithCapacity, GVObject searchBarResultsObject) {
        if (searchBarResultsObject == null) {
            return;
        }

        if (searchBarResultsObject instanceof GVVertex) {
            var shape = ((GVVertex)searchBarResultsObject).getShape();
            g2.setStroke(new BasicStroke(4f));
            g2.setColor(Color.RED);
            g2.drawRect(
                    shape.getAbsolutePosition().x - viewportWithCapacity.x - 5,
                    shape.getAbsolutePosition().y - viewportWithCapacity.y - 5,
                    shape.getAbsoluteSize().x + 10,
                    shape.getAbsoluteSize().y + 10);
        }

        if (searchBarResultsObject instanceof GVEdge) {
            var shape = ((GVEdge)searchBarResultsObject).getShape();

            g2.setStroke(new BasicStroke(4f));
            g2.setColor(Color.RED);

            List<Point> points = shape.getAbsolutePoints();
            for (int i = 0; i < points.size() - 1; i++) {
                Point p1 = points.get(i);
                Point p2 = points.get(i + 1);


                g2.drawLine(
                        p1.x - viewportWithCapacity.x,
                        p1.y - viewportWithCapacity.y,
                        p2.x - viewportWithCapacity.x,
                        p2.y - viewportWithCapacity.y);
            }
        }
    }

    private long drawMinimap(
            Graphics2D g2,
            Rectangle viewportWithCapacity,
            Point size,
            List<GVVertex> vertices,
            List<GVEdge> edges) {
        int initMinimapX = viewportWithCapacity.width - 200;
        int initMinimapY = 10;
        int initMinimapSize = 190;
        int shiftMinimapX = 0;
        int shiftMinimapY = 0;
        int border = 2;

        int width = initMinimapSize;
        if (size.y > size.x) {
            float ratio = (float)size.x / size.y;
            width = Math.round(initMinimapSize * ratio);
            shiftMinimapX += (initMinimapSize - width) / 2;
        }

        int height = initMinimapSize;
        if (size.x > size.y) {
            float ratio = (float)size.y / size.x;
            height = Math.round(initMinimapSize * ratio);
            shiftMinimapY += (initMinimapSize - height) / 2;
        }

        g2.translate(initMinimapX, initMinimapY);
        g2.setColor(Color.BLACK);
        g2.fillRect(
                -border,
                -border,
                initMinimapSize + 2 * border,
                initMinimapSize + 2 * border);
        g2.setColor(Color.GRAY);
        g2.fillRect(
                0,
                0,
                initMinimapSize,
                initMinimapSize);

        g2.translate(shiftMinimapX, shiftMinimapY);
        g2.setColor(Color.WHITE);
        g2.fillRect(
                0,
                0,
                width,
                height);

        long minimapRendering = 0;
        for (int level = 0; level < graphViewModel.getLevelSize(); level++) {
            // draw edges.
            long startEdgeRendering = System.currentTimeMillis();
            for (var edge : edges) {
                if (edge.getLevel() != level) {
                    continue;
                }

                drawEdgeOnMinimap(g2, size, new Point(width, height), edge);
            }
            long finishEdgeRendering = System.currentTimeMillis();
            minimapRendering += (finishEdgeRendering - startEdgeRendering);

            // draw vertices on minimap.
            long startVertexRendering = System.currentTimeMillis();
            for (GVVertex vertex : vertices) {
                if (vertex.getLevel() != level) {
                    continue;
                }

                drawVertexOnMinimap(g2, size, new Point(width, height), vertex);
            }
            long finishVertexRendering = System.currentTimeMillis();

            minimapRendering += (finishVertexRendering - startVertexRendering);
        }

        // draw selection frame on minimap.
        g2.setStroke(new BasicStroke(1f));
        g2.setColor(Color.BLUE);
        int viewportX = Math.round((float)width * viewportWithCapacity.x / size.x);
        int viewportY = Math.round((float)height * viewportWithCapacity.y / size.y);
        int viewportWidth = Math.round((float)width * viewportWithCapacity.width / size.x);
        int viewportHeight = Math.round((float)height * viewportWithCapacity.height / size.y);

        if (viewportWidth > width) {
            viewportWidth = width;
        }
        if (viewportHeight > height) {
            viewportHeight = height;
        }
        g2.drawRect(
                viewportX,
                viewportY,
                viewportWidth,
                viewportHeight);

        return minimapRendering;
    }

    private void drawSelectionFrame(Graphics2D g2, Rectangle viewportWithCapacity, GVSelectionFrame selectionFrame) {
        if (!selectionFrame.isVisible()) {
            return;
        }

        g2.setColor(Color.BLUE);
        g2.setStroke(new BasicStroke(1f));

        var selectionFrameRect = selectionFrame.getSelectionFrameRect();
        var border = new Point(2, 2);
        var size = new Point(selectionFrameRect.width, selectionFrameRect.height);
        GraphViewUtils.drawRectWithBorder(
                g2,
                selectionFrameRect.getLocation(),
                size,
                border,
                viewportWithCapacity);
    }

    private void drawVertex(Graphics2D g2, Rectangle viewportWithCapacity, GVVertex vertex) {
        var shape = vertex.getShape();
        shape.draw(g2, viewportWithCapacity, vertex.isSelect());
        shape.getLabel().draw(g2, viewportWithCapacity, shape.getAbsolutePosition());
    }

    private void drawVertexOnMinimap(Graphics2D g2, Point size, Point minimapSize, GVVertex vertex) {
        var shape = vertex.getShape();
        shape.drawOnMinimap(g2, size, minimapSize, vertex.isSelect());
    }

    private void drawEdge(Graphics2D g2, Rectangle viewportWithCapacity, GVEdge edge) {
        var shape = edge.getShape();
        shape.draw(g2, viewportWithCapacity, edge.isSelect());
        shape.getLabel().draw(g2, viewportWithCapacity, new Point());
    }

    private void drawEdgeOnMinimap(Graphics2D g2, Point size, Point minimapSize, GVEdge edge) {
        var shape = edge.getShape();
        shape.drawOnMinimap(g2, size, minimapSize, edge.isSelect());
    }
}
