package vg.lib.view.elements;

import java.awt.Color;
import java.util.List;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import vg.lib.common.VGUtils;
import vg.lib.config.VGConfigSettings;
import vg.lib.view.shapes.eshape.GraphViewDefaultEdgeShape;
import vg.lib.view.shapes.eshape.GraphViewEdgeShape;
import vg.lib.model.graph.Attribute;

@Getter
@Setter
public class GVEdge extends GVObject {
    private UUID srcId;

    private UUID trgId;

    private UUID srcPortId;

    private UUID trgPortId;

    private int masterIdOfCompositeEdge;

    private GraphViewEdgeShape shape;

    public GVEdge(UUID srcId,
                  UUID trgId,
                  UUID srcPortId,
                  UUID trgPortId,
                  UUID parentId,
                  List<Attribute> attributes) {
        super(parentId, attributes);
        this.srcId = srcId;
        this.trgId = trgId;
        this.srcPortId = srcPortId;
        this.trgPortId = trgPortId;
        this.masterIdOfCompositeEdge = VGUtils.getMasterIdOfCompositeEdge(attributes);

        this.shape = new GraphViewDefaultEdgeShape(
            GraphViewEdgeShape.DEFAULT_EDGE_SHAPE,
            VGUtils.isDirectedEdge(attributes),
            VGUtils.isPartOfCompositeEdge(attributes),
            VGUtils.isFinishPartOfCompositeEdge(attributes),
            VGConfigSettings.DEFAULT_EDGE_COLOR);
    }

    public GVEdge(GVEdge gvEdge) {
        super(gvEdge);
        this.srcId = gvEdge.getSrcId();
        this.trgId = gvEdge.getTrgId();
        this.srcPortId = gvEdge.getSrcPortId();
        this.trgPortId = gvEdge.getTrgPortId();
        this.masterIdOfCompositeEdge = gvEdge.getMasterIdOfCompositeEdge();

        this.shape = gvEdge.getShape().clone();
    }

    public Color getInitialColor() {
        return VGUtils.getColor(getAttributes().keySet(), VGConfigSettings.DEFAULT_EDGE_COLOR);
    }
}
