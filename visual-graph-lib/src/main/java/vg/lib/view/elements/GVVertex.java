package vg.lib.view.elements;

import java.awt.Color;
import java.awt.Point;
import java.util.List;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import vg.lib.common.VGUtils;
import vg.lib.config.VGConfigSettings;
import vg.lib.view.shapes.vshape.GraphViewCircleShape;
import vg.lib.view.shapes.vshape.GraphViewDiamondShape;
import vg.lib.view.shapes.vshape.GraphViewRectangleShape;
import vg.lib.view.shapes.vshape.GraphViewVertexShape;
import vg.lib.model.graph.Attribute;

@Getter
@Setter
public class GVVertex extends GVObject {
    public static final int VERTEX_TYPE = 1;
    public static final int VERTEX_WITH_PORTS_TYPE = 2;
    public static final int FRAGMENT_TYPE = 3;

    private GraphViewVertexShape shape;

    private int type;

    private String nameCache;

    public GVVertex(UUID parentId,
                    Point position,
                    Point size,
                    List<Attribute> attributes) {
        super(parentId, attributes);

        this.shape = new GraphViewRectangleShape(position, size, VGConfigSettings.DEFAULT_VERTEX_COLOR);
        this.type = VERTEX_TYPE;
    }

    public GVVertex(GVVertex vertex) {
        super(vertex);

        this.shape = vertex.getShape().clone();

        this.type = vertex.getType();
    }

    public void setTypeIfNeed(int type) {
        if (this.type < type) {
            this.type = type;
        }
    }

    public boolean isPort() {
        return VGUtils.isPort(this);
    }

    public boolean isFakePort() {
        return VGUtils.isFakePort(this);
    }

    public boolean isRealPort() {
        return isPort() && !VGUtils.isFakePort(this);
    }

    public boolean isInputPort() {
        return VGUtils.isInputPort(this);
    }

    public boolean isOutputPort() {
        return VGUtils.isOutputPort(this);
    }

    public boolean isFragment() {
        return type == FRAGMENT_TYPE;
    }

    public boolean isVertexWithPorts() {
        return type == VERTEX_WITH_PORTS_TYPE;
    }

    public boolean isVertex() {
        return type == VERTEX_TYPE;
    }

    public GraphViewVertexShape getInitialShape(int defaultShapeType) {
        if (type == VERTEX_WITH_PORTS_TYPE || type == FRAGMENT_TYPE) {
            return shape;
        }

        switch (getInitialShapeType(defaultShapeType)) {
            case GraphViewVertexShape.DIAMOND_VERTEX_SHAPE:
                return new GraphViewDiamondShape(shape);
            case GraphViewVertexShape.CIRCLE_VERTEX_SHAPE:
                return new GraphViewCircleShape(shape);
            default:
                return new GraphViewRectangleShape(shape);
        }
    }

    public int getInitialShapeType(int defaultShapeType) {
        if (type == VERTEX_WITH_PORTS_TYPE || type == FRAGMENT_TYPE) {
            return shape.getType();
        }

        return VGUtils.getShape(getAttributes().keySet(), defaultShapeType);
    }

    public Color getInitialColor() {
        if (isPort()) {
            if (isFakePort()) {
                return VGConfigSettings.FAKE_PORT_COLOR;
            }
            return VGConfigSettings.PORT_COLOR;
        }

        if (type == GVVertex.FRAGMENT_TYPE) {
            return VGConfigSettings.COLORS[getLevel() % VGConfigSettings.COLORS.length];
        }

        return VGUtils.getColor(getAttributes().keySet(), VGConfigSettings.DEFAULT_VERTEX_COLOR);
    }

    public String getName() {
        if (nameCache == null) {
            nameCache = VGUtils.getVertexName(getAttributes().keySet());
        }

        return nameCache;
    }
}
