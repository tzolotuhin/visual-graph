package vg.lib.view.elements;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

public class GVParentWithElements {
    @Getter
    @Setter
    private GVVertex parent;

    @Getter @Setter
    private List<GVVertex> vertices;

    @Getter @Setter
    private List<GVEdge> edges;

    public GVParentWithElements(GVVertex parent, List<GVVertex> vertices, List<GVEdge> edges) {
        this.parent = parent;
        this.vertices = vertices;
        this.edges = edges;
    }
}
