package vg.lib.view.elements;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.model.graph.Attribute;

@Getter
@Setter
public class GVObject {
    private UUID id;

    private UUID parentId;

    private int level;

    private boolean select;

    private Map<Attribute, MutableBoolean> attributes = Maps.newLinkedHashMap();

    public GVObject(UUID parentId, List<Attribute> attributes) {
        if (attributes == null) {
            attributes = Lists.newArrayList();
        }

        this.parentId = parentId;
        attributes.forEach(x -> this.attributes.put(x, new MutableBoolean(true)));
    }

    public GVObject(GVObject gvObject) {
        this.id = gvObject.id;
        this.parentId = gvObject.parentId;
        this.level = gvObject.level;
        this.select = gvObject.select;
        gvObject.attributes.forEach((attribute, mutableBoolean) -> attributes.put(new Attribute(attribute),
            new MutableBoolean(mutableBoolean.booleanValue())));
    }

    public Attribute getAttribute(String attributeName) {
        return attributes.keySet().stream().filter(x -> x.getName().equals(attributeName)).findFirst().orElse(null);
    }

    @Override
    public String toString() {
        return "GVObject{" +
            "id=" + id +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GVObject gvObject = (GVObject) o;
        return Objects.equals(id, gvObject.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
