package vg.lib.view.elements;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GVSelectionFrame {
    private Point start;

    private Point finish;

    private boolean visible;

    private List<GVVertex> selectedVertices;

    private List<GVEdge> selectedEdges;

    public Rectangle getSelectionFrameRect() {
        int x = Math.min(start.x, finish.x);
        int width = Math.max(start.x, finish.x) - x;
        int y = Math.min(start.y, finish.y);
        int height = Math.max(start.y, finish.y) - y;
        return new Rectangle(x, y, width, height);
    }

    @Override
    public String toString() {
        return "SelectionFrame{" +
                "start=" + start +
                ", finish=" + finish +
                '}';
    }
}
