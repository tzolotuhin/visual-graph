package vg.lib.view;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GraphViewBufferedImage extends BufferedImage {
    private Rectangle viewport;
    private Rectangle viewportWithCapacity;
    private Dimension preferredSize;

    GraphViewBufferedImage(int width, int height, int imageType) {
        super(width, height, imageType);
    }
}
