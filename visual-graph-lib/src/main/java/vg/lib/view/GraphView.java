package vg.lib.view;

import vg.lib.common.CallBack;
import vg.lib.model.graph.Graph;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVParentWithElements;
import vg.lib.view.elements.GVVertex;

import javax.swing.*;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

/**
 * This interface determine which methods should be implemented for
 * visual presentation of graph.
 */
public interface GraphView {
    UUID getId();

    GVVertex getVertex(UUID vertexId);

    /**
     * Note: the method should return copies of elements.
     */
    GVParentWithElements getElements(GVVertex parent);

    /**
     * Note: the method should return copies of elements.
     */
    GVParentWithElements getElements(UUID parentId);

    void clear();

    void addGraph(Graph graph);

    void addGraph(Graph graph, CallBack<GraphView> callBack);

    GVVertex addVertex(GVVertex vertex);

    GVEdge addEdge(GVEdge edge);

    void removeVertex(UUID vertexId);

    void collapse(List<GVVertex> vertices);

    void uncollapse(GVVertex fragment);

    void modifyElements(List<GVVertex> vertices, List<GVEdge> edges);

    void addListener(GVListener listener);

    int getFont();

    void setFont(int fontSize);

    void showGrid(int gridType);

    void showMinimap(boolean value);

    void updateElementsSize();

    JComponent getView();

    void refreshView();

    void zoomIn();

    void zoomOut();

    void saveToImage(String fileName) throws IOException;

    void lock(String text);

    void unlock();

    void bfs(GraphViewHandler graphViewHandler);

    void dfs(GraphViewHandler graphViewHandler);

    void setName(String name);

    void setInfo(String info);

    void setShowAttributeNames(boolean value);

    boolean isShowAttributeNames();

    abstract class GraphViewHandler {
        public void onParentWithElements(GVParentWithElements parentWithElements) { }
    }

    class GVListener {
        public void onSelectElements(List<GVVertex> vertices, List<GVEdge> edges) { }

        public void onAddElements(List<GVVertex> vertices, List<GVEdge> edges) { }

        public void onClear() { }
    }
}
