package vg.lib.view.shapes.vshape;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Map;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.model.graph.Attribute;

public class GraphViewLeftFragmentShape extends GraphViewRectangleShape {
    public GraphViewLeftFragmentShape(Point position, Point size, Color color) {
        super(LEFT_FRAGMENT_VERTEX_SHAPE, position, size, color);
    }

    public GraphViewLeftFragmentShape(GraphViewVertexShape shape) {
        super(LEFT_FRAGMENT_VERTEX_SHAPE, shape);
    }

    @Override
    public void calculateSize(Map<Attribute, MutableBoolean> attributes, int fontSize, boolean showAttributeNames) {
        getLabel().calculateSize(attributes, fontSize, showAttributeNames);

        var size = new Point(
                getLabel().getSize().x + getBorder().x * 2,
                getLabel().getSize().y + getBorder().y * 2);

        getLabel().getPosition().x = getBorder().x;
        getLabel().getPosition().y = getBorder().y;
        setSize(size);
    }

    @Override
    public void draw(Graphics2D g2, Rectangle viewportWithCapacity, boolean isSelect) {
        var pos = getAbsolutePosition();
        var size = getAbsoluteSize();
        var border = getAbsoluteBorder();

        super.draw(g2, viewportWithCapacity, isSelect);

        // draw border.
        if (isSelect) {
            g2.setColor(Color.RED);
        } else {
            g2.setColor(Color.BLACK);
        }

        var textSize = getLabel().getAbsoluteTextSize();

        int x = pos.x - viewportWithCapacity.x;
        int y = pos.y - viewportWithCapacity.y;

        g2.fillRect(x + border.x + textSize.x, y, border.x, size.y);
    }

    @Override
    public GraphViewLeftFragmentShape clone() {
        return new GraphViewLeftFragmentShape(this);
    }
}
