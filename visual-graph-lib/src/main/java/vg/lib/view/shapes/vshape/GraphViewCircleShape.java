package vg.lib.view.shapes.vshape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Map;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.view.GraphViewUtils;
import vg.lib.model.graph.Attribute;

public class GraphViewCircleShape extends GraphViewVertexShape {
    public GraphViewCircleShape(Point position, Point size, Color color) {
        super(CIRCLE_VERTEX_SHAPE, position, size, color);
    }

    public GraphViewCircleShape(GraphViewVertexShape shape) {
        super(CIRCLE_VERTEX_SHAPE, shape);
    }

    @Override
    public void calculateSize(Map<Attribute, MutableBoolean> attributes, int fontSize, boolean showAttributeNames) {
        getLabel().calculateSize(attributes, fontSize, showAttributeNames);

        int x = getLabel().getSize().x;
        int y = getLabel().getSize().y;

        double r = Math.sqrt((x / 2.0f) * (x / 2.0f) + (y / 2.0f) * (y / 2.0f));
        int roundR = (int)r;
        int d1 = 0;

        if (r > roundR) {
            d1 = 1;
        }

        var size = getSize();

        size.x = 2 * roundR + getBorder().x * 2 + d1 * 2;
        size.y = 2 * roundR + getBorder().y * 2 + d1 * 2;

        double dx = (size.x - x) / 2.0;
        double dy = (size.y - y) / 2.0;
        int roundDx = (int)dx;
        int roundDy = (int)dy;
        int d2x = 0;
        int d2y = 0;

        if (dx > roundDx) {
            d2x = 1;
        }
        if (dy > roundDy) {
            d2y = 1;
        }

        var labelPosition = getLabel().getPosition();
        labelPosition.x = roundDx + d2x;
        labelPosition.y = roundDy + d2y;
    }

    @Override
    public void draw(Graphics2D g2, Rectangle viewportWithCapacity, boolean isSelect) {
        var pos = getAbsolutePosition();
        var size = getAbsoluteSize();
        var border = getAbsoluteBorder();

        g2.setColor(getColor());
        g2.fillOval(
                pos.x - viewportWithCapacity.x,
                pos.y - viewportWithCapacity.y,
                size.x,
                size.y);

        // draw border, if need.
        if (border.x > 0 && border.y > 0) {
            if (isSelect) {
                g2.setColor(Color.RED);
            } else {
                g2.setColor(Color.BLACK);
            }

            int x = pos.x - viewportWithCapacity.x;
            int y = pos.y - viewportWithCapacity.y;

            g2.setStroke(new BasicStroke(border.x));
            g2.drawOval(x, y, size.x, size.y);
        }
    }

    @Override
    public void drawOnMinimap(Graphics2D g2, Point graphViewSize, Point minimapSize, boolean isSelect) {
        var pos = GraphViewUtils.toMinimapCoordinate(getAbsolutePosition(), minimapSize, graphViewSize);
        var size = GraphViewUtils.toMinimapCoordinate(getAbsoluteSize(), minimapSize, graphViewSize);

        g2.setStroke(new BasicStroke(1f));
        g2.setColor(getColor());
        if (isSelect) {
            g2.setColor(Color.RED);
        }

        if (size.x == 0) {
            size.x +=1;
        }
        if (size.y == 0) {
            size.y += 1;
        }

        g2.fillOval(pos.x, pos.y, size.x, size.y);

        g2.setColor(Color.BLACK);
        g2.drawOval(pos.x, pos.y, size.x, size.y);
    }

    @Override
    public GraphViewCircleShape clone() {
        return new GraphViewCircleShape(this);
    }
}
