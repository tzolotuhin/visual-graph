package vg.lib.view.shapes.vshape;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.view.GraphViewLabel;
import vg.lib.model.graph.Attribute;

public abstract class GraphViewVertexShape {
    public static final int LEFT_FRAGMENT_VERTEX_SHAPE = 1;
    public static final int RECTANGLE_VERTEX_SHAPE = 4;
    public static final int CIRCLE_VERTEX_SHAPE = 5;
    public static final int DIAMOND_VERTEX_SHAPE = 7;

    @Getter
    private final GraphViewLabel label;

    @Getter @Setter
    private Color color;

    @Getter @Setter
    private Point position;

    @Getter @Setter
    private Point absolutePosition;

    // content(text) + border.
    @Getter @Setter
    private Point size;

    @Getter @Setter
    private Point absoluteSize;

    @Getter @Setter
    private Point border;

    @Getter @Setter
    private Point absoluteBorder;

    @Getter
    private final int type;

    public GraphViewVertexShape(int type, Point position, Point size, Color color) {
        this.label = new GraphViewLabel();
        this.color = color;
        this.position = new Point();
        this.absolutePosition = new Point(position);
        this.size = new Point();
        this.absoluteSize = new Point(size);
        this.border = new Point(1, 1);
        this.absoluteBorder = new Point(1, 1);
        this.type = type;
    }

    public GraphViewVertexShape(int type, GraphViewVertexShape shape) {
        this.label = new GraphViewLabel(shape.label);
        this.color = shape.getColor();
        this.position = new Point(shape.getPosition());
        this.absolutePosition = new Point(shape.getAbsolutePosition());
        this.size = new Point(shape.getSize());
        this.absoluteSize = new Point(shape.getAbsoluteSize());
        this.border = new Point(shape.getBorder());
        this.absoluteBorder = new Point(shape.getAbsoluteBorder());
        this.type = type;
    }

    public void translate(Point delta) {
        position.translate(delta.x, delta.y);
    }

    public double getAbsoluteCenterX() {
        return getPosition().x + getSize().x / 2.0;
    }

    public double getAbsoluteCenterY() {
        return getPosition().y + getSize().y / 2.0;
    }

    public Point getCenterPosition() {
        int x = getPosition().x + getSize().x / 2;
        int y = getPosition().y + getSize().y / 2;

        return new Point(x, y);
    }

    public abstract void calculateSize(Map<Attribute, MutableBoolean> attributes, int fontSize, boolean showAttributeNames);

    public abstract void draw(Graphics2D g2, Rectangle viewportWithCapacity, boolean isSelect);

    public abstract void drawOnMinimap(Graphics2D g2, Point graphViewSize, Point minimapSize, boolean isSelect);

    public abstract GraphViewVertexShape clone();
}
