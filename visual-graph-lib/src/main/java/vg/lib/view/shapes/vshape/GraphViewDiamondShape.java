package vg.lib.view.shapes.vshape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Map;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.view.GraphViewUtils;
import vg.lib.model.graph.Attribute;

public class GraphViewDiamondShape extends GraphViewVertexShape {
    public GraphViewDiamondShape(Point position, Point size, Color color) {
        super(DIAMOND_VERTEX_SHAPE, position, size, color);
    }

    public GraphViewDiamondShape(GraphViewVertexShape shape) {
        super(DIAMOND_VERTEX_SHAPE, shape);
    }

    @Override
    public void calculateSize(Map<Attribute, MutableBoolean> attributes, int fontSize, boolean showAttributeNames) {
        getLabel().calculateSize(attributes, fontSize, showAttributeNames);

        int x = getLabel().getSize().x;
        int y = getLabel().getSize().y;

        double dy = (x / 2.0) * Math.tan(Math.PI / 6);
        double dx = (y / 2.0) * Math.tan(Math.PI / 3);

        int roundDx = (int)dx;
        int remainderDx = 0;

        int roundDy = (int)dy;
        int remainderDy = 0;

        if (dx > remainderDx) {
            remainderDx = 1;
        }

        if (dy > remainderDy) {
            remainderDy = 1;
        }

        var size = getSize();

        size.x = 2 * roundDx + x + getBorder().x * 2 + remainderDx * 2;
        size.y = 2 * roundDy + y + getBorder().y * 2 + remainderDy * 2;

        var labelPosition = getLabel().getPosition();
        labelPosition.x = getBorder().x + roundDx + remainderDx;
        labelPosition.y = getBorder().y + roundDy + remainderDy;
    }

    @Override
    public void draw(Graphics2D g2, Rectangle viewportWithCapacity, boolean isSelect) {
        var pos = getAbsolutePosition();
        var size = getAbsoluteSize();
        var border = getAbsoluteBorder();

        int x1 = pos.x - viewportWithCapacity.x;
        int x2 = pos.x - viewportWithCapacity.x + Math.round(size.x / 2.0f);
        int x3 = pos.x - viewportWithCapacity.x + size.x;

        int y1 = pos.y - viewportWithCapacity.y + Math.round(size.y / 2.0f);
        int y2 = pos.y - viewportWithCapacity.y;
        int y3 = pos.y - viewportWithCapacity.y + size.y;

        g2.setColor(getColor());
        g2.fillPolygon(
                new int[] {x1, x2, x3, x2},
                new int[] {y1, y2, y1, y3},
                4);

        // draw border, if need.
        if (border.x > 0 && border.y > 0) {
            if (isSelect) {
                g2.setColor(Color.RED);
            } else {
                g2.setColor(Color.BLACK);
            }

            g2.setStroke(new BasicStroke(border.x));
            g2.drawLine(x1, y1, x2, y2);
            g2.drawLine(x2, y2, x3, y1);
            g2.drawLine(x1, y1, x2, y3);
            g2.drawLine(x2, y3, x3, y1);
        }
    }

    @Override
    public void drawOnMinimap(Graphics2D g2, Point graphViewSize, Point minimapSize, boolean isSelect) {
        var pos = GraphViewUtils.toMinimapCoordinate(getAbsolutePosition(), minimapSize, graphViewSize);
        var size = GraphViewUtils.toMinimapCoordinate(getAbsoluteSize(), minimapSize, graphViewSize);

        g2.setStroke(new BasicStroke(1f));
        g2.setColor(getColor());
        if (isSelect) {
            g2.setColor(Color.RED);
        }

        int x1 = pos.x;
        int x2 = pos.x + Math.round(size.x / 2.0f);
        int x3 = pos.x + size.x;

        int y1 = pos.y + Math.round(size.y / 2.0f);
        int y2 = pos.y;
        int y3 = pos.y + size.y;

        if (x1 == x2) {
            x2 += 1;
        }

        if (y2 == y3) {
            y3 += 1;
        }

        g2.fillPolygon(
                new int[] {x1, x2, x3, x2},
                new int[] {y1, y2, y1, y3},
                4);

        g2.setColor(Color.BLACK);
        g2.drawPolygon(
                new int[] {x1, x2, x3, x2},
                new int[] {y1, y2, y1, y3},
                4);
    }

    @Override
    public GraphViewDiamondShape clone() {
        return new GraphViewDiamondShape(this);
    }
}
