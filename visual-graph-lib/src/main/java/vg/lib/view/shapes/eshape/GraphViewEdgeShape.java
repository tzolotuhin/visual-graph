package vg.lib.view.shapes.eshape;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.view.GraphViewLabel;
import vg.lib.model.graph.Attribute;

public abstract class GraphViewEdgeShape {
    public static final int DEFAULT_EDGE_SHAPE = 1;

    @Getter
    private final GraphViewLabel label;

    @Getter @Setter
    private Color color;

    @Getter @Setter
    private boolean directed;

    @Getter @Setter
    private boolean backward;

    @Getter @Setter
    private boolean partOfCompositeEdge;

    @Getter @Setter
    private boolean finishPartOfCompositeEdge;

    @Getter @Setter
    private List<Point> points;

    @Getter @Setter
    private List<Point> absolutePoints;

    @Getter @Setter
    private int arrowSize;

    @Getter @Setter
    private int absoluteArrowSize;

    @Getter
    private final int shapeType;

    public GraphViewEdgeShape(
            int shapeType,
            boolean directed,
            boolean partOfCompositeEdge,
            boolean finishPartOfCompositeEdge,
            Color color) {
        this.label = new GraphViewLabel();
        this.color = color;
        this.directed = directed;
        this.partOfCompositeEdge = partOfCompositeEdge;
        this.finishPartOfCompositeEdge = finishPartOfCompositeEdge;
        this.backward = false;
        this.points = new ArrayList<>();
        this.absolutePoints = new ArrayList<>();
        this.arrowSize = 14;
        this.absoluteArrowSize = 14;
        this.shapeType = shapeType;
    }

    public GraphViewEdgeShape(int shapeType, GraphViewEdgeShape shape) {
        this.label = new GraphViewLabel(shape.label);
        this.color = shape.getColor();
        this.directed = shape.isDirected();
        this.backward = shape.isBackward();
        this.partOfCompositeEdge = shape.isPartOfCompositeEdge();
        this.finishPartOfCompositeEdge = shape.isFinishPartOfCompositeEdge();
        this.points = new ArrayList<>(shape.getPoints());
        this.absolutePoints = new ArrayList<>(shape.getAbsolutePoints());
        this.arrowSize = shape.getArrowSize();
        this.absoluteArrowSize = shape.getAbsoluteArrowSize();
        this.shapeType = shapeType;
    }

    public void translate(Point delta) {
        points.forEach(p -> p.translate(delta.x, delta.y));
        label.translate(delta);
    }

    public Point calculateAveragePoint() {
        var averagePoint = new Point();
        var points = getPoints();

        if (points.size() < 2) {
            return averagePoint;
        }

        int centerIndex = points.size() / 2;
        var point1 = points.get(centerIndex);
        var point2 = points.get(centerIndex - 1);

        averagePoint.x = (point1.x + point2.x) / 2;
        averagePoint.y = (point1.y + point2.y) / 2;;

        return averagePoint;
    }

    public abstract void calculateSize(Map<Attribute, MutableBoolean> attributes, int fontSize, boolean optimizeSize);

    public abstract void draw(Graphics2D g2, Rectangle viewportWithCapacity, boolean isSelect);

    public abstract void drawOnMinimap(Graphics2D g2, Point graphViewSize, Point minimapSize, boolean isSelect);

    public abstract GraphViewEdgeShape clone();
}
