package vg.lib.view.shapes.vshape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Map;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.view.GraphViewUtils;
import vg.lib.model.graph.Attribute;

public class GraphViewRectangleShape extends GraphViewVertexShape {
    protected GraphViewRectangleShape(int type, Point position, Point size, Color color) {
        super(type, position, size, color);
    }

    public GraphViewRectangleShape(Point position, Point size, Color color) {
        super(RECTANGLE_VERTEX_SHAPE, position, size, color);
    }

    public GraphViewRectangleShape(int type, GraphViewVertexShape shape) {
        super(type, shape);
    }

    public GraphViewRectangleShape(GraphViewVertexShape shape) {
        super(RECTANGLE_VERTEX_SHAPE, shape);
    }

    @Override
    public void calculateSize(Map<Attribute, MutableBoolean> attributes, int fontSize, boolean showAttributeNames) {
        getLabel().calculateSize(attributes, fontSize, showAttributeNames);

        var size = new Point(
                getLabel().getSize().x + getBorder().x * 2,
                getLabel().getSize().y + getBorder().y * 2);

        getLabel().getPosition().x = getBorder().x;
        getLabel().getPosition().y = getBorder().y;
        setSize(size);
    }

    @Override
    public void draw(Graphics2D g2, Rectangle viewportWithCapacity, boolean isSelect) {
        var pos = getAbsolutePosition();
        var size = getAbsoluteSize();
        var border = getAbsoluteBorder();

        g2.setColor(getColor());
        g2.fillRect(
                pos.x - viewportWithCapacity.x,
                pos.y - viewportWithCapacity.y,
                size.x,
                size.y);

        // draw border, if need.
        if (border.x > 0 && border.y > 0) {
            if (isSelect) {
                g2.setColor(Color.RED);
            } else {
                g2.setColor(Color.BLACK);
            }

            GraphViewUtils.drawRectWithBorder(g2, pos, size, border, viewportWithCapacity);
        }
    }

    @Override
    public void drawOnMinimap(Graphics2D g2, Point graphViewSize, Point minimapSize, boolean isSelect) {
        var pos = GraphViewUtils.toMinimapCoordinate(getAbsolutePosition(), minimapSize, graphViewSize);
        var size = GraphViewUtils.toMinimapCoordinate(getAbsoluteSize(), minimapSize, graphViewSize);

        g2.setStroke(new BasicStroke(1f));
        g2.setColor(getColor());
        if (isSelect) {
            g2.setColor(Color.RED);
        }

        if (size.x == 0) {
            size.x +=1;
        }
        if (size.y == 0) {
            size.y += 1;
        }

        g2.fillRect(pos.x, pos.y, size.x, size.y);

        g2.setColor(Color.BLACK);
        g2.drawRect(pos.x, pos.y, size.x, size.y);
    }

    @Override
    public GraphViewRectangleShape clone() {
        return new GraphViewRectangleShape(this);
    }
}
