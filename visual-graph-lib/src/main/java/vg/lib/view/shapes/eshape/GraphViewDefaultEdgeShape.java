package vg.lib.view.shapes.eshape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.view.GraphViewUtils;
import vg.lib.model.graph.Attribute;

public class GraphViewDefaultEdgeShape extends GraphViewEdgeShape {
    public GraphViewDefaultEdgeShape(
            int type,
            boolean directed,
            boolean partOfCompositeEdge,
            boolean finishPartOfCompositeEdge, Color color) {
        super(type, directed, partOfCompositeEdge, finishPartOfCompositeEdge, color);
    }

    public GraphViewDefaultEdgeShape(GraphViewDefaultEdgeShape shape) {
        super(DEFAULT_EDGE_SHAPE, shape);
    }

    @Override
    public void calculateSize(Map<Attribute, MutableBoolean> attributes, int fontSize, boolean optimizeSize) {
        getLabel().calculateSize(attributes, fontSize, optimizeSize);
    }

    @Override
    public void draw(Graphics2D g2, Rectangle viewportWithCapacity, boolean isSelect) {
        if (isBackward()) {
            g2.setStroke(new BasicStroke(1.5f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0));
        } else {
            g2.setStroke(new BasicStroke(1f));
        }

        g2.setColor(getColor());

        if (isSelect) {
            g2.setColor(Color.RED);
        }

        List<Point> points = getAbsolutePoints();
        for (int i = 0; i < points.size() - 1; i++) {
            Point p1 = points.get(i);
            Point p2 = points.get(i + 1);

            // draw source point.
            if (i == 0 && isDirected()) {
                g2.fillRect(
                        p1.x - viewportWithCapacity.x - 2,
                        p1.y - viewportWithCapacity.y - 4,
                        5,
                        8);
            }

            if (!isDirected()
                    || i != points.size() - 2
                    || (isPartOfCompositeEdge() && !isFinishPartOfCompositeEdge())) {
                g2.drawLine(
                        p1.x - viewportWithCapacity.x,
                        p1.y - viewportWithCapacity.y,
                        p2.x - viewportWithCapacity.x,
                        p2.y - viewportWithCapacity.y);
            } else {
                drawArrow(
                        g2,
                        p1.x - viewportWithCapacity.x,
                        p1.y - viewportWithCapacity.y,
                        p2.x - viewportWithCapacity.x,
                        p2.y - viewportWithCapacity.y);
            }
        }
    }

    @Override
    public void drawOnMinimap(Graphics2D g2, Point graphViewSize, Point minimapSize, boolean isSelect) {
        g2.setStroke(new BasicStroke(1f));
        g2.setColor(getColor());
        if (isSelect) {
            g2.setColor(Color.RED);
        }

        List<Point> points = getAbsolutePoints();
        for (int i = 0; i < points.size() - 1; i++) {
            var p1 = GraphViewUtils.toMinimapCoordinate(points.get(i), minimapSize, graphViewSize);
            var p2 = GraphViewUtils.toMinimapCoordinate(points.get(i + 1), minimapSize, graphViewSize);

            g2.drawLine(p1.x, p1.y, p2.x, p2.y);
        }
    }

    @Override
    public GraphViewDefaultEdgeShape clone() {
        return new GraphViewDefaultEdgeShape(this);
    }

    private void drawArrow(Graphics2D g2, int x1, int y1, int x2, int y2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double angle = Math.atan2(dy, dx);
        int length = Math.toIntExact(Math.round(Math.sqrt(dx * dx + dy * dy)));

        int x = getAbsoluteArrowSize();
        int y = Math.toIntExact(Math.round(getAbsoluteArrowSize() * Math.tan(Math.PI / 7)));

        // keep old transform matrix.
        var oldAffineTransform = g2.getTransform();

        // rotate matrix.
        var at = AffineTransform.getTranslateInstance(x1, y1);
        at.concatenate(AffineTransform.getRotateInstance(angle));
        g2.transform(at);

        // draw horizontal arrow starting in (0, 0).
        g2.drawLine(0, 0, length, 0);
        g2.fillPolygon(
                new int[] {length, length - x, Math.round(length - x + x * 0.2f), length - x, length},
                new int[] {0, -y, 0, y, 0},
                5);

        // rollback transform matrix.
        g2.setTransform(oldAffineTransform);
    }
}
