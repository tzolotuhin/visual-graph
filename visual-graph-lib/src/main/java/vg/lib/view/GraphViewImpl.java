package vg.lib.view;

import ar.com.hjg.pngj.FilterType;
import ar.com.hjg.pngj.ImageInfo;
import ar.com.hjg.pngj.ImageLineHelper;
import ar.com.hjg.pngj.ImageLineInt;
import ar.com.hjg.pngj.PngWriter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.common.CallBack;
import vg.lib.common.VGUtils;
import vg.lib.model.graph.Attribute;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVParentWithElements;
import vg.lib.view.elements.GVVertex;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
public class GraphViewImpl implements GraphView {
    private final GraphViewModel graphViewModel;
    private final GraphViewPanel graphViewPanel;

    protected final List<GVListener> listeners;

    protected final UUID id = UUID.randomUUID();

    protected final Object generalMutex = new Object();

    public GraphViewImpl(@NonNull GraphViewModel graphViewModel, @NonNull GraphViewPanel graphViewPanel) {
        this.graphViewModel = graphViewModel;
        this.graphViewPanel = graphViewPanel;

        listeners = Lists.newArrayList();
    }

    protected GraphViewModel getGraphViewModel() {
        return graphViewModel;
    }

    protected GraphViewPanel getGraphViewPanel() {
        return graphViewPanel;
    }

    @Override
    public JPanel getView() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return graphViewPanel;
        }
    }

    @Override
    public UUID getId() {
        return id;
    }

    @Override
    public GVVertex getVertex(UUID vertexId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return graphViewModel.getVertices().stream().filter(x -> x.getId().equals(vertexId)).findFirst().orElseThrow(IllegalArgumentException::new);
        }
    }

    @Override
    public GVParentWithElements getElements(GVVertex parent) {
        return getElements(parent != null ? parent.getId() : null);
    }

    @Override
    public GVParentWithElements getElements(UUID parentId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            List<GVVertex> vertices = Lists.newArrayList();
            List<GVEdge> edges = Lists.newArrayList();

            graphViewModel.getVertices().forEach(x -> {
                if (Objects.equals(parentId, x.getParentId())) {
                    vertices.add(new GVVertex(x));
                }
            });

            graphViewModel.getEdges().forEach(x -> {
                if (Objects.equals(parentId, x.getParentId())) {
                    edges.add(new GVEdge(x));
                }
            });

            return new GVParentWithElements(parentId == null ? null : getVertex(parentId), vertices, edges);
        }
    }

    @Override
    public void clear() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            lock("Please wait, clear the graph view...");

            graphViewModel.clearElements();

            graphViewModel.updateSize();
            graphViewPanel.update();
            unlock();

            doNotifyOnClear();
        }
    }

    @Override
    public void addGraph(Graph graph) {
        addGraph(graph, null);
    }

    @Override
    public void addGraph(Graph graph, CallBack<GraphView> callBack) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            lock("Please wait, open new graph...");

            Map<Vertex, UUID> vertexToId = Maps.newHashMap();
            Map<UUID, UUID> vertexDbIdToVertexId = Maps.newHashMap();

            graph.bfs(new Graph.GraphSearchListener() {
                @Override
                public void onEdge(Edge edge) {
                    UUID srcId = vertexToId.get(edge.getSource());
                    UUID trgId = vertexToId.get(edge.getTarget());
                    UUID parentId = graphViewModel.getVertexById(srcId).getParentId();

                    UUID tmpSrcPortId = VGUtils.getSrcPortId(edge);
                    UUID srcPortId = tmpSrcPortId != null ? vertexDbIdToVertexId.get(tmpSrcPortId) : null;

                    UUID tmpTrgPortId = VGUtils.getTrgPortId(edge);
                    UUID trgPortId = tmpTrgPortId != null ? vertexDbIdToVertexId.get(tmpTrgPortId) : null;

                    Validate.notNull(srcId, "Source id should not be null.");
                    Validate.notNull(trgId, "Target id should not be null.");

                    addEdge(new GVEdge(srcId, trgId, srcPortId, trgPortId, parentId, edge.getAttributes()));
                }

                @Override
                public void onVertex(Vertex vertex, Vertex parent) {
                    UUID parentId = null;

                    if (parent != null) {
                        parentId = vertexToId.get(parent);
                        Validate.notNull(parentId, "Parent id should not be null.");
                    }

                    var gvVertex = buildVertex(parentId, vertex.getAttributes());

                    var vertexName = VGUtils.getVertexName(vertex.getAttributes());
                    Map<Attribute, MutableBoolean> attributes = Maps.newLinkedHashMap();
                    attributes.put(new Attribute(graphViewModel.getDefaultIdAttribute(), vertexName, true), new MutableBoolean(true));
                    attributes.putAll(gvVertex.getAttributes());

                    gvVertex.setAttributes(attributes);


                    gvVertex = addVertex(gvVertex);

                    vertexToId.put(vertex, gvVertex.getId());

                    var vertexId = VGUtils.getSystemVertexId(vertex);

                    if (vertexId != null) {
                        vertexDbIdToVertexId.put(vertexId, gvVertex.getId());
                    }
                }
            });

            graphViewModel.updateSize();
            graphViewPanel.update();
            unlock();

            doNotifyOnAddElements(
                    graphViewModel.getVertices().stream().map(GVVertex::new).collect(Collectors.toList()),
                    graphViewModel.getEdges().stream().map(GVEdge::new).collect(Collectors.toList()),
                    callBack);
        }
    }

    @Override
    public GVVertex addVertex(GVVertex vertex) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return graphViewModel.addVertex(vertex);
        }
    }

    @Override
    public void removeVertex(UUID vertexId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.removeVertex(vertexId);
        }
    }

    @Override
    public GVEdge addEdge(GVEdge edge) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return graphViewModel.addEdge(edge);
        }
    }

    @Override
    public void collapse(List<GVVertex> vertices) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        // do nothing if list of vertices is empty.
        if (vertices.isEmpty()) {
            return;
        }

        synchronized (generalMutex) {
            // obtain parent id.
            var parentId = vertices.iterator().next().getParentId();

            // check that all vertices has one parent id.
            if (vertices.stream().allMatch(x -> Objects.equals(x.getParentId(), parentId))) {
                addVertex(buildVertex(parentId, Collections.emptyList()));
                // TODO: Please implement...
            }
        }
    }

    @Override
    public void uncollapse(GVVertex fragment) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            // TODO: Please implement...
        }
    }

    @Override
    public void modifyElements(List<GVVertex> vertices, List<GVEdge> edges) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        if (vertices == null) {
            vertices = Collections.emptyList();
        }
        if (edges == null) {
            edges = Collections.emptyList();
        }

        log.debug(String.format("Method 'modifyElements' was called, amount of vertices: %d, amount of edges: %d.", vertices.size(), edges.size()));

        synchronized (generalMutex) {
            vertices.forEach(graphViewModel::modifyVertex);
            edges.forEach(graphViewModel::modifyEdge);

            graphViewModel.updateSize();
        }
    }

    @Override
    public void refreshView() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.updateSize();
            graphViewPanel.update();
        }
    }

    @Override
    public void zoomIn() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            var zoomLevel = graphViewModel.getZoomLevel();

            if (zoomLevel >= 8) {
                // do nothing.
                return;
            }

            lock("Please wait...");
            graphViewModel.setZoomLevel(zoomLevel * 2);
            graphViewPanel.update();
            unlock();
        }
    }

    @Override
    public void zoomOut() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            var zoomLevel = graphViewModel.getZoomLevel();

            if (zoomLevel <= 0.0625) {
                // do nothing.
                return;
            }

            lock("Please wait...");
            graphViewModel.setZoomLevel(zoomLevel / 2);
            graphViewPanel.update();
            unlock();
        }
    }

    @Override
    public void addListener(GVListener listener) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            listeners.add(listener);
        }
    }

    @Override
    public int getFont() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return graphViewModel.getFontSize();
        }
    }

    @Override
    public void setFont(int fontSize) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.setFontSize(fontSize);
        }
    }

    @Override
    public void showGrid(int gridType) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.setGridType(gridType);
        }
    }

    @Override
    public void showMinimap(boolean value) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.setVisibilityOfMinimap(value);
        }
    }

    @Override
    public void updateElementsSize() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.updateElementsSize();
        }
    }

    @Override
    public void lock(String text) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        // please override this method.
    }

    @Override
    public void unlock() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        // please override this method.
    }

    @Override
    public void setName(String name) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.setName(name);
        }
    }

    @Override
    public void setInfo(String info) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.setInfo(info);
        }
    }

    @Override
    public void setShowAttributeNames(boolean value) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            graphViewModel.setShowAttributeNames(value);
        }
    }

    @Override
    public boolean isShowAttributeNames() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return graphViewModel.isShowAttributeNames();
        }
    }

    @Override
    public void saveToImage(String fileName) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            int CHUNK_SIZE = 5000;

            int oldViewportCapacity = graphViewModel.getViewportCapacity();
            Rectangle oldViewport = graphViewModel.getViewport();
            boolean oldVisibilityOfMinimap = graphViewModel.getVisibilityOfMinimap();

            graphViewModel.setVisibilityOfMinimap(false);
            graphViewModel.setViewportCapacity(0);
            try {
                Point size = graphViewModel.getAbsoluteSize();

                ImageInfo imageInfo = new ImageInfo(size.x, size.y, 8, true);
                PngWriter pngWriter = new PngWriter(new File(fileName), imageInfo);
                pngWriter.setFilterType(FilterType.FILTER_NONE);
                pngWriter.setIdatMaxSize(0x10000 * 256);
                pngWriter.setCompLevel(0);

                for (int y = 0; y < size.y; y += CHUNK_SIZE) {
                    lock(String.format("Try to save image to file with name '%s'. Completed %s/%s...", fileName, y, size.y));

                    List<BufferedImage> images = Lists.newArrayList();
                    for (int x = 0; x <= size.x; x += CHUNK_SIZE) {
                        int width = CHUNK_SIZE;
                        if (size.x - x < CHUNK_SIZE) {
                            width = size.x - x;
                        }
                        int height = CHUNK_SIZE;
                        if (size.y - y < CHUNK_SIZE) {
                            height = size.y - y;
                        }

                        graphViewModel.setViewport(new Rectangle(x, y, width, height));
                        graphViewPanel.update();

                        images.add(graphViewPanel.getSharedImage());
                    }

                    for (int dy = 0; dy < images.get(0).getHeight(); dy++) {
                        int x = 0;
                        ImageLineInt imageLineInt = new ImageLineInt(imageInfo);
                        for (BufferedImage image : images) {
                            for (int dx = 0; dx < image.getWidth(); dx++) {
                                int[] pixel = image.getRaster().getPixel(dx, dy, new int[4]);
                                //int grayColor = Math.round(((pixel[0] / 255.0f) + (pixel[1] / 255.0f) + (pixel[2] / 255.0f)) * 255);
                                //ImageLineHelper.setPixelRGBA8(imageLineInt, x++, grayColor, grayColor, grayColor, pixel[3]);
                                ImageLineHelper.setPixelRGBA8(imageLineInt, x++, pixel[0], pixel[1], pixel[2], pixel[3]);
                            }
                        }
                        pngWriter.writeRow(imageLineInt, dy + y);
                    }
                }

                pngWriter.end();
            } finally {
                graphViewModel.setViewport(oldViewport);
                graphViewModel.setViewportCapacity(oldViewportCapacity);
                graphViewModel.setVisibilityOfMinimap(oldVisibilityOfMinimap);
                graphViewPanel.update();
                unlock();
            }
        }
    }

    @Override
    public void bfs(GraphViewHandler graphViewHandler) {
        var queue = new ArrayDeque<GVParentWithElements>();
        queue.add(getElements((UUID) null));
        while (!queue.isEmpty()) {
            var parentWithElements = queue.poll();
            for (var vertex : parentWithElements.getVertices()) {
                if (!vertex.isVertex()) {
                    queue.add(getElements(vertex));
                }

                // note: call getElements method again for copying the elements...
                graphViewHandler.onParentWithElements(getElements(parentWithElements.getParent()));
            }
        }
    }

    @Override
    public void dfs(GraphViewHandler graphViewHandler) {
        doDFS(graphViewHandler, null);
    }

    private void doDFS(GraphViewHandler graphViewHandler, GVVertex parent) {
        var parentWithElements = getElements(parent);
        for (var parentElement : parentWithElements.getVertices()) {
            if (!parentElement.isVertex()) {
                doDFS(graphViewHandler, parentElement);
            }
        }

        // note: call getElements method again for copying the elements...
        graphViewHandler.onParentWithElements(getElements(parent));
    }

    private GVVertex buildVertex(UUID parentId, List<Attribute> attributes) {
        Random random = new Random();

        Point pos = new Point(random.nextInt(1000), random.nextInt(1000));
        Point size = new Point(30 + random.nextInt(40), 30 + random.nextInt(40));

        return new GVVertex(parentId, pos, size, attributes);
    }

    private void doNotifyOnAddElements(List<GVVertex> vertices, List<GVEdge> edges, CallBack<GraphView> callBack) {
        listeners.forEach(it -> SwingUtilities.invokeLater(() -> it.onAddElements(vertices, edges)));
        if (callBack != null) {
            SwingUtilities.invokeLater(() -> callBack.callingBack(this));
        }
    }

    private void doNotifyOnClear() {
        listeners.forEach(it -> SwingUtilities.invokeLater(it::onClear));
    }
}
