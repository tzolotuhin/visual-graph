package vg.lib.view;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Collections;
import java.util.List;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVVertex;
import vg.lib.view.shapes.vshape.GraphViewLeftFragmentShape;
import vg.lib.view.shapes.vshape.GraphViewRectangleShape;
import vg.lib.view.shapes.vshape.GraphViewVertexShape;

public class GraphViewUtils {
    /**
     * Modifies the elements and size of the parent.
     */
    public static void calculateParentSize(
            int type,
            GVVertex parent,
            Point contentSize,
            List<GVVertex> vertices,
            List<GVEdge> edges) {
        if (parent == null) {
            // do nothing.
            return;
        }

        if (parent.isVertex()) {
            throw new IllegalArgumentException("Parent argument should be fragment or vertex with ports.");
        }

        if (vertices == null) {
            vertices = Collections.emptyList();
        }

        if (edges == null) {
            edges = Collections.emptyList();
        }

        // reset offset for text.
        var parentLabelPos = parent.getShape().getLabel().getPosition();
        var parentLabelSize = parent.getShape().getLabel().getSize();

        parentLabelPos.x = parent.getShape().getBorder().x;
        parentLabelPos.y = parent.getShape().getBorder().y;

        var size = new Point();

        // add start border size.
        size.x += parent.getShape().getBorder().x;
        size.y += parent.getShape().getBorder().y;

        if (type == GraphViewVertexShape.LEFT_FRAGMENT_VERTEX_SHAPE) {
            size.x += parentLabelSize.x;
            size.x += parent.getShape().getBorder().x;
        }

        // move vertices and edges.
        vertices.forEach(v -> v.getShape().translate(size));
        edges.forEach(e -> e.getShape().translate(size));

        // add content size.
        size.x += contentSize.x;
        size.y += contentSize.y;

        // add finish border size.
        size.x += parent.getShape().getBorder().x;
        size.y += parent.getShape().getBorder().y;

        parent.getShape().setPosition(new Point(0, 0));
        parent.getShape().setSize(size);

        parent.getShape().getLabel().setVisible(true);
        switch (type) {
            case GraphViewVertexShape.LEFT_FRAGMENT_VERTEX_SHAPE:
                parent.setShape(new GraphViewLeftFragmentShape(parent.getShape()));
                break;
            case GraphViewVertexShape.RECTANGLE_VERTEX_SHAPE:
            default:
                parent.setShape(new GraphViewRectangleShape(parent.getShape()));
                break;
        }
    }

    public static void drawRectWithBorder(
            Graphics2D g2,
            Point pos,
            Point size,
            Point border,
            Rectangle viewportWithCapacity) {
        int x = pos.x - viewportWithCapacity.x;
        int y = pos.y - viewportWithCapacity.y;

        g2.fillRect(x, y, border.x, size.y);
        g2.fillRect(x, y, size.x, border.y);
        g2.fillRect(x + size.x - border.x, y, border.x, size.y);
        g2.fillRect(x, y + size.y - border.y, size.x, border.y);
    }

    public static Point toMinimapCoordinate(Point pos, Point minimapSize, Point graphViewSize) {
        int x = Math.round((float)minimapSize.x * pos.x / graphViewSize.x);
        int y = Math.round((float)minimapSize.y * pos.y / graphViewSize.y);
        return new Point(x, y);
    }
}
