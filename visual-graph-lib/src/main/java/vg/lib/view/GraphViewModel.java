package vg.lib.view;

import com.google.common.collect.Lists;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.AbstractMap;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.common.VGUtils;
import vg.lib.config.VGConfigSettings;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVObject;
import vg.lib.view.elements.GVVertex;
import vg.lib.view.elements.GVSelectionFrame;

@Slf4j
public class GraphViewModel {
    // Constants
    private static final String DEFAULT_ID_ATTRIBUTE = "name";

    // TODO: необходимо пересмотреть весь механизм с фрагментами и обычными вершинами с портами...
    private static final List<String> DEFAULT_TITLE_ATTRIBUTES = Lists.newArrayList();

    static {
        DEFAULT_TITLE_ATTRIBUTES.add(DEFAULT_ID_ATTRIBUTE);
        DEFAULT_TITLE_ATTRIBUTES.add("type");
        DEFAULT_TITLE_ATTRIBUTES.add("name");
    }

    // Main data
    private int gridType = VGConfigSettings.UNDER_THE_GRAPH_GRID_TYPE;
    private final Point gridSize;

    private boolean minimapIsVisible = true;

    private final Point size;
    private Rectangle viewport;
    private int viewportCapacity;
    private float zoomLevel = 1.0f;
    private int fontSize;
    private String name;
    private String info;

    private boolean showAttributeNames = false;

    private final GVSelectionFrame selectionFrame;

    private final List<GVVertex> vertices;
    private final List<GVEdge> edges;
    private int levelSize = 0;

    // Data for the search bar.
    private String searchBarStr;
    private List<GVObject> searchBarResults = Lists.newArrayList();
    private int searchBarResultsIndex;
    private GVObject searchBarResultsObject;

    private boolean needUpdate = true;

    private final Object generalMutex = new Object();

    public GraphViewModel(String name) {
        vertices = Lists.newArrayList();
        edges = Lists.newArrayList();

        selectionFrame = new GVSelectionFrame();

        gridSize = VGConfigSettings.DEFAULT_GRID_SIZE;
        size = new Point();
        viewport = new Rectangle();
        setFontSize(8);
        setName(name);
    }

    public GVVertex addVertex(GVVertex vertex) {
        synchronized (generalMutex) {
            //generate new id.
            vertex.setId(UUID.randomUUID());

            // calculate level.
            int level = 0;
            if (vertex.getParentId() != null) {
                var parent = getVertexById(vertex.getParentId());
                level = parent.getLevel() + 1;

                // select type for parent...
                if (!vertex.isPort()) {
                    parent.setTypeIfNeed(GVVertex.FRAGMENT_TYPE);
                } else {
                    parent.setTypeIfNeed(GVVertex.VERTEX_WITH_PORTS_TYPE);
                }
            }
            if (level >= levelSize) {
                levelSize = level + 1;
            }
            vertex.setLevel(level);

            // calculate a size for the vertex.
            vertex.getShape().calculateSize(vertex.getAttributes(), fontSize, showAttributeNames);

            vertices.add(new GVVertex(vertex));

            needUpdate = true;

            return vertex;
        }
    }

    public void removeVertex(UUID vertexId) {
        synchronized (generalMutex) {
            var vertex = getVertexById(vertexId);

            vertices.remove(vertex);

            if (vertex.getParentId() != null) {
                var parent = getVertexById(vertex.getParentId());

                if (parent.isFragment()) {
                    if (vertices.stream().noneMatch(x -> Objects.equals(parent.getId(), x.getParentId()) && !x.isPort())) {
                        parent.setType(GVVertex.VERTEX_WITH_PORTS_TYPE);
                    }
                }
            }
        }
    }

    public GVEdge addEdge(GVEdge edge) {
        synchronized (generalMutex) {
            //generate new id.
            edge.setId(UUID.randomUUID());

            // check src id, trg id, src port id and trg port id.
            Validate.notNull(edge.getSrcId());
            Validate.notNull(edge.getTrgId());

            Validate.isTrue(vertices.stream().anyMatch(x -> x.getId().equals(edge.getSrcId())));
            Validate.isTrue(vertices.stream().anyMatch(x -> x.getId().equals(edge.getTrgId())));

            if (edge.getSrcPortId() != null) {
                Validate.isTrue(vertices.stream().anyMatch(x -> x.getId().equals(edge.getSrcPortId())));
            }
            if (edge.getTrgPortId() != null) {
                Validate.isTrue(vertices.stream().anyMatch(x -> x.getId().equals(edge.getTrgPortId())));
            }

            // calculate level.
            int level = 0;
            if (edge.getParentId() != null) {
                GVVertex parent = vertices.stream().filter(x -> x.getId().equals(edge.getParentId())).collect(Collectors.toList()).get(0);
                level = parent.getLevel() + 1;
            }
            if (level >= levelSize) {
                levelSize = level + 1;
            }
            edge.setLevel(level);

            // calculate a size for a label of the edge.
            edge.getShape().getLabel().calculateSize(edge.getAttributes(), fontSize, showAttributeNames);

            edges.add(new GVEdge(edge));

            needUpdate = true;

            return edge;
        }
    }

    public void clearElements() {
        synchronized (generalMutex) {
            vertices.clear();
            edges.clear();

            needUpdate = true;
        }
    }

    public void modifyVertex(GVVertex vertex) {
        synchronized (generalMutex) {
            var modifiedVertex = getVertexById(vertex.getId());

            modifiedVertex.setShape(vertex.getShape());

            vertex.getAttributes().forEach((attribute, mutableBoolean) -> {
                MutableBoolean value = modifiedVertex.getAttributes().get(attribute);
                if (value != null) {
                    value.setValue(mutableBoolean.booleanValue());
                }
            });

            needUpdate = true;
        }
    }

    public void modifyEdge(GVEdge edge) {
        synchronized (generalMutex) {
            var modifiedEdge = getEdgeById(edge.getId());

            edge.getAttributes().forEach((attribute, mutableBoolean) -> {
                MutableBoolean value = modifiedEdge.getAttributes().get(attribute);
                if (value != null) {
                    value.setValue(mutableBoolean.booleanValue());
                }
            });

            modifiedEdge.setShape(edge.getShape());

            needUpdate = true;
        }
    }

    public GVVertex getVertexById(UUID id) {
        synchronized (generalMutex) {
            return vertices.stream().filter(x -> x.getId().equals(id)).findFirst().orElseThrow(IllegalArgumentException::new);
        }
    }

    private GVEdge getEdgeById(UUID id) {
        synchronized (generalMutex) {
            return edges.stream().filter(x -> x.getId().equals(id)).collect(Collectors.toList()).get(0);
        }
    }

    public List<GVVertex> getVertices() {
        synchronized (generalMutex) {
            return Collections.unmodifiableList(vertices);
        }
    }

    public List<GVEdge> getEdges() {
        synchronized (generalMutex) {
            return Collections.unmodifiableList(edges);
        }
    }

    public Point getSize() {
        synchronized (generalMutex) {
            return new Point(size);
        }
    }

    public Point2D.Float getAbsoluteGridSize() {
        synchronized (generalMutex) {
            return new Point2D.Float(Math.round(gridSize.x * zoomLevel), Math.round(gridSize.y * zoomLevel));
        }
    }

    public Point getAbsoluteSize() {
        synchronized (generalMutex) {
            return new Point(Math.round(size.x * zoomLevel), Math.round(size.y * zoomLevel));
        }
    }

    public void updateSize() {
        synchronized (generalMutex) {
            var boundary = VGUtils.calcBoundary(vertices, edges);

            // TODO: + ~200 - is margin... need additional research for deleting it...
            size.x = boundary.getKey().x + boundary.getValue().x + 200;
            size.y = boundary.getKey().y + boundary.getValue().y + 200;
        }
    }

    public Rectangle getViewport() {
        synchronized (generalMutex) {
            return new Rectangle(viewport);
        }
    }

    public void setViewport(Rectangle viewport) {
        synchronized (generalMutex) {
            this.viewport = viewport;
        }
    }

    public void setGridType(int gridType) {
        synchronized (generalMutex) {
            log.debug(String.format("Set grid type %s.", gridType));

            needUpdate = true;

            this.gridType = gridType;
        }
    }

    public int getGridType() {
        synchronized (generalMutex) {
            return gridType;
        }
    }

    public void setVisibilityOfMinimap(boolean value) {
        synchronized (generalMutex) {
            log.debug(String.format("Set visibility of minimap %s.", value));

            needUpdate = true;

            this.minimapIsVisible = value;
        }
    }

    public boolean getVisibilityOfMinimap() {
        synchronized (generalMutex) {
            return minimapIsVisible;
        }
    }

    public float getZoomLevel() {
        synchronized (generalMutex) {
            return zoomLevel;
        }
    }

    public void setZoomLevel(float zoomLevel) {
        synchronized (generalMutex) {
            log.debug(String.format("Set zoom level %s.", zoomLevel));

            needUpdate = true;

            this.zoomLevel = zoomLevel;
        }
    }

    public String getName() {
        synchronized (generalMutex) {
            return name;
        }
    }

    public void setName(String name) {
        synchronized (generalMutex) {
            log.debug(String.format("Set name '%s'.", name));

            this.name = name;
        }
    }

    public String getInfo() {
        synchronized (generalMutex) {
            return info;
        }
    }

    public void setInfo(String info) {
        synchronized (generalMutex) {
            log.debug("Set info '{}'.", info);

            this.info = info;
        }
    }

    public void setShowAttributeNames(boolean value) {
        synchronized (generalMutex) {
            log.debug(String.format("Set show attribute names (%s).", value));

            this.showAttributeNames = value;

            updateElementsSize();
        }
    }

    public boolean isShowAttributeNames() {
        synchronized (generalMutex) {
            return showAttributeNames;
        }
    }

    public void setFontSize(int fontSize) {
        synchronized (generalMutex) {
            log.debug(String.format("Set font size %s.", fontSize));

            this.fontSize = fontSize;

            updateElementsSize();
        }
    }

    public int getFontSize() {
        synchronized (generalMutex) {
            return fontSize;
        }
    }

    public void updateElementsSize() {
        synchronized (generalMutex) {
            log.debug("Update sizes for the elements (vertices and edges).");

            needUpdate = true;

            int vertexFontSize = fontSize;
            int edgeFontSize = Math.round(fontSize * 0.7f);

            vertices.forEach(vertex -> vertex.getShape().calculateSize(vertex.getAttributes(), vertexFontSize, showAttributeNames));
            edges.forEach(edge -> edge.getShape().calculateSize(edge.getAttributes(), edgeFontSize, showAttributeNames));
        }
    }

    public void setViewportCapacity(int viewportCapacity) {
        synchronized (generalMutex) {
            this.viewportCapacity = viewportCapacity;
        }
    }

    public int getViewportCapacity() {
        synchronized (generalMutex) {
            return viewportCapacity;
        }
    }

    private Point getVertexAbsolutePosition(UUID id) {
        var pos = getVertexAbsolutePositionWithoutRound(id);
        return new Point(Math.round(pos.x), Math.round(pos.y));
    }

    private Point2D.Float getVertexAbsolutePositionWithoutRound(UUID id) {
        synchronized (generalMutex) {
            var vertex = getVertexById(id);

            Point position = new Point(vertex.getShape().getPosition());

            while (vertex.getParentId() != null) {
                vertex = getVertexById(vertex.getParentId());
                position.setLocation(position.x + vertex.getShape().getPosition().x, position.y + vertex.getShape().getPosition().y);
            }

            return new Point2D.Float(position.x * zoomLevel, position.y * zoomLevel);
        }
    }

    private List<Point> getEdgeAbsolutePoints(UUID id) {
        synchronized (generalMutex) {
            var edge = getEdgeById(id);

            Point2D.Float parentPos;
            if (edge.getParentId() != null) {
                parentPos = getVertexAbsolutePositionWithoutRound(edge.getParentId());
            } else {
                parentPos = new Point2D.Float();
            }

            List<Point> points = Lists.newArrayList();
            edge.getShape().getPoints().forEach(x -> points.add(new Point(Math.round(x.x * zoomLevel + parentPos.x), Math.round(x.y * zoomLevel + parentPos.y))));

            return points;
        }
    }

    private Point calculateRoundAbsolutePoint(Point point, UUID parentId) {
        var absolutePoint = calculateAbsolutePoint(point, parentId);

        return new Point(Math.round(absolutePoint.x), Math.round(absolutePoint.y));
    }

    private Point2D.Float calculateAbsolutePoint(Point point, UUID parentId) {
        synchronized (generalMutex) {
            Point2D.Float parentPos;
            if (parentId != null) {
                parentPos = getVertexAbsolutePositionWithoutRound(parentId);
            } else {
                parentPos = new Point2D.Float();
            }

            return new Point2D.Float(point.x * zoomLevel + parentPos.x, point.y * zoomLevel + parentPos.y);
        }
    }

    private static int getAbsoluteSize(int originalSize, float zoomLevel) {
        return Math.round(originalSize * zoomLevel);
    }

    public int getLevelSize() {
        synchronized (generalMutex) {
            return levelSize;
        }
    }

    public void calculate() {
        synchronized (generalMutex) {
            if (!needUpdate) {
                return;
            }

            log.debug("Calculate absolute data.");

            for (int level = 0; level < levelSize; level++) {
                int finalLevel = level;
                vertices.forEach(x -> {
                    if (x.getLevel() == finalLevel) {
                        x.getShape().setAbsolutePosition(getVertexAbsolutePosition(x.getId()));
                        x.getShape().getAbsoluteSize().setLocation(
                                getAbsoluteSize(x.getShape().getSize().x, zoomLevel),
                                getAbsoluteSize(x.getShape().getSize().y, zoomLevel));
                        x.getShape().getAbsoluteBorder().setLocation(
                                getAbsoluteSize(x.getShape().getBorder().x, zoomLevel),
                                getAbsoluteSize(x.getShape().getBorder().y, zoomLevel));

                        var l = x.getShape().getLabel();
                        l.getAbsolutePosition().setLocation(
                                getAbsoluteSize(l.getPosition().x, zoomLevel),
                                getAbsoluteSize(l.getPosition().y, zoomLevel));
                        l.getAbsoluteSize().setLocation(
                                getAbsoluteSize(l.getSize().x, zoomLevel),
                                getAbsoluteSize(l.getSize().y, zoomLevel));
                        l.getAbsoluteTextSize().setLocation(
                                getAbsoluteSize(l.getTextSize().x, zoomLevel),
                                getAbsoluteSize(l.getTextSize().y, zoomLevel));
                        l.getAbsoluteBorder().setLocation(
                                getAbsoluteSize(l.getBorder().x, zoomLevel),
                                getAbsoluteSize(l.getBorder().y, zoomLevel));
                        l.setAbsoluteFontSize(getAbsoluteSize(l.getFontSize(), zoomLevel));

                        // border should not be 0.
                        if (x.getShape().getAbsoluteBorder().x == 0) {
                            x.getShape().getAbsoluteBorder().x = 1;
                        }
                        if (x.getShape().getAbsoluteBorder().y == 0) {
                            x.getShape().getAbsoluteBorder().y = 1;
                        }
                    }
                });

                edges.forEach(x -> {
                    if (x.getLevel() == finalLevel) {
                        x.getShape().setAbsolutePoints(getEdgeAbsolutePoints(x.getId()));

                        x.getShape().setAbsoluteArrowSize(getAbsoluteSize(x.getShape().getArrowSize(), zoomLevel));

                        var l = x.getShape().getLabel();
                        l.setAbsolutePosition(
                                calculateRoundAbsolutePoint(l.getPosition(), x.getParentId()));
                        l.getAbsoluteSize().setLocation(
                                getAbsoluteSize(l.getSize().x, zoomLevel),
                                getAbsoluteSize(l.getSize().y, zoomLevel));
                        l.getAbsoluteTextSize().setLocation(
                                getAbsoluteSize(l.getTextSize().x, zoomLevel),
                                getAbsoluteSize(l.getTextSize().y, zoomLevel));
                        l.getAbsoluteBorder().setLocation(
                                getAbsoluteSize(l.getBorder().x, zoomLevel),
                                getAbsoluteSize(l.getBorder().y, zoomLevel));
                        l.setAbsoluteFontSize(getAbsoluteSize(l.getFontSize(), zoomLevel));
                    }
                });
            }

            log.debug("Calculate absolute data - Done.");

            needUpdate = false;
        }
    }

    public GVSelectionFrame getSelectionFrame() {
        synchronized (generalMutex) {
            return selectionFrame;
        }
    }

    public void onSelectStart(Point start) {
        synchronized (generalMutex) {
            selectionFrame.setStart(start);
            selectionFrame.setFinish(start);
            selectionFrame.setVisible(true);
            selectionFrame.setSelectedVertices(getSelectedVertices());
            selectionFrame.setSelectedEdges(getSelectedEdges());
        }
    }

    public void onSelectFinish(Point finish) {
        synchronized (generalMutex) {
            selectionFrame.setFinish(finish);
            selectionFrame.setVisible(false);
        }
    }

    public void onSelect(Point point) {
        synchronized (generalMutex) {
            selectionFrame.setFinish(point);
        }
    }

    public Map.Entry<List<GVVertex>, List<GVEdge>> onSelect(Point point,
                                                            boolean addToSelect,
                                                            boolean hierarchy) {
        synchronized (generalMutex) {
            selectionFrame.setFinish(point);

            var selectionFrameRect = selectionFrame.getSelectionFrameRect();
            var selectedVertices = selectionFrame.getSelectedVertices();
            var selectedEdges = selectionFrame.getSelectedEdges();

            // fix the problem with the intersection of rectangles in case its width (or height) equals 0.
            selectionFrameRect.x -= 2;
            selectionFrameRect.y -= 2;
            selectionFrameRect.width += 4;
            selectionFrameRect.height += 4;

            // reset state 'selected' for all elements.
            vertices.forEach(vertex -> vertex.setSelect(false));
            edges.forEach(edge -> edge.setSelect(false));

            // start working...
            MutableBoolean stopWorking = new MutableBoolean();
            for (int level = levelSize - 1; level >= 0; level--) {
                int finalLevel = level;

                // if the selection frame is contained by some vertex (or fragment) needs to break the process
                // because otherwise, the process may select parents of this vertex (or fragment).
                if (stopWorking.isFalse()) {
                    vertices.forEach(x -> {
                        if (x.getLevel() == finalLevel) {
                            Point pos = getVertexAbsolutePosition(x.getId());
                            Point size = new Point(
                                    getAbsoluteSize(x.getShape().getSize().x, zoomLevel),
                                    getAbsoluteSize(x.getShape().getSize().y, zoomLevel));

                            var vertexRect = new Rectangle(pos, new Dimension(size.x, size.y));

                            if (vertexRect.contains(selectionFrameRect)) {
                                stopWorking.setTrue();
                            }

                            if (vertexRect.intersects(selectionFrameRect)) {
                                if (hierarchy) {
                                    selectHierarchy(x, true);
                                } else {
                                    x.setSelect(true);
                                }
                            }
                        }
                    });
                }

                edges.forEach(x -> {
                    if (x.getLevel() == finalLevel) {
                        for (int i = 0; i < x.getShape().getAbsolutePoints().size() - 1; i++) {
                            Point p1 = x.getShape().getAbsolutePoints().get(i);
                            Point p2 = x.getShape().getAbsolutePoints().get(i + 1);

                            Line2D line2D = new Line2D.Float(p1, p2);

                            if (line2D.intersects(selectionFrameRect)) {
                                x.setSelect(true);

                                // handle composite edges.
                                if (x.getShape().isPartOfCompositeEdge()) {
                                    edges.forEach(y -> {
                                        if (y.getShape().isPartOfCompositeEdge() && y.getMasterIdOfCompositeEdge() == x.getMasterIdOfCompositeEdge()) {
                                            y.setSelect(x.isSelect());
                                        }
                                    });
                                }
                            }
                        }
                    }
                });
            }

            // merge the result with backup selected elements.
            List<GVVertex> resultVertices = Lists.newArrayList();
            List<GVEdge> resultEdges = Lists.newArrayList();
            vertices.forEach(vertex -> {
                if (addToSelect) {
                    if (vertex.isSelect() && selectedVertices.contains(vertex)) {
                        vertex.setSelect(false);
                    } else if (!vertex.isSelect() && selectedVertices.contains(vertex)) {
                        vertex.setSelect(true);
                        resultVertices.add(vertex);
                    } else if (vertex.isSelect()) {
                        resultVertices.add(vertex);
                    }
                } else {
                    if (vertex.isSelect()) {
                        resultVertices.add(vertex);
                    }
                }
            });

            edges.forEach(edge -> {
                if (addToSelect) {
                    if (edge.isSelect() && selectedEdges.contains(edge)) {
                        edge.setSelect(false);
                    } else if (!edge.isSelect() && selectedEdges.contains(edge)) {
                        edge.setSelect(true);
                        resultEdges.add(edge);
                    } else if (edge.isSelect()) {
                        resultEdges.add(edge);
                    }
                } else {
                    if (edge.isSelect()) {
                        resultEdges.add(edge);
                    }
                }
            });

            return new AbstractMap.SimpleEntry<>(resultVertices, resultEdges);
        }
    }

    public Map.Entry<List<GVVertex>, List<GVEdge>> selectAllElements() {
        synchronized (generalMutex) {
            List<GVVertex> selectedVertices = Lists.newArrayList();
            List<GVEdge> selectedEdges = Lists.newArrayList();

            vertices.forEach(x -> {
                x.setSelect(true);
                selectedVertices.add(x);
            });
            edges.forEach(x -> {
                x.setSelect(true);
                selectedEdges.add(x);
            });

            return new AbstractMap.SimpleEntry<>(selectedVertices, selectedEdges);
        }
    }

    private boolean doSearchSubStrInText(String text,
                                         String subStr,
                                         boolean matchCase,
                                         boolean words,
                                         boolean isRegex) {
        boolean check;
        if (StringUtils.isEmpty(subStr)) {
            check = false;
        } else if (isRegex) {
            Pattern regex = Pattern.compile(subStr, Pattern.DOTALL);
            check = regex.matcher(text).find();
        } else {
            int flags = Pattern.DOTALL;
            if (!matchCase) {
                flags |= Pattern.CASE_INSENSITIVE;
            }
            if (words) {
                flags |= Pattern.LITERAL;
            }
            Pattern regex = Pattern.compile(subStr, flags);
            check = regex.matcher(text).find();
        }

        return check;
    }

    public Map.Entry<List<GVVertex>, List<GVEdge>> searchElements(String searchBarStr,
                                                                  boolean searchAmongVertices,
                                                                  boolean searchAmongEdges,
                                                                  boolean matchCase,
                                                                  boolean words,
                                                                  boolean isRegex) {
        Validate.notNull(searchBarStr);

        synchronized (generalMutex) {
            this.searchBarStr = searchBarStr;
            searchBarResults = Lists.newArrayList();
            searchBarResultsIndex = -1;
            searchBarResultsObject = null;

            List<GVVertex> selectedVertices = Lists.newArrayList();
            List<GVEdge> selectedEdges = Lists.newArrayList();

            vertices.forEach(vertex -> {
                boolean check = doSearchSubStrInText(
                        vertex.getShape().getLabel().getText(),
                        searchBarStr,
                        matchCase,
                        words,
                        isRegex);

                if (searchAmongVertices && check) {
                    vertex.setSelect(true);
                    searchBarResults.add(vertex);
                    selectedVertices.add(new GVVertex(vertex));
                } else {
                    vertex.setSelect(false);
                }
            });

            edges.forEach(edge -> {
                boolean check = doSearchSubStrInText(
                        edge.getShape().getLabel().getText(),
                        searchBarStr,
                        matchCase,
                        words,
                        isRegex);

                if (searchAmongEdges && check) {
                    edge.setSelect(true);
                    searchBarResults.add(edge);
                    selectedEdges.add(new GVEdge(edge));
                } else {
                    edge.setSelect(false);
                }
            });

            return new AbstractMap.SimpleEntry<>(selectedVertices, selectedEdges);
        }
    }

    public GVObject nextSearchBarResult() {
        synchronized (generalMutex) {
            if (searchBarResults.size() == 0) {
                searchBarResultsIndex = -1;
                searchBarResultsObject = null;
                return null;
            }

            searchBarResultsIndex++;
            if (searchBarResultsIndex >= searchBarResults.size()) {
                searchBarResultsIndex = 0;
            }

            return searchBarResultsObject = searchBarResults.get(searchBarResultsIndex);
        }
    }

    public GVObject prevSearchBarResult() {
        synchronized (generalMutex) {
            if (searchBarResults.size() == 0) {
                searchBarResultsIndex = -1;
                return null;
            }

            searchBarResultsIndex--;
            if (searchBarResultsIndex < 0) {
                searchBarResultsIndex = searchBarResults.size() - 1;
            }

            return searchBarResultsObject = searchBarResults.get(searchBarResultsIndex);
        }
    }

    public String getSearchBarStr() {
        synchronized (generalMutex) {
            return searchBarStr;
        }
    }

    public int getSearchBarResultsIndex() {
        synchronized (generalMutex) {
            return searchBarResultsIndex;
        }
    }

    public GVObject getSearchBarResultsObject() {
        synchronized (generalMutex) {
            return searchBarResultsObject;
        }
    }

    public List<GVObject> getSearchBarResults() {
        synchronized (generalMutex) {
            return Collections.unmodifiableList(searchBarResults);
        }
    }

    public List<GVVertex> getElementsInOneParent(List<GVVertex> vertices) {
        synchronized (generalMutex) {
            List<GVVertex> resultV = Lists.newArrayList();

            resultV.addAll(vertices);

            return resultV;
        }
    }

    public List<GVEdge> getEdges(List<GVVertex> vertices) {
        synchronized (generalMutex) {
            List<GVEdge> resultE = Lists.newArrayList();

            edges.forEach(x -> {
                boolean src = false;
                boolean trg = false;
                for (var vertex : vertices) {
                    if (x.getSrcId().equals(vertex.getId())) {
                        src = true;
                    }
                    if (x.getTrgId().equals(vertex.getId())) {
                        trg = true;
                    }
                }
                if (src && trg) {
                    resultE.add(x);
                }
            });

            return resultE;
        }
    }

    public List<GVVertex> getSelectedVertices() {
        synchronized (generalMutex) {
            List<GVVertex> selectedVertices = Lists.newArrayList();

            vertices.forEach(vertex -> {
                if (vertex.isSelect()) {
                    selectedVertices.add(vertex);
                }
            });

            return selectedVertices;
        }
    }

    public List<GVEdge> getSelectedEdges() {
        synchronized (generalMutex) {
            List<GVEdge> selectedEdges = Lists.newArrayList();

            edges.forEach(edge -> {
                if (edge.isSelect()) {
                    selectedEdges.add(edge);
                }
            });

            return selectedEdges;
        }
    }

    public String getDefaultIdAttribute() {
        return DEFAULT_ID_ATTRIBUTE;
    }

    List<String> getDefaultTitleAttributes() {
        return DEFAULT_TITLE_ATTRIBUTES;
    }

    private void selectHierarchy(GVVertex vertex, boolean select) {
        vertex.setSelect(select);
        vertices.forEach(x -> {
            if (x.getParentId() != null && vertex.getId().equals(x.getParentId())) {
                x.setSelect(select);
                if (x.getType() == GVVertex.FRAGMENT_TYPE) {
                    selectHierarchy(x, select);
                }
            }
        });
        edges.forEach(x -> {
            if (x.getParentId() != null && vertex.getId().equals(x.getParentId())) {
                x.setSelect(select);
            }
        });
    }
}
