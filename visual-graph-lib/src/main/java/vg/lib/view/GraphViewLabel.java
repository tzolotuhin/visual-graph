package vg.lib.view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.common.VGUtils;
import vg.lib.config.VGConfigSettings;
import vg.lib.model.graph.Attribute;

@Getter
@Setter
@Slf4j
public class GraphViewLabel {
    private String text;

    private Point position;

    private Point absolutePosition;

    // content + border.
    private Point size;

    private Point absoluteSize;

    private Point textSize;

    private Point absoluteTextSize;

    private Point border;

    private Point absoluteBorder;

    private boolean visible;

    private int fontSize;

    private float absoluteFontSize;

    public GraphViewLabel() {
        text = "";

        position = new Point();
        absolutePosition = new Point();

        size = new Point();
        absoluteSize = new Point();

        textSize = new Point();
        absoluteTextSize = new Point();

        border = new Point();
        absoluteBorder = new Point();

        visible = true;

        fontSize = 1;
        absoluteFontSize = 1;
    }

    public GraphViewLabel(GraphViewLabel label) {
        text = label.getText();

        position = new Point(label.getPosition());
        absolutePosition = new Point(label.getAbsolutePosition());

        size = new Point(label.getSize());
        absoluteSize = new Point(label.getAbsoluteSize());

        textSize = new Point(label.getTextSize());
        absoluteTextSize = new Point(label.getAbsoluteTextSize());

        border = new Point(label.getBorder());
        absoluteBorder = new Point(label.getAbsoluteBorder());

        visible = label.isVisible();

        fontSize = label.getFontSize();
        absoluteFontSize = label.getAbsoluteFontSize();
    }

    public String getPlainText() {
        return text.replace("\r\n", "").replace("\n", "");
    }

    public void translate(Point delta) {
        position.translate(delta.x, delta.y);
    }

    public boolean isBorderVisible() {
        return absoluteBorder.x > 0 && absoluteBorder.y > 0;
    }

    public boolean isTextVisible() {
        return absoluteTextSize.x > 0 && absoluteTextSize.y > 0 && !text.isEmpty();
    }

    public void calculateSize(Map<Attribute, MutableBoolean> attributesToIsVisible, int fontSize, boolean showAttributeNames) {
        final String ATTRIBUTE_DELIMITER = ": ";

        // collect visible attributes.
        var visibleAttributes = VGUtils.findSortedVisibleAttributes(attributesToIsVisible);

        StringBuilder textBuilder = new StringBuilder();
        int rowSize, columnSize;

        // calculate column size and row size.
        int maxName = 0, maxNameWithAttributeDelimiter = 0;
        if (showAttributeNames) {
            maxName = visibleAttributes.stream()
                    .filter(attribute -> !VGUtils.isNodeIdAttribute(attribute))
                    .mapToInt(x -> x.getName().length())
                    .max()
                    .orElse(0);

            maxNameWithAttributeDelimiter = maxName + ATTRIBUTE_DELIMITER.length();
        }

        int maxValue = visibleAttributes.stream().mapToInt(x -> {
            if (x.getStringValue().contains("\n")) {
                String[] values = x.getStringValue().split("\n");
                return Arrays.stream(values).mapToInt(String::length).max().orElse(0);
            } else {
                return x.getStringValue().length();
            }
        }).max().orElse(0);

        rowSize = 0;
        columnSize = maxNameWithAttributeDelimiter + maxValue;

        // build text data.
        for (int i = 0; i < visibleAttributes.size(); i++) {
            var attribute = visibleAttributes.get(i);

            var isNodeIdAttribute = VGUtils.isNodeIdAttribute(attribute);

            if (showAttributeNames && !isNodeIdAttribute) {
                textBuilder.append(attribute.getName());
                textBuilder.append(ATTRIBUTE_DELIMITER);

                char[] nameSpace = new char[maxName - attribute.getName().length()];
                Arrays.fill(nameSpace, ' ');
                textBuilder.append(nameSpace);
            }

            if (isNodeIdAttribute) {
                char[] nameSpace = new char[(columnSize - attribute.getStringValue().length()) / 2];
                Arrays.fill(nameSpace, ' ');
                textBuilder.append(nameSpace);
            }

            if (!attribute.getStringValue().contains("\n")) {
                if (showAttributeNames && !isNodeIdAttribute) {
                    char[] valueSpace = new char[maxValue - attribute.getStringValue().length()];
                    Arrays.fill(valueSpace, ' ');
                    textBuilder.append(valueSpace);
                }
                textBuilder.append(attribute.getStringValue());
                rowSize++;
            } else {
                if (showAttributeNames) {
                    textBuilder.append("\n");
                }
                textBuilder.append(attribute.getStringValue());
                rowSize += attribute.getStringValue().split("\n").length + 1;
                if (showAttributeNames) {
                    rowSize++;
                }
            }

            if (i != visibleAttributes.size() - 1) {
                char[] blankSpace = new char[columnSize];
                Arrays.fill(blankSpace, '-');
                textBuilder.append("\n");
                textBuilder.append(blankSpace);
                textBuilder.append("\n");
                rowSize++;
            }
        }

        // handle case with rowSize == 0.
        if (rowSize == 0 && VGUtils.isPort(attributesToIsVisible.keySet())) {
            rowSize = 1;
            columnSize = 3;
        }

        // update label's fields...
        setText(textBuilder.toString());

        setFontSize(fontSize);

        setTextSize(new Point(
                Math.toIntExact(Math.round(columnSize * getFontSize() * 0.50)),
                Math.toIntExact(Math.round(rowSize * getFontSize()))));

        // TODO: need to fix +5, it should passed to the method as parameter of the label...
        getSize().x = 2 * getBorder().x + getTextSize().x + 5;
        getSize().y = 2 * getBorder().y + getTextSize().y + 5;
    }

    public void draw(Graphics2D g2, Rectangle viewportWithCapacity, Point parentTranslate) {
        if (!isVisible()) {
            // do nothing.
            return;
        }

        var labelPosition = getAbsolutePosition();
        var labelBorder = getAbsoluteBorder();
        var labelTextSize = getAbsoluteTextSize();
        var labelSize = getAbsoluteSize();
        var fontSize = getAbsoluteFontSize();

        if (labelSize.x > 0 && labelSize.y > 0) {
            int x = parentTranslate.x - viewportWithCapacity.x + labelPosition.x;
            int y = parentTranslate.y - viewportWithCapacity.y + labelPosition.y;

            if (isTextVisible()) {
                if (isBorderVisible()) {
                    g2.fillRect(x, y, labelBorder.x, labelSize.y);
                    g2.fillRect(x, y, labelSize.x, labelBorder.y);
                    g2.fillRect(x + labelSize.x - labelBorder.x, y, labelBorder.x, labelSize.y);
                    g2.fillRect(x, y + labelSize.y - labelBorder.y, labelSize.x, labelBorder.y);
                }

                g2.drawImage(
                        drawText(getText(), labelTextSize, fontSize),
                        x + labelBorder.x,
                        y + labelBorder.y,
                        labelTextSize.x,
                        labelTextSize.y,
                        null);
            }
        }
    }

    private static BufferedImage drawText(
            String text,
            Point expectedTextSize,
            float fontSize) {
        BufferedImage bufferedImage = new BufferedImage(expectedTextSize.x, expectedTextSize.y, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2 = (Graphics2D)bufferedImage.getGraphics();
        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g2.setRenderingHints(rh);

        // fill background for debug.
        if (VGConfigSettings.DIAGNOSTIC_MODE) {
            g2.setPaint(Color.CYAN);
            g2.fillRect(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight());
        }

        // set color for text.
        g2.setColor(Color.BLACK);

        // split text.
        String[] lines = text.split("\n");

        // find best font size.
        var entry = findBestFontSize(g2, lines, expectedTextSize, fontSize);
        var font = entry.getKey();
        var realTextSize = entry.getValue();
        int x = Math.toIntExact(Math.round((expectedTextSize.x - realTextSize.getX()) / 2.0f));
        int y = Math.toIntExact(Math.round((expectedTextSize.y - realTextSize.getY()) / 2.0f));

        g2.setFont(font);

        // draw text if size more than 5.
        if (font.getSize() > 5) {
            for (String line : lines) {
                y += g2.getFontMetrics().getAscent();

                g2.drawString(line, x, y);

                y -= g2.getFontMetrics().getAscent();
                y += g2.getFontMetrics().getHeight();
            }
        }

        return bufferedImage;
    }

    private static Map.Entry<Font, Point2D> findBestFontSize(
            Graphics2D g2,
            String[] lines,
            Point expectedTextSize,
            float fontSize) {
        // find max line.
        String maxLine = "";
        for (var line : lines) {
            if (line.length() > maxLine.length()) {
                maxLine = line;
            }
        }

        // find best font size.
        var font = VGConfigSettings.getFontResource(VGConfigSettings.GRAPH_VIEW_FONT).deriveFont(fontSize);

        boolean minCheck = false, plusCheck = false;
        while (fontSize > 1) {
            font = VGConfigSettings.getFontResource(VGConfigSettings.GRAPH_VIEW_FONT).deriveFont(fontSize);
            var metrics = g2.getFontMetrics(font);

            float h = lines.length * metrics.getHeight();
            float w = metrics.stringWidth(maxLine);

            if (expectedTextSize.x < w || expectedTextSize.y < h) {
                fontSize -= 0.1f;
                minCheck = true;
            } else {
                float yp = (expectedTextSize.y - h) / expectedTextSize.y;

                if (minCheck && plusCheck) {
                    return new AbstractMap.SimpleEntry<>(font, new Point2D.Float(w, h));
                }

                if (yp < 0.05) {
                    return new AbstractMap.SimpleEntry<>(font, new Point2D.Float(w, h));
                } else {
                    fontSize += 0.1f;
                    plusCheck = true;
                }
            }
        }

        return new AbstractMap.SimpleEntry<>(font, new Point2D.Float());
    }
}
