package vg.lib.common;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.awt.Color;
import java.awt.Point;
import java.lang.reflect.Field;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.config.VGConfigSettings;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVVertex;
import vg.lib.view.shapes.vshape.GraphViewVertexShape;
import vg.lib.model.graph.Attribute;
import vg.lib.model.graph.AttributedItem;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.lib.model.record.AttributeRecord;
import vg.lib.model.record.AttributeRecordType;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
@Slf4j
public class VGUtils {
    // Constants
    public static final int INPUT_PORT_TYPE = 0;
    public static final int OUTPUT_PORT_TYPE = 1;

    private static final int COMPOSITE_EDGE_MIDDLE_TYPE = 0;
    private static final int COMPOSITE_EDGE_START_TYPE = 1;
    private static final int COMPOSITE_EDGE_FINISH_TYPE = 2;

    private static final String ATTRIBUTE_NAME_AMOUNT_OF_ATTACHMENTS = "src_code_amount";
    private static final String ATTRIBUTE_NAME_PREFIX_FOR_SOURCE_CODE = "src_code_file_";

    // 0 - start, 0 - is master part of composite edge
    private static final String ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER = "comp_edge_order";
    private static final String ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID = "comp_edge_master_id";
    private static final String ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_TYPE = "comp_edge_type";

    private static final String ATTRIBUTE_NAME_FOR_SYSTEM_VERTEX_ID = "system_vertex_id";

    private static final String ATTRIBUTE_NAME_FOR_SYSTEM_SRC_PORT_ID = "system_src_port_id";
    private static final String ATTRIBUTE_NAME_FOR_SYSTEM_TRG_PORT_ID = "system_trg_port_id";

    private static final String ATTRIBUTE_NAME_FOR_SRC_PORT_DB_ID = "src_port_db_id";
    private static final String ATTRIBUTE_NAME_FOR_TRG_PORT_DB_ID = "trg_port_db_id";

    private static final String SYSTEM_DB_ID_ATTRIBUTE = "system_db_id";

    private static final String SYSTEM_FAKE_PORT_ATTRIBUTE = "is_fake_port";
    private static final String SYSTEM_PORT_TYPE_ATTRIBUTE = "port_type";

    private static final String ATTRIBUTE_NAME_FOR_COLOR = "color";

    private static final String ATTRIBUTE_NAME_FOR_VERTEX_SHAPE = "shape";

    // The following attributes will be created with visible = true.
    private static final String DIRECTED_EDGE_ATTRIBUTE = "directed_edge";
    private static final String ATTRIBUTE_NAME_FOR_INNER_GRAPH_ID = "inner_graph_name";
    private static final String ATTRIBUTE_NAME_FOR_NODE_ID = "node_id";
    private static final String ATTRIBUTE_NAME_FOR_EDGE_ID = "edge_id";

    public static boolean isPort(GVVertex vertex) {
        return vertex != null && vertex.getAttribute(SYSTEM_FAKE_PORT_ATTRIBUTE) != null;
    }

    public static boolean isPort(Vertex vertex) {
        return vertex != null && vertex.getAttribute(SYSTEM_FAKE_PORT_ATTRIBUTE) != null;
    }

    public static boolean isPort(Collection<Attribute> attributes) {
        return attributes != null
                && attributes.stream().filter(x -> x.getName().equals(SYSTEM_FAKE_PORT_ATTRIBUTE)).findFirst().orElse(null) != null;
    }

    public static boolean isFakePort(GVVertex vertex) {
        if (vertex == null) {
            return false;
        }

        Attribute attr = vertex.getAttribute(SYSTEM_FAKE_PORT_ATTRIBUTE);

        return attr != null && attr.getBooleanValue();
    }

    public static boolean isFakePort(Vertex vertex) {
        if (vertex == null) {
            return false;
        }

        Attribute attr = vertex.getAttribute(SYSTEM_FAKE_PORT_ATTRIBUTE);

        return attr != null && attr.getBooleanValue();
    }

    public static boolean isInputPort(Vertex vertex) {
        if (vertex == null) {
            return false;
        }

        Attribute attr = vertex.getAttribute(SYSTEM_PORT_TYPE_ATTRIBUTE);
        return attr != null && attr.getIntegerValue() == INPUT_PORT_TYPE;
    }

    public static boolean isInputPort(GVVertex vertex) {
        if (vertex == null) {
            return false;
        }

        Attribute attr = vertex.getAttribute(SYSTEM_PORT_TYPE_ATTRIBUTE);
        return attr != null && attr.getIntegerValue() == INPUT_PORT_TYPE;
    }

    public static boolean isOutputPort(Vertex vertex) {
        if (vertex == null) {
            return false;
        }

        Attribute attr = vertex.getAttribute(SYSTEM_PORT_TYPE_ATTRIBUTE);
        return attr != null && attr.getIntegerValue() == OUTPUT_PORT_TYPE;
    }

    public static boolean isOutputPort(GVVertex vertex) {
        if (vertex == null) {
            return false;
        }

        Attribute attr = vertex.getAttribute(SYSTEM_PORT_TYPE_ATTRIBUTE);
        return attr != null && attr.getIntegerValue() == OUTPUT_PORT_TYPE;
    }

    public static UUID getSystemVertexId(Vertex vertex) {
        var attribute = vertex.getAttribute(ATTRIBUTE_NAME_FOR_SYSTEM_VERTEX_ID);
        return attribute == null ? null : attribute.getUUIDValue();
    }

    /**
     * +----------+
     * |   NODE1  |
     * +-SRC PORT-+
     *      |
     *      V
     *     NODE2
     */
    public static int getSrcPortDbId(Edge edge) {
        var attribute = edge.getAttribute(ATTRIBUTE_NAME_FOR_SRC_PORT_DB_ID);
        return attribute == null ? -1 : attribute.getIntegerValue();
    }

    public static UUID getSrcPortId(Edge edge) {
        var attribute = edge.getAttribute(ATTRIBUTE_NAME_FOR_SYSTEM_SRC_PORT_ID);
        return attribute == null ? null : attribute.getUUIDValue();
    }

    public static void addSrcPortIdAttribute(Edge edge, UUID srcPortId) {
        edge.addAttribute(
                new Attribute(ATTRIBUTE_NAME_FOR_SYSTEM_SRC_PORT_ID, srcPortId, AttributeRecordType.UUID, false));
    }

    public static void resetSystemSrcPortIdAttribute(Edge edge, UUID srcPortId) {
        edge.removeAllAttributes(ATTRIBUTE_NAME_FOR_SYSTEM_SRC_PORT_ID);

        if (srcPortId != null) {
            addSrcPortIdAttribute(edge, srcPortId);
        }
    }

    /**
     *     NODE1
     *      |
     *      V
     * +-TRG PORT-+
     * |   NODE2  |
     * +----------+
     */
    public static int getTrgPortDbId(Edge edge) {
        Attribute attribute = edge.getAttribute(ATTRIBUTE_NAME_FOR_TRG_PORT_DB_ID);
        if (attribute == null)
            return -1;
        return attribute.getIntegerValue();
    }

    public static UUID getTrgPortId(Edge edge) {
        var attribute = edge.getAttribute(ATTRIBUTE_NAME_FOR_SYSTEM_TRG_PORT_ID);
        return attribute == null ? null : attribute.getUUIDValue();
    }

    public static void addTrgPortIdAttribute(Edge edge, UUID trgPortId) {
        edge.addAttribute(new Attribute(ATTRIBUTE_NAME_FOR_SYSTEM_TRG_PORT_ID, trgPortId, AttributeRecordType.UUID, false));
    }

    public static void resetSystemTrgPortIdAttribute(Edge edge, UUID trgPortId) {
        edge.removeAllAttributes(ATTRIBUTE_NAME_FOR_SYSTEM_TRG_PORT_ID);
        if (trgPortId != null) {
            addTrgPortIdAttribute(edge, trgPortId);
        }
    }

    public static int getTypeOfPartOfCompositeEdge(List<Attribute> attributes) {
        var attr = AttributedItem.getAttribute(ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_TYPE, attributes);

        if (attr == null) {
            return -1;
        }

        return attr.getIntegerValue();
    }

    public static boolean isFinishPartOfCompositeEdge(List<Attribute> attributes) {
        return getTypeOfPartOfCompositeEdge(attributes) == COMPOSITE_EDGE_FINISH_TYPE;
    }

    public static boolean isPartOfCompositeEdge(List<Attribute> attributes) {
        return getTypeOfPartOfCompositeEdge(attributes) >= 0;
    }

    /**
     * Returns master id of composite edge or -1 otherwise.
     */
    public static int getMasterIdOfCompositeEdge(List<Attribute> attributes) {
        var attr = AttributedItem.getAttribute(ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID, attributes);

        if (attr == null) {
            return -1;
        }

        return attr.getIntegerValue();
    }

    public static boolean isDirectedEdge(List<Attribute> attributes) {
        Attribute attr = AttributedItem.getAttribute(DIRECTED_EDGE_ATTRIBUTE, attributes);
        return attr != null && attr.isBooleanType() && attr.getBooleanValue();
    }

    public static String getVertexName(Collection<Attribute> attributes) {
        var elementName = getElementName("v", ATTRIBUTE_NAME_FOR_NODE_ID, attributes);

        var innerGraphIdAttr = AttributedItem.getAttribute(ATTRIBUTE_NAME_FOR_INNER_GRAPH_ID, attributes);
        if (innerGraphIdAttr != null && innerGraphIdAttr.isStringType()) {
            elementName = String.format("%s | %s", StringUtils.abbreviate(innerGraphIdAttr.getStringValue(), 128), elementName);
        }

        return elementName;
    }

    public static boolean isNodeIdAttribute(Attribute attribute) {
        if (attribute == null
                || attribute.getName() == null
                || !attribute.getName().equals(ATTRIBUTE_NAME_FOR_NODE_ID)) {
            return false;
        }

        return true;
    }

    public static String getEdgeName(Collection<Attribute> attributes) {
        return getElementName("e", ATTRIBUTE_NAME_FOR_EDGE_ID, attributes);
    }

    public static String getAttributeName(String name, String stringValue) {
        return StringUtils.abbreviate(String.format("%s : %s", name, stringValue), 128);
    }

    private static String getElementName(String prefix, String attributeNameForNode, Collection<Attribute> attributes) {
        String name = null;
        String dbId = null;

        var nameAttr = AttributedItem.getAttribute(attributeNameForNode, attributes);
        if (nameAttr != null && nameAttr.isStringType()) {
            name = StringUtils.abbreviate(nameAttr.getStringValue(), 128);
        }

        var dbIdAttr = AttributedItem.getAttribute(SYSTEM_DB_ID_ATTRIBUTE, attributes);
        if (dbIdAttr != null && dbIdAttr.isIntegerType()) {
            dbId = String.format("%s[%s]", prefix, dbIdAttr.getIntegerValue());
        }

        if (name != null && dbId != null) {
            return name + " " + dbId;
        }

        if (name != null) {
            return name;
        }

        if (dbId != null) {
            return dbId;
        }

        return "unknown";
    }

    public static List<Graph> findAllPaths(Graph graph, Vertex source, Vertex target, AtomicBoolean findProcessInterrupter) {
        List<List<Vertex>> paths = Lists.newArrayList();
        dfs(graph, source, target, paths, findProcessInterrupter);

        List<Graph> result = Lists.newArrayList();
        for (List<Vertex> path : paths) {
            List<Edge> edges = Lists.newArrayList();
            for (int i = 0; i < path.size() - 1; i++) {
                Vertex tmpSource = path.get(i);
                Vertex tmpTarget = path.get(i+1);
                edges.addAll(graph.findEdgesBetweenVertices(tmpSource, tmpTarget));
            }

            result.add(new Graph(path, edges));
        }

        return result;
    }

    public static List<Graph> findAllCycles(Graph graph, Vertex source, AtomicBoolean findProcessInterrupter) {
        List<List<Vertex>> paths = Lists.newArrayList();

        for (Vertex vertex : graph.getTrgNeighborhoods(source)) {
            List<List<Vertex>> tmpPaths = Lists.newArrayList();
            dfs(graph, vertex, source, tmpPaths, findProcessInterrupter);
            paths.addAll(tmpPaths);
        }

        List<Graph> result = Lists.newArrayList();
        for (List<Vertex> path : paths) {
            List<Edge> edges = Lists.newArrayList();
            for (int i = 0; i < path.size() - 1; i++) {
                Vertex tmpSource = path.get(i);
                Vertex tmpTarget = path.get(i+1);
                edges.addAll(graph.findEdgesBetweenVertices(tmpSource, tmpTarget));
            }
            if (path.size() > 1) {
                Vertex tmpSource = path.get(0);
                Vertex tmpTarget = path.get(path.size() - 1);
                edges.addAll(graph.findEdgesBetweenVertices(tmpTarget, tmpSource));
            }

            if (!(path.size() == 2 && edges.size() == 2 && edges.get(0) == edges.get(1))) {
                result.add(new Graph(path, edges));
            }
        }

        return result;
    }

    public static Map<Graph, Float> findCriticalPaths(Graph graph, Vertex source, Vertex target, String attributeName, AtomicBoolean findProcessInterrupter) {
        List<Graph> paths = findAllPaths(graph, source, target, findProcessInterrupter);

        Map<Graph, Float> result = Maps.newLinkedHashMap();
        for (Graph path : paths) {
            if (StringUtils.isEmpty(attributeName)) {
                result.put(path, (float)path.getAllEdges().size());
            } else {
                float count = 0;
                for (Edge edge : path.getAllEdges()) {
                    Attribute attribute = edge.getAttribute(attributeName);
                    if (attribute != null && attribute.castValueToReal() != null) {
                        count += attribute.castValueToReal();
                    }
                }

                result.put(path, count);
            }
        }

        return new MapUtils<Graph, Float>().sortMapByValue(result);
    }

    /**
     * Depth first search algorithm for finding all paths from source to target.
     */
    public static void dfs(Graph graph, Vertex source, Vertex target, List<List<Vertex>> paths, AtomicBoolean findProcessInterrupter) {
        Stack<Vertex> stack = new Stack<>();
        stack.add(source);

        Map<Vertex, Integer> path = Maps.newLinkedHashMap();
        while (!stack.isEmpty() && !findProcessInterrupter.get()) {
            //MainService.logger.printDebug("Stack size: " + stack.size() + ", path count: " + paths.size());
            Vertex curr = stack.pop();

            int count = 0;
            if (curr != target) {
                for (Vertex vertex : graph.getTrgNeighborhoods(curr)) {
                    if (!path.containsKey(vertex)) {
                        stack.add(vertex);
                        count++;
                    }
                }
            }

            path.put(curr, count);
            if (count == 0) {
                if (curr == target) {
                    paths.add(Lists.newArrayList(path.keySet()));
                }

                List<Vertex> reversePath = Lists.reverse(Lists.newArrayList(path.keySet()));
                for (Vertex vertex : reversePath) {
                    if (path.get(vertex) > 1) {
                        path.put(vertex, path.get(vertex) - 1);
                        break;
                    } else {
                        path.remove(vertex);
                    }
                }
            }
        }
    }

    /**
     * Returns min and max positions.
     *
     * TODO: возможно здесь есть потенциальные проблемы, когда мы вычисляем размер графа и parent...
     */
    public static Map.Entry<Point, Point> calcBoundary(List<GVVertex> vertices, List<GVEdge> edges) {
        int vpx = vertices.stream().mapToInt(x -> x.getShape().getPosition().x).min().orElse(0);
        int vpy = vertices.stream().mapToInt(x -> x.getShape().getPosition().y).min().orElse(0);

        int vsx = vertices.stream().mapToInt(x -> x.getShape().getPosition().x + x.getShape().getSize().x).max().orElse(0);
        int vsy = vertices.stream().mapToInt(x -> x.getShape().getPosition().y + x.getShape().getSize().y).max().orElse(0);

        int esx = edges.stream().mapToInt(x -> x.getShape().getPoints().stream().mapToInt(y -> y.x).max().orElse(0)).max().orElse(vsx);
        int esy = edges.stream().mapToInt(x -> x.getShape().getPoints().stream().mapToInt(y -> y.y).max().orElse(0)).max().orElse(vsy);

        int epx = edges.stream().mapToInt(x -> x.getShape().getPoints().stream().mapToInt(y -> y.x).max().orElse(0)).min().orElse(vpx);
        int epy = edges.stream().mapToInt(x -> x.getShape().getPoints().stream().mapToInt(y -> y.y).max().orElse(0)).min().orElse(vpy);

        return new AbstractMap.SimpleEntry<>(new Point(Math.min(vpx, epx), Math.min(vpy, epy)), new Point(Math.max(vsx, esx), Math.max(vsy, esy)));
    }

    public static int getShape(Collection<Attribute> attributes, int defaultShapeType) {
        var attribute = AttributedItem.getAttribute(ATTRIBUTE_NAME_FOR_VERTEX_SHAPE, attributes);

        if (attribute == null || !attribute.isStringType()) {
            return defaultShapeType;
        }

        if (attribute.getStringValue().toLowerCase().contains("diamond")) {
            return GraphViewVertexShape.DIAMOND_VERTEX_SHAPE;
        }

        if (attribute.getStringValue().toLowerCase().contains("circle")) {
            return GraphViewVertexShape.CIRCLE_VERTEX_SHAPE;
        }

        return defaultShapeType;
    }

    public static void resetSystemColorAttribute(AttributedItem item, String color) {
        item.removeAllAttributes(ATTRIBUTE_NAME_FOR_COLOR);

        if (!StringUtils.isEmpty(color)) {
            item.addAttribute(new Attribute(ATTRIBUTE_NAME_FOR_COLOR, color, AttributeRecordType.STRING, false));
        }
    }

    public static Color getColor(Collection<Attribute> attributes, Color defaultColor) {
        var attribute = AttributedItem.getAttribute(ATTRIBUTE_NAME_FOR_COLOR, attributes);

        if (attribute == null || !attribute.isStringType()) {
            return defaultColor;
        }

        var color = VGUtils.convertColorNameToColor(attribute.getStringValue(), VGConfigSettings.DEFAULT_VERTEX_COLOR);

        if (color.equals(Color.BLACK)) {
            color = defaultColor;
        }

        return color;
    }

    public static Attribute convertToAttribute(AttributeRecord attributeRecord) {
        Validate.notNull(attributeRecord);

        return new Attribute(attributeRecord.getName(), attributeRecord.getStringValue(), attributeRecord.getType(), attributeRecord.isVisible());
    }

    public static List<Attribute> convertToAttributes(List<AttributeRecord> attributeRecords) {
        Validate.notNull(attributeRecords);

        List<Attribute> result = Lists.newArrayList();
        for (AttributeRecord header : attributeRecords) {
            result.add(convertToAttribute(header));
        }

        return result;
    }

    /**
     * Converts a given string into a color.
     *
     * @param value
     *          the string, either a name or a hex-string.
     * @return the color.
     */
    public static Color convertColorNameToColor(String value, Color defaultColor) {
        if (value == null) {
            return defaultColor;
        }

        try {
            // get color by hex or octal value
            return Color.decode(value);
        } catch (NumberFormatException nfe) {
            // if we can't decode lets try to get it by name
            try {
                // try to get a color by name using reflection
                final Field f = Color.class.getField(value);

                return (Color) f.get(null);
            } catch (Exception ce) {
                // if we can't get any color return default color.
                return defaultColor;
            }
        }
    }

    /**
     * @return [node_id attribute if visible, other visible attributes]
     */
    public static List<Attribute> findSortedVisibleAttributes(Map<Attribute, MutableBoolean> attributesToIsVisible) {
        var result = new ArrayList<Attribute>();

        Attribute nodeIdAttribute = null;
        for (var attributeToIsVisible : attributesToIsVisible.entrySet()) {
            var attribute = attributeToIsVisible.getKey();
            var isVisible = attributeToIsVisible.getValue();

            if (!attribute.isVisible()) {
                continue;
            }

            if (!isVisible.getValue()) {
                continue;
            }

            if (VGUtils.isNodeIdAttribute(attribute)) {
                nodeIdAttribute = attribute;
                continue;
            }

            result.add(attribute);
        }

        if (nodeIdAttribute != null) {
            result.add(0, nodeIdAttribute);
        }

        return result;
    }
}
