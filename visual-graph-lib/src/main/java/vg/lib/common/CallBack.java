package vg.lib.common;

public interface CallBack<T> {
    void callingBack(T value);
}
