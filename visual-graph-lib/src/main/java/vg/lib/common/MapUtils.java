package vg.lib.common;

import com.google.common.collect.Maps;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class MapUtils<K, V extends Comparable> {
    public Map<K, V> sortMapByValue(Map<K, V> unorderedMap) {
        ValueComparator<K, V> bvc =  new ValueComparator<>(unorderedMap);
        TreeMap<K, V> orderedMap = Maps.newTreeMap(bvc);
        orderedMap.putAll(unorderedMap);
        return Maps.newLinkedHashMap(orderedMap);
    }

    public Map<K, V> getMinByValue(Map<K, V> unorderedMap) {
        Map<K, V> result = Maps.newLinkedHashMap();
        Map<K, V> sortedMap = sortMapByValue(unorderedMap);
        V curr = null;
        for (K k : sortedMap.keySet()) {
            V v = sortedMap.get(k);
            if (curr == null || v.compareTo(curr) == 0) {
                curr = v;
                result.put(k, v);
            } else {
                break;
            }
        }
        return result;
    }

    private class ValueComparator<K, V extends Comparable> implements Comparator<K> {
        private Map<K, V> base;
        public ValueComparator(Map<K, V> base) {
            this.base = base;
        }

        public int compare(K a, K b) {
            int re;
            if (base.get(a) == null && base.get(b) == null)
                re = 0;
            else if (base.get(a) == null)
                re =  1;
            else if (base.get(b) == null)
                re = -1;
            else
                re = base.get(a).compareTo(base.get(b));

            // note: we shouldn't merge results
            if (re == 0)
                return 1;

            return re;
        }
    }
}
