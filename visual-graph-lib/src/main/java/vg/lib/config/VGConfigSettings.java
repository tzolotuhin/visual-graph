package vg.lib.config;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

@Slf4j
public class VGConfigSettings {
    public static final int DISABLE_GRID_TYPE = 0;
    public static final int UNDER_THE_GRAPH_GRID_TYPE = 1;
    public static final int ABOVE_THE_GRAPH_GRID_TYPE = 2;

    public static final Color DEFAULT_VERTEX_COLOR = Color.decode("0xFFCC33");
    public static final Color DEFAULT_EDGE_COLOR = Color.decode("0x000000");

    public static final Color PORT_COLOR = new Color(252,251,227);
    public static final Color FAKE_PORT_COLOR = new Color(169,169,169);
    public static final Color[] COLORS = new Color[] {
        new Color(152, 251, 152),
        new Color(175, 238, 238),
        new Color(240, 230, 140),
        new Color(255, 182, 193)
    };

    public static final Color GRID_COLOR = new Color(189, 183, 107, 200);
    public static final Point DEFAULT_GRID_SIZE = new Point(20, 20);

    public static boolean DIAGNOSTIC_MODE = false;

    // List of images.
    public static final String LOGO_IMAGE = "./data/resources/textures/help/logotip.png";

    public static final String ARROW_TOP_IMAGE = "./data/resources/textures/arrow_top.png";
    public static final String ARROW_BOTTOM_IMAGE = "./data/resources/textures/arrow_bottom.png";

    public static final String VERTEX_BOTTOM_IMAGE = "./data/resources/textures/vertex.png";
    public static final String EDGE_BOTTOM_IMAGE = "./data/resources/textures/edge.png";
    public static final String MATCH_CASE_BOTTOM_IMAGE = "./data/resources/textures/match_case.png";
    public static final String WORDS_BOTTOM_IMAGE = "./data/resources/textures/words.png";
    public static final String REGEX_BOTTOM_IMAGE = "./data/resources/textures/regex.png";

    // List of fonts.
    public static final String UI_MONOSPACED_FONT_FAMILY = "Consolas";
    public static final String UI_STANDARD_FONT_FAMILY = "Courier";
    public static final String GRAPH_VIEW_FONT = "./data/resources/font/ubuntu-mono/UbuntuMono-R.ttf";

    public static final Font SUB_TITLE_FONT = new Font(UI_STANDARD_FONT_FAMILY, Font.BOLD, 13);
    public static final Font DEFAULT_MONOSPACED_FONT = new Font(UI_MONOSPACED_FONT_FAMILY, Font.PLAIN, 14);

    // List of colors.
    public static final Color DEFAULT_PANEL_BACKGROUND_COLOR = UIManager.getColor("Panel.background");

    private static final ConcurrentHashMap<String, Image> images = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, ImageIcon> imageIcons = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<String, Font> fonts = new ConcurrentHashMap<>();

    public static Image getImageResource(String resourcePath) {
        var image = images.computeIfAbsent(resourcePath, (key) -> {
            try {
                return ImageIO.read(new File(resourcePath));
            } catch (IOException ex) {
                log.error(ex.getMessage(), ex);
            }

            return null;
        });

        Validate.notNull(image, String.format("Image with path '%s' was not found.", resourcePath));

        return image;
    }

    public static ImageIcon getImageIcon(String resourcePath) {
        var imageIcon = imageIcons.computeIfAbsent(resourcePath, (key) -> {
            var image = getImageResource(resourcePath);
            if (image == null) {
                return null;
            }
            return new ImageIcon(image);
        });

        Validate.notNull(imageIcon, String.format("Image icon with path '%s' was not found.", resourcePath));

        return imageIcon;
    }

    public static Font getFontResource(String resourcePath) {
        var font = fonts.computeIfAbsent(resourcePath, (key) -> {
            try (FileInputStream in = new FileInputStream(resourcePath)) {
                return Font.createFont(Font.TRUETYPE_FONT, in);
            } catch (IOException | FontFormatException ex) {
                log.error(ex.getMessage(), ex);
            }

            return null;
        });

        Validate.notNull(font, String.format("Font with path '%s' was not found.", resourcePath));

        return font;
    }
}
