package vg.lib.operation;

@FunctionalInterface
public interface ProcedureWithOneArg<Arg1> {
    void execute(Arg1 arg1);
}
