package vg.lib.operation;

@FunctionalInterface
public interface OperationWithThreeArgs<Arg1, Arg2, Arg3, ResultType> {
    ResultType execute(Arg1 arg1, Arg2 arg2, Arg3 arg3);
}
