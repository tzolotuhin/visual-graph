package vg.lib.operation;

@FunctionalInterface
public interface ProcedureWithThreeArgs<Arg1, Arg2, Arg3> {
    void execute(Arg1 arg1, Arg2 arg2, Arg3 arg3);
}
