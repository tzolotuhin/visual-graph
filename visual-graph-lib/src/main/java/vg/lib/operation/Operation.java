package vg.lib.operation;

@FunctionalInterface
public interface Operation<ResultType> {
    ResultType execute();
}
