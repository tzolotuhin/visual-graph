package vg.lib.operation;

@FunctionalInterface
public interface OperationWithTwoArgs<Arg1, Arg2, ResultType> {
    ResultType execute(Arg1 arg1, Arg2 arg2);
}
