package vg.lib.operation;

@FunctionalInterface
public interface Procedure {
    void execute();
}
