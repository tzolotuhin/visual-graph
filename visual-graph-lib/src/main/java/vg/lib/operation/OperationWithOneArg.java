package vg.lib.operation;

@FunctionalInterface
public interface OperationWithOneArg<Arg1, ResultType> {
    ResultType execute(Arg1 arg1);
}
