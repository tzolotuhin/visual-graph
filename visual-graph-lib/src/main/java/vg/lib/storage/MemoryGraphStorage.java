package vg.lib.storage;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.model.record.AttributeOwnerType;
import vg.lib.model.record.AttributeRecord;
import vg.lib.model.record.AttributeRecordType;
import vg.lib.model.record.EdgeRecord;
import vg.lib.model.record.GraphModelRecord;
import vg.lib.model.record.VertexRecord;

@Slf4j
public class MemoryGraphStorage implements GraphStorage {
    private int graphModelIdCounter;
    private int vertexIdCounter;
    private int edgeIdCounter;
    private int attributeIdCounter;

    private final Map<Integer, GraphModelRecord> cacheGraphModels;
    private final Map<Integer, VertexRecord> cacheVertices;
    private final Map<Integer, EdgeRecord> cacheEdges;
    private final Map<Integer, AttributeRecord> cacheAttributes;

    public MemoryGraphStorage() {
        // initialize maps for storage.
        cacheGraphModels = new LinkedHashMap<>();
        cacheVertices = new LinkedHashMap<>();
        cacheEdges = new LinkedHashMap<>();
        cacheAttributes = new LinkedHashMap<>();

        // initialize counters for ids.
        graphModelIdCounter = 1;
        vertexIdCounter = 1;
        edgeIdCounter = 1;
        attributeIdCounter = 1;
    }

    @Override
    public int createGraphModel(String name) {
        var graphRecord = new GraphModelRecord(graphModelIdCounter++);
        graphRecord.setName(name);

        cacheGraphModels.put(graphRecord.getId(), graphRecord);

        return graphRecord.getId();
    }

    @Override
    public int createVertex(int graphModelId, int parentId) {
        var vertexRecord = new VertexRecord(vertexIdCounter++, graphModelId, parentId, false);

        cacheVertices.put(vertexRecord.getId(), vertexRecord);

        var parent = cacheVertices.get(parentId);
        if (parent != null) {
            parent.setVertexWithInnerGraph(true);
        }

        return vertexRecord.getId();
    }

    @Override
    public int createEdge(int graphModelId, int sourceVertexId, int targetVertexId) {
        // check preconditions
        var sourceVertexRecord = getVertexRecord(sourceVertexId);
        var targetVertexRecord = getVertexRecord(targetVertexId);

        Validate.notNull(sourceVertexRecord);
        Validate.notNull(targetVertexRecord);

        if (sourceVertexRecord.getGraphModelId() != targetVertexRecord.getGraphModelId()) {
            throw new IllegalArgumentException(
                String.format(
                    "Source graph model id is '%d', target graph model id is '%d'. "
                        + "But they should have same graph model id.",
                    sourceVertexRecord.getGraphModelId(),
                    targetVertexRecord.getGraphModelId()));
        }

        if (sourceVertexRecord.getParentId() != targetVertexRecord.getParentId()) {
            throw new IllegalArgumentException(
                String.format(
                    "Source parent id is '%d', target parent id is '%d'. But they should have same parent id.",
                    sourceVertexRecord.getParentId(),
                    targetVertexRecord.getParentId()));
        }

        // add new record
        EdgeRecord edgeRecord = new EdgeRecord(edgeIdCounter++, graphModelId, sourceVertexId, targetVertexId);
        cacheEdges.put(edgeRecord.getId(), edgeRecord);

        return edgeRecord.getId();
    }

    @Override
    public int createAttribute(int ownerId,
                               AttributeOwnerType ownerType,
                               String name,
                               String strValue,
                               AttributeRecordType valueType,
                               boolean visible) {
        var attributeRecord = new AttributeRecord(
            attributeIdCounter++, ownerId, ownerType, name, strValue, valueType, visible);
        cacheAttributes.put(attributeRecord.getId(), attributeRecord);

        return attributeRecord.getId();
    }

    @Override
    public List<GraphModelRecord> getGraphRecords() {
        return cacheGraphModels.values().stream()
            .map(GraphModelRecord::clone)
            .collect(Collectors.toList());
    }

    @Override
    @Nullable
    public VertexRecord getVertexRecord(int vertexId) {
        var vertexRecord = cacheVertices.get(vertexId);
        if(vertexRecord != null) {
            return vertexRecord.clone();
        }

        return null;
    }

    @Override
    public List<VertexRecord> getVertexRecordsByOwnerId(int parentId) {
        return cacheVertices.values().stream()
            .filter(vertexRecord -> vertexRecord.getParentId() == parentId)
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public EdgeRecord getEdgeRecord(int edgeId) {
        var eh = cacheEdges.get(edgeId);
        if(eh != null) {
            return eh.clone();
        }
        return null;
    }

    @Override
    public List<EdgeRecord> getEdgeRecordsByVertexId(int vertexId) {
        return cacheEdges.values().stream()
            .filter(edgeRecord -> edgeRecord.getSourceId() == vertexId || edgeRecord.getTargetId() == vertexId)
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<EdgeRecord> getEdgeRecordsByGraphModelId(int graphModelId) {
        return cacheEdges.values().stream()
            .filter(edgeRecord -> edgeRecord.getGraphModelId() == graphModelId)
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public List<VertexRecord> getRootRecords(int graphModelId) {
        return cacheVertices.values().stream()
            .filter(vertexRecord -> vertexRecord.getGraphModelId() == graphModelId && vertexRecord.isRoot())
            .sorted()
            .collect(Collectors.toList());
    }

    @Override
    public Optional<GraphModelRecord> findGraphModelRecord(int graphModelId) {
        var graphModelRecord = cacheGraphModels.get(graphModelId);
        return Optional.ofNullable(graphModelRecord);
    }

    @Override
    @Nullable
    public AttributeRecord getAttributeRecord(int attributeId) {
        var attributeRecord = cacheAttributes.get(attributeId);

        if (attributeRecord == null) {
            log.warn("Can't find attribute record, attribute id = {}.", attributeId);
            return null;
        }

        return attributeRecord.clone();
    }

    @Override
    public Optional<AttributeRecord> findAttributeRecord(int ownerId, AttributeOwnerType ownerType, String name) {
        return cacheAttributes.values().stream()
            .filter(ar -> ar.getOwnerId() == ownerId
                && ar.getOwnerType() == ownerType
                && Objects.equals(name, ar.getName()))
            .collect(Collectors.toList())
            .stream().findFirst();
    }

    @Override
    public List<AttributeRecord> getAttributeRecordsByOwner(int ownerId, AttributeOwnerType ownerType) {
        return cacheAttributes.values().stream()
            .filter(ar -> ar.getOwnerId() == ownerId && ar.getOwnerType() == ownerType)
            .collect(Collectors.toList());
    }

    @Override
    public List<AttributeRecord> getAttributeRecordsByValue(String name, String value) {
        return cacheAttributes.values().stream()
            .filter(ar -> Objects.equals(name, ar.getName()) && Objects.equals(value, ar.getStringValue()))
            .collect(Collectors.toList());
    }

    @Override
    public void editAttributeRecord(AttributeRecord attributeRecord) {
        var oldValue = cacheAttributes.get(attributeRecord.getId());
        Validate.notNull(oldValue);
        cacheAttributes.put(attributeRecord.getId(), attributeRecord);
    }

    @Override
    public void removeEdgeRecord(int edgeId) {
        cacheEdges.remove(edgeId);
    }
}
