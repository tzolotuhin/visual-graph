package vg.lib.storage;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import org.apache.commons.lang3.Validate;
import vg.lib.common.VGUtils;
import vg.lib.model.graph.Attribute;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.lib.model.record.AttributeOwnerType;
import vg.lib.model.record.AttributeRecord;
import vg.lib.model.record.AttributeRecordType;
import vg.lib.model.record.EdgeRecord;
import vg.lib.model.record.GraphModelRecord;
import vg.lib.model.record.VertexRecord;

public interface GraphStorage {
    // Constants
    int INPUT_PORT_TYPE = 0;
    int OUTPUT_PORT_TYPE = 1;

    int COMPOSITE_EDGE_MIDDLE_TYPE = 0;
    int COMPOSITE_EDGE_START_TYPE = 1;
    int COMPOSITE_EDGE_FINISH_TYPE = 2;

    String ATTRIBUTE_NAME_AMOUNT_OF_ATTACHMENTS = "src_code_amount";
    String ATTRIBUTE_NAME_PREFIX_FOR_SOURCE_CODE = "src_code_file_";

    // 0 - start, 0 - is master part of composite edge
    String ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER = "comp_edge_order";
    String ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID = "comp_edge_master_id";
    String ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_TYPE = "comp_edge_type";

    String ATTRIBUTE_NAME_FOR_SYSTEM_VERTEX_ID = "system_vertex_id";

    String ATTRIBUTE_NAME_FOR_SYSTEM_SRC_PORT_ID = "system_src_port_id";
    String ATTRIBUTE_NAME_FOR_SYSTEM_TRG_PORT_ID = "system_trg_port_id";

    String ATTRIBUTE_NAME_FOR_SRC_PORT_DB_ID = "src_port_db_id";
    String ATTRIBUTE_NAME_FOR_TRG_PORT_DB_ID = "trg_port_db_id";

    String SYSTEM_DB_ID_ATTRIBUTE = "system_db_id";

    String SYSTEM_FAKE_PORT_ATTRIBUTE = "is_fake_port";
    String SYSTEM_PORT_TYPE_ATTRIBUTE = "port_type";

    String ATTRIBUTE_NAME_FOR_COLOR = "color";

    String ATTRIBUTE_NAME_FOR_VERTEX_SHAPE = "shape";

    // The following attributes will be created with visible = true.
    String DIRECTED_EDGE_ATTRIBUTE = "directed_edge";
    String ATTRIBUTE_NAME_FOR_INNER_GRAPH_ID = "inner_graph_name";
    String ATTRIBUTE_NAME_FOR_NODE_ID = "node_id";
    String ATTRIBUTE_NAME_FOR_EDGE_ID = "edge_id";

    //region create methods.

    /**
     * Creates new graph model record and returns the unique id.
     *
     * @param name - name of the graph model.
     */
    int createGraphModel(String name);

    /**
     * Creates record for the vertex and returns the unique id.
     */
    int createVertex(int graphModelId, int parentId);

    /**
     * Creates record for the edge and returns the unique id.
     *
     * @param graphModelId   - id of graph model.
     * @param sourceVertexId - id of source vertex.
     * @param targetVertexId - id of target vertex.
     */
    int createEdge(int graphModelId, int sourceVertexId, int targetVertexId);

    /**
     * Creates record for the attribute and returns the unique id.
     *
     * @param ownerId   - id of the owner.
     * @param ownerType - type of the owner.
     * @param name      - attribute name.
     * @param strValue  - string attribute value.
     * @param valueType - type of value.
     * @param visible   - for system attributes.
     */
    int createAttribute(int ownerId,
                        AttributeOwnerType ownerType,
                        String name,
                        String strValue,
                        AttributeRecordType valueType,
                        boolean visible);

    //endregion

    //region create default methods.

    default int createGraphModelAttribute(int graphModelId,
                                          String name,
                                          String strValue,
                                          AttributeRecordType valueType,
                                          boolean visible) {
        return createAttribute(graphModelId, AttributeOwnerType.GRAPH_MODEL, name, strValue, valueType, visible);
    }

    default int createVertexAttribute(int vertexId,
                                      String name,
                                      String strValue,
                                      AttributeRecordType valueType,
                                      boolean visible) {
        return createAttribute(vertexId, AttributeOwnerType.VERTEX, name, strValue, valueType, visible);
    }

    default int createEdgeAttribute(int edgeId,
                                    String name,
                                    String strValue,
                                    AttributeRecordType valueType,
                                    boolean visible) {
        return createAttribute(edgeId, AttributeOwnerType.EDGE, name, strValue, valueType, visible);
    }

    default int createExtendedVertex(int graphModelId, int parentId) {
        int newVertexId = createVertex(graphModelId, parentId);

        createVertexAttribute(
            newVertexId,
            SYSTEM_DB_ID_ATTRIBUTE,
            Integer.toString(newVertexId),
            AttributeRecordType.INTEGER,
            false);

        return newVertexId;
    }

    default int createPort(int graphId, int vertexId, boolean isFake, int index) {
        int portId = createVertex(graphId, vertexId);

        createVertexAttribute(
            portId,
            SYSTEM_FAKE_PORT_ATTRIBUTE,
            Boolean.toString(isFake),
            AttributeRecordType.BOOLEAN,
            false);

        createVertexAttribute(
            portId,
            SYSTEM_PORT_TYPE_ATTRIBUTE,
            Integer.toString(INPUT_PORT_TYPE),
            AttributeRecordType.INTEGER,
            false);

        return portId;
    }

    /**
     * Returns parts of composite edge - List[0-master, 1, 2, ..., n].
     */
    default List<Integer> createCompositeEdge(int graphId,
                                              int sourceVertexId,
                                              int targetVertexId,
                                              int sourcePortId,
                                              int targetPortId,
                                              boolean directed) {
        var parentIds = findParents(Arrays.asList(sourceVertexId, targetVertexId));

        Validate.isTrue(parentIds.size() == 2
            || (parentIds.size() == 1 && sourceVertexId == targetVertexId));

        List<Integer> sourceParentIds = parentIds.get(sourceVertexId);
        List<Integer> targetParentIds = parentIds.get(targetVertexId);

        if (sourceParentIds.size() == 1 && targetParentIds.size() == 1) {
            var edgeId = createEdgeForVerticesWithSameParents(graphId, sourceVertexId, targetVertexId, sourcePortId, targetPortId, directed);

            return Collections.singletonList(edgeId);
        } else {
            List<Integer> sourceEdges = Lists.newArrayList();
            List<Integer> middleEdges = Lists.newArrayList();
            List<Integer> targetEdges = Lists.newArrayList();

            int portId;
            int bufSourceVertexId = sourceVertexId;
            int bufTargetVertexId = targetVertexId;
            int bufSourcePortId = sourcePortId;
            int bufTargetPortId = targetPortId;

            for (int i = 0; i < sourceParentIds.size() - 1; i++) {
                int sourceParentId = sourceParentIds.get(i);

                if (sourceParentIds.contains(targetVertexId) && i == sourceParentIds.size() - 2 && targetPortId >= 0) {
                    portId = targetPortId;

                    // setup 'output' type for the port
                    var attributeRecord = findAttributeRecord(
                        targetPortId,
                        AttributeOwnerType.VERTEX,
                        SYSTEM_PORT_TYPE_ATTRIBUTE)
                        .orElseThrow(IllegalArgumentException::new);

                    attributeRecord.setIntegerValue(OUTPUT_PORT_TYPE);
                    editAttributeRecord(attributeRecord);
                } else {
                    portId = createPort(graphId, sourceParentId, true, -1);
                }

                var edgeId = createCompositeEdge(
                    graphId, bufSourceVertexId, portId, bufSourcePortId, -1, directed).get(0);
                bufSourceVertexId = sourceParentId;
                bufSourcePortId = portId;
                sourceEdges.add(edgeId);
            }

            for (int i = 0; i < targetParentIds.size() - 1; i++) {
                int targetParentId = targetParentIds.get(i);

                if (targetParentIds.contains(sourceVertexId) && i == targetParentIds.size() - 2 && sourcePortId >= 0) {
                    portId = sourcePortId;

                    // setup 'input' type for the port
                    var attributeRecord = findAttributeRecord(
                        sourcePortId, AttributeOwnerType.VERTEX, SYSTEM_PORT_TYPE_ATTRIBUTE)
                        .orElseThrow(IllegalArgumentException::new);
                    attributeRecord.setIntegerValue(INPUT_PORT_TYPE);
                    editAttributeRecord(attributeRecord);
                } else {
                    portId = createPort(graphId, targetParentId, true, -1);
                }

                var edgeId = createCompositeEdge(
                    graphId, portId, bufTargetVertexId, -1, bufTargetPortId, directed).get(0);
                bufTargetVertexId = targetParentId;
                bufTargetPortId = portId;
                targetEdges.add(edgeId);
            }

            if (!sourceParentIds.contains(targetVertexId) && !targetParentIds.contains(sourceVertexId)) {
                var edgeId = createCompositeEdge(
                    graphId, bufSourceVertexId, bufTargetVertexId, bufSourcePortId, bufTargetPortId, directed).get(0);
                middleEdges.add(edgeId);
            }

            List<Integer> result = Lists.newArrayList();
            result.addAll(sourceEdges);
            result.addAll(middleEdges);
            result.addAll(targetEdges);

            int index = 0;
            for (Integer edgeId : result) {
                createEdgeAttribute(
                    edgeId,
                    ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_MASTER_ID,
                    Integer.toString(result.get(result.size() - 1)),
                    AttributeRecordType.INTEGER,
                    false);

                createEdgeAttribute(
                    edgeId,
                    ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_ORDER,
                    Integer.toString(index),
                    AttributeRecordType.INTEGER,
                    false);

                int type = COMPOSITE_EDGE_MIDDLE_TYPE;

                if (index == 0) {
                    type = COMPOSITE_EDGE_START_TYPE;
                }

                if (index == result.size() - 1) {
                    type = COMPOSITE_EDGE_FINISH_TYPE;
                }

                createEdgeAttribute(
                    edgeId,
                    ATTRIBUTE_NAME_FOR_COMPOSITE_EDGE_TYPE,
                    Integer.toString(type),
                    AttributeRecordType.INTEGER,
                    false);

                index++;
            }

            return result;
        }
    }

    default int createEdgeForVerticesWithSameParents(int graphId, int sourceVertexId, int targetVertexId) {
        return createEdgeForVerticesWithSameParents(
            graphId, sourceVertexId, targetVertexId, -1, -1, false);
    }

    default int createEdgeForVerticesWithSameParents(int graphId,
                                                     int sourceVertexId,
                                                     int targetVertexId,
                                                     int sourcePortId,
                                                     int targetPortId,
                                                     boolean directed) {
        var edgeId = createEdge(graphId, sourceVertexId, targetVertexId);
        createEdgeAttribute(edgeId, SYSTEM_DB_ID_ATTRIBUTE, Integer.toString(edgeId), AttributeRecordType.INTEGER, false);
        createEdgeAttribute(edgeId, DIRECTED_EDGE_ATTRIBUTE, Boolean.toString(directed), AttributeRecordType.BOOLEAN, true);
        if (sourcePortId > 0) {
            createEdgeAttribute(
                edgeId,
                ATTRIBUTE_NAME_FOR_SRC_PORT_DB_ID,
                Integer.toString(sourcePortId),
                AttributeRecordType.INTEGER,
                false);

            var attributeRecord = findAttributeRecord(
                sourcePortId,
                AttributeOwnerType.VERTEX,
                SYSTEM_PORT_TYPE_ATTRIBUTE)
                .orElseThrow(IllegalArgumentException::new);

            attributeRecord.setIntegerValue(OUTPUT_PORT_TYPE);
            editAttributeRecord(attributeRecord);
        }
        if (targetPortId > 0) {
            createEdgeAttribute(
                edgeId,
                ATTRIBUTE_NAME_FOR_TRG_PORT_DB_ID,
                Integer.toString(targetPortId),
                AttributeRecordType.INTEGER,
                false);
            var attributeRecord = findAttributeRecord(
                targetPortId, AttributeOwnerType.VERTEX, SYSTEM_PORT_TYPE_ATTRIBUTE)
                .orElseThrow(IllegalArgumentException::new);

            attributeRecord.setIntegerValue(INPUT_PORT_TYPE);
            editAttributeRecord(attributeRecord);
        }

        return edgeId;
    }

    //endregion

    //region find records methods.

    // TODO: should be deleted with GraphRecord.
    /**
     * Returns list of graph records.
     */
    List<GraphModelRecord> getGraphRecords();

    @Nullable
    VertexRecord getVertexRecord(int vertexId);

    List<VertexRecord> getVertexRecordsByOwnerId(int ownerId);

    EdgeRecord getEdgeRecord(int edgeId);

    List<EdgeRecord> getEdgeRecordsByVertexId(int vertexId);

    List<EdgeRecord> getEdgeRecordsByGraphModelId(int graphModelId);

    List<VertexRecord> getRootRecords(int graphId);

    Optional<GraphModelRecord> findGraphModelRecord(int graphModelId);

    @Nullable
    AttributeRecord getAttributeRecord(int attributeId);

    Optional<AttributeRecord> findAttributeRecord(int ownerId, AttributeOwnerType ownerType, String name);

    List<AttributeRecord> getAttributeRecordsByOwner(int ownerId, AttributeOwnerType ownerType);

    List<AttributeRecord> getAttributeRecordsByValue(String name, String value);

    default Graph getGraph(int graphModelId, int parentId) {
        var vertices = getVertices(parentId);
        var entryEdges = getEdges(graphModelId, vertices);
        var attributeRecords = getAttributeRecordsByOwner(parentId, AttributeOwnerType.VERTEX);
        var attributes = VGUtils.convertToAttributes(attributeRecords);
        return new Graph(vertices, entryEdges.getKey(), entryEdges.getValue(), attributes);
    }

    default Vertex getVertex(int vertexId) {
        var vertexRecord = getVertexRecord(vertexId);
        if (vertexRecord == null) {
            return null;
        }

        var attributes = getAttributesByOwner(vertexRecord.getId(), AttributeOwnerType.VERTEX);

        return new Vertex(attributes, vertexRecord);
    }

    default Edge getEdge(int edgeId) {
        var edgeRecord = getEdgeRecord(edgeId);
        if (edgeRecord == null) {
            return null;
        }

        return new Edge(
            getVertex(edgeRecord.getSourceId()),
            getVertex(edgeRecord.getTargetId()),
            getAttributesByOwner(edgeRecord.getId(), AttributeOwnerType.EDGE),
            edgeRecord);
    }

    default List<Attribute> getAttributesByOwner(int ownerId, AttributeOwnerType ownerType) {
        return getAttributeRecordsByOwner(ownerId, ownerType).stream()
            .map(VGUtils::convertToAttribute)
            .collect(Collectors.toList());
    }

    //endregion

    //region edit methods

    void editAttributeRecord(AttributeRecord attributeRecord);

    //endregion

    //region remove methods

    void removeEdgeRecord(int edgeId);

    //endregion

    private List<Vertex> getVertices(int vertexOwnerId) {
        List<Vertex> vertices = Lists.newArrayList();
        List<VertexRecord> vertexRecords = getVertexRecordsByOwnerId(vertexOwnerId);
        for (VertexRecord vertexRecord : vertexRecords) {
            vertices.add(getVertex(vertexRecord.getId()));
        }
        return vertices;
    }

    /**
     * Returns entity: first value is edges between the vertices,
     * second value is outer edges.
     */
    private Map.Entry<List<Edge>, Set<Edge>> getEdges(int graphModelId, List<Vertex> vertices) {
        Validate.isTrue(graphModelId > 0);

        var precondition = vertices.stream().allMatch(vertex -> {
            var vertexRecord = vertex.getLinkToVertexRecord();
            return vertexRecord != null && vertexRecord.getGraphModelId() == graphModelId;
        });
        Validate.isTrue(precondition);

        List<Edge> edges = Lists.newArrayList();
        var outsideEdges = new LinkedHashSet<Edge>();

        List<EdgeRecord> edgeRecords = getEdgeRecordsByGraphModelId(graphModelId);

        for (var edgeRecord : edgeRecords) {
            Vertex src = null, trg = null;

            for (Vertex vertex : vertices) {
                if (vertex.getLinkToVertexRecord().getId() == edgeRecord.getSourceId())
                    src = vertex;
                if (vertex.getLinkToVertexRecord().getId() == edgeRecord.getTargetId())
                    trg = vertex;
            }

            if (src == null && trg == null) {
                continue;
            }

            var attributes = getAttributesByOwner(edgeRecord.getId(), AttributeOwnerType.EDGE);
            var edge = new Edge(src, trg, attributes, edgeRecord);

            if (src != null && trg != null) {
                edges.add(edge);
            } else {
                outsideEdges.add(edge);
            }
        }

        return new AbstractMap.SimpleEntry<>(edges, outsideEdges);
    }

    /**
     * Calculate general parents for input vertices
     * <p>
     * V1 -> [P1, P2, P3, ..., -1]
     */
    default Map<Integer, List<Integer>> findParents(List<Integer> vertexIds) {
        Map<Integer, List<Integer>> hierarchies = Maps.newHashMap();
        for (int vertexId : vertexIds) {
            List<Integer> hierarchy = Lists.newArrayList();
            int tempVertexId = vertexId;
            do {
                VertexRecord vertexRecord = getVertexRecord(tempVertexId);
                tempVertexId = vertexRecord.getParentId();
                hierarchy.add(tempVertexId);
            } while (tempVertexId > -1);

            hierarchies.put(vertexId, Lists.reverse(hierarchy));
        }

        List<Integer> parentIds = hierarchies.get(vertexIds.get(0));
        int index;
        for (index = 0; index < parentIds.size(); index++) {
            int currentParentId = parentIds.get(index);
            boolean check = false;
            for (List<Integer> hierarchy : hierarchies.values()) {
                if (index >= hierarchy.size()) {
                    check = true;
                    break;
                }

                if (hierarchy.get(index) != currentParentId) {
                    check = true;
                    break;
                }
            }

            if (check) {
                break;
            }
        }
        index--;

        Map<Integer, List<Integer>> result = Maps.newHashMap();
        for (int vertexId : vertexIds) {
            List<Integer> hierarchy = hierarchies.get(vertexId);
            List<Integer> newHierarchy = Lists.newArrayList();

            for (int i = index; i < hierarchy.size(); i++) {
                newHierarchy.add(hierarchy.get(i));
            }

            result.put(vertexId, Lists.reverse(newHierarchy));
        }

        return result;
    }

    default void setInnerGraphIdAttribute(int dbVertexId, String innerGraphId) {
        createVertexAttribute(
            dbVertexId,
            ATTRIBUTE_NAME_FOR_INNER_GRAPH_ID,
            innerGraphId,
            AttributeRecordType.STRING,
            true);
    }

    default void setVertexIdAttribute(int dbVertexId, String nodeId) {
        createVertexAttribute(
            dbVertexId,
            ATTRIBUTE_NAME_FOR_NODE_ID,
            nodeId,
            AttributeRecordType.STRING,
            true);
    }

    default void setEdgeIdAttribute(int dbEdgeId, String edgeId) {
        createEdgeAttribute(
            dbEdgeId,
            ATTRIBUTE_NAME_FOR_EDGE_ID,
            edgeId,
            AttributeRecordType.STRING,
            true);
    }
}
