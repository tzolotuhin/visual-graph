package vg.lib.progress;

public interface Progress {
    long getValue();

    long getLength();

    String getName();
}
