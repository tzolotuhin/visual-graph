package vg.lib.progress;

import lombok.Getter;
import lombok.Setter;

public class ProgressImpl implements Progress {
    @Getter
    private final String name;

    @Getter @Setter
    private volatile long value;

    @Getter @Setter
    private volatile long length;

    public ProgressImpl(String name) {
        this.name = name;
        this.value = 0;
        this.length = 1;
    }
}
