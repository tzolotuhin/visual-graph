package vg.lib.layout.hierarchical.step2.legacy;

import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.step2.legacy.data.Edge;
import vg.lib.operation.Operation;

public class CalcCrossesOperation implements Operation<CalcCrossesOperation.Crosses> {
    private final Edge[] edges;
    private final Direction direction;

    public CalcCrossesOperation(Edge[] edges, Direction direction) {
        this.edges = edges;
        this.direction = direction;
    }

    @Override
    public Crosses execute() {
        int amount = 0;
        long w = 0;

        if (edges == null) {
            return new Crosses(amount, w);
        }

        for (var edge : edges) {
            edge.c = 0;
            edge.cw = 0;
        }

        for (int i = 0; i < edges.length; i++) {
            for (int j = i + 1; j < edges.length; j++) {
                var edge1 = edges[i];
                var edge2 = edges[j];

                if (!Edge.isIntersected(edge1, edge2)) {
                    continue;
                }

                amount++;

                edge1.c++;
                edge2.c++;

                if (!edge1.isSuitable(direction) || !edge2.isSuitable(direction)) {
                    continue;
                }

                int edgeW = Edge.GENERAL_CROSS_W;
                if (edge1.originalEdge.isTopBackEdge() || edge2.originalEdge.isTopBackEdge()) {
                    edgeW = Edge.TOP_BACK_FAKE_CROSS_W;
                } else if (edge1.originalEdge.isBottomBackEdge() || edge2.originalEdge.isBottomBackEdge()) {
                    edgeW = Edge.BOTTOM_BACK_FAKE_CROSS_W;
                } else if (edge1.originalEdge.isFakeEdge() && edge2.originalEdge.isFakeEdge()) {
                    edgeW = Edge.TWO_FAKE_CROSS_W;
                } else if (edge1.originalEdge.isTopMainBackEdge() || edge2.originalEdge.isTopMainBackEdge()) {
                    edgeW = Edge.TOP_MAIN_BACK_FAKE_CROSS_W;
                } else if (edge1.originalEdge.isBottomMainBackEdge() || edge2.originalEdge.isBottomMainBackEdge()) {
                    edgeW = Edge.BOTTOM_MAIN_BACK_FAKE_CROSS_W;
                } else if (edge1.originalEdge.isMainBackEdge() || edge2.originalEdge.isMainBackEdge()) {
                    edgeW = Edge.MAIN_BACK_FAKE_CROSS_W;
                } else if (edge1.originalEdge.isFakeEdge() || edge2.originalEdge.isFakeEdge()) {
                    edgeW = Edge.ONE_FAKE_CROSS_W;
                }

                if (edge1.originalEdge.levelGroupId != edge2.originalEdge.levelGroupId) {
                    edgeW += Edge.DIFF_LEVEL_GROUP_CROSS_W;
                }

                w += edgeW;
                edge1.cw += edgeW;
                edge2.cw += edgeW;
            }
        }

        return new Crosses(amount, w);
    }

    public static class Crosses {
        public final int amount;
        public final long w;

        public Crosses(int amount, long w) {
            this.amount = amount;
            this.w = w;
        }
    }
}
