package vg.lib.layout.hierarchical.data;

import lombok.Getter;

import java.util.UUID;

public class HierarchicalPort {
    @Getter
    private UUID portId;

    @Getter
    private int initialPortIndex;

    @Getter
    private int initialPortPosX;

    @Getter
    private int portWidth;

    public HierarchicalPort(UUID portId, int initialPortIndex, int initialPortPosX, int portWidth) {
        this.portId = portId;
        this.initialPortIndex = initialPortIndex;
        this.initialPortPosX = initialPortPosX;
        this.portWidth = portWidth;
    }
}
