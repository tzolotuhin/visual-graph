package vg.lib.layout.hierarchical.data;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.util.Objects;
import java.util.UUID;

public class HierarchicalEdge {
    @Getter
    private final UUID id;

    @Getter
    private final HierarchicalVertex srcVertex, trgVertex;

    // TODO: необходимо заменить на класс HierarchicalPort.
    @Getter
    private UUID srcPortId, trgPortId;

    @Getter
    private int srcPortIndex, trgPortIndex;

    @Getter
    private int srcPortPosX, trgPortPosX;

    @Getter
    private int srcPortSizeX, trgPortSizeX;

    @Getter @Setter
    private int srcPosX, trgPosX;

    @Getter @Setter
    private boolean fake;

    @Getter @Setter
    private UUID longEdgeId;
    @Getter @Setter
    private UUID backEdgeId;

    // TODO: should be deleted!
    public int levelGroupId = -1;

    @Getter @Setter
    private boolean srcBetweenRealVertices;
    @Getter @Setter
    private boolean trgBetweenRealVertices;

    // TODO: тут не ясно, возможно стоит отдельным полем хранить оригинальный id дуги.
    public HierarchicalEdge(UUID id, HierarchicalVertex srcVertex, HierarchicalVertex trgVertex) {
        this.id = id;
        this.srcVertex = srcVertex;
        this.trgVertex = trgVertex;
        this.srcPortId = null;
        this.trgPortId = null;
        this.srcPortIndex = -1;
        this.trgPortIndex = -1;
        this.srcPortPosX = -1;
        this.trgPortPosX = -1;
        this.fake = false;
        this.longEdgeId = null;
    }

    public void setSrcPortId(UUID srcPortId, int srcPortIndex, int srcPortPosX, int srcPortSizeX) {
        Validate.notNull(srcPortId);

        this.srcPortId = srcPortId;
        this.srcPortIndex = srcPortIndex;
        this.srcPortPosX = srcPortPosX;
        this.srcPortSizeX = srcPortSizeX;
    }

    public HierarchicalPort getSrcPort() {
        if (!hasSrcPort()) {
            return null;
        }

        return new HierarchicalPort(srcPortId, srcPortIndex, srcPortPosX, srcPortSizeX);
    }

    public void setTrgPortId(UUID trgPortId, int trgPortIndex, int trgPortPosX, int trgPortSizeX) {
        Validate.notNull(trgPortId);

        this.trgPortId = trgPortId;
        this.trgPortIndex = trgPortIndex;
        this.trgPortPosX = trgPortPosX;
        this.trgPortSizeX = trgPortSizeX;
    }

    public HierarchicalPort getTrgPort() {
        if (!hasTrgPort()) {
            return null;
        }

        return new HierarchicalPort(trgPortId, trgPortIndex, trgPortPosX, trgPortSizeX);
    }

    public boolean hasLongEdgeId() {
        return longEdgeId != null;
    }

    public boolean hasSrcPort() {
        return srcPortId != null;
    }

    public boolean hasTrgPort() {
        return trgPortId != null;
    }

    // TODO: need rename.
    public boolean isLongEdge() {
        return Math.abs(trgVertex.getLevel() - srcVertex.getLevel()) > 1;
    }

    public boolean isLoopEdge() {
        return srcVertex == trgVertex;
    }

    public boolean isFakeEdge() {
        return srcVertex.isFakeVertex() || trgVertex.isFakeVertex();
    }

    public boolean isBackEdge() {
        return srcVertex.isBackVertex() || trgVertex.isBackVertex();
    }

    public boolean isTopBackEdge() {
        return srcVertex.isCompanionVertex() && trgVertex.isRealVertex() || srcVertex.isTopBackVertex() || trgVertex.isTopBackVertex();
    }

    public boolean isBottomBackEdge() {
        return srcVertex.isRealVertex() && trgVertex.isCompanionVertex() || srcVertex.isBottomBackVertex() || trgVertex.isBottomBackVertex();
    }

    // TODO: use only for stage 1-2.
    public boolean isTopMainBackEdge() {
        return srcVertex.isCompanionVertex() && trgVertex.isBackVertex();
    }

    // TODO: use only for stage 1-2.
    public boolean isBottomMainBackEdge() {
        return srcVertex.isBackVertex() && trgVertex.isCompanionVertex();
    }

    public boolean isMainBackEdge() {
        return srcVertex.isBackVertex() && trgVertex.isBackVertex();
    }

    public boolean isTopBeaconEdge() {
        return srcVertex.isTopBeaconVertex();
    }

    public boolean isBottomBeaconEdge() {
        return trgVertex.isBottomBeaconVertex();
    }

    public float getSrcPortCenterX() {
        return srcPortPosX + srcPortSizeX / 2.0f;
    }

    public int getRoundSrcPortCenterX() {
        return Math.round(getSrcPortCenterX());
    }

    public float getTrgPortCenterX() {
        return trgPortPosX + trgPortSizeX / 2.0f;
    }

    public int getRoundTrgPortCenterX() {
        return Math.round(getTrgPortCenterX());
    }

    public int getGroupId() {
        return srcVertex.getGroupId();
    }

    @Override
    public String toString() {
        return srcVertex.getName() + " --> " + trgVertex.getName() + " : " + levelGroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HierarchicalEdge that = (HierarchicalEdge) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
