package vg.lib.layout.hierarchical.step2.custom;

import org.apache.commons.lang3.Validate;
import vg.lib.operation.Operation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.DUMMY_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.REAL_AND_SUITABLE_FOR_CHAIN_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.REAL_VERTEX_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.toChainIndexWithVertexIndex;

public class CustomExtractChains implements Operation<CustomExtractChains.Result> {
    private final int[] vertexIndexToType;

    private final int[][] vertexIndexToInputEdgeIndices;
    private final int[][] vertexIndexToOutputEdgeIndices;

    private final int[] vertexIndexToLayer;

    private final int numberOfEdges;

    private final int[] edgeIndexToType;

    private final int[] edgeIndexToSrcVertexIndex;
    private final int[] edgeIndexToTrgVertexIndex;

    private final int numberOfLayers;
    private final int[][] layerToVertexIndices;

    public CustomExtractChains(int[] vertexIndexToType,

                               int[][] vertexIndexToInputEdgeIndices,
                               int[][] vertexIndexToOutputEdgeIndices,

                               int[] vertexIndexToLayer,

                               int numberOfEdges,

                               int[] edgeIndexToType,

                               int[] edgeIndexToSrcVertexIndex,
                               int[] edgeIndexToTrgVertexIndex,

                               int numberOfLayers,
                               int[][] layerToVertexIndices) {
        this.vertexIndexToType = vertexIndexToType;

        this.vertexIndexToInputEdgeIndices = vertexIndexToInputEdgeIndices;
        this.vertexIndexToOutputEdgeIndices = vertexIndexToOutputEdgeIndices;

        this.vertexIndexToLayer = vertexIndexToLayer;

        this.numberOfEdges = numberOfEdges;

        this.edgeIndexToType = edgeIndexToType;

        this.edgeIndexToSrcVertexIndex = edgeIndexToSrcVertexIndex;
        this.edgeIndexToTrgVertexIndex = edgeIndexToTrgVertexIndex;

        this.numberOfLayers = numberOfLayers;
        this.layerToVertexIndices = layerToVertexIndices;
    }

    @Override
    public Result execute() {
        var layerToIsDummy = findLayerToIsDummy(numberOfLayers, layerToVertexIndices, vertexIndexToType);

        var chainIndexToStartVertexIndex = new HashMap<Integer, Integer>();
        var chainIndexToFinishVertexIndex = new HashMap<Integer, Integer>();

        var edgeIndexToChainIndex = new int[numberOfEdges];
        Arrays.fill(edgeIndexToChainIndex, -1);

        var chainIndexCounter = 0;

        var lastVertexIndexToChainIndex = new HashMap<Integer, Integer>();
        for (var layer = 0; layer < numberOfLayers; layer++) {
            var vertexIndices = layerToVertexIndices[layer];
            for (var vertexIndex : vertexIndices) {
                var outputEdgeIndices = vertexIndexToOutputEdgeIndices[vertexIndex];

                for (var outputEdgeIndex : outputEdgeIndices) {
                    if (edgeIndexToChainIndex[outputEdgeIndex] >= 0) {
                        continue;
                    }

                    var edgeType = edgeIndexToType[outputEdgeIndex];
                    if (edgeType != DUMMY_EDGE_TYPE && edgeType != REAL_AND_SUITABLE_FOR_CHAIN_EDGE_TYPE) {
                        continue;
                    }

                    var chainIndex = lastVertexIndexToChainIndex.remove(vertexIndex);
                    if (chainIndex == null) {
                        chainIndex = chainIndexCounter;
                        chainIndexToStartVertexIndex.put(chainIndex, vertexIndex);
                        chainIndexCounter++;
                    }

                    var trgVertexIndex = edgeIndexToTrgVertexIndex[outputEdgeIndex];

                    var trgInputEdgeIndices = vertexIndexToInputEdgeIndices[trgVertexIndex];
                    var trgOutputEdgeIndices = vertexIndexToOutputEdgeIndices[trgVertexIndex];
                    if (trgInputEdgeIndices.length == 1
                            && trgOutputEdgeIndices.length == 1
                            && (edgeIndexToType[trgOutputEdgeIndices[0]] == DUMMY_EDGE_TYPE
                            || edgeIndexToType[trgOutputEdgeIndices[0]] == REAL_AND_SUITABLE_FOR_CHAIN_EDGE_TYPE)) {
                        lastVertexIndexToChainIndex.put(trgVertexIndex, chainIndex);
                    } else {
                        chainIndexToFinishVertexIndex.put(chainIndex, trgVertexIndex);
                    }

                    edgeIndexToChainIndex[outputEdgeIndex] = chainIndex;
                }
            }
        }

        Validate.isTrue(lastVertexIndexToChainIndex.isEmpty());
        Validate.isTrue(chainIndexToStartVertexIndex.size() == chainIndexToFinishVertexIndex.size());

        var chainIndexToStartVertexIndexResult = new ArrayList<Integer>();
        var chainIndexToFinishVertexIndexResult = new ArrayList<Integer>();
        var chainIndexBeforeExcludeToChainIndex = new HashMap<Integer, Integer>();
        for (var chainIndex = 0; chainIndex < chainIndexCounter; chainIndex++) {
            var startVertexIndex = chainIndexToStartVertexIndex.get(chainIndex);
            var finishVertexIndex = chainIndexToFinishVertexIndex.get(chainIndex);

            // exclude chains which length = 2 and edges are connected to vertex which places on dummy layer.
            if (vertexIndexToType[startVertexIndex] == REAL_VERTEX_TYPE
                    && vertexIndexToType[finishVertexIndex] == REAL_VERTEX_TYPE
                    && vertexIndexToLayer[finishVertexIndex] - vertexIndexToLayer[startVertexIndex] == 2
                    && layerToIsDummy[vertexIndexToLayer[startVertexIndex] + 1]) {
                continue;
            }

            chainIndexBeforeExcludeToChainIndex.put(chainIndex, chainIndexToStartVertexIndexResult.size());

            chainIndexToStartVertexIndexResult.add(startVertexIndex);
            chainIndexToFinishVertexIndexResult.add(finishVertexIndex);
        }

        var edgeIndexToChainIndexAfterExclude = new int[numberOfEdges];
        for (var edgeIndex = 0; edgeIndex < numberOfEdges; edgeIndex++) {
            var chainIndexBeforeExclude = edgeIndexToChainIndex[edgeIndex];

            var chainIndexAfterExclude = chainIndexBeforeExclude;
            if (chainIndexBeforeExclude >= 0) {
                chainIndexAfterExclude = chainIndexBeforeExcludeToChainIndex
                        .getOrDefault(chainIndexBeforeExclude, -1);
            }

            edgeIndexToChainIndexAfterExclude[edgeIndex] = chainIndexAfterExclude;
        }

        var numberOfChains = chainIndexToStartVertexIndexResult.size();

        return new Result(
                numberOfChains,
                chainIndexToStartVertexIndexResult.stream().mapToInt(Integer::intValue).toArray(),
                chainIndexToFinishVertexIndexResult.stream().mapToInt(Integer::intValue).toArray(),
                edgeIndexToChainIndexAfterExclude,
                findLayerToChainIndicesWithVertexIndices(edgeIndexToChainIndexAfterExclude));
    }

    private long[][] findLayerToChainIndicesWithVertexIndices(int[] edgeIndexToChainIndex) {
        var layerToChainIndicesWithVertexIndices = new LinkedHashMap<Integer, Set<Long>>();
        for (var edgeIndex = 0; edgeIndex < numberOfEdges; edgeIndex++) {
            var chainIndex = edgeIndexToChainIndex[edgeIndex];
            if (chainIndex < 0) {
                continue;
            }

            var srcVertexIndex = edgeIndexToSrcVertexIndex[edgeIndex];
            var trgVertexIndex = edgeIndexToTrgVertexIndex[edgeIndex];

            var srcLayer = vertexIndexToLayer[srcVertexIndex];
            var trgLayer = vertexIndexToLayer[trgVertexIndex];

            layerToChainIndicesWithVertexIndices.putIfAbsent(srcLayer, new LinkedHashSet<>());
            layerToChainIndicesWithVertexIndices.putIfAbsent(trgLayer, new LinkedHashSet<>());

            var chainIndexWithSrcVertexIndex = toChainIndexWithVertexIndex(chainIndex, srcVertexIndex);
            var chainIndexWithTrgVertexIndex = toChainIndexWithVertexIndex(chainIndex, trgVertexIndex);

            layerToChainIndicesWithVertexIndices.get(srcLayer).add(chainIndexWithSrcVertexIndex);
            layerToChainIndicesWithVertexIndices.get(trgLayer).add(chainIndexWithTrgVertexIndex);
        }

        var result = new long[numberOfLayers][];
        for (var layer = 0; layer < numberOfLayers; layer++) {
            result[layer] = layerToChainIndicesWithVertexIndices
                    .getOrDefault(layer, Collections.emptySet()).stream()
                    .mapToLong(Long::longValue).toArray();
        }

        return result;
    }

    private boolean[] findLayerToIsDummy(int numberOfLayers,
                                         int[][] layerToVertexIndices,
                                         int[] vertexIndexToType) {
        var layerToIsDummy = new boolean[numberOfLayers];
        for (var layer = 0; layer < numberOfLayers; layer++) {
            var isDummy = true;
            for (var vertexIndex : layerToVertexIndices[layer]) {
                if (vertexIndexToType[vertexIndex] == REAL_VERTEX_TYPE) {
                    isDummy = false;
                    break;
                }
            }
            layerToIsDummy[layer] = isDummy;
        }

        return layerToIsDummy;
    }

    public record Result(int numberOfChains,
                         int[] chainIndexToStartVertexIndex,
                         int[] chainIndexToFinishVertexIndex,
                         int[] edgeIndexToChainIndex,
                         long[][] layerToChainIndicesWithVertexIndices) { }
}
