package vg.lib.layout.hierarchical.step2.legacy.data;

public class Group {
    public String name;
    public int[] columnIndices;
    public boolean isBack;
    public int parentRowIndex;
    public int parentColumnIndex;
    public Vertex parentVertex;

    public boolean visited;

    public Group(Vertex vertex, int[] columnIndices) {
        this(vertex.vertex.getName(), columnIndices, vertex.vertex.isBackVertex(), vertex.rowIndex, vertex.columnIndex, vertex);
    }

    public Group(String name, int[] columnIndices, boolean isBack, int parentRowIndex, int parentColumnIndex, Vertex parentVertex) {
        this.name = name;
        this.columnIndices = columnIndices;
        this.isBack = isBack;
        this.parentRowIndex = parentRowIndex;
        this.parentColumnIndex = parentColumnIndex;
        this.parentVertex = parentVertex;
    }

    @Override
    public String toString() {
        return name;
    }
}
