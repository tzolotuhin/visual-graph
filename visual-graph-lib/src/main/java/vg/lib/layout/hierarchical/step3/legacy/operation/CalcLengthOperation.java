package vg.lib.layout.hierarchical.step3.legacy.operation;

import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.step3.legacy.data.Edge;
import vg.lib.operation.Operation;

public class CalcLengthOperation implements Operation<Long> {
    private final Edge[] edges;
    private final Direction direction;
    private final boolean ignoreBackwardEdges;

    public CalcLengthOperation(Edge[] edges, Direction direction, boolean ignoreBackwardEdges) {
        this.edges = edges;
        this.direction = direction;
        this.ignoreBackwardEdges = ignoreBackwardEdges;
    }

    @Override
    public Long execute() {
        long[] maxSourceLengths = new long[0];
        long[] maxTargetLengths = new long[0];
        if (edges.length > 0) {
            maxSourceLengths = new long[edges[0].getSourceRowSize()];
            maxTargetLengths = new long[edges[0].getTargetRowSize()];
        }

        for (var edge : edges) {
            long realPartLong = Math.abs(edge.getSourceX() - edge.getTargetX());

            realPartLong = Math.multiplyExact(realPartLong, calcW(edge));

            maxSourceLengths[edge.sourceIndex] = Math.max(maxSourceLengths[edge.sourceIndex], realPartLong);
            maxTargetLengths[edge.targetIndex] = Math.max(maxTargetLengths[edge.targetIndex], realPartLong);
        }

        long sumOfLengthsOfAllEdges = 0;
        for (long l : maxSourceLengths) {
            sumOfLengthsOfAllEdges = Math.addExact(sumOfLengthsOfAllEdges, l);
        }
        for (long l : maxTargetLengths) {
            sumOfLengthsOfAllEdges = Math.addExact(sumOfLengthsOfAllEdges, l);
        }

        return sumOfLengthsOfAllEdges;
    }

    // TODO: возможно стоит сделать возвращаемое значение double.
    private long calcW(Edge edge) {
        if (!edge.isSuitable(direction)) {
            return 0;
        }

        var src = edge.getSourceBlock();
        var trg = edge.getTargetBlock();

        int X3 = 100000;
        int X2 = 1000;
        int DEFAULT = 1;

        if (ignoreBackwardEdges && (src.isBackVertex() || trg.isBackVertex())) {
            return 0;
        }

        // TODO: необходимо переработать эти if-ы так как они оба про обработку back дуг.
//        if (direction == Direction.BOTTOM) {
//            if (src.isFakeVertex() && trg.type == VertexType.TOP_BACK_VERTEX_TYPE) {
//                return 0;
//            }
//            if (src.isBackVertex() && trg.isRealVertex()) {
//                return 0;
//            }
//            if (src.isCompanionVertex() && trg.isMainBackVertex()) {
//                return DEFAULT;
//            }
//            if (src.isMainBackVertex() || trg.isMainBackVertex()) {
//                return DEFAULT;
//            }
//        }
//        if (direction == Direction.TOP) {
//            if (trg.isFakeVertex() && src.type == VertexType.BOTTOM_BACK_VERTEX_TYPE) {
//                return 0;
//            }
//            if (src.isRealVertex() && trg.isBackVertex()) {
//                return 0;
//            }
//            if (src.isMainBackVertex() && trg.isCompanionVertex()) {
//                return DEFAULT;
//            }
//            if (src.isMainBackVertex() || trg.isMainBackVertex()) {
//                return DEFAULT;
//            }
//        }

        if (trg.isRealVertex() && edge.srcBetweenRealVertices && src.isFakeVertex()) {
            return X3;
        }

        if (src.isRealVertex() && edge.trgBetweenRealVertices && trg.isFakeVertex()) {
            return X3;
        }

        if (src.isRealVertex() && trg.isRealVertex()) {
            return X2;
        }

        if (src.original.isPartOfCoreTree() && trg.original.isPartOfCoreTree()) {
            return X2;
        }

        return DEFAULT;
    }
}
