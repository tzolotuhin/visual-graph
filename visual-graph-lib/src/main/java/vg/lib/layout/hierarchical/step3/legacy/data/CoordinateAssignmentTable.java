package vg.lib.layout.hierarchical.step3.legacy.data;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.layout.hierarchical.step3.legacy.operation.AlignRowProcedure;
import vg.lib.layout.hierarchical.step3.legacy.operation.ExtCalcLengthOperation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.TreeMap;

@Slf4j
public class CoordinateAssignmentTable {
    private static final int RIGHT_VALUE = 50_000;
    private static final int CENTER_VALUE = 25_000;

    public int[][] x;
    public int[][] backupX;

    public boolean[][] visited;
    public boolean[][] fixed;

    public Edge[][] edges;

    public Vertex[][] vertices;
    public List<StraightLine> lines;

    public Row[] rows;

    public Vertex[] toMapping;
    public HierarchicalVertex[] fromMapping;

    public CoordinateAssignmentTable(TreeMap<Integer, List<HierarchicalVertex>> group,
                                     HierarchicalGraph graph) {
        this(group, graph, true);
    }

    public CoordinateAssignmentTable(
            TreeMap<Integer, List<HierarchicalVertex>> group,
            HierarchicalGraph graph,
            boolean ignorePorts) {
        // initialize inner fields.
        vertices = new Vertex[group.size()][];
        visited = new boolean[vertices.length][];
        fixed = new boolean[vertices.length][];
        x = new int[vertices.length][];
        backupX = new int[vertices.length][];
        rows = new Row[vertices.length];

        int amountOfVertices = graph.getVertices().size();
        toMapping = new Vertex[amountOfVertices];
        fromMapping = new HierarchicalVertex[amountOfVertices];

        int rowIndex = 0;
        for (var groupEntry : group.entrySet()) {
            var row = groupEntry.getValue();

            vertices[rowIndex] = new Vertex[row.size()];
            visited[rowIndex] = new boolean[row.size()];
            fixed[rowIndex] = new boolean[row.size()];
            x[rowIndex] = new int[row.size()];
            backupX[rowIndex] = new int[row.size()];

            rows[rowIndex] = new Row(vertices[rowIndex], rowIndex, this);

            for (int columnIndex = 0; columnIndex < row.size(); columnIndex++) {
                var vertex = row.get(columnIndex);

                var block = new Vertex(
                        vertex,
                        vertex.getIndex(),
                        vertex.getName(),
                        vertex.getType(),
                        rowIndex,
                        columnIndex,
                        vertex.getWidth(),
                        x[rowIndex],
                        visited[rowIndex],
                        fixed[rowIndex]);

                toMapping[vertex.getIndex()] = block;
                fromMapping[vertex.getIndex()] = vertex;

                vertices[rowIndex][columnIndex] = block;
            }

            rows[rowIndex].resetCoordinates(0);

            rowIndex++;
        }

        // initialize edges.
        edges = new Edge[group.size()][];
        rowIndex = 0;
        for (var groupEntry : group.entrySet()) {
            var outputEdges = graph.getOutputEdgesDefault(groupEntry.getValue());

            var rowEdges = new ArrayList<Edge>(outputEdges.size());
            for (int edgeIndex = 0; edgeIndex < outputEdges.size(); edgeIndex++) {
                var outputEdge = outputEdges.get(edgeIndex);

                if (ignorePorts && (outputEdge.getSrcVertex().isPort() || outputEdge.getTrgVertex().isPort())) {
                    continue;
                }

                var srcVertex = outputEdge.getSrcVertex();
                var trgVertex = outputEdge.getTrgVertex();

                var sourceBlock = toMapping[srcVertex.getIndex()];
                var targetBlock = toMapping[trgVertex.getIndex()];

                Validate.notNull(sourceBlock, "Can't find block for vertex: " + srcVertex.getName());
                Validate.notNull(targetBlock, "Can't find block for vertex: " + trgVertex.getName());

                var edge = new Edge(
                        outputEdge,
                        sourceBlock.columnIndex,
                        targetBlock.columnIndex,
                        srcVertex.getOutputEdges().size(),
                        trgVertex.getInputEdges().size(),
                        outputEdge.getSrcPosX() + outputEdge.getSrcVertex().getBorderSize().x,
                        outputEdge.getTrgPosX() + outputEdge.getTrgVertex().getBorderSize().x,
                        x[rowIndex],
                        x[rowIndex + 1],
                        vertices[rowIndex],
                        vertices[rowIndex + 1]
                );

                edge.srcBetweenRealVertices = outputEdge.isSrcBetweenRealVertices();
                edge.trgBetweenRealVertices = outputEdge.isTrgBetweenRealVertices();

                rowEdges.add(edge);
            }

            edges[rowIndex] = new Edge[rowEdges.size()];
            rowEdges.toArray(edges[rowIndex]);

            rowIndex++;
        }

        // TODO: need to me the part of the code in another place because depend on this code we have in ApplyStraightLinesProcedure.
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];
                if (vertex.original.isInputBreakPoint()) {
                    var src = vertex;
                    while (src.isFakeVertex()) {
                        var edge = getEdgeBySrc(src);
                        edge.direction = Direction.TOP;
                        src = edge.getTargetBlock();
                    }
                }
                if (vertex.original.isOutputBreakPoint()) {
                    var trg = vertex;
                    while (trg.isFakeVertex()) {
                        var edge = getEdgeByTrg(trg);
                        edge.direction = Direction.BOTTOM;
                        trg = edge.getSourceBlock();
                    }
                }
                if (vertex.original.isTopCompanionVertex()) {
                    var src = vertex;
                    while (src.isFakeVertex()) {
                        var edge = getTopBackEdgeBySrc(src);
                        edge.direction = Direction.TOP;
                        src = edge.getTargetBlock();
                    }
                    var edge = getTopMainBackEdgeBySrc(vertex);
                    edge.direction = Direction.BOTH;
                }
                if (vertex.original.isBottomCompanionVertex()) {
                    var trg = vertex;
                    while (trg.isFakeVertex()) {
                        var edge = getBottomBackEdgeByTrg(trg);
                        edge.direction = Direction.BOTTOM;
                        trg = edge.getSourceBlock();
                    }
                    var edge = getBottomMainBackEdgeByTrg(vertex);
                    edge.direction = Direction.BOTH;
                }
            }
        }
    }

    // TODO: необходимо выкинуть куда-то эти методы или как-то оптимизировать код.
    private Edge getTopBackEdgeBySrc(Vertex src) {
        for (int i = 0; i < edges.length; i++) {
            for (int j = 0; j < edges[i].length; j++) {
                var edge = edges[i][j];
                if (edge.getSourceBlock() == src && edge.original.isTopBackEdge()) {
                    return edge;
                }
            }
        }
        return null;
    }

    private Edge getTopMainBackEdgeBySrc(Vertex src) {
        for (int i = 0; i < edges.length; i++) {
            for (int j = 0; j < edges[i].length; j++) {
                var edge = edges[i][j];
                if (edge.getSourceBlock() == src && !edge.original.isTopBackEdge()) {
                    return edge;
                }
            }
        }
        return null;
    }

    private Edge getEdgeBySrc(Vertex src) {
        for (int i = 0; i < edges.length; i++) {
            for (int j = 0; j < edges[i].length; j++) {
                var edge = edges[i][j];
                if (edge.getSourceBlock() == src) {
                    return edge;
                }
            }
        }
        return null;
    }

    private Edge getBottomBackEdgeByTrg(Vertex trg) {
        for (int i = 0; i < edges.length; i++) {
            for (int j = 0; j < edges[i].length; j++) {
                var edge = edges[i][j];
                if (edge.getTargetBlock() == trg && edge.original.isBottomBackEdge()) {
                    return edge;
                }
            }
        }
        return null;
    }

    private Edge getBottomMainBackEdgeByTrg(Vertex trg) {
        for (int i = 0; i < edges.length; i++) {
            for (int j = 0; j < edges[i].length; j++) {
                var edge = edges[i][j];
                if (edge.getTargetBlock() == trg && !edge.original.isBottomBackEdge()) {
                    return edge;
                }
            }
        }
        return null;
    }

    private Edge getEdgeByTrg(Vertex trg) {
        for (int i = 0; i < edges.length; i++) {
            for (int j = 0; j < edges[i].length; j++) {
                var edge = edges[i][j];
                if (edge.getTargetBlock() == trg) {
                    return edge;
                }
            }
        }
        return null;
    }

    public void fixRealVertices() {
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];
                if (vertex.isRealVertex()) {
                    vertex.setFixed(true);
                }
                vertex.leftSpace = 0;
                vertex.rightSpace = 0;
            }
        }
    }

    public void fixMainBackVertices() {
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];
                if (vertex.isMainBackVertex()) {
                    vertex.setFixed(true);
                }
                vertex.leftSpace = 0;
                vertex.rightSpace = 0;
            }
        }
    }

    public void unfixCompanionVertices() {
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];
                if (vertex.isCompanionVertex()) {
                    vertex.setFixed(false);
                }
                vertex.leftSpace = 0;
                vertex.rightSpace = 0;
            }
        }
    }

    public void fixTopCompanionVertices() {
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];
                if (vertex.isTopCompanionVertex()) {
                    vertex.setFixed(true);
                }
                vertex.leftSpace = 0;
                vertex.rightSpace = 0;
            }
        }
    }

    public void fixBottomCompanionVertices() {
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];
                if (vertex.isBottomCompanionVertex()) {
                    vertex.setFixed(true);
                }
                vertex.leftSpace = 0;
                vertex.rightSpace = 0;
            }
        }
    }

    public void fixEdges(Direction direction) {
        for (int i = 0; i < edges.length; i++) {
            for (int j = 0; j < edges[i].length; j++) {
                var edge = edges[i][j];
                if (edge.isSuitable(direction)) {
                    edge.getSourceBlock().setFixed(true);
                    edge.getTargetBlock().setFixed(true);
                }
            }
        }
    }

    public void bottomPass(Direction direction, boolean insertFillers, boolean ignoreBackwardEdges) {
        for (int currLevelIndex = 1; currLevelIndex < vertices.length; currLevelIndex++) {
            new AlignRowProcedure(rows[currLevelIndex], new ExtCalcLengthOperation(currLevelIndex, this, Direction.BOTTOM, direction, ignoreBackwardEdges)).execute();
            //printRows(currLevelIndex);
        }

        if (insertFillers) {
            updateFillers();
        }
    }

    public void topPass(Direction direction, boolean insertFillers, boolean ignoreBackwardEdges) {
        for (int currLevelIndex = vertices.length - 2; currLevelIndex >= 0; currLevelIndex--) {
            new AlignRowProcedure(rows[currLevelIndex], new ExtCalcLengthOperation(currLevelIndex, this, Direction.TOP, direction, ignoreBackwardEdges)).execute();
            //printRows(currLevelIndex);
        }

        if (insertFillers) {
            updateFillers();
        }
    }

    private void printRows(int currRowIndex) {
        if (!VGConfigSettings.DIAGNOSTIC_MODE) {
            return;
        }

        log.debug("\n\n\n==========\nAfter row index: " + currRowIndex);
        for (Row row : rows) {
            log.debug(row.toString());
        }
    }

    private final Queue<Vertex> _moveQueue = new ArrayDeque<>(300);
    private int _moveNumber = 0;

    public boolean moveLeft(Vertex vertex) {
       _moveInit(vertex);

        while (!_moveQueue.isEmpty()) {
            vertex = _moveQueue.poll();
            vertex.moveNumber = _moveNumber;

            if (vertex.columnIndex > 0) {
                var leftBlock = vertices[vertex.rowIndex][vertex.columnIndex - 1];

                if (leftBlock.moveNumber == _moveNumber) {
                    continue;
                }

                int leftX = x[vertex.rowIndex][vertex.columnIndex - 1];
                int blockX = x[vertex.rowIndex][vertex.columnIndex];

                if (leftX + leftBlock.size + leftBlock.rightSpace >= blockX) {
                    _addToQueue(leftBlock);
                }
            }
        }

        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                if (vertices[i][j].moveNumber == _moveNumber && fixed[i][j]) {
                    return false;
                }
            }
        }

        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                if (vertices[i][j].moveNumber == _moveNumber) {
                    x[i][j]--;
                }
            }
        }

        return true;
    }

    public boolean moveRight(Vertex vertex) {
        _moveInit(vertex);

        while (!_moveQueue.isEmpty()) {
            vertex = _moveQueue.poll();
            vertex.moveNumber = _moveNumber;

            if (vertex.columnIndex < vertices[vertex.rowIndex].length - 1) {
                var rightBlock = vertices[vertex.rowIndex][vertex.columnIndex + 1];

                if (rightBlock.moveNumber == _moveNumber) {
                    continue;
                }

                int rightX = x[vertex.rowIndex][vertex.columnIndex + 1];
                int blockX = x[vertex.rowIndex][vertex.columnIndex];

                if (blockX + vertex.size + vertex.rightSpace >= rightX) {
                    _addToQueue(rightBlock);
               }
            }
        }

        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                if (vertices[i][j].moveNumber == _moveNumber && fixed[i][j]) {
                    return false;
                }
            }
        }

        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                if (vertices[i][j].moveNumber == _moveNumber) {
                    x[i][j]++;
                }
            }
        }

        return true;
    }

    private void _moveInit(Vertex vertex) {
        _moveQueue.clear();
        _moveNumber++;

        _addToQueue(vertex);
    }

    private void _addToQueue(Vertex vertex) {
        if (vertex.straightLineId >= 0) {
            var line = lines.get(vertex.straightLineId);

            line.setMoveNumber(_moveNumber);

            _moveQueue.addAll(line.vertices);
        } else {
            vertex.moveNumber = _moveNumber;

            _moveQueue.add(vertex);
        }
    }

    private void updateFillers() {
        for (int rowIndex = 0; rowIndex < vertices.length; rowIndex++) {
            for (int columnIndex = 0; columnIndex < vertices[rowIndex].length - 1; columnIndex++) {
                var currBlock = vertices[rowIndex][columnIndex];
                var nextBlock = vertices[rowIndex][columnIndex + 1];

                if (currBlock.isFakeVertex() && nextBlock.isFakeVertex()) {
                    continue;
                }

                if (currBlock.isBackVertex() || nextBlock.isBackVertex()) {
                    continue;
                }

                int len = x[nextBlock.rowIndex][nextBlock.columnIndex] - x[currBlock.rowIndex][currBlock.columnIndex] - currBlock.size;

                currBlock.rightSpace = len;
                nextBlock.leftSpace = len;
            }
        }
    }

    public void resetVisited() {
        for (boolean[] booleans : visited) {
            Arrays.fill(booleans, false);
        }
    }

    public void backup() {
        for (int i = 0; i < x.length; i++) {
            System.arraycopy(x[i], 0, backupX[i], 0, x[i].length);
        }
    }

    public void rollback() {
        for (int i = 0; i < backupX.length; i++) {
            System.arraycopy(backupX[i], 0, x[i], 0, backupX[i].length);
        }
    }

    public void setupCoordinates() {
        int min = Integer.MAX_VALUE;
        for (int[] row : x) {
            if (row.length > 0) {
                min = Math.min(min, row[0]);
            }
        }

        for (int i = 0; i < x.length; i++) {
            for (int j = 0; j < x[i].length; j++) {
                x[i][j] += (-min);
            }
        }

        for (int i = 0; i < vertices.length; i++) {
            int lastX = 0;
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];
                var hVertex = fromMapping[vertex.originalId];

                Validate.isTrue(lastX <= x[i][j], String.format("Problem with block %s %s, name = %s", vertex.rowIndex, vertex.columnIndex, vertex.toString()));

                hVertex.setPosX(x[i][j]);
                lastX = x[i][j] + vertex.size;
            }
        }
    }

    public void applyCoordinates() {
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];
                var hVertex = fromMapping[vertex.originalId];
                x[i][j] = hVertex.getPosX();
            }
        }
    }

    // setup candidates.
    int[] leftWallX;
    int[] rightWallIndex;
    int rightWallAmount;

    public void applyCandidates(List<StraightLineCandidate> candidates) {
        leftWallX = new int[vertices.length];
        Arrays.fill(leftWallX, 0);

        rightWallIndex = new int[vertices.length];
        Arrays.fill(rightWallIndex, 0);

        rightWallAmount = 0;
        for (var row : rows) {
            row.resetCoordinates(RIGHT_VALUE);

            rightWallAmount += row.size();
        }

        // build straight lines.
        var existedCandidates = new ArrayList<StraightLineCandidate>();
        lines = new ArrayList<>();
        int straightLineId = 0;
        for (var candidate : candidates) {
            if (candidate.vertices.size() <= 1) {
                continue;
            }

            if (checkIntersections(candidate, existedCandidates)) {
                continue;
            }

            existedCandidates.add(candidate);
            var line = new StraightLine(straightLineId);
            lines.add(line);

            int negativeShift = 0;
            line.shifts = new int[candidate.vertices.size()];
            for (int blockIndex = 0; blockIndex < candidate.vertices.size(); blockIndex++) {
                var block = candidate.vertices.get(blockIndex);
                line.vertices.add(block);
                block.straightLineId = straightLineId;

                // TODO: тут необходимо перетереть
                if (blockIndex > 0) {
                    var pair = candidate.edges.get(blockIndex - 1);
                    int prevShift = line.shifts[blockIndex - 1] + pair.sourceShift;
                    line.shifts[blockIndex] = prevShift - pair.targetShift;
                    if (negativeShift > line.shifts[blockIndex]) {
                        negativeShift = line.shifts[blockIndex];
                    }
                }

                if (blockIndex == 0) {
                    block.isStraightLineStartBlock = true;
                }
                if (blockIndex == candidate.vertices.size() - 1) {
                    block.isStraightLineFinishBlock = true;
                }
            }

            for (int shiftIndex = 0; shiftIndex < line.shifts.length; shiftIndex++) {
                line.shifts[shiftIndex] += (-negativeShift);
            }

            straightLineId++;
        }

        // TODO: считаем что к этому моменту все лайны не пересекаются, в противном случае возможны проблемы...
        while (rightWallAmount > 0) {
            for (int rowIndex = 0; rowIndex < vertices.length; rowIndex++) {
                int columnIndex = rightWallIndex[rowIndex];

                if (columnIndex < 0) {
                    continue;
                }

                var block = vertices[rowIndex][columnIndex];
                if (block.straightLineId >= 0) {
                    var line = lines.get(block.straightLineId);
                    if (!leftShiftLine(line)) {
                        if (VGConfigSettings.DIAGNOSTIC_MODE) {
                            log.debug("Can't move to left following line:");
                            for (var lineBlock : line.vertices) {
                                log.debug(lineBlock.toString());
                            }
                        }

                        line.shiftAttempt++;
                        // TODO: достаточно узкое место... необходимо пофиксить.
                        if (line.shiftAttempt > 500) {
                            line.vertices.forEach(v -> v.straightLineId = -1);
                        }
                    }
                } else {
                    leftShift(block, false);
                }
            }
        }

        // print lines.
        if (VGConfigSettings.DIAGNOSTIC_MODE) {
            log.debug("\n\nStraight lines: " + lines.size());
            for (var line : lines) {
                log.debug("\n========================================\n");
                log.debug(String.format("Line #%s: ", line.id));
                for (var block : line.vertices) {
                    log.debug(block + " : " + x[block.rowIndex][block.columnIndex] + " : " + block.size);
                }
                log.debug("========================================\n");
            }
        }

        setupCoordinates();

        log.debug("CalculateStraightLinesProcedure was finished.");
    }

    private boolean leftShiftLine(StraightLine line) {
        boolean check = true;
        for (var lineBlock : line.vertices) {
            check &= leftShift(lineBlock.rowIndex, lineBlock.columnIndex, true);
        }

        if (!check) {
            return false;
        }

        int leftMax = findLeftMaxForLine(line);
        for (int blockIndex = 0; blockIndex < line.vertices.size(); blockIndex++) {
            var lineBlock = line.vertices.get(blockIndex);

            int lineBlockX = leftMax + line.shifts[blockIndex];

            x[lineBlock.rowIndex][lineBlock.columnIndex] = lineBlockX;

            leftWallX[lineBlock.rowIndex] = lineBlockX + lineBlock.size;

            rightWallAmount--;

            rightWallIndex[lineBlock.rowIndex]++;

            if (rightWallIndex[lineBlock.rowIndex] >= vertices[lineBlock.rowIndex].length) {
                rightWallIndex[lineBlock.rowIndex] = -1;
            }
        }

        return true;
    }

    private int findLeftMaxForLine(StraightLine line) {
        int max = 0;
        for (var lineBlock : line.vertices) {
            int rowIndex = lineBlock.rowIndex;
            if (max < leftWallX[rowIndex]) {
                max = leftWallX[rowIndex];
            }
        }

        return max;
    }

    // TODO: необходимо переписать этот метод на более оптимальный, просто как прототип написан.
    private boolean checkIntersections(
            StraightLineCandidate candidate,
            List<StraightLineCandidate> existedCandidates) {
        for (int i = 0; i < candidate.vertices.size() - 1; i++) {
            var currVertex = candidate.vertices.get(i);
            var nextVertex = candidate.vertices.get(i + 1);

            for (var existLongEdge : existedCandidates) {
                for (int j = 0; j < existLongEdge.vertices.size()- 1; j++) {
                    var existCurrVertex = existLongEdge.vertices.get(j);
                    var existNextVertex = existLongEdge.vertices.get(j + 1);
                    if (existCurrVertex.rowIndex == currVertex.rowIndex && existNextVertex.rowIndex == nextVertex.rowIndex) {
                        if (currVertex.columnIndex < existCurrVertex.columnIndex && existNextVertex.columnIndex < nextVertex.columnIndex) {
                            return true;
                        }
                        if (currVertex.columnIndex > existCurrVertex.columnIndex && existNextVertex.columnIndex > nextVertex.columnIndex) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    private boolean leftShift(int rowIndex, int columnIndex, boolean dryRun) {
        if (columnIndex < 0) {
            return false;
        }

        return leftShift(vertices[rowIndex][columnIndex], dryRun);
    }

    private boolean leftShift(Vertex vertex, boolean dryRun) {
        int rowIndex = vertex.rowIndex;
        int columnIndex = vertex.columnIndex;

        if (x[rowIndex][columnIndex] < CENTER_VALUE) {
            return true;
        }

        if (columnIndex > 0) {
            int nextColumnIndex = columnIndex - 1;
            if (x[rowIndex][nextColumnIndex] > CENTER_VALUE) {
                return false;
            }
        }

        if (dryRun) {
            return true;
        }

        x[rowIndex][columnIndex] = leftWallX[rowIndex];
        leftWallX[rowIndex] += vertex.size;

        rightWallAmount--;

        rightWallIndex[rowIndex]++;

        if (rightWallIndex[rowIndex] >= vertices[rowIndex].length) {
            rightWallIndex[rowIndex] = -1;
        }

        return true;
    }
}
