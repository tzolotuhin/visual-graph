package vg.lib.layout.hierarchical.step2.custom;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.step2.data.CrossingReductionOperationResult;
import vg.lib.operation.Operation;
import vg.lib.operation.ProcedureWithOneArg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.BOTTOM_OF_CYCLE_VERTEX_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.COUNT_ADDITIONAL_CROSSES_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.COUNT_CROSSES_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.COUNT_CROSSES_WITH_TRAPS_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.COUNT_NEIGHBORHOOD_ANOMALIES_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.DUMMY_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.DUMMY_VERTEX_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_BOTTOM_TO_TOP_DIRECTION;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_DUMMY_TO_REAL_TOP_BEACON_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_DUMMY_TO_REAL_TOP_OF_CYCLE_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_REAL_TO_DUMMY_BOTTOM_BEACON_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_REAL_TO_DUMMY_BOTTOM_OF_CYCLE_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_TOP_TO_BOTTOM_DIRECTION;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.NUMBER_OF_FAILED_MOVES;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.REAL_AND_SUITABLE_FOR_CHAIN_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.REAL_EDGE_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.REAL_VERTEX_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.TOP_OF_CYCLE_VERTEX_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.UNKNOWN_DIRECTION;

@Slf4j
public class CustomCrossingReductionForOneGroupOperation implements Operation<CrossingReductionOperationResult> {
    // input arguments.
    private final List<UUID> vertexIds;
    private final Map<UUID, Map.Entry<UUID, UUID>> edgeIdToNodeIds;
    private final Map<UUID, Map.Entry<Integer, Integer>> edgeIdToPortIndices;
    private final Map<UUID, Integer> vertexIdToLayer;
    private final List<UUID> dummyVertexIds;
    private final Map<UUID, Integer> vertexIdToInitialOrder;
    private final Map<UUID, String> vertexIdToName;

    //region: Fields which were initiated in the 'init' method which will be called in execute method.
    private int numberOfVertices;
    private UUID[] vertexIndexToId;
    private boolean[] vertexIndexToIsDummy;

    // The order of two fixed vertices relative to each other must be immutable.
    private boolean[] vertexIndexToIsFixed;

    private boolean[] vertexIndexToDisabled;
    private int[] vertexIndexToType;

    private int[] vertexIndexToWeight;
    private int[][] weightToVertexIndices;
    private int[] weightToDirection;

    private int[] vertexIndexToLayer;
    private int[] cycleVertexIndexToParentVertexIndex;

    private int[][] layerToVertexIndices;

    private int numberOfEdges;
    private int[] edgeIndexToType;
    private int[] edgeIndexToSrcVertexIndex;
    private int[] edgeIndexToTrgVertexIndex;
    private int[] edgeIndexToSrcPortIndex;
    private int[] edgeIndexToTrgPortIndex;
    private int[][] vertexIndexToInputEdgeIndices;
    private int[][] vertexIndexToOutputEdgeIndices;

    private int[][] vertexIndexToInputVertexIndices;
    private int[][] vertexIndexToOutputVertexIndices;

    private int[][] layerToInputEdgeIndices;
    private int[][] layerToOutputEdgeIndices;

    private int maxLayer;

    private int numberOfLayers;
    //endregion

    //region: Fields related to chains.
    private int numberOfChains;
    private int[] chainIndexToStartVertexIndex;
    private int[] chainIndexToFinishVertexIndex;
    private int[] edgeIndexToChainIndex;
    // layer -> group of chainIndex * 10_000 + vertexIndex
    private long[][] layerToChainIndicesWithVertexIndices;
    //endregion

    //region: Fields related to traps.
    private int[][][] layerToTopTrapGroupsWithVertexIndices;
    private int[][][] layerToBottomTrapGroupsWithVertexIndices;
    private int[][] layerToTopTrapVertexIndices;
    private int[][] layerToBottomTrapVertexIndices;
    //endregion

    //region: Fields related to order.
    private int[] vertexIndexToOrder;
    private int[][] layerToOrderToVertexIndices;
    private int[][] layerToOrderToVertexIndicesBestSolution;
    private int[][] layerToOrderToVertexIndicesTemp1;
    private int[][] layerToOrderToVertexIndicesTemp2;

    private int[][] layerToVertexIndicesWithPriority;
    //endregion

    // Fields for optimization.
    private boolean[] _countChainsCrossesWithTrapsVertexIndexToBoolean;

    private int[] _orderLayerVertexIndicesStack;

    //region: Fields for diagnostics
    private boolean enableDiagnosticMode;
    private long countCrossesTime;
    private long countChainsCrossesWithTrapsTime;
    private long countNeighborhoodAnomaliesTime;
    private long orderLayerUnsuccessfulSwap;
    private long orderLayerSuccessfulSwap;
    //endregion

    public CustomCrossingReductionForOneGroupOperation(List<UUID> vertexIds,
                                                       Map<UUID, Map.Entry<UUID, UUID>> edgeIdToNodeIds,
                                                       Map<UUID, Map.Entry<Integer, Integer>> edgeIdToPortIndices,
                                                       Map<UUID, Integer> vertexIdToLayer,
                                                       List<UUID> dummyVertexIds,
                                                       Map<UUID, Integer> vertexIdToInitialOrder,
                                                       Map<UUID, String> vertexIdToName) {
        this.vertexIds = vertexIds;
        this.edgeIdToNodeIds = edgeIdToNodeIds;
        this.edgeIdToPortIndices = edgeIdToPortIndices;
        this.vertexIdToLayer = vertexIdToLayer;
        this.dummyVertexIds = dummyVertexIds;
        this.vertexIdToInitialOrder = vertexIdToInitialOrder;
        this.vertexIdToName = vertexIdToName;
    }

    @Override
    public CrossingReductionOperationResult execute() {
        init();

        // count metrics for initial order before some actions.
        var numberOfCrossesBefore = countCrosses();

        enableDiagnosticMode = VGConfigSettings.DIAGNOSTIC_MODE;
        var layerToNumberOfCrossesFromTopToBottomBefore = countLayerToNumberOfCrossesFromTopToBottom();
        var layerToNumberOfCrossesFromBottomToTopBefore = countLayerToNumberOfCrossesFromBottomToTop();
        var layerToNumberOfChainCrossesWithTopTrapsBefore = countLayerToNumberOfCrossesBetweenChainsAndTopTraps();
        var layerToNumberOfChainCrossesWithBottomTrapsBefore = countLayerToNumberOfCrossesBetweenChainsAndBottomTraps();
        var layerToNumberOfNeighborhoodAnomaliesBefore = countLayerToNumberOfNeighborhoodAnomalies();
        enableDiagnosticMode = false;

        // this preset should be used only for enabling new vertices.
        var prevLayerWeights = new long[][] {
                { COUNT_CROSSES_TYPE,                50_000 },
                { COUNT_NEIGHBORHOOD_ANOMALIES_TYPE,      1 }
        };

        // this preset should be used for finding better solution after enabling new vertices.
        var prevLayerAndChainsWeights = new long[][] {
                { COUNT_CROSSES_TYPE,            50_000_000_000_000L },
                { COUNT_CROSSES_WITH_TRAPS_TYPE,        500_000_000 },
                { COUNT_ADDITIONAL_CROSSES_TYPE,             50_000 },
                { COUNT_NEIGHBORHOOD_ANOMALIES_TYPE,              1 }
        };

        // this preset should be used for last step.
        var prevAndNextLayersWeights = new long[][] {
                { COUNT_CROSSES_TYPE,            1_000_000_000 },
                { COUNT_ADDITIONAL_CROSSES_TYPE, 1_000_000_000 },
                { COUNT_CROSSES_WITH_TRAPS_TYPE,        50_000 },
                { COUNT_NEIGHBORHOOD_ANOMALIES_TYPE,         1 }
        };

        // split vertices into groups.
        var splitVerticesIntoGroupsResult = new CustomSplitVerticesIntoGroups(
                numberOfVertices,
                vertexIndexToType,
                vertexIndexToInputVertexIndices,
                vertexIndexToOutputVertexIndices,
                vertexIndexToInputEdgeIndices,
                vertexIndexToOutputEdgeIndices,
                vertexIndexToLayer,
                numberOfEdges,
                edgeIndexToSrcVertexIndex,
                edgeIndexToTrgVertexIndex,
                edgeIndexToChainIndex,
                numberOfLayers,
                layerToVertexIndices,
                numberOfChains,
                chainIndexToStartVertexIndex,
                chainIndexToFinishVertexIndex,
                cycleVertexIndexToParentVertexIndex).execute();
        weightToVertexIndices = splitVerticesIntoGroupsResult.weightToVertexIndices();
        weightToDirection = splitVerticesIntoGroupsResult.weightToDirection();
        vertexIndexToWeight = splitVerticesIntoGroupsResult.vertexIndexToWeight();

        diagnosticPrintGroups();

        // do order for vertices and make passages.
        var startTime = System.currentTimeMillis();
        orderLayers(prevLayerWeights, prevLayerAndChainsWeights, prevAndNextLayersWeights);
        var crossesAfterOrderLayers = countCrosses();
        var orderLayersTime = (System.currentTimeMillis() - startTime);
        log.debug("""

                        Crosses after order layers = {},
                        countCrossesTime = {}ms
                        countChainsCrossesWithTrapsTime = {}ms
                        countNeighborhoodAnomaliesTime = {}ms
                        orderLayersTime={}ms

                        orderLayerSuccessfulSwap={}, orderLayerUnsuccessfulSwap={}, percent={}""",
                crossesAfterOrderLayers,
                countCrossesTime,
                countChainsCrossesWithTrapsTime,
                countNeighborhoodAnomaliesTime,
                orderLayersTime,
                orderLayerSuccessfulSwap,
                orderLayerUnsuccessfulSwap,
                orderLayerSuccessfulSwap /(float)(orderLayerSuccessfulSwap + orderLayerUnsuccessfulSwap + 1) * 100);

        var numberOfCrosses = countCrosses();

        return CrossingReductionOperationResult.builder()
                .numberOfVertices(numberOfVertices)
                .vertexIndexToId(vertexIndexToId)
                .vertexIndexToOrder(vertexIndexToOrder)
                .numberOfCrosses(numberOfCrosses)
                .vertexIndexToWeight(vertexIndexToWeight)
                .weightToVertexIndices(weightToVertexIndices)
                .edgeIndexToType(edgeIndexToType)
                .cycleVertexIndexToParentVertexIndex(cycleVertexIndexToParentVertexIndex)
                .chainIndexToStartVertexIndex(chainIndexToStartVertexIndex)
                .chainIndexToFinishVertexIndex(chainIndexToFinishVertexIndex)
                .layerToChainIndicesWithVertexIndices(layerToChainIndicesWithVertexIndices)
                .layerToTopTrapGroupsWithVertexIndices(layerToTopTrapGroupsWithVertexIndices)
                .layerToBottomTrapGroupsWithVertexIndices(layerToBottomTrapGroupsWithVertexIndices)
                .layerToTopTrapVertexIndices(layerToTopTrapVertexIndices)
                .layerToBottomTrapVertexIndices(layerToBottomTrapVertexIndices)
                .numberOfCrossesBefore(numberOfCrossesBefore)
                .layerToNumberOfCrossesFromTopToBottomBefore(layerToNumberOfCrossesFromTopToBottomBefore)
                .layerToNumberOfCrossesFromBottomToTopBefore(layerToNumberOfCrossesFromBottomToTopBefore)
                .layerToNumberOfChainCrossesWithTopTrapsBefore(layerToNumberOfChainCrossesWithTopTrapsBefore)
                .layerToNumberOfChainCrossesWithBottomTrapsBefore(layerToNumberOfChainCrossesWithBottomTrapsBefore)
                .layerToNumberOfNeighborhoodAnomaliesBefore(layerToNumberOfNeighborhoodAnomaliesBefore)
                .build();
    }

    private void saveToBestSolution() {
        for (var layer = 0; layer < numberOfLayers; layer++) {
            System.arraycopy(
                    layerToOrderToVertexIndices[layer], 0,
                    layerToOrderToVertexIndicesBestSolution[layer], 0,
                    layerToOrderToVertexIndices[layer].length);
        }
    }

    private void saveToTemp1() {
        for (var layer = 0; layer < numberOfLayers; layer++) {
            System.arraycopy(
                    layerToOrderToVertexIndices[layer], 0,
                    layerToOrderToVertexIndicesTemp1[layer], 0,
                    layerToOrderToVertexIndices[layer].length);
        }
    }

    private void saveToTemp2() {
        for (var layer = 0; layer < numberOfLayers; layer++) {
            System.arraycopy(
                    layerToOrderToVertexIndices[layer], 0,
                    layerToOrderToVertexIndicesTemp2[layer], 0,
                    layerToOrderToVertexIndices[layer].length);
        }
    }

    @SuppressWarnings("Duplicates")
    private void revertFromBestSolution() {
        for (var layer = 0; layer < numberOfLayers; layer++) {
            for (var order = 0; order < layerToOrderToVertexIndicesBestSolution[layer].length; order++) {
                var vertexIndex = layerToOrderToVertexIndicesBestSolution[layer][order];
                vertexIndexToOrder[vertexIndex] = order;
                layerToOrderToVertexIndices[layer][order] = layerToOrderToVertexIndicesBestSolution[layer][order];
            }
        }
    }

    @SuppressWarnings("Duplicates")
    private void revertFromTemp1() {
        for (var layer = 0; layer < numberOfLayers; layer++) {
            for (var order = 0; order < layerToOrderToVertexIndicesTemp1[layer].length; order++) {
                var vertexIndex = layerToOrderToVertexIndicesTemp1[layer][order];
                vertexIndexToOrder[vertexIndex] = order;
                layerToOrderToVertexIndices[layer][order] = layerToOrderToVertexIndicesTemp1[layer][order];
            }
        }
    }

    @SuppressWarnings("Duplicates")
    private void revertFromTemp2() {
        for (var layer = 0; layer < numberOfLayers; layer++) {
            for (var order = 0; order < layerToOrderToVertexIndicesTemp2[layer].length; order++) {
                var vertexIndex = layerToOrderToVertexIndicesTemp2[layer][order];
                vertexIndexToOrder[vertexIndex] = order;
                layerToOrderToVertexIndices[layer][order] = layerToOrderToVertexIndicesTemp2[layer][order];
            }
        }
    }

    private void init() {
        numberOfVertices = vertexIds.size();
        vertexIndexToId = new UUID[numberOfVertices];
        var vertexIdToIndex = new HashMap<UUID, Integer>();

        vertexIndexToLayer = new int[numberOfVertices];
        maxLayer = Integer.MIN_VALUE;

        for (int vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var vertexId = vertexIds.get(vertexIndex);

            vertexIndexToId[vertexIndex] = vertexId;
            vertexIdToIndex.put(vertexId, vertexIndex);

            // extract layer.
            int layer = vertexIdToLayer.get(vertexId);
            if (layer > maxLayer) {
                maxLayer = layer;
            }

            vertexIndexToLayer[vertexIndex] = layer;
        }

        if (maxLayer == Integer.MIN_VALUE) {
            throw new IllegalArgumentException("Map 'vertexIdToLayer' contains incorrect data.");
        }

        numberOfLayers = maxLayer + 1;

        // fill dummy vertices.
        vertexIndexToIsDummy = findVertexToIsDummy(dummyVertexIds, vertexIdToIndex);

        vertexIndexToIsFixed = findVertexIndexToIsFixed(vertexIdToInitialOrder, vertexIndexToId);
        vertexIndexToDisabled = new boolean[numberOfVertices];

        numberOfEdges = edgeIdToNodeIds.size();
        edgeIndexToSrcVertexIndex = new int[numberOfEdges];
        edgeIndexToTrgVertexIndex = new int[numberOfEdges];
        edgeIndexToSrcPortIndex = new int[numberOfEdges];
        edgeIndexToTrgPortIndex = new int[numberOfEdges];
        var tmpVertexIndexToInputVertexIndices = new HashMap<Integer, List<Integer>>();
        var tmpVertexIndexToOutputVertexIndices = new HashMap<Integer, List<Integer>>();
        var tmpVertexIndexToInputEdgeIndices = new HashMap<Integer, List<Integer>>();
        var tmpVertexIndexToOutputEdgeIndices = new HashMap<Integer, List<Integer>>();
        var tmpLayerToInputEdgeIndices = new HashMap<Integer, List<Integer>>();
        var tmpLayerToOutputEdgeIndices = new HashMap<Integer, List<Integer>>();
        int edgeIndex = 0;
        for (var edgeToNodesEntry : edgeIdToNodeIds.entrySet()) {
            var edgeId = edgeToNodesEntry.getKey();

            var srcVertexId = edgeToNodesEntry.getValue().getKey();
            var trgVertexId = edgeToNodesEntry.getValue().getValue();

            int srcIndex = vertexIdToIndex.get(srcVertexId);
            int trgIndex = vertexIdToIndex.get(trgVertexId);

            edgeIndexToSrcVertexIndex[edgeIndex] = srcIndex;
            edgeIndexToTrgVertexIndex[edgeIndex] = trgIndex;

            tmpVertexIndexToInputVertexIndices.putIfAbsent(trgIndex, new ArrayList<>());
            tmpVertexIndexToOutputVertexIndices.putIfAbsent(srcIndex, new ArrayList<>());

            tmpVertexIndexToInputEdgeIndices.putIfAbsent(trgIndex, new ArrayList<>());
            tmpVertexIndexToOutputEdgeIndices.putIfAbsent(srcIndex, new ArrayList<>());

            tmpVertexIndexToInputVertexIndices.get(trgIndex).add(srcIndex);
            tmpVertexIndexToOutputVertexIndices.get(srcIndex).add(trgIndex);

            tmpVertexIndexToInputEdgeIndices.get(trgIndex).add(edgeIndex);
            tmpVertexIndexToOutputEdgeIndices.get(srcIndex).add(edgeIndex);

            // extract port indices.
            var portIndices = edgeIdToPortIndices.getOrDefault(edgeId, null);
            edgeIndexToSrcPortIndex[edgeIndex] = -1;
            edgeIndexToTrgPortIndex[edgeIndex] = -1;
            if (portIndices != null) {
                edgeIndexToSrcPortIndex[edgeIndex] = portIndices.getKey();
                edgeIndexToTrgPortIndex[edgeIndex] = portIndices.getValue();
            }

            // fill temporary data for mapping layer to input and output edges.
            var srcLayer = vertexIndexToLayer[srcIndex];
            var trgLayer = vertexIndexToLayer[trgIndex];

            tmpLayerToInputEdgeIndices.putIfAbsent(trgLayer, new ArrayList<>());
            tmpLayerToOutputEdgeIndices.putIfAbsent(srcLayer, new ArrayList<>());

            tmpLayerToInputEdgeIndices.get(trgLayer).add(edgeIndex);
            tmpLayerToOutputEdgeIndices.get(srcLayer).add(edgeIndex);

            edgeIndex++;
        }

        vertexIndexToInputVertexIndices = new int[numberOfVertices][];
        vertexIndexToOutputVertexIndices = new int[numberOfVertices][];
        vertexIndexToInputEdgeIndices = new int[numberOfVertices][];
        vertexIndexToOutputEdgeIndices = new int[numberOfVertices][];
        for (int vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            vertexIndexToInputVertexIndices[vertexIndex] = tmpVertexIndexToInputVertexIndices
                    .getOrDefault(vertexIndex, List.of()).stream()
                    .mapToInt(Integer::intValue)
                    .toArray();

            // TODO: need to reduce duplicates.
            vertexIndexToOutputVertexIndices[vertexIndex] = tmpVertexIndexToOutputVertexIndices
                    .getOrDefault(vertexIndex, List.of()).stream()
                    .mapToInt(Integer::intValue)
                    .toArray();

            vertexIndexToInputEdgeIndices[vertexIndex] = tmpVertexIndexToInputEdgeIndices
                    .getOrDefault(vertexIndex, List.of()).stream()
                    .mapToInt(Integer::intValue)
                    .toArray();

            vertexIndexToOutputEdgeIndices[vertexIndex] = tmpVertexIndexToOutputEdgeIndices
                    .getOrDefault(vertexIndex, List.of()).stream()
                    .mapToInt(Integer::intValue)
                    .toArray();
        }

        // fill vertex types.
        vertexIndexToType = findVertexIndexToType();

        // find layer to vertex indices (initial order).
        layerToVertexIndices = findLayerToVertexIndices();

        // fill edge types.
        edgeIndexToType = findEdgeTypes();

        // fill disabled edge group ids.
        cycleVertexIndexToParentVertexIndex = findCycleVertexIndexToParentVertexIndex();

        // find chains and fields related to the chains.
        var extractChainsResult = new CustomExtractChains(
                vertexIndexToType,
                vertexIndexToInputEdgeIndices,
                vertexIndexToOutputEdgeIndices,
                vertexIndexToLayer,
                numberOfEdges,
                edgeIndexToType,
                edgeIndexToSrcVertexIndex,
                edgeIndexToTrgVertexIndex,
                numberOfLayers,
                layerToVertexIndices).execute();
        numberOfChains = extractChainsResult.numberOfChains();
        chainIndexToStartVertexIndex = extractChainsResult.chainIndexToStartVertexIndex();
        chainIndexToFinishVertexIndex = extractChainsResult.chainIndexToFinishVertexIndex();
        edgeIndexToChainIndex = extractChainsResult.edgeIndexToChainIndex();
        layerToChainIndicesWithVertexIndices = extractChainsResult.layerToChainIndicesWithVertexIndices();

        // initialize layerToOrderToVertexIndices, savedLayerToOrderToVertexIndices and vertexIndexToOrder.
        layerToOrderToVertexIndices = new int[numberOfLayers][];
        layerToOrderToVertexIndicesBestSolution = new int[numberOfLayers][];
        layerToOrderToVertexIndicesTemp1 = new int[numberOfLayers][];
        layerToOrderToVertexIndicesTemp2 = new int[numberOfLayers][];
        vertexIndexToOrder = new int[numberOfVertices];
        layerToVertexIndicesWithPriority = new int[numberOfLayers][];
        for (var layer = 0; layer < numberOfLayers; layer++) {
            layerToOrderToVertexIndices[layer] = new int[layerToVertexIndices[layer].length];
            layerToOrderToVertexIndicesBestSolution[layer] = new int[layerToVertexIndices[layer].length];
            layerToOrderToVertexIndicesTemp1[layer] = new int[layerToVertexIndices[layer].length];
            layerToOrderToVertexIndicesTemp2[layer] = new int[layerToVertexIndices[layer].length];
            layerToVertexIndicesWithPriority[layer] = new int[layerToVertexIndices[layer].length];
            Arrays.fill(layerToVertexIndicesWithPriority[layer], -1);

            for (var order = 0; order < layerToVertexIndices[layer].length; order++) {
                var vertexIndex = layerToVertexIndices[layer][order];
                layerToOrderToVertexIndices[layer][order] = vertexIndex;
                vertexIndexToOrder[vertexIndex] = order;
            }
        }

        layerToInputEdgeIndices = new int[numberOfLayers][];
        layerToOutputEdgeIndices = new int[numberOfLayers][];

        // fill mapping layer to input and output edges.
        for (int layer = 0; layer <= maxLayer; layer++) {
            layerToInputEdgeIndices[layer] = tmpLayerToInputEdgeIndices
                    .getOrDefault(layer, Collections.emptyList()).stream()
                    .mapToInt(Integer::intValue).toArray();
            layerToOutputEdgeIndices[layer] = tmpLayerToOutputEdgeIndices
                    .getOrDefault(layer, Collections.emptyList()).stream()
                    .mapToInt(Integer::intValue).toArray();
        }

        // find traps and fields related to the traps.
        var extractTraps = new CustomExtractTraps(
                numberOfVertices,
                vertexIndexToInputVertexIndices,
                vertexIndexToOutputVertexIndices,
                vertexIndexToLayer,
                numberOfLayers,
                layerToVertexIndices).execute();
        layerToTopTrapGroupsWithVertexIndices = extractTraps.layerToTopTrapGroupsWithVertexIndices();
        layerToBottomTrapGroupsWithVertexIndices = extractTraps.layerToBottomTrapGroupsWithVertexIndices();
        layerToTopTrapVertexIndices = extractTraps.layerToTopTrapVertexIndices();
        layerToBottomTrapVertexIndices = extractTraps.layerToBottomTrapVertexIndices();

        // initialize fields for optimization.
        _countChainsCrossesWithTrapsVertexIndexToBoolean = new boolean[numberOfVertices];
        _orderLayerVertexIndicesStack = new int[numberOfVertices];

        // temporary initialize...
        vertexIndexToWeight = new int[numberOfVertices];
        Arrays.fill(vertexIndexToWeight, 1);
    }

    private int tryToFindBetterSolution(long[][] weights) {
        saveToTemp1();

        var fromTopToBottom = tryToFindBetterSolution(FROM_TOP_TO_BOTTOM_DIRECTION, weights);
        saveToTemp2();

        revertFromTemp1();

        var fromBottomToTop = tryToFindBetterSolution(FROM_BOTTOM_TO_TOP_DIRECTION, weights);

        Validate.isTrue(fromTopToBottom.crossesBefore == fromBottomToTop.crossesBefore);

        // handle case where both solutions are not good.
        if (fromTopToBottom.crossesAfter >= fromTopToBottom.crossesBefore && fromBottomToTop.crossesAfter >= fromBottomToTop.crossesBefore) {
            revertFromTemp1();
            return fromTopToBottom.crossesBefore;
        }

        if (fromBottomToTop.crossesAfter <= fromTopToBottom.crossesAfter) {
            return fromBottomToTop.crossesAfter;
        } else {
            revertFromTemp2();
            return fromTopToBottom.crossesAfter;
        }
    }

    /**
     * Returns pair - number of crosses, the cost before, the cost after.
     */
    private record TryToFindBetterSolutionResult(int crossesBefore, int crossesAfter) {
    }

    private TryToFindBetterSolutionResult tryToFindBetterSolution(int startDirection, long[][] weights) {
        assert startDirection == FROM_TOP_TO_BOTTOM_DIRECTION || startDirection == FROM_BOTTOM_TO_TOP_DIRECTION;

        // calculate crosses before finding better solution.
        saveToBestSolution();
        var crossesBefore = countCrosses();
        var crossesAfter = crossesBefore;

        // find better solution.
        var currentDirection = startDirection;
        var currentIteration = 0;
        while (true) {
            doPassage(currentDirection, weights);
            if (currentDirection == FROM_TOP_TO_BOTTOM_DIRECTION) {
                currentDirection = FROM_BOTTOM_TO_TOP_DIRECTION;
            } else {
                currentDirection = FROM_TOP_TO_BOTTOM_DIRECTION;
            }

            var crosses = countCrosses();

            if (crosses < crossesAfter) {
                crossesAfter = crosses;
                saveToBestSolution();
                continue;
            } else if (currentIteration > CustomConfig.NUMBER_OF_FAILED_PASSAGES) {
                break;
            }

            currentIteration++;
        }

        revertFromBestSolution();

        return new TryToFindBetterSolutionResult(crossesBefore, crossesAfter);
    }

    /**
     * Order groups.
     */
    private void orderLayers(long[][] prevLayerWeights,
                             long[][] prevLayerAndChainsWeights,
                             long[][] prevAndNextLayersWeights) {
        orderLayerSuccessfulSwap = 0;
        orderLayerUnsuccessfulSwap = 0;

        var originalVertexIndexToIsFixed = Arrays.copyOf(vertexIndexToIsFixed, vertexIndexToIsFixed.length);

        Arrays.fill(vertexIndexToDisabled, true);

        // it's needed only for skipping slow finding solution which better than current.
        var lastNumberOfCrosses = 0;

        // function for marking handled vertices as fixed.
        ProcedureWithOneArg<Integer> markHandledVerticesAsFixed = weight -> {
            for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
                if (vertexIndexToWeight[vertexIndex] >= weight) {
                    vertexIndexToIsFixed[vertexIndex] = true;
                }
            }
        };

        ProcedureWithOneArg<Integer> markHandledVerticesAsNotFixed = weight -> {
            for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
                if (vertexIndexToWeight[vertexIndex] == weight && !originalVertexIndexToIsFixed[vertexIndex]) {
                    vertexIndexToIsFixed[vertexIndex] = false;
                }
            }
        };

        var maxWeight = weightToVertexIndices.length - 1;
        for (var weight = maxWeight; weight >= 0; weight--) {
            log.debug("Enable {}/{} group of vertices, number of vertices in the group = {}.",
                    maxWeight - weight, maxWeight, weightToVertexIndices[weight].length);

            var direction = weightToDirection[weight];

            var count = 0;
            if (direction == FROM_TOP_TO_BOTTOM_DIRECTION) {
                for (var layer = 0; layer < numberOfLayers; layer++) {
                    count += enableSuitableVertices(layer, FROM_TOP_TO_BOTTOM_DIRECTION, weight, prevLayerAndChainsWeights);
                }
            } else if (direction == FROM_BOTTOM_TO_TOP_DIRECTION) {
                for (var layer = numberOfLayers - 1; layer >= 0; layer--) {
                    count += enableSuitableVertices(layer, FROM_BOTTOM_TO_TOP_DIRECTION, weight, prevLayerAndChainsWeights);
                }
            } else {
                for (var layer = numberOfLayers - 1; layer >= 0; layer--) {
                    count += enableSuitableVertices(layer, FROM_BOTTOM_TO_TOP_DIRECTION, weight, prevLayerAndChainsWeights);
                }
            }

            Validate.isTrue(weightToVertexIndices[weight].length == count);

            markHandledVerticesAsNotFixed.execute(weight);

            // try to find better solution.
            var fastCrosses0 = tryToFindBetterSolution(prevLayerWeights);
            var fastCrosses1 = tryToFindBetterSolution(prevLayerAndChainsWeights);
            var fastCrosses2 = tryToFindBetterSolution(prevAndNextLayersWeights);

            // optimization, skip slow finding solution which better than current.
            if (fastCrosses2 <= lastNumberOfCrosses) {
                log.debug("Skip slow finding better solution which better than current, weight = {}.", weight);
                lastNumberOfCrosses = fastCrosses2;
                markHandledVerticesAsFixed.execute(weight);
                continue;
            }

            System.arraycopy(
                    originalVertexIndexToIsFixed, 0,
                    vertexIndexToIsFixed, 0,
                    vertexIndexToIsFixed.length);

            // try to find better solution.
            var slowCrosses0 = tryToFindBetterSolution(prevLayerWeights);
            var slowCrosses1 = tryToFindBetterSolution(prevLayerAndChainsWeights);
            var slowCrosses2 = tryToFindBetterSolution(prevAndNextLayersWeights);

            // mark handled vertices as fixed.
            markHandledVerticesAsFixed.execute(weight);

            log.debug("weight = {}, fast crosses = {} -> {} -> {}, slow crosses = {} -> {} -> {}",
                    weight, fastCrosses0, fastCrosses1, fastCrosses2, slowCrosses0, slowCrosses1, slowCrosses2);

            lastNumberOfCrosses = slowCrosses2;
        }

        // rollback vertexIndexToIsFixed to original.
        System.arraycopy(
                originalVertexIndexToIsFixed, 0,
                vertexIndexToIsFixed, 0,
                vertexIndexToIsFixed.length);
    }

    /**
     * Enables suitable vertices.
     */
    private int enableSuitableVertices(int layer, int direction, int currentWeight, long[][] weights) {
        var currentOrder = layerToFirstDisabledVertexOrder(layer);

        // find parent layer.
        var parentLayer = layer - 1;
        if (direction == FROM_BOTTOM_TO_TOP_DIRECTION) {
            parentLayer = layer + 1;
        }

        // find parent vertices.
        int[] parentVertexIndices;
        if (parentLayer < 0 || parentLayer >= numberOfLayers) {
            parentVertexIndices = new int[0];
        } else {
            parentVertexIndices = layerToOrderToVertexIndices[parentLayer];
        }

        var result = 0;
        while (true) {
            var parentVertexIndex = findNextParentVertexIndexForEnabling(parentVertexIndices, direction, currentWeight);

            if (parentVertexIndex < 0) {
                break;
            }

            int[] vertexIndices;
            if (direction == FROM_TOP_TO_BOTTOM_DIRECTION) {
                vertexIndices = vertexIndexToOutputVertexIndices[parentVertexIndex];
            } else {
                vertexIndices = vertexIndexToInputVertexIndices[parentVertexIndex];
            }

            while (true) {
                var vertexIndex = findNextVertexIndexForEnabling(vertexIndices, currentWeight);

                if (vertexIndex < 0) {
                    break;
                }

                var fromVertexOrder = vertexIndexToOrder[vertexIndex];
                var toVertexOrder = findVertexOrderForEnabling(vertexIndex, layer, currentOrder);

                move(layer, fromVertexOrder, toVertexOrder);

                vertexIndexToDisabled[vertexIndex] = false;

                layerToVertexIndicesWithPriority[layer][currentOrder] = vertexIndex;

                orderLayer(layer, direction, weights, Integer.MAX_VALUE);

                vertexIndexToIsFixed[vertexIndex] = true;

                currentOrder++;
                result++;
            }
        }

        // add vertices with no parents.
        for (var vertexIndex : layerToOrderToVertexIndices[layer]) {
            if (!vertexIndexToDisabled[vertexIndex]) {
                continue;
            }

            if (vertexIndexToWeight[vertexIndex] != currentWeight) {
                continue;
            }

            var fromVertexOrder = vertexIndexToOrder[vertexIndex];
            var toVertexOrder = findVertexOrderForEnabling(vertexIndex, layer, currentOrder);

            move(layer, fromVertexOrder, toVertexOrder);

            vertexIndexToDisabled[vertexIndex] = false;

            layerToVertexIndicesWithPriority[layer][currentOrder] = vertexIndex;

            orderLayer(layer, direction, weights, Integer.MAX_VALUE);

            vertexIndexToIsFixed[vertexIndex] = true;

            currentOrder++;
            result++;
        }

        return result;
    }

    /**
     * Find more suitable parent vertex index for enabling, otherwise returns -1.
     */
    private int findNextParentVertexIndexForEnabling(int[] parentVertexIndices, int direction, int currentWeight) {
        var suitableParentVertexIndex = -1;
        var suitableParentVertexIndexToWeight = Integer.MIN_VALUE;
        for (var parentVertexIndex : parentVertexIndices) {
            if (vertexIndexToDisabled[parentVertexIndex]) {
                continue;
            }

            int[] vertexIndices;
            if (direction == FROM_TOP_TO_BOTTOM_DIRECTION) {
                vertexIndices = vertexIndexToOutputVertexIndices[parentVertexIndex];
            } else {
                vertexIndices = vertexIndexToInputVertexIndices[parentVertexIndex];
            }

            int weight = 0;
            boolean check = false;
            for (var vertexIndex : vertexIndices) {
                if (!vertexIndexToDisabled[vertexIndex]) {
                    weight += 1_000;
                    continue;
                }

                if (vertexIndexToWeight[vertexIndex] == currentWeight) {
                    weight += 1;
                    check = true;
                }
            }

            if (!check) {
                continue;
            }

            if (weight > suitableParentVertexIndexToWeight) {
                suitableParentVertexIndexToWeight = weight;
                suitableParentVertexIndex = parentVertexIndex;
            }
        }

        return suitableParentVertexIndex;
    }

    /**
     * Find more suitable vertex index for enabling, otherwise returns -1.
     */
    private int findNextVertexIndexForEnabling(int[] vertexIndices, int currentWeight) {
        var suitableVertexIndex = -1;
        var suitableVertexIndexToWeight = Integer.MIN_VALUE;
        for (var vertexIndex : vertexIndices) {
            if (!vertexIndexToDisabled[vertexIndex]) {
                continue;
            }

            if (vertexIndexToWeight[vertexIndex] != currentWeight) {
                continue;
            }

            var weightInput = 0;
            for (var inputVertexIndex : vertexIndexToInputVertexIndices[vertexIndex]) {
                if (vertexIndexToDisabled[inputVertexIndex]) {
                    continue;
                }
                weightInput += vertexIndexToWeight[inputVertexIndex];
            }

            var weightOutput = 0;
            for (var outputVertexIndex : vertexIndexToOutputVertexIndices[vertexIndex]) {
                if (vertexIndexToDisabled[outputVertexIndex]) {
                    continue;
                }
                weightOutput += vertexIndexToWeight[outputVertexIndex];
            }

            int weight = weightInput + weightOutput;

            if (weight > suitableVertexIndexToWeight) {
                suitableVertexIndexToWeight = weight;
                suitableVertexIndex = vertexIndex;
            }
        }

        return suitableVertexIndex;
    }

    /**
     * Find more suitable vertex order for enabling.
     */
    private int findVertexOrderForEnabling(int vertexIndex, int layer, int maxVertexOrder) {
        if (!vertexIndexToIsFixed[vertexIndex]) {
            return maxVertexOrder;
        }

        var vertexId = vertexIndexToId[vertexIndex];
        var initialVertexOrder = vertexIdToInitialOrder.get(vertexId);

        assert initialVertexOrder != null && initialVertexOrder >= 0;

        var result = maxVertexOrder;
        for (var vertexOrder = maxVertexOrder; vertexOrder >= 0; vertexOrder--) {
            var itVertexIndex = layerToOrderToVertexIndices[layer][vertexOrder];
            var itVertexId = vertexIndexToId[itVertexIndex];
            var itInitialVertexOrder = vertexIdToInitialOrder.get(itVertexId);

            if (itInitialVertexOrder == null || itInitialVertexOrder < 0) {
                continue;
            }

            if (itInitialVertexOrder > initialVertexOrder) {
                result = vertexOrder;
            }
        }

        return result;
    }

    //region: Code block with methods that refer to passages.
    private void doPassage(int direction, long[][] weights) {
        assert direction == FROM_TOP_TO_BOTTOM_DIRECTION || direction == FROM_BOTTOM_TO_TOP_DIRECTION;

        if (direction == FROM_TOP_TO_BOTTOM_DIRECTION) {
            for (var layer = 0; layer < numberOfLayers; layer++) {
                orderLayer(layer, direction, weights, NUMBER_OF_FAILED_MOVES);            }
        } else {
            for (var layer = maxLayer; layer >= 0; layer--) {
                orderLayer(layer, direction, weights, NUMBER_OF_FAILED_MOVES);
            }
        }
    }

    private void orderLayer(int layer, int direction, long[][] weights, int numberOfFailedMoves) {
        assert direction == FROM_TOP_TO_BOTTOM_DIRECTION || direction == FROM_BOTTOM_TO_TOP_DIRECTION;

        if (direction == FROM_TOP_TO_BOTTOM_DIRECTION) {
            orderLayer(
                    layer,
                    layerToInputEdgeIndices[layer],
                    layerToOutputEdgeIndices[layer],
                    weights,
                    numberOfFailedMoves);
        } else {
            orderLayer(
                    layer,
                    layerToOutputEdgeIndices[layer],
                    layerToInputEdgeIndices[layer],
                    weights,
                    numberOfFailedMoves);
        }
    }

    @SuppressWarnings("Duplicates")
    private void orderLayer(int layer,
                            int[] edgeIndices,
                            int[] additionalEdgeIndices,
                            long[][] weights,
                            int numberOfFailedMoves) {
        var currCost = calculateCost(layer, edgeIndices, additionalEdgeIndices, weights);

        if (currCost == 0) {
            // do nothing.
            return;
        }

        // check that initial order is right.
        var maxRightVertexOrder = layerToFirstDisabledVertexOrder(layer);

        if (maxRightVertexOrder <= 1) {
            // do nothing.
            return;
        }

        var currNumberOfFails = 0;
        var isFoundBetterSolution = true;
        while (isFoundBetterSolution) {
            isFoundBetterSolution = false;

            // move suitable vertex indices to stack.
            var _orderLayerVertexIndicesStackIndex = 0;
            for (var initialVertexOrder = 0; initialVertexOrder < maxRightVertexOrder; initialVertexOrder++) {
                var initialVertexIndex = layerToVertexIndicesWithPriority[layer][initialVertexOrder];

                // vertex should be enabled here.
                assert initialVertexIndex != -1;
                assert !vertexIndexToDisabled[initialVertexIndex];

                // skip fixed vertex, because we can't swap two fixed vertices.
                if (vertexIndexToIsFixed[initialVertexIndex]) {
                    continue;
                }

                _orderLayerVertexIndicesStack[_orderLayerVertexIndicesStackIndex++] = initialVertexIndex;
            }

            // do passage and use vertex indices from the stack.
            while (_orderLayerVertexIndicesStackIndex > 0) {
                _orderLayerVertexIndicesStackIndex--;

                var vertexIndex = _orderLayerVertexIndicesStack[_orderLayerVertexIndicesStackIndex];
                var initialVertexOrder = vertexIndexToOrder[vertexIndex];
                var vertexOrder = initialVertexOrder;

                currNumberOfFails = 0;
                for (var order = initialVertexOrder + 1; order < maxRightVertexOrder; order++) {
                    move(layer, vertexOrder, order);

                    var costAfterSwapping = calculateCost(layer, edgeIndices, additionalEdgeIndices, weights, currCost);

                    if (costAfterSwapping < currCost) {
                        // collect statistics.
                        orderLayerSuccessfulSwap++;

                        currCost = costAfterSwapping;
                        isFoundBetterSolution = true;
                        vertexOrder = order;
                    } else {
                        // collect statistics.
                        orderLayerUnsuccessfulSwap++;

                        currNumberOfFails++;
                        move(layer, order, vertexOrder);
                        if (currNumberOfFails >= numberOfFailedMoves) {
                            break;
                        }
                    }
                }

                if (isFoundBetterSolution) {
                    continue;
                }

                currNumberOfFails = 0;
                for (var order = initialVertexOrder - 1; order >= 0; order--) {
                    move(layer, vertexOrder, order);

                    var costAfterSwapping = calculateCost(layer, edgeIndices, additionalEdgeIndices, weights, currCost);

                    if (costAfterSwapping < currCost) {
                        // collect statistics.
                        orderLayerSuccessfulSwap++;

                        currCost = costAfterSwapping;
                        isFoundBetterSolution = true;
                        vertexOrder = order;
                    } else {
                        // collect statistics.
                        orderLayerUnsuccessfulSwap++;

                        currNumberOfFails++;
                        move(layer, order, vertexOrder);
                        if (currNumberOfFails >= numberOfFailedMoves) {
                            break;
                        }
                    }
                }
            }

            if (currCost == 0) {
                // stop the cycle.
                break;
            }
        }
    }
    //endregion

    private void swap(int layer, int vertexOrder1, int vertexOrder2) {
        var vertexIndex1 = layerToOrderToVertexIndices[layer][vertexOrder1];
        var vertexIndex2 = layerToOrderToVertexIndices[layer][vertexOrder2];

        vertexIndexToOrder[vertexIndex1] = vertexOrder2;
        vertexIndexToOrder[vertexIndex2] = vertexOrder1;

        layerToOrderToVertexIndices[layer][vertexOrder1] = vertexIndex2;
        layerToOrderToVertexIndices[layer][vertexOrder2] = vertexIndex1;
    }

    private void move(int layer, int fromVertexOrder, int toVertexOrder) {
        if (fromVertexOrder < toVertexOrder) {
            for (var order = fromVertexOrder; order < toVertexOrder; order++) {
                swap(layer, order, order + 1);
            }
        } else {
            for (var order = fromVertexOrder; order > toVertexOrder; order--) {
                swap(layer, order, order - 1);
            }
        }
    }

    private long calculateCost(int layer,
                              int[] edgeIndices,
                              int[] additionalEdgeIndices,
                              long[][] weights) {
        return calculateCost(layer, edgeIndices, additionalEdgeIndices, weights, Long.MAX_VALUE);
    }

    private long calculateCost(int layer,
                               int[] edgeIndices,
                               int[] additionalEdgeIndices,
                               long[][] weights,
                               long currentCost) {
        var result = 0L;
        for (var weight : weights) {
            var re = Math.multiplyExact(weight[1], switch ((int) weight[0]) {
                case COUNT_CROSSES_TYPE -> countCrosses(edgeIndices);
                case COUNT_ADDITIONAL_CROSSES_TYPE -> countCrosses(additionalEdgeIndices);
                case COUNT_CROSSES_WITH_TRAPS_TYPE -> countChainsCrossesWithTraps(layer, UNKNOWN_DIRECTION);
                case COUNT_NEIGHBORHOOD_ANOMALIES_TYPE -> countNeighborhoodAnomalies(layer);
                default -> throw new IllegalStateException("Unexpected value: " + weight[0]);
            });

            result = Math.addExact(result, re);

            if (result > currentCost) {
                break;
            }
        }

        return result;
    }

    private int countChainsCrossesWithTraps(int layer, int direction) {
        var startTime = System.currentTimeMillis();
        var result = 0;
        for (var chainIndexWithVertexIndex : layerToChainIndicesWithVertexIndices[layer]) {
            // get the index of the chain and the vertex at a given layer through which this chain passes.
            var chainIndex = (int)(chainIndexWithVertexIndex / 10_000);
            var chainVertexIndex = (int)(chainIndexWithVertexIndex % 10_000);

            if (vertexIndexToDisabled[chainVertexIndex]) {
                continue;
            }

            var chainVertexOrder = vertexIndexToOrder[chainVertexIndex];

            var topTrapGroupIndexToTrapVertexIndex = layerToTopTrapVertexIndices[layer];
            if (topTrapGroupIndexToTrapVertexIndex != null
                    && topTrapGroupIndexToTrapVertexIndex.length > 0
                    && (direction & FROM_BOTTOM_TO_TOP_DIRECTION) > 0) {
                var topTrapGroupsWithVertexIndices = layerToTopTrapGroupsWithVertexIndices[layer];

                Arrays.fill(_countChainsCrossesWithTrapsVertexIndexToBoolean, false);
                result += countChainCrossesWithTraps(
                        layer,
                        FROM_BOTTOM_TO_TOP_DIRECTION,
                        chainIndex,
                        chainVertexOrder,
                        topTrapGroupIndexToTrapVertexIndex,
                        topTrapGroupsWithVertexIndices,
                        _countChainsCrossesWithTrapsVertexIndexToBoolean);
            }

            var bottomTrapGroupIndexToTrapVertexIndex = layerToBottomTrapVertexIndices[layer];
            if (bottomTrapGroupIndexToTrapVertexIndex != null
                    && bottomTrapGroupIndexToTrapVertexIndex.length > 0
                    && (direction & FROM_TOP_TO_BOTTOM_DIRECTION) > 0) {
                var bottomTrapGroupsWithVertexIndices = layerToBottomTrapGroupsWithVertexIndices[layer];

                Arrays.fill(_countChainsCrossesWithTrapsVertexIndexToBoolean, false);
                result += countChainCrossesWithTraps(
                        layer,
                        FROM_TOP_TO_BOTTOM_DIRECTION,
                        chainIndex,
                        chainVertexOrder,
                        bottomTrapGroupIndexToTrapVertexIndex,
                        bottomTrapGroupsWithVertexIndices,
                        _countChainsCrossesWithTrapsVertexIndexToBoolean);
            }
        }

        countChainsCrossesWithTrapsTime += (System.currentTimeMillis() - startTime);

        return result;
    }

    private int countChainCrossesWithTraps(int layer,
                                           int direction,
                                           int chainIndex,
                                           int chainVertexOrder,
                                           int[] trapGroupIndexToTrapVertexIndex,
                                           int[][] trapGroupsWithVertexIndices,
                                           boolean[] _vertexIndexToIsVisited) {
        var result = 0;
        for (var trapGroupIndex = 0; trapGroupIndex < trapGroupIndexToTrapVertexIndex.length; trapGroupIndex++) {
            var trapVertexIndex = trapGroupIndexToTrapVertexIndex[trapGroupIndex];
            var trapVertexType = vertexIndexToType[trapVertexIndex];
            var trapVertexLayer = vertexIndexToLayer[trapVertexIndex];

            if (direction == FROM_TOP_TO_BOTTOM_DIRECTION && trapVertexLayer < layer) {
                continue;
            }

            if (direction == FROM_BOTTOM_TO_TOP_DIRECTION && trapVertexLayer > layer) {
                continue;
            }

            var trapGroupVertexIndices = trapGroupsWithVertexIndices[trapGroupIndex];

            // skip the group if the number of vertices is less than two.
            if (trapGroupVertexIndices.length < 2) {
                continue;
            }

            var startChainVertexIndex = chainIndexToStartVertexIndex[chainIndex];
            var finishChainVertexIndex = chainIndexToFinishVertexIndex[chainIndex];
            var startChainVertexLayer = vertexIndexToLayer[startChainVertexIndex];
            var finishChainVertexLayer = vertexIndexToLayer[finishChainVertexIndex];

            // skip the trap if layer of this trap has located outside the boundaries of the chain.
            if (trapVertexLayer < startChainVertexLayer || trapVertexLayer > finishChainVertexLayer) {
                continue;
            }

            // skip the trap if the chain has been started of finished in the trap vertex.
            if (startChainVertexIndex == trapVertexIndex || finishChainVertexIndex == trapVertexIndex) {
                continue;
            }

            // skip the trap which is part of a cycle and the chain is also part of the same cycle.
            if (trapVertexType == BOTTOM_OF_CYCLE_VERTEX_TYPE || trapVertexType == TOP_OF_CYCLE_VERTEX_TYPE) {
                var parentTrapVertexIndex = cycleVertexIndexToParentVertexIndex[trapVertexIndex];

                var parentStartChainVertexIndex = cycleVertexIndexToParentVertexIndex[startChainVertexIndex];
                var parentFinishChainVertexIndex = cycleVertexIndexToParentVertexIndex[finishChainVertexIndex];

                if (parentTrapVertexIndex == parentStartChainVertexIndex || parentTrapVertexIndex == parentFinishChainVertexIndex) {
                    continue;
                }
            }

            if (isChainIntersectedWithTrap(chainIndex, chainVertexOrder, trapGroupVertexIndices, _vertexIndexToIsVisited)) {
                result++;
                if (enableDiagnosticMode) {
                    log.debug("Chain {} ({} -> {}) has been intersected with trap {} on layer {}, direction {}.",
                            chainIndex,
                            startChainVertexIndex,
                            finishChainVertexIndex,
                            trapVertexIndex,
                            layer,
                            CustomConfig.directionToString(direction));
                }
            }
        }

        return result;
    }

    private boolean isChainIntersectedWithTrap(int chainIndex,
                                               int chainVertexOrder,
                                               int[] trapGroupVertexIndices,
                                               boolean[] _vertexIndexToIsVisited) {
        var left = false;
        var right = false;
        for (var vertexIndex : trapGroupVertexIndices) {
            if (_vertexIndexToIsVisited[vertexIndex]) {
                continue;
            }

            if (vertexIndexToDisabled[vertexIndex]) {
                continue;
            }

            var vertexOrder = vertexIndexToOrder[vertexIndex];
            var vertexType = vertexIndexToType[vertexIndex];

            // skip the vertex (bottom of cycle or top of cycle) if the vertex and the chain have the same parent.
            if (vertexType == BOTTOM_OF_CYCLE_VERTEX_TYPE || vertexType == TOP_OF_CYCLE_VERTEX_TYPE) {
                var parentVertexIndex = cycleVertexIndexToParentVertexIndex[vertexIndex];

                var startChainVertexIndex = chainIndexToStartVertexIndex[chainIndex];
                var finishChainVertexIndex = chainIndexToFinishVertexIndex[chainIndex];

                var parentStartChainVertexIndex = cycleVertexIndexToParentVertexIndex[startChainVertexIndex];
                var parentFinishChainVertexIndex = cycleVertexIndexToParentVertexIndex[finishChainVertexIndex];
                if (parentVertexIndex == parentStartChainVertexIndex || parentVertexIndex == parentFinishChainVertexIndex) {
                    continue;
                }
            }

            if (chainVertexOrder < vertexOrder) {
                right = true;
            }
            if (chainVertexOrder > vertexOrder) {
                left = true;
            }

            // skip the vertex if the chain and the trap have common parent.
            if (chainVertexOrder == vertexOrder) {
                continue;
            }

            if (left && right) {
                break;
            }
        }

        if (left && right) {
            for (var vertexIndex : trapGroupVertexIndices) {
                _vertexIndexToIsVisited[vertexIndex] = true;
            }
        }

        return left && right;
    }

    private long[] countLayerToNumberOfCrossesBetweenChainsAndBottomTraps() {
        log.debug("[STARTED] Count layer -> number of crosses between chains and bottom traps.");
        var result = new long[numberOfLayers];
        for (var layer = 0; layer <= maxLayer; layer++) {
            result[layer] = countChainsCrossesWithTraps(layer, FROM_TOP_TO_BOTTOM_DIRECTION);
        }

        log.debug("[FINISHED] Count layer -> number of crosses between chains and bottom traps.");

        return result;
    }

    private long[] countLayerToNumberOfCrossesBetweenChainsAndTopTraps() {
        log.debug("[STARTED] Count layer -> number of crosses between chains and top traps.");
        var result = new long[numberOfLayers];
        for (var layer = maxLayer; layer >= 0; layer--) {
            result[layer] = countChainsCrossesWithTraps(layer, FROM_BOTTOM_TO_TOP_DIRECTION);
        }

        log.debug("[FINISHED] Count layer -> number of crosses between chains and top traps.");

        return result;
    }

    private long[] countLayerToNumberOfNeighborhoodAnomalies() {
        var layerToNumberOfNeighborhoodAnomalies = new long[numberOfLayers];
        for (var layer = 0; layer <= maxLayer; layer++) {
            layerToNumberOfNeighborhoodAnomalies[layer] = countNeighborhoodAnomalies(layer);
        }

        return layerToNumberOfNeighborhoodAnomalies;
    }

    @SuppressWarnings("Duplicates")
    private long countNeighborhoodAnomalies(int layer) {
        var startTime = System.currentTimeMillis();
        var result = 0;
        if (layer - 1 >= 0) {
            for (var vertexIndex : layerToVertexIndices[layer - 1]) {
                if (vertexIndexToDisabled[vertexIndex]) {
                    continue;
                }

                var outputVertexIndices = vertexIndexToOutputVertexIndices[vertexIndex];
                var maxOrder = Integer.MIN_VALUE;
                var minOrder = Integer.MAX_VALUE;
                var number = 0;
                for (var outputVertexIndex : outputVertexIndices) {
                    if (vertexIndexToDisabled[outputVertexIndex]) {
                        continue;
                    }

                    var order = vertexIndexToOrder[outputVertexIndex];
                    if (order < minOrder) {
                        minOrder = order;
                    }
                    if (order > maxOrder) {
                        maxOrder = order;
                    }
                    number++;
                }

                if (number <= 1) {
                    continue;
                }

                result += (maxOrder - minOrder - number + 1) * vertexIndexToWeight[vertexIndex];
            }
        }

        if (layer + 1 <= maxLayer) {
            for (var vertexIndex : layerToVertexIndices[layer + 1]) {
                if (vertexIndexToDisabled[vertexIndex]) {
                    continue;
                }

                var inputVertexIndices = vertexIndexToInputVertexIndices[vertexIndex];
                var maxOrder = Integer.MIN_VALUE;
                var minOrder = Integer.MAX_VALUE;
                var number = 0;
                for (var inputVertexIndex : inputVertexIndices) {
                    if (vertexIndexToDisabled[inputVertexIndex]) {
                        continue;
                    }

                    var order = vertexIndexToOrder[inputVertexIndex];
                    if (order < minOrder) {
                        minOrder = order;
                    }
                    if (order > maxOrder) {
                        maxOrder = order;
                    }
                    number++;
                }

                if (number <= 1) {
                    continue;
                }

                result += (maxOrder - minOrder - number + 1) * vertexIndexToWeight[vertexIndex];
            }
        }

        countNeighborhoodAnomaliesTime += (System.currentTimeMillis() - startTime);

        if (result >= 50_000) {
            log.warn("countNeighborhoodAnomalies result = {}", result);
            result = 49_999;
        }

        return result;
    }

    private int countCrosses() {
        var crosses = 0;
        for (var layer = 0; layer < maxLayer; layer++) {
            crosses += countCrosses(layerToOutputEdgeIndices[layer]);
        }

        return crosses;
    }

    private long[] countLayerToNumberOfCrossesFromTopToBottom() {
        var result = new long[numberOfLayers];
        for (var layer = 0; layer <= maxLayer; layer++) {
            result[layer] = countCrosses(layerToInputEdgeIndices[layer]);
        }

        return result;
    }

    private long[] countLayerToNumberOfCrossesFromBottomToTop() {
        var result = new long[numberOfLayers];
        for (var layer = 0; layer <= maxLayer; layer++) {
            result[layer] = countCrosses(layerToOutputEdgeIndices[layer]);
        }

        return result;
    }

    /**
     * @return number of crosses between the edges
     */
    @SuppressWarnings("Duplicates")
    private int countCrosses(int[] edgeIndices) {
        var startTime = System.currentTimeMillis();
        var result = 0;
        for (int i = 0; i < edgeIndices.length; i++) {
            var edgeIndex1 = edgeIndices[i];

            for (int j = i + 1; j < edgeIndices.length; j++) {
                var edgeIndex2 = edgeIndices[j];

                var srcVertexIndex1 = edgeIndexToSrcVertexIndex[edgeIndex1];
                var trgVertexIndex1 = edgeIndexToTrgVertexIndex[edgeIndex1];

                var srcPortIndex1 = edgeIndexToSrcPortIndex[edgeIndex1];
                var trgPortIndex1 = edgeIndexToTrgPortIndex[edgeIndex1];

                var srcVertexOrder1 = vertexIndexToOrder[srcVertexIndex1];
                var trgVertexOrder1 = vertexIndexToOrder[trgVertexIndex1];

                var srcVertexIndex2 = edgeIndexToSrcVertexIndex[edgeIndex2];
                var trgVertexIndex2 = edgeIndexToTrgVertexIndex[edgeIndex2];

                var srcPortIndex2 = edgeIndexToSrcPortIndex[edgeIndex2];
                var trgPortIndex2 = edgeIndexToTrgPortIndex[edgeIndex2];

                var srcVertexOrder2 = vertexIndexToOrder[srcVertexIndex2];
                var trgVertexOrder2 = vertexIndexToOrder[trgVertexIndex2];

                if (vertexIndexToDisabled[srcVertexIndex1]
                        || vertexIndexToDisabled[srcVertexIndex2]
                        || vertexIndexToDisabled[trgVertexIndex1]
                        || vertexIndexToDisabled[trgVertexIndex2]) {
                    continue;
                }

                // handle case when the first edge and the second edge are parts of cycles.
                if ((edgeIndexToType[edgeIndex1] == FROM_DUMMY_TO_REAL_TOP_OF_CYCLE_EDGE_TYPE
                    || edgeIndexToType[edgeIndex2] == FROM_DUMMY_TO_REAL_TOP_OF_CYCLE_EDGE_TYPE)
                    && cycleVertexIndexToParentVertexIndex[srcVertexIndex1]
                    == cycleVertexIndexToParentVertexIndex[srcVertexIndex2]
                    && cycleVertexIndexToParentVertexIndex[srcVertexIndex1] >= 0) {
                    continue;
                }

                if ((edgeIndexToType[edgeIndex1] == FROM_REAL_TO_DUMMY_BOTTOM_OF_CYCLE_EDGE_TYPE
                    || edgeIndexToType[edgeIndex2] == FROM_REAL_TO_DUMMY_BOTTOM_OF_CYCLE_EDGE_TYPE)
                    && cycleVertexIndexToParentVertexIndex[trgVertexIndex1]
                    == cycleVertexIndexToParentVertexIndex[trgVertexIndex2]
                    && cycleVertexIndexToParentVertexIndex[trgVertexIndex1] >= 0) {
                    continue;
                }

                // handle case when src and target vertices are same.
                if (srcVertexIndex1 == srcVertexIndex2 && trgVertexIndex1 == trgVertexIndex2) {
                    if (srcPortIndex1 >= 0 && srcPortIndex2 >= 0 && trgPortIndex1 >= 0 && trgPortIndex2 >= 0) {
                        if (srcPortIndex1 == srcPortIndex2 || trgPortIndex1 == trgPortIndex2) {
                            continue;
                        }

                        if (srcPortIndex1 < srcPortIndex2 && trgPortIndex1 > trgPortIndex2) {
                            result++;
                            continue;
                        }

                        if (srcPortIndex1 > srcPortIndex2 && trgPortIndex1 < trgPortIndex2) {
                            result++;
                            continue;
                        }
                    }
                    continue;
                }

                // handle case when src vertex contains output ports.
                if (srcVertexIndex1 == srcVertexIndex2 && srcPortIndex1 >= 0 && srcPortIndex2 >= 0) {
                    if (srcPortIndex1 == srcPortIndex2) {
                        continue;
                    }

                    if (srcPortIndex1 < srcPortIndex2 && trgVertexOrder1 > trgVertexOrder2) {
                        result++;
                        continue;
                    }

                    if (srcPortIndex1 > srcPortIndex2 && trgVertexOrder1 < trgVertexOrder2) {
                        result++;
                        continue;
                    }
                }

                // handle case when trg vertex contains input ports.
                if (trgVertexIndex1 == trgVertexIndex2 && trgPortIndex1 >= 0 && trgPortIndex2 >= 0) {
                    if (trgPortIndex1 == trgPortIndex2) {
                        continue;
                    }

                    if (srcVertexOrder1 < srcVertexOrder2 && trgPortIndex1 > trgPortIndex2) {
                        result++;
                        continue;
                    }

                    if (srcVertexOrder1 > srcVertexOrder2 && trgPortIndex1 < trgPortIndex2) {
                        result++;
                        continue;
                    }
                }

                // otherwise handle general cases without ports.
                if (srcVertexIndex1 == srcVertexIndex2) {
                    continue;
                }

                if (trgVertexIndex1 == trgVertexIndex2) {
                    continue;
                }

                if (srcVertexOrder1 < srcVertexOrder2 && trgVertexOrder1 < trgVertexOrder2) {
                    continue;
                }

                if (srcVertexOrder1 > srcVertexOrder2 && trgVertexOrder1 > trgVertexOrder2) {
                    continue;
                }

                result++;
            }
        }

        countCrossesTime += (System.currentTimeMillis() - startTime);

        return result;
    }

    /**
     * Required parameters:
     *  - numberOfLayers,
     *  - vertexIndexToLayer.
     */
    private int[][] findLayerToVertexIndices() {
        var layerToNumberOfVertices = new int[numberOfLayers];
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var layer = vertexIndexToLayer[vertexIndex];
            layerToNumberOfVertices[layer]++;
        }

        var layerToVertexIndices = new int[numberOfLayers][];
        for (var layer = 0; layer < numberOfLayers; layer++) {
            layerToVertexIndices[layer] = new int[layerToNumberOfVertices[layer]];
        }

        var layerToCurrentNumberOfVertices = new int[numberOfLayers];
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var layer = vertexIndexToLayer[vertexIndex];
            var currentNumberOfVertices = layerToCurrentNumberOfVertices[layer]++;
            layerToVertexIndices[layer][currentNumberOfVertices] = vertexIndex;
        }

        return layerToVertexIndices;
    }

    /**
     * Required parameters:
     *  - vertexIndexToIsDummy,
     *  - vertexIndexToInputEdgeIndices,
     *  - vertexIndexToOutputEdgeIndices.
     */
    @SuppressWarnings("Duplicates")
    private int[] findVertexIndexToType() {
        var result = new int[numberOfVertices];
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            result[vertexIndex] = REAL_VERTEX_TYPE;

            if (!vertexIndexToIsDummy[vertexIndex]) {
                continue;
            }

            result[vertexIndex] = DUMMY_VERTEX_TYPE;

            if (vertexIndexToOutputEdgeIndices[vertexIndex].length == 2
                && vertexIndexToInputEdgeIndices[vertexIndex].length == 0) {
                result[vertexIndex] = TOP_OF_CYCLE_VERTEX_TYPE;
            }

            if (vertexIndexToOutputEdgeIndices[vertexIndex].length == 0
                && vertexIndexToInputEdgeIndices[vertexIndex].length == 2) {
                result[vertexIndex] = BOTTOM_OF_CYCLE_VERTEX_TYPE;
            }
        }

        return result;
    }

    @SuppressWarnings("Duplicates")
    private int[] findEdgeTypes() {
        var result = new int[numberOfEdges];
        for (var edgeIndex = 0; edgeIndex < numberOfEdges; edgeIndex++) {
            result[edgeIndex] = REAL_EDGE_TYPE;

            var srcVertexIndex = edgeIndexToSrcVertexIndex[edgeIndex];
            var trgVertexIndex = edgeIndexToTrgVertexIndex[edgeIndex];

            if (!vertexIndexToIsDummy[srcVertexIndex] && !vertexIndexToIsDummy[trgVertexIndex]) {
                if (vertexIndexToInputEdgeIndices[trgVertexIndex].length <= 1
                    && vertexIndexToOutputEdgeIndices[trgVertexIndex].length <= 1) {
                    result[edgeIndex] = REAL_AND_SUITABLE_FOR_CHAIN_EDGE_TYPE;
                    continue;
                }
                if (vertexIndexToInputEdgeIndices[srcVertexIndex].length <= 1
                    && vertexIndexToOutputEdgeIndices[srcVertexIndex].length <= 1) {
                    result[edgeIndex] = REAL_AND_SUITABLE_FOR_CHAIN_EDGE_TYPE;
                    continue;
                }
            }

            if (vertexIndexToIsDummy[srcVertexIndex] || vertexIndexToIsDummy[trgVertexIndex]) {
                result[edgeIndex] = DUMMY_EDGE_TYPE;
            }

            if (!vertexIndexToIsDummy[srcVertexIndex] && vertexIndexToIsDummy[trgVertexIndex]) {
                var trgInputEdgeIndices = vertexIndexToInputEdgeIndices[trgVertexIndex];
                var trgOutputEdgeIndices = vertexIndexToOutputEdgeIndices[trgVertexIndex];

                if (trgInputEdgeIndices.length == 1 && trgOutputEdgeIndices.length == 0) {
                    result[edgeIndex] = FROM_REAL_TO_DUMMY_BOTTOM_BEACON_EDGE_TYPE;
                }

                if (trgInputEdgeIndices.length == 2 && trgOutputEdgeIndices.length == 0) {
                    var inputEdgeIndex0 = trgInputEdgeIndices[0];
                    var inputEdgeIndex1 = trgInputEdgeIndices[1];

                    var srcVertexIndex0 = edgeIndexToSrcVertexIndex[inputEdgeIndex0];
                    var srcVertexIndex1 = edgeIndexToSrcVertexIndex[inputEdgeIndex1];

                    if ((vertexIndexToIsDummy[srcVertexIndex0] && !vertexIndexToIsDummy[srcVertexIndex1])
                        || (!vertexIndexToIsDummy[srcVertexIndex0] && vertexIndexToIsDummy[srcVertexIndex1])) {
                        result[edgeIndex] = FROM_REAL_TO_DUMMY_BOTTOM_OF_CYCLE_EDGE_TYPE;
                    }
                }
            }

            if (vertexIndexToIsDummy[srcVertexIndex] && !vertexIndexToIsDummy[trgVertexIndex])
            {
                var srcOutputEdgeIndices = vertexIndexToOutputEdgeIndices[srcVertexIndex];
                var srcInputEdgeIndices = vertexIndexToInputEdgeIndices[srcVertexIndex];

                if (srcOutputEdgeIndices.length == 1 && srcInputEdgeIndices.length == 0) {
                    result[edgeIndex] = FROM_DUMMY_TO_REAL_TOP_BEACON_EDGE_TYPE;
                }

                if (srcOutputEdgeIndices.length == 2 && srcInputEdgeIndices.length == 0) {
                    var outputEdgeIndex0 = srcOutputEdgeIndices[0];
                    var outputEdgeIndex1 = srcOutputEdgeIndices[1];

                    var trgVertexIndex0 = edgeIndexToTrgVertexIndex[outputEdgeIndex0];
                    var trgVertexIndex1 = edgeIndexToTrgVertexIndex[outputEdgeIndex1];

                    if ((vertexIndexToIsDummy[trgVertexIndex0] && !vertexIndexToIsDummy[trgVertexIndex1])
                        || (!vertexIndexToIsDummy[trgVertexIndex0] && vertexIndexToIsDummy[trgVertexIndex1])) {
                        result[edgeIndex] = FROM_DUMMY_TO_REAL_TOP_OF_CYCLE_EDGE_TYPE;
                    }
                }
            }
        }

        return result;
    }

    /**
     * Required parameters:
     *  - vertexIndexToType
     *  - edgeIndexToSrcVertexIndex
     *  - edgeIndexToTrgVertexIndex
     *  - vertexIndexToInputEdgeIndices
     *  - vertexIndexToOutputEdgeIndices
     * Returns values:
     *  0 - cycleVertexIndexToParentVertexIndex
     */
    @SuppressWarnings("Duplicates")
    private int[] findCycleVertexIndexToParentVertexIndex() {
        var result = new int[numberOfVertices];
        Arrays.fill(result, -1);

        // where A, B, C, D - are dummy vertices.
        //   |        |
        //   v        v
        //  SRC       A    B
        //  | \       /   /
        //  |  \-----+   /
        //  |   \   / \ /
        //  v     v    v
        // ABC    C    D
        //  |
        //  v
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            if (vertexIndexToType[vertexIndex] == BOTTOM_OF_CYCLE_VERTEX_TYPE) {
                var inputEdgeIndices = vertexIndexToInputEdgeIndices[vertexIndex];

                var inputEdgeIndex0 = inputEdgeIndices[0];
                var inputEdgeIndex1 = inputEdgeIndices[1];

                var srcVertexIndex0 = edgeIndexToSrcVertexIndex[inputEdgeIndex0];
                var srcVertexIndex1 = edgeIndexToSrcVertexIndex[inputEdgeIndex1];

                if (vertexIndexToType[srcVertexIndex0] == REAL_VERTEX_TYPE
                    && vertexIndexToType[srcVertexIndex1] == DUMMY_VERTEX_TYPE) {
                    result[vertexIndex] = srcVertexIndex0;
                }

                if (vertexIndexToType[srcVertexIndex1] == REAL_VERTEX_TYPE
                    && vertexIndexToType[srcVertexIndex0] == DUMMY_VERTEX_TYPE) {
                    result[vertexIndex] = srcVertexIndex1;
                }
            }

            if (vertexIndexToType[vertexIndex] == TOP_OF_CYCLE_VERTEX_TYPE) {
                var outputEdgeIndices = vertexIndexToOutputEdgeIndices[vertexIndex];

                var outputEdgeIndex0 = outputEdgeIndices[0];
                var outputEdgeIndex1 = outputEdgeIndices[1];

                var trgVertexIndex0 = edgeIndexToTrgVertexIndex[outputEdgeIndex0];
                var trgVertexIndex1 = edgeIndexToTrgVertexIndex[outputEdgeIndex1];

                if (vertexIndexToType[trgVertexIndex0] == REAL_VERTEX_TYPE
                    && vertexIndexToType[trgVertexIndex1] == DUMMY_VERTEX_TYPE) {
                    result[vertexIndex] = trgVertexIndex0;
                }

                if (vertexIndexToType[trgVertexIndex1] == REAL_VERTEX_TYPE
                    && vertexIndexToType[trgVertexIndex0] == DUMMY_VERTEX_TYPE) {
                    result[vertexIndex] = trgVertexIndex1;
                }
            }
        }

        return result;
    }

    private boolean[] findVertexIndexToIsFixed(Map<UUID, Integer> vertexIdToInitialOrder, UUID[] vertexIndexToId) {
        var result = new boolean[vertexIndexToId.length];
        for (var vertexIndex = 0; vertexIndex < vertexIndexToId.length; vertexIndex++) {
            var vertexId = vertexIndexToId[vertexIndex];
            var initialVertexOrder = vertexIdToInitialOrder.get(vertexId);
            if (initialVertexOrder != null && initialVertexOrder >= 0) {
                result[vertexIndex] = true;
            }
        }

        return result;
    }

    /**
     * Required parameters:
     *  - vertexIdToIndex.
     */
    private boolean[] findVertexToIsDummy(List<UUID> dummyVertices, Map<UUID, Integer> vertexIdToIndex) {
        var result = new boolean[numberOfVertices];
        for (var dummyVertex : dummyVertices) {
            var vertexIndex = vertexIdToIndex.get(dummyVertex);
            result[vertexIndex] = true;
        }

        return result;
    }

    /**
     * Finds order of the first disabled vertex in the layer, otherwise 0.
     * Also, it checks that all remaining vertices are disabled starting from the vertex.
     */
    private int layerToFirstDisabledVertexOrder(int layer) {
        var currentOrder = 0;
        for (var vertexIndex : layerToOrderToVertexIndices[layer]) {
            if (vertexIndexToDisabled[vertexIndex]) {
                break;
            }
            currentOrder++;
        }

        assert verticesAreDisabled(layer, currentOrder);

        return currentOrder;
    }

    //region: Code block with experimental methods.
    @Deprecated
    private void diagnosticPrintGroups() {
        var maxWeight = weightToVertexIndices.length - 1;
        for (var weight = maxWeight; weight >= 0; weight--) {
            log.debug("=====================================");
            log.debug("Weight = {}, direction = {}.", weight, CustomConfig.directionToString(weightToDirection[weight]));
            for (int vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
                if (vertexIndexToWeight[vertexIndex] != weight) {
                    continue;
                }

                log.debug(vertexIdToName.get(vertexIndexToId[vertexIndex]) + "   LAYER = " + vertexIndexToLayer[vertexIndex]);
            }
            log.debug("=====================================");
        }
    }

    private boolean verticesAreDisabled(int layer, int startVertexOrder) {
        for (var order = startVertexOrder; order < layerToOrderToVertexIndices[layer].length; order++) {
            var vertexIndex = layerToOrderToVertexIndices[layer][order];
            if (!vertexIndexToDisabled[vertexIndex]) {
                return false;
            }
        }

        return true;
    }
    //endregion
}
