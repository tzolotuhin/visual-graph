package vg.lib.layout.hierarchical.step2.legacy.data;

import vg.lib.layout.hierarchical.data.Direction;

import java.util.Arrays;
import java.util.List;

public class Row {
    private final CrossingReductionTable table;

    public final Vertex[] verticesRef;
    public final int[] ordersRef;
    public final int[] orderToIndexRef;

    private final boolean[] visitedRef;

    public final int rowIndex;

    public Group[] topGroups = new Group[0];
    public Group[] bottomGroups = new Group[0];

    // TODO: цепи которые проходят через этот уровень.
    public Chain[] chains = new Chain[0];

    public Row(
            Vertex[] vertices,
            int[] ordersRef,
            int[] orderToIndexRef,
            boolean[] visitedRef,
            int rowIndex,
            CrossingReductionTable table) {
        this.verticesRef = vertices;
        this.ordersRef = ordersRef;
        this.orderToIndexRef = orderToIndexRef;
        this.visitedRef = visitedRef;
        this.rowIndex = rowIndex;
        this.table = table;
    }

    public void setGroups(List<Group> topGroups, List<Group> bottomGroups) {
        this.topGroups = new Group[topGroups.size()];
        topGroups.toArray(this.topGroups);

        this.bottomGroups = new Group[bottomGroups.size()];
        bottomGroups.toArray(this.bottomGroups);
    }

    public void setChains(List<Chain> chains) {
        this.chains = new Chain[chains.size()];
        chains.toArray(this.chains);
    }

    public int getOrder(int columnIndex) {
        return ordersRef[columnIndex];
    }

    public Vertex getVertexByOrder(int order) {
        return verticesRef[orderToIndexRef[order]];
    }

    public Vertex takeSuitableVertex(Direction direction) {
        Vertex suitableVertex = null;
        for (Vertex vertex : verticesRef) {
            if (vertex.isVisited()) {
                continue;
            }
            if (vertex.isFixed) {
                continue;
            }
            if (!vertex.isSuitable(direction)) {
                continue;
            }
            suitableVertex = vertex;
            break;
        }

        if (suitableVertex != null) {
            suitableVertex.setVisited(true);
            return suitableVertex;
        }

        return null;
    }

    public void resetOrders() {
        for (int columnIndex = 0; columnIndex < verticesRef.length; columnIndex++) {
            var vertex = verticesRef[columnIndex];

            ordersRef[columnIndex] = columnIndex;
            orderToIndexRef[columnIndex] = columnIndex;

            if (vertex.vertex.getInitialPortIndex() >= 0) {
                vertex.isFixed = true;
            }
        }
    }

    public void resetVisited() {
        Arrays.fill(visitedRef, false);
        for (var group : topGroups) {
            group.visited = false;
        }
        for (var group : bottomGroups) {
            group.visited = false;
        }
    }

    public void backup() {
        table.backup(rowIndex);
    }

    public void rollback() {
        table.rollback(rowIndex);
    }

    public void longBackup() {
        table.longBackup(rowIndex);
    }

    public void longRollback() {
        table.longRollback(rowIndex);
    }

    public int size() {
        return verticesRef.length;
    }

    public boolean leftSwap(Vertex vertex) {
        int order = vertex.getOrder();
        if (order <= 0) {
            return false;
        }

        int columnIndex = orderToIndexRef[order];
        int leftColumnIndex = orderToIndexRef[order - 1];

        ordersRef[leftColumnIndex]++;
        ordersRef[columnIndex]--;

        orderToIndexRef[order] = leftColumnIndex;
        orderToIndexRef[order - 1] = columnIndex;

        return true;
    }

    public boolean rightSwap(Vertex vertex) {
        int order = vertex.getOrder();
        if (order >= size() - 1) {
            return false;
        }

        int columnIndex = orderToIndexRef[order];
        int rightColumnIndex = orderToIndexRef[order + 1];

        ordersRef[rightColumnIndex]--;
        ordersRef[columnIndex]++;

        orderToIndexRef[order] = rightColumnIndex;
        orderToIndexRef[order + 1] = columnIndex;

        return true;
    }
}
