package vg.lib.layout.hierarchical.step2.custom;

public class CustomConfig {
    public static final int COUNT_CROSSES_TYPE                = 1;
    public static final int COUNT_ADDITIONAL_CROSSES_TYPE     = 2;
    public static final int COUNT_CROSSES_WITH_TRAPS_TYPE     = 3;
    public static final int COUNT_NEIGHBORHOOD_ANOMALIES_TYPE = 4;

    public static final int NUMBER_OF_FAILED_MOVES      = 5;
    public static final int NUMBER_OF_FAILED_PASSAGES   = 1;

    public static final int REAL_VERTEX_TYPE            = 0b100; // 4
    public static final int DUMMY_VERTEX_TYPE           = 0b011; // 3
    public static final int TOP_OF_CYCLE_VERTEX_TYPE    = 0b010; // 2
    public static final int BOTTOM_OF_CYCLE_VERTEX_TYPE = 0b001; // 1

    public static final int REAL_EDGE_TYPE                               = 0b110000; // 48
    public static final int REAL_AND_SUITABLE_FOR_CHAIN_EDGE_TYPE        = 0b010000; // 16
    public static final int DUMMY_EDGE_TYPE                              = 0b001111; // 15
    public static final int FROM_DUMMY_TO_REAL_TOP_OF_CYCLE_EDGE_TYPE    = 0b001000; // 8
    public static final int FROM_REAL_TO_DUMMY_BOTTOM_OF_CYCLE_EDGE_TYPE = 0b000100; // 4
    public static final int FROM_DUMMY_TO_REAL_TOP_BEACON_EDGE_TYPE      = 0b000010; // 2
    public static final int FROM_REAL_TO_DUMMY_BOTTOM_BEACON_EDGE_TYPE   = 0b000001; // 1

    public static final int UNKNOWN_DIRECTION            = 0b011; // 3
    public static final int FROM_BOTTOM_TO_TOP_DIRECTION = 0b010; // 2
    public static final int FROM_TOP_TO_BOTTOM_DIRECTION = 0b001; // 1

    public static long toChainIndexWithVertexIndex(int chainIndex, int vertexIndex) {
        return (long)chainIndex * 10_000 + vertexIndex;
    }

    public static String directionToString(int direction) {
        return switch (direction) {
            case 1 -> "DOWN";
            case 2 -> "UP";
            default -> "UNKNOWN";
        };
    }
}
