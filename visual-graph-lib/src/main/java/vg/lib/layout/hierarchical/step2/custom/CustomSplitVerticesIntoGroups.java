package vg.lib.layout.hierarchical.step2.custom;

import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;
import vg.lib.operation.Operation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.BOTTOM_OF_CYCLE_VERTEX_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.DUMMY_VERTEX_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_BOTTOM_TO_TOP_DIRECTION;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_TOP_TO_BOTTOM_DIRECTION;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.TOP_OF_CYCLE_VERTEX_TYPE;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.UNKNOWN_DIRECTION;

public class CustomSplitVerticesIntoGroups implements Operation<CustomSplitVerticesIntoGroups.Result> {
    private final int numberOfVertices;

    private final int[] vertexIndexToType;

    private final int[][] vertexIndexToInputVertexIndices;
    private final int[][] vertexIndexToOutputVertexIndices;

    private final int[][] vertexIndexToInputEdgeIndices;
    private final int[][] vertexIndexToOutputEdgeIndices;

    private final int[] vertexIndexToLayer;

    private final int numberOfEdges;
    private final int[] edgeIndexToSrcVertexIndex;
    private final int[] edgeIndexToTrgVertexIndex;
    private final int[] edgeIndexToChainIndex;

    private final int numberOfLayers;
    private final int[][] layerToVertexIndices;

    private final int numberOfChains;
    private final int[] chainIndexToStartVertexIndex;
    private final int[] chainIndexToFinishVertexIndex;
    private final int[] cycleVertexIndexToParentVertexIndex;

    public CustomSplitVerticesIntoGroups(int numberOfVertices,

                                         int[] vertexIndexToType,

                                         int[][] vertexIndexToInputVertexIndices,
                                         int[][] vertexIndexToOutputVertexIndices,

                                         int[][] vertexIndexToInputEdgeIndices,
                                         int[][] vertexIndexToOutputEdgeIndices,

                                         int[] vertexIndexToLayer,

                                         int numberOfEdges,
                                         int[] edgeIndexToSrcVertexIndex,
                                         int[] edgeIndexToTrgVertexIndex,
                                         int[] edgeIndexToChainIndex,

                                         int numberOfLayers,
                                         int[][] layerToVertexIndices,

                                         int numberOfChains,
                                         int[] chainIndexToStartVertexIndex,
                                         int[] chainIndexToFinishVertexIndex,
                                         int[] cycleVertexIndexToParentVertexIndex) {
        this.numberOfVertices = numberOfVertices;

        this.vertexIndexToType = vertexIndexToType;

        this.vertexIndexToInputVertexIndices = vertexIndexToInputVertexIndices;
        this.vertexIndexToOutputVertexIndices = vertexIndexToOutputVertexIndices;

        this.vertexIndexToInputEdgeIndices = vertexIndexToInputEdgeIndices;
        this.vertexIndexToOutputEdgeIndices = vertexIndexToOutputEdgeIndices;

        this.vertexIndexToLayer = vertexIndexToLayer;

        this.numberOfEdges = numberOfEdges;
        this.edgeIndexToSrcVertexIndex = edgeIndexToSrcVertexIndex;
        this.edgeIndexToTrgVertexIndex = edgeIndexToTrgVertexIndex;
        this.edgeIndexToChainIndex = edgeIndexToChainIndex;

        this.numberOfLayers = numberOfLayers;
        this.layerToVertexIndices = layerToVertexIndices;

        this.numberOfChains = numberOfChains;
        this.chainIndexToStartVertexIndex = chainIndexToStartVertexIndex;
        this.chainIndexToFinishVertexIndex = chainIndexToFinishVertexIndex;
        this.cycleVertexIndexToParentVertexIndex = cycleVertexIndexToParentVertexIndex;
    }

    public Result execute() {
        var vertexIndexToMinLayer = findVertexIndexToMinMaxLayer(FROM_BOTTOM_TO_TOP_DIRECTION);
        var vertexIndexToMaxLayer = findVertexIndexToMinMaxLayer(FROM_TOP_TO_BOTTOM_DIRECTION);
        return findWeightToVertexIndices(vertexIndexToMinLayer, vertexIndexToMaxLayer);
    }

    private Result findWeightToVertexIndices(int[] vertexIndexToMinLayer, int[] vertexIndexToMaxLayer) {
        var weightToVertexIndicesList = new ArrayList<LinkedHashSet<Integer>>();
        var weightToDirectionList = new ArrayList<Integer>();
        var vertexIndexToIsVisited = new boolean[numberOfVertices];

        var count = 0;
        while (count < numberOfVertices) {
            var vertexIndexToIsSuitableFromTop = new boolean[numberOfVertices];
            System.arraycopy(vertexIndexToIsVisited, 0, vertexIndexToIsSuitableFromTop, 0, numberOfVertices);

            var vertexIndexToIsSuitableFromBottom = new boolean[numberOfVertices];
            System.arraycopy(vertexIndexToIsVisited, 0, vertexIndexToIsSuitableFromBottom, 0, numberOfVertices);

            var vertexIndicesFromTop = new LinkedHashSet<Integer>();
            {
                for (var layer = 0; layer < numberOfLayers; layer++) {
                    for (var vertexIndex : layerToVertexIndices[layer]) {
                        if (!vertexIndexToIsSuitableFromTop[vertexIndex]) {
                            continue;
                        }

                        for (var outputVertexIndex : vertexIndexToOutputVertexIndices[vertexIndex]) {
                            if (vertexIndexToIsSuitableFromTop[outputVertexIndex]) {
                                continue;
                            }

                            if (doesVertexIsPartOfCycle(outputVertexIndex)) {
                                continue;
                            }

                            vertexIndexToIsSuitableFromTop[outputVertexIndex] = true;

                            if (vertexIndexToIsVisited[outputVertexIndex]) {
                                continue;
                            }

                            vertexIndicesFromTop.add(outputVertexIndex);
                        }
                    }
                }
            }

            // mark vertex indices as visited.
            for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
                if (vertexIndexToIsSuitableFromTop[vertexIndex]) {
                    vertexIndexToIsVisited[vertexIndex] = true;
                }
            }

            var vertexIndicesFromBottom = new LinkedHashSet<Integer>();
            {
                for (var layer = numberOfLayers - 1; layer >= 0; layer--) {
                    for (var vertexIndex : layerToVertexIndices[layer]) {
                        if (!vertexIndexToIsSuitableFromBottom[vertexIndex]) {
                            continue;
                        }

                        for (var inputVertexIndex : vertexIndexToInputVertexIndices[vertexIndex]) {
                            if (vertexIndexToIsSuitableFromBottom[inputVertexIndex]) {
                                continue;
                            }

                            if (doesVertexIsPartOfCycle(inputVertexIndex)) {
                                continue;
                            }

                            vertexIndexToIsSuitableFromBottom[inputVertexIndex] = true;

                            if (vertexIndexToIsVisited[inputVertexIndex]) {
                                continue;
                            }

                            vertexIndicesFromBottom.add(inputVertexIndex);
                        }
                    }
                }
            }

            // mark vertex indices as visited.
            for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
                if (vertexIndexToIsSuitableFromBottom[vertexIndex]) {
                    vertexIndexToIsVisited[vertexIndex] = true;
                }
            }

            // handle case when vertex hasn't edges.
            if (vertexIndicesFromTop.isEmpty() && vertexIndicesFromBottom.isEmpty()) {
                var coreGroupOfVertexIndices = findNextCoreGroup(
                        vertexIndexToMinLayer, vertexIndexToMaxLayer, vertexIndexToIsVisited);

                Validate.isTrue(!coreGroupOfVertexIndices.isEmpty());

                var numberOfTopTraps = 0;
                var numberOfBottomTraps = 0;
                for (var vertexIndex : coreGroupOfVertexIndices) {
                    var numberOfInputVertexIndices = IntStream.of(vertexIndexToInputVertexIndices[vertexIndex])
                            .filter(inputVertexIndex -> vertexIndexToIsVisited[inputVertexIndex]).count();
                    var numberOfOutputVertexIndices = IntStream.of(vertexIndexToOutputVertexIndices[vertexIndex])
                            .filter(outputVertexIndex -> vertexIndexToIsVisited[outputVertexIndex]).count();

                    if (numberOfOutputVertexIndices > 1) {
                        numberOfTopTraps++;
                    }

                    if (numberOfInputVertexIndices > 1) {
                        numberOfBottomTraps++;
                    }
                }

                if (numberOfTopTraps >= numberOfBottomTraps) {
                    vertexIndicesFromTop = coreGroupOfVertexIndices;
                } else {
                    vertexIndicesFromBottom = coreGroupOfVertexIndices;
                }
            }

            // calculate group of vertex indices related with cycles.
            var cycleVertexIndices = includeCyclesToGroup(vertexIndexToIsVisited);

            // note: it's additional check that the indices doesn't have intersections.
            count += vertexIndicesFromTop.size() + vertexIndicesFromBottom.size() + cycleVertexIndices.size();

            // collect all vertex indices to one set.
            var allVertexIndices = Stream.of(vertexIndicesFromTop, vertexIndicesFromBottom, cycleVertexIndices)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toSet());

            // extract chains from the top and bottom vertex indices.
            var extractedChains = extractChains(allVertexIndices);

            // remove it from the top and bottom vertex indices.
            vertexIndicesFromTop.removeAll(extractedChains.getKey());
            vertexIndicesFromBottom.removeAll(extractedChains.getKey());
            cycleVertexIndices.removeAll(extractedChains.getKey());

            // add the top and bottom vertex indices to the result.
            if (vertexIndicesFromTop.size() >= vertexIndicesFromBottom.size()) {
                if (!vertexIndicesFromTop.isEmpty()) {
                    weightToDirectionList.add(FROM_TOP_TO_BOTTOM_DIRECTION);
                    weightToVertexIndicesList.add(vertexIndicesFromTop);
                }
                if (!vertexIndicesFromBottom.isEmpty()) {
                    weightToDirectionList.add(FROM_BOTTOM_TO_TOP_DIRECTION);
                    weightToVertexIndicesList.add(vertexIndicesFromBottom);
                }
            } else {
                weightToDirectionList.add(FROM_BOTTOM_TO_TOP_DIRECTION);
                weightToVertexIndicesList.add(vertexIndicesFromBottom);
                if (!vertexIndicesFromTop.isEmpty()) {
                    weightToDirectionList.add(FROM_TOP_TO_BOTTOM_DIRECTION);
                    weightToVertexIndicesList.add(vertexIndicesFromTop);
                }
            }

            // add the extracted chains to the result.
            if (!extractedChains.getKey().isEmpty()) {
                extractedChains.getValue().forEach(it -> {
                    weightToDirectionList.add(UNKNOWN_DIRECTION);
                    weightToVertexIndicesList.add(it);
                });
            }
        }

        Collections.reverse(weightToDirectionList);
        Collections.reverse(weightToVertexIndicesList);

        var weightToVertexIndices = new int[weightToVertexIndicesList.size()][];
        var weightToDirection = new int[weightToDirectionList.size()];
        for (var weight = 0; weight < weightToVertexIndicesList.size(); weight++) {
            weightToVertexIndices[weight] = weightToVertexIndicesList.get(weight).stream().mapToInt(Integer::intValue).toArray();
            weightToDirection[weight] = weightToDirectionList.get(weight);
        }

        return new Result(weightToVertexIndices, weightToDirection, findVertexIndexToWeight(weightToVertexIndices));
    }

    private Pair<LinkedHashSet<Integer>, List<LinkedHashSet<Integer>>> extractChains(Set<Integer> allVertexIndices) {
        var chainIndexToWeight = new LinkedHashMap<Integer, Integer>();
        var chainIndexToVertexIndices = new LinkedHashMap<Integer, LinkedHashSet<Integer>>();
        for (var edgeIndex = 0; edgeIndex < numberOfEdges; edgeIndex++) {
            var chainIndex = edgeIndexToChainIndex[edgeIndex];
            var srcVertexIndex = edgeIndexToSrcVertexIndex[edgeIndex];
            var trgVertexIndex = edgeIndexToTrgVertexIndex[edgeIndex];

            if (chainIndex < 0) {
                continue;
            }

            if (!allVertexIndices.contains(srcVertexIndex) || !allVertexIndices.contains(trgVertexIndex)) {
                continue;
            }

            var vertexIndices = chainIndexToVertexIndices.get(chainIndex);
            if (vertexIndices == null) {
                vertexIndices = new LinkedHashSet<>();
            }

            if ((vertexIndexToType[srcVertexIndex] & DUMMY_VERTEX_TYPE) > 0) {
                vertexIndices.add(srcVertexIndex);
                chainIndexToVertexIndices.put(chainIndex, vertexIndices);
                chainIndexToWeight.put(chainIndex, vertexIndices.size());
            }

            if ((vertexIndexToType[trgVertexIndex] & DUMMY_VERTEX_TYPE) > 0) {
                vertexIndices.add(trgVertexIndex);
                chainIndexToVertexIndices.put(chainIndex, vertexIndices);
                chainIndexToWeight.put(chainIndex, vertexIndices.size());
            }
        }

        var resultValue = new ArrayList<LinkedHashSet<Integer>>();
        chainIndexToWeight.entrySet().stream()
                .filter(it -> it.getValue() > 1)
                .sorted(Map.Entry.comparingByValue())
                .forEachOrdered(it -> resultValue.add(chainIndexToVertexIndices.get(it.getKey())));

        var resultKey = new LinkedHashSet<Integer>();
        resultValue.forEach(resultKey::addAll);

        return Pair.of(resultKey, resultValue);
    }

    private LinkedHashSet<Integer> findNextCoreGroup(int[] vertexIndexToMinLayer,
                                                     int[] vertexIndexToMaxLayer,
                                                     boolean[] vertexIndexToIsVisited) {
        var maxWeight = Integer.MIN_VALUE;
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            if (vertexIndexToIsVisited[vertexIndex]) {
                continue;
            }

            var weight = vertexIndexToMaxLayer[vertexIndex] - vertexIndexToMinLayer[vertexIndex];
            if (weight > maxWeight) {
                maxWeight = weight;
            }
        }

        var result = new LinkedHashSet<Integer>();
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            if (vertexIndexToIsVisited[vertexIndex]) {
                continue;
            }

            var weight = vertexIndexToMaxLayer[vertexIndex] - vertexIndexToMinLayer[vertexIndex];

            if (weight != maxWeight) {
                continue;
            }

            if (doesVertexIsPartOfCycle(vertexIndex)) {
                continue;
            }

            result.add(vertexIndex);
            vertexIndexToIsVisited[vertexIndex] = true;
        }

        return result;
    }

    private LinkedHashSet<Integer> includeCyclesToGroup(boolean[] vertexIndexToIsVisited) {
        var result = new LinkedHashSet<Integer>();
        for (var chainIndex = 0; chainIndex < numberOfChains; chainIndex++) {
            var startVertexIndex = chainIndexToStartVertexIndex[chainIndex];
            var finishVertexIndex = chainIndexToFinishVertexIndex[chainIndex];

            var parentStartVertexIndex = cycleVertexIndexToParentVertexIndex[startVertexIndex];
            var parentFinishVertexIndex = cycleVertexIndexToParentVertexIndex[finishVertexIndex];

            if (parentStartVertexIndex < 0 || parentFinishVertexIndex < 0) {
                continue;
            }

            if (!vertexIndexToIsVisited[parentStartVertexIndex] || !vertexIndexToIsVisited[parentFinishVertexIndex]) {
                continue;
            }

            if (vertexIndexToIsVisited[startVertexIndex] && vertexIndexToIsVisited[finishVertexIndex]) {
                continue;
            }

            for (var edgeIndex = 0; edgeIndex < numberOfEdges; edgeIndex++) {
                var edgeChainIndex = edgeIndexToChainIndex[edgeIndex];
                if (edgeChainIndex != chainIndex) {
                    continue;
                }

                var srcVertexIndex = edgeIndexToSrcVertexIndex[edgeIndex];
                var trgVertexIndex = edgeIndexToTrgVertexIndex[edgeIndex];

                if ((vertexIndexToType[srcVertexIndex] & DUMMY_VERTEX_TYPE) > 0) {
                    result.add(srcVertexIndex);
                    vertexIndexToIsVisited[srcVertexIndex] = true;
                }
                if ((vertexIndexToType[trgVertexIndex] & DUMMY_VERTEX_TYPE) > 0) {
                    result.add(trgVertexIndex);
                    vertexIndexToIsVisited[trgVertexIndex] = true;
                }
            }
        }

        return result;
    }

    private int[] findVertexIndexToMinMaxLayer(int direction) {
        var vertexIndexToMinMaxLayer = new int[numberOfVertices];

        var _vertexIndexToIsVisited = new boolean[numberOfVertices];
        var _stack = new int[numberOfVertices];
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            Arrays.fill(_vertexIndexToIsVisited, false);
            var minLayer = findMinMaxLayerFromStartVertex(
                    vertexIndex, direction, _vertexIndexToIsVisited, _stack);
            vertexIndexToMinMaxLayer[vertexIndex] = minLayer;
        }

        return vertexIndexToMinMaxLayer;
    }

    private int findMinMaxLayerFromStartVertex(int startVertexIndex,
                                               int direction,
                                               boolean[] _vertexIndexToIsVisited,
                                               int[] _stack) {
        var minMaxLayer = vertexIndexToLayer[startVertexIndex];

        var stackCounter = 0;
        _stack[stackCounter++] = startVertexIndex;
        while (stackCounter > 0) {
            var vertexIndex = _stack[--stackCounter];

            var vertexLayer = vertexIndexToLayer[vertexIndex];
            if (direction == FROM_BOTTOM_TO_TOP_DIRECTION && minMaxLayer > vertexLayer) {
                minMaxLayer = vertexLayer;
            }
            if (direction == FROM_TOP_TO_BOTTOM_DIRECTION && minMaxLayer < vertexLayer) {
                minMaxLayer = vertexLayer;
            }

            _vertexIndexToIsVisited[vertexIndex] = true;

            var nextVertexIndices = vertexIndexToOutputVertexIndices[vertexIndex];
            if (direction == FROM_BOTTOM_TO_TOP_DIRECTION) {
                nextVertexIndices = vertexIndexToInputVertexIndices[vertexIndex];
            }

            for (var nextVertexIndex : nextVertexIndices) {
                if (!_vertexIndexToIsVisited[nextVertexIndex]) {
                    _stack[stackCounter++] = nextVertexIndex;
                }
            }
        }

        return minMaxLayer;
    }

    private boolean doesVertexIsPartOfCycle(int vertexIndex) {
        if (vertexIndexToType[vertexIndex] == TOP_OF_CYCLE_VERTEX_TYPE
                || vertexIndexToType[vertexIndex] == BOTTOM_OF_CYCLE_VERTEX_TYPE) {
            return true;
        }

        if (vertexIndexToType[vertexIndex] == DUMMY_VERTEX_TYPE) {
            if (vertexIndexToInputEdgeIndices[vertexIndex].length == 1
                    && vertexIndexToOutputEdgeIndices[vertexIndex].length == 1) {
                var chainIndex = edgeIndexToChainIndex[vertexIndexToInputEdgeIndices[vertexIndex][0]];

                if (chainIndex < 0) {
                    return false;
                }

                var startChainVertexIndex = chainIndexToStartVertexIndex[chainIndex];

                if (vertexIndexToType[startChainVertexIndex] == TOP_OF_CYCLE_VERTEX_TYPE
                        || vertexIndexToType[startChainVertexIndex] == BOTTOM_OF_CYCLE_VERTEX_TYPE) {
                    return true;
                }
            }
        }

        return false;
    }

    private int[] findVertexIndexToWeight(int[][] weightToVertexIndices) {
        var vertexIndexToWeight = new int[numberOfVertices];
        for (var weight = 0; weight < weightToVertexIndices.length; weight++) {
            for (var vertexIndex : weightToVertexIndices[weight]) {
                vertexIndexToWeight[vertexIndex] = weight;
            }
        }

        return vertexIndexToWeight;
    }

    public record Result(int[][] weightToVertexIndices, int[] weightToDirection, int[] vertexIndexToWeight) { }
}
