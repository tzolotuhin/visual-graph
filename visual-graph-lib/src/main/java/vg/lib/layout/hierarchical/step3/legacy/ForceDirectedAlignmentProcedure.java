package vg.lib.layout.hierarchical.step3.legacy;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import vg.lib.layout.hierarchical.data.HierarchicalEdge;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.operation.Procedure;

import java.util.List;
import java.util.function.Predicate;

@Slf4j
public class ForceDirectedAlignmentProcedure implements Procedure {
    private final HierarchicalGraph graph;

    private final boolean collapseEdgeSrc;
    private final boolean collapseEdgeTrg;

    public ForceDirectedAlignmentProcedure(boolean collapseEdgeSrc, boolean collapseEdgeTrg, HierarchicalGraph graph) {
        this.graph = graph;

        this.collapseEdgeSrc = collapseEdgeSrc;
        this.collapseEdgeTrg = collapseEdgeTrg;
    }

    @Override
    public void execute() {
        log.debug("Use force directed alignment...");

        // calculate source and target edge's positions.
        for (var vertex : graph.getVertices()) {
            var outputEdges = graph.getOutputEdgesSortedByOutputVertices(vertex);
            calcEdgeSrcPosX(outputEdges, vertex.getWidthWithoutBorder(), vertex.getFragmentTextSize().x);
            calcEdgeTrgPosX(graph.getInputEdges(vertex), vertex.getWidthWithoutBorder(), vertex.getFragmentTextSize().x);
        }

        int prevGroupPosX = 0;
        for (var detailedGroupsEntry : graph.getDetailedGroups().entrySet()) {
            new ForceDirectedGroupAlignmentProcedure(detailedGroupsEntry.getValue(), graph).execute();

            prevGroupPosX = HierarchicalGraph.shiftPosXForDetailedGroup(prevGroupPosX, detailedGroupsEntry.getValue());
        }

        new ForceDirectedAlignmentForPortsProcedure(graph).execute();
    }

    // TODO: необходимо вынести в отдельную операцию/операции.
    private void calcEdgeSrcPosX(List<HierarchicalEdge> outputEdges, int width, int fragmentTextSize) {
        var borders = findRealBorders(outputEdges, edge -> edge.getTrgVertex().isRealVertex());

        int i = 0;
        float posI = 0, posJ = 0;
        float sizeI = 0, sizeJ = 0;
        while (i < outputEdges.size()) {
            int j;
            for (j = i; j < outputEdges.size(); j++) {
                var outputEdge = outputEdges.get(j);
                if (!outputEdge.hasSrcPort()) {
                    continue;
                }

                posJ = outputEdge.getSrcPortPosX();
                sizeJ = outputEdge.getSrcPortSizeX();

                break;
            }

            if (i == j) {
                var outputEdge = outputEdges.get(i);
                outputEdge.setSrcPosX(outputEdge.getRoundSrcPortCenterX());
                posI = outputEdge.getSrcPortPosX();
                sizeI = outputEdge.getSrcPortSizeX();
                i++;

                if (borders.getLeft() < i && i < borders.getRight()) {
                    outputEdge.setTrgBetweenRealVertices(true);
                }

                continue;
            }

            if (j == outputEdges.size()) {
                posJ = width - fragmentTextSize;
                sizeJ = 0;
            }

            float w = (posJ - posI - sizeI) / (float)(j - i);

            int initI = i;
            for (; i < j; i++) {
                var outputEdge = outputEdges.get(i);
                if (collapseEdgeSrc || outputEdge.getSrcVertex().isCompanionVertex()) {
                    outputEdge.setSrcPosX(fragmentTextSize + Math.round(posI + sizeI + (j - initI) / 2.0f * w));
                } else {
                    outputEdge.setSrcPosX(fragmentTextSize + Math.round(posI + sizeI + (i - initI) * w + w / 2.0f));
                }

                if (borders.getLeft() < i && i < borders.getRight()) {
                    outputEdge.setTrgBetweenRealVertices(true);
                }
            }

            posI = posJ;
            sizeI = sizeJ;
        }
    }

    private void calcEdgeTrgPosX(List<HierarchicalEdge> inputEdges, int width, int fragmentTextSize) {
        var borders = findRealBorders(inputEdges, edge -> edge.getSrcVertex().isRealVertex());

        int i = 0;
        float posI = 0, posJ = 0;
        float sizeI = 0, sizeJ = 0;
        while (i < inputEdges.size()) {
            int j;
            for (j = i; j < inputEdges.size(); j++) {
                var inputEdge = inputEdges.get(j);
                if (!inputEdge.hasTrgPort()) {
                    continue;
                }

                posJ = inputEdge.getTrgPortPosX();
                sizeJ = inputEdge.getTrgPortSizeX();

                break;
            }

            if (i == j) {
                var inputEdge = inputEdges.get(i);
                inputEdge.setTrgPosX(inputEdge.getRoundTrgPortCenterX());
                posI = inputEdge.getTrgPortPosX();
                sizeI = inputEdge.getTrgPortSizeX();
                i++;

                if (borders.getLeft() < i && i < borders.getRight()) {
                    inputEdge.setSrcBetweenRealVertices(true);
                }

                continue;
            }

            if (j == inputEdges.size()) {
                posJ = width - fragmentTextSize;
                sizeJ = 0;
            }

            float w = (posJ - posI - sizeI) / (float)(j - i);

            int initI = i;
            for (; i < j; i++) {
                var inputEdge = inputEdges.get(i);
                if (collapseEdgeTrg || inputEdge.getTrgVertex().isCompanionVertex()) {
                    inputEdge.setTrgPosX(fragmentTextSize + Math.round(posI + sizeI + (j - initI) / 2.0f * w));
                } else {
                    inputEdge.setTrgPosX(fragmentTextSize + Math.round(posI + sizeI + (i - initI) * w + w / 2.0f));
                }

                if (borders.getLeft() < i && i < borders.getRight()) {
                    inputEdge.setSrcBetweenRealVertices(true);
                }
            }

            posI = posJ;
            sizeI = sizeJ;
        }
    }

    private Pair<Integer, Integer> findRealBorders(List<HierarchicalEdge> edges, Predicate<HierarchicalEdge> func) {
        var pair = new MutablePair<>(Integer.MAX_VALUE, Integer.MIN_VALUE);
        int index = 0;
        for (var edge : edges) {
            if (func.test(edge)) {
                if (index < pair.getLeft()) {
                    pair.setLeft(index);
                }
                if (index > pair.getRight()) {
                    pair.setRight(index);
                }
            }
            index++;
        }

        return pair;
    }
}
