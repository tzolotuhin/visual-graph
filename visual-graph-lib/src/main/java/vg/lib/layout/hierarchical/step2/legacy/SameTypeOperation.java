package vg.lib.layout.hierarchical.step2.legacy;

import vg.lib.layout.hierarchical.step2.legacy.data.Row;
import vg.lib.operation.Operation;

// TODO: тут имеется ввиду группировать однотипные вершины рядом... достаточно странная эвристика...
public class SameTypeOperation implements Operation<Long> {
    private final Row row;

    public SameTypeOperation(Row row) {
        this.row = row;
    }

    // TODO тут необходим более интеллектуальный подход, т.е. необходимо включать данный модуль для
    @Override
    public Long execute() {
        long w = 0;
        for (int order = 0; order < row.orderToIndexRef.length - 1; order++) {
            var curr = row.getVertexByOrder(order);
            var next = row.getVertexByOrder(order + 1);

            long dw = 5;
            if (curr.vertex.isFakeVertex() && next.vertex.isFakeVertex()) {
                dw = 4;
            }

            if (curr.vertex.isCompanionVertex() && next.vertex.isCompanionVertex()) {
                dw = 0;
            }

            //w += dw;
        }

        return w;
    }
}
