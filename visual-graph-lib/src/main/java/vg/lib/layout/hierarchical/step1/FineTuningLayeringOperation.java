package vg.lib.layout.hierarchical.step1;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.layout.hierarchical.step1.data.LayeringOperationResult;
import vg.lib.operation.Operation;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

// TODO: Необходимо переименовать в класс который умеет делить все вершины на группы, а так же уровни.
@Slf4j
public class FineTuningLayeringOperation extends BaseLayeringOperation implements Operation<LayeringOperationResult> {

    // TODO: should be deleted!!!
    private final CustomLayeringOperation customLayeringOperation;

    public FineTuningLayeringOperation(
            List<UUID> vertices,
            List<UUID> inputPorts,
            List<UUID> outputPorts,
            Map<UUID, Map.Entry<UUID, UUID>> edgeToNodes) {
        super(vertices, inputPorts, outputPorts, edgeToNodes, Collections.emptyMap());

        customLayeringOperation = new CustomLayeringOperation(
            vertices, inputPorts, outputPorts, edgeToNodes, Collections.emptyMap());
    }

    @Override
    public LayeringOperationResult execute() {
        // build adjacency matrix.
        int[][] adjacencyMatrix = new int[numberOfVertices][numberOfVertices];
        for (int i = 0; i < numberOfVertices; i++) {
            Arrays.fill(adjacencyMatrix[i], 0);
        }

        // fill the adjacency matrix.
        for (var nodes : edgeIdToNodeIds.values()) {
            int srcIndex = vertexIdToIndex.get(nodes.getKey());
            int trgIndex = vertexIdToIndex.get(nodes.getValue());
            if (inputPorts[srcIndex] || outputPorts[srcIndex] || inputPorts[trgIndex] || outputPorts[trgIndex]) {
                continue;
            }
            adjacencyMatrix[srcIndex][trgIndex]++;
        }

        // remove backward edges in adjacency matrix.
        {
            boolean[] _used = new boolean[numberOfVertices];
            int[] _colors = new int[numberOfVertices];
            int[] _stack = new int[numberOfVertices];

            // exclude input and output ports.
            for (int i = 0; i < numberOfVertices; i++) {
                if (inputPorts[i] || outputPorts[i]) {
                    _used[i] = true;
                }
            }

            int[] vertexToInput = calcVertexToInput(numberOfVertices, adjacencyMatrix);

            int rootAmount = 0;
            while (true) {
                int rootIndex = findNextRoot(numberOfVertices, adjacencyMatrix, vertexToInput, _used);

                if (rootIndex < 0) {
                    break;
                }

                doDFS(adjacencyMatrix, numberOfVertices, rootIndex, _used, _colors, _stack);

                log.debug(String.format("Amount of handled roots = %s. Last root index = %s.", rootAmount, rootIndex));
                rootAmount++;
            }
        }

        boolean[] coreTree = new boolean[numberOfVertices];
        int[] levels = new int[numberOfVertices];

        fromTopToBottom(coreTree, levels, adjacencyMatrix, numberOfVertices);
        buildCoreTree(coreTree, levels, adjacencyMatrix, numberOfVertices, true);

        // save info about parts of core tree.
        var partOfCoreTree = new boolean[coreTree.length];
        System.arraycopy(coreTree, 0, partOfCoreTree, 0, coreTree.length);

        // TODO: тут необходимо уменьшить количество итераций. смысл именно в том чтобы достроить оптимальное дерево без лишних длинных ребер.
        for (int i = 0; i < 5; i++) {
            fromBottomToTop(coreTree, levels, adjacencyMatrix, numberOfVertices);
            buildCoreTree(coreTree, levels, adjacencyMatrix, numberOfVertices, false);
            fromTopToBottom(coreTree, levels, adjacencyMatrix, numberOfVertices);
            buildCoreTree(coreTree, levels, adjacencyMatrix, numberOfVertices, false);
        }

        for (int i = 0; i < levels.length; i++) {
            if (inputPorts[i]) {
                levels[i] = Integer.MIN_VALUE;
            }
            if (outputPorts[i]) {
                levels[i] = Integer.MAX_VALUE;
            }
        }

        // TODO: необходимо вынести в отдельную операцию.
        // optimize long edges.
        {
            boolean check = true;
            while (check) {
                check = false;
                for (int i = 0; i < levels.length; i++) {
                    if (levels[i] == Integer.MAX_VALUE || levels[i] == Integer.MIN_VALUE) {
                        continue;
                    }

                    // find minimum level for inputs and outputs.
                    int maxInput = Integer.MIN_VALUE;
                    int inCount = 0;
                    int minOutput = Integer.MAX_VALUE;
                    int outCount = 0;
                    for (int j = 0; j < levels.length; j++) {
                        if (levels[j] == Integer.MAX_VALUE || levels[j] == Integer.MIN_VALUE) {
                            continue;
                        }
                        if (adjacencyMatrix[j][i] > 0) {
                            inCount += adjacencyMatrix[j][i];
                            if (levels[j] > maxInput) {
                                maxInput = levels[j];
                            }
                        }
                        if (adjacencyMatrix[i][j] > 0) {
                            outCount += adjacencyMatrix[i][j];
                            if (levels[j] < minOutput) {
                                minOutput = levels[j];
                            }
                        }
                    }

                    int level = levels[i];
                    if (maxInput == Integer.MIN_VALUE && minOutput != Integer.MAX_VALUE) {
                        if (level != minOutput - 1) {
                            levels[i] = minOutput - 1;
                            check = true;
                            break;
                        }
                        continue;
                    }
                    if (maxInput != Integer.MIN_VALUE && minOutput == Integer.MAX_VALUE) {
                        if (level != maxInput + 1) {
                            levels[i] = maxInput + 1;
                            check = true;
                            break;
                        }
                        continue;
                    }

                    Validate.isTrue(maxInput < level);
                    Validate.isTrue(minOutput > level);

                    if (maxInput == level - 1 && minOutput == level + 1) {
                        continue;
                    }
                    int up = inCount + (minOutput - maxInput) * outCount;
                    int down = outCount + (minOutput - maxInput) * inCount;
                    if (maxInput != level - 1 && up < down) {
                        levels[i] = maxInput + 1;
                        check = true;
                        break;
                    }
                    if (minOutput != level + 1 && up > down) {
                        levels[i] = minOutput - 1;
                        check = true;
                        break;
                    }
                }
            }
        }

        var re = customLayeringOperation.execute();

        // TODO: should be refactored.
        return new LayeringOperationResult(
            numberOfVertices,
            vertexToId,
            inputPorts,
            outputPorts,
            partOfCoreTree,
            null,
            null,
            levels,
            null,
            re.vertexIndexToGroupId);
    }

    private int[] calcVertexToInput(int amountOfVertices, int[][] adjacencyMatrix) {
        int[] vertexToIn = new int[amountOfVertices];
        Arrays.fill(vertexToIn, -1);

        int vertexToInCount = 0;
        for (int i = 0; i < amountOfVertices; i++) {
            if (!inputPorts[i] && !outputPorts[i]) {
                vertexToIn[i] = 0;
            }
        }

        for (int i = 0; i < amountOfVertices; i++) {
            if (vertexToIn[i] < 0) {
                continue;
            }

            for (int j = 0; j < amountOfVertices; j++) {
                if (adjacencyMatrix[j][i] > 0) {
                    vertexToIn[i] += adjacencyMatrix[j][i];
                }
            }
        }

        return vertexToIn;
    }

    private int findNextRoot(int amountOfVertices, int[][] adjacencyMatrix, int[] vertexToInput, boolean[] used) {
        int min = Integer.MAX_VALUE;
        int minIndex = -1;
        for (int i = 0; i < amountOfVertices; i++) {
            if (!used[i] && vertexToInput[i] >= 0 && min > vertexToInput[i]) {
                min = vertexToInput[i];
                minIndex = i;
            }
        }

        if (min == 0 || min == Integer.MAX_VALUE) {
            return minIndex;
        }

        // in case min is not zero.
        for (int i = 0; i < amountOfVertices; i++) {
            if (vertexToInput[i] != min) {
                continue;
            }

            for (int j = 0; j < amountOfVertices; j++) {
                if (adjacencyMatrix[j][i] > 0 && inputPorts[j]) {
                    return i;
                }
            }
        }

        return minIndex;
    }

    private void fromBottomToTop(
            boolean[] coreTree,
            int[] levels,
            int [][] adjacencyMatrix,
            int count) {
        int maxLevel = Arrays.stream(levels).max().orElse(0);

        for (int i = 0; i < count; i++) {
            if (!coreTree[i]) {
                levels[i] = maxLevel;
            }
        }

        while (true) {
            boolean check = false;

            for (int i = 0; i < count; i++) {
                for (int j = 0; j < count; j++) {
                    if (adjacencyMatrix[i][j] > 0 && !coreTree[i] && levels[i] >= levels[j]) {
                        levels[i] = levels[j] - 1;
                        check = true;
                    }
                }
            }

            if (!check) {
                break;
            }
        }

        int min = Arrays.stream(levels).min().orElse(0);
        for (int i = 0; i < levels.length; i++) {
            levels[i] -= min;
        }
    }

    private boolean[] buildCoreTree(
            boolean[] coreTree,
            int[] levels,
            int [][] adjacencyMatrix,
            int count,
            boolean fillCoreTree) {

        int maxLevel = Arrays.stream(levels).max().orElse(0);
        int currLevel = maxLevel;

        if (fillCoreTree) {
            for (int j = 0; j < count; j++) {
                if (levels[j] == maxLevel) {
                    coreTree[j] = true;
                }
            }
        }

        int idle = 0;
        int direction = 0;
        boolean idleCheck = false;
        while (idle < 10) {
            for (int j = 0; j < count; j++) {
                if (levels[j] != currLevel) {
                    continue;
                }

                for (int i = 0; i < count; i++) {
                    if (adjacencyMatrix[i][j] > 0 && levels[i] == currLevel - 1 && (coreTree[i] || coreTree[j])) {
                        if ((coreTree[i] || coreTree[j]) && (!coreTree[i] || !coreTree[j])) {
                            coreTree[i] = true;
                            coreTree[j] = true;
                            idleCheck = true;
                            break;
                        }
                    }
                }
            }
            if (direction == 0) {
                currLevel--;
            } else {
                currLevel++;
            }

            if (currLevel < 0) {
                if (!idleCheck) {
                    idle++;
                }
                direction = 1;
                idleCheck = false;
            }
            if (currLevel > maxLevel) {
                if (!idleCheck) {
                    idle++;
                }
                direction = 0;
                idleCheck = false;
            }
        }

        return coreTree;
    }

    private void fromTopToBottom(
            boolean[] coreTree,
            int[] levels,
            int [][] adjacencyMatrix,
            int count) {

        for (int i = 0; i < count; i++) {
            if (!coreTree[i]) {
                levels[i] = 0;
            }
        }

        while (true) {
            boolean check = false;

            for (int i = 0; i < count; i++) {
                for (int j = 0; j < count; j++) {
                    if (adjacencyMatrix[i][j] > 0 && !coreTree[j] && levels[i] >= levels[j]) {
                        levels[j] = levels[i] + 1;
                        check = true;
                    }
                }
            }

            if (!check) {
                break;
            }
        }

        int min = Arrays.stream(levels).min().orElse(0);
        for (int i = 0; i < levels.length; i++) {
            levels[i] -= min;
        }
    }

    /**
     * Note: the method changes adjacency matrix.
     */
    private void doDFS(int [][] adjacencyMatrix, int amountOfVertices, int startIndex,
                       boolean[] _used, int[] _colors, int[] _stack) {
        // map for colors, vertex index -> color.
        int NO_COLOR = 0;
        int COLOR_GREY = 1;
        int COLOR_BLACK = 2;

        int stackCount = 0;
        _stack[stackCount++] = startIndex;
        while (stackCount > 0) {
            int vertexIndex = _stack[stackCount - 1];

            _colors[vertexIndex] = COLOR_GREY;
            _used[vertexIndex] = true;

            boolean check = true;
            for (int i = 0; i < amountOfVertices; i++) {
                if (adjacencyMatrix[vertexIndex][i] > 0) {
                    int color = _colors[i];
                    if (color == NO_COLOR) {
                        _stack[stackCount++] = i;
                        check = false;
                        break;
                    } else {
                        if (color == COLOR_GREY) {
                            adjacencyMatrix[vertexIndex][i] = 0;
                        }
                    }
                }
            }

            if (check) {
                _colors[_stack[--stackCount]] = COLOR_BLACK;
            }
        }
    }
}
