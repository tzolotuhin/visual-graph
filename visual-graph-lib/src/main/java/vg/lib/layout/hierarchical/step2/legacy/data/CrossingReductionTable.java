package vg.lib.layout.hierarchical.step2.legacy.data;

import org.apache.commons.lang3.Validate;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalEdge;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.layout.hierarchical.step2.legacy.ApplyChainsProcedure;
import vg.lib.layout.hierarchical.step2.legacy.CalcCrossesOperation;
import vg.lib.layout.hierarchical.step2.legacy.CrossingReductionRowProcedure;
import vg.lib.layout.hierarchical.step2.legacy.EstCrossesOperation;
import vg.lib.layout.hierarchical.step2.legacy.ProximityOfVerticesOperation;
import vg.lib.layout.hierarchical.step2.legacy.SameTypeOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class CrossingReductionTable {
    public final int amountOfRows;

    public long bestW = Integer.MAX_VALUE;
    public int bestCrosses = Integer.MAX_VALUE;
    public int[][] bestOrders;
    public int[][] bestOrderToIndex;

    public int[][] orders;
    public int[][] backupOrders;
    public int[][] longBackupOrders;

    public int[][] orderToIndex;
    public int[][] backupOrderToIndex;
    public int[][] longBackupOrderToIndex;

    public boolean[][] visited;

    public Edge[][] edges;

    public Vertex[][] vertices;
    public List<Chain> chains;

    public Row[] rows;

    private final Vertex[] allVertices;

    public final List<Integer> rowIndicesOrder;

    public CrossingReductionTable(
            TreeMap<Integer, List<HierarchicalVertex>> group,
            HierarchicalGraph graph) {
        // initialize inner fields.
        amountOfRows = group.size() + 2;
        allVertices = new Vertex[graph.getVertices().size()];

        // initialize vertices.
        vertices = new Vertex[amountOfRows][];
        visited = new boolean[amountOfRows][];
        orders = new int[amountOfRows][];
        bestOrders = new int[amountOfRows][];
        bestOrderToIndex = new int[amountOfRows][];
        backupOrders = new int[amountOfRows][];
        orderToIndex = new int[amountOfRows][];
        backupOrderToIndex = new int[amountOfRows][];
        longBackupOrders = new int[amountOfRows][];
        longBackupOrderToIndex = new int[amountOfRows][];
        rows = new Row[amountOfRows];

        int rowIndex = 0;
        fillRow(graph.getInputPorts(), rowIndex++);
        for (var groupEntry : group.entrySet()) {
            fillRow(groupEntry.getValue(), rowIndex++);
        }
        fillRow(graph.getOutputPorts(), rowIndex);

        // initialize edges.
        edges = new Edge[amountOfRows][];

        rowIndex = 0;
        fillEdges(graph.getOutputEdgesDefault(graph.getInputPorts()), rowIndex++);
        for (var groupEntry : group.entrySet()) {
            fillEdges(graph.getOutputEdgesDefault(groupEntry.getValue()), rowIndex++);
        }
        fillEdges(Collections.emptyList(), rowIndex);

        // TODO: необходимо убрать из конструктора вызов процедур.
        // build chains.
        new ApplyChainsProcedure(this, graph).execute();

        for (rowIndex = 0; rowIndex < vertices.length; rowIndex++) {
            var rowChains = new ArrayList<Chain>();
            for (var chain : chains) {
                if (chain.isOnRow(rowIndex)) {
                    rowChains.add(chain);
                }
            }
            rows[rowIndex].setChains(rowChains);

            // TODO: calculate nodes
            int minRowIndex = Integer.MAX_VALUE;
            int maxRowIndex = Integer.MIN_VALUE;
            for (var chain : chains) {
                if (chain.startRowIndex < minRowIndex) {
                    minRowIndex = chain.startRowIndex;
                }

                if (chain.finishRowIndex > maxRowIndex) {
                    maxRowIndex = chain.finishRowIndex;
                }
            }

            var topGroups = new ArrayList<Group>();
            var bottomGroups = new ArrayList<Group>();
            for (int index = rowIndex + 1; index <= maxRowIndex; index++) {
                for (var vertex : vertices[index]) {
                    if (vertex.inputs.size() > 1 && (vertex.vertex.isRealVertex() || vertex.vertex.isCompanionVertex())) {
                        Collection<HierarchicalVertex> hParents = Collections.singletonList(vertex.vertex);
                        while (true) {
                            hParents = hParents.stream()
                                    .filter(x -> {
                                        var v = allVertices[x.getIndex()];
                                        return v != null;
                                    }).collect(Collectors.toList());

                            var parents = hParents.stream().map(x -> allVertices[x.getIndex()]).collect(Collectors.toList());

                            if (parents.isEmpty()) {
                                break;
                            }

                            if (parents.get(0).rowIndex == rowIndex) {
                                if (parents.size() >= 2) {
                                    bottomGroups.add(new Group(vertex, parents.stream().mapToInt(p -> p.columnIndex).toArray()));
                                }
                                break;
                            }

                            hParents = graph.getInputVertices(hParents).values();
                        }
                    }
                }
            }
            for (int index = rowIndex - 1; index >= minRowIndex; index--) {
                for (var vertex : vertices[index]) {
                    if (vertex.outputs.size() > 1 && (vertex.vertex.isRealVertex() || vertex.vertex.isCompanionVertex())) {
                        Collection<HierarchicalVertex> hChildren = Collections.singletonList(vertex.vertex);
                        while (true) {
                            hChildren = hChildren.stream()
                                    .filter(x -> {
                                        var v = allVertices[x.getIndex()];
                                        return v != null;
                                    }).collect(Collectors.toList());

                            var children = hChildren.stream().map(x -> allVertices[x.getIndex()]).collect(Collectors.toList());

                            if (children.isEmpty()) {
                                break;
                            }

                            if (children.get(0).rowIndex == rowIndex) {
                                if (children.size() >= 2) {
                                    topGroups.add(new Group(vertex, children.stream().mapToInt(p -> p.columnIndex).toArray()));
                                }
                                break;
                            }

                            hChildren = graph.getOutputVertices(hChildren).values();
                        }
                    }
                }
            }

            Collections.reverse(topGroups);
            Collections.reverse(bottomGroups);

            rows[rowIndex].setGroups(topGroups, bottomGroups);
        }

        rowIndicesOrder = new ArrayList<>();
        {
            rowIndicesOrder.add(0);
            rowIndicesOrder.add(vertices.length - 1);
        }
    }

    public void fixDirection(Direction direction) {
        for (var row : vertices) {
            for (var v : row) {
                if (v.isFixed || v.isSuitable(direction)) {
                    v.isFixed = true;
                }
            }
        }
    }

    public long doExecuteBottom(int startRowIndex, Direction direction, int[] vector) {
        long w = 0;
        for (int currLevelIndex = startRowIndex; currLevelIndex < vertices.length; currLevelIndex++) {
            var p = new CrossingReductionRowProcedure(
                    rows[currLevelIndex],
                    direction,
                    new CalcCrossesOperation(currLevelIndex == 0 ? new Edge[0] : edges[currLevelIndex - 1], direction),
                    new EstCrossesOperation(rows[currLevelIndex], direction),
                    new ProximityOfVerticesOperation(rows[currLevelIndex]),
                    new SameTypeOperation(rows[currLevelIndex]),
                    vector
            );
            p.execute();
            w += p.w;
        }

        backupBestSolution();

        return w;
    }

    public long doExecuteTop(int startRowIndex, Direction direction, int[] vector) {
        long w = 0;
        for (int currLevelIndex = startRowIndex; currLevelIndex >= 0; currLevelIndex--) {
            var p = new CrossingReductionRowProcedure(
                    rows[currLevelIndex],
                    direction,
                    new CalcCrossesOperation(currLevelIndex == vertices.length - 1 ? new Edge[0] : edges[currLevelIndex], direction),
                    new EstCrossesOperation(rows[currLevelIndex], direction),
                    new ProximityOfVerticesOperation(rows[currLevelIndex]),
                    new SameTypeOperation(rows[currLevelIndex]),
                    vector
            );
            p.execute();
            w += p.w;
        }

        backupBestSolution();

        return w;
    }

    public void backupBestSolution() {
        int crosses = calculateCrosses();
        if (bestCrosses > crosses) {
            bestCrosses = crosses;
            for (int rowIndex = 0; rowIndex < orders.length; rowIndex++) {
                System.arraycopy(orders[rowIndex], 0, bestOrders[rowIndex], 0, orders[rowIndex].length);
                System.arraycopy(orderToIndex[rowIndex], 0, bestOrderToIndex[rowIndex], 0, orders[rowIndex].length);
            }
        }
    }

    public void rollbackBestSolution() {
        for (int rowIndex = 0; rowIndex < bestOrders.length; rowIndex++) {
            System.arraycopy(bestOrders[rowIndex], 0, orders[rowIndex], 0, bestOrders[rowIndex].length);
            System.arraycopy(bestOrderToIndex[rowIndex], 0, orderToIndex[rowIndex], 0, bestOrderToIndex[rowIndex].length);
        }
    }

    private void fillRow(List<HierarchicalVertex> rowVertices, int rowIndex) {
        vertices[rowIndex] = new Vertex[rowVertices.size()];
        visited[rowIndex] = new boolean[rowVertices.size()];
        orders[rowIndex] = new int[rowVertices.size()];
        bestOrders[rowIndex] = new int[rowVertices.size()];
        bestOrderToIndex[rowIndex] = new int[rowVertices.size()];
        backupOrders[rowIndex] = new int[rowVertices.size()];
        orderToIndex[rowIndex] = new int[rowVertices.size()];
        backupOrderToIndex[rowIndex] = new int[rowVertices.size()];
        longBackupOrders[rowIndex] = new int[rowVertices.size()];
        longBackupOrderToIndex[rowIndex] = new int[rowVertices.size()];

        rows[rowIndex] = new Row(vertices[rowIndex], orders[rowIndex], orderToIndex[rowIndex], visited[rowIndex], rowIndex, this);

        for (int columnIndex = 0; columnIndex < rowVertices.size(); columnIndex++) {
            var hVertex = rowVertices.get(columnIndex);
            var vertex = new Vertex(
                    hVertex,
                    orders[rowIndex],
                    orderToIndex[rowIndex],
                    visited[rowIndex],
                    rowIndex,
                    columnIndex);

            vertices[rowIndex][columnIndex] = vertex;

            allVertices[hVertex.getIndex()] = vertex;
        }

        rows[rowIndex].resetOrders();
    }

    private void fillEdges(List<HierarchicalEdge> outputEdges, int rowIndex) {
        var edgeList = new ArrayList<Edge>();
        for (var outputEdge : outputEdges) {
            var source = allVertices[outputEdge.getSrcVertex().getIndex()];
            var target = allVertices[outputEdge.getTrgVertex().getIndex()];

            if (source == null || target == null) {
                continue;
            }

            var edge = new Edge(
                    outputEdge,
                    source.columnIndex,
                    target.columnIndex,
                    outputEdge.getSrcPortIndex(),
                    outputEdge.getTrgPortIndex(),
                    orders[rowIndex],
                    orders[rowIndex + 1],
                    vertices[rowIndex],
                    vertices[rowIndex + 1]
            );

            edgeList.add(edge);

            source.outputs.add(edge);
            target.inputs.add(edge);
        }

        edges[rowIndex] = new Edge[edgeList.size()];
        edgeList.toArray(edges[rowIndex]);
    }

    public void backup(int rowIndex) {
        System.arraycopy(orders[rowIndex], 0, backupOrders[rowIndex], 0, orders[rowIndex].length);
        System.arraycopy(orderToIndex[rowIndex], 0, backupOrderToIndex[rowIndex], 0, orderToIndex[rowIndex].length);
    }

    public void rollback(int rowIndex) {
        System.arraycopy(backupOrders[rowIndex], 0, orders[rowIndex], 0, backupOrders[rowIndex].length);
        System.arraycopy(backupOrderToIndex[rowIndex], 0, orderToIndex[rowIndex], 0, backupOrderToIndex[rowIndex].length);
    }

    public void longBackup(int rowIndex) {
        System.arraycopy(orders[rowIndex], 0, longBackupOrders[rowIndex], 0, orders[rowIndex].length);
        System.arraycopy(orderToIndex[rowIndex], 0, longBackupOrderToIndex[rowIndex], 0, orderToIndex[rowIndex].length);
    }

    public void longRollback(int rowIndex) {
        System.arraycopy(longBackupOrders[rowIndex], 0, orders[rowIndex], 0, longBackupOrders[rowIndex].length);
        System.arraycopy(longBackupOrderToIndex[rowIndex], 0, orderToIndex[rowIndex], 0, longBackupOrderToIndex[rowIndex].length);
    }

    public int calculateCrosses() {
        int crosses = 0;
        for (Edge[] edgeRow : edges) {
            crosses += new CalcCrossesOperation(edgeRow, Direction.ALL).execute().amount;
        }
        return crosses;
    }

    public void setupOrders() {
        for (int i = 0; i < vertices.length; i++) {
            for (int j = 0; j < vertices[i].length; j++) {
                var vertex = vertices[i][j];

                vertex.vertex.setOrder(orders[i][j]);

                Validate.isTrue(orders[i][j] >= 0, String.format("Vertex '%s' hasn't order.", vertex.vertex));
            }
        }
    }
}
