package vg.lib.layout.hierarchical.step1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class BaseLayeringOperation {
    protected final int numberOfVertices;
    protected final UUID[] vertexToId;
    protected final Map<UUID, Integer> vertexIdToIndex;

    protected final int numberOfEdges;
    protected final UUID[] edgeToId;
    protected final int[] edgeIndexToSrcVertexIndex;
    protected final int[] edgeIndexToTrgVertexIndex;
    protected final int[][] vertexIndexToInputEdgeIndices;
    protected final int[][] vertexIndexToOutputEdgeIndices;
    protected final boolean[] disabledEdges;

    // TODO: тоже следует удалить, т.к. много памяти и нет никакой доп. информации, которая уже будет в portIndexToId.
    protected final boolean[] inputPorts;
    protected final boolean[] outputPorts;
    protected boolean hasInputPorts;

    // TODO: should be deleted.
    protected Map<UUID, Map.Entry<UUID, UUID>> edgeIdToNodeIds;

    protected Map<UUID, Integer> portIdToSpecifiedOrder;

    protected BaseLayeringOperation(
        List<UUID> vertices,
        List<UUID> inputPorts,
        List<UUID> outputPorts,
        Map<UUID, Map.Entry<UUID, UUID>> edgeIdToNodeIds,
        Map<UUID, Integer> portIdToSpecifiedOrder) {
        numberOfVertices = vertices.size();
        vertexToId = new UUID[numberOfVertices];
        vertexIdToIndex = new HashMap<>();
        this.inputPorts = new boolean[numberOfVertices];
        this.outputPorts = new boolean[numberOfVertices];
        for (int i = 0; i < numberOfVertices; i++) {
            var vertexId = vertices.get(i);
            if (inputPorts.contains(vertexId)) {
                this.inputPorts[i] = true;
                hasInputPorts = true;
            }
            if (outputPorts.contains(vertexId)) {
                this.outputPorts[i] = true;
            }
            vertexToId[i] = vertexId;
            vertexIdToIndex.put(vertexId, i);
        }

        numberOfEdges = edgeIdToNodeIds.size();
        edgeToId = new UUID[numberOfEdges];
        disabledEdges = new boolean[numberOfEdges];
        edgeIndexToSrcVertexIndex = new int[numberOfEdges];
        edgeIndexToTrgVertexIndex = new int[numberOfEdges];
        var tmpVertexIndexToInputEdgeIndices = new HashMap<Integer, List<Integer>>();
        var tmpVertexIndexToOutputEdgeIndices = new HashMap<Integer, List<Integer>>();
        int edgeIndex = 0;
        for (var edgeIdToNodeIdsEntry : edgeIdToNodeIds.entrySet()) {
            edgeToId[edgeIndex] = edgeIdToNodeIdsEntry.getKey();

            var srcVertexId = edgeIdToNodeIdsEntry.getValue().getKey();
            var trgVertexId = edgeIdToNodeIdsEntry.getValue().getValue();

            int srcIndex = vertexIdToIndex.get(srcVertexId);
            int trgIndex = vertexIdToIndex.get(trgVertexId);

            edgeIndexToSrcVertexIndex[edgeIndex] = srcIndex;
            edgeIndexToTrgVertexIndex[edgeIndex] = trgIndex;

            tmpVertexIndexToInputEdgeIndices.putIfAbsent(trgIndex, new ArrayList<>());
            tmpVertexIndexToOutputEdgeIndices.putIfAbsent(srcIndex, new ArrayList<>());

            tmpVertexIndexToInputEdgeIndices.get(trgIndex).add(edgeIndex);
            tmpVertexIndexToOutputEdgeIndices.get(srcIndex).add(edgeIndex);

            edgeIndex++;
        }
        this.edgeIdToNodeIds = edgeIdToNodeIds;

        vertexIndexToInputEdgeIndices = new int[numberOfVertices][];
        vertexIndexToOutputEdgeIndices = new int[numberOfVertices][];
        for (int vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var inputEdgeIndices = tmpVertexIndexToInputEdgeIndices.getOrDefault(vertexIndex, List.of());
            vertexIndexToInputEdgeIndices[vertexIndex] = inputEdgeIndices.stream()
                .mapToInt(Integer::intValue).toArray();

            var outputEdgeIndices = tmpVertexIndexToOutputEdgeIndices.getOrDefault(vertexIndex, List.of());
            vertexIndexToOutputEdgeIndices[vertexIndex] = outputEdgeIndices.stream()
                .mapToInt(Integer::intValue).toArray();
        }

        this.portIdToSpecifiedOrder = portIdToSpecifiedOrder;
    }
}
