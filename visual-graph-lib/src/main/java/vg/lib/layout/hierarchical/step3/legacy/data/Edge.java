package vg.lib.layout.hierarchical.step3.legacy.data;

import org.apache.commons.lang3.Validate;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalEdge;

public class Edge {
    public final HierarchicalEdge original;
    private final int[] sourceRowXRef;
    private final int[] targetRowXRef;

    private final Vertex[] sourceRowVertexRef;
    private final Vertex[] targetRowVertexRef;

    public final int sourceIndex;
    public final int targetIndex;

    public final int sourceAmount;
    public final int targetAmount;

    public int sourceShift;
    public int targetShift;

    public boolean srcBetweenRealVertices;
    public boolean trgBetweenRealVertices;

    public Direction direction;

    public Edge(
            HierarchicalEdge edge,
            int sourceIndex,
            int targetIndex,
            int sourceAmount,
            int targetAmount,
            int sourceShift,
            int targetShift,
            int[] sourceRowXRef,
            int[] targetRowXRef,
            Vertex[] sourceRowVertexRef,
            Vertex[] targetRowVertexRef) {
        this.original = edge;

        this.sourceIndex = sourceIndex;
        this.targetIndex = targetIndex;

        this.sourceAmount = sourceAmount;
        this.targetAmount = targetAmount;

        this.sourceShift = sourceShift;
        this.targetShift = targetShift;

        this.sourceRowXRef = sourceRowXRef;
        this.targetRowXRef = targetRowXRef;

        this.sourceRowVertexRef = sourceRowVertexRef;
        this.targetRowVertexRef = targetRowVertexRef;

        this.direction = Direction.BOTH;
    }

    public boolean isSuitable(Direction value) {
        Validate.isTrue(direction != Direction.BOTH_AND_TOP);
        Validate.isTrue(direction != Direction.BOTH_AND_BOTTOM);

        if (direction == Direction.UNKNOWN) {
            return false;
        }

        if (value == Direction.BOTH_AND_BOTTOM && (direction == Direction.BOTH || direction == Direction.BOTTOM)) {
            return true;
        }
        if (value == Direction.BOTH_AND_TOP && (direction == Direction.BOTH || direction == Direction.TOP)) {
            return true;
        }
        return value == direction;
    }

    public int getSourceRowSize() {
        return sourceRowXRef.length;
    }

    public int getTargetRowSize() {
        return targetRowXRef.length;
    }

    public int getSourceX() {
        return sourceRowXRef[sourceIndex] + sourceShift;
    }

    public int getTargetX() {
        return targetRowXRef[targetIndex] + targetShift;
    }

    public Vertex getSourceBlock() {
        return sourceRowVertexRef[sourceIndex];
    }

    public Vertex getTargetBlock() {
        return targetRowVertexRef[targetIndex];
    }

    @Override
    public String toString() {
        return sourceRowVertexRef[sourceIndex] + " -> " + targetRowVertexRef[targetIndex] + " | " + direction;
    }
}
