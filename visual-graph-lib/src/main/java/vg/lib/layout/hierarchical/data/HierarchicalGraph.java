package vg.lib.layout.hierarchical.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.step1.CalcEdgeLevelGroupProcedure;
import vg.lib.layout.hierarchical.step1.CheckThatAllChildrenHasSameLevelAndGroupProcedure;
import vg.lib.layout.hierarchical.step1.CheckThatAllParentsHasSameLevelAndGroupProcedure;
import vg.lib.layout.hierarchical.step1.HandleEdgesBetweenOneLevelProcedure;
import vg.lib.layout.hierarchical.step1.InsertBeaconsProcedure;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Slf4j
public class HierarchicalGraph {
    public int crosses;

    @Getter
    private final Point vertexBorderSize;

    // TODO: должен быть приватным и настраиваемым.
    public final Point companionVertexSize;
    private final Point fakeVertexSize;
    private final Point fakeVertexBorderSize;

    public final boolean hasInputPorts;

    public final boolean hasOutputPorts;

    // start level of the graph (no ports) or -1 otherwise.
    @Getter
    private int startLevelOfGraph = -1;
    @Getter
    private int startLevelOfGraphWithInputPorts = -1;

    // finish level of the graph (no ports) or -1 otherwise.
    @Getter
    private int finishLevelOfGraph = -1;
    @Getter
    private int finishLevelOfGraphWithOutputPorts = -1;

    // finish level of the graph or -1 otherwise.
    @Getter
    private int rowSize = 0;

    // sorted using initial order.
    // Note: the list contains all vertices and ports.
    @Getter
    private final List<HierarchicalVertex> vertices = Lists.newArrayList();

    // sorted using initial order.
    @Getter
    private final Map<UUID, HierarchicalEdge> edges = new LinkedHashMap<>();

    @Getter
    private final LinkedHashMap<UUID, HierarchicalLongEdge> longEdges = Maps.newLinkedHashMap();

    @Getter
    private final LinkedHashMap<UUID, HierarchicalBackwardEdge> backwardEdges = Maps.newLinkedHashMap();

    public HierarchicalGraph(Point companionVertexSize, Point fakeVertexSize, Point vertexBorderSize, boolean hasInputPorts, boolean hasOutputPorts) {
        this.companionVertexSize = companionVertexSize;
        this.fakeVertexSize = fakeVertexSize;
        this.vertexBorderSize = vertexBorderSize;
        this.fakeVertexBorderSize = vertexBorderSize;
        this.hasInputPorts = hasInputPorts;
        this.hasOutputPorts = hasOutputPorts;
    }

    /*
     * Returns sorted input ports.
     *
     * +----------+
     * |   INPUT  |
     * |          |
     * |   GRAPH  |
     * |          |
     * |  OUTPUT  |
     * +----------+
     */
    public List<HierarchicalVertex> getInputPorts() {
        return vertices.stream().filter(HierarchicalVertex::isInputPort).sorted().collect(Collectors.toList());
    }

    /*
     * Returns sorted output ports.
     *
     * +----------+
     * |   INPUT  |
     * |          |
     * |   GRAPH  |
     * |          |
     * |  OUTPUT  |
     * +----------+
     */
    public List<HierarchicalVertex> getOutputPorts() {
        return vertices.stream().filter(HierarchicalVertex::isOutputPort).sorted().collect(Collectors.toList());
    }

    public HierarchicalEdge getEdgeById(UUID id) {
        return edges.get(id);
    }

    public List<HierarchicalEdge> getInputEdges(HierarchicalVertex vertex) {
        return getInputEdges(Collections.singletonList(vertex));
    }

    /**
     * Returns edges which sorted by order of input vertices.
     */
    public List<HierarchicalEdge> getInputEdges(Collection<HierarchicalVertex> vertices) {
        var inputEdgeIds = new LinkedHashSet<UUID>();
        var inputVertexIds = new LinkedHashSet<UUID>();
        for (var vertex : vertices) {
            inputEdgeIds.addAll(vertex.getInputEdges());
            inputVertexIds.addAll(vertex.getInputVertexIds());
        }

        var inputEdges = getEdgesByIds(inputEdgeIds);
        var inputVertices = getVerticesByIds(inputVertexIds);

        var result = new ArrayList<HierarchicalEdge>(inputEdges.size());
        for (var inputVertex : inputVertices) {
            for (var inputEdge : inputEdges) {
                if (Objects.equals(inputVertex, inputEdge.getSrcVertex())) {
                    result.add(inputEdge);
                }
            }
        }

        return result;
    }

    public List<HierarchicalEdge> getOutputEdgesSortedByOutputVertices(HierarchicalVertex vertex) {
        return getOutputEdgesSortedByOutputVertices(Collections.singletonList(vertex));
    }

    /**
     * Returns edges which sorted by order of output vertices.
     */
    public List<HierarchicalEdge> getOutputEdgesSortedByOutputVertices(Collection<HierarchicalVertex> vertices) {
        var outputEdgeIds = new LinkedHashSet<UUID>();
        var outputVertexIds = new LinkedHashSet<UUID>();
        for (var vertex : vertices) {
            outputEdgeIds.addAll(vertex.getOutputEdges());
            outputVertexIds.addAll(vertex.getOutputVertexIds());
        }

        var outputEdges = getEdgesByIds(outputEdgeIds);
        var outputVertices = getVerticesByIds(outputVertexIds);

        var result = new ArrayList<HierarchicalEdge>(outputEdges.size());
        for (var outputVertex : outputVertices) {
            for (var outputEdge : outputEdges) {
                if (Objects.equals(outputVertex, outputEdge.getTrgVertex())) {
                    result.add(outputEdge);
                }
            }
        }

        return result;
    }

    public List<HierarchicalEdge> getInputEdgesDefault(HierarchicalVertex[] vertices) {
        var inputEdgeIds = new LinkedHashSet<UUID>();
        for (var vertex : vertices) {
            inputEdgeIds.addAll(vertex.getInputEdges());
        }

        return getEdges().values().stream().filter(edge -> inputEdgeIds.contains(edge.getId())).collect(Collectors.toList());
    }

    public List<HierarchicalEdge> getOutputEdgesDefault(List<HierarchicalVertex> vertices) {
        var outputEdgeIds = new LinkedHashSet<UUID>();
        for (var vertex : vertices) {
            outputEdgeIds.addAll(vertex.getOutputEdges());
        }
        return getEdges().values().stream().filter(edge -> outputEdgeIds.contains(edge.getId())).collect(Collectors.toList());
    }

    public List<HierarchicalEdge> getOutputEdgesDefault(HierarchicalVertex[] vertices) {
        var outputEdgeIds = new LinkedHashSet<UUID>();
        for (var vertex : vertices) {
            outputEdgeIds.addAll(vertex.getOutputEdges());
        }
        return getEdges().values().stream().filter(edge -> outputEdgeIds.contains(edge.getId())).collect(Collectors.toList());
    }

    /**
     * Returns edges which sorted by order of the vertices arg.
     */
    public List<HierarchicalEdge> getOutputEdges(Collection<HierarchicalVertex> vertices) {
        var outputEdgeIds = new LinkedHashSet<UUID>();
        for (var vertex : vertices) {
            outputEdgeIds.addAll(vertex.getOutputEdges());
        }

        return getEdgesByIds(outputEdgeIds);
    }

    /**
     * Returns collection of edges using order of the ids arg.
     */
    public List<HierarchicalEdge> getEdgesByIds(Collection<UUID> ids) {
        return ids.stream().map(edges::get).collect(Collectors.toList());
    }

    public HierarchicalVertex getVertexById(UUID id) {
        return vertices.stream().filter(x -> x.getVertexId().equals(id)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    public HierarchicalVertex getVertexByName(int level, String name) {
        return vertices.stream().filter(x -> x.getLevel() == level && Objects.equals(x.getName(), name)).findFirst().orElseThrow(IllegalArgumentException::new);
    }

    /**
     * Returns vertices which sorted by order.
     */
    public List<HierarchicalVertex> getVerticesByIds(Collection<UUID> ids) {
        return vertices.stream()
                .filter(x -> ids.contains(x.getVertexId()))
                .sorted()
                .collect(Collectors.toList());
    }

    public LinkedHashMap<UUID, HierarchicalVertex> getInputVertices(Collection<HierarchicalVertex> vertices) {
        var inputVertices = new LinkedHashMap<UUID, HierarchicalVertex>();
        for (var vertex : vertices) {
            for (var inputVertex : getVerticesByIds(vertex.getInputVertexIds())) {
                if (!inputVertices.containsKey(inputVertex.getVertexId())) {
                    inputVertices.put(inputVertex.getVertexId(), inputVertex);
                }
            }
        }

        return inputVertices;
    }

    public LinkedHashMap<UUID, HierarchicalVertex> getOutputVertices(Collection<HierarchicalVertex> vertices) {
        var outputVertices = new LinkedHashMap<UUID, HierarchicalVertex>();
        for (var vertex : vertices) {
            for (var outputVertex : getVerticesByIds(vertex.getOutputVertexIds())) {
                if (!outputVertices.containsKey(outputVertex.getVertexId())) {
                    outputVertices.put(outputVertex.getVertexId(), outputVertex);
                }
            }
        }

        return outputVertices;
    }

    /**
     * Returns row which sorted by order.
     */
    public List<HierarchicalVertex> getRowByLevel(int level) {
        return vertices.stream().filter(x -> x.getLevel() == level).sorted().collect(Collectors.toList());
    }

    public void applyOrders(int level, List<String> orderedNames) {
        resetOrders(level);

        int index = 0;
        for (var name : orderedNames) {
            getVertexByName(level, name).setOrder(index++);
        }
    }

    public void resetOrders(int level) {
        getRowByLevel(level).forEach(v -> v.setOrder(-1));
    }

    public List<HierarchicalVertex> getFirstRow() {
        return getRowByLevel(startLevelOfGraph);
    }

    public List<HierarchicalVertex> getLastRow() {
        return getRowByLevel(finishLevelOfGraph);
    }

    public HierarchicalVertex addInputPort(UUID vertexId, int initialPortIndex, String name, int groupId, Point size) {
        var inputPortType = initialPortIndex >= 0 ? VertexType.REAL_INPUT_PORT_TYPE : VertexType.FAKE_INPUT_PORT_TYPE;

        var port = addVertex(vertexId, inputPortType, name, 0, groupId, size, null);
        port.setInitialPortIndex(initialPortIndex);
        return port;
    }

    public HierarchicalVertex addOutputPort(UUID vertexId, int initialPortIndex, String name, int groupId, Point size) {
        return addOutputPort(vertexId, initialPortIndex, name, getFinishLevelOfGraph() + 1, groupId, size);
    }

    public HierarchicalVertex addOutputPort(UUID vertexId,
                                            int initialPortIndex,
                                            String name,
                                            int layer,
                                            int groupId,
                                            Point size) {
        var outputPortType = initialPortIndex >= 0
            ? VertexType.REAL_OUTPUT_PORT_TYPE
            : VertexType.FAKE_OUTPUT_PORT_TYPE;

        var port = addVertex(
            vertexId, outputPortType, name, layer, groupId, size, null);
        port.setInitialPortIndex(initialPortIndex);
        return port;
    }

    // TODO: need to rename fake to dummy.
    public HierarchicalVertex addFakeVertex(int layer, int groupId, String name) {
        // TODO: возможно здесь не стоит передавать borderVertexSize.
        var fake = createVertex(
            UUID.randomUUID(),
            VertexType.FAKE_VERTEX_TYPE,
            name,
            layer,
            groupId,
            fakeVertexSize,
            fakeVertexBorderSize,
            null);
        vertices.add(fake);
        return fake;
    }

    public HierarchicalVertex addRealVertex(UUID vertexId,
                                            String name,
                                            int layer,
                                            int groupId,
                                            Point size) {
        return addVertex(vertexId, VertexType.VERTEX_TYPE, name, layer, groupId, size, null);
    }

    public HierarchicalVertex addRealVertexWithDefaultGroupId(UUID vertexId,
                                                              String name,
                                                              int layer,
                                                              Point size) {
        return addVertex(vertexId, VertexType.VERTEX_TYPE, name, layer, 0, size, null);
    }

    public HierarchicalVertex addVertex(UUID vertexId,
                                        VertexType vertexType,
                                        String name,
                                        int layer,
                                        int groupId,
                                        Point size) {
        return addVertex(vertexId, vertexType, name, layer, groupId, size, null);
    }

    public HierarchicalVertex addVertex(
        UUID vertexId,
        VertexType vertexType,
        String name,
        int layer,
        int groupId,
        Point size,
        Point fragmentTextSize) {
        var vertex = createVertex(
            vertexId, vertexType, name, layer, groupId, size, vertexBorderSize, fragmentTextSize);

        log.debug("Vertex was added: " + vertex.getName() + " - layer: " + layer);

        vertices.add(vertex);
        return vertex;
    }

    public void shiftLevelForGraphVertices(int shift) {
        shiftLevelForGraphVertices(-1, shift);
    }

    public void shiftLevelForGraphVertices(int startLevel, int shift) {
        vertices.forEach(vertex -> {
            if (vertex.isVertex() && vertex.getLevel() >= startLevel) {
                vertex.setLevel(vertex.getLevel() + shift);
            }
        });

        reCalculateRoots();
    }

    public void shiftLeftOrder(HierarchicalVertex vertex, int shift) {
        int order = vertex.getOrder();
        getRowByLevel(vertex.getLevel()).forEach(v -> {
            int vOrder = v.getOrder();
            if (vOrder <= order) {
                v.setOrder(vOrder - shift);
            }
        });
    }

    public void shiftRightOrder(HierarchicalVertex vertex, int shift) {
        int order = vertex.getOrder();
        getRowByLevel(vertex.getLevel()).forEach(v -> {
            int vOrder = v.getOrder();
            if (vOrder >= order) {
                v.setOrder(vOrder + shift);
            }
        });
    }

    // TODO: необходимо добавить различные проверки, и оговорить что данный метод работает только с уровнями с фейковыми вершинами.
    public void extendRows(int parentRowIndex, int groupId, int amount, Direction direction) {
        vertices.forEach(vertex -> {
            if (!vertex.isVertex()) {
                return;
            }

            if (direction == Direction.TOP && vertex.getLevel() < parentRowIndex) {
                return;
            }

            if (direction == Direction.BOTTOM && vertex.getLevel() < parentRowIndex) {
                return;
            }

            if (vertex.getGroupId() != groupId) {
                return;
            }

            vertex.setLevel(vertex.getLevel() + amount);
        });

        reCalculateRoots();
    }

    public void removeVertex(UUID vertexId) {
        removeVertex(getVertexById(vertexId));
    }

    public void removeVertex(HierarchicalVertex vertex) {
        Validate.isTrue(vertex.getInputEdges().isEmpty());
        Validate.isTrue(vertex.getOutputEdges().isEmpty());

        vertices.remove(vertex);
    }

    /**
     * Converts the edge to a long edge with save id of the edge.
     */
    public HierarchicalLongEdge convertToLongEdge(HierarchicalEdge edge) {
        if (!edge.isLongEdge()) {
            return null;
        }

        // collect all info about the edge before removing.
        var longEdgeId = edge.getLongEdgeId() == null ? edge.getId() : edge.getLongEdgeId();
        var groupId = edge.getSrcVertex().getGroupId();
        var order = -1;
        long importance = 0;
        var existedLongEdge = longEdges.get(longEdgeId);

        // remove the edge.
        removeEdge(edge);

        // fake vertex is necessary part of long edge if the vertex was placed on level with the companions.
        boolean isNecessaryPartOfLongEdge = false;

        var fakeVertexName = "FAKE";
        if (edge.isFake() && edge.getLongEdgeId() != null) {
            // the IF works only on stage 3.

            if (edge.getSrcVertex().isFakeVertex() && !edge.getSrcVertex().isCompanionVertex()) {
                fakeVertexName = edge.getSrcVertex().getName();
            }
            if (edge.getTrgVertex().isFakeVertex() && !edge.getTrgVertex().isCompanionVertex()) {
                fakeVertexName = edge.getTrgVertex().getName();
            }

            isNecessaryPartOfLongEdge = edge.getSrcVertex().isNecessaryPartOfLongEdge() | edge.getTrgVertex().isNecessaryPartOfLongEdge();

            // TODO: тут есть проблема, эта хрень считается правильно только в том случае если мы двигаем comp уровни вниз... вообщем нужно переделать.
            if (edge.getSrcVertex().isCompanionVertex()) {
                order = edge.getSrcVertex().getOrder();
                importance = edge.getSrcVertex().getImportance();
            } else if (edge.getTrgVertex().isCompanionVertex()) {
                order = edge.getTrgVertex().getOrder();
                importance = edge.getTrgVertex().getImportance();
            } else if (edge.getTrgVertex().isFakeVertex()) {
                order = edge.getTrgVertex().getOrder();
                importance = edge.getTrgVertex().getImportance();
            } else if (edge.getSrcVertex().isFakeVertex()) {
                order = edge.getSrcVertex().getOrder();
                importance = edge.getSrcVertex().getImportance();
            }
        } else {
            fakeVertexName = String.format("%s <-> %s -- F", edge.getSrcVertex().getName(), edge.getTrgVertex().getName());
            var backwardEdge = backwardEdges.get(longEdgeId);
            if (backwardEdge != null) {
                fakeVertexName = String.format("%s <-> %s -- B", backwardEdge.getSrcVertex().getName(), backwardEdge.getTrgVertex().getName());
            }

            if (edge.getSrcVertex().getLevel() == edge.getTrgVertex().getLevel() - 2) {
                var row = getRowByLevel(edge.getSrcVertex().getLevel() + 1);
                if (row.stream().noneMatch(HierarchicalVertex::isRealVertex)) {
                    isNecessaryPartOfLongEdge = true;
                }
            }
        }

        var prevVertex = edge.getSrcVertex();
        for (int level = edge.getSrcVertex().getLevel() + 1; level <= edge.getTrgVertex().getLevel(); level++) {
            HierarchicalVertex fakeVertex;
            if (level == edge.getTrgVertex().getLevel()) {
                fakeVertex = edge.getTrgVertex();
            } else {
                fakeVertex = addFakeVertex(level, groupId, fakeVertexName);

                fakeVertex.setLongEdgeId(longEdgeId);
                fakeVertex.setNecessaryPartOfLongEdge(isNecessaryPartOfLongEdge);
                fakeVertex.setBackEdgeId(prevVertex.getBackEdgeId());
                fakeVertex.setOrder(order);
                fakeVertex.setImportance(importance);
            }

            // add fake edge.
            var fakeEdge = addFakeEdge(prevVertex, fakeVertex);
            fakeEdge.setBackEdgeId(edge.getBackEdgeId());
            fakeEdge.setLongEdgeId(longEdgeId);

            if (level == edge.getSrcVertex().getLevel() + 1 && edge.getSrcPortId() != null) {
                fakeEdge.setSrcPortId(edge.getSrcPortId(), edge.getSrcPortIndex(), edge.getSrcPortPosX(), edge.getSrcPortSizeX());
            }
            if (level == edge.getTrgVertex().getLevel() && edge.getTrgPortId() != null) {
                fakeEdge.setTrgPortId(edge.getTrgPortId(), edge.getTrgPortIndex(), edge.getTrgPortPosX(), edge.getTrgPortSizeX());
            }

            prevVertex = fakeVertex;
        }

        var fakeVertices = new ArrayList<>(vertices.stream()
                .filter(x -> x.getLongEdgeId() != null && x.getLongEdgeId().equals(longEdgeId))
                .collect(Collectors.toMap(x -> x, HierarchicalVertex::getLevel))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new))
                .keySet());

        var fakeEdges = new ArrayList<>(edges.values().stream()
                .filter(x -> x.getLongEdgeId() != null && x.getLongEdgeId().equals(longEdgeId))
                .collect(Collectors.toMap(x -> x, v -> v.getSrcVertex().getLevel()))
                .entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new))
                .keySet());

        var originalEdgeId = longEdgeId;
        if (existedLongEdge != null) {
            originalEdgeId = existedLongEdge.getOriginalEdgeId();
        }

        var longEdge = new HierarchicalLongEdge(longEdgeId, originalEdgeId, fakeVertices, fakeEdges);

        if (longEdge.getSrcVertex().isCompanionVertex() && longEdge.getTrgVertex().isCompanionVertex()) {
            fakeVertices.forEach(v -> v.setType(VertexType.MAIN_BACK_VERTEX_TYPE));
        }

        longEdges.put(longEdgeId, longEdge);

        return longEdge;
    }

    public boolean rowIsBack(int level) {
        var row = getRowByLevel(level);

        if (row.isEmpty()) {
            return false;
        }

        boolean containsBack = false;
        for (var v : row) {
            if (!v.isFakeVertex()) {
                return false;
            }
            containsBack |= v.isBackVertex();
        }

        return containsBack;
    }

    /*
     * Note:
     * 1. the target vertex should have less level than the source vertex.
     * 2. the id from the passed edge will be used as the id for the future backward edge.
     * +---------+
     * |   TRG   |
     * |    ^    |
     * |    |    |
     * |    |    |
     * |   SRC   |
     * +---------+
     */
    public void convertToBackwardEdge(HierarchicalEdge backwardEdge) {
        var edgeId = backwardEdge.getId();
        var srcVertex = backwardEdge.getSrcVertex();
        var trgVertex = backwardEdge.getTrgVertex();

        Validate.notNull(edgeId, "Argument 'edgeId'  can't be null.");
        Validate.isTrue(srcVertex.getLevel() >= trgVertex.getLevel());

        removeEdge(backwardEdge);

        srcVertex.setContainsOutputBackEdges(true);
        trgVertex.setContainsInputBackEdges(true);

        // add fake companions for the source and the target vertices.
        var srcName = String.format("SC: %s [%s]", srcVertex.getName(), trgVertex.getName());
        var trgName = String.format("TC: %s [%s]", trgVertex.getName(), srcVertex.getName());

        var srcCompanionVertex = addFakeVertex(srcVertex.getLevel() + 1, srcVertex.getGroupId(), srcName);
        srcCompanionVertex.setCompanionVertex(srcVertex);
        srcCompanionVertex.setBackEdgeId(edgeId);
        srcCompanionVertex.setType(VertexType.BOTTOM_COMPANION_VERTEX_TYPE);
        srcCompanionVertex.getSize().x = companionVertexSize.x;
        srcCompanionVertex.getSize().y = companionVertexSize.y;
        srcCompanionVertex.getBorderSize().x = 0;
        var trgCompanionVertex = addFakeVertex(trgVertex.getLevel() - 1, trgVertex.getGroupId(), trgName);
        trgCompanionVertex.setCompanionVertex(trgVertex);
        trgCompanionVertex.setBackEdgeId(edgeId);
        trgCompanionVertex.setType(VertexType.TOP_COMPANION_VERTEX_TYPE);
        trgCompanionVertex.getSize().x = companionVertexSize.x;
        trgCompanionVertex.getSize().y = companionVertexSize.y;
        trgCompanionVertex.getBorderSize().x = 0;

        if (trgVertex.getLevel() == 0 || (hasInputPorts && trgVertex.getLevel() == 1)) {
            shiftLevelForGraphVertices(1);
        }

        var edge = addFakeEdge(trgCompanionVertex, srcCompanionVertex);
        edge.setBackEdgeId(edgeId);

        var topEdge = addEdge(
                trgCompanionVertex,
                trgVertex,
                null,
                null,
                backwardEdge.getTrgPortId(),
                -1,
                backwardEdge.getTrgPortIndex(),
                -1,
                backwardEdge.getTrgPortPosX(),
                0,
                backwardEdge.getTrgPortSizeX());
        topEdge.setFake(true);
        topEdge.setBackEdgeId(edgeId);

        var bottomEdge = addEdge(
                srcVertex,
                srcCompanionVertex,
                null,
                backwardEdge.getSrcPortId(),
                null,
                backwardEdge.getSrcPortIndex(),
                -1,
                backwardEdge.getSrcPortPosX(),
                -1,
                backwardEdge.getSrcPortSizeX(),
                0);
        bottomEdge.setFake(true);
        bottomEdge.setBackEdgeId(edgeId);

        var result = new HierarchicalBackwardEdge(
                edgeId,
                edge.getId(),
                topEdge.getId(),
                bottomEdge.getId(),
                srcVertex,
                trgVertex,
                srcCompanionVertex,
                trgCompanionVertex
        );

        backwardEdges.put(edgeId, result);

        // TODO: тут есть проблема, т.к. сдвиг будет действовать не только на группу в которой находятся данные вершины но и на другие, что не правильно.
        {
            var row = getRowByLevel(trgCompanionVertex.getLevel());
            if (row.stream().anyMatch(vertex -> !vertex.isCompanionVertex())) {
                shiftLevelForGraphVertices(trgVertex.getLevel(), 1);
                for (var vertex : row) {
                    if (vertex.isCompanionVertex()) {
                        vertex.setLevel(trgCompanionVertex.getLevel() + 1);
                    }
                }
            }
        }
        {
            var row = getRowByLevel(srcCompanionVertex.getLevel());
            if (row.stream().anyMatch(vertex -> !vertex.isCompanionVertex())) {
                shiftLevelForGraphVertices(srcCompanionVertex.getLevel() + 1, 1);
                for (var vertex : row) {
                    if (!vertex.isCompanionVertex()) {
                        vertex.setLevel(vertex.getLevel() + 1);
                    }
                }
            }
        }
    }

    public void removeLongEdges(Collection<UUID> edgeIds) {
        edgeIds.forEach(this::removeLongEdge);
    }

    public void removeLongEdge(UUID longEdgeId) {
        var longEdge = longEdges.get(longEdgeId);
        if (longEdge == null) {
            removeEdge(longEdgeId);
            return;
        }

        longEdge.getFakeEdges().forEach(this::removeEdge);
        longEdge.getFakeVertices().forEach(this::removeVertex);
        longEdges.remove(longEdgeId);
    }

    public HierarchicalEdge addEdge(HierarchicalEdge edge) {
        return doAddEdge(
                edge.getSrcVertex(),
                edge.getTrgVertex(),
                edge.hasLongEdgeId() ? edge.getLongEdgeId() : edge.getId(),
                edge.getSrcPortId(),
                edge.getTrgPortId(),
                edge.getSrcPortIndex(),
                edge.getTrgPortIndex(),
                edge.getSrcPortPosX(),
                edge.getTrgPortPosX(),
                edge.getSrcPortSizeX(),
                edge.getTrgPortSizeX());
    }

    public HierarchicalEdge addFakeEdge(HierarchicalVertex source, HierarchicalVertex target) {
        var edge = addEdge(
                source,
                target,
                null,
                null,
                null,
                -1,
                -1,
                -1,
                -1,
                0,
                0);

        edge.setFake(true);

        return edge;
    }

    public HierarchicalEdge addEdge(HierarchicalVertex source, HierarchicalVertex target) {
        var edge = addEdge(
                source,
                target,
                null,
                null,
                null,
                -1,
                -1,
                -1,
                -1,
                0,
                0);

        return edge;
    }

    public HierarchicalEdge addEdge(
            HierarchicalVertex source,
            HierarchicalVertex target,
            UUID edgeId,
            UUID srcPortId,
            UUID trgPortId,
            int srcPortIndex,
            int trgPortIndex,
            int srcPortPosX,
            int trgPortPosX,
            int srcPortSizeX,
            int trgPortSizeX) {
        return doAddEdge(
                source,
                target,
                edgeId,
                srcPortId,
                trgPortId,
                srcPortIndex,
                trgPortIndex,
                srcPortPosX,
                trgPortPosX,
                srcPortSizeX,
                trgPortSizeX);
    }

    public void removeEdge(UUID edgeId) {
        removeEdge(getEdgeById(edgeId));
    }

    public void removeEdge(HierarchicalEdge edge) {
        edge.getSrcVertex().removeOutputEdge(edge.getId());
        edge.getTrgVertex().removeInputEdge(edge.getId());

        edges.remove(edge.getId());
    }

    private HierarchicalEdge doAddEdge(
            HierarchicalVertex source,
            HierarchicalVertex target,
            UUID edgeId,
            UUID srcPortId,
            UUID trgPortId,
            int srcPortIndex,
            int trgPortIndex,
            int srcPortPosX,
            int trgPortPosX,
            int srcPortSizeX,
            int trgPortSizeX) {
        if (edgeId == null) {
            edgeId = UUID.randomUUID();
        } else {
            var edge = getEdgeById(edgeId);
            if (edge != null) {
                return edge;
            }
        }

        source.addOutputEdge(edgeId, target.getVertexId());
        target.addInputEdge(edgeId, source.getVertexId());

        var edge = new HierarchicalEdge(edgeId, source, target);

        if (srcPortId != null) {
            edge.setSrcPortId(srcPortId, srcPortIndex, srcPortPosX, srcPortSizeX);
        }
        if (trgPortId != null) {
            edge.setTrgPortId(trgPortId, trgPortIndex, trgPortPosX, trgPortSizeX);
        }

        edges.put(edgeId, edge);

        return edge;
    }

    public void resetOrders() {
        // reset order for all elements.
        vertices.forEach(x -> x.setOrder(-1));

        // setup initial orders for the input ports.
        HierarchicalVertex.setupDefaultOrderForElements(getInputPorts());

        // setup initial orders for the output ports.
        HierarchicalVertex.setupDefaultOrderForElements(getOutputPorts());
    }

    /**
     * Creates a vertex without inserting to the graph.
     */
    public static HierarchicalVertex createVertex(
            UUID vertexId,
            VertexType vertexType,
            String name,
            int layer,
            int groupId,
            Point size,
            Point borderSize,
            Point fragmentTextSize) {
        var e = new HierarchicalVertex(vertexId, vertexType, layer, groupId, size, borderSize, fragmentTextSize);
        e.setName(name);
        return e;
    }

    public List<HierarchicalVertex> getVertices(int groupId) {
        return vertices.stream().filter(x -> x.getGroupId() == groupId).collect(Collectors.toList());
    }

    public List<HierarchicalVertex> getVertices(int groupId, int level) {
        return vertices.stream().filter(x -> x.getGroupId() == groupId && x.getLevel() == level).collect(Collectors.toList());
    }

    public static class LookupCommonRootResult {
        public final HierarchicalVertex root;
        public final Collection<HierarchicalVertex> leftVertices;
        public final Collection<HierarchicalVertex> rightVertices;
        public final Collection<HierarchicalVertex> commonVertices;

        public LookupCommonRootResult(
                HierarchicalVertex root,
                Collection<HierarchicalVertex> leftVertices,
                Collection<HierarchicalVertex> rightVertices,
                Collection<HierarchicalVertex> commonVertices) {
            this.root = root;
            this.leftVertices = leftVertices;
            this.rightVertices = rightVertices;
            this.commonVertices = commonVertices;
        }

        public List<HierarchicalVertex> reverse() {
            var result = new ArrayList<HierarchicalVertex>();
            result.addAll(rightVertices);
            result.addAll(commonVertices);
            result.addAll(leftVertices);
            return result;
        }
    }

    /**
     * Returns groups of elements without ports.
     * GroupId -> List of elements of the group.
     */
    private TreeMap<Integer, List<HierarchicalVertex>> getGroups() {
        return vertices.stream()
                .filter(x -> !x.isInputPort() && !x.isOutputPort())
                .collect(sortedGroupingBy(HierarchicalVertex::getGroupId));
    }

    public int[] getGroupIds() {
        return vertices.stream().map(HierarchicalVertex::getGroupId)
            .collect(Collectors.toSet()).stream()
            .mapToInt(Integer::intValue)
            .sorted()
            .toArray();
    }

    public List<HierarchicalVertex> getVerticesByGroupId(int groupId) {
        return vertices.stream()
            .filter(vertex -> vertex.getGroupId() == groupId)
            .collect(Collectors.toList());
    }

    public List<HierarchicalEdge> getEdgesByGroupId(int groupId) {
        return edges.values().stream()
            .filter(edge -> edge.getGroupId() == groupId)
            .collect(Collectors.toList());
    }

    // TODO: should be removed.
    public TreeMap<Integer, List<HierarchicalVertex>> getDefaultDetailedGroup() {
        return getDetailedGroups().get(0);
    }

    /**
     * Returns detailed groups of elements without ports.
     * GroupId -> LevelId -> List of sorted elements (using current order or initial order) of the level.
     */
    public Map<Integer, TreeMap<Integer, List<HierarchicalVertex>>> getDetailedGroups() {
        var result = new TreeMap<Integer, TreeMap<Integer, List<HierarchicalVertex>>>();
        for (var groupEntry : getGroups().entrySet()) {
            var sortedByLevel = groupEntry.getValue().stream().collect(sortedGroupingBy(HierarchicalVertex::getLevel));
            sortedByLevel.values().forEach(x -> x.sort(HierarchicalVertex::compareTo));
            result.put(groupEntry.getKey(), sortedByLevel);
        }

        return result;
    }

    public void build(List<UUID> initialVertices) {
        new HandleEdgesBetweenOneLevelProcedure(this).execute();

        // update roots.
        reCalculateRoots();

        // handle backward edges.
        var backwardEdges = getEdges().values().stream().filter(edge -> edge.getSrcVertex().getLevel() >= edge.getTrgVertex().getLevel()).collect(Collectors.toList());
        for (var backwardEdge : backwardEdges) {
            convertToBackwardEdge(backwardEdge);
        }

        // update roots.
        reCalculateRoots();

        // handle long edges.
        var longEdges = getEdges().values().stream().filter(HierarchicalEdge::isLongEdge).collect(Collectors.toList());
        for (var longEdge : longEdges) {
            convertToLongEdge(longEdge);
        }

        // update roots.
        reCalculateRoots();

        new InsertBeaconsProcedure(this).execute();

        log.debug("Calculate initial order for the vertices...");
        reCalculateInitialOrderForVertices(initialVertices);
        log.debug("Initial order for the vertices were calculated...");

        log.debug("Calculate initial order for the long edges...");
        reCalculateInitialOrderForLongEdges();
        log.debug("Initial order for the long edges were calculated...");

        new CalcEdgeLevelGroupProcedure(this).execute();

        new CheckThatAllParentsHasSameLevelAndGroupProcedure(this).execute();
        new CheckThatAllChildrenHasSameLevelAndGroupProcedure(this).execute();
    }

    public void printResultData(String message) {
        printResultData(message, -1);
    }

    public void printResultData(String message, int level) {
        printHierarchy(message);
        //printTable(level);
    }

    public void printHierarchy(String message) {
        log.debug("################################################################################");

        log.debug(message);

        if (VGConfigSettings.DIAGNOSTIC_MODE) {
            log.debug("Print levels (amount: {}):", rowSize);
            for (int rowIndex = 0; rowIndex < rowSize; rowIndex++) {
                var row = getRowByLevel(rowIndex);

                printCollection(String.format("\nLevel %s:", rowIndex), row);
            }

            log.debug("Print edges (amount: {}):", edges.size());
            for (var edge : edges.values()) {
                log.debug(edge.toString());
            }
        }

        log.debug("################################################################################");
    }

    public void printTable(int level) {
        log.debug("################################################################################");

        int maxOrder = vertices.stream().mapToInt(HierarchicalVertex::getOrder).max().orElse(-1);
        for (int rowIndex = 0; rowIndex < rowSize; rowIndex++) {
            var row = getRowByLevel(rowIndex);

            // check that all elements has order.
            row.forEach(x -> Validate.isTrue(x.getOrder() >= 0, String.format("Element '%s' hasn't order.", x.getVertexId())));

            char[] rowArray = new char[maxOrder + 1];
            Arrays.fill(rowArray, ' ');
            row.forEach(x -> {
                switch (x.getType()) {
                    case VERTEX_TYPE:
                        rowArray[x.getOrder()] = 'R';
                        break;
                    case FAKE_VERTEX_TYPE:
                        rowArray[x.getOrder()] = 'F';
                        break;
                }
            });

            String levelRowPrefix = String.format("%3s", rowIndex);

            String levelPrefix = "| ";
            if (rowIndex == level) {
                levelPrefix = "> ";
            }

            log.debug(levelRowPrefix + levelPrefix + StringUtils.join(Arrays.asList(ArrayUtils.toObject(rowArray)), " | "));
        }

        log.debug("################################################################################");
    }

    public static void printCollection(String message, Collection<HierarchicalVertex> collection) {
        var sb = new StringBuilder();

        if (message != null) {
            sb.append(message);
            sb.append("\n");
        }

        for (var e : collection) {
            sb.append(String.format("    %s\n", e));
        }
        log.debug("----------------------------------------");
        log.debug(sb.toString());
        log.debug("----------------------------------------");
    }

    public void printElement(String message, HierarchicalVertex element) {
        var name = "";

        if (element != null) {
            name = element.getName();
        }

        if (name == null) {
            name = "";
        }
        log.debug(String.format("%s %5s %s\n", message, name, element));
    }

    /**
     * Sort the elements in the graph using initial order of the input vertices.
     */
    public void reCalculateInitialOrderForVertices(List<UUID> initialVertices) {
        reCalculateRoots();

        if (initialVertices != null) {
            Map<HierarchicalVertex, Float> unsortedMap = Maps.newLinkedHashMap();
            vertices.forEach(x -> {
                int index = initialVertices.indexOf(x.getVertexId());

                if ((x.isPort() || x.isRealVertex()) && index >= 0) {
                    unsortedMap.put(x, (float) index);
                } else {
                    if (x.getLongEdgeId() != null) {
                        var longEdge = longEdges.get(x.getLongEdgeId());
                        Validate.notNull(longEdge);

                        int sourceElementIndex = initialVertices.indexOf(longEdge.getSrcVertex().getVertexId());
                        int targetElementIndex = initialVertices.indexOf(longEdge.getTrgVertex().getVertexId());

                        unsortedMap.put(x, sourceElementIndex + 0.01f * targetElementIndex);
                    } else {
                        unsortedMap.put(x, Float.MAX_VALUE);
                    }
                }
            });

            vertices.clear();
            unsortedMap.entrySet().stream()
                    .sorted(Map.Entry.comparingByValue())
                    .forEachOrdered(x -> vertices.add(x.getKey()));
        }

        reCalculateVertexIndices();

        // need recalculate for right orders
        reCalculateRoots();
    }

    public void reCalculateVertexIndices() {
        int index = 0;
        for (var vertex : vertices) {
            vertex.setIndex(index++);
        }
    }

    public void reCalculateInitialOrderForLongEdges() {
        Map<HierarchicalLongEdge, Float> unsortedMap = Maps.newLinkedHashMap();

        longEdges.values().forEach(longEdge -> {
            int sourceElementIndex = vertices.indexOf(longEdge.getSrcVertex());
            int targetElementIndex = vertices.indexOf(longEdge.getTrgVertex());

            unsortedMap.put(longEdge, longEdge.getFakeVertices().size() + (float)(sourceElementIndex * 0.01 + 0.0001f * targetElementIndex));
        });

        longEdges.clear();
        unsortedMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEachOrdered(x -> longEdges.put(x.getKey().getLongEdgeId(), x.getKey()));
    }

    public void reCalculateRoots() {
        // update start and finish levels for the graph.
        startLevelOfGraph = -1;
        finishLevelOfGraph = -1;
        rowSize = 0;

        var stats = vertices.stream().filter(HierarchicalVertex::isVertex).mapToInt(HierarchicalVertex::getLevel).summaryStatistics();
        if (stats.getCount() > 0) {
            startLevelOfGraphWithInputPorts = startLevelOfGraph = stats.getMin();
            finishLevelOfGraphWithOutputPorts = finishLevelOfGraph = stats.getMax();
            rowSize = finishLevelOfGraph - startLevelOfGraph + 1;
        }

        // update row size and fix levels for input and output ports.
        var inputPorts = getInputPorts();
        var outputPorts = getOutputPorts();

        if (!inputPorts.isEmpty()) {
            startLevelOfGraphWithInputPorts--;
            rowSize++;
            inputPorts.forEach(port -> port.setLevel(0));
        }

        if (!outputPorts.isEmpty()) {
            finishLevelOfGraphWithOutputPorts++;
            rowSize++;
            outputPorts.forEach(port -> port.setLevel(rowSize - 1));
        }
    }

    public static int shiftOrderForDetailedGroup(int startGroupOrder, Map<Integer, List<HierarchicalVertex>> detailedGroup) {
        int maxOrder = -1;
        for (var levelEntry : detailedGroup.entrySet()) {
            for (var e : levelEntry.getValue()) {
                e.setOrder(e.getOrder() + startGroupOrder);

                if (maxOrder < e.getOrder()) {
                    maxOrder = e.getOrder();
                }
            }
        }
        return maxOrder;
    }

    public static int shiftPosXForDetailedGroup(int startGroupPosX, Map<Integer, List<HierarchicalVertex>> detailedGroup) {
        int maxPosX = 0;
        for (var levelEntry : detailedGroup.entrySet()) {
            for (var e : levelEntry.getValue()) {
                e.setPosX(e.getPosX() + startGroupPosX);

                if (maxPosX < e.getPosX() + e.getWidth()) {
                    maxPosX = e.getPosX() + e.getWidth();
                }
            }
        }
        return maxPosX;
    }

    private static <T, K extends Comparable<K>> Collector<T, ?, TreeMap<K, List<T>>> sortedGroupingBy(Function<T, K> function) {
        return Collectors.groupingBy(
                function,
                TreeMap::new,
                Collectors.toList());
    }

    private static void checkThatAllVerticesHasSameLevel(Collection<HierarchicalVertex> vertices) {
        if (vertices == null || vertices.isEmpty()) {
            return;
        }

        var referenceVertex = vertices.iterator().next();
        for (var vertex : vertices) {
            if (referenceVertex.getLevel() != vertex.getLevel()) {
                printCollection("Two or more vertices has different levels.", vertices);
                throw new IllegalArgumentException(String.format(
                        "Two or more vertices (%s) has different levels.",
                        vertices.stream().map(HierarchicalVertex::getName).collect(Collectors.joining(", "))));
            }
        }
    }

    private static void checkThatAllVerticesHasSameGroup(Collection<HierarchicalVertex> vertices) {
        if (vertices == null || vertices.isEmpty()) {
            return;
        }

        var referenceVertex = vertices.iterator().next();
        for (var vertex : vertices) {
            if (referenceVertex.getGroupId() != vertex.getGroupId()) {
                printCollection("Two or more vertices has different group ids.", vertices);
                throw new IllegalArgumentException(String.format(
                        "Two or more vertices ('%s') has different group ids.",
                        vertices.stream().map(HierarchicalVertex::getName).collect(Collectors.joining(", "))));
            }
        }
    }
}
