package vg.lib.layout.hierarchical.step1;

import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.operation.Procedure;

// TODO: Кандидат на удаление.
public class HandleEdgesBetweenOneLevelProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public HandleEdgesBetweenOneLevelProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        boolean check = true;
        while(check) {
            check = false;
            for (var edge : graph.getEdges().values()) {
                var srcVertex = edge.getSrcVertex();
                var trgVertex = edge.getTrgVertex();

                if (srcVertex != trgVertex && srcVertex.getLevel() == trgVertex.getLevel()) {
                    trgVertex.setLevel(trgVertex.getLevel() + 1);
                    check = true;
                }
            }
        }

        graph.reCalculateRoots();
    }
}
