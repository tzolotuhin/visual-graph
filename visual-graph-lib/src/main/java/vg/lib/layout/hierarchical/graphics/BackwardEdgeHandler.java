package vg.lib.layout.hierarchical.graphics;

import vg.lib.layout.hierarchical.HierarchicalLayoutSettings;
import vg.lib.layout.hierarchical.HierarchicalPlainLayout;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class BackwardEdgeHandler extends EdgeHandler {
    private static final float DX_P = 0.5f;

    private final int inputYMargin, outputYMargin;

    public BackwardEdgeHandler(HierarchicalPlainLayout.Result.OutputEdge outputEdge,
                               HierarchicalPlainLayout.Result.OutputVertex outputSrcVertex,
                               HierarchicalPlainLayout.Result.OutputVertex outputTrgVertex,
                               HierarchicalLayoutSettings settings) {
        super(outputEdge, outputSrcVertex, outputTrgVertex, settings);

        this.inputYMargin = settings.arrowSize + settings.baseArrow;
        this.outputYMargin = settings.arrowSize + settings.baseArrow;
    }

    public List<Point> execute() {
        calcStartEdgePoints(outputYMargin);
        calcFinishEdgePoints(inputYMargin);

        result.addAll(splineInterpolationIfNeed(buildBottomChain()));
        result.addAll(splineInterpolationIfNeed(buildChainBetweenCompanions(), true));
        result.addAll(splineInterpolationIfNeed(buildTopChain()));

        return result;
    }

    public List<Point> buildBottomChain() {
        var chain = new ArrayList<Point>();

        chain.add(srcVertexPoint);
        chain.add(srcVertexWithOutputMarginPoint);

        for (var additionalPoint : outputEdge.getBottomAdditionalPoints()) {
            chain.add(new Point(additionalPoint.roundCenterX(), additionalPoint.yTop()));
            chain.add(new Point(additionalPoint.roundCenterX(), additionalPoint.roundCenterY()));
        }

        if (!outputEdge.getBottomAdditionalPoints().isEmpty() && settings.routingStyle == HierarchicalLayoutSettings.SPLINE_ROUTING_STYLE) {
            var bottomPoint = outputEdge.getBottomAdditionalPoints().get(outputEdge.getBottomAdditionalPoints().size() - 1);
            int dx = Math.round(outputEdge.getSrcCompanion().getSize().x * DX_P);
            if (outputEdge.getBottomPointX() < outputEdge.getBottomAdditionalPointX()) {
                chain.add(new Point(bottomPoint.roundCenterX() + dx, bottomPoint.yBottom()));
            } else {
                chain.add(new Point(bottomPoint.roundCenterX() - dx, bottomPoint.yBottom()));
            }
        } else {
            chain.add(new Point(outputEdge.getSrcCompanion().roundCenterX(), outputEdge.getSrcCompanion().roundCenterY()));
        }

        return chain;
    }

    public List<Point> buildChainBetweenCompanions() {
        var chain = new ArrayList<Point>();

        if (!outputEdge.getAdditionalPoints().isEmpty() && settings.routingStyle == HierarchicalLayoutSettings.SPLINE_ROUTING_STYLE) {
            var topPoint = outputEdge.getAdditionalPoints().get(0);
            int dx = Math.round(outputEdge.getTrgCompanion().getSize().x * DX_P);
            if (outputEdge.getTopPointX() > outputEdge.getTopAdditionalPointX()) {
                chain.add(new Point(topPoint.roundCenterX() + dx, topPoint.yTop()));
            } else {
                chain.add(new Point(topPoint.roundCenterX() - dx, topPoint.yTop()));
            }
        }

        for (var additionalPoint : outputEdge.getAdditionalPoints()) {
            chain.add(new Point(additionalPoint.roundCenterX(), additionalPoint.roundCenterY()));
        }

        if (!outputEdge.getAdditionalPoints().isEmpty() && settings.routingStyle == HierarchicalLayoutSettings.SPLINE_ROUTING_STYLE) {
            var bottomPoint = outputEdge.getAdditionalPoints().get(outputEdge.getAdditionalPoints().size() - 1);
            int dx = Math.round(outputEdge.getSrcCompanion().getSize().x * DX_P);
            if (outputEdge.getBottomPointX() < outputEdge.getBottomAdditionalPointX()) {
                chain.add(new Point(bottomPoint.roundCenterX() - dx, bottomPoint.yBottom()));
            } else {
                chain.add(new Point(bottomPoint.roundCenterX() + dx, bottomPoint.yBottom()));
            }
        }

        return chain;
    }

    public List<Point> buildTopChain() {
        var chain = new ArrayList<Point>();

        if (!outputEdge.getTopAdditionalPoints().isEmpty() && settings.routingStyle == HierarchicalLayoutSettings.SPLINE_ROUTING_STYLE) {
            var topPoint = outputEdge.getTopAdditionalPoints().get(0);
            int dx = Math.round(outputEdge.getTrgCompanion().getSize().x * DX_P);
            if (outputEdge.getTopPointX() > outputEdge.getTopAdditionalPointX()) {
                chain.add(new Point(topPoint.roundCenterX() - dx, topPoint.yTop()));
            } else {
                chain.add(new Point(topPoint.roundCenterX() + dx, topPoint.yTop()));
            }
        } else {
            chain.add(new Point(outputEdge.getTrgCompanion().roundCenterX(), outputEdge.getTrgCompanion().roundCenterY()));
        }

        for (var additionalPoint : outputEdge.getTopAdditionalPoints()) {
            chain.add(new Point(additionalPoint.roundCenterX(), additionalPoint.roundCenterY()));
            chain.add(new Point(additionalPoint.roundCenterX(), additionalPoint.yBottom()));
        }

        chain.add(trgVertexWithOutputMarginPoint);
        chain.add(trgVertexPoint);

        return chain;
    }
}
