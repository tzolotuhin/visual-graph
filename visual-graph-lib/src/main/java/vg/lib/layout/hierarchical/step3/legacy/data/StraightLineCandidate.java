package vg.lib.layout.hierarchical.step3.legacy.data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StraightLineCandidate {
    public List<Vertex> vertices;
    public List<Edge> edges;

    public StraightLineCandidate(ArrayList<Edge> edges) {
        this.edges = edges;
        this.vertices = new ArrayList<>();
        for (int i = 0; i < edges.size(); i++) {
            var edge = edges.get(i);
            if (i == 0) {
                vertices.add(edge.getSourceBlock());
            }
            vertices.add(edge.getTargetBlock());
        }
    }

    @Override
    public String toString() {
        return vertices.stream().map(Vertex::toString).collect(Collectors.joining(", "));
    }
}
