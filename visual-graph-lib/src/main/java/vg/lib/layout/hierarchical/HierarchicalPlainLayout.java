package vg.lib.layout.hierarchical;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.layout.hierarchical.data.HierarchicalEdge;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.layout.hierarchical.data.VertexType;
import vg.lib.layout.hierarchical.step1.CustomLayeringOperation;
import vg.lib.layout.hierarchical.step1.FineTuningLayeringOperation;
import vg.lib.layout.hierarchical.step1.data.LayeringOperationResult;
import vg.lib.layout.hierarchical.step2.SetOrderForVerticesByDefaultCrossingReductionProcedure;
import vg.lib.layout.hierarchical.step2.custom.CustomCrossingReductionProcedure;
import vg.lib.layout.hierarchical.step2.legacy.LegacyCrossingReductionProcedure;
import vg.lib.layout.hierarchical.step3.legacy.ForceDirectedAlignmentProcedure;
import vg.lib.layout.hierarchical.step3.legacy.LeftAlignmentProcedure;
import vg.lib.layout.hierarchical.step3.legacy.operation.PrepareBackwardEdgesToAlignmentProcedure;
import vg.lib.layout.hierarchical.step3.legacy.operation.PrepareLongEdgesToAlignmentProcedure;
import vg.lib.operation.Operation;
import vg.lib.operation.OperationWithOneArg;
import vg.lib.operation.OperationWithTwoArgs;
import vg.lib.operation.ProcedureWithThreeArgs;

import java.awt.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class HierarchicalPlainLayout implements Operation<HierarchicalPlainLayout.Result> {
    // Input data.
    private final List<UUID> vertices;
    private final Map<UUID, Map.Entry<UUID, UUID>> edgeToNodes;
    private final Map<UUID, UUID> edgeToSrcPort;
    private final Map<UUID, UUID> edgeToTrgPort;
    private final Map<UUID, Integer> portIdToSpecifiedOrder;
    private final Map<UUID, Point> portIdToPos;
    private final Map<UUID, Point> portIdToSize;
    private final List<UUID> inputPorts;
    private final List<UUID> outputPorts;
    private final Map<UUID, String> vertexIdToName;
    private final Map<UUID, Point> vertexIdToSize;
    private final Map<UUID, Point> vertexIdToFragmentTextSize;
    private final HierarchicalLayoutSettings settings;

    private final HierarchicalGraph graph;

    HierarchicalPlainLayout(
            List<UUID> vertices,
            List<UUID> inputPorts,
            List<UUID> outputPorts,
            Map<UUID, Map.Entry<UUID, UUID>> edgeToNodes,
            Map<UUID, UUID> edgeToSrcPort,
            Map<UUID, UUID> edgeToTrgPort,
            Map<UUID, Integer> portIdToSpecifiedOrder,
            Map<UUID, Point> portIdToPos,
            Map<UUID, Point> portIdToSize,
            Map<UUID, String> vertexIdToName,
            Map<UUID, Point> vertexIdToSize,
            Map<UUID, Point> vertexIdToFragmentTextSize,
            HierarchicalLayoutSettings settings) {
        this.vertices = vertices;
        this.inputPorts = inputPorts;
        this.outputPorts = outputPorts;
        this.edgeToNodes = edgeToNodes;
        this.edgeToSrcPort = edgeToSrcPort;
        this.edgeToTrgPort = edgeToTrgPort;
        this.portIdToSpecifiedOrder = portIdToSpecifiedOrder;
        this.portIdToPos = portIdToPos;
        this.portIdToSize = portIdToSize;
        this.vertexIdToName = vertexIdToName;
        this.vertexIdToSize = vertexIdToSize;
        this.vertexIdToFragmentTextSize = vertexIdToFragmentTextSize;
        this.settings = settings;

        // check that all ports has indices.
        for (var inputPort : inputPorts) {
            if (!portIdToSpecifiedOrder.containsKey(inputPort)) {
                throw new IllegalArgumentException(String.format("Can't find index for input port with id '%s'.", inputPort));
            }
        }

        for (var outputPort : outputPorts) {
            if (!portIdToSpecifiedOrder.containsKey(outputPort)) {
                throw new IllegalArgumentException(String.format("Can't find index for output port with id '%s'.", outputPort));
            }
        }

        // check that all vertices has size.
        for (var vertex : vertices) {
            if (!vertexIdToSize.containsKey(vertex)) {
                throw new IllegalArgumentException(String.format("Can't find size for vertex with id '%s'.", vertex));
            }
        }

        graph = new HierarchicalGraph(
                new Point(20, 20),
                new Point(1, 1),
                new Point(settings.minHSpaceBetweenVertices, 0),
                !inputPorts.isEmpty(),
                !outputPorts.isEmpty());
    }

    @Override
    public Result execute() {
        long t1 = System.currentTimeMillis();

        executeStep1();

        long t2 = System.currentTimeMillis();

        executeStep2();

        long t3 = System.currentTimeMillis();

        executeStep3();

        long t4 = System.currentTimeMillis();

        log.debug(String.format("Time:\nStep 1: %sms.\nStep 2: %sms\nStep 3: %sms.", t2 - t1, t3 - t2, t4 - t3));

        return prepareResult();
    }

    private void executeStep1() {
        log.debug("Step 1: prepare vertices and edges. Set level for each vertex.");

        prepareVertices();
        prepareEdges();

        graph.build(vertices);

        graph.printHierarchy("Print result after step 1.");
    }

    private void executeStep2() {
        log.debug("Step 2: crossing reduction step.");

        switch (settings.crossingReductionAlgorithm) {
            case HierarchicalLayoutSettings.CROSSING_REDUCTION_WITH_DEFAULT_ORDER_ALGORITHM:
                new SetOrderForVerticesByDefaultCrossingReductionProcedure(graph).execute();
                break;
            case HierarchicalLayoutSettings.GENERAL_CROSSING_REDUCTION_ALGORITHM:
            case HierarchicalLayoutSettings.LEGACY_CROSSING_REDUCTION_ALGORITHM:
                new LegacyCrossingReductionProcedure(graph).execute();
                break;
            case HierarchicalLayoutSettings.CUSTOM_CROSSING_REDUCTION_ALGORITHM:
                new CustomCrossingReductionProcedure(graph).execute();
                break;
        }

        // update portIdToIndex argument.
        Stream.concat(graph.getInputPorts().stream(), graph.getOutputPorts().stream()).forEach(
            port -> portIdToSpecifiedOrder.put(port.getVertexId(), port.getOrder())
        );

        graph.printHierarchy("Print result after step 2.");
    }

    private void executeStep3() {
        log.debug("Step 3: align the vertices and ports of the graph.");

        var startTime = System.currentTimeMillis();

        new PrepareBackwardEdgesToAlignmentProcedure(graph).execute();
        new PrepareLongEdgesToAlignmentProcedure(graph).execute();

        switch (settings.alignment) {
            case HierarchicalLayoutSettings.LEGACY_ALIGNMENT_ALGORITHM:
                new ForceDirectedAlignmentProcedure(settings.collapseEdgeSrc, settings.collapseEdgeTrg, graph).execute();
                break;
            case HierarchicalLayoutSettings.GENERAL_ALIGNMENT_ALGORITHM:
            default:
                new LeftAlignmentProcedure(graph).execute();
                break;
        }

        var finishTime = System.currentTimeMillis();

        graph.printResultData(String.format("Print result after step 3 (%s ms).", finishTime - startTime));
    }

    private Result prepareResult() {
        log.debug("Prepare results of the algorithm.");

        // prepare the result.
        var result = new Result();

        result.setCrosses(graph.crosses);

        result.setContainsOutputPorts(!outputPorts.isEmpty());

        // calculate row height and update graph width.
        var calcMaxRowHeightFunc = new OperationWithOneArg<List<HierarchicalVertex>, Integer>() {
            @Override
            public Integer execute(List<HierarchicalVertex> vertices) {
                // obtain max height of the row via the row's vertices.
                int maxRowHeight = 0;
                for (var vertex : vertices) {
                    // TODO: достаточно спорно делать это здесь, возможно стоит вынести в отдельный блок кода.
                    if (result.getGraphSize().x < vertex.getPosX() + vertex.getWidth()) {
                        result.getGraphSize().x = vertex.getPosX() + vertex.getWidth();
                    }
                    if (maxRowHeight < vertex.getHeight()) {
                        maxRowHeight = vertex.getHeight();
                    }
                }

                return maxRowHeight;
            }
        };

        var calcSpaceBetweenRowsFunc = new OperationWithTwoArgs<List<HierarchicalEdge>, Integer, Integer>() {
            @Override
            public Integer execute(List<HierarchicalEdge> edges, Integer minVSpaceBetweenRows) {
                // obtain max edge's width.
                int maxEdgeWidth = 0;
                boolean allEdgesAreFake = true;
                boolean someEdgeIsBackward = false;
                boolean nextRowIsFake = true;
                boolean prevRowIsFake = true;
                for (var edge : edges) {
                    if (edge.getSrcVertex().isRealVertex()) {
                        prevRowIsFake = false;
                    }

                    if (edge.getTrgVertex().isRealVertex()) {
                        nextRowIsFake = false;
                    }

                    if (!edge.getSrcVertex().isFakeVertex()) {
                        allEdgesAreFake = false;
                    }

                    if (edge.isBackEdge()) {
                        someEdgeIsBackward = true;
                        continue;
                    }

                    var edgeWidth = Math.abs(edge.getSrcVertex().getPosX() + edge.getSrcPosX() - (edge.getTrgVertex().getPosX() + edge.getTrgPosX()));
                    if (maxEdgeWidth < edgeWidth) {
                        maxEdgeWidth = edgeWidth;
                    }
                }

                if (someEdgeIsBackward && allEdgesAreFake && nextRowIsFake && prevRowIsFake) {
                    minVSpaceBetweenRows = Math.round(minVSpaceBetweenRows * 0.15f);
                }

                if (allEdgesAreFake && !nextRowIsFake && prevRowIsFake) {
                    minVSpaceBetweenRows = Math.round(minVSpaceBetweenRows * 0.75f);
                }

                if (allEdgesAreFake && !prevRowIsFake && nextRowIsFake) {
                    minVSpaceBetweenRows = Math.round(minVSpaceBetweenRows * 0.75f);
                }

                int maxEdgeWidthExt = Math.round(maxEdgeWidth * 0.35f);
                return Math.max(maxEdgeWidthExt, minVSpaceBetweenRows);
            }
        };

        var convertToAdditionalPointFunc = new Function<HierarchicalVertex, Result.OutputEdge.AdditionalPoint>() {
            @Override
            public Result.OutputEdge.AdditionalPoint apply(HierarchicalVertex vertex) {
                return new Result.OutputEdge.AdditionalPoint(
                        vertex.getRowPosY(),
                        new Point(vertex.getPosX(), vertex.getPosY()),
                        new Point(vertex.getWidth(), vertex.getHeight()));
            }
        };

        var convertToAdditionalPointsFunc = new OperationWithOneArg<List<HierarchicalVertex>, List<Result.OutputEdge.AdditionalPoint>>() {
            @Override
            public List<Result.OutputEdge.AdditionalPoint> execute(List<HierarchicalVertex> vertices) {
                return vertices.stream()
                        .map(convertToAdditionalPointFunc)
                        .collect(Collectors.toList());
            }
        };

        // update vertical coordinate for the row's vertices.
        var setPosYProc = new ProcedureWithThreeArgs<List<HierarchicalVertex>, Integer, Integer>() {
            @Override
            public void execute(List<HierarchicalVertex> vertices, Integer rowPosY, Integer maxRowHeight) {
                for (var vertex : vertices) {
                    vertex.setPosY(rowPosY + maxRowHeight - vertex.getHeight());
                    vertex.setRowPosY(rowPosY);
                }
            }
        };

        int maxInputPortHeight = 0, maxOutputPortHeight;
        int spaceBetweenInputPorts = settings.minVSpaceBetweenPorts, spaceBetweenOutputPorts = settings.minVSpaceBetweenPorts;
        if (graph.hasInputPorts) {
            var outputEdges = graph.getOutputEdges(graph.getInputPorts());
            maxInputPortHeight = calcMaxRowHeightFunc.execute(graph.getInputPorts());
            spaceBetweenInputPorts = calcSpaceBetweenRowsFunc.execute(outputEdges, outputEdges.size() > 0 ? settings.minVSpaceBetweenRows : settings.minVSpaceBetweenPorts);
            setPosYProc.execute(graph.getInputPorts(), 0, maxInputPortHeight);
        }

        for (var detailedGroup : graph.getDetailedGroups().entrySet()) {
            int groupSizeY = maxInputPortHeight + spaceBetweenInputPorts;
            int count = 0;
            for (var detailedGroupEntry : detailedGroup.getValue().entrySet()) {
                var row = detailedGroupEntry.getValue().stream().sorted().collect(Collectors.toList());

                int maxRowHeight = calcMaxRowHeightFunc.execute(row);

                setPosYProc.execute(row, groupSizeY, maxRowHeight);

                groupSizeY += maxRowHeight;

                if (count != detailedGroup.getValue().size() - 1) {
                    int spaceBetweenRows = calcSpaceBetweenRowsFunc.execute(graph.getOutputEdgesSortedByOutputVertices(row), settings.minVSpaceBetweenRows);
                    groupSizeY += spaceBetweenRows;
                }
                count++;
            }

            result.getGraphSize().y = Math.max(result.getGraphSize().y, groupSizeY);
        }

        if (graph.hasOutputPorts) {
            var inputEdges = graph.getInputEdges(graph.getOutputPorts());
            maxOutputPortHeight = calcMaxRowHeightFunc.execute(graph.getOutputPorts());
            spaceBetweenOutputPorts = calcSpaceBetweenRowsFunc.execute(inputEdges, inputEdges.size() > 0 ? settings.minVSpaceBetweenRows : settings.minVSpaceBetweenPorts);
            result.getGraphSize().y += spaceBetweenOutputPorts;
            setPosYProc.execute(graph.getOutputPorts(), result.getGraphSize().y, maxOutputPortHeight);
            result.getGraphSize().y += maxOutputPortHeight;
        } else {
            result.getGraphSize().y += spaceBetweenOutputPorts;
        }

        graph.getEdges().values().forEach(edge -> {
            if (edge.isFake()) {
                return;
            }

            var edgeId = edge.getId();

            var outputEdge = new Result.OutputEdge(edgeId);

            outputEdge.setSrcId(edge.getSrcVertex().getVertexId());
            outputEdge.setTrgId(edge.getTrgVertex().getVertexId());
            outputEdge.setSrcPosX(edge.getSrcPosX());
            outputEdge.setTrgPosX(edge.getTrgPosX());

            result.getEdges().put(edge.getId(), outputEdge);
        });

        graph.getLongEdges().values().forEach(longEdge -> {
            if (longEdge.isPartOfBackwardEdge()) {
                return;
            }

            var edgeId = longEdge.getOriginalEdgeId();

            var outputEdge = new Result.OutputEdge(edgeId);

            outputEdge.setSrcId(longEdge.getSrcVertex().getVertexId());
            outputEdge.setTrgId(longEdge.getTrgVertex().getVertexId());
            outputEdge.setSrcPosX(longEdge.getFirstEdge().getSrcPosX());
            outputEdge.setTrgPosX(longEdge.getLastEdge().getTrgPosX());

            outputEdge.additionalPoints = convertToAdditionalPointsFunc.execute(longEdge.getFakeVertices());

            result.getEdges().put(edgeId, outputEdge);
        });

        graph.getBackwardEdges().values().forEach(backwardEdge -> {
            var edgeId = backwardEdge.getOriginalEdgeId();

            var outputEdge = new Result.OutputEdge(edgeId);
            outputEdge.setBackward(true);

            var topLongEdge = graph.getLongEdges().get(backwardEdge.getTopEdgeId());
            var bottomLongEdge = graph.getLongEdges().get(backwardEdge.getBottomEdgeId());
            var edgeBetweenCompanions = graph.getLongEdges().get(backwardEdge.getEdgeBetweenCompanionsId());

            if (topLongEdge != null && bottomLongEdge != null) {
                outputEdge.setSrcId(bottomLongEdge.getSrcVertex().getVertexId());
                outputEdge.setTrgId(topLongEdge.getTrgVertex().getVertexId());
                outputEdge.setSrcPosX(bottomLongEdge.getFirstEdge().getSrcPosX());
                outputEdge.setTrgPosX(topLongEdge.getLastEdge().getTrgPosX());
                outputEdge.topAdditionalPoints = convertToAdditionalPointsFunc.execute(topLongEdge.getFakeVertices());
                outputEdge.bottomAdditionalPoints = convertToAdditionalPointsFunc.execute(bottomLongEdge.getFakeVertices());
                outputEdge.additionalPoints = convertToAdditionalPointsFunc.execute(edgeBetweenCompanions.getFakeVertices());
            } else {
                var topEdge = graph.getEdgeById(backwardEdge.getTopEdgeId());
                var bottomEdge = graph.getEdgeById(backwardEdge.getBottomEdgeId());
                outputEdge.setSrcId(bottomEdge.getSrcVertex().getVertexId());
                outputEdge.setTrgId(topEdge.getTrgVertex().getVertexId());
                outputEdge.setSrcPosX(bottomEdge.getSrcPosX());
                outputEdge.setTrgPosX(topEdge.getTrgPosX());
                outputEdge.additionalPoints = convertToAdditionalPointsFunc.execute(edgeBetweenCompanions.getFakeVertices());
            }

            outputEdge.setSrcCompanion(convertToAdditionalPointFunc.apply(backwardEdge.getSrcCompanionVertex()));
            outputEdge.setTrgCompanion(convertToAdditionalPointFunc.apply(backwardEdge.getTrgCompanionVertex()));

            result.getEdges().put(edgeId, outputEdge);
        });

        // handle vertices.
        for (int rowIndex = 0; rowIndex < graph.getRowSize(); rowIndex++) {
            var row = graph.getRowByLevel(rowIndex);

            // TODO: need place the code in phase 2...
            // check that all elements has order.
            row.forEach(x -> Validate.isTrue(x.getOrder() >= 0, String.format("Vertex '%s' hasn't order.", x.getName())));

            for (HierarchicalVertex vertex : row) {
                if (!vertex.isRealVertex() && !vertex.isPort()) {
                    continue;
                }

                result.getVertices().put(
                        vertex.getVertexId(),
                        new Result.OutputVertex(
                                vertex.getVertexId(),
                                vertex.getLevel(),
                                vertex.getOrder(),
                                vertex.getRowPosY(),
                                new Point(vertex.getPosX() + vertex.getBorderSize().x, vertex.getPosY()),
                                new Point(vertex.getSize())));
            }
        }

        Validate.isTrue(
                result.getVertices().size() == vertices.size(),
                "Amount of vertices in result is not equal with input.");
        Validate.isTrue(
                result.getEdges().size() == edgeToNodes.size(),
                "Amount of edges in result is not equal with input.");

        return result;
    }

    private void prepareVertices() {
        log.debug("Prepare vertices...");

        LayeringOperationResult result;
        switch (settings.layeringAlgorithm) {
            case HierarchicalLayoutSettings.CUSTOM_LAYERING_ALGORITHM:
                result = new CustomLayeringOperation(
                    vertices, inputPorts, outputPorts, edgeToNodes, portIdToSpecifiedOrder)
                    .execute();
                break;
            case HierarchicalLayoutSettings.LEGACY_LAYERING_ALGORITHM:
            default:
                result = new FineTuningLayeringOperation(vertices, inputPorts, outputPorts, edgeToNodes)
                    .execute();
                break;
        }

        for (int vertexIndex = 0; vertexIndex < result.numberOfVertices; vertexIndex++) {
            var vertexId = result.vertices[vertexIndex];
            var groupId = result.vertexIndexToGroupId[vertexIndex];
            var layer = result.vertexIndexToLayer[vertexIndex];

            if (result.inputPorts[vertexIndex]) {
                graph.addInputPort(
                    vertexId,
                    portIdToSpecifiedOrder.getOrDefault(vertexId, -1),
                    vertexIdToName.get(vertexId),
                    groupId,
                    vertexIdToSize.get(vertexId));
            } else if (result.outputPorts[vertexIndex]) {
                graph.addOutputPort(
                    vertexId,
                    portIdToSpecifiedOrder.getOrDefault(vertexId, -1),
                    vertexIdToName.get(vertexId),
                    layer,
                    groupId,
                    vertexIdToSize.get(vertexId));
            } else {
                graph.addVertex(
                    vertexId,
                    VertexType.VERTEX_TYPE,
                    vertexIdToName.get(vertexId),
                    layer,
                    groupId,
                    vertexIdToSize.get(vertexId),
                    vertexIdToFragmentTextSize.get(vertexId));
            }
        }

        graph.reCalculateRoots();

        log.debug("Vertices were prepared...");
    }

    private void prepareEdges() {
        log.debug("Prepare the edges...");

        // add edges.
        for (Map.Entry<UUID, Map.Entry<UUID, UUID>> edgeToNodesEntry : edgeToNodes.entrySet()) {
            UUID edgeId = edgeToNodesEntry.getKey();
            UUID srcVertexId = edgeToNodesEntry.getValue().getKey();
            UUID trgVertexId = edgeToNodesEntry.getValue().getValue();

            var srcVertex = graph.getVertexById(srcVertexId);
            var trgVertex = graph.getVertexById(trgVertexId);

            var srcPortId = edgeToSrcPort.get(edgeId);
            var trgPortId = edgeToTrgPort.get(edgeId);

            graph.addEdge(
                srcVertex,
                trgVertex,
                edgeId,
                srcPortId,
                trgPortId,
                portIdToSpecifiedOrder.getOrDefault(srcPortId, -1),
                portIdToSpecifiedOrder.getOrDefault(trgPortId, -1),
                portIdToPos.getOrDefault(srcPortId, new Point(-1, -1)).x,
                portIdToPos.getOrDefault(trgPortId, new Point(-1, -1)).x,
                portIdToSize.getOrDefault(srcPortId, new Point(0, 0)).x,
                portIdToSize.getOrDefault(trgPortId, new Point(0, 0)).x);
        }

        log.debug("The edges were prepared...");
    }

    public static class Result {
        @Getter
        private final Map<UUID, OutputEdge> edges = new LinkedHashMap<>();

        @Getter
        private final Map<UUID, OutputVertex> vertices = new LinkedHashMap<>();

        @Getter @Setter
        private boolean containsOutputPorts;

        @Getter @Setter
        private Point graphSize = new Point();

        @Getter @Setter
        private int crosses;

        public static class OutputVertex implements Comparable<OutputVertex> {
            @Getter
            private final UUID vertexId;

            @Getter @Setter
            private int level;

            @Getter @Setter
            private int order;

            @Getter @Setter
            private int rowPosY;

            @Getter @Setter
            private Point pos;

            @Getter @Setter
            private Point size;

            OutputVertex(UUID vertexId, int level, int order, int rowPosY, Point pos, Point size) {
                this.vertexId = vertexId;
                this.level = level;
                this.order = order;
                this.rowPosY = rowPosY;
                this.pos = pos;
                this.size = size;
            }

            @Override
            public int compareTo(OutputVertex o) {
                return Integer.compare(order, o.getOrder());
            }
        }

        public static class OutputEdge {
            @Getter
            private final UUID edgeId;

            @Getter @Setter
            private UUID srcId;

            @Getter @Setter
            private UUID trgId;

            @Getter @Setter
            private boolean backward;

            @Getter @Setter
            private AdditionalPoint srcCompanion;

            @Getter @Setter
            private AdditionalPoint trgCompanion;

            @Getter @Setter
            private List<AdditionalPoint> additionalPoints = new ArrayList<>();

            @Getter @Setter
            private List<AdditionalPoint> topAdditionalPoints = new ArrayList<>();

            @Getter @Setter
            private List<AdditionalPoint> bottomAdditionalPoints = new ArrayList<>();

            @Getter @Setter
            private int srcPosX;

            @Getter @Setter
            private int trgPosX;

            OutputEdge(UUID edgeId) {
                this.edgeId = edgeId;
            }

            public int getTopPointX() {
                return topAdditionalPoints.get(0).pos.x;
            }

            public int getTopAdditionalPointX() {
                return additionalPoints.get(0).pos.x;
            }

            public int getBottomPointX() {
                return bottomAdditionalPoints.get(bottomAdditionalPoints.size() - 1).pos.x;
            }

            public int getBottomAdditionalPointX() {
                return additionalPoints.get(additionalPoints.size() - 1).pos.x;
            }

            public static class AdditionalPoint {
                @Getter @Setter
                private int rowPosY;

                @Getter @Setter
                private Point pos;

                @Getter @Setter
                private Point size;

                public AdditionalPoint(int rowPosY, Point pos, Point size) {
                    this.rowPosY = rowPosY;
                    this.pos = pos;
                    this.size = size;
                }

                public float centerX() {
                    return pos.x + size.x / 2.0f;
                }

                public int roundCenterX() {
                    return Math.round(centerX());
                }

                public int xLeft() {
                    return pos.x;
                }

                public int xRight() {
                    return pos.x + size.x;
                }

                public int roundCenterY() {
                    return pos.y + size.y / 2;
                }

                public int yTop() {
                    return pos.y;
                }

                public int yBottom() {
                    return pos.y + size.y;
                }
            }
        }
    }
}
