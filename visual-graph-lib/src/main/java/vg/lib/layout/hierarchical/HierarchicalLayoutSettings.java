package vg.lib.layout.hierarchical;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HierarchicalLayoutSettings {
    public static final int LEGACY_LAYERING_ALGORITHM = 1;
    public static final int CUSTOM_LAYERING_ALGORITHM = 2;

    public static final int GENERAL_CROSSING_REDUCTION_ALGORITHM = 0;
    public static final int LEGACY_CROSSING_REDUCTION_ALGORITHM = 1;
    public static final int CROSSING_REDUCTION_WITH_DEFAULT_ORDER_ALGORITHM = 3;
    public static final int CUSTOM_CROSSING_REDUCTION_ALGORITHM = 4;

    public static final int LEFT_ALIGNMENT_ALGORITHM = 1;
    public static final int GENERAL_ALIGNMENT_ALGORITHM = 3;
    public static final int LEGACY_ALIGNMENT_ALGORITHM = 4;

    public static final int POLYLINE_ROUTING_STYLE = 0;
    public static final int ORTHOGONAL_ROUTING_STYLE = 1;
    public static final int SPLINE_ROUTING_STYLE = 2;

    public int minVSpaceBetweenPorts;

    public int minVSpaceBetweenRows;

    public int minHSpaceBetweenVertices;

    public int layeringAlgorithm;

    public int crossingReductionAlgorithm;

    public int alignment;

    public boolean collapseEdgeSrc;

    public boolean collapseEdgeTrg;

    public int routingStyle;

    public int fragmentShapeStyle;

    public int defaultShapeStyle;

    public int baseArrow;

    public int arrowSize;

    public int freeSpaceY;
}
