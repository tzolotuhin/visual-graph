package vg.lib.layout.hierarchical.step1;

import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.VertexType;
import vg.lib.operation.Procedure;

import java.util.ArrayList;

public class InsertBeaconsProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public InsertBeaconsProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        var topEdgeIds = new ArrayList<>();
        for (var backwardEdge : graph.getBackwardEdges().values()) {
            topEdgeIds.add(backwardEdge.getTopEdgeId());
        }
        var bottomEdgeIds = new ArrayList<>();
        for (var backwardEdge : graph.getBackwardEdges().values()) {
            bottomEdgeIds.add(backwardEdge.getBottomEdgeId());
        }

        for (var backwardEdge : graph.getBackwardEdges().values()) {
            var srcVertex = backwardEdge.getSrcVertex();
            if (bottomEdgeIds.containsAll(srcVertex.getOutputEdges())) {
                var beacon = graph.addFakeVertex(
                        backwardEdge.getSrcCompanionVertex().getLevel(),
                        srcVertex.getGroupId(),
                        String.format("BB: %s", srcVertex.getName()));
                beacon.setType(VertexType.BOTTOM_BEACON_VERTEX_TYPE);
                graph.addFakeEdge(srcVertex, beacon);
            }
            var trgVertex = backwardEdge.getTrgVertex();
            if (topEdgeIds.containsAll(trgVertex.getInputEdges())) {
                var beacon = graph.addFakeVertex(
                        backwardEdge.getTrgCompanionVertex().getLevel(),
                        trgVertex.getGroupId(),
                        String.format("TB: %s", trgVertex.getName()));
                beacon.setType(VertexType.TOP_BEACON_VERTEX_TYPE);
                graph.addFakeEdge(beacon, trgVertex);
            }
        }

        graph.reCalculateRoots();
    }
}
