package vg.lib.layout.hierarchical.step2;

import lombok.extern.slf4j.Slf4j;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.operation.Procedure;

import java.util.List;
import java.util.Map;

@Slf4j
public class SetOrderForVerticesByDefaultCrossingReductionProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public SetOrderForVerticesByDefaultCrossingReductionProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        log.debug("Set order for vertices by default in the crossing reduction operation...");

        // reset order for all elements.
        graph.getVertices().forEach(x -> x.setOrder(-1));

        // setup initial orders for the input ports.
        HierarchicalVertex.setupDefaultOrderForElements(graph.getInputPorts());

        // setup initial orders for the output ports.
        HierarchicalVertex.setupDefaultOrderForElements(graph.getOutputPorts());

        // setup initial orders for the roots.
        HierarchicalVertex.setupDefaultOrderForElements(graph.getFirstRow());

        // setup initial orders for the vertices.
        int prevGroupOrder = 0;
        var detailedGroups = graph.getDetailedGroups();

        log.debug("Amount of detailed groups '{}'.", detailedGroups.size());
        for (var detailedGroupEntry : detailedGroups.entrySet()) {
            log.debug("Handle detailed group '{}'.", detailedGroupEntry.getKey());

            Map.Entry<Integer, List<HierarchicalVertex>> parentEntry = null;
            for (var levelEntry : detailedGroupEntry.getValue().entrySet()) {
                log.debug("Handle level '{}' of detailed group '{}'.", levelEntry.getKey(), detailedGroupEntry.getKey());

                if (parentEntry != null) {
                    HierarchicalVertex.setupDefaultOrderForElements(levelEntry.getValue());
                } else {
                    for (int i = 0; i < levelEntry.getValue().size(); i++) {
                        levelEntry.getValue().get(i).setOrder(i);
                    }
                }

                parentEntry = levelEntry;
            }

            prevGroupOrder = HierarchicalGraph.shiftOrderForDetailedGroup(prevGroupOrder, detailedGroupEntry.getValue()) + 1;
        }

        log.debug("Crossing reduction operation was finished.");
    }
}
