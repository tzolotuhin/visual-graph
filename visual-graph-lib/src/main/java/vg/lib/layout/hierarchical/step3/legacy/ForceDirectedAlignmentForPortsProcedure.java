package vg.lib.layout.hierarchical.step3.legacy;

import lombok.extern.slf4j.Slf4j;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.layout.hierarchical.step3.legacy.data.CoordinateAssignmentTable;
import vg.lib.layout.hierarchical.step3.legacy.operation.AlignRowProcedure;
import vg.lib.layout.hierarchical.step3.legacy.operation.CalcLengthOperation;
import vg.lib.operation.Procedure;

import java.util.List;
import java.util.TreeMap;

@Slf4j
public class ForceDirectedAlignmentForPortsProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public ForceDirectedAlignmentForPortsProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        log.debug("Use force directed alignment for the input and output ports...");

        new LeftAlignmentForPortsProcedure(graph).execute();

        TreeMap<Integer, List<HierarchicalVertex>> group = new TreeMap<>();
        for (int levelIndex = graph.getStartLevelOfGraphWithInputPorts(); levelIndex <= graph.getFinishLevelOfGraphWithOutputPorts(); levelIndex++) {
            group.put(levelIndex, graph.getRowByLevel(levelIndex));
        }

        var table = new CoordinateAssignmentTable(group, graph, false);
        table.applyCoordinates();

        int inputPortsWidth = 0;
        if (graph.hasInputPorts) {
            inputPortsWidth = table.rows[0].calcWidth();
        }

        int outputPortsWidth = 0;
        if (graph.hasOutputPorts) {
            outputPortsWidth = table.rows[table.rows.length - 1].calcWidth();
        }

        var firstRow = graph.getFirstRow();
        var lastRow = graph.getLastRow();

        var inputEdges = graph.getInputEdges(graph.getFirstRow());
        var outputEdges = graph.getOutputEdges(graph.getLastRow());

        int maxWidth = Math.max(inputPortsWidth, outputPortsWidth);

        if (graph.hasInputPorts && !firstRow.isEmpty()) {
            if (inputEdges.isEmpty()) {
                table.rows[0].resetCoordinates((maxWidth - inputPortsWidth) / 2);
            } else {
                new AlignRowProcedure(
                        table.rows[0],
                        new CalcLengthOperation(table.edges[0], Direction.BOTH_AND_TOP, false)).execute();
            }
        }

        if (graph.hasOutputPorts && !lastRow.isEmpty()) {
            if (outputEdges.isEmpty()) {
                table.rows[table.rows.length - 1].resetCoordinates((maxWidth - outputPortsWidth)/ 2);
            } else {
                new AlignRowProcedure(
                        table.rows[table.rows.length - 1],
                        new CalcLengthOperation(table.edges[table.rows.length - 2], Direction.BOTH_AND_BOTTOM, false)).execute();
            }
        }

        table.setupCoordinates();
    }
}
