package vg.lib.layout.hierarchical.step2.legacy;

import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.step2.legacy.data.Row;
import vg.lib.operation.Operation;
import vg.lib.operation.Procedure;

public class CrossingReductionRowProcedure implements Procedure {
    private final Row row;
    private final Direction direction;
    private final Operation<CalcCrossesOperation.Crosses> calcCrossesOperation;
    private final Operation<Long> sameTypeOperation;
    private final Operation<Long> proximityOfVerticesOperation;
    private final Operation<Long> estCrossesOperation;
    private final int[] vector;
    public long w;

    public CrossingReductionRowProcedure(Row row,
                                         Direction direction,
                                         Operation<CalcCrossesOperation.Crosses> calcCrossesOperation,
                                         Operation<Long> estCrossesOperation,
                                         Operation<Long> proximityOfVerticesOperation,
                                         Operation<Long> sameTypeOperation,
                                         int[] vector) {
        this.row = row;
        this.direction = direction;
        this.calcCrossesOperation = calcCrossesOperation;
        this.estCrossesOperation = estCrossesOperation;
        this.proximityOfVerticesOperation = proximityOfVerticesOperation;
        this.sameTypeOperation = sameTypeOperation;
        this.vector = vector;
    }

    @Override
    public void execute() {
        long bestCrosses = calcCrosses();

        long re;
        while (true) {
            row.longBackup();

            re = doExecute();

            if (re < bestCrosses) {
                bestCrosses = re;
                row.longBackup();
            } else {
                break;
            }
        }

        row.longRollback();

        w = calcCrosses();

//        if (MainService.logger.isDebugMode()) {
//            MainService.logger.printDebug("\nStart=============================================>");
//
//            var forPrinting = new StringBuilder();
//            forPrinting
//                    .append("\nRow = ").append(row.rowIndex)
//                    .append(", Direction = ").append(direction)
//                    .append(", crosses = ").append(calcCrossesOperation.execute().w)
//                    .append(", est = ").append(estCrossesOperation.execute()).append("\n");
//            for (var v : row.verticesRef) {
//                forPrinting.append(v.vertex.getName()).append("  --  ").append(v.getOrder()).append("\n");
//            }
//            forPrinting.append("\n");
//            MainService.logger.printDebug(forPrinting.toString());
//            estCrossesOperation.execute();
//        }
    }

    // TODO: ак-то следует скрестить два метода, т.к. обычный метод это группы с одним элементом.
    public long doExecute() {
        row.resetVisited();

        long bestCrossesResult = calcCrosses();
        while (true) {
            long currentCrossesResult = calcCrosses();

            // choose the best suitable vertex.
            var suitableVertex = row.takeSuitableVertex(direction);
            if (suitableVertex == null) {
                if (currentCrossesResult < bestCrossesResult) {
                    bestCrossesResult = currentCrossesResult;
                    row.resetVisited();
                    continue;
                }

                break;
            }

            // left swap
            row.backup();
            while (true) {
                if (!row.leftSwap(suitableVertex)) {
                    break;
                }

                long re = calcCrosses();

                if (re < currentCrossesResult) {
                    currentCrossesResult = re;
                    row.backup();
                }

                if (currentCrossesResult == 0) {
                    break;
                }
            }
            row.rollback();

            // right swap
            row.backup();
            while (true) {
                if (!row.rightSwap(suitableVertex)) {
                    break;
                }

                long re = calcCrosses();

                if (re < currentCrossesResult) {
                    currentCrossesResult = re;
                    row.backup();
                }

                if (currentCrossesResult == 0) {
                    break;
                }
            }
            row.rollback();
        }

        return calcCrosses();
    }

    private long calcCrosses() {
        return vector[0] * calcCrossesOperation.execute().w
                + vector[1] * estCrossesOperation.execute()
                + vector[2] * proximityOfVerticesOperation.execute()
                + vector[3] * sameTypeOperation.execute();
    }
}
