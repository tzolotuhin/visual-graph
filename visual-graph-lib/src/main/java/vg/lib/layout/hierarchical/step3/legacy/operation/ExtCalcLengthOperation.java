package vg.lib.layout.hierarchical.step3.legacy.operation;

import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.step3.legacy.data.CoordinateAssignmentTable;
import vg.lib.operation.Operation;

public class ExtCalcLengthOperation implements Operation<Long> {
    private final Direction topOrBottomDirection;
    private final Direction direction;
    private final boolean ignoreBackwardEdges;

    private final int currRowIndex;
    private final CoordinateAssignmentTable table;

    public ExtCalcLengthOperation(int currRowIndex,
                                  CoordinateAssignmentTable table,
                                  Direction topOrBottomDirection,
                                  Direction direction,
                                  boolean ignoreBackwardEdges) {
        this.currRowIndex = currRowIndex;
        this.table = table;
        this.topOrBottomDirection = topOrBottomDirection;
        this.direction = direction;
        this.ignoreBackwardEdges = ignoreBackwardEdges;
    }

    @Override
    public Long execute() {
        long result = 0;

        int importance = 5;
        if (topOrBottomDirection == Direction.BOTTOM) {
            for (int rowIndex = 0; rowIndex < currRowIndex - 1; rowIndex++) {
                result += importance * new CalcLengthOperation(table.edges[rowIndex], direction, ignoreBackwardEdges).execute();
                importance += 5;
            }
            if (currRowIndex - 1 >= 0) {
                result += importance * new CalcLengthOperation(table.edges[currRowIndex - 1], direction, ignoreBackwardEdges).execute();
            }
        } else {
            if (currRowIndex < table.vertices.length - 1) {
                result += importance * new CalcLengthOperation(table.edges[currRowIndex], direction, ignoreBackwardEdges).execute();
            }
            for (int rowIndex = table.vertices.length - 2; rowIndex >= currRowIndex + 1; rowIndex--) {
                result += importance * new CalcLengthOperation(table.edges[rowIndex], direction, ignoreBackwardEdges).execute();
                importance += 5;
            }
        }

        return result;
    }
}
