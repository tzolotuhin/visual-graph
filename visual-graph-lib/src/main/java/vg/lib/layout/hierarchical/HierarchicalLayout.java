package vg.lib.layout.hierarchical;

import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.mutable.MutableInt;
import vg.lib.common.VGUtils;
import vg.lib.layout.hierarchical.graphics.BackwardEdgeHandler;
import vg.lib.layout.hierarchical.graphics.ForwardEdgeHandler;
import vg.lib.operation.Operation;
import vg.lib.view.GraphView;
import vg.lib.view.GraphViewUtils;
import vg.lib.view.elements.GVParentWithElements;
import vg.lib.view.elements.GVVertex;
import vg.lib.view.shapes.vshape.GraphViewVertexShape;

import java.awt.*;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class HierarchicalLayout implements Operation<Integer> {
    private final GraphView graphView;
    private final HierarchicalLayoutSettings settings;

    public HierarchicalLayout(GraphView graphView, HierarchicalLayoutSettings settings) {
        this.graphView = graphView;
        this.settings = settings;
    }

    @Override
    public Integer execute() {
        Map<UUID, Integer> portIdToIndex = Maps.newHashMap();
        Map<UUID, Point> portIdToPos =  Maps.newHashMap();
        Map<UUID, Point> portIdToSize =  Maps.newHashMap();

        MutableInt crosses = new MutableInt();
        graphView.dfs(new GraphView.GraphViewHandler() {
            @Override
            public void onParentWithElements(GVParentWithElements parentWithElements) {
                // fill map: port id -> index.
                int inputPortIndex = 0, outputPortIndex = 0;
                for (var vertex : parentWithElements.getVertices()) {
                    if (VGUtils.isPort(vertex)) {
                        if (VGUtils.isFakePort(vertex)) {
                            portIdToIndex.put(vertex.getId(), -1);
                        } else if (VGUtils.isInputPort(vertex)) {
                            portIdToIndex.put(vertex.getId(), inputPortIndex++);
                        } else {
                            portIdToIndex.put(vertex.getId(), outputPortIndex++);
                        }
                    }
                }

                // execute plain layout.
                var re = prepareDataAndExecuteHierarchicalPlainLayout(
                        graphView, parentWithElements, portIdToIndex, portIdToPos, portIdToSize);

                crosses.add(re);

                // fill maps: port id -> position, size.
                parentWithElements.getVertices().forEach(x -> {
                    if (VGUtils.isPort(x)) {
                        portIdToPos.put(x.getId(), x.getShape().getPosition());
                        portIdToSize.put(x.getId(), x.getShape().getSize());
                    }
                });
            }
        });

        return crosses.intValue();
    }

    int prepareDataAndExecuteHierarchicalPlainLayout(GraphView graphView,
                                                     GVParentWithElements parentWithElements,
                                                     Map<UUID, Integer> portIdToIndex,
                                                     Map<UUID, Point> portIdToPos,
                                                     Map<UUID, Point> portIdToSize) {
        var vertices = parentWithElements.getVertices();
        var edges = parentWithElements.getEdges();
        var parent = parentWithElements.getParent();

        log.debug("Prepare data and execute hierarchical layout, number of vertices {}, number of edges {}.",
                vertices.size(), edges.size());

        GVVertex fakeVertexForVertexWithPorts = null;

        var vertexIds = new ArrayList<UUID>();
        var vertexIdToGVVertex = new HashMap<UUID, GVVertex>();
        var inputPorts = new ArrayList<UUID>();
        var outputPorts = new ArrayList<UUID>();
        var edgeToNodes = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
        var edgeToSrcPort = new LinkedHashMap<UUID, UUID>();
        var edgeToTrgPort = new LinkedHashMap<UUID, UUID>();
        var vertexIdToName = new LinkedHashMap<UUID, String>();
        var vertexIdToSize = new LinkedHashMap<UUID, Point>();
        var vertexIdToFragmentTextSize = new LinkedHashMap<UUID, Point>();

        // if need add fake vertex with size of a text of the parent.
        if (parent != null && parent.isVertexWithPorts()) {
            // insert fake vertex.
            fakeVertexForVertexWithPorts = graphView.addVertex(new GVVertex(
                    parent.getId(),
                    new Point(0, 0),
                    parent.getShape().getLabel().getSize(),
                    null
            ));
            fakeVertexForVertexWithPorts.getShape().setSize(new Point(parent.getShape().getLabel().getSize()));
            vertices.add(fakeVertexForVertexWithPorts);
        }

        // prepare the edges.
        for (var vertex : vertices) {
            vertexIds.add(vertex.getId());
            vertexIdToName.put(vertex.getId(), vertex.getName());

            vertexIdToSize.put(vertex.getId(), new Point(vertex.getShape().getSize()));

            if (vertex.isFragment() && settings.fragmentShapeStyle == GraphViewVertexShape.LEFT_FRAGMENT_VERTEX_SHAPE) {
                vertexIdToFragmentTextSize.put(
                        vertex.getId(),
                        new Point(
                                vertex.getShape().getLabel().getSize().x + vertex.getShape().getBorder().x,
                                vertex.getShape().getLabel().getSize().y));
            }

            vertexIdToGVVertex.put(vertex.getId(), vertex);

            if (vertex.isPort()) {
                if (vertex.isInputPort()) {
                    inputPorts.add(vertex.getId());
                } else {
                    outputPorts.add(vertex.getId());
                }
            }
        }

        // prepare the edges.
        edges.forEach(edge -> {
            if (edge.getSrcPortId() != null) {
                edgeToSrcPort.put(edge.getId(), edge.getSrcPortId());
            }

            if (edge.getTrgPortId() != null) {
                edgeToTrgPort.put(edge.getId(), edge.getTrgPortId());
            }

            edgeToNodes.put(edge.getId(), new AbstractMap.SimpleEntry<>(edge.getSrcId(), edge.getTrgId()));
        });

        var result = new HierarchicalPlainLayout(
                vertexIds,
                inputPorts,
                outputPorts,
                edgeToNodes,
                edgeToSrcPort,
                edgeToTrgPort,
                portIdToIndex,
                portIdToPos,
                portIdToSize,
                vertexIdToName,
                vertexIdToSize,
                vertexIdToFragmentTextSize,
                settings).execute();

        // apply the result to the vertices.
        for (var outputVertex : result.getVertices().values()) {
            var vertex = vertexIdToGVVertex.get(outputVertex.getVertexId());

            float cellStartX = outputVertex.getPos().x;

            float cellStartY = outputVertex.getPos().y;
            if (vertex.isInputPort()) {
                cellStartY = outputVertex.getPos().y;
                if (vertex.isFakePort()) {
                    cellStartY -= Math.round(vertex.getShape().getSize().y / 2.0f);
                }
            } else if (vertex.isOutputPort()) {
                cellStartY += (outputVertex.getSize().y - vertex.getShape().getSize().y);
                if (vertex.isFakePort()) {
                    cellStartY += Math.round(vertex.getShape().getSize().y / 2.0f);
                }
            } else {
                cellStartY = outputVertex.getPos().y;
            }

            // setup new positions.
            vertex.getShape().getPosition().setLocation(Math.round(cellStartX), Math.round(cellStartY));
        }

        // apply the result to the edges.
        edges.forEach(it -> {
            var outputEdge = result.getEdges().get(it.getId());
            var outputSrcVertex = result.getVertices().get(outputEdge.getSrcId());
            var outputTrgVertex = result.getVertices().get(outputEdge.getTrgId());

            // setup points and edge's type.
            if (result.getEdges().get(it.getId()).isBackward()) {
                it.getShape().setBackward(true);
                it.getShape().setPoints(new BackwardEdgeHandler(outputEdge, outputSrcVertex, outputTrgVertex, settings).execute());
            } else {
                it.getShape().setBackward(false);
                it.getShape().setPoints(new ForwardEdgeHandler(outputEdge, outputSrcVertex, outputTrgVertex, settings).execute());
            }

            // calculate position for edge's label.
            Validate.notEmpty(
                    it.getShape().getPoints(),
                    String.format("Edge with id '%s' ('%s') has no points after handling.",
                            it.getId(),
                            it.getShape().getLabel().getPlainText()));

            it.getShape().getLabel().setPosition(it.getShape().calculateAveragePoint());
        });

        // dirty hack...
        if (parent != null && fakeVertexForVertexWithPorts != null) {
            parent.setType(GVVertex.VERTEX_WITH_PORTS_TYPE);
        }

        if (parent != null) {
            if (parent.isVertexWithPorts()) {
                GraphViewUtils.calculateParentSize(GraphViewVertexShape.RECTANGLE_VERTEX_SHAPE, parent, result.getGraphSize(), vertices, edges);
                var shape = fakeVertexForVertexWithPorts.getShape();
                shape.getPosition().x = (result.getGraphSize().x - shape.getSize().x) / 2;
                parent.getShape().getLabel().setPosition(shape.getPosition());
            } else if (parent.isFragment()) {
                if (settings.fragmentShapeStyle == GraphViewVertexShape.LEFT_FRAGMENT_VERTEX_SHAPE) {
                    GraphViewUtils.calculateParentSize(GraphViewVertexShape.LEFT_FRAGMENT_VERTEX_SHAPE, parent, result.getGraphSize(), vertices, edges);
                } else {
                    GraphViewUtils.calculateParentSize(GraphViewVertexShape.RECTANGLE_VERTEX_SHAPE, parent, result.getGraphSize(), vertices, edges);
                    parent.getShape().getLabel().setVisible(false);
                }
            }
            vertices.add(parent);
        }

        graphView.modifyElements(vertices, edges);

        if (parent != null) {
            vertices.remove(parent);
        }

        // remove fake vertex if it's needed.
        if (fakeVertexForVertexWithPorts != null) {
            vertices.remove(fakeVertexForVertexWithPorts);
            graphView.removeVertex(fakeVertexForVertexWithPorts.getId());
        }

        return result.getCrosses();
    }
}
