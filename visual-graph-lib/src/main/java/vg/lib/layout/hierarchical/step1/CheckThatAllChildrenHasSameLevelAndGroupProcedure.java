package vg.lib.layout.hierarchical.step1;

import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.operation.Procedure;

import java.util.stream.Collectors;

// TODO: Кандидат на удаление.
public class CheckThatAllChildrenHasSameLevelAndGroupProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public CheckThatAllChildrenHasSameLevelAndGroupProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        if (!VGConfigSettings.DIAGNOSTIC_MODE) {
            return;
        }

        for (var vertex : graph.getVertices()) {
            var children = graph.getVerticesByIds(vertex.getOutputVertexIds());

            if (children.size() <= 1) {
                continue;
            }

            var referenceChild = children.get(0);
            for (var child : children) {
                if (child.getLevel() != referenceChild.getLevel()) {
                    throw new IllegalArgumentException(String.format(
                            "Two or more children ('%s') of the vertex ('%s') has different levels.",
                            children.stream().map(HierarchicalVertex::toString).collect(Collectors.joining(", ")),
                            vertex));
                }
                if (child.getGroupId() != referenceChild.getGroupId()) {
                    throw new IllegalArgumentException(String.format(
                            "Two or more children ('%s') of the vertex ('%s') has different group ids.",
                            children.stream().map(HierarchicalVertex::toString).collect(Collectors.joining(", ")),
                            vertex));
                }
            }
        }
    }
}
