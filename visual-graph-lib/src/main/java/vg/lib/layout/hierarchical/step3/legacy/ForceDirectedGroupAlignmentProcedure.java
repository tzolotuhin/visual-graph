package vg.lib.layout.hierarchical.step3.legacy;

import lombok.extern.slf4j.Slf4j;
import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.layout.hierarchical.step3.legacy.data.CoordinateAssignmentTable;
import vg.lib.layout.hierarchical.step3.legacy.operation.ApplyStraightLinesProcedure;
import vg.lib.operation.Procedure;

import java.util.List;
import java.util.TreeMap;

@Slf4j
public class ForceDirectedGroupAlignmentProcedure implements Procedure {
    private final HierarchicalGraph graph;

    private final TreeMap<Integer, List<HierarchicalVertex>> group;

    ForceDirectedGroupAlignmentProcedure(TreeMap<Integer, List<HierarchicalVertex>> group, HierarchicalGraph graph) {
        this.group = group;
        this.graph = graph;
    }

    @Override
    public void execute() {
        log.debug("Use force directed group alignment operation...");

        if (VGConfigSettings.DIAGNOSTIC_MODE) {
            log.debug("Print all levels before execution:");
//            for (var level : levels) {
//                MainService.logger.printDebug("----------------------------------------");
//                MainService.logger.printDebug("Level " + level.getLevel() + ":\n" + level);
//                MainService.logger.printDebug("----------------------------------------");
//            }
        }

        CoordinateAssignmentTable table = new CoordinateAssignmentTable(group, graph);

        new ApplyStraightLinesProcedure(table).execute();

        table.topPass(Direction.BOTH_AND_TOP, true, true);
        table.bottomPass(Direction.BOTH_AND_BOTTOM, true, true);
        table.topPass(Direction.BOTH_AND_TOP, true, true);
        table.bottomPass(Direction.BOTH_AND_BOTTOM, true, true);

        table.fixRealVertices();

        table.bottomPass(Direction.BOTH_AND_BOTTOM, false, false);
        table.topPass(Direction.BOTH_AND_TOP, false, false);

        table.fixMainBackVertices();

        table.bottomPass(Direction.BOTTOM, false, false);
        table.fixEdges(Direction.BOTTOM);

        table.topPass(Direction.TOP, false, false);
        table.fixEdges(Direction.TOP);

        table.unfixCompanionVertices();
        table.bottomPass(Direction.BOTH_AND_BOTTOM, false, false);
        table.fixBottomCompanionVertices();
        table.topPass(Direction.BOTH_AND_TOP, false, false);
        table.fixTopCompanionVertices();

        table.setupCoordinates();

        if (VGConfigSettings.DIAGNOSTIC_MODE) {
            log.debug("Print all levels after execution:");
//            for (var level : levels) {
//                MainService.logger.printDebug("----------------------------------------");
//                MainService.logger.printDebug("Level " + level.getLevel() + ":\n" + level);
//                MainService.logger.printDebug("----------------------------------------");
//            }
        }
    }
}
