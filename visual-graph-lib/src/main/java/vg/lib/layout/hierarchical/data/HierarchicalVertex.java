package vg.lib.layout.hierarchical.data;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;

import java.awt.*;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class HierarchicalVertex implements Comparable<HierarchicalVertex> {
    @Getter
    private final UUID vertexId;

    @Getter @Setter
    private int index;

    @Getter @Setter
    private int posX;

    @Getter @Setter
    private int posY;

    @Getter @Setter
    private int rowPosY;

    @Getter
    private final Point size;

    @Getter
    private final Point borderSize;

    // TODO: необходимо подобрать более удачное название для данной переменной...
    @Getter @Setter
    private Point fragmentTextSize;

    @Getter @Setter
    private VertexType type;

    @Getter @Setter
    private String name;

    @Getter @Setter
    private int initialPortIndex = -1;

    private final Map<UUID, Boolean> inputEdgeToVisible = Maps.newLinkedHashMap();
    private final Map<UUID, Boolean> outputEdgeToVisible = Maps.newLinkedHashMap();

    // edge id -> vertex id.
    private final Map<UUID, UUID> inputEdgeToVertexId = Maps.newLinkedHashMap();
    private final Map<UUID, UUID> outputEdgeToVertexId = Maps.newLinkedHashMap();

    // TODO: should be renamed.
    @Getter
    private int level;

    @Getter @Setter
    private boolean partOfCoreTree;

    @Getter @Setter
    private long importance;

    @Getter
    private int order = -1;

    @Getter
    private int groupId = -1;

    @Getter @Setter
    private UUID longEdgeId;

    @Getter @Setter
    private UUID backEdgeId;

    @Getter @Setter
    private boolean necessaryPartOfLongEdge;

    // TODO: необходимо переименовать это поле в original companion + просмотреть все места где используется.
    @Getter @Setter
    private HierarchicalVertex companionVertex;

    public int topLevelGroupP = 0;
    public int topLevelGroupId = -1;

    public int bottomLevelGroupP = 0;
    public int bottomLevelGroupId = -1;

    @Getter @Setter
    public boolean inputBreakPoint = false;
    @Getter @Setter
    public boolean outputBreakPoint = false;

    @Getter @Setter
    public boolean containsInputBackEdges = false;
    @Getter @Setter
    public boolean containsOutputBackEdges = false;

    HierarchicalVertex(UUID vertexId,
                       VertexType type,
                       int layer,
                       int groupId,
                       Point size,
                       Point borderSize,
                       Point fragmentTextSize) {
        this.vertexId = vertexId;
        this.index = 0;
        this.type = type;
        this.level = layer;
        this.groupId = groupId;
        this.size = size == null ? new Point() : new Point(size);
        this.borderSize = borderSize == null ? new Point() : new Point(borderSize);
        this.fragmentTextSize = fragmentTextSize == null ? new Point() : new Point(fragmentTextSize);
    }

    public boolean isBreakPoint() {
        return inputBreakPoint | outputBreakPoint;
    }

    public HierarchicalVertex setOrder(int order) {
        this.order = order;
        return this;
    }

    // TODO: лучше переименовать данный параметр да и метод в целом... возможно в устоявшийся rank или банально назвать rowIndex.
    public HierarchicalVertex setLevel(int level) {
        this.level = level;
        return this;
    }

    public HierarchicalVertex setGroupId(int groupId) {
        this.groupId = groupId;
        return this;
    }

    public int getWidth() {
        return size.x + 2 * borderSize.x;
    }

    public int getWidthWithoutBorder() {
        return size.x;
    }

    public int getHeight() {
        return size.y + 2 * borderSize.y;
    }

    public int getRoundCenterX() {
        return Math.round(posX + fragmentTextSize.x + (getWidth() - fragmentTextSize.x) / 2.0f);
    }

    // TODO: сделать идентично как в edge.
    public boolean isPartOfLongEdge() {
        return longEdgeId != null;
    }

    public boolean isPartOfBackwardEdge() {
        return backEdgeId != null;
    }

    public boolean isVertex() {
        return isRealVertex() || isFakeVertex();
    }

    public boolean isRealVertex() {
        return type == VertexType.VERTEX_TYPE;
    }

    public boolean isFakeVertex() {
        return type == VertexType.FAKE_VERTEX_TYPE
                || type == VertexType.TOP_BACK_VERTEX_TYPE
                || type == VertexType.MAIN_BACK_VERTEX_TYPE
                || type == VertexType.BOTTOM_BACK_VERTEX_TYPE
                || type == VertexType.TOP_COMPANION_VERTEX_TYPE
                || type == VertexType.BOTTOM_COMPANION_VERTEX_TYPE
                || isBeaconVertex();
    }

    public boolean isCompanionVertex() {
        return type == VertexType.TOP_COMPANION_VERTEX_TYPE
                || type == VertexType.BOTTOM_COMPANION_VERTEX_TYPE;
    }

    public boolean isTopCompanionVertex() {
        return type == VertexType.TOP_COMPANION_VERTEX_TYPE;
    }

    public boolean isBottomCompanionVertex() {
        return type == VertexType.BOTTOM_COMPANION_VERTEX_TYPE;
    }

    public boolean isTopBackVertex() {
        return type == VertexType.TOP_BACK_VERTEX_TYPE;
    }

    public boolean isBottomBackVertex() {
        return type == VertexType.BOTTOM_BACK_VERTEX_TYPE;
    }

    public boolean isMainBackVertex() {
        return type == VertexType.MAIN_BACK_VERTEX_TYPE;
    }

    public boolean isBackVertex() {
        return isBottomBackVertex() || isTopBackVertex() || isMainBackVertex() || isCompanionVertex();
    }

    public boolean isInputPort() {
        return type == VertexType.REAL_INPUT_PORT_TYPE || type == VertexType.FAKE_INPUT_PORT_TYPE;
    }

    public boolean isOutputPort() {
        return type == VertexType.REAL_OUTPUT_PORT_TYPE || type == VertexType.FAKE_OUTPUT_PORT_TYPE;
    }

    public boolean isPort() {
        return isInputPort() | isOutputPort();
    }

    public boolean isTopBeaconVertex() {
        return type == VertexType.TOP_BEACON_VERTEX_TYPE;
    }

    public boolean isBottomBeaconVertex() {
        return type == VertexType.BOTTOM_BEACON_VERTEX_TYPE;
    }

    public boolean isBeaconVertex() {
        return type == VertexType.TOP_BEACON_VERTEX_TYPE || type == VertexType.BOTTOM_BEACON_VERTEX_TYPE;
    }

    /**
     * src vertex id -> input edge id -> the vertex.
     */
    public void addInputEdge(UUID inEdgeId, UUID srcVertexId) {
        if (!inputEdgeToVisible.containsKey(inEdgeId)) {
            inputEdgeToVisible.put(inEdgeId, true);
            inputEdgeToVertexId.put(inEdgeId, srcVertexId);
        }
    }

    /**
     * trg vertex id -> output edge id -> the vertex.
     */
    public void addOutputEdge(UUID outEdgeId, UUID trgVertexId) {
        if (!outputEdgeToVisible.containsKey(outEdgeId)) {
            outputEdgeToVisible.put(outEdgeId, true);
            outputEdgeToVertexId.put(outEdgeId, trgVertexId);
        }
    }

    public void setInputEdgeVisible(UUID inEdgeId, boolean visible) {
        Validate.isTrue(inputEdgeToVisible.containsKey(inEdgeId));
        inputEdgeToVisible.put(inEdgeId, visible);
    }

    public void setOutputEdgeVisible(UUID outEdgeId, boolean visible) {
        Validate.isTrue(outputEdgeToVisible.containsKey(outEdgeId));
        outputEdgeToVisible.put(outEdgeId, visible);
    }

    public List<UUID> getInputEdges() {
        List<UUID> inEdges = Lists.newArrayListWithExpectedSize(inputEdgeToVisible.size());

        for (UUID edgeId : inputEdgeToVisible.keySet()) {
            if (inputEdgeToVisible.get(edgeId)) {
                inEdges.add(edgeId);
            }
        }

        return Collections.unmodifiableList(inEdges);
    }

    // TODO: написать отдельный метод который достает количество, для оптимизации.
    public List<UUID> getOutputEdges() {
        List<UUID> outEdges = Lists.newArrayListWithExpectedSize(outputEdgeToVisible.size());

        for (UUID edgeId : outputEdgeToVisible.keySet()) {
            if (outputEdgeToVisible.get(edgeId)) {
                outEdges.add(edgeId);
            }
        }

        return Collections.unmodifiableList(outEdges);
    }

    public List<UUID> getInputVertexIds() {
        List<UUID> inVertexIds = Lists.newArrayListWithExpectedSize(inputEdgeToVisible.size());

        for (UUID edgeId : inputEdgeToVisible.keySet()) {
            if (inputEdgeToVisible.get(edgeId)) {
                inVertexIds.add(inputEdgeToVertexId.get(edgeId));
            }
        }

        return Collections.unmodifiableList(inVertexIds);
    }

    public List<EdgeWithVertex> getInputEdgeWithVertexList() {
        List<EdgeWithVertex> result = Lists.newArrayListWithExpectedSize(inputEdgeToVisible.size());

        for (UUID edgeId : inputEdgeToVisible.keySet()) {
            if (inputEdgeToVisible.get(edgeId)) {
                result.add(new EdgeWithVertex(edgeId, inputEdgeToVertexId.get(edgeId)));
            }
        }

        return Collections.unmodifiableList(result);
    }

    public List<UUID> getOutputVertexIds() {
        List<UUID> outVertexIds = Lists.newArrayListWithExpectedSize(outputEdgeToVisible.size());

        for (UUID edgeId : outputEdgeToVisible.keySet()) {
            if (outputEdgeToVisible.get(edgeId)) {
                outVertexIds.add(outputEdgeToVertexId.get(edgeId));
            }
        }

        return Collections.unmodifiableList(outVertexIds);
    }

    public List<EdgeWithVertex> getOutputEdgeWithVertexList() {
        List<EdgeWithVertex> result = Lists.newArrayListWithExpectedSize(outputEdgeToVisible.size());

        for (UUID edgeId : outputEdgeToVisible.keySet()) {
            if (outputEdgeToVisible.get(edgeId)) {
                result.add(new EdgeWithVertex(edgeId, outputEdgeToVertexId.get(edgeId)));
            }
        }

        return Collections.unmodifiableList(result);
    }

    public void removeInputEdge(UUID edgeId) {
        Validate.isTrue(inputEdgeToVisible.containsKey(edgeId));
        Validate.isTrue(inputEdgeToVertexId.containsKey(edgeId));

        inputEdgeToVisible.remove(edgeId);
        inputEdgeToVertexId.remove(edgeId);
    }

    public void removeOutputEdge(UUID edgeId) {
        Validate.isTrue(outputEdgeToVisible.containsKey(edgeId));
        Validate.isTrue(outputEdgeToVertexId.containsKey(edgeId));

        outputEdgeToVisible.remove(edgeId);
        outputEdgeToVertexId.remove(edgeId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HierarchicalVertex element = (HierarchicalVertex) o;

        return vertexId.equals(element.vertexId);
    }

    @Override
    public int hashCode() {
        return vertexId.hashCode();
    }

    @Override
    public int compareTo(HierarchicalVertex o) {
        return Integer.compare(order, o.getOrder());
    }

    @Override
    public String toString() {
        return name;

        // TODO: необходимо внести всю необходимую инфу.
//        return name + '\'' +
//                ", groupId=" + groupId +
//                ", level=" + level +
//                ", order=" + order +
//                ", posX=" + posX +
//                ", sizeX=" + size.x;
    }

    /**
     * 0, 1, 2 ... N - 1.
     */
    public static void setupDefaultOrderForElements(List<HierarchicalVertex> vertices) {
        int order = 0;
        for (var vertex : vertices) {
            vertex.setOrder(order++);
        }
    }

    public static class EdgeWithVertex {
        public UUID edgeId;
        public UUID vertexId;

        public EdgeWithVertex(UUID edgeId, UUID vertexId) {
            this.edgeId = edgeId;
            this.vertexId = vertexId;
        }
    }
}