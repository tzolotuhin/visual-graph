package vg.lib.layout.hierarchical.step3.legacy;

import lombok.extern.slf4j.Slf4j;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.operation.Procedure;

@Slf4j
public class LeftAlignmentProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public LeftAlignmentProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        log.debug("Use left alignment...");

        int prevGroupPosX = 0;
        for (var detailedGroupEntry : graph.getDetailedGroups().entrySet()) {
            for (var levelEntry : detailedGroupEntry.getValue().entrySet()) {
                int prevPosX = 0;
                for (int i = 0; i < levelEntry.getValue().size(); i++) {
                    levelEntry.getValue().get(i).setPosX(prevPosX);
                    prevPosX += levelEntry.getValue().get(i).getWidth();
                }
            }

            prevGroupPosX = HierarchicalGraph.shiftPosXForDetailedGroup(prevGroupPosX, detailedGroupEntry.getValue());
        }

        new LeftAlignmentForPortsProcedure(graph).execute();
    }
}
