package vg.lib.layout.hierarchical.step2.legacy.data;

import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;

import java.util.ArrayList;
import java.util.List;

public class Vertex {
    public final int[] ordersRef;
    public final int[] orderToIndexRef;
    public boolean[] visitedRef;

    public List<Edge> inputs = new ArrayList<>();
    public List<Edge> outputs = new ArrayList<>();

    public HierarchicalVertex vertex;

    public int rowIndex;
    public int columnIndex;

    public boolean isPartOfLongChain;

    public boolean isFixed;

    public Direction direction;

    public Vertex(
            HierarchicalVertex vertex,
            int[] orders,
            int[] orderToIndex,
            boolean[] visitedRef,
            int rowIndex,
            int columnIndex) {
        this.vertex = vertex;

        this.ordersRef = orders;
        this.orderToIndexRef = orderToIndex;
        this.visitedRef = visitedRef;

        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;

        this.direction = Direction.BOTH;
    }

    public boolean isSuitable(Direction value) {
        return value == Direction.ALL || direction == Direction.BOTH || direction == value;
    }

    public boolean isVisited() {
        return visitedRef[columnIndex];
    }

    public void setVisited(boolean value) {
        visitedRef[columnIndex] = value;
    }

    public int getLevel() {
        return vertex.getLevel();
    }

    public int getOrder() {
        return ordersRef[columnIndex];
    }

    public void setOrder(int value) {
        ordersRef[columnIndex] = value;
    }

    public boolean containInput(Vertex value) {
        for (var input : inputs) {
            if (input.getSource() == value) {
                return true;
            }
        }
        return false;
    }

    public boolean containOutput(Vertex value) {
        for (var output : outputs) {
            if (output.getTarget() == value) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return vertex.getName() + " - " + direction + " - " + ordersRef[columnIndex];
    }
}
