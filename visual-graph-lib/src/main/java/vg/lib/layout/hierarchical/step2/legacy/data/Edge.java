package vg.lib.layout.hierarchical.step2.legacy.data;

import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalEdge;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;

public class Edge {
    public static final int GENERAL_CROSS_W  = 10000;
    public static final int ONE_FAKE_CROSS_W = 10000;
    public static final int TWO_FAKE_CROSS_W = 10000;

    public static final int TOP_MAIN_BACK_FAKE_CROSS_W = 9000;
    public static final int BOTTOM_MAIN_BACK_FAKE_CROSS_W = 9000;
    public static final int MAIN_BACK_FAKE_CROSS_W = 9000;

    public static final int TOP_BACK_FAKE_CROSS_W = 6000;
    public static final int BOTTOM_BACK_FAKE_CROSS_W = 6000;

    public static final int DIFF_LEVEL_GROUP_CROSS_W = 10000;

    private final int[] sourceRowOrderRef;
    private final int[] targetRowOrderRef;

    private final Vertex[] sourceRowVertexRef;
    private final Vertex[] targetRowVertexRef;

    public final int sourceIndex;
    public final int targetIndex;

    public final int sourcePortOrder;
    public final int targetPortOrder;

    public int chainId = -1;

    public HierarchicalEdge originalEdge;

    public Direction direction;

    public long cw, c;

    public Edge(
            HierarchicalEdge originalEdge,
            int sourceIndex,
            int targetIndex,
            int sourcePortOrder,
            int targetPortOrder,
            int[] sourceRowOrderRef,
            int[] targetRowOrderRef,
            Vertex[] sourceRowVertexRef,
            Vertex[] targetRowVertexRef) {
        this.originalEdge = originalEdge;

        this.sourceIndex = sourceIndex;
        this.targetIndex = targetIndex;

        this.sourcePortOrder = sourcePortOrder;
        this.targetPortOrder = targetPortOrder;

        this.sourceRowOrderRef = sourceRowOrderRef;
        this.targetRowOrderRef = targetRowOrderRef;

        this.sourceRowVertexRef = sourceRowVertexRef;
        this.targetRowVertexRef = targetRowVertexRef;

        this.direction = Direction.BOTH;
    }

    public Vertex getSource() {
        return sourceRowVertexRef[sourceIndex];
    }

    public Vertex getTarget() {
        return targetRowVertexRef[targetIndex];
    }

    public int getSourceOrder() {
        return sourceRowOrderRef[sourceIndex];
    }

    public int getTargetOrder() {
        return targetRowOrderRef[targetIndex];
    }

    public boolean isSrcCompanion() {
        return sourceRowVertexRef[sourceIndex].vertex.isCompanionVertex();
    }

    public boolean isTrgCompanion() {
        return targetRowVertexRef[targetIndex].vertex.isCompanionVertex();
    }

    public HierarchicalVertex getSrcCompanion() {
        return sourceRowVertexRef[sourceIndex].vertex.getCompanionVertex();
    }

    public HierarchicalVertex getTrgCompanion() {
        return targetRowVertexRef[targetIndex].vertex.getCompanionVertex();
    }

    @Override
    public String toString() {
        return sourceRowVertexRef[sourceIndex] + " -> " + targetRowVertexRef[targetIndex];
    }

    public boolean isSuitable(Direction value) {
        return value == Direction.ALL || direction == Direction.BOTH || direction == value;
    }

    public static boolean isIntersected(Edge edge1, Edge edge2) {
        // skip edges with incorrect orders.
        if (edge1.getSourceOrder() < 0 || edge1.getTargetOrder() < 0 || edge2.getSourceOrder() < 0 || edge2.getTargetOrder() < 0) {
            return false;
        }

        // handle case with backward edges.
        if (edge1.isSrcCompanion() && edge2.isSrcCompanion() && edge1.getSrcCompanion() == edge2.getSrcCompanion()
                && (edge1.getTarget().vertex == edge1.getSrcCompanion() || edge2.getTarget().vertex == edge2.getSrcCompanion())) {
            return false;
        }
        if (edge1.isTrgCompanion() && edge2.isTrgCompanion() && edge1.getTrgCompanion() == edge2.getTrgCompanion()
                && (edge1.getSource().vertex == edge1.getTrgCompanion() || edge2.getSource().vertex == edge2.getTrgCompanion())) {
            return false;
        }

        // handle general cases.
        if (edge1.getSourceOrder() < edge2.getSourceOrder() && edge2.getTargetOrder() < edge1.getTargetOrder()) {
            return true;
        }
        if (edge2.getSourceOrder() < edge1.getSourceOrder() && edge1.getTargetOrder() < edge2.getTargetOrder()) {
            return true;
        }

        // handle case when edges have intersection between one vertex and different ports.
        if (edge1.sourcePortOrder >= 0 && edge2.sourcePortOrder >= 0 && edge1.getSourceOrder() == edge2.getSourceOrder()) {
            if (edge1.sourcePortOrder < edge2.sourcePortOrder && edge2.getTargetOrder() < edge1.getTargetOrder()) {
                return true;
            }
            if (edge2.sourcePortOrder < edge1.sourcePortOrder && edge1.getTargetOrder() < edge2.getTargetOrder()) {
                return true;
            }
        }

        if (edge1.targetPortOrder >= 0 && edge2.targetPortOrder >= 0 && edge1.getTargetOrder() == edge2.getTargetOrder()) {
            if (edge1.targetPortOrder < edge2.targetPortOrder && edge2.getSourceOrder() < edge1.getSourceOrder()) {
                return true;
            }
            if (edge2.targetPortOrder < edge1.targetPortOrder && edge1.getSourceOrder() < edge2.getSourceOrder()) {
                return true;
            }
        }

        return false;
    }
}
