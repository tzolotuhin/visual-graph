package vg.lib.layout.hierarchical.data;

import lombok.Getter;

import java.util.Objects;
import java.util.UUID;

public class HierarchicalBackwardEdge {
    @Getter
    private final UUID originalEdgeId;

    @Getter
    private final UUID topEdgeId, bottomEdgeId, edgeBetweenCompanionsId;

    @Getter
    private final HierarchicalVertex srcVertex;

    @Getter
    private final HierarchicalVertex trgVertex;

    @Getter
    private final HierarchicalVertex srcCompanionVertex;

    @Getter
    private final HierarchicalVertex trgCompanionVertex;

    // TODO: необходим перенос как temp полей в операцию.
    public HierarchicalVertex srcCompanionCoupleVertex;

    public HierarchicalVertex trgCompanionCoupleVertex;

    /*
     *   +------------------+
     *   |       TRG_COMP   |
     *   |  TRG      |      |
     *   |   ^       |      |
     *   |   |       |      |
     *   |   |       |      |
     *   |  SRC      v      |
     *   |       SRC_COMP   |
     *   +------------------+
     */
    public HierarchicalBackwardEdge(
            UUID originalEdgeId,
            UUID edgeBetweenCompanionsId,
            UUID topEdgeId,
            UUID bottomEdgeId,
            HierarchicalVertex srcVertex,
            HierarchicalVertex trgVertex,
            HierarchicalVertex srcCompanionVertex,
            HierarchicalVertex trgCompanionVertex) {
        this.originalEdgeId = originalEdgeId;
        this.edgeBetweenCompanionsId = edgeBetweenCompanionsId;
        this.topEdgeId = topEdgeId;
        this.bottomEdgeId = bottomEdgeId;
        this.srcVertex = srcVertex;
        this.trgVertex = trgVertex;
        this.srcCompanionVertex = srcCompanionVertex;
        this.trgCompanionVertex = trgCompanionVertex;
    }

    public boolean isLoop() {
        return srcVertex == trgVertex;
    }

    public int getGroupId() {
        return srcVertex.getGroupId();
    }

    public UUID getSrcCompanionInputVertex() {
        for (var id : srcCompanionVertex.getInputVertexIds()) {
            if (!Objects.equals(id, srcVertex.getVertexId())) {
                return id;
            }
        }
        return null;
    }

    public UUID getTrgCompanionOutputVertex() {
        for (var id : trgCompanionVertex.getOutputVertexIds()) {
            if (!Objects.equals(id, trgVertex.getVertexId())) {
                return id;
            }
        }
        return null;
    }
}
