package vg.lib.layout.hierarchical.step2.data;

import lombok.Builder;

import java.util.UUID;

@Builder
public class CrossingReductionOperationResult {
    // main data.
    public final int numberOfVertices;
    public final UUID[] vertexIndexToId;
    public final int[] vertexIndexToOrder;
    public final int numberOfCrosses;

    // data for debugging and tests.
    public final int[] vertexIndexToWeight;
    public final int[][] weightToVertexIndices;

    public final int[] edgeIndexToType;

    public final long numberOfCrossesBefore;
    public final long[] layerToNumberOfCrossesFromTopToBottomBefore;
    public final long[] layerToNumberOfCrossesFromBottomToTopBefore;
    public final long[] layerToNumberOfChainCrossesWithTopTrapsBefore;
    public final long[] layerToNumberOfChainCrossesWithBottomTrapsBefore;
    public final long[] layerToNumberOfNeighborhoodAnomaliesBefore;

    public final int[] cycleVertexIndexToParentVertexIndex;

    public final int[] chainIndexToStartVertexIndex;
    public final int[] chainIndexToFinishVertexIndex;
    public final long[][] layerToChainIndicesWithVertexIndices;

    public final int[][][] layerToTopTrapGroupsWithVertexIndices;
    public final int[][][] layerToBottomTrapGroupsWithVertexIndices;
    public final int[][] layerToTopTrapVertexIndices;
    public final int[][] layerToBottomTrapVertexIndices;
}
