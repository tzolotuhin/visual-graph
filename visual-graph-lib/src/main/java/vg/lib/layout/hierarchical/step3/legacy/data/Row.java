package vg.lib.layout.hierarchical.step3.legacy.data;

public class Row {
    private final CoordinateAssignmentTable table;

    private final Vertex[] blocksRef;

    public final int rowIndex;

    public Row(Vertex[] vertices, int rowIndex, CoordinateAssignmentTable table) {
        this.blocksRef = vertices;
        this.rowIndex = rowIndex;
        this.table = table;
    }

    public void resetCoordinates(int startX) {
        resetCoordinates(startX, 0);
    }

    public void resetCoordinates(int startX, int space) {
        int lastX = startX;
        for (int columnIndex = 0; columnIndex < blocksRef.length; columnIndex++) {
            blocksRef[columnIndex].xRef[columnIndex] = lastX;
            lastX += blocksRef[columnIndex].size + space;
        }
    }

    public void resetVisited() {
        table.resetVisited();
    }

    public Vertex takeSuitableBlock() {
        Vertex suitableVertex = null;
        for (Vertex vertex : blocksRef) {
            if (vertex.isFixed() || vertex.isVisited()) {
                continue;
            }
            if (vertex.straightLineId >= 0) {
                suitableVertex = vertex;
                break;
            }
        }

        if (suitableVertex != null) {
            suitableVertex.setVisited(true);
            return suitableVertex;
        }

        long maxImportance = Long.MAX_VALUE;
        for (Vertex vertex : blocksRef) {
            if (vertex.isFixed() || vertex.isVisited() || vertex.straightLineId >= 0) {
                continue;
            }

            if (vertex.original.getImportance() < maxImportance) {
                maxImportance = vertex.original.getImportance();
                suitableVertex = vertex;
            }
        }

        if (suitableVertex != null) {
            suitableVertex.setVisited(true);
            return suitableVertex;
        }

        return null;
    }

    public void backup() {
        table.backup();
    }

    public void rollback() {
        table.rollback();
    }

    public boolean moveRight(Vertex vertex) {
        return table.moveRight(vertex);
    }

    public boolean moveLeft(Vertex vertex) {
        return table.moveLeft(vertex);
    }

    public int size() {
        return blocksRef.length;
    }

    public int calcWidth() {
        return blocksRef[blocksRef.length - 1].getX() + blocksRef[blocksRef.length - 1].getSize() - blocksRef[0].getX();
    }

    @Override
    public String toString() {
        var sb = new StringBuilder();

        sb.append("Row #").append(rowIndex).append(":\n");
        for (var v : blocksRef) {
            sb.append(String.format("    %s\n", v));
        }

        return sb.toString();
    }
}
