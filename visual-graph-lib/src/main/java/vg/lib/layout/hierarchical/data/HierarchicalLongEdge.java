package vg.lib.layout.hierarchical.data;

import lombok.Getter;
import org.apache.commons.lang3.Validate;

import java.util.List;
import java.util.UUID;

public class HierarchicalLongEdge {
    @Getter
    private final UUID longEdgeId;

    @Getter
    private final UUID originalEdgeId;

    @Getter
    private final List<HierarchicalVertex> fakeVertices;

    @Getter
    private final List<HierarchicalEdge> fakeEdges;

    public HierarchicalLongEdge(UUID longEdgeId, UUID originalEdgeId, List<HierarchicalVertex> fakeVertices, List<HierarchicalEdge> fakeEdges) {
        Validate.notNull(longEdgeId);
        Validate.notNull(originalEdgeId);
        Validate.notEmpty(fakeVertices);
        Validate.notEmpty(fakeEdges);

        this.longEdgeId = longEdgeId;
        this.originalEdgeId = originalEdgeId;
        this.fakeVertices = fakeVertices;
        this.fakeEdges = fakeEdges;
    }

    public HierarchicalEdge getAnyEdge() {
        return getFirstEdge();
    }

    public HierarchicalEdge getFirstEdge() {
        return fakeEdges.get(0);
    }

    public HierarchicalEdge getLastEdge() {
        return fakeEdges.get(fakeEdges.size() - 1);
    }

    public HierarchicalVertex getSrcVertex() {
        return getFirstEdge().getSrcVertex();
    }

    public HierarchicalVertex getTrgVertex() {
        return getLastEdge().getTrgVertex();
    }

    public UUID getBackId() {
        return getFirstEdge().getBackEdgeId();
    }

    public boolean isPartOfBackwardEdge() {
        return getFirstEdge().getBackEdgeId() != null;
    }
}
