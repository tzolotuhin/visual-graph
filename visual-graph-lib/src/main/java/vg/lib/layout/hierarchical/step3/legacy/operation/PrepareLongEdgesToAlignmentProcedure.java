package vg.lib.layout.hierarchical.step3.legacy.operation;

import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.operation.Procedure;

public class PrepareLongEdgesToAlignmentProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public PrepareLongEdgesToAlignmentProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        for (var longEdge : graph.getLongEdges().values()) {
            var fakeVertices = longEdge.getFakeVertices();

            fakeVertices.forEach(fakeVertex -> {
                fakeVertex.setInputBreakPoint(false);
                fakeVertex.setOutputBreakPoint(false);
                if (graph.rowIsBack(fakeVertex.getLevel())) {
                    fakeVertex.getBorderSize().x = 0;
                }
            });

            if (!longEdge.getAnyEdge().isBackEdge() && longEdge.getTrgVertex().isContainsInputBackEdges()) {
                for (int i = fakeVertices.size() - 1; i >= 0; i--) {
                    var fakeVertex = fakeVertices.get(i);
                    if (!graph.rowIsBack(fakeVertex.getLevel())) {
                        fakeVertex.setInputBreakPoint(true);
                        break;
                    }
                }
            }
            if (!longEdge.getAnyEdge().isBackEdge() && longEdge.getSrcVertex().isContainsOutputBackEdges()) {
                for (int i = 0; i < fakeVertices.size(); i++) {
                    var fakeVertex = fakeVertices.get(i);
                    if (!graph.rowIsBack(fakeVertex.getLevel())) {
                        fakeVertex.setOutputBreakPoint(true);
                        break;
                    }
                }
            }
        }
    }
}
