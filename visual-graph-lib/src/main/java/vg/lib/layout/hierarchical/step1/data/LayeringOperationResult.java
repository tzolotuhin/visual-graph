package vg.lib.layout.hierarchical.step1.data;

import java.util.UUID;

public class LayeringOperationResult {
    public final int numberOfVertices;
    public final UUID[] vertices;
    public final boolean[] inputPorts;
    public final boolean[] outputPorts;

    // TODO: should be removed... because it's really strange thing...
    public final boolean[] partOfCoreTree;

    public final int[] vertexIndexToLayerBeforeMerge;
    public final int[] vertexIndexToLayerAfterMerge;
    public final int[] vertexIndexToLayer;

    public final int[] vertexIndexToGroupIdBeforeMerge;
    public final int[] vertexIndexToGroupId;

    public LayeringOperationResult(int numberOfVertices,
                                   UUID[] vertices,
                                   boolean[] inputPorts,
                                   boolean[] outputPorts,
                                   boolean[] partOfCoreTree,
                                   int[] vertexIndexToLayerBeforeMerge,
                                   int[] vertexIndexToLayerAfterMerge,
                                   int[] vertexIndexToLayer,
                                   int[] vertexIndexToGroupIdBeforeMerge,
                                   int[] vertexIndexToGroupId) {
        this.numberOfVertices = numberOfVertices;
        this.vertices = vertices;
        this.inputPorts = inputPorts;
        this.outputPorts = outputPorts;
        this.partOfCoreTree = partOfCoreTree;

        this.vertexIndexToLayerBeforeMerge = vertexIndexToLayerBeforeMerge;
        this.vertexIndexToLayerAfterMerge = vertexIndexToLayerAfterMerge;
        this.vertexIndexToLayer = vertexIndexToLayer;

        this.vertexIndexToGroupIdBeforeMerge = vertexIndexToGroupIdBeforeMerge;
        this.vertexIndexToGroupId = vertexIndexToGroupId;
    }
}
