package vg.lib.layout.hierarchical.step1;

import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.operation.Procedure;

import java.util.stream.Collectors;

// TODO: Кандидат на удаление.
public class CheckThatAllParentsHasSameLevelAndGroupProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public CheckThatAllParentsHasSameLevelAndGroupProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        if (!VGConfigSettings.DIAGNOSTIC_MODE) {
            return;
        }

        for (var vertex : graph.getVertices()) {
            var parents = graph.getVerticesByIds(vertex.getInputVertexIds());

            if (parents.size() <= 1) {
                continue;
            }

            var referenceParent = parents.get(0);
            for (var parent : parents) {
                if (parent.getLevel() != referenceParent.getLevel()) {
                    HierarchicalGraph.printCollection(
                            String.format(
                                    "Two or more parents of the vertex '%s' (level: %s) has different levels.",
                                    vertex.getName(),
                                    vertex.getLevel()),
                            parents);
                    throw new IllegalArgumentException(String.format(
                            "Two or more parents ('%s') of the vertex ('%s') has different levels.",
                            parents.stream().map(HierarchicalVertex::getName).collect(Collectors.joining(", ")),
                            vertex.getName()));
                }

                if (parent.getGroupId() != referenceParent.getGroupId()) {
                    HierarchicalGraph.printCollection(String.format("Two or more parents of the vertex '%s' has different group ids.", vertex.getName()), parents);
                    throw new IllegalArgumentException(String.format(
                            "Two or more parents ('%s') of the vertex ('%s') has different group ids.",
                            parents.stream().map(HierarchicalVertex::getName).collect(Collectors.joining(", ")),
                            vertex.getName()));
                }
            }
        }
    }
}
