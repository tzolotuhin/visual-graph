package vg.lib.layout.hierarchical.step3.legacy.operation;

import lombok.extern.slf4j.Slf4j;
import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.step3.legacy.data.CoordinateAssignmentTable;
import vg.lib.layout.hierarchical.step3.legacy.data.Edge;
import vg.lib.layout.hierarchical.step3.legacy.data.Row;
import vg.lib.layout.hierarchical.step3.legacy.data.StraightLineCandidate;
import vg.lib.layout.hierarchical.step3.legacy.data.Vertex;
import vg.lib.operation.Procedure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

// TODO: необходимо множество переименований. Поменять принцип работы. См. ApplyChainsProcedure.
@Slf4j
public class ApplyStraightLinesProcedure implements Procedure {
    private final CoordinateAssignmentTable table;

    public ApplyStraightLinesProcedure(CoordinateAssignmentTable table) {
        this.table = table;
    }

    @Override
    public void execute() {
        var downResult = down();

        List<StraightLineCandidate> candidates = new ArrayList<>();
        for (var entry : downResult.entrySet()) {
            var edges = entry.getValue();

            var candidateEdges = new ArrayList<Edge>();
            for (var edge : edges) {
                if (edge.getTargetBlock().original.isOutputBreakPoint()) {
                    edge.direction = Direction.UNKNOWN;
                    if (!candidateEdges.isEmpty()) {
                        candidates.add(new StraightLineCandidate(candidateEdges));
                    }
                    candidateEdges = new ArrayList<>();
                } else if (edge.getSourceBlock().original.isInputBreakPoint()) {
                    edge.direction = Direction.UNKNOWN;
                    if (!candidateEdges.isEmpty()) {
                        candidates.add(new StraightLineCandidate(candidateEdges));
                    }
                    candidateEdges = new ArrayList<>();
                } else {
                    candidateEdges.add(edge);
                }
            }

            if (!candidateEdges.isEmpty()) {
                candidates.add(new StraightLineCandidate(candidateEdges));
            }
        }

        table.applyCandidates(candidates);
    }

    private Map<Vertex, List<Edge>> down() {
        var downResult = new HashMap<Vertex, List<Edge>>();
        for (int rowIndex = 0; rowIndex < table.rows.length - 1; rowIndex++) {
            var prevRow = table.rows[rowIndex];
            var row = table.rows[rowIndex + 1];

            var edgePairs = table.edges[rowIndex];

            var straightEdgePairs = calcStraightLinePairs(prevRow, row, row, edgePairs, Direction.BOTH_AND_BOTTOM);
            var straightEdgePairsAdditional = calcStraightLinePairs(prevRow, row, prevRow, edgePairs, Direction.BOTH_AND_TOP);

            Set<Vertex> used = new HashSet<>();
            Stream.concat(
                    straightEdgePairs.stream(),
                    straightEdgePairsAdditional.stream()).forEach(straightEdge -> {
                if (used.contains(straightEdge.getSourceBlock()) || used.contains(straightEdge.getTargetBlock())) {
                    return;
                }

                used.add(straightEdge.getSourceBlock());
                used.add(straightEdge.getTargetBlock());

                var value = downResult.get(straightEdge.getSourceBlock());
                if (value == null) {
                    value = new ArrayList<>();
                }
                downResult.remove(straightEdge.getSourceBlock());
                downResult.put(straightEdge.getTargetBlock(), value);
                value.add(straightEdge);
            });
        }

        return downResult;
    }

    private List<Edge> collectStraightLinePairs(Edge[] edges) {
        List<Edge> pairs = new ArrayList<>();
        for (var edgePair : edges) {
            int realPart = Math.abs(edgePair.getSourceX() - edgePair.getTargetX());

            if (realPart > 0) {
                continue;
            }

            var src = edgePair.getSourceBlock();
            var trg = edgePair.getTargetBlock();

            if (src.isCompanionVertex() || trg.isCompanionVertex()) {
                continue;
            }

            if (src.isRealVertex() && trg.isBackVertex()) {
                continue;
            }
            if (src.isBackVertex() && trg.isRealVertex()) {
                continue;
            }

            if (src.inputEdgeAmount > 1 || src.outputEdgeAmount > 1) {
                continue;
            }

            if (trg.inputEdgeAmount > 1 || trg.outputEdgeAmount > 1) {
                continue;
            }

            if (!trg.isFakeVertex() || !trg.isFakeVertex()) {
                //continue;
            }

            pairs.add(edgePair);
        }

        return pairs;
    }

    private List<Edge> calcStraightLinePairs(Row row1, Row row2, Row currRow, Edge[] edges, Direction direction) {
        row1.resetCoordinates(0, 500);
        row2.resetCoordinates(0, 500);

        new AlignRowProcedure(currRow, new CalcLengthOperation(edges, direction, false)).execute();

        var straightEdgePairs = collectStraightLinePairs(edges);

        if (VGConfigSettings.DIAGNOSTIC_MODE) {
            log.debug("Straight edge pairs for between levels: {} and {}.", row1.rowIndex, row2.rowIndex);
            for (var straightEdgePair : straightEdgePairs) {
                log.debug(straightEdgePair.toString());
            }
        }

        return straightEdgePairs;
    }
}
