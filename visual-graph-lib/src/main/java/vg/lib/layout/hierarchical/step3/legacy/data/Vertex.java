package vg.lib.layout.hierarchical.step3.legacy.data;

import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.layout.hierarchical.data.VertexType;

public class Vertex {
    public HierarchicalVertex original;
    public final int originalId;

    public VertexType type;

    public final int[] xRef;
    public boolean[] visitedRef;
    public boolean[] fixedRef;

    public String name;

    public int rowIndex;
    public int columnIndex;

    public int size;

    public int inputEdgeAmount;
    public int outputEdgeAmount;

    public int straightLineId = -1;

    public boolean isStraightLineStartBlock;
    public boolean isStraightLineFinishBlock;

    public int leftSpace;
    public int rightSpace;

    public int moveNumber;

    public Vertex(
            HierarchicalVertex original,
            int originalId,
            String name,
            VertexType type,
            int rowIndex,
            int columnIndex,
            int size,
            int[] xRef,
            boolean[] visitedRef,
            boolean[] fixedRef) {
        this.original = original;
        this.originalId = originalId;

        this.name = name;

        this.type = type;

        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;

        this.size = size;

        this.xRef = xRef;
        this.visitedRef = visitedRef;
        this.fixedRef = fixedRef;
    }

    public boolean isVertex() {
        return isRealVertex() || isFakeVertex();
    }

    public boolean isRealVertex() {
        return type == VertexType.VERTEX_TYPE;
    }

    public boolean isFakeVertex() {
        return type == VertexType.FAKE_VERTEX_TYPE
                || type == VertexType.TOP_BACK_VERTEX_TYPE
                || type == VertexType.MAIN_BACK_VERTEX_TYPE
                || type == VertexType.BOTTOM_BACK_VERTEX_TYPE
                || type == VertexType.TOP_COMPANION_VERTEX_TYPE
                || type == VertexType.BOTTOM_COMPANION_VERTEX_TYPE
                || type == VertexType.TOP_BEACON_VERTEX_TYPE
                || type == VertexType.BOTTOM_BEACON_VERTEX_TYPE;
    }

    public boolean isCompanionVertex() {
        return type == VertexType.TOP_COMPANION_VERTEX_TYPE
                || type == VertexType.BOTTOM_COMPANION_VERTEX_TYPE;
    }

    public boolean isTopCompanionVertex() {
        return type == VertexType.TOP_COMPANION_VERTEX_TYPE;
    }

    public boolean isBottomCompanionVertex() {
        return type == VertexType.BOTTOM_COMPANION_VERTEX_TYPE;
    }

    public boolean isTopBackVertex() {
        return type == VertexType.TOP_BACK_VERTEX_TYPE;
    }

    public boolean isBottomBackVertex() {
        return type == VertexType.BOTTOM_BACK_VERTEX_TYPE;
    }

    public boolean isMainBackVertex() {
        return type == VertexType.MAIN_BACK_VERTEX_TYPE;
    }

    public boolean isBackVertex() {
        return isBottomBackVertex() || isTopBackVertex() || isMainBackVertex() || isCompanionVertex();
    }

    public boolean isInputPort() {
        return type == VertexType.REAL_INPUT_PORT_TYPE || type == VertexType.FAKE_INPUT_PORT_TYPE;
    }

    public boolean isOutputPort() {
        return type == VertexType.REAL_OUTPUT_PORT_TYPE || type == VertexType.FAKE_OUTPUT_PORT_TYPE;
    }

    public boolean isPort() {
        return isInputPort() | isOutputPort();
    }

    public boolean isFixed() {
        return fixedRef[columnIndex];
    }

    public void setFixed(boolean value) {
        fixedRef[columnIndex] = value;
    }

    public boolean isVisited() {
        return visitedRef[columnIndex];
    }

    public void setVisited(boolean value) {
        visitedRef[columnIndex] = value;
    }

    public void setX(int x) {
        xRef[columnIndex] = x;
    }

    public int getX() {
        return xRef[columnIndex];
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return name;
    }
}
