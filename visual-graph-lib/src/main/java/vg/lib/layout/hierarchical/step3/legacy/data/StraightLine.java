package vg.lib.layout.hierarchical.step3.legacy.data;

import java.util.ArrayList;
import java.util.List;

public class StraightLine {
    public int id;
    public List<Vertex> vertices;
    public int[] shifts;
    public int shiftAttempt;

    private int moveNumber;

    public StraightLine(int id) {
        this.id = id;
        vertices = new ArrayList<>();
    }

    public void setMoveNumber(int moveNumber) {
        this.moveNumber = moveNumber;
        vertices.forEach(x -> x.moveNumber = moveNumber);
    }

    public int getMoveNumber() {
        return moveNumber;
    }

    @Override
    public String toString() {
        return vertices.size() + " | " + vertices.get(0).toString() + " | " + vertices.get(vertices.size() - 1).toString();
    }
}
