package vg.lib.layout.hierarchical.step2.legacy;

import lombok.extern.slf4j.Slf4j;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.step2.legacy.data.CrossingReductionTable;
import vg.lib.operation.Procedure;

@Slf4j
public class LegacyCrossingReductionProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public LegacyCrossingReductionProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        log.debug("Use crossing reduction procedure...");

        graph.resetOrders();

        // setup initial orders for the vertices.
        int amountOfIntersections = 0;
        int prevGroupOrder = 0;
        var detailedGroups = graph.getDetailedGroups();

        log.debug("Amount of detailed groups '{}'.", detailedGroups.size());
        for (var detailedGroupEntry : detailedGroups.entrySet()) {
            log.debug("Handle detailed group '{}'.", detailedGroupEntry.getKey());

            var table = new CrossingReductionTable(detailedGroupEntry.getValue(), graph);

            if (!graph.getEdges().isEmpty()) {
                // TODO: возможно нужно будет передавать данный вектор из настроек.
                var defaultVector = new int[] {1, 1, 1, 1};

                for (int it = 0; it < 3; it++) {
                    table.doExecuteBottom(0, Direction.ALL, defaultVector);
                    table.doExecuteTop(table.vertices.length - 1, Direction.ALL, defaultVector);
                }
                table.rollbackBestSolution();

                table.fixDirection(Direction.BOTH);
                table.doExecuteBottom(0, Direction.BOTTOM, defaultVector);
                table.fixDirection(Direction.BOTTOM);
                table.doExecuteTop(table.vertices.length - 1, Direction.ALL, defaultVector);
            }

            table.setupOrders();

            long crosses = table.calculateCrosses();

            log.debug("Crossing reduction procedure was finished. Amount of crosses: {}", crosses);

            prevGroupOrder = HierarchicalGraph.shiftOrderForDetailedGroup(prevGroupOrder, detailedGroupEntry.getValue()) + 1;

            graph.crosses += crosses;
        }

        log.debug("Crossing reduction operation was finished. Amount of intersections: {}", amountOfIntersections);
    }
}
