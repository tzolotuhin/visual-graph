package vg.lib.layout.hierarchical.data;

public enum Direction {
    TOP,
    BOTTOM,
    BOTH,
    BOTH_AND_TOP,
    BOTH_AND_BOTTOM,
    UNKNOWN,
    ALL
}
