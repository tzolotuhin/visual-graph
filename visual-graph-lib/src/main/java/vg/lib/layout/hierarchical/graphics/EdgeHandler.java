package vg.lib.layout.hierarchical.graphics;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import vg.lib.common.SplineInterpolator;
import vg.lib.layout.hierarchical.HierarchicalLayoutSettings;
import vg.lib.layout.hierarchical.HierarchicalPlainLayout;
import vg.lib.operation.Operation;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public abstract class EdgeHandler implements Operation<List<Point>> {
    protected final HierarchicalLayoutSettings settings;

    HierarchicalPlainLayout.Result.OutputEdge outputEdge;
    HierarchicalPlainLayout.Result.OutputVertex outputSrcVertex;
    HierarchicalPlainLayout.Result.OutputVertex outputTrgVertex;

    Point srcVertexPoint, srcVertexWithOutputMarginPoint;
    Point trgVertexPoint, trgVertexWithOutputMarginPoint;

    protected List<Point> result;

    EdgeHandler(HierarchicalPlainLayout.Result.OutputEdge outputEdge,
                HierarchicalPlainLayout.Result.OutputVertex outputSrcVertex,
                HierarchicalPlainLayout.Result.OutputVertex outputTrgVertex,
                HierarchicalLayoutSettings settings) {
        this.outputEdge = outputEdge;
        this.outputSrcVertex = outputSrcVertex;
        this.outputTrgVertex = outputTrgVertex;

        this.result = Lists.newArrayList();

        this.settings = settings;
    }

    /**
     * Calculates points for source vertex.
     */
    void calcStartEdgePoints(int outputYMargin) {
        float x = outputSrcVertex.getPos().x + outputEdge.getSrcPosX();
        float y = outputSrcVertex.getPos().y + outputSrcVertex.getSize().y;

        srcVertexPoint = new Point(Math.round(x), Math.round(y));
        srcVertexWithOutputMarginPoint = new Point(Math.round(x), Math.round(y + outputYMargin));
    }

    /**
     * Calculates points for target vertex.
     */
    void calcFinishEdgePoints(int inputYMargin) {
        float x = outputTrgVertex.getPos().x + outputEdge.getTrgPosX();
        float y = outputTrgVertex.getPos().y;

        trgVertexPoint = new Point(Math.round(x), Math.round(y));
        trgVertexWithOutputMarginPoint = new Point(Math.round(x), Math.round(y - inputYMargin));
    }

    protected List<Point> splineInterpolationIfNeed(List<Point> points) {
        return splineInterpolationIfNeed(points, false);
    }

    protected List<Point> splineInterpolationIfNeed(List<Point> points, boolean reverse) {
        var re = points;
        try {
            if (settings.routingStyle == HierarchicalLayoutSettings.SPLINE_ROUTING_STYLE) {
                re = doSplineInterpolation(points);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }

        if (reverse) {
            Collections.reverse(re);
        }

        return re;
    }

    private List<Point> doSplineInterpolation(List<Point> points) {
        // remove duplicates.
        List<Float> xPoints = Lists.newArrayList();
        List<Float> yPoints = Lists.newArrayList();
        for (int i = 0; i < points.size() - 1; i++) {
            var p1 = points.get(i);
            var p2 = points.get(i + 1);

            float h = p2.y - p1.y;
            if (h > 0f) {
                xPoints.add((float)p1.y);
                yPoints.add((float)p1.x);
            }

            if (i == points.size() - 2) {
                xPoints.add((float)p2.y);
                yPoints.add((float)p2.x);
            }
        }

        // apply spline interpolator.
        var interpolator = SplineInterpolator.createMonotoneCubicSpline(xPoints, yPoints);

        // obtain result.
        int xPrev = Integer.MIN_VALUE;
        var result = new ArrayList<Point>();
        for (int i = 0; i < points.size() - 1; i++) {
            var p1 = points.get(i);
            var p2 = points.get(i + 1);

            boolean ignore;
            for (int j = 0; j <= 5; j++) {
                ignore = false;

                float xy = p1.y + j * (p2.y - p1.y) / 5.0f;
                int x = Math.round(interpolator.interpolate(xy));

                if (xPrev == x && j != 0 && j != 5) {
                    ignore = true;
                }

                int yPrev = Math.round(xy);
                xPrev = x;

                if (!ignore) {
                    result.add(new Point(xPrev, yPrev));
                }

            }
        }

        return result;
    }
}
