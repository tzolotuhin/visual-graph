package vg.lib.layout.hierarchical.step2.custom;

import lombok.extern.slf4j.Slf4j;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.operation.Procedure;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class CustomCrossingReductionProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public CustomCrossingReductionProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        log.debug("Use custom crossing reduction procedure...");

        graph.resetOrders();

        var groupIds = graph.getGroupIds();
        log.debug("Number of groups is '{}'.", groupIds.length);

        var prevMaxGroupOrder = 0;
        for (var groupId : groupIds) {
            log.debug("Handle group {}.", groupId);

            // prepare vertices.
            var vertices = new ArrayList<UUID>();
            var dummyVertices = new ArrayList<UUID>();
            var vertexIdToLayer = new LinkedHashMap<UUID, Integer>();
            var vertexIdToInitialOrder = new LinkedHashMap<UUID, Integer>();
            var vertexIdToName = new LinkedHashMap<UUID, String>();
            var vertexIdToVertex = new LinkedHashMap<UUID, HierarchicalVertex>();
            for (var vertex: graph.getVerticesByGroupId(groupId)) {
                vertices.add(vertex.getVertexId());
                vertexIdToLayer.put(vertex.getVertexId(), vertex.getLevel());
                vertexIdToInitialOrder.put(vertex.getVertexId(), vertex.getInitialPortIndex());
                vertexIdToName.put(vertex.getVertexId(), vertex.getName());
                vertexIdToVertex.put(vertex.getVertexId(), vertex);
                if (vertex.isFakeVertex()) {
                    dummyVertices.add(vertex.getVertexId());
                }
            }

            // prepare edges.
            var edgeIdToNodeIds = new LinkedHashMap<UUID, Map.Entry<UUID, UUID>>();
            var edgeIdToPortIndices = new LinkedHashMap<UUID, Map.Entry<Integer, Integer>>();
            for (var edge : graph.getEdgesByGroupId(groupId)) {
                edgeIdToNodeIds.put(
                        edge.getId(),
                        Map.entry(edge.getSrcVertex().getVertexId(), edge.getTrgVertex().getVertexId()));

                edgeIdToPortIndices.put(
                        edge.getId(),
                        Map.entry(edge.getSrcPortIndex(), edge.getTrgPortIndex())
                );
            }

            // execute algorithm.
            var result = new CustomCrossingReductionForOneGroupOperation(
                    vertices,
                    edgeIdToNodeIds,
                    edgeIdToPortIndices,
                    vertexIdToLayer,
                    dummyVertices,
                    vertexIdToInitialOrder,
                    vertexIdToName).execute();

            // setup calculated orders.
            var currMaxGroupOrder = 0;
            for (var vertexIndex = 0; vertexIndex < result.numberOfVertices; vertexIndex++) {
                var vertexId = result.vertexIndexToId[vertexIndex];
                var order = result.vertexIndexToOrder[vertexIndex];
                var vertex = vertexIdToVertex.get(vertexId);
                vertex.setOrder(prevMaxGroupOrder + order);

                if (prevMaxGroupOrder + order > currMaxGroupOrder) {
                    currMaxGroupOrder = prevMaxGroupOrder + order;
                }
            }

            prevMaxGroupOrder = currMaxGroupOrder + 1;

            graph.crosses += result.numberOfCrosses;

            // print result information.
            log.debug("Group {} was handled. Number of crosses: {}.", groupId, result.numberOfCrosses);
            log.debug("Chains: {}.", result.chainIndexToStartVertexIndex.length);
            for (var chainIndex = 0; chainIndex < result.chainIndexToStartVertexIndex.length; chainIndex++) {
                var startVertexId = result.vertexIndexToId[result.chainIndexToStartVertexIndex[chainIndex]];
                var finishVertexId = result.vertexIndexToId[result.chainIndexToFinishVertexIndex[chainIndex]];
                var startVertexName = vertexIdToName.get(startVertexId);
                var finishVertexName = vertexIdToName.get(finishVertexId);
                log.debug("{}: '{}'  ->  '{}'", chainIndex, startVertexName, finishVertexName);
            }

            log.debug("Weight to number of vertices: ");
            for (var weight = 0; weight < result.weightToVertexIndices.length; weight++) {
                if (result.weightToVertexIndices[weight].length == 0) {
                    continue;
                }

                log.debug("'{}'  ->  '{}'", weight, result.weightToVertexIndices[weight].length);
            }
        }

        log.debug("Crossing reduction operation was finished. Amount of crosses: {}", graph.crosses);
    }
}
