package vg.lib.layout.hierarchical.step3.legacy.operation;

import org.apache.commons.lang3.Validate;
import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.data.HierarchicalBackwardEdge;
import vg.lib.layout.hierarchical.data.HierarchicalEdge;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.layout.hierarchical.data.VertexType;
import vg.lib.operation.Procedure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class PrepareBackwardEdgesToAlignmentProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public PrepareBackwardEdgesToAlignmentProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        for (var detailedGroup : graph.getDetailedGroups().entrySet()) {
            int index = 0;
            // TODO: тут с запасом берется... но лучше конечно если будет вся информация в группе.
            var shifts = new Shift[detailedGroup.getValue().size() + 5];
            var candidates = new ArrayList<PossibleCandidate>();
            for (var detailedGroupEntry : detailedGroup.getValue().entrySet()) {
                var rowCandidates = detailedGroupEntry.getValue().stream()
                        .map(this::handleVertex)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());

                for (var rowCandidate : rowCandidates) {
                    int topMax = rowCandidate.topMax();
                    if (topMax > 0) {
                        var comp = rowCandidate.peekAnyInputCompanion();
                        Validate.notNull(comp);
                        var shift = shifts[comp.getLevel()];
                        if (shift == null || shift.amount < topMax) {
                            shifts[comp.getLevel()] = new Shift(comp, topMax);
                        }
                    }

                    int bottomMax = rowCandidate.bottomMax();
                    if (bottomMax > 0) {
                        var comp = rowCandidate.peekAnyOutputCompanion();
                        Validate.notNull(comp);
                        var shift = shifts[comp.getLevel()];
                        if (shift == null || shift.amount < bottomMax) {
                            shifts[comp.getLevel()] = new Shift(comp, bottomMax);
                        }
                    }
                }

                candidates.addAll(rowCandidates);

                index++;
            }

            // construction...
            for (var c : candidates) {
                c.addFakes(graph);
            }

            // TODO: re-order можно вставить сюда и убрать снизу.

            // extend...
            for (var shift : shifts) {
                if (shift == null) {
                    continue;
                }
                var companion = shift.companion;
                graph.extendRows(companion.getLevel(), companion.getGroupId(), shift.amount, Direction.BOTTOM);
            }

            // construction...
            for (var c : candidates) {
                c.setupCorrectRowIndexForCompanions(graph);
            }

            // update roots.
            graph.reCalculateRoots();

            // fix long edges.
            var longEdges = graph.getEdges().values().stream().filter(HierarchicalEdge::isLongEdge).collect(Collectors.toList());
            for (var longEdge : longEdges) {
                graph.convertToLongEdge(longEdge);
            }

            // update indices.
            graph.reCalculateVertexIndices();

            graph.getBackwardEdges().values().stream().forEach(backwardEdge -> {
                graph.getVerticesByIds(backwardEdge.getTrgCompanionVertex().getOutputVertexIds()).forEach(v -> {
                    v.getSize().y = graph.companionVertexSize.y;
                });
                graph.getVerticesByIds(backwardEdge.getSrcCompanionVertex().getInputVertexIds()).forEach(v -> {
                    v.getSize().y = graph.companionVertexSize.y;
                });
            });
        }

        // re-order the vertices.
        int prevGroupOrder = 0;
        for (var detailedGroupEntry : graph.getDetailedGroups().entrySet()) {
            for (var levelEntry : detailedGroupEntry.getValue().entrySet()) {
                HierarchicalVertex.setupDefaultOrderForElements(levelEntry.getValue());
            }

            prevGroupOrder = HierarchicalGraph.shiftOrderForDetailedGroup(prevGroupOrder, detailedGroupEntry.getValue()) + 1;
        }
    }

    private PossibleCandidate handleVertex(HierarchicalVertex vertex) {
        if (vertex.isFakeVertex()) {
            return null;
        }

        var outputLeftVertices = new ArrayList<HierarchicalBackwardEdge>();
        var outputRightVertices = new ArrayList<HierarchicalBackwardEdge>();
        var outputVertices = graph.getVerticesByIds(vertex.getOutputVertexIds());
        for (var outputVertex : outputVertices) {
            if (outputVertex.isCompanionVertex()) {
                var backwardEdgeId = outputVertex.getBackEdgeId();
                var backwardEdge = graph.getBackwardEdges().get(backwardEdgeId);
                var backwardVertex = graph.getVertexById(backwardEdge.getSrcCompanionInputVertex());
                if (backwardVertex.getOrder() > vertex.getOrder()) {
                    outputRightVertices.add(backwardEdge);
                } else {
                    outputLeftVertices.add(backwardEdge);
                }
            }
        }

        var inputLeftVertices = new ArrayList<HierarchicalBackwardEdge>();
        var inputRightVertices = new ArrayList<HierarchicalBackwardEdge>();
        var inputVertices = graph.getVerticesByIds(vertex.getInputVertexIds());
        for (var inputVertex : inputVertices) {
            if (inputVertex.isCompanionVertex()) {
                var backwardEdgeId = inputVertex.getBackEdgeId();
                var backwardEdge = graph.getBackwardEdges().get(backwardEdgeId);
                var backwardVertex = graph.getVertexById(backwardEdge.getTrgCompanionOutputVertex());
                if (backwardVertex.getOrder() > vertex.getOrder()) {
                    inputRightVertices.add(backwardEdge);
                } else {
                    inputLeftVertices.add(backwardEdge);
                }
            }
        }

        if (inputLeftVertices.isEmpty()
                && inputRightVertices.isEmpty()
                && outputLeftVertices.isEmpty()
                && outputRightVertices.isEmpty()) {
            return null;
        }

        Collections.reverse(inputLeftVertices);
        Collections.reverse(outputLeftVertices);

        return new PossibleCandidate(vertex, inputLeftVertices, inputRightVertices, outputLeftVertices, outputRightVertices);
    }

    private static class Shift {
        HierarchicalVertex companion;
        int amount;

        public Shift(HierarchicalVertex companion, int amount) {
            this.companion = companion;
            this.amount = amount;
        }
    }

    // TODO: необходимо приложить схему для расположения back'ов.
    private static class PossibleCandidate {
        HierarchicalVertex vertex;
        List<HierarchicalBackwardEdge> inputLeftVertices;
        List<HierarchicalBackwardEdge> inputRightVertices;
        List<HierarchicalBackwardEdge> outputLeftVertices;
        List<HierarchicalBackwardEdge> outputRightVertices;

        public PossibleCandidate(
                HierarchicalVertex vertex,
                List<HierarchicalBackwardEdge> inputLeftVertices,
                List<HierarchicalBackwardEdge> inputRightVertices,
                List<HierarchicalBackwardEdge> outputLeftVertices,
                List<HierarchicalBackwardEdge> outputRightVertices) {
            this.vertex = vertex;
            this.inputLeftVertices = inputLeftVertices;
            this.inputRightVertices = inputRightVertices;
            this.outputLeftVertices = outputLeftVertices;
            this.outputRightVertices = outputRightVertices;
        }

        public HierarchicalVertex peekAnyInputCompanion() {
            if (!inputLeftVertices.isEmpty()) {
                return inputLeftVertices.get(0).getTrgCompanionVertex();
            }
            if (!inputRightVertices.isEmpty()) {
                return inputRightVertices.get(0).getTrgCompanionVertex();
            }

            return null;
        }

        public HierarchicalVertex peekAnyOutputCompanion() {
            if (!outputLeftVertices.isEmpty()) {
                return outputLeftVertices.get(0).getSrcCompanionVertex();
            }
            if (!outputRightVertices.isEmpty()) {
                return outputRightVertices.get(0).getSrcCompanionVertex();
            }

            return null;
        }

        public int topMax() {
            return Math.max(inputLeftVertices.size(), inputRightVertices.size());
        }

        public int bottomMax() {
            return Math.max(outputLeftVertices.size(), outputRightVertices.size());
        }

        // TODO: переименовать необходимо и вынести в отдельный метод графа.
        public List<HierarchicalVertex> get(List<UUID> vertexIds, HierarchicalGraph graph) {
            return graph.getVerticesByIds(vertexIds).stream().filter(v -> !v.isCompanionVertex()).collect(Collectors.toList());
        }

        // TODO: этот метод необходимо вытащить в отдельную операцию.
        public void addFakes(HierarchicalGraph graph) {
            if (inputLeftVertices.size() > 0) {
                var inputVertices = get(vertex.getInputVertexIds(), graph);
                var inputLeftVertex = inputVertices.get(0);

                graph.shiftRightOrder(inputLeftVertex, inputLeftVertices.size());

                int order = inputLeftVertex.getOrder();
                for (int backIndex = inputLeftVertices.size() - 1; backIndex >= 0; backIndex--) {
                    var back = inputLeftVertices.get(backIndex);
                    back.trgCompanionCoupleVertex = graph.addFakeVertex(
                        back.getTrgCompanionVertex().getLevel(),
                        back.getGroupId(),
                        "L_COUPLE_TRG");
                    back.trgCompanionCoupleVertex.setOrder(order - 1);
                    order--;
                }
            }
            if (inputRightVertices.size() > 0) {
                var inputVertices = get(vertex.getInputVertexIds(), graph);
                var inputRightVertex = inputVertices.get(inputVertices.size() - 1);

                graph.shiftLeftOrder(inputRightVertex, inputRightVertices.size());

                int order = inputRightVertex.getOrder();
                for (int backIndex = inputRightVertices.size() - 1; backIndex >= 0; backIndex--) {
                    var back = inputRightVertices.get(backIndex);
                    back.trgCompanionCoupleVertex = graph.addFakeVertex(
                        back.getTrgCompanionVertex().getLevel(),
                        back.getGroupId(),
                        "R_COUPLE_TRG");
                    back.trgCompanionCoupleVertex.setOrder(order + 1);
                    order++;
                }
            }

            if (outputLeftVertices.size() > 0) {
                var outputVertices = get(vertex.getOutputVertexIds(), graph);
                var outputLeftVertex = outputVertices.get(0);

                graph.shiftRightOrder(outputLeftVertex, outputLeftVertices.size());

                int order = outputLeftVertex.getOrder();
                for (int backIndex = outputLeftVertices.size() - 1; backIndex >= 0; backIndex--) {
                    var back = outputLeftVertices.get(backIndex);
                    back.srcCompanionCoupleVertex = graph.addFakeVertex(
                        back.getSrcCompanionVertex().getLevel(),
                        back.getGroupId(),
                        "L_COUPLE_SRC");
                    back.srcCompanionCoupleVertex.setOrder(order - 1);
                    order--;
                }
            }
            if (outputRightVertices.size() > 0) {
                var outputVertices = get(vertex.getOutputVertexIds(), graph);
                var outputRightVertex = outputVertices.get(outputVertices.size() - 1);

                graph.shiftLeftOrder(outputRightVertex, outputRightVertices.size());

                int order = outputRightVertex.getOrder();
                for (int backIndex = outputRightVertices.size() - 1; backIndex >= 0; backIndex--) {
                    var back = outputRightVertices.get(backIndex);
                    back.srcCompanionCoupleVertex = graph.addFakeVertex(
                        back.getSrcCompanionVertex().getLevel(),
                        back.getGroupId(),
                        "R_COUPLE_SRC");
                    back.srcCompanionCoupleVertex.setOrder(order + 1);
                    order++;
                }
            }

            // TODO: удаление маячков необходимо вероятно как-то более красиво оформить.
            graph.getVerticesByIds(vertex.getInputVertexIds()).forEach(v -> {
                if (v.isBeaconVertex()) {
                    Validate.isTrue(v.getInputEdges().size() == 0);
                    Validate.isTrue(v.getOutputEdges().size() == 1);
                    graph.removeEdge(v.getOutputEdges().get(0));
                    graph.removeVertex(v);
                }
            });
            graph.getVerticesByIds(vertex.getOutputVertexIds()).forEach(v -> {
                if (v.isBeaconVertex()) {
                    Validate.isTrue(v.getInputEdges().size() == 1);
                    Validate.isTrue(v.getOutputEdges().size() == 0);
                    graph.removeEdge(v.getInputEdges().get(0));
                    graph.removeVertex(v);
                }
            });
        }

        public void setupCorrectRowIndexForCompanions(HierarchicalGraph graph) {
            int backIndex = 1;
            for (var back : inputLeftVertices) {
                back.getTrgCompanionVertex().setLevel(vertex.getLevel() - backIndex - 1);

                var fakeEdge = graph.getEdgeById(back.getTopEdgeId());
                var longEdge = graph.convertToLongEdge(fakeEdge);

                int order = back.trgCompanionCoupleVertex.getOrder();
                longEdge.getFakeVertices().forEach(v -> v.setOrder(order));
                longEdge.getFakeVertices().forEach(v -> v.setType(VertexType.TOP_BACK_VERTEX_TYPE));
                longEdge.getFakeVertices().forEach(v -> v.getBorderSize().x = 0);

                graph.removeVertex(back.trgCompanionCoupleVertex);

                backIndex++;
            }
            backIndex = 1;
            for (var back : inputRightVertices) {
                back.getTrgCompanionVertex().setLevel(vertex.getLevel() - backIndex - 1);

                var fakeEdge = graph.getEdgeById(back.getTopEdgeId());
                var longEdge = graph.convertToLongEdge(fakeEdge);

                int order = back.trgCompanionCoupleVertex.getOrder();
                longEdge.getFakeVertices().forEach(v -> v.setOrder(order));
                longEdge.getFakeVertices().forEach(v -> v.setType(VertexType.TOP_BACK_VERTEX_TYPE));
                longEdge.getFakeVertices().forEach(v -> v.getBorderSize().x = 0);

                graph.removeVertex(back.trgCompanionCoupleVertex);

                backIndex++;
            }
            backIndex = 1;
            for (var back : outputLeftVertices) {
                back.getSrcCompanionVertex().setLevel(vertex.getLevel() + backIndex + 1);

                var fakeEdge = graph.getEdgeById(back.getBottomEdgeId());
                var longEdge = graph.convertToLongEdge(fakeEdge);

                int order = back.srcCompanionCoupleVertex.getOrder();
                longEdge.getFakeVertices().forEach(v -> v.setOrder(order));
                longEdge.getFakeVertices().forEach(v -> v.setType(VertexType.BOTTOM_BACK_VERTEX_TYPE));
                longEdge.getFakeVertices().forEach(v -> v.getBorderSize().x = 0);

                graph.removeVertex(back.srcCompanionCoupleVertex);

                backIndex++;
            }
            backIndex = 1;
            for (var back : outputRightVertices) {
                back.getSrcCompanionVertex().setLevel(vertex.getLevel() + backIndex + 1);

                var fakeEdge = graph.getEdgeById(back.getBottomEdgeId());
                var longEdge = graph.convertToLongEdge(fakeEdge);

                int order = back.srcCompanionCoupleVertex.getOrder();
                longEdge.getFakeVertices().forEach(v -> v.setOrder(order));
                longEdge.getFakeVertices().forEach(v -> v.setType(VertexType.BOTTOM_BACK_VERTEX_TYPE));
                longEdge.getFakeVertices().forEach(v -> v.getBorderSize().x = 0);

                graph.removeVertex(back.srcCompanionCoupleVertex);

                backIndex++;
            }
        }
    }
}
