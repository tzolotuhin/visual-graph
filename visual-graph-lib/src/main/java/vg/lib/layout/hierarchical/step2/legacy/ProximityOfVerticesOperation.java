package vg.lib.layout.hierarchical.step2.legacy;

import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.step2.legacy.data.Group;
import vg.lib.layout.hierarchical.step2.legacy.data.Row;
import vg.lib.operation.Operation;

public class ProximityOfVerticesOperation implements Operation<Long> {
    private final Row row;

    public ProximityOfVerticesOperation(Row row) {
        this.row = row;
    }

    @Override
    public Long execute() {
        long w = 0;

        w += doExecuteForGroups(row.bottomGroups);
        w += doExecuteForGroups(row.topGroups);

        //w += doExecuteForLevelGroups(Direction.BOTTOM);
        //w += doExecuteForLevelGroups(Direction.TOP);

        return w;
    }

    private long doExecuteForLevelGroups(Direction direction) {
        if (direction != Direction.BOTTOM && direction != Direction.TOP) {
            return 0;
        }

        long w = 0;
        long maxImp = -1;
        int maxCount = 0;
        boolean check = false;
        for (int order = 0; order < row.orderToIndexRef.length - 1; order++) {
            var curr = row.getVertexByOrder(order);
            var next = row.getVertexByOrder(order + 1);

            if (direction == Direction.BOTTOM) {
                if (curr.vertex.bottomLevelGroupId == next.vertex.bottomLevelGroupId && maxImp < next.vertex.bottomLevelGroupP) {
                    maxImp = next.vertex.bottomLevelGroupP;
                }
                if (maxImp < curr.vertex.bottomLevelGroupP) {
                    maxImp = curr.vertex.bottomLevelGroupP;
                }
                if (curr.vertex.bottomLevelGroupId != next.vertex.bottomLevelGroupId || order == row.orderToIndexRef.length - 2) {
                    check = true;
                }
            } else {
                if (curr.vertex.topLevelGroupId == next.vertex.topLevelGroupId && maxImp < next.vertex.topLevelGroupP) {
                    maxImp = next.vertex.topLevelGroupP;
                }
                if (maxImp < curr.vertex.topLevelGroupP) {
                    maxImp = curr.vertex.topLevelGroupP;
                }
                if (curr.vertex.topLevelGroupId != next.vertex.topLevelGroupId || order == row.orderToIndexRef.length - 2) {
                    check = true;
                }
            }

            maxCount++;

            if (!check) {
                continue;
            }

            check = false;
            if (maxCount > 1) {
                w += (maxImp + 1);
            }
            maxCount = 0;
            maxImp = -1;
        }

        return Math.max(1000 - w, 0);
    }

    private long doExecuteForGroups(Group[] groups) {
        long dw = 0;
        for (var group : groups) {
            int minGroupOrder = Integer.MAX_VALUE;
            int maxGroupOrder = Integer.MIN_VALUE;

            for (int columnIndex : group.columnIndices) {
                int order = row.getOrder(columnIndex);
                if (order < minGroupOrder) {
                    minGroupOrder = order;
                }
                if (order > maxGroupOrder) {
                    maxGroupOrder = order;
                }
            }

            if (group.columnIndices.length > 0) {
                int s = maxGroupOrder - minGroupOrder - group.columnIndices.length;
                if (s > 0) {
                    dw += s * 10L;
                }
            }
        }

        return dw > 1000 ? 1000 : dw;
    }
}
