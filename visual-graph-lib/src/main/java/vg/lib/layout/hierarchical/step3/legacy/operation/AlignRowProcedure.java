package vg.lib.layout.hierarchical.step3.legacy.operation;

import lombok.extern.slf4j.Slf4j;
import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.step3.legacy.data.Row;
import vg.lib.operation.Operation;
import vg.lib.operation.Procedure;

@Slf4j
public class AlignRowProcedure implements Procedure {
    private final Row row;
    private final Operation<Long> calcLengthOperation;

    public AlignRowProcedure(Row row, Operation<Long> calcLengthOperation) {
        this.row = row;
        this.calcLengthOperation = calcLengthOperation;
    }

    @Override
    public void execute() {
        for (int it = 0; it < 3; it++) {
            align();
        }
    }

    private void align() {
        row.resetVisited();

        long currentResult = 0;
        while (true) {
            // choose the best suitable vertex.
            var suitableBlock = row.takeSuitableBlock();

            if (suitableBlock == null) {
                break;
            }

            currentResult = calcLengthOperation.execute();

            // choose direction for pushing.
            var direction = Direction.BREAK;

            row.backup();
            boolean toRightRe = row.moveRight(suitableBlock);
            long toRightResult = toRightRe
                    ? calcLengthOperation.execute()
                    : Long.MAX_VALUE;
            row.rollback();

            row.backup();
            boolean toLeftRe = row.moveLeft(suitableBlock);
            long toLeftResult = toLeftRe
                    ? calcLengthOperation.execute()
                    : Long.MAX_VALUE;
            row.rollback();

            // TODO: то есть уменьшаем общую длину но не за счет себя.
            if (toLeftResult < currentResult) {
                direction = Direction.LEFT;
            }

            if (toRightResult < currentResult) {
                direction = Direction.RIGHT;
            }

            if (VGConfigSettings.DIAGNOSTIC_MODE) {
                log.debug("Choose direction for pushing. Pusher index = {}, direction = {}.", suitableBlock, direction);
            }

            while (true) {
                boolean pushResult = false;
                row.backup();

                switch (direction) {
                    case RIGHT:
                        pushResult = row.moveRight(suitableBlock);
                        break;
                    case LEFT:
                        pushResult = row.moveLeft(suitableBlock);
                }

                if (!pushResult) {
                    row.rollback();
                    break;
                }

                var tempResult = calcLengthOperation.execute();

                if (tempResult >= currentResult) {
                    row.rollback();
                    break;
                }

                currentResult = tempResult;
            }
        }

        // TODO: нужно будет оптимизировать этот участок кода и вынести его куда-то в отдельную операцию.
        //slidingLevel.compact();
    }

    private enum Direction {
        BREAK, LEFT, RIGHT
    }
}
