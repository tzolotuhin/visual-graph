package vg.lib.layout.hierarchical.step2.legacy;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.config.VGConfigSettings;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.layout.hierarchical.step2.legacy.data.Chain;
import vg.lib.layout.hierarchical.step2.legacy.data.CrossingReductionTable;
import vg.lib.layout.hierarchical.step2.legacy.data.Edge;
import vg.lib.layout.hierarchical.step2.legacy.data.Vertex;
import vg.lib.operation.Procedure;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class ApplyChainsProcedure implements Procedure {
    private final CrossingReductionTable table;
    private final HierarchicalGraph graph;

    private int chainId = 0;
    private ArrayList<Chain> chains;

    public ApplyChainsProcedure(CrossingReductionTable table, HierarchicalGraph graph) {
        this.table = table;
        this.graph = graph;
    }

    @Override
    public void execute() {
        chainId = 0;
        chains = new ArrayList<>();

        int edgeAmount = 0;
        var currentChains = new LinkedHashMap<Vertex, List<Edge>>();
        for (int rowIndex = 0; rowIndex < table.rows.length - 1; rowIndex++) {
            for (var edge : table.edges[rowIndex]) {
                edgeAmount++;

                var trg = edge.getTarget().vertex;

                if (edge.originalEdge.isTopBackEdge()
                        || edge.originalEdge.isBottomBackEdge()
                        || edge.originalEdge.isTopBeaconEdge()
                        || edge.originalEdge.isBottomBeaconEdge()) {
                    chains.add(new Chain(chainId++, edge));
                    continue;
                }

                if (edge.originalEdge.isTopMainBackEdge()) {
                    putOrUpdateCurrentChains(currentChains, edge);
                    continue;
                }
                if (edge.originalEdge.isMainBackEdge()) {
                    putOrUpdateCurrentChains(currentChains, edge);
                    continue;
                }
                if (edge.originalEdge.isBottomMainBackEdge()) {
                    putOrUpdateCurrentChains(currentChains, edge);
                    var chainEdges = currentChains.remove(edge.getTarget());
                    chains.add(new Chain(chainId++, chainEdges));
                    continue;
                }

                if (!currentChains.containsKey(edge.getSource())) {
                     if (isSuitable(trg)) {
                         putOrUpdateCurrentChains(currentChains, edge);
                     } else {
                         chains.add(new Chain(chainId++, edge));
                     }
                    continue;
                }

                if (isSuitable(trg)) {
                    putOrUpdateCurrentChains(currentChains, edge);
                    continue;
                }

                putOrUpdateCurrentChains(currentChains, edge);
                chains.add(new Chain(chainId++, currentChains.remove(edge.getTarget())));
            }
        }

        for (var chainEdges : currentChains.values()) {
            chains.add(new Chain(chainId++, chainEdges));
        }

        int chainEdgeAmount = 0;
        for (var chain : chains) {
            chainEdgeAmount += chain.calcLength();
        }

        table.chains = chains;

        log.debug("Calculate (and apply) chains procedure was finished. Chains: {}.", chains.size());
        if (VGConfigSettings.DIAGNOSTIC_MODE) {
            int longChainsAmount = 0;
            for (var chain : chains) {
                if (!chain.isShort()) {
                    longChainsAmount++;
                    log.debug("Long chain: {}.", chain);
                }
            }
            log.debug("Long chains amount: {}.", longChainsAmount);
        }

        Validate.isTrue(chainEdgeAmount == edgeAmount, String.format("Expected %s but found %s.", edgeAmount, chainEdgeAmount));
    }

    private void putOrUpdateCurrentChains(LinkedHashMap<Vertex, List<Edge>> currentChains, Edge edge) {
        if (currentChains.containsKey(edge.getTarget())) {
            // TODO: the case was be reproduced on arith.graphml. The example contains two edges from REAL port.
            log.error(String.format("Map currentChains contains target %s.", edge.getTarget()));
            chains.add(new Chain(chainId++, edge));
            return;
        }

        var currentChainEdges = currentChains.remove(edge.getSource());
        if (currentChainEdges == null) {
            currentChains.put(edge.getTarget(), Stream.of(edge).collect(Collectors.toList()));
            return;
        }

        currentChainEdges.add(edge);
        currentChains.put(edge.getTarget(), currentChainEdges);
    }

    private boolean isSuitable(HierarchicalVertex vertex) {
        int inputRealVertices = 0, outputRealVertices = 0;
        for (var inputVertex : graph.getVerticesByIds(vertex.getInputVertexIds())) {
            if (inputVertex.isRealVertex()) {
                inputRealVertices++;
                continue;
            }
            if (inputVertex.isFakeVertex() && !inputVertex.isBackVertex()) {
                inputRealVertices++;
            }
            if (inputVertex.isPort()) {
                inputRealVertices++;
            }
        }
        for (var outputVertex : graph.getVerticesByIds(vertex.getOutputVertexIds())) {
            if (outputVertex.isRealVertex()) {
                outputRealVertices++;
                continue;
            }
            if (outputVertex.isFakeVertex() && !outputVertex.isBackVertex()) {
                outputRealVertices++;
            }
            if (outputVertex.isPort()) {
                outputRealVertices++;
            }
        }

        return inputRealVertices <= 1 && outputRealVertices <= 1;
    }
}
