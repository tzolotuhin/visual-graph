package vg.lib.layout.hierarchical.step1;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.layout.hierarchical.step1.data.LayeringOperationResult;
import vg.lib.operation.Operation;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
public class CustomLayeringOperation extends BaseLayeringOperation implements Operation<LayeringOperationResult> {
    // map for colors, vertex index -> color.
    private static final int NO_COLOR = 0;
    private static final int COLOR_GREY = 1;
    private static final int COLOR_BLACK = 2;

    protected final int[] inputPortIndices;
    protected final int[] outputPortIndices;
    protected final int[] sortedInputPortIndices;
    protected final int[] sortedOutputPortIndices;

    protected final int[] vertexIndexToLayerBeforeMerge;
    protected final int[] vertexIndexToLayerAfterMerge;
    protected final int[] vertexIndexToLayer;

    protected final int[] vertexIndexToGroupIdBeforeMerge;
    protected final int[] vertexIndexToGroupId;

    public CustomLayeringOperation(List<UUID> vertices,
                                   List<UUID> inputPorts,
                                   List<UUID> outputPorts,
                                   Map<UUID, Map.Entry<UUID, UUID>> edgeIdToNodeIds,
                                   Map<UUID, Integer> portIdToSpecifiedOrder) {
        super(vertices, inputPorts, outputPorts, edgeIdToNodeIds, portIdToSpecifiedOrder);

        {
            var re = convertToSortedVertexIndices(inputPorts, vertexToId, vertexIdToIndex, portIdToSpecifiedOrder);
            sortedInputPortIndices = re[0];
            inputPortIndices = re[1];
        }

        {
            var re = convertToSortedVertexIndices(outputPorts, vertexToId, vertexIdToIndex, portIdToSpecifiedOrder);
            sortedOutputPortIndices = re[0];
            outputPortIndices = re[1];
        }

        vertexIndexToLayerBeforeMerge = new int[numberOfVertices];
        vertexIndexToLayerAfterMerge = new int[numberOfVertices];
        vertexIndexToLayer = new int[numberOfVertices];
        Arrays.fill(vertexIndexToLayer, -1);

        vertexIndexToGroupIdBeforeMerge = new int[numberOfVertices];
        vertexIndexToGroupId = new int[numberOfVertices];
        Arrays.fill(vertexIndexToGroupId, -1);
    }

    @Override
    public LayeringOperationResult execute() {
        // remove backward edges.
        removeCycles();

        // split vertices into groups.
        splitVerticesIntoGroups();

        // save the info for debug and unit tests.
        System.arraycopy(
                vertexIndexToGroupId, 0, vertexIndexToGroupIdBeforeMerge, 0, vertexIndexToGroupId.length);
        System.arraycopy(
                vertexIndexToLayer, 0, vertexIndexToLayerBeforeMerge, 0, vertexIndexToLayer.length);

        // merge the groups.
        mergeGroups();

        // save the info for debug and unit tests.
        System.arraycopy(
                vertexIndexToLayer, 0, vertexIndexToLayerAfterMerge, 0, vertexIndexToLayer.length);

        // normalize groups.
        normalizeGroups();

        // make group assignment.
        reGroupViaSpecifiedOrderForPorts();

        // build result.
        return new LayeringOperationResult(
            numberOfVertices,
            vertexToId,
            inputPorts,
            outputPorts,
            new boolean[numberOfVertices],
            vertexIndexToLayerBeforeMerge,
            vertexIndexToLayerAfterMerge,
            vertexIndexToLayer,
            vertexIndexToGroupIdBeforeMerge,
            vertexIndexToGroupId);
    }

    private void removeCycles() {
        boolean[] vertexIndexToIsUsed = new boolean[numberOfVertices];
        int[] _colors = new int[numberOfVertices];
        int[] _stack = new int[numberOfVertices];

        while (true) {
            int rootIndex = findNextRoot(vertexIndexToIsUsed);

            if (rootIndex < 0) {
                break;
            }

            int stackCount = 0;
            _stack[stackCount++] = rootIndex;
            while (stackCount > 0) {
                int vertexIndex = _stack[stackCount - 1];

                _colors[vertexIndex] = COLOR_GREY;
                vertexIndexToIsUsed[vertexIndex] = true;

                boolean check = true;
                for (int outputEdgeIndex : vertexIndexToOutputEdgeIndices[vertexIndex]) {
                    if (disabledEdges[outputEdgeIndex]) {
                        continue;
                    }

                    int trgVertexIndex = edgeIndexToTrgVertexIndex[outputEdgeIndex];

                    int color = _colors[trgVertexIndex];
                    if (color == NO_COLOR) {
                        _stack[stackCount++] = trgVertexIndex;
                        check = false;
                        break;
                    } else {
                        if (color == COLOR_GREY) {
                            // cycle was found, break it.
                            disabledEdges[outputEdgeIndex] = true;
                        }
                    }
                }

                if (check) {
                    _colors[_stack[--stackCount]] = COLOR_BLACK;
                }
            }
        }
    }

    private void splitVerticesIntoGroups() {
        var _stack = new int[numberOfVertices];
        var vertexIndexToIsUsed = new boolean[numberOfVertices];
        var edgeIndexToIsUsed = new boolean[numberOfEdges];
        var groupIdCounter = 0;
        while (true) {
            var rootVertexIndex = findNextRoot(vertexIndexToIsUsed);

            if (rootVertexIndex < 0) {
                break;
            }

            setLayerIfPossibleDFS(
                rootVertexIndex, groupIdCounter++, vertexIndexToIsUsed, edgeIndexToIsUsed, _stack, 0);
        }
    }

    private void mergeGroups() {
        var _stack = new int[numberOfVertices];
        var vertexIndexToIsUsed = new boolean[numberOfVertices];
        Arrays.fill(vertexIndexToIsUsed, true);

        var edgeIndexToIsUsed = new boolean[numberOfEdges];
        Arrays.fill(edgeIndexToIsUsed, true);

        var _stackOfEdgeIndices = new int[numberOfEdges];
        var stackCount = 0;

        for (var edgeIndex1 = 0; edgeIndex1 < numberOfEdges; edgeIndex1++) {
            if (disabledEdges[edgeIndex1]) {
                continue;
            }

            var srcVertexIndex1 = edgeIndexToSrcVertexIndex[edgeIndex1];
            var trgVertexIndex1 = edgeIndexToTrgVertexIndex[edgeIndex1];

            var srcVertexGroupId1 = vertexIndexToGroupId[srcVertexIndex1];
            var trgVertexGroupId1 = vertexIndexToGroupId[trgVertexIndex1];

            if (srcVertexGroupId1 == trgVertexGroupId1) {
                // the vertices already in one group or were merged in previous step.
                continue;
            }

            var maxSrcLayer = Integer.MIN_VALUE;
            var numberOfForwardEdges = 0;
            var numberOfBackwardEdges = 0;
            stackCount = 0;
            for (var edgeIndex2 = 0; edgeIndex2 < numberOfEdges; edgeIndex2++) {
                if (disabledEdges[edgeIndex2]) {
                    continue;
                }

                var srcVertexIndex2 = edgeIndexToSrcVertexIndex[edgeIndex2];
                var trgVertexIndex2 = edgeIndexToTrgVertexIndex[edgeIndex2];

                var srcVertexGroupId2 = vertexIndexToGroupId[srcVertexIndex2];
                var trgVertexGroupId2 = vertexIndexToGroupId[trgVertexIndex2];

                var srcVertexLayer2 = vertexIndexToLayer[srcVertexIndex2];

                if (srcVertexGroupId1 == srcVertexGroupId2 && trgVertexGroupId1 == trgVertexGroupId2) {
                    numberOfForwardEdges++;

                    _stackOfEdgeIndices[stackCount++] = edgeIndex2;

                    if (srcVertexLayer2 > maxSrcLayer) {
                        maxSrcLayer = srcVertexLayer2;
                    }
                }

                if (srcVertexGroupId1 == trgVertexGroupId2 && trgVertexGroupId1 == srcVertexGroupId2) {
                    numberOfBackwardEdges++;
                }
            }

            if (numberOfForwardEdges < numberOfBackwardEdges) {
                // do nothing and wait one of the backward edges and after will be merged.
                continue;
            }

            var bestShift = calcBestShift(_stackOfEdgeIndices, stackCount, maxSrcLayer);

            shiftGroup(trgVertexGroupId1, bestShift);

            updateGroupId(trgVertexGroupId1, srcVertexGroupId1);

            if (numberOfBackwardEdges > 0) {
                // try to resolve the edges before it will be merged.
                resolveBackwardEdgesAfterMerge(srcVertexGroupId1, vertexIndexToIsUsed, edgeIndexToIsUsed, _stack);
            }
        }
    }

    private void resolveBackwardEdgesAfterMerge(int groupId,
                                                boolean[] vertexIndexToIsUsed,
                                                boolean[] edgeIndexToIsUsed,
                                                int[] _stack) {
        for (var edgeIndex = 0; edgeIndex < numberOfEdges; edgeIndex++) {
            if (disabledEdges[edgeIndex]) {
                continue;
            }

            var srcVertexIndex = edgeIndexToSrcVertexIndex[edgeIndex];
            var trgVertexIndex = edgeIndexToTrgVertexIndex[edgeIndex];

            if (vertexIndexToGroupId[srcVertexIndex] != groupId
                || vertexIndexToGroupId[trgVertexIndex] != groupId) {
                continue;
            }

            if (vertexIndexToLayer[srcVertexIndex] < vertexIndexToLayer[trgVertexIndex]) {
                continue;
            }

            unsetLayerIfPossibleDFS(trgVertexIndex, groupId, vertexIndexToIsUsed, edgeIndexToIsUsed, _stack);

            var maxLayer = findMaxLayer(trgVertexIndex, groupId, edgeIndexToIsUsed);

            Validate.isTrue(maxLayer != Integer.MIN_VALUE);

            setLayerIfPossibleDFS(
                trgVertexIndex,
                groupId,
                vertexIndexToIsUsed,
                edgeIndexToIsUsed,
                _stack,
                maxLayer + 1);
        }
    }

    private int calcBestShift(int[] edgeIndices, int size, int maxSrcLayer)  {
        // any big number, it's not matter because algorithm will stop after first violation of the condition.
        var UPPER_BOUND = 250;
        var LOWER_BOUND = -1_000;

        var result = UPPER_BOUND;
        for (var shift = UPPER_BOUND; shift >= LOWER_BOUND; shift--) {
            var doesConditionViolated = false;
            for (var i = 0; i < size; i++) {
                var edgeIndex = edgeIndices[i];

                var srcVertexIndex = edgeIndexToSrcVertexIndex[edgeIndex];
                var trgVertexIndex = edgeIndexToTrgVertexIndex[edgeIndex];

                var srcVertexLayer = vertexIndexToLayer[srcVertexIndex];
                var trgVertexLayer = vertexIndexToLayer[trgVertexIndex];

                var diff = shift + trgVertexLayer - srcVertexLayer;

                if (diff <= 0) {
                    doesConditionViolated = true;
                    break;
                }
            }

            if (doesConditionViolated) {
                break;
            }

            result = shift;
        }

        return result;
    }

    // TODO: достаточно простая задача и функция, не требующая каких-то приседаний, соответственно можно ее
    //  откомментировать зачем делаем то или иное действие, чтобы потом легче в статью перекладывать.
    private void normalizeGroups() {
        var hasInputPorts = inputPortIndices.length > 0;
        var hasOutputPorts = outputPortIndices.length > 0;

        var groupIdToHasInputPorts = new boolean[numberOfVertices];

        var groupIdToNonPortMinLayer = new int[numberOfVertices];
        var groupIdToNonPortMaxLayer = new int[numberOfVertices];
        Arrays.fill(groupIdToNonPortMinLayer, Integer.MAX_VALUE);
        Arrays.fill(groupIdToNonPortMaxLayer, Integer.MIN_VALUE);
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var groupId = vertexIndexToGroupId[vertexIndex];
            var layer = vertexIndexToLayer[vertexIndex];
            if (!inputPorts[vertexIndex] && !outputPorts[vertexIndex] && layer < groupIdToNonPortMinLayer[groupId]) {
                groupIdToNonPortMinLayer[groupId] = layer;
            }

            if (!inputPorts[vertexIndex] && !outputPorts[vertexIndex] && layer > groupIdToNonPortMaxLayer[groupId]) {
                groupIdToNonPortMaxLayer[groupId] = layer;
            }

            if (inputPorts[vertexIndex]) {
                groupIdToHasInputPorts[groupId] = true;
            }
        }

        var groupIdToTopLayer = new int[numberOfVertices];
        Arrays.fill(groupIdToTopLayer, Integer.MAX_VALUE);
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var groupId = vertexIndexToGroupId[vertexIndex];

            if (inputPorts[vertexIndex]) {
                if (groupIdToNonPortMinLayer[groupId] != Integer.MAX_VALUE) {
                    vertexIndexToLayer[vertexIndex] = groupIdToNonPortMinLayer[groupId] - 1;
                } else {
                    vertexIndexToLayer[vertexIndex] = 0;
                }
            }

            if (outputPorts[vertexIndex]) {
                if (groupIdToNonPortMaxLayer[groupId] != Integer.MIN_VALUE) {
                    vertexIndexToLayer[vertexIndex] = groupIdToNonPortMaxLayer[groupId] + 1;
                } else {
                    vertexIndexToLayer[vertexIndex] = 1;
                }
            }

            if (vertexIndexToLayer[vertexIndex] < groupIdToTopLayer[groupId]) {
                groupIdToTopLayer[groupId] = vertexIndexToLayer[vertexIndex];
            }
        }

        var nonPortsMaxLayer = Integer.MIN_VALUE;
        var outputPortsLayer = 1;
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var groupId = vertexIndexToGroupId[vertexIndex];

            vertexIndexToLayer[vertexIndex] -=  groupIdToTopLayer[groupId];

            if (hasInputPorts && !groupIdToHasInputPorts[groupId]) {
                vertexIndexToLayer[vertexIndex]++;
            }

            if (outputPorts[vertexIndex] && vertexIndexToLayer[vertexIndex] > outputPortsLayer) {
                outputPortsLayer = vertexIndexToLayer[vertexIndex];
            }

            if (!inputPorts[vertexIndex]
                && !outputPorts[vertexIndex]
                && vertexIndexToLayer[vertexIndex] > nonPortsMaxLayer) {
                nonPortsMaxLayer = vertexIndexToLayer[vertexIndex];
            }
        }

        if (outputPortsLayer <= nonPortsMaxLayer) {
            outputPortsLayer = nonPortsMaxLayer + 1;
        }

        var groupIdToNumberOfEdgesBetweenInputPorts = new int[numberOfVertices];
        var groupIdToNumberOfEdgesBetweenOutputPorts = new int[numberOfVertices];
        var groupIdToMinLengthToOutputPorts = new int[numberOfVertices];
        Arrays.fill(groupIdToMinLengthToOutputPorts, Integer.MAX_VALUE);
        for (var edgeIndex = 0; edgeIndex < numberOfEdges; edgeIndex++) {
            var srcVertexIndex = edgeIndexToSrcVertexIndex[edgeIndex];
            var trgVertexIndex = edgeIndexToTrgVertexIndex[edgeIndex];

            assert vertexIndexToGroupId[srcVertexIndex] == vertexIndexToGroupId[trgVertexIndex];

            var groupId = vertexIndexToGroupId[srcVertexIndex];

            if (inputPorts[srcVertexIndex] && !outputPorts[trgVertexIndex]) {
                groupIdToNumberOfEdgesBetweenInputPorts[groupId]++;
            }
            if (!inputPorts[srcVertexIndex] && outputPorts[trgVertexIndex]) {
                groupIdToNumberOfEdgesBetweenOutputPorts[groupId]++;
            }

            if (!inputPorts[srcVertexIndex]) {
                var length = outputPortsLayer - vertexIndexToLayer[srcVertexIndex];
                if (length < groupIdToMinLengthToOutputPorts[groupId]) {
                    groupIdToMinLengthToOutputPorts[groupId] = length;
                }
            }

            if (!outputPorts[trgVertexIndex]) {
                var length = outputPortsLayer - vertexIndexToLayer[trgVertexIndex];
                if (length < groupIdToMinLengthToOutputPorts[groupId]) {
                    groupIdToMinLengthToOutputPorts[groupId] = length;
                }
            }
        }

        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var groupId = vertexIndexToGroupId[vertexIndex];

            if (!hasOutputPorts) {
                continue;
            }

            if (inputPorts[vertexIndex]) {
                continue;
            }

            if (outputPorts[vertexIndex]) {
                vertexIndexToLayer[vertexIndex] = outputPortsLayer;
                continue;
            }

            if (groupIdToNumberOfEdgesBetweenInputPorts[groupId] >= groupIdToNumberOfEdgesBetweenOutputPorts[groupId]) {
                continue;
            }

            vertexIndexToLayer[vertexIndex] += groupIdToMinLengthToOutputPorts[groupId] - 1;
        }
    }

    private void shiftGroup(int groupId, int shift) {
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            if (vertexIndexToGroupId[vertexIndex] == groupId) {
                vertexIndexToLayer[vertexIndex] += shift;
            }
        }
    }

    private void updateGroupId(int oldGroupId, int newGroupId) {
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            if (vertexIndexToGroupId[vertexIndex] == oldGroupId) {
                vertexIndexToGroupId[vertexIndex] = newGroupId;
            }
        }
    }

    /**
     * Find next root vertex index among the ports and vertices.
     */
    private int findNextRoot(boolean[] vertexIndexToIsUsed) {
        int minNumberOfInputEdgeIndices = Integer.MAX_VALUE;
        int maxNumberOfOutputEdgeIndices = Integer.MIN_VALUE;
        var moreSuitableTopVertexIndex = -1;
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            if (vertexIndexToIsUsed[vertexIndex]) {
                continue;
            }

            var numberOfInputEdgeIndices = 0;
            for (int edgeId : vertexIndexToInputEdgeIndices[vertexIndex]) {
                if (!disabledEdges[edgeId]) {
                    numberOfInputEdgeIndices++;
                }
            }

            var numberOfOutputEdgeIndices = 0;
            for (int edgeId : vertexIndexToOutputEdgeIndices[vertexIndex]) {
                if (!disabledEdges[edgeId]) {
                    numberOfOutputEdgeIndices++;
                }
            }

            if (numberOfInputEdgeIndices <= minNumberOfInputEdgeIndices) {
                if (numberOfInputEdgeIndices < minNumberOfInputEdgeIndices
                        || maxNumberOfOutputEdgeIndices < numberOfOutputEdgeIndices) {
                    minNumberOfInputEdgeIndices = numberOfInputEdgeIndices;
                    maxNumberOfOutputEdgeIndices = numberOfOutputEdgeIndices;
                    moreSuitableTopVertexIndex = vertexIndex;
                }
            }
        }

        return moreSuitableTopVertexIndex;
    }

    private void unsetLayerIfPossibleDFS(int startIndex,
                                         int groupId,
                                         boolean[] vertexIndexToIsUsed,
                                         boolean[] edgeIndexToIsUsed,
                                         int[] _stack) {
        var stackCount = 0;
        _stack[stackCount++] = startIndex;
        while (stackCount > 0) {
            var vertexIndex = _stack[--stackCount];

            vertexIndexToIsUsed[vertexIndex] = false;

            for (var outputEdgeIndex : vertexIndexToOutputEdgeIndices[vertexIndex]) {
                if (disabledEdges[outputEdgeIndex]) {
                    continue;
                }

                var trgVertexIndex = edgeIndexToTrgVertexIndex[outputEdgeIndex];

                if (vertexIndexToGroupId[trgVertexIndex] != groupId) {
                    continue;
                }

                edgeIndexToIsUsed[outputEdgeIndex] = false;

                if (!vertexIndexToIsUsed[trgVertexIndex]) {
                    continue;
                }

                _stack[stackCount++] = trgVertexIndex;
            }
        }
    }

    private void setLayerIfPossibleDFS(int startIndex,
                                       int groupId,
                                       boolean[] vertexIndexToIsUsed,
                                       boolean[] edgeIndexToIsUsed,
                                       int[] _stack,
                                       int startLayer) {
        vertexIndexToLayer[startIndex] = startLayer;

        var stackCount = 0;
        _stack[stackCount++] = startIndex;
        while (stackCount > 0) {
            var vertexIndex = _stack[--stackCount];

            vertexIndexToIsUsed[vertexIndex] = true;
            vertexIndexToGroupId[vertexIndex] = groupId;

            for (var outputEdgeIndex : vertexIndexToOutputEdgeIndices[vertexIndex]) {
                edgeIndexToIsUsed[outputEdgeIndex] = true;

                if (disabledEdges[outputEdgeIndex]) {
                    continue;
                }

                var trgVertexIndex = edgeIndexToTrgVertexIndex[outputEdgeIndex];

                if (vertexIndexToIsUsed[trgVertexIndex]) {
                    continue;
                }

                // set layer if it's possible.
                var maxLayer = findMaxLayer(trgVertexIndex, groupId, edgeIndexToIsUsed);

                if (maxLayer >= 0) {
                    vertexIndexToLayer[trgVertexIndex] = maxLayer + 1;
                    _stack[stackCount++] = trgVertexIndex;
                }
            }
        }
    }

    private int findMaxLayer(int vertexIndex, int groupId, boolean[] edgeIndexToIsUsed) {
        var maxLayer = Integer.MIN_VALUE;
        for (var inputEdgeIndex : vertexIndexToInputEdgeIndices[vertexIndex]) {
            if (disabledEdges[inputEdgeIndex] || edgeIndexToIsUsed[inputEdgeIndex]) {
                var srcVertexIndex = edgeIndexToSrcVertexIndex[inputEdgeIndex];

                var srcVertexGroupId = vertexIndexToGroupId[srcVertexIndex];
                if (groupId != srcVertexGroupId) {
                    continue;
                }

                var srcVertexLayer = vertexIndexToLayer[srcVertexIndex];
                if (srcVertexLayer > maxLayer) {
                    maxLayer = srcVertexLayer;
                }

                continue;
            }

            maxLayer = -1;
            break;
        }

        return maxLayer;
    }

    // TODO: надо подумать стоит ли сохранять группы изначальные... super group странная затея... но уж как минимум
    //  надо задокументировать что за 0 за 1 и остальные отвечает, какая философия этих groupId, практическая и
    //  эстетическая.
    private void reGroupViaSpecifiedOrderForPorts() {
        var groupIdToSuperGroupId = new int[numberOfVertices];
        Arrays.fill(groupIdToSuperGroupId, -1);

        for (var sortedInputPortIndex : sortedInputPortIndices) {
            var groupId = vertexIndexToGroupId[sortedInputPortIndex];
            groupIdToSuperGroupId[groupId] = 0;
        }

        for (var sortedOutputPortIndex : sortedOutputPortIndices) {
            var groupId = vertexIndexToGroupId[sortedOutputPortIndex];
            groupIdToSuperGroupId[groupId] = 0;
        }

        var superGroupIdCounter = 2;

        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var groupId = vertexIndexToGroupId[vertexIndex];

            if (groupIdToSuperGroupId[groupId] != -1) {
                continue;
            }

            if (inputPorts[vertexIndex] || outputPorts[vertexIndex]) {
                groupIdToSuperGroupId[groupId] = 1;
                continue;
            }

            groupIdToSuperGroupId[groupId] = superGroupIdCounter++;
        }

        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            var groupId = vertexIndexToGroupId[vertexIndex];
            vertexIndexToGroupId[vertexIndex] = groupIdToSuperGroupId[groupId];
        }
    }

    /**
     * @return
     * [
     *      [sorted elements via the specified order],
     *      [sorted elements via the specified order, another unsorted elements with initial order]
     * ]
     */
    private static int[][] convertToSortedVertexIndices(List<UUID> vertexIds,
                                                       UUID[] vertexIndexToId,
                                                       Map<UUID, Integer> vertexIdToIndex,
                                                       Map<UUID, Integer> vertexIdToSpecifiedOrder) {
        var result = new int[vertexIds.size()];
        var index = 0;
        for (var vertexId : vertexIds) {
            if (!vertexIdToSpecifiedOrder.containsKey(vertexId)) {
                continue;
            }

            var vertexIndex = vertexIdToIndex.get(vertexId);

            result[index] = vertexIndex;

            index++;
        }

        sortVertexIndexToIdViaSpecifiedOrder(result, index, vertexIndexToId, vertexIdToSpecifiedOrder);

        var sortedVertexIndices = Arrays.copyOfRange(result, 0, index);

        for (var vertexId : vertexIds) {
            if (vertexIdToSpecifiedOrder.containsKey(vertexId)) {
                continue;
            }

            var vertexIndex = vertexIdToIndex.get(vertexId);

            result[index] = vertexIndex;

            index++;
        }

        return new int[][] {sortedVertexIndices, result};
    }

    private static void sortVertexIndexToIdViaSpecifiedOrder(int[] vertexOrderToIndex,
                                                            int size,
                                                            UUID[] vertexIndexToId,
                                                            Map<UUID, Integer> vertexIdToSpecifiedOrder) {
        for (var vertexOrder1 = 0; vertexOrder1 < size; vertexOrder1++) {
            var vertexIndex1 = vertexOrderToIndex[vertexOrder1];
            var vertexId1 = vertexIndexToId[vertexIndex1];
            var specifiedVertexOrder1 = vertexIdToSpecifiedOrder.getOrDefault(vertexId1, -1);

            if (specifiedVertexOrder1 < 0) {
                continue;
            }

            for (var vertexOrder2 = vertexOrder1 + 1; vertexOrder2 < size; vertexOrder2++) {
                var vertexIndex2 = vertexOrderToIndex[vertexOrder2];
                var vertexId2 = vertexIndexToId[vertexIndex2];
                var specifiedVertexOrder2 = vertexIdToSpecifiedOrder.getOrDefault(vertexId2, -1);

                if (specifiedVertexOrder2 < 0) {
                    continue;
                }

                if (vertexOrder1 < vertexOrder2 && specifiedVertexOrder1 < specifiedVertexOrder2) {
                    continue;
                }

                if (vertexOrder2 < vertexOrder1 && specifiedVertexOrder2 < specifiedVertexOrder1) {
                    continue;
                }

                // otherwise swap the vertices.
                vertexOrderToIndex[vertexOrder1] = vertexIndex2;
                vertexOrderToIndex[vertexOrder2] = vertexIndex1;
            }
        }
    }
}
