package vg.lib.layout.hierarchical.step2.legacy;

import vg.lib.layout.hierarchical.data.Direction;
import vg.lib.layout.hierarchical.step2.legacy.data.Chain;
import vg.lib.layout.hierarchical.step2.legacy.data.Edge;
import vg.lib.layout.hierarchical.step2.legacy.data.Group;
import vg.lib.layout.hierarchical.step2.legacy.data.Row;
import vg.lib.layout.hierarchical.step2.legacy.data.Vertex;
import vg.lib.operation.Operation;

// TODO: необходимо ускорить данную операцию, и сделать более точными предсказания.
public class EstCrossesOperation implements Operation<Long> {
    private final Row row;
    private final Direction direction;

    private long w;

    public EstCrossesOperation(Row row, Direction direction) {
        this.row = row;
        this.direction = direction;
    }

    @Override
    public Long execute() {
        w = 0;

        doExecute(row.bottomGroups, direction);
        doExecute(row.topGroups, direction);

        return w;
    }

    private void doExecute(Group[] groups, Direction direction) {
        for (var group : groups) {
            int minChainOrder = Integer.MAX_VALUE;
            int maxChainOrder = Integer.MIN_VALUE;

            for (int columnIndex : group.columnIndices) {
                int order = row.getOrder(columnIndex);
                if (order < minChainOrder) {
                    minChainOrder = order;
                }
                if (order > maxChainOrder) {
                    maxChainOrder = order;
                }
            }

            if (minChainOrder == maxChainOrder) {
                continue;
            }

            for (var chain : row.chains) {
                if (chain.startRowIndex == group.parentRowIndex && chain.startColumnIndex == group.parentColumnIndex) {
                    continue;
                }
                if (chain.finishRowIndex == group.parentRowIndex && chain.finishColumnIndex == group.parentColumnIndex) {
                    continue;
                }

                if (direction == Direction.BOTTOM && row.rowIndex == group.parentRowIndex + 1) {
                    continue;
                }

                if (direction == Direction.TOP && row.rowIndex == group.parentRowIndex - 1) {
                    continue;
                }

                if (row.rowIndex < group.parentRowIndex) {
                    // handle a bottom group.
                    if (chain.finishRowIndex < group.parentRowIndex) {
                        continue;
                    }
                    if (chain.isBackChain() && group.isBack && chain.finishRowIndex == group.parentRowIndex) {
                        continue;
                    }
                    if (chain.isBackChain() && chain.startVertex.containOutput(group.parentVertex)) {
                        continue;
                    }
                } else {
                    // handle a top group.
                    if (chain.startRowIndex > group.parentRowIndex) {
                        continue;
                    }
                    if (chain.isBackChain() && group.isBack && chain.startRowIndex == group.parentRowIndex) {
                        continue;
                    }
                    if (chain.isBackChain() && chain.finishVertex.containInput(group.parentVertex)) {
                        continue;
                    }
                }

                var chainVertex = chain.getVertex(row.rowIndex);
                if (minChainOrder < chainVertex.getOrder() && chainVertex.getOrder() < maxChainOrder) {
                    w += calcCost(group, chain, chainVertex);
                    chain.visited = true;
                }
            }
        }
    }

    private long calcCost(Group group, Chain chain, Vertex chainVertex) {
        long left = 0, right = 0;
        for (int columnIndex : group.columnIndices) {
            var vertex = row.verticesRef[columnIndex];

            int w = Edge.GENERAL_CROSS_W;
            if (vertex.vertex.isBackVertex() || chain.isBackChain()) {
                w = Edge.MAIN_BACK_FAKE_CROSS_W;
            } else if (vertex.vertex.isFakeVertex() && chain.isFakeChain()) {
                w = Edge.TWO_FAKE_CROSS_W;
            } else if (vertex.vertex.isFakeVertex() || chain.isFakeChain()) {
                w = Edge.ONE_FAKE_CROSS_W;
            }

            if (row.rowIndex < group.parentRowIndex) {
                // handle a bottom group.
                if (vertex.vertex.bottomLevelGroupId != chainVertex.vertex.bottomLevelGroupId) {
                    //w = Edge.DIFF_LEVEL_GROUP_CROSS_W;
                }
            } else {
                // handle a top group.
                if (vertex.vertex.topLevelGroupId != chainVertex.vertex.topLevelGroupId) {
                   //w = Edge.DIFF_LEVEL_GROUP_CROSS_W;
                }
            }

            int order = row.getOrder(columnIndex);
            if (order < chainVertex.getOrder()) {
                left += w;
            }
            if (order > chainVertex.getOrder()) {
                right += w;
            }
        }

        return Math.min(left, right);
    }
}
