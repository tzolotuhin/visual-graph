package vg.lib.layout.hierarchical.step3.legacy;

import lombok.extern.slf4j.Slf4j;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.operation.Procedure;

@Slf4j
public class LeftAlignmentForPortsProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public LeftAlignmentForPortsProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        log.debug("Use left alignment for the input and output ports...");

        // update posX for ports.
        int startPosX = 0;
        for (var inputPort : graph.getInputPorts()) {
            inputPort.setPosX(startPosX);
            startPosX += inputPort.getWidth();
        }
        startPosX = 0;
        for (var outputPort : graph.getOutputPorts()) {
            outputPort.setPosX(startPosX);
            startPosX += outputPort.getWidth();
        }
    }
}
