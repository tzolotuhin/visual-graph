package vg.lib.layout.hierarchical.step2.legacy.data;

import vg.lib.layout.hierarchical.data.Direction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Chain {
    public final int id;
    public int startRowIndex;
    public int finishRowIndex;
    public Vertex startVertex;
    public int startColumnIndex;
    public Vertex finishVertex;
    public int finishColumnIndex;
    public List<Vertex> vertices = new ArrayList<>();
    public List<Edge> edges;

    public boolean visited;

    public Chain(int id, Edge edge) {
        this(id, Collections.singletonList(edge));
    }

    // TODO: похоже неверно считается так как if else... и finish норм не обрабатывается...
    public Chain(int id, List<Edge> edges) {
        this.id = id;
        this.edges = edges;

        startVertex = edges.get(0).getSource();
        startRowIndex = startVertex.rowIndex;
        vertices.add(startVertex);
        for (var edge : edges) {
            finishVertex = edge.getTarget();
            finishRowIndex = finishVertex.rowIndex;
            vertices.add(finishVertex);

            edge.chainId = id;
        }

        // TODO: эту часть возможно стоит вынести в другое место...
        if (startVertex.vertex.isCompanionVertex()) {
            startVertex.direction = Direction.TOP;
            for (var e : startVertex.outputs) {
                if (e.originalEdge.isTopMainBackEdge()) {
                    e.direction = Direction.BOTH;
                } else {
                    e.direction = Direction.TOP;
                }
            }
        }

        if (finishVertex.vertex.isCompanionVertex()) {
            finishVertex.direction = Direction.BOTTOM;
            for (var e : finishVertex.inputs) {
                if (e.originalEdge.isBottomMainBackEdge()) {
                    e.direction = Direction.BOTH;
                } else {
                    e.direction = Direction.BOTTOM;
                }
            }
        }

        if (!startVertex.vertex.isCompanionVertex() && !finishVertex.vertex.isCompanionVertex()){
            var direction = Direction.BOTH;
            if (startVertex.inputs.isEmpty() && startVertex.outputs.size() == 1) {
                direction = Direction.TOP;
                for (int vertexIndex = 0; vertexIndex < vertices.size() - 1; vertexIndex++) {
                    var vertex = vertices.get(vertexIndex);
                    vertex.direction = direction;
                }
            } else if (finishVertex.outputs.isEmpty() && finishVertex.inputs.size() == 1) {
                direction = Direction.BOTTOM;
                for (int vertexIndex = 1; vertexIndex < vertices.size(); vertexIndex++) {
                    var vertex = vertices.get(vertexIndex);
                    vertex.direction = direction;
                }
            }
            for (var edge : edges) {
                edge.direction = direction;
            }
        }

        for (int i = 1; i < vertices.size() - 1; i++) {
            vertices.get(i).isPartOfLongChain = true;
        }

        startColumnIndex = startVertex.columnIndex;
        finishColumnIndex = finishVertex.columnIndex;
    }

    public boolean isOnRow(int rowIndex) {
        return startRowIndex <= rowIndex && rowIndex <= finishRowIndex;
    }

    public Vertex getVertex(int rowIndex) {
        return vertices.get(rowIndex - startRowIndex);
    }

    public boolean isBackChain() {
        return startVertex.vertex.isCompanionVertex() || finishVertex.vertex.isCompanionVertex();
    }

    public boolean isFakeChain() {
        return edges.get(0).originalEdge.isFakeEdge();
    }

    public boolean isShort() {
        return finishRowIndex - startRowIndex <= 1;
    }

    public int calcLength() {
        return edges.size();
    }

    @Override
    public String toString() {
        return "{" +
                "startRowIndex=" + startRowIndex +
                ", finishRowIndex=" + finishRowIndex +
                ", startVertex=" + startVertex +
                ", finishVertex=" + finishVertex +
                '}';
    }
}
