package vg.lib.layout.hierarchical.step2.custom;

import vg.lib.operation.Operation;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeMap;

import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_BOTTOM_TO_TOP_DIRECTION;
import static vg.lib.layout.hierarchical.step2.custom.CustomConfig.FROM_TOP_TO_BOTTOM_DIRECTION;

public class CustomExtractTraps implements Operation<CustomExtractTraps.Result> {
    private final int numberOfVertices;

    private final int[][] vertexIndexToInputVertexIndices;
    private final int[][] vertexIndexToOutputVertexIndices;

    private final int[] vertexIndexToLayer;

    private final int numberOfLayers;
    private final int[][] layerToVertexIndices;

    public CustomExtractTraps(int numberOfVertices,
                              int[][] vertexIndexToInputVertexIndices,
                              int[][] vertexIndexToOutputVertexIndices,
                              int[] vertexIndexToLayer,
                              int numberOfLayers,
                              int[][] layerToVertexIndices) {
        this.numberOfVertices = numberOfVertices;

        this.vertexIndexToInputVertexIndices = vertexIndexToInputVertexIndices;
        this.vertexIndexToOutputVertexIndices = vertexIndexToOutputVertexIndices;

        this.vertexIndexToLayer = vertexIndexToLayer;

        this.numberOfLayers = numberOfLayers;
        this.layerToVertexIndices = layerToVertexIndices;
    }

    @Override
    public Result execute() {
        var topTrapVertexIndexToVertexIndex = new boolean[numberOfVertices][numberOfVertices];
        var bottomTrapVertexIndexToVertexIndex = new boolean[numberOfVertices][numberOfVertices];
        var layerToTopTrapIndexToVertexIndices = new TreeMap<Integer, TreeMap<Long, Set<Integer>>>();
        var layerToBottomTrapIndexToVertexIndices = new TreeMap<Integer, TreeMap<Long, Set<Integer>>>();
        var _stack = new int[numberOfVertices];

        for (var layer = 0; layer < numberOfLayers; layer++) {
            for (var vertexIndex : layerToVertexIndices[layer]) {
                var inputVertices = vertexIndexToInputVertexIndices[vertexIndex];

                if (inputVertices.length > 1) {
                    doPassageFromStartVertex(
                            vertexIndex,
                            bottomTrapVertexIndexToVertexIndex[vertexIndex],
                            FROM_BOTTOM_TO_TOP_DIRECTION,
                            _stack);

                    addToLayerToTrapGroupsWithVertexIndices(
                            vertexIndex,
                            bottomTrapVertexIndexToVertexIndex[vertexIndex],
                            layerToBottomTrapIndexToVertexIndices);
                }
            }
        }

        for (var layer = numberOfLayers - 1; layer >= 0; layer--) {
            for (var vertexIndex : layerToVertexIndices[layer]) {
                var outputVertices = vertexIndexToOutputVertexIndices[vertexIndex];

                if (outputVertices.length > 1) {
                    doPassageFromStartVertex(
                            vertexIndex,
                            topTrapVertexIndexToVertexIndex[vertexIndex],
                            FROM_TOP_TO_BOTTOM_DIRECTION,
                            _stack);

                    addToLayerToTrapGroupsWithVertexIndices(
                            vertexIndex,
                            topTrapVertexIndexToVertexIndex[vertexIndex],
                            layerToTopTrapIndexToVertexIndices);
                }
            }
        }

        return new Result(
                mapToLayerToTrapGroupsWithVertexIndices(layerToTopTrapIndexToVertexIndices),
                mapToLayerToTrapGroupsWithVertexIndices(layerToBottomTrapIndexToVertexIndices),
                mapToLayerToTrapVertexIndices(layerToTopTrapIndexToVertexIndices),
                mapToLayerToTrapVertexIndices(layerToBottomTrapIndexToVertexIndices)
        );
    }

    private void doPassageFromStartVertex(int startVertexIndex,
                                          boolean[] vertexIndexToIsVisited,
                                          int direction,
                                          int[] _stack) {
        //boolean excludeTrapChildren = true/false;
        var stackCounter = 0;
        _stack[stackCounter++] = startVertexIndex;
        while (stackCounter > 0) {
            var vertexIndex = _stack[--stackCounter];

            vertexIndexToIsVisited[vertexIndex] = true;

            var nextVertexIndices = vertexIndexToOutputVertexIndices[vertexIndex];
            if (direction == FROM_BOTTOM_TO_TOP_DIRECTION) {
                nextVertexIndices = vertexIndexToInputVertexIndices[vertexIndex];
            }

            //if (excludeTrapChildren && vertexIndex != startVertexIndex && nextVertexIndices.length > 1) {
            //    continue;
            //}

            for (var nextVertexIndex : nextVertexIndices) {
                if (!vertexIndexToIsVisited[nextVertexIndex]) {
                    _stack[stackCounter++] = nextVertexIndex;
                }
            }
        }
    }

    private int[][][] mapToLayerToTrapGroupsWithVertexIndices(
            TreeMap<Integer, TreeMap<Long, Set<Integer>>> layerToTrapGroupsWithVertexIndices) {
        var result = new int[numberOfLayers][][];
        for (var layer = 0; layer < numberOfLayers; layer++) {
            var trapGroupsWithVertexIndices = layerToTrapGroupsWithVertexIndices
                    .getOrDefault(layer, new TreeMap<>());

            var trapGroupsWithVertexIndicesArrayIndex = 0;
            var trapGroupsWithVertexIndicesArray = new int[trapGroupsWithVertexIndices.size()][];
            for (var topTrapGroupsWithVertexIndicesEntry : trapGroupsWithVertexIndices.entrySet()) {
                trapGroupsWithVertexIndicesArray[trapGroupsWithVertexIndicesArrayIndex] = topTrapGroupsWithVertexIndicesEntry
                        .getValue().stream()
                        .mapToInt(Integer::intValue)
                        .toArray();
                trapGroupsWithVertexIndicesArrayIndex++;
            }

            result[layer] = trapGroupsWithVertexIndicesArray;
        }

        return result;
    }

    private int[][] mapToLayerToTrapVertexIndices(TreeMap<Integer, TreeMap<Long, Set<Integer>>> layerToTrapGroupsWithVertexIndices) {
        var result = new int[numberOfLayers][];
        for (var layer = 0; layer < numberOfLayers; layer++) {
            result[layer] = layerToTrapGroupsWithVertexIndices
                    .getOrDefault(layer, new TreeMap<>())
                    .keySet().stream()
                    .mapToInt(CustomExtractTraps::fromKeyToTrapVertexIndex)
                    .toArray();
        }

        return result;
    }

    private void addToLayerToTrapGroupsWithVertexIndices(int trapVertexIndex,
                                                         boolean[] vertexIndexToIsVisited,
                                                         TreeMap<Integer, TreeMap<Long, Set<Integer>>> layerToTrapGroupsWithVertexIndices) {
        int numberOfRelatedVertices = 0;
        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            if (vertexIndexToIsVisited[vertexIndex]) {
                numberOfRelatedVertices++;
            }
        }

        for (var vertexIndex = 0; vertexIndex < numberOfVertices; vertexIndex++) {
            if (!vertexIndexToIsVisited[vertexIndex]) {
                continue;
            }

            var layer = vertexIndexToLayer[vertexIndex];

            layerToTrapGroupsWithVertexIndices.putIfAbsent(layer, new TreeMap<>());
            var trapGroupsWithVertexIndices = layerToTrapGroupsWithVertexIndices.get(layer);

            // calculate right key for sorting.
            var trapVertexLayer = vertexIndexToLayer[trapVertexIndex];
            var key = toKey(layer, trapVertexLayer, trapVertexIndex, numberOfRelatedVertices);

            trapGroupsWithVertexIndices.putIfAbsent(key, new LinkedHashSet<>());
            var trapGroupWithVertexIndices = trapGroupsWithVertexIndices.get(key);

            trapGroupWithVertexIndices.add(vertexIndex);
        }
    }

    private static long toKey(int currentLayer,
                              int trapVertexLayer,
                              int trapVertexIndex,
                              int numberOfRelatedVertices) {
       return Math.abs(trapVertexLayer - currentLayer) * 10_000_000_000L
                + numberOfRelatedVertices * 50_000L
                + trapVertexIndex;
    }

    private static int fromKeyToTrapVertexIndex(long key) {
        return (int)((key % 10_000_000_000L) % 50_000L);
    }

    public record Result(int[][][] layerToTopTrapGroupsWithVertexIndices,
                         int[][][] layerToBottomTrapGroupsWithVertexIndices,
                         int[][] layerToTopTrapVertexIndices,
                         int[][] layerToBottomTrapVertexIndices) { }
}
