package vg.lib.layout.hierarchical.step1;

import org.apache.commons.lang3.Validate;
import vg.lib.layout.hierarchical.data.HierarchicalGraph;
import vg.lib.layout.hierarchical.data.HierarchicalVertex;
import vg.lib.operation.Procedure;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

public class CalcEdgeLevelGroupProcedure implements Procedure {
    private final HierarchicalGraph graph;

    public CalcEdgeLevelGroupProcedure(HierarchicalGraph graph) {
        this.graph = graph;
    }

    @Override
    public void execute() {
        int lastLevelGroupId = 1;
        int[] levelGroupIds = new int[graph.getVertices().size()];
        Arrays.fill(levelGroupIds, -1);
        for (int rowIndex = graph.getStartLevelOfGraphWithInputPorts(); rowIndex <= graph.getFinishLevelOfGraphWithOutputPorts(); rowIndex++) {
            var row = graph.getRowByLevel(rowIndex);

            for (var vertex : row) {
                levelGroupIds[vertex.getIndex()] = -1;
            }

            for (var vertex : row) {
                if (levelGroupIds[vertex.getIndex()] != -1) {
                    continue;
                }

                int existedLevelGroupId = lastLevelGroupId++;
                levelGroupIds[vertex.getIndex()] = existedLevelGroupId;

                Collection<HierarchicalVertex> parents = Collections.singletonList(vertex);
                Collection<HierarchicalVertex> children;
                boolean check = true;
                while (check) {
                    check = false;

                    children = graph.getOutputVertices(parents).values();
                    for (var child : children) {
                        int levelGroupId = levelGroupIds[child.getIndex()];
                        if (levelGroupId == -1) {
                            check = true;
                            levelGroupIds[child.getIndex()] = existedLevelGroupId;
                        }
                    }

                    parents = graph.getInputVertices(children).values();
                    for (var parent : parents) {
                        int levelGroupId = levelGroupIds[parent.getIndex()];
                        if (levelGroupId == -1) {
                            check = true;
                            levelGroupIds[parent.getIndex()] = existedLevelGroupId;
                        }
                    }
                }
            }

            for (var edge : graph.getOutputEdges(row)) {
                var srcVertex = edge.getSrcVertex();
                var trgVertex = edge.getTrgVertex();

                int srcLevelGroupId = levelGroupIds[srcVertex.getIndex()];
                int trgLevelGroupId = levelGroupIds[trgVertex.getIndex()];
                Validate.isTrue(srcLevelGroupId == trgLevelGroupId);

                edge.levelGroupId = srcLevelGroupId;

                srcVertex.bottomLevelGroupId = edge.levelGroupId;
                trgVertex.topLevelGroupId = edge.levelGroupId;
            }
        }

        var topLevelGroupIds = new HashSet<Integer>();
        var bottomLevelGroupIds = new HashSet<Integer>();
        for (var v : graph.getVertices()) {
            if (v.topLevelGroupId == -1) {
                v.topLevelGroupId = v.bottomLevelGroupId;
            }
            if (v.bottomLevelGroupId == -1) {
                v.bottomLevelGroupId = v.topLevelGroupId;
            }

            if (v.getInputEdges().isEmpty() && v.getOutputEdges().isEmpty()) {
                Validate.isTrue(v.topLevelGroupId == -1);
                Validate.isTrue(v.bottomLevelGroupId == -1);
            } else {
                Validate.isTrue(v.topLevelGroupId != -1);
                Validate.isTrue(v.bottomLevelGroupId != -1);
            }

            topLevelGroupIds.add(v.topLevelGroupId);
            bottomLevelGroupIds.add(v.bottomLevelGroupId);
        }

        for (int rowIndex = graph.getStartLevelOfGraphWithInputPorts(); rowIndex <= graph.getFinishLevelOfGraphWithOutputPorts(); rowIndex++) {
            var row = graph.getRowByLevel(rowIndex);

            for (var levelGroupId : topLevelGroupIds) {
                Map<HierarchicalVertex, Long> unsorted = new HashMap<>();
                for (var vertex : row) {
                    if (vertex.topLevelGroupId == levelGroupId) {
                        unsorted.put(vertex, vertex.getImportance());
                    }
                }

                int index = -1;
                long lastImp = -1;
                for (var e : unsorted.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(Collectors.toList())) {
                    if (lastImp != e.getValue()) {
                        lastImp = e.getValue();
                        index++;
                    }
                    e.getKey().topLevelGroupP = index;
                }
            }

            for (var levelGroupId : bottomLevelGroupIds) {
                Map<HierarchicalVertex, Long> unsorted = new HashMap<>();
                for (var vertex : row) {
                    if (vertex.bottomLevelGroupId == levelGroupId) {
                        unsorted.put(vertex, vertex.getImportance());
                    }
                }

                int index = -1;
                long lastImp = -1;
                for (var e : unsorted.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(Collectors.toList())) {
                    if (lastImp != e.getValue()) {
                        lastImp = e.getValue();
                        index++;
                    }
                    e.getKey().bottomLevelGroupP = index;
                }
            }
        }
    }
}
