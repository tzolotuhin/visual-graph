package vg.lib.layout.hierarchical.graphics;

import vg.lib.layout.hierarchical.HierarchicalLayoutSettings;
import vg.lib.layout.hierarchical.HierarchicalPlainLayout;

import java.awt.*;
import java.util.List;

public class ForwardEdgeHandler extends EdgeHandler {
    private final int inputYMargin, outputYMargin;

    public ForwardEdgeHandler(HierarchicalPlainLayout.Result.OutputEdge outputEdge,
                              HierarchicalPlainLayout.Result.OutputVertex outputSrcVertex,
                              HierarchicalPlainLayout.Result.OutputVertex outputTrgVertex,
                              HierarchicalLayoutSettings settings) {
        super(outputEdge, outputSrcVertex, outputTrgVertex, settings);

        this.inputYMargin = settings.arrowSize + settings.baseArrow;
        this.outputYMargin = settings.arrowSize + settings.baseArrow;
    }

    public List<Point> execute() {
        calcStartEdgePoints(outputYMargin);
        calcFinishEdgePoints(inputYMargin);

        result.add(srcVertexPoint);
        result.add(srcVertexWithOutputMarginPoint);

        specifyAdditionalEdgePoints();

        result.add(trgVertexWithOutputMarginPoint);
        result.add(trgVertexPoint);

        return splineInterpolationIfNeed(result);
    }

    private void specifyAdditionalEdgePoints() {
        if (outputEdge.getAdditionalPoints().isEmpty()) {
            result.add(new Point(trgVertexWithOutputMarginPoint.x, Math.round(outputTrgVertex.getRowPosY() - inputYMargin)));
        }

        for (var additionalPoint : outputEdge.getAdditionalPoints()) {
            result.add(new Point(Math.round(additionalPoint.centerX()), additionalPoint.getRowPosY()));
            result.add(new Point(Math.round(additionalPoint.centerX()), additionalPoint.roundCenterY()));
            result.add(new Point(Math.round(additionalPoint.centerX()), additionalPoint.getPos().y + additionalPoint.getSize().y));
        }

        if (!outputEdge.getAdditionalPoints().isEmpty()) {
            result.add(new Point(trgVertexWithOutputMarginPoint.x, Math.round(outputTrgVertex.getRowPosY() - inputYMargin)));
        }
    }
}
