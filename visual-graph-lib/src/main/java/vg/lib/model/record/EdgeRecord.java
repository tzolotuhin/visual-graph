package vg.lib.model.record;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EdgeRecord implements Cloneable, Serializable, Comparable<EdgeRecord> {
	// Main data
	private int id;
    private int graphModelId;
    private int sourceId;
    private int targetId;
	
	public EdgeRecord(int id, int graphModelId, int sourceId, int targetId) {
		this.id = id;
		this.graphModelId = graphModelId;
		this.sourceId = sourceId;
		this.targetId = targetId;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EdgeRecord that = (EdgeRecord) o;

        return id == that.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public EdgeRecord clone() {
        return new EdgeRecord(id, graphModelId, sourceId, targetId);
    }

    @Override
    public int compareTo(EdgeRecord o) {
        return Integer.compare(id, o.id);
    }
}
