package vg.lib.model.record;

import java.util.Arrays;
import lombok.Getter;

public enum AttributeOwnerType {
    VERTEX(1),
    EDGE(2),
    GRAPH_MODEL(3);

    @Getter
    private final int id;

    AttributeOwnerType(int id) {
        this.id = id;
    }

    public static AttributeOwnerType valueOf(int id) {
        return Arrays.stream(values()).filter(value -> value.id == id).findFirst().orElse(null);
    }
}
