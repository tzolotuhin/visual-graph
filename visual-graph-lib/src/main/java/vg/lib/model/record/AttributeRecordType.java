package vg.lib.model.record;

import java.util.Arrays;
import lombok.Getter;

public enum AttributeRecordType {
    STRING(1),
    BOOLEAN(2),
    INTEGER(3),
    DOUBLE(4),
    UUID(5);

    @Getter
    private final int id;

    AttributeRecordType(int id) {
        this.id = id;
    }

    public static AttributeRecordType valueOf(int id) {
        return Arrays.stream(values()).filter(value -> value.id == id).findFirst().orElse(null);
    }

    public static boolean isRealType(AttributeRecordType type) {
        return (type == INTEGER || type == DOUBLE);
    }
}
