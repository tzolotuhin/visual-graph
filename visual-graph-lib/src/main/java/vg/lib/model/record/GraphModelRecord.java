package vg.lib.model.record;

import lombok.Getter;
import lombok.Setter;

public class GraphModelRecord implements Cloneable, Comparable<GraphModelRecord> {
    @Getter
    private final int id;

    @Getter @Setter
    private String name;

    public GraphModelRecord(int id) {
        this.id = id;
        this.name = null;
    }

    public GraphModelRecord(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public GraphModelRecord clone() {
        return new GraphModelRecord(id, name);
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        return o != null && (o instanceof GraphModelRecord && ((GraphModelRecord) o).id == id);
    }

    @Override
    public int compareTo(GraphModelRecord o) {
        return Integer.compare(id, o.id);
    }
}
