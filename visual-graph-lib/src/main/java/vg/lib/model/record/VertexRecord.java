package vg.lib.model.record;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

public class VertexRecord implements Cloneable, Serializable, Comparable<VertexRecord> {
    // Defines
    public final static int NO_PARENT_ID = -1;

    // Main data
    @Getter
    private final int id;
    @Getter @Setter
    private int graphModelId;
    @Getter @Setter
    private int parentId;
    @Getter @Setter
    private boolean vertexWithInnerGraph;

    public VertexRecord(int id, int graphModelId, int parentId, boolean vertexWithInnerGraph) {
        this.id = id;
        this.graphModelId = graphModelId;
        this.parentId = parentId;
        this.vertexWithInnerGraph = vertexWithInnerGraph;
    }

    public boolean isRoot() {
        return parentId == NO_PARENT_ID;
    }

    @Override
    public VertexRecord clone() {
        return new VertexRecord(id, graphModelId, parentId, vertexWithInnerGraph);
    }

    @Override
    public int compareTo(VertexRecord o) {
        return Integer.compare(id, o.id);
    }
}
