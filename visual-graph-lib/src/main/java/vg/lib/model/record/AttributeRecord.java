package vg.lib.model.record;

import java.io.Serializable;
import java.util.UUID;
import lombok.Getter;
import org.apache.commons.lang3.Validate;

public class AttributeRecord implements Cloneable, Serializable, Comparable<AttributeRecord> {
	// Main data
	private int id;

    private int ownerId;

    @Getter
    private AttributeOwnerType ownerType;

    private String name;

	private Object value;

    @Getter
    private AttributeRecordType type;

    private boolean visible;

    public AttributeRecord(int id,
                           int ownerId,
                           AttributeOwnerType ownerType,
                           String name,
                           String value,
                           AttributeRecordType type,
                           boolean visible) {
        this.id = id;
        this.ownerId = ownerId;
        this.ownerType = ownerType;
        this.name = name;
        this.visible = visible;

        setValue(value, type);
    }

    public void update(String strValue, AttributeRecordType type, boolean visible) {
        setValue(strValue, type);
        setVisible(visible);
    }

    public int getId() {
        return id;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public String getName() {
        return name;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    /**
     * Returns string value, if type is string, otherwise <b>null</b>
     */
    public String getStringValue() {
        return value.toString();
    }

    /**
     * Returns integer value, if type is integer, otherwise <b>null</b>
     */
    public Integer getIntegerValue() {
        if (type == AttributeRecordType.INTEGER) {
            return (Integer) value;
        }
        return null;
    }

    /**
     * Returns boolean value, if type is boolean, otherwise <b>null</b>
     */
    public Boolean getBooleanValue() {
        if (type == AttributeRecordType.BOOLEAN) {
            return (Boolean) value;
        }
        return null;
    }

    /**
     * Returns double value, if type is double, otherwise <b>null</b>
     */
    public Double getDoubleValue() {
        if (type == AttributeRecordType.DOUBLE) {
            return (Double) value;
        }
        return null;
    }

    /**
     * Returns double value, if type is real, otherwise <b>null</b>
     */
    public Double getRealValue() {
        if (isIntegerType())
            return ((Integer)value).doubleValue();

        if (isDoubleType())
            return (Double)value;

        return null;
    }

    public boolean isStringType() {
        return getType() == AttributeRecordType.STRING;
    }

    public boolean isBooleanType() {
        return getType() == AttributeRecordType.BOOLEAN;
    }

    public boolean isIntegerType() {
        return getType() == AttributeRecordType.INTEGER;
    }

    public boolean isDoubleType() {
        return getType() == AttributeRecordType.DOUBLE;
    }

    public boolean isRealType() {
        return isDoubleType() || isIntegerType();
    }

    public void setName(String newName) {
        Validate.notNull(newName);

        name = newName;
    }

    public void setStringValue(String newValue) {
        Validate.notNull(newValue);

        type = AttributeRecordType.STRING;
        value = newValue;
    }

    public void setBooleanValue(Boolean newValue) {
        Validate.notNull(newValue);

        type = AttributeRecordType.BOOLEAN;
        value = newValue;
    }

    public void setIntegerValue(Integer newValue) {
        Validate.notNull(newValue);

        type = AttributeRecordType.INTEGER;
        value = newValue;
    }

    public void setDoubleValue(Double newValue) {
        Validate.notNull(newValue);

        type = AttributeRecordType.DOUBLE;
        value = newValue;
    }

    public void setUUIDValue(UUID newValue) {
        Validate.notNull(newValue);

        type = AttributeRecordType.UUID;
        value = newValue;
    }

    public void setValue(String newValue, AttributeRecordType newType) {
        switch (newType) {
            case BOOLEAN:
                setBooleanValue(Boolean.valueOf(newValue));
                break;

            case DOUBLE:
                setDoubleValue(Double.valueOf(newValue));
                break;

            case INTEGER:
                setIntegerValue(Integer.valueOf(newValue));
                break;

            case UUID:
                setUUIDValue(UUID.fromString(newValue));
                break;

            case STRING:
            default:
                setStringValue(String.valueOf(newValue));
                break;
        }
    }

    @Override
	public AttributeRecord clone() {
		return new AttributeRecord(id, ownerId, ownerType, name, value.toString(), type, visible);
	}

    @Override
    public int compareTo(AttributeRecord o) {
        return Integer.compare(id, o.id);
    }
}
