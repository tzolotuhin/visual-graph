package vg.lib.model.graph;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import vg.lib.model.record.GraphModelRecord;

/**
 * It's aggregation of vertices, edges and attributes.
 * Note: this class is not thread safety.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Graph extends Vertex implements Cloneable {
    // Constants
    private static final int ANY_NEIGHBORHOOD = 0;
    private static final int SRC_NEIGHBORHOOD = 1;
    private static final int TRG_NEIGHBORHOOD = 2;

    // Main data
    // TODO: should be deleted.
    private BiMap<Vertex, Integer> vertexToId;
    private BiMap<Edge, Integer> edgeToId;
    private int vertexCounter = 0;
    private int edgeCounter = 0;

    private GraphModelRecord linkToGraphModelRecord;

    private List<Edge> edges;

    private Set<Edge> outEdges;

    public Graph() {
        this(new ArrayList<>(), new ArrayList<>(), new HashSet<>(), null, null);
    }

    public Graph(Collection<Vertex> vertices, Collection<Edge> edges) {
        this(vertices, edges, null, null, null);
    }

    public Graph(Collection<Vertex> vertices,
                 Collection<Edge> edges,
                 Set<Edge> outEdges,
                 List<Attribute> attributes) {
        this(vertices, edges, outEdges, attributes, null);
    }

    public Graph(Collection<Vertex> vertices,
                 Collection<Edge> edges,
                 Set<Edge> outEdges,
                 List<Attribute> attributes,
                 GraphModelRecord linkToGraphModelRecord) {
        vertexToId = HashBiMap.create();
        edgeToId = HashBiMap.create();
        this.vertices = Lists.newArrayList();
        this.edges = Lists.newArrayList();
        this.outEdges = new HashSet<>();
        this.attributes = Lists.newArrayList();

        if (vertices != null) {
            for (Vertex vertex : vertices) {
                insertVertex(vertex, null);
            }
        }

        if (edges != null) {
            for (Edge edge : edges) {
                addEdge(edge);
            }
        }

        if (outEdges != null) {
            this.outEdges.addAll(outEdges);
        }

        addAttributes(attributes);

        setLinkToGraphRecord(linkToGraphModelRecord);
    }

    public Integer getIdByVertex(Vertex vertex) {
        return vertexToId.get(vertex);
    }

    public Vertex getVertexById(int id) {
        return vertexToId.inverse().get(id);
    }

    public Integer getIdByEdge(Edge edge) {
        return edgeToId.get(edge);
    }

    public Edge getEdgeById(int id) {
        return edgeToId.inverse().get(id);
    }

    public GraphModelRecord getLinkToGraphRecord() {
        return linkToGraphModelRecord;
    }

    public void setLinkToGraphRecord(GraphModelRecord linkToGraphModelRecord) {
        this.linkToGraphModelRecord = linkToGraphModelRecord;
    }

    public void setVertexId(Vertex vertex, int id) {
        if (vertexToId.remove(vertex) == null) {
            throw new IllegalArgumentException("Vertex was not found");
        } else {
            vertexToId.put(vertex, id);
        }
        if (id > vertexCounter) {
            vertexCounter = id + 1;
        }
    }

    public void setEdgeId(Edge edge, int id) {
        if (edgeToId.remove(edge) == null) {
            throw new IllegalArgumentException("Edge was not found");
        }
        edgeToId.put(edge, id);
        if (id > edgeCounter) {
            edgeCounter = id + 1;
        }
    }

    @Override
    public boolean insertVertex(Vertex vertex, Vertex parent) {
        vertexToId.put(vertex, vertexCounter++);

        if (parent == null) {
            return vertices.add(vertex);
        }

        if (!super.insertVertex(vertex, parent)) {
            vertices.add(vertex);
        }

        return true;
    }

    public boolean addEdge(Edge edge) {
        edgeToId.put(edge, edgeCounter++);
        return edges.add(edge);
    }

    public void addEdges(Collection<Edge> edges) {
        edges.forEach(this::addEdge);
    }

    public Collection<Vertex> getAllVertices() {
        return vertexToId.keySet();
    }

    public Set<String> getAllVertexAttributeNames(boolean onlyVisible) {
        Set<String> attrNames = Sets.newHashSet();
        vertexToId.keySet()
                .forEach(vertex -> vertex.getAttributes()
                        .forEach(attribute -> {
                            if (!onlyVisible || attribute.isVisible()) {
                                attrNames.add(attribute.getName());
                            }
                        }));
        return attrNames;
    }

    public Set<String> getAllEdgeAttributeNames(boolean onlyVisible) {
        Set<String> attrNames = Sets.newHashSet();
        edgeToId.keySet()
                .forEach(edge -> edge.getAttributes()
                        .forEach(attribute -> {
                            if (!onlyVisible || attribute.isVisible()) {
                                attrNames.add(attribute.getName());
                            }
                        }));
        return attrNames;
    }

    public boolean isEmpty() {
        return vertices.isEmpty() && edges.isEmpty();
    }

    public Collection<Edge> getAllEdges() {
        return edges;
    }

    public Collection<Vertex> getSrcNeighborhoods(Vertex vertex) {
        return getNeighborhoods(vertex, SRC_NEIGHBORHOOD);
    }

    public Collection<Vertex> getTrgNeighborhoods(Vertex vertex) {
        return getNeighborhoods(vertex, TRG_NEIGHBORHOOD);
    }

    public Collection<Vertex> getAnyNeighborhoods(Vertex vertex) {
        return getNeighborhoods(vertex, ANY_NEIGHBORHOOD);
    }

    public int getIncomingEdgeCount(Vertex vertex) {
        return getSrcNeighborhoods(vertex).size();
    }

    public int getOutgoingEdgeCount(Vertex vertex) {
        return getTrgNeighborhoods(vertex).size();
    }

    public int getEdgeCount(Vertex vertex) {
        return getAnyNeighborhoods(vertex).size();
    }

    public Set<Edge> getEdgeNeighborhoods(Vertex vertex) {
        return getEdgeNeighborhoods(Collections.singletonList(vertex));
    }

    public Set<Edge> getEdgeNeighborhoods(Collection<Vertex> vertices) {
        if (vertices == null) {
            return Collections.emptySet();
        }

        var result = new LinkedHashSet<Edge>(edges.size());
        for (Edge edge : edges) {
            for (var vertex : vertices) {
                if (edge.getSource().hashCode() == vertex.hashCode() && edge.getSource().equals(vertex)) {
                    result.add(edge);
                }
                if (edge.getTarget().hashCode() == vertex.hashCode() && edge.getTarget().equals(vertex)) {
                    result.add(edge);
                }
            }
        }

        return result;
    }

    public boolean isAdjacentVertices(Vertex source, Vertex target) {
        return !findEdgesBetweenVertices(source, target).isEmpty();
    }

    public List<Edge> findEdgesBetweenVertices(Vertex source, Vertex target) {
        List<Edge> result = Lists.newArrayList();
        for (Edge edge : edges) {
            if (edge.getSource().hashCode() == source.hashCode() && edge.getTarget().hashCode() == target.hashCode() &&
                    edge.getSource().equals(source) && edge.getTarget().equals(target)) {
                result.add(edge);
            }

            if (source.hashCode() != target.hashCode() && !source.equals(target)) {
                if (edge.getSource().hashCode() == target.hashCode() && edge.getTarget().hashCode() == source.hashCode() &&
                        edge.getSource().equals(target) && edge.getTarget().equals(source)) {
                    result.add(edge);
                }
            }
        }

        return result;
    }

    @Override
    public Graph clone() {
        final Graph cloneGraph = new Graph();

        bfs(new GraphSearchListener() {
            @Override
            public void onVertex(Vertex vertex, Vertex parent) {
                Vertex cloneVertex = new Vertex(vertex.getAttributes(), vertex.getLinkToVertexRecord());
                Vertex cloneParent = parent;
                if (cloneParent != null) {
                    cloneParent = cloneGraph.getVertexById(getIdByVertex(parent));
                }
                cloneGraph.insertVertex(cloneVertex, cloneParent);
                cloneGraph.setVertexId(cloneVertex, getIdByVertex(vertex));
            }
        });

        for (Edge edge : edges) {
            Vertex srcCloneVertex = cloneGraph.getVertexById(getIdByVertex(edge.getSource()));
            Vertex trgCloneVertex = cloneGraph.getVertexById(getIdByVertex(edge.getTarget()));
            Edge cloneEdge = new Edge(srcCloneVertex, trgCloneVertex, edge.getAttributes(), edge.getLinkToEdgeRecord());
            cloneGraph.addEdge(cloneEdge);
            cloneGraph.setEdgeId(cloneEdge, getIdByEdge(edge));
        }

        cloneGraph.setLinkToGraphRecord(getLinkToGraphRecord());

        return cloneGraph;
    }

    public Graph getSubGraph(List<Vertex> vertices) {
        // TODO: здесь возможно требуется включать дуги между потомками...
        return new Graph(vertices, getEdgeNeighborhoods(vertices));
    }

    public void changeParent(Vertex srcParent, Vertex trgParent) {
        if (trgParent != null)
            trgParent.getChildVertices().addAll(srcParent.getChildVertices());
        else
            vertices.addAll(srcParent.getChildVertices());
        srcParent.getChildVertices().clear();
    }

    public void bfs(GraphSearchListener graphSearchListener) {
        Queue<Vertex> queue = Queues.newArrayDeque();
        queue.add(this);
        while (!queue.isEmpty()) {
            Vertex curr = queue.poll();
            for (Vertex vertex : curr.getChildVertices()) {
                queue.add(vertex);
                graphSearchListener.onVertex(vertex, curr == this ? null : curr);
            }
        }

        for (Edge edge : edges) {
            graphSearchListener.onEdge(edge);
        }
    }

    public void dfs(GraphSearchListener graphSearchListener) {
        List<Map.Entry<Vertex, Vertex>> calls = Lists.newArrayList();
        for (Vertex vertex : vertices) {
            doDFS(graphSearchListener, vertex, null, calls);
        }

        for (Map.Entry<Vertex, Vertex> call : calls) {
            graphSearchListener.onVertex(call.getKey(), call.getValue());
        }

        for (Edge edge : Lists.newArrayList(edges)) {
            graphSearchListener.onEdge(edge);
        }
    }

    public void removeVertex(Vertex vertex, Vertex parent) {
        if (parent == null) {
            vertices.remove(vertex);
        }
        if (parent != null) {
            parent.getChildVertices().remove(vertex);
        }
        vertexToId.remove(vertex);
        List<Edge> newEdges = Lists.newArrayList();
        for (Edge edge : edges) {
            if (edge.getSource() != vertex && edge.getTarget() != vertex)
                newEdges.add(edge);
            else
                edgeToId.remove(edge);
        }
        edges = newEdges;
    }

    private void doDFS(GraphSearchListener graphSearchListener, Vertex vertex, Vertex parent, List<Map.Entry<Vertex, Vertex>> calls) {
        for (Vertex child : vertex.getChildVertices()) {
            doDFS(graphSearchListener, child, vertex, calls);
        }
        calls.add(new AbstractMap.SimpleEntry<Vertex, Vertex>(vertex, parent));
    }

    /**
     * Returns neighborhoods for the vertex.
     * <p>
     * Note: doesn't return empty collection.
     */
    private Collection<Vertex> getNeighborhoods(Vertex vertex, int neighborhoodType) {
        if (vertex == null)
            return Collections.emptyList();

        List<Vertex> result = Lists.newArrayList();

        for (Edge edge : edges) {
            Vertex src = edge.getSource();
            Vertex trg = edge.getTarget();

            if (neighborhoodType == ANY_NEIGHBORHOOD || neighborhoodType == TRG_NEIGHBORHOOD) {
                if (vertex.hashCode() == src.hashCode() && vertex.equals(src)) {
                    result.add(trg);
                }
            }
            if (neighborhoodType == ANY_NEIGHBORHOOD || neighborhoodType == SRC_NEIGHBORHOOD) {
                if (vertex.hashCode() == trg.hashCode() && vertex.equals(trg)) {
                    result.add(src);
                }
            }
        }

        return result;
    }

    public static abstract class GraphSearchListener {
        public void onEdge(Edge edge) {
        }

        public void onVertex(Vertex vertex, Vertex parent) {
        }
    }
}
