package vg.lib.model.graph;

import com.google.common.collect.Lists;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import vg.lib.common.TypeComparatorUtils;
import vg.lib.model.record.AttributeRecordType;

/**
 * Adds collections attributes to class which will extended.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class AttributedItem implements Cloneable, Serializable {
    // Main data
    protected List<Attribute> attributes;

    public AttributedItem() {
        this((List<Attribute>) null);
    }

    public AttributedItem(List<Attribute> attributes) {
        if (attributes != null) {
            this.attributes = attributes;
        } else {
            this.attributes = Lists.newArrayList();
        }
    }

    public AttributedItem(AttributedItem item) {
        if (item != null) {
            attributes = Lists.newArrayList(item.attributes);
        } else {
            attributes = Lists.newArrayList();
        }
    }

    public void addAttributes(Collection<Attribute> attributes) {
        if (attributes == null || attributes.size() == 0) {
            return;
        }

        for (Attribute attr : attributes) {
            addAttribute(attr);
        }
    }

    /**
     * Remove all attributes with the passed key.
     */
    public void removeAllAttributes(String key) {
        attributes.removeIf(attribute -> Objects.equals(key, attribute.getName()));
    }

    /**
     * This method adds an attribute in the vertex.
     */
    public boolean addAttribute(String key, String value) {
        return attributes.add(new Attribute(key, value, AttributeRecordType.STRING, true));
    }

    public Attribute addAttribute(String key, Object value, AttributeRecordType type) {
        return addAttribute(key, value, type, true);
    }

    public Attribute addInvisibleAttribute(String key, Object value, AttributeRecordType type) {
        return addAttribute(key, value, type, false);
    }

    public Attribute addAttribute(String key, Object value, AttributeRecordType type, boolean visible) {
        return addAttribute(new Attribute(key, value, type, visible));
    }

    public Attribute addAttribute(Attribute attribute) {
        attributes.add(attribute);
        return attribute;
    }

    /**
     * This methods returns all attributes of this vertex.
     * <p>
     * Note: isn't null.
     */
    public List<Attribute> getAttributes() {
        return attributes;
    }

    /**
     * This method returns attribute with name = 'name', otherwise null.
     */
    public Attribute getAttribute(String name) {
        return getAttribute(name, attributes);
    }

    public static Attribute getAttribute(String name, Collection<Attribute> attributes) {
        return attributes.stream().filter(x -> Objects.equals(x.getName(), name)).findFirst().orElse(null);
    }

    /**
     * This method returns attributes with name = 'name'.
     */
    public List<Attribute> getAttributes(String name) {
        return attributes.stream().filter(x -> x.getName().equals(name)).collect(Collectors.toList());
    }

    /**
     * Returns true if the item have no attributes
     */
    public boolean isEmpty() {
        return attributes.size() == 0;
    }

    @Override
    public AttributedItem clone() {
        return new AttributedItem(this);
    }

    /**
     * Returns percentage of similarity of two items in range from 0 to 1.
     */
    public static float getSimilarityPercentage(
            AttributedItem attributeItem1,
            AttributedItem attributeItem2,
            Map<String, Float> weights,
            boolean enableStrongComparison) {
        float currResult = 0.0f;
        float maxResult = 0.0f;

        for (var weightEntry : weights.entrySet()) {
            float match = 0.0f;

            Attribute attribute1 = attributeItem1.getAttribute(weightEntry.getKey());
            Attribute attribute2 = attributeItem2.getAttribute(weightEntry.getKey());

            if (attribute1 == null && attribute2 == null) {
                // do nothing...
                continue;
            }

            if (attribute1 != null && attribute2 != null) {
                if (attribute1.isRealType() && attribute2.isRealType()) {
                    if (enableStrongComparison)
                        match = TypeComparatorUtils.strongDoubleComparison(attribute1.getRealValue(), attribute2.getRealValue());
                    else
                        match = TypeComparatorUtils.flexibleDoubleComparison(attribute1.getRealValue(), attribute2.getRealValue());
                } else if (attribute1.isStringType() && attribute2.isStringType()) {
                    if (enableStrongComparison)
                        match = TypeComparatorUtils.strongStringComparison(attribute1.getStringValue(), attribute2.getStringValue());
                    else
                        match = TypeComparatorUtils.flexibleStringComparison(attribute1.getStringValue(), attribute2.getStringValue());
                } else if (attribute1.isBooleanType() && attribute2.isBooleanType()) {
                    if (attribute1.getBooleanValue() == attribute2.getBooleanValue()) {
                        match = 1.0f;
                    }
                }
            }

            maxResult += weightEntry.getValue();
            currResult += match * weightEntry.getValue();
        }

        if (maxResult != 0) {
            currResult /= maxResult;
        } else {
            currResult = 1.0f;
        }

        if (currResult > 1.0f) {
            currResult = 1.0f;
        }

        if (currResult < 0) {
            currResult = 0.0f;
        }

        return currResult;
    }
}
