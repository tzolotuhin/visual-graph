package vg.lib.model.graph;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import org.apache.commons.lang3.Validate;
import vg.lib.model.record.AttributeRecordType;

/**
 * This class realizes something attribute.
 * Warning: No thread safety.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Attribute implements Cloneable, Serializable {
    // Main data
    private String name = null;
    private Object value = null;
    // Note: visible = true is flag for system attribute.
    private boolean visible = true;
    private AttributeRecordType type = AttributeRecordType.STRING;

    public Attribute(String name, Object value, AttributeRecordType type, boolean visible) {
        Validate.notNull(name);
        Validate.notNull(value);

        setName(name);
        setValue(value.toString(), type);
        setVisible(visible);
    }

    public Attribute(String name, String value, boolean visible) {
        this(name, value, AttributeRecordType.STRING, visible);
    }

    public Attribute(String name, Integer value) {
        this(name, value, AttributeRecordType.INTEGER, true);
    }

    public Attribute(String name, Float value) {
        this(name, value, AttributeRecordType.DOUBLE, true);
    }

    public Attribute(String name, Double value) {
        this(name, value, AttributeRecordType.DOUBLE, true);
    }

    public Attribute(String name, Boolean value) {
        this(name, value, AttributeRecordType.BOOLEAN, true);
    }

    public Attribute(String name, UUID value) {
        this(name, value, AttributeRecordType.UUID, true);
    }

    public Attribute(Attribute attribute) {
        this(attribute.name, attribute.value, attribute.getType(), attribute.isVisible());
    }

    public String getName() {
		return name;
	}

    public AttributeRecordType getType() {
        return type;
    }

    /**
     * Returns string value, if type is string, otherwise <b>null</b>
     */
    public String getStringValue() {
        return value.toString();
    }

    /**
     * Returns integer value, if type is integer, otherwise <b>null</b>
     */
    public Integer getIntegerValue() {
        if (type == AttributeRecordType.INTEGER) {
            return (Integer) value;
        }
        throw new RuntimeException("Value type isn't integer");
    }

    /**
     * Returns boolean value, if type is boolean, otherwise <b>null</b>
     */
    public Boolean getBooleanValue() {
        if (type == AttributeRecordType.BOOLEAN) {
            return (Boolean) value;
        }
        throw new RuntimeException("Value type isn't boolean");
    }

    /**
     * Returns double value, if type is double, otherwise <b>null</b>
     */
    public Double getDoubleValue() {
        if (type == AttributeRecordType.DOUBLE) {
            return (Double) value;
        }
        throw new RuntimeException("Value type isn't double");
    }

    public Double getRealValue() {
        if (isIntegerType()) {
            return ((Integer) value).doubleValue();
        }

        if (isDoubleType())
            return ((Double)value);
        throw new RuntimeException("Value type isn't real");
    }

    public UUID getUUIDValue() {
        if (type == AttributeRecordType.UUID) {
            return (UUID)value;
        }
        throw new RuntimeException("Type of value should be uuid.");
    }

    public boolean isStringType() {
        return getType() == AttributeRecordType.STRING;
    }

    public boolean isBooleanType() {
        return getType() == AttributeRecordType.BOOLEAN;
    }

    public boolean isIntegerType() {
        return getType() == AttributeRecordType.INTEGER;
    }

    public boolean isDoubleType() {
        return getType() == AttributeRecordType.DOUBLE;
    }

    public boolean isUUIDType() {
        return getType() == AttributeRecordType.UUID;
    }

    public void setName(String name) {
        Validate.notNull(name);
        this.name = name;
    }

    public boolean isRealType() {
        return isDoubleType() || isIntegerType();
    }

    public void setStringValue(String value) {
        Validate.notNull(value);
        type = AttributeRecordType.STRING;
        this.value = value;
    }

    public void setBooleanValue(Boolean value) {
        Validate.notNull(value);
        type = AttributeRecordType.BOOLEAN;
        this.value = value;
    }

    public void setIntegerValue(String value) {
        Validate.notNull(value);
        type = AttributeRecordType.INTEGER;
        this.value = Integer.parseInt(value);
    }

    public void setIntegerValue(Integer value) {
        Validate.notNull(value);
        type = AttributeRecordType.INTEGER;
        this.value = value;
    }

    public void setUUIDValue(UUID value) {
        Validate.notNull(value);
        type = AttributeRecordType.UUID;
        this.value = value;
    }

    public void setDoubleValue(Double value) {
        Validate.notNull(value);
        type = AttributeRecordType.DOUBLE;
        this.value = value;
    }

    public void setValue(String strValue, AttributeRecordType type) {
        switch (type) {
            case BOOLEAN:
                setBooleanValue(Boolean.valueOf(strValue));
                break;
            case DOUBLE:
                setDoubleValue(Double.valueOf(strValue));
                break;
            case INTEGER:
                setIntegerValue(Integer.valueOf(strValue));
                break;
            case STRING:
                setStringValue(String.valueOf(strValue));
                break;
            case UUID:
                setUUIDValue(UUID.fromString(strValue));
                break;
            default:
                throw new IllegalArgumentException("Unknown attribute type");
        }
    }

    /**
     * Returns real value or null otherwise.
     */
    public Double castValueToReal() {
        if (isRealType())
            return getRealValue();

        try {
            return Double.valueOf(getStringValue());
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public Attribute clone() {
		return new Attribute(this);
	}

    @Override
    public String toString() {
        return "name = " + name + ", value = " + value + ", visible = " + visible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attribute attribute = (Attribute) o;

        return Objects.equals(name, attribute.name);
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
