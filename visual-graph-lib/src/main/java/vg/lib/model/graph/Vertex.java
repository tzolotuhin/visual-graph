package vg.lib.model.graph;

import com.google.common.collect.Lists;
import java.util.Collections;
import java.util.List;
import vg.lib.model.record.VertexRecord;


/**
 * It's aggregation of vertices and attributes.
 * Note: this class is not thread safety.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Vertex extends AttributedItem {
    // Main data
    protected List<Vertex> vertices = Lists.newArrayList();
    private VertexRecord linkToVertexRecord;

    public Vertex() { }

    public Vertex(List<Attribute> attributes) {
        this(attributes, null);
    }

    public Vertex(List<Attribute> attributes, VertexRecord vertexRecord) {
        super(attributes);
        setLinkToVertexRecord(vertexRecord);
    }

    public Vertex(AttributedItem attributedItem, VertexRecord vertexRecord) {
        super(attributedItem);
        setLinkToVertexRecord(vertexRecord);
    }

    public Vertex(AttributedItem attributedItem) {
        this(attributedItem, null);
    }

    protected boolean insertVertex(Vertex vertex, Vertex parent) {
        if (this == parent || parent == null) {
            return vertices.add(vertex);
        }
        for (Vertex bufVertex : vertices) {
            if (bufVertex.insertVertex(vertex, parent))
                return true;
        }
        return false;
    }

    public Vertex cloneWithoutChildren() {
        return new Vertex(this, linkToVertexRecord);
    }

    public VertexRecord getLinkToVertexRecord() {
        return linkToVertexRecord;
    }

    public void setLinkToVertexRecord(VertexRecord linkToVertexRecord) {
        this.linkToVertexRecord = linkToVertexRecord;
    }

    public List<Vertex> getChildVertices() {
        return Collections.unmodifiableList(vertices);
    }

    public boolean hasChildren() {
        return vertices.size() != 0;
    }
}
