CREATE TABLE IF NOT EXISTS public.user
(
    id            SERIAL       NOT NULL PRIMARY KEY,
    login         VARCHAR(255) NOT NULL UNIQUE,
    pass_hash     VARCHAR(255) NOT NULL,
    authorities   VARCHAR(255) NOT NULL,
    first_name    VARCHAR(255) NOT NULL,
    last_name     VARCHAR(255) NOT NULL,
    deleted       BOOLEAN      NOT NULL
);
