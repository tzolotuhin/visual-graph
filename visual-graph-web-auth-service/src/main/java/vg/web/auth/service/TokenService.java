package vg.web.auth.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vg.web.auth.model.AuthTokens;

@Service
@RequiredArgsConstructor
public class TokenService {
    private final JwtService jwtService;

    public AuthTokens create(String email, String password) {
        var accessToken = jwtService.generate(email, "ACCESS");
        var refreshToken = jwtService.generate(email, "REFRESH");

        return AuthTokens.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
    }
}
