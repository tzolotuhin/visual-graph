package vg.web.auth.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import vg.web.auth.model.Authorities;
import vg.web.auth.model.User;
import vg.web.auth.repo.entity.UserEntity;
import vg.web.auth.repo.jpa.UserRepository;
import vg.web.auth.service.mapper.UserRepoMapper;

@Service
@RequiredArgsConstructor
public class UserService {
    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final UserRepoMapper userRepoMapper;

    public User create(String login,
                       String password,
                       String firstName,
                       String lastName) {
        var passwordHash = passwordEncoder.encode(password);

        var userEntity = UserEntity.builder()
                .login(login)
                .passwordHash(passwordHash)
                .firstName(firstName)
                .lastName(lastName)
                .authorities(Authorities.ALL_AUTHORITIES)
                .deleted(false)
                .build();

        var result = userRepository.save(userEntity);

        return userRepoMapper.toUser(result);
    }
}
