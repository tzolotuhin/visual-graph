package vg.web.auth.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import vg.web.auth.model.Authorities;
import vg.web.auth.model.Authority;
import vg.web.auth.model.User;
import vg.web.auth.repo.entity.UserEntity;

import java.util.Set;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface UserRepoMapper {
    @Mapping(target = "enabled", source = "src.deleted", qualifiedByName = "toEnabled")
    @Mapping(target = "authorities", source = "authorities", qualifiedByName = "toAuthorities")
    User toUser(UserEntity src);

    @Named("toAuthorities")
    default Set<Authority> toAuthorities(String src) {
        // TODO: need to implement split.
        return Set.of(Authority.builder().authority(Authorities.ALL_AUTHORITIES).build());
    }

    @Named("toEnabled")
    default boolean toEnabled(Boolean src) {
        if (src == null) {
            return false;
        }

        return !src;
    }
}
