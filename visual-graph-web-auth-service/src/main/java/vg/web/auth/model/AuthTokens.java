package vg.web.auth.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AuthTokens {
    private String accessToken;
    private String refreshToken;
}
