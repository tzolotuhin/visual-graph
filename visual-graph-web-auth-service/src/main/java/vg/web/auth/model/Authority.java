package vg.web.auth.model;

import lombok.Builder;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@Getter
@Builder
public class Authority implements GrantedAuthority {
    private String authority;
}
