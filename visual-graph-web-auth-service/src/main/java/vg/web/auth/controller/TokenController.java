package vg.web.auth.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vg.web.auth.controller.data.CreateTokenRequest;
import vg.web.auth.controller.data.CreateTokenResponse;
import vg.web.auth.controller.mapper.TokenRestMapper;
import vg.web.auth.service.TokenService;

@RestController
@RequestMapping(value = "v1/token")
@RequiredArgsConstructor
public class TokenController {
    private final TokenService tokenService;

    private final TokenRestMapper tokenRestMapper;

    @PostMapping(value = "/create")
    public ResponseEntity<CreateTokenResponse> createToken(@RequestBody CreateTokenRequest createTokenRequest) {
        var result = tokenService.create(createTokenRequest.getLogin(), createTokenRequest.getPassword());

        return ResponseEntity.ok(tokenRestMapper.toCreateUserResponse(result));
    }
}
