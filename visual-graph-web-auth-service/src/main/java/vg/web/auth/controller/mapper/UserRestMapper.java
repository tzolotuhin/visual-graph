package vg.web.auth.controller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import vg.web.auth.controller.data.CreateUserResponse;
import vg.web.auth.model.User;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface UserRestMapper {
    CreateUserResponse toCreateUserResponse(User src);
}
