package vg.web.auth.controller.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateUserRequest {
    private String login;

    private String password;

    private String firstName;

    private String lastName;
}
