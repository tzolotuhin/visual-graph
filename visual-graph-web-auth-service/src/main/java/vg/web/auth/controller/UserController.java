package vg.web.auth.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vg.web.auth.controller.data.CreateUserRequest;
import vg.web.auth.controller.data.CreateUserResponse;
import vg.web.auth.controller.mapper.UserRestMapper;
import vg.web.auth.service.UserService;

@RestController
@RequestMapping(value = "v1/user")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    private final UserRestMapper userRestMapper;

    @PostMapping("create")
    public ResponseEntity<CreateUserResponse> createUser(@RequestBody CreateUserRequest request) {
        var user = userService
                .create(request.getLogin(), request.getPassword(), request.getFirstName(), request.getLastName());

        var result = userRestMapper.toCreateUserResponse(user);

        return ResponseEntity.ok(result);
    }
}
