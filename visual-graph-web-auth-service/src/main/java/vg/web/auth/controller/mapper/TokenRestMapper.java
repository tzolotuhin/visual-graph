package vg.web.auth.controller.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import vg.web.auth.controller.data.CreateTokenResponse;
import vg.web.auth.model.AuthTokens;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface TokenRestMapper {
    CreateTokenResponse toCreateUserResponse(AuthTokens src);
}
