package vg.web.auth.config;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.converter.RsaKeyConverters;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.ResourceUtils;

import java.io.ByteArrayInputStream;
import java.nio.file.Files;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@Configuration
public class SecurityConfig {
    @Value("${auth.jwt.private-key}")
    private String rsaPrivateKeyPath;

    @Value("${auth.jwt.public-key}")
    private String rsaPublicKeyPath;

    /**
     * Disable security for all requests.
     */
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return web -> web.ignoring().requestMatchers("/**");
    }

    @Bean
    @SneakyThrows
    public RSAPrivateKey rsaPrivateKey () {
        var rsaPrivateKeyFile = ResourceUtils.getFile(rsaPrivateKeyPath);
        var rsaPrivateKeyBytes = Files.readAllBytes(rsaPrivateKeyFile.toPath());
        return RsaKeyConverters.pkcs8().convert(new ByteArrayInputStream(rsaPrivateKeyBytes));
    }

    @Bean
    @SneakyThrows
    public RSAPublicKey rsaPublicKey () {
        var rsaPublicKeyFile = ResourceUtils.getFile(rsaPublicKeyPath);
        var rsaPublicKeyBytes = Files.readAllBytes(rsaPublicKeyFile.toPath());
        return RsaKeyConverters.x509().convert(new ByteArrayInputStream(rsaPublicKeyBytes));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
