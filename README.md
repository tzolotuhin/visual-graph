**Visual Graph Desktop** is system for drawing hierarchical attributed graphs on the plane and searching different 
information in it.
For that user may use wide set of tools: navigator, attribute manager, mini map, graph viewer, graph matching, path searcher,
cycle searcher and other.

Visual Graph Desktop has wide set of layout for graphs: hierarchical layout (main layout for the program), circle 
layout, organic layout,
fast organic layout, layered layout and other.

Key application area: graphs in the compiler (control flow graph, syntax trees, call graphs).

The program supports the following popular graph data formats: dot (gz), graphml, gml.

Analogs: yEd, aiSee, Cytoscape, Gephi

Main window of Visual Graph Desktop:
![vg_main_window_1.5.4.png](https://bitbucket.org/repo/5Mxb6L/images/3153984640-vg_main_window_1.5.4.png)

Graph matching:
![vg_graph_matching_1.5.4.png](https://bitbucket.org/repo/5Mxb6L/images/2071791486-vg_graph_matching_1.5.4.png)

# Visual Graph Web Service

## Building

### How to push new visual graph web service image to the Docker Hub
```
./gradlew jib
```

### Files:
* .env - file with all constants which should be used in application.properties files.
* gradle.properties - file with constants which should be used by the gradle scripts. It contains versions, 
  credentials or something else.

### How to build local version

Use *jibDockerBuild* task for building local images and pushing to local docker registry.

## How to launch Visual Graph Web Service locally

Use docker compose tool with following files:
* docker-compose-vg-web.yml - for launching all services.
* docker-compose-elk.yml - for launching ELK services.

Note: if you need to launch some service from IDE, please make sure that 
following lines were added to corresponding files:
```
# Visual Graph Development:
127.0.0.1	visual-graph-web-psql-container
127.0.0.1	visual-graph-web-discovery-service-container
127.0.0.1	visual-graph-web-auth-service-container
127.0.0.1	visual-graph-web-gateway-service-container
127.0.0.1	visual-graph-web-api-service-container
127.0.0.1	visual-graph-web-elasticsearch
```
* Linux - /etc/hosts
* Windows - C:/Windows/System32/Drivers/etc/hosts

### How to check Visual Graph Web Service is up

`http://localhost:8090/api/public/health-check`
`http://localhost:8091/api/public/health-check`

### How to launch a specific service
```
docker compose -f ./docker-compose-vg-web.yml up service-name -d --build
```

## How to launch one microservice in standalone mode
Use local and standalone profiles.


## How to launch Visual Graph Web Service on remote server
1. On remote server execute following command in `/srv` directory:
    ```
    git clone https://tzolotuhin@bitbucket.org/tzolotuhin/visual-graph.git
    ```
2. Generate private and public keys for JWT authentication. 
3. Generate key store and certificate for SSL.
4. Edit .env file.
5. To make docker login for access to Docker Hub `tzolotuhin/visual-graph-repo`

### How to generate private and public keys for JWT authentication:
Execute the following commands in `./docker/auth` directory:
```
openssl genrsa -out keypair.pem 2048
openssl rsa -in keypair.pem -pubout -out rsa.public.key
openssl pkcs8 -topk8 -inform PEM -outform PEM -nocrypt -in keypair.pem -out rsa.private.key
```

### How to generate key store and self-signed certificate for SSL:
```
keytool -genkey -alias vg-alias -keystore ./vg-keystore.pfx -storetype PKCS12 -keyalg RSA -storepass vgpass -validity 3650 -keysize 4096

Example of requisites:
Generating 4,096 bit RSA key pair and self-signed certificate (SHA384withRSA) with a validity of 3,650 days
	for: CN=VG Adminisitrator, OU=Visual Graph Web Service, O=Visual Graph, L=Novosibirsk, ST=Novosibirsk Region, C=RU
```

### Launch ELK stack
```
docker compose -f ./elk-docker-compose.yml up --build
```

### Launch Visual Graph Web Service
```
docker compose --env-file=.prod_env -f ./prod-docker-compose.yml up --build
```

# FAQ
### How to run docker without sudo
https://docs.docker.com/engine/install/linux-postinstall/

### How to check elk indices
```
curl 'visual-graph-web-elasticsearch:8097/_cat/indices?v' -u elastic:vgpass
```

# Contacts
Mail: **tzolotuhin@gmail.com**
