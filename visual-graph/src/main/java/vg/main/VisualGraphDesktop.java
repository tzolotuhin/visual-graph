package vg.main;

import com.google.common.collect.Lists;
import vg.plugin.analyzer.GraphAnalyzerPlugin;
import vg.service.main.MainService;
import vg.plugin.attribute.AttributeManagerPlugin;
import vg.plugin.gcomparator.GraphComparatorPlugin;
import vg.plugin.gview.*;
import vg.plugin.help.about.AboutPlugin;
import vg.plugin.glayout.GraphLayoutPlugin;
import vg.plugin.navigator.NavigatorPlugin;
import vg.plugin.opener.GraphOpenerPlugin;
import vg.plugin.skin.SkinPlugin;
import vg.service.plugin.Plugin;

import java.util.List;

/**
 * Entry point.
 * FAQ: https://en.wikipedia.org/wiki/Graph_(discrete_mathematics)
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class VisualGraphDesktop {
    public static void main(String[] args) {
        // initialize the services.
        MainService.executeInitialPreparations(args);

        // start ui of the program.
        MainService.logger.printInfo("Start program");
        MainService.userInterfaceService.start();

        // add plugins to list for launch.
        final List<Plugin> plugins = Lists.newArrayList();
        plugins.add(new SkinPlugin());
        plugins.add(new GraphOpenerPlugin());
        plugins.add(new GraphLayoutPlugin());
        plugins.add(new SaveToImagePlugin());
        plugins.add(new AttributeManagerPlugin());
        plugins.add(new ZoomPlugin());
        plugins.add(new AboutPlugin());
        plugins.add(new NavigatorPlugin());
        //plugins.add(new PathsSearcherPlugin());
        plugins.add(new GraphComparatorPlugin());
        plugins.add(new GraphAnalyzerPlugin());

        // install & launch plugins.
        MainService.executorService.executeInEDT(() -> {
            for (Plugin plugin : plugins) {
                try {
                    plugin.install();
                } catch (Throwable ex) {
                    MainService.logger.printError(ex.getMessage(), ex);
                }
            }
        });
    }
}
