package vg.service.progress;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface IProgressService {
    void addTask(IProgressTask task);

    void removeTask(IProgressTask task);

    double updateProgress();

    int getTaskCount();

    String getTaskName();

    interface IProgressTask {
        long getValue();

        long getLength();

        String getTaskName();
    }
}
