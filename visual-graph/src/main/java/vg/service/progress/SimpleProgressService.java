package vg.service.progress;

import java.util.LinkedList;

public class SimpleProgressService implements IProgressService {
    private final LinkedList<IProgressTask> tasks = new LinkedList<>();

    public void addTask(IProgressTask task) {
        synchronized (tasks) {
            tasks.addLast(task);
        }
    }

    public void removeTask(IProgressTask task) {
        synchronized (tasks) {
            tasks.remove(task);
        }
    }

    public double updateProgress() {
        synchronized (tasks) {
            if (!tasks.isEmpty()) {
                IProgressTask task = tasks.getFirst();
                if (task.getValue() == task.getLength()) removeTask(task);
                return 100.0 * task.getValue() / task.getLength();
            }
            return 100;
        }
    }

    public String getTaskName() {
        synchronized (tasks) {
            if (!tasks.isEmpty()) {
                return tasks.getFirst().getTaskName();
            }
            return "";
        }
    }

    @Override
    public int getTaskCount() {
        synchronized (tasks) {
            return tasks.size();
        }
    }
}
