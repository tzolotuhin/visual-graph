package vg.service.config;

import org.apache.commons.cli.Option;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public abstract class CmdLineOption extends Option {
    protected String[] values;

    public CmdLineOption(String opt, String description) throws IllegalArgumentException {
        super(opt, description);
    }

    public CmdLineOption(String opt, boolean hasArg, String description) throws IllegalArgumentException {
        super(opt, hasArg, description);
    }

    public CmdLineOption(String opt, String longOpt, boolean hasArg, String description) throws IllegalArgumentException {
        super(opt, longOpt, hasArg, description);
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public String[] getDefaultValues() {
        return new String[0];
    }

    public String getAnyOpt() {
        if (getLongOpt() != null)
            return getLongOpt();
        return getOpt();
    }
}
