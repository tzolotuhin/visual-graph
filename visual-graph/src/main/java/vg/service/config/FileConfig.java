package vg.service.config;

import org.apache.commons.io.FileUtils;
import vg.service.main.MainService;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Singleton class for keeping decoders (or plugins) properties.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class FileConfig implements IConfig {
    // Constants
    private static final boolean DEBUG_MODE = false;

    private static final String CONFIG_FILE_NAME = "config.ini";

    // Main data
    private Properties prop;
    private File configFile;
    private int behavior;

    public FileConfig() {
        configFile = new File(MainService.getWorkingDirectory() + CONFIG_FILE_NAME);

        // development mode: copy config.ini from package directory.
        if (!configFile.exists()) {
            try {
                FileUtils.copyFileToDirectory(
                        new File(MainService.getWorkingDirectory() + "package" + File.separator + CONFIG_FILE_NAME),
                        new File(MainService.getWorkingDirectory()));
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        behavior = SAVE_WHEN_EDIT_BEHAVIOR;

        // read properties from config.ini file.
        FileInputStream in = null;
		try {
			in = new FileInputStream(configFile);
			prop = new Properties();
			prop.load(in);
		} catch (IOException ex) {
			prop = new Properties();
            MainService.logger.printError("Couldn't open file " + configFile + ".");
            MainService.logger.printError("Try to create new config file.");
			try {
				if(configFile.createNewFile()) {
                    MainService.logger.printError("Creating of config file. Successfully.");
				} else {
                    MainService.logger.printError("Creating of config file. Fail.");
				}
			} catch (IOException e) {
                MainService.logger.printDebug("Please, ignore this exception.", ex);
			}
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch (IOException ex) {
                    MainService.logger.printDebug("Please, ignore this exception.", ex);
                }
			}
		}
		
		//finalize of service
		Runtime.getRuntime().addShutdownHook(new Thread(this::save));
	}

    @Override
    public synchronized String getStringProperty(String key) {
        return prop.getProperty(key);
    }

    @Override
    public synchronized String getStringProperty(String key, String defaultValue) {
        String value = getStringProperty(key);

        if (value == null) {
            setProperty(key, defaultValue);
            return defaultValue;
        }

        return value;
    }

    @Override
    public synchronized boolean getBooleanProperty(String key) {
        String value = getStringProperty(key);

        if (value != null) {
            return Boolean.valueOf(value);
        } else {
            throw new NullPointerException("Element with key" + key + " was not found.");
        }
    }

    @Override
    public synchronized boolean getBooleanProperty(String key, boolean defaultValue) {
        try {
            return getBooleanProperty(key);
        } catch (Throwable ex) {
            setProperty(key, Boolean.toString(defaultValue));
        }

        return defaultValue;
    }

    @Override
    public synchronized int getIntegerProperty(String key) {
        String value = getStringProperty(key);

        if (value != null) {
            return Integer.valueOf(value);
        } else {
            throw new NullPointerException("Element with key" + key + " was not found.");
        }
    }

    @Override
    public synchronized int getIntegerProperty(String key, int defaultValue) {
        try {
            return getIntegerProperty(key);
        } catch (Throwable ex) {
            setProperty(key, Float.toString(defaultValue));
        }

        return defaultValue;
    }

    @Override
    public synchronized float getFloatProperty(String key) {
        String value = getStringProperty(key);

        if (value != null) {
            return Float.valueOf(value);
        } else {
            throw new NullPointerException("Element with key" + key + " was not found.");
        }
    }

    @Override
    public synchronized float getFloatProperty(String key, float defaultValue) {
        try {
            return getFloatProperty(key);
        } catch (Throwable ex) {
            setProperty(key, Float.toString(defaultValue));
        }

        return defaultValue;
    }

    @Override
    public synchronized double getDoubleProperty(String key) {
        String value = getStringProperty(key);

        if (value != null) {
            return Double.valueOf(value);
        } else {
            throw new NullPointerException("Element with key" + key + " was not found.");
        }
    }

    @Override
    public synchronized double getDoubleProperty(String key, double defaultValue) {
        try {
            return getFloatProperty(key);
        } catch (Throwable ex) {
            setProperty(key, Double.toString(defaultValue));
        }

        return defaultValue;
    }

    @Override
    public synchronized void setProperty(String key, String value) {
        prop.setProperty(key, value);
        if (behavior == SAVE_WHEN_EDIT_BEHAVIOR)
            save();
	}

    @Override
    public synchronized void setBooleanProperty(String key, boolean value) {
        setProperty(key, Boolean.toString(value));
    }

    @Override
    public synchronized void setIntegerProperty(String key, int value) {
        setProperty(key, Integer.toString(value));
    }

    @Override
    public synchronized void setDoubleProperty(String key, double value) {
        setProperty(key, Double.toString(value));
    }

    @Override
    public synchronized void save() {
        try (OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream(configFile), StandardCharsets.UTF_8)){
            List<String> keys = new ArrayList<>();
            for (Object key : prop.keySet()) {
                if (key != null)
                    keys.add(key.toString());
            }

            String[] arrayKeys = new String[keys.size()];
            keys.toArray(arrayKeys);
            Arrays.sort(arrayKeys);

            String lineSeparator = System.getProperty("line.separator");

            for (String key : arrayKeys) {
                Object value = prop.get(key);

                if (value != null) {
                    String outStr = (key + "=" + value.toString()).replace("\\", "\\\\");

                    out.write(outStr + lineSeparator);
                }
            }
		} catch (Throwable ex) {
			System.err.println(ex.getMessage());
            throw new RuntimeException(ex);
		}
	}

    @Override
    public synchronized void setSaveBehavior(int behavior) {
        this.behavior = behavior;
    }

    @Override
    public synchronized int getSaveBehavior() {
        return behavior;
    }

    @Override
    public boolean isDebugMode() {
        return DEBUG_MODE;
    }
}
