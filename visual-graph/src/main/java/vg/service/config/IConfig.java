package vg.service.config;

/**
 * Interface for all configuration classes in this decoders.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface IConfig {
    // Constants
    public static final int SAVE_WHEN_EDIT_BEHAVIOR = 1;
    public static final int SAVE_WHEN_EXIT_BEHAVIOR = 2;

    boolean getBooleanProperty(String key);
    boolean getBooleanProperty(String key, boolean defaultValue);
    int getIntegerProperty(String key);
    int getIntegerProperty(String key, int defaultValue);
    float getFloatProperty(String key);
    float getFloatProperty(String key, float defaultValue);
    double getDoubleProperty(String key);
    double getDoubleProperty(String key, double defaultValue);
    String getStringProperty(String key);
    String getStringProperty(String key, String defaultValue);

	void setProperty(String key, String value);
    void setBooleanProperty(String key, boolean value);
    void setIntegerProperty(String key, int value);
    void setDoubleProperty(String key, double value);

	void save();
    void setSaveBehavior(int behavior);
    int getSaveBehavior();

    boolean isDebugMode();
}
