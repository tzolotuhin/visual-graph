package vg.service.gdecoder.decoders.zip;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import vg.lib.decoder.graphml.GraphMLDecoder;
import vg.lib.storage.GraphStorage;
import vg.plugin.opener.GraphOpenerGlobals;
import vg.service.main.MainService;
import vg.shared.utils.UnzipUtils;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class ZipDecoder extends GraphMLDecoder {
    public ZipDecoder(InputStream inputStream, String graphName, GraphStorage graphStorage) {
        super(inputStream, graphName, graphStorage);
    }

    @Override
    public int decode() throws GraphDecoderException {
        File tmpDir;
        try {
            tmpDir = Files.createTempDirectory("vg").toFile();
        } catch (IOException ex) {
            throw new GraphDecoderException(ex);
        }

        try {
            UnzipUtils.unzip(inputStream, tmpDir);

            var tmpDirFiles = tmpDir.list();
            Validate.notNull(tmpDirFiles);

            for (String fileName : tmpDirFiles) {
                if (fileName.endsWith(GraphOpenerGlobals.GRAPHML_FORMAT)) {
                    File graphmlFile = new File(tmpDir.getAbsolutePath() + File.separator + fileName);
                    return MainService.graphDecoderService.openFile(graphmlFile);
                }
                if (fileName.endsWith(GraphOpenerGlobals.GML_FORMAT)) {
                    File gmlFile = new File(tmpDir.getAbsolutePath() + File.separator + fileName);
                    return MainService.graphDecoderService.openFile(gmlFile);
                }
                if (fileName.endsWith(GraphOpenerGlobals.DOT_FORMAT) || fileName.endsWith(GraphOpenerGlobals.GV_FORMAT)) {
                    File dotFile = new File(tmpDir.getAbsolutePath() + File.separator + fileName);
                    return MainService.graphDecoderService.openFile(dotFile);
                }
            }

            throw new GraphDecoderException(
                "Zip container should contains " + GraphOpenerGlobals.GRAPHML_FORMAT + " | " +
                    GraphOpenerGlobals.GML_FORMAT + "|" +
                    GraphOpenerGlobals.DOT_FORMAT + "|" +
                    GraphOpenerGlobals.GV_FORMAT + " file");
        } catch (IOException ex) {
            throw new GraphDecoderException(ex);
        } finally {
            FileUtils.deleteQuietly(tmpDir);
        }
    }
}
