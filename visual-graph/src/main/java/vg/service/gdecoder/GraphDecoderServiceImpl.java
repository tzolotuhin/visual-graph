package vg.service.gdecoder;

import com.google.common.collect.Maps;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import vg.lib.decoder.GraphDecoder;
import vg.lib.decoder.dot.DotDecoder;
import vg.lib.decoder.gml.GMLDecoder;
import vg.lib.decoder.graphml.GraphMLDecoder;
import vg.lib.storage.GraphStorage;
import vg.plugin.opener.GraphOpenerGlobals;
import vg.service.gdecoder.decoders.zip.ZipDecoder;
import vg.service.main.MainService;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
@Slf4j
public class GraphDecoderServiceImpl implements GraphDecoderService {
    // Main data
    private final HashMap<String, Class<? extends GraphDecoder>> decoders;

    public GraphDecoderServiceImpl() {
        decoders = Maps.newLinkedHashMap();

        decoders.put(GraphOpenerGlobals.GRAPHML_FORMAT, GraphMLDecoder.class);
        decoders.put(GraphOpenerGlobals.DOT_FORMAT, DotDecoder.class);
        decoders.put(GraphOpenerGlobals.GV_FORMAT, DotDecoder.class);
        decoders.put(GraphOpenerGlobals.GML_FORMAT, GMLDecoder.class);

        decoders.put(GraphOpenerGlobals.ZIP_FORMAT, ZipDecoder.class);
        decoders.put(GraphOpenerGlobals.ZIP_GRAPHML_FORMAT, ZipDecoder.class);
        decoders.put(GraphOpenerGlobals.ZIP_GML_FORMAT, ZipDecoder.class);
        decoders.put(GraphOpenerGlobals.ZIP_DOT_FORMAT, ZipDecoder.class);
        decoders.put(GraphOpenerGlobals.ZIP_GV_FORMAT, ZipDecoder.class);
    }

    @Override
    public Set<String> getAvailableExtensions() {
        return decoders.keySet();
    }

    @Override
    public int openFile(File file) throws GraphDecoder.GraphDecoderException {
        var fileName = file.getName();

        // obtain format.
        var format = fileName.toLowerCase().replaceAll(".*\\.", "");

        try (var fis = new FileInputStream(file)){
            return doOpenFile(fileName, fis, format);
        } catch (IOException ex) {
            throw new GraphDecoder.GraphDecoderException(ex);
        }
    }

    @Override
    public int openGraph(String graphName, String content, String format) throws GraphDecoder.GraphDecoderException {
        return doOpenFile(graphName, IOUtils.toInputStream(content, StandardCharsets.UTF_8), format);
    }

    private int doOpenFile(String graphName, InputStream inputStream, String format)
        throws GraphDecoder.GraphDecoderException {
        var decoderClass = decoders.get(format);

        if (decoderClass == null) {
            throw new GraphDecoder.GraphDecoderException(
                String.format("Can't find a decoder for the following format '%s'.", format));
        }

        // decode...
        try {
            var ctor = decoderClass.getConstructor(InputStream.class, String.class, GraphStorage.class);
            var decoder = ctor.newInstance(inputStream, graphName, MainService.graphDataBaseService);

            long startTimeMs = System.currentTimeMillis();

            var graphId = decoder.decode();

            log.info(String.format(
                "Graph load time: %.3s seconds.",
                (System.currentTimeMillis() - startTimeMs) / 1000.0f));

            return graphId;
        } catch (GraphDecoder.GraphDecoderException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new GraphDecoder.GraphDecoderException(ex);
        }
    }
}
