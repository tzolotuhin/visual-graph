package vg.service.gdecoder;

import java.io.File;
import java.util.Set;
import vg.lib.decoder.GraphDecoder;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface GraphDecoderService {
    Set<String> getAvailableExtensions();

    /**
     * Reads a graph from the file and returns graph id.
     */
    int openFile(File file) throws GraphDecoder.GraphDecoderException;

    /**
     * Reads a graph from string content and returns graph id.
     */
    int openGraph(String graphName, String content, String format) throws GraphDecoder.GraphDecoderException;
}
