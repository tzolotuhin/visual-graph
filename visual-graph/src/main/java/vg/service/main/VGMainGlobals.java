package vg.service.main;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class VGMainGlobals {
    public static final String GRAPH_VIEW_ARG = "GRAPH_VIEW_ARG";

    public static final String LAYOUT_OPERATION_FACTORY = "LAYOUT_OPERATION_FACTORY";
}
