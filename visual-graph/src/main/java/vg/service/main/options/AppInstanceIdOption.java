package vg.service.main.options;

import vg.service.config.CmdLineOption;

import java.util.UUID;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class AppInstanceIdOption extends CmdLineOption {
    private UUID instanceId;

    public AppInstanceIdOption() {
        super("app_instance_id", true, "Application Instance Id");

        setRequired(false);
    }

    public UUID getInstanceId() {
        return instanceId;
    }

    @Override
    public void setValues(String[] values) {
        super.setValues(values);

        instanceId = UUID.fromString(values[0]);
    }

    @Override
    public String[] getDefaultValues() {
        return new String[] {UUID.randomUUID().toString()};
    }
}