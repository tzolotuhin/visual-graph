package vg.service.main;

import org.apache.log4j.PropertyConfigurator;
import vg.service.config.CliCmdLine;
import vg.service.config.FileConfig;
import vg.service.config.ICmdLine;
import vg.service.config.IConfig;
import vg.service.db.SQLiteDataBaseService;
import vg.service.executor.ExecutorServiceImpl;
import vg.service.log.DummyLogger;
import vg.service.operation.OperationService;
import vg.service.gdecoder.GraphDecoderServiceImpl;
import vg.service.gview.GraphViewServiceImpl;
import vg.service.log.ILogger;
import vg.service.log.IWindowMessenger;
import vg.service.log.Log4jLogger;
import vg.service.log.WindowMessenger;
import vg.service.main.options.AppInstanceIdOption;
import vg.service.db.IDataBaseService;
import vg.service.executor.ExecutorService;
import vg.service.gdecoder.GraphDecoderService;
import vg.service.gview.GraphViewService;
import vg.service.progress.SimpleProgressService;
import vg.service.resource.ResourceService;
import vg.service.ui.UserInterfaceService;
import vg.service.progress.IProgressService;
import vg.service.ui.UserInterfaceServiceImpl;

import java.io.File;

/**
 * Contains all necessary services (or resources) for quick access.
 *
 * Note: Don't edit this class. Read only.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class MainService {
    public static ILogger logger = new DummyLogger();
    public static IConfig config;
    public static IWindowMessenger windowMessenger;

    public static ResourceService resourceService;

    public static IDataBaseService graphDataBaseService;
    public static UserInterfaceService userInterfaceService;

    public static IProgressService progressManager;
    public static ExecutorService executorService;

    public static GraphDecoderService graphDecoderService;
    public static GraphViewService graphViewService;
    public static OperationService operationService;

    public static class CmdLineOptions {
        static AppInstanceIdOption appInstanceIdOption = new AppInstanceIdOption();
    }

    // Mutex
    private static final Object generalMutex = new Object();

    /**
     * Executes initial preparations.
     */
    public static void executeInitialPreparations(String[] args) {
        synchronized (generalMutex) {
            try {
                // make changes in default service.
                PropertyConfigurator.configure("data/log4j.properties");
                logger = new Log4jLogger();

                // execute preparations.
                windowMessenger = new WindowMessenger();
                config = new FileConfig();
                ICmdLine cmdLine = new CliCmdLine();

                // add options.
                cmdLine.addOption(MainService.CmdLineOptions.appInstanceIdOption);

                cmdLine.parseCmdLineArgs(args);

                resourceService = new ResourceService();

                graphDataBaseService = new SQLiteDataBaseService();
                userInterfaceService = new UserInterfaceServiceImpl();

                progressManager = new SimpleProgressService();
                executorService = new ExecutorServiceImpl();

                graphDecoderService = new GraphDecoderServiceImpl();
                operationService = new OperationService();
                graphViewService = new GraphViewServiceImpl();
            } catch (Throwable ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static String getWorkingDirectory() {
        return System.getProperty("user.dir") + File.separator;
    }
}
