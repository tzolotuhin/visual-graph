package vg.service.ui;

import com.google.common.collect.Maps;
import vg.service.executor.ExecutorService;
import vg.service.main.MainService;
import vg.shared.utils.MapUtils;
import vg.service.ui.components.MainWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.Map;

/**
 * This class realizes user interface and use swing for it.
 *
 * @author tzolotuhin
 */
public class UserInterfaceServiceImpl implements UserInterfaceService, UserInterfaceService.UserInterfaceServiceCallback {
    // Main components
    private MainWindow mainWindow;

    // Main data
    private Map<UserInterfaceListener, Integer> listeners = Maps.newLinkedHashMap();

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void start() {
        MainService.executorService.executeInEDT(() -> {
            synchronized (generalMutex) {
                mainWindow = new MainWindow(UserInterfaceServiceImpl.this);
            }
        });
    }

    @Override
    public void addMenuItem(JMenuItem item, String place) {
        MainService.executorService.executeInEDT(() -> {
            synchronized (generalMutex) {
                mainWindow.addMenuItem(item, place);
            }
        });
    }

    @Override
    public void addPanel(UserInterfacePanel panel) {
        synchronized (generalMutex) {
            mainWindow.addPanel(panel);
        }
    }

    @Override
    public void selectPanel(UserInterfacePanel panel) {
        synchronized (generalMutex) {
            mainWindow.selectPanel(panel);
        }
    }

    @Override
    public void addTab(UserInterfaceTab tab, FinishActionCallBack callBack) {
        MainService.executorService.execute(new ExecutorService.SwingExecutor() {
            private UserInterfaceTab userInterfaceTab;
            private String title;
            private JComponent view;

            @Override
            public void doInBackground() {
                userInterfaceTab = tab;
                title = tab.getTabTitle();
                view = tab.getView();
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    mainWindow.addTab(userInterfaceTab, title, view, callBack);
                }
            }
        });
    }

    @Override
    public void updateTabTitle(UserInterfaceTab tab, String title) {
        MainService.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                tab.setTabTitle(title);
            }

            @Override
            public void doInEDT() {
                mainWindow.updateTabTitles(tab, title);
            }
        });
    }

    @Override
    public void addObserver(UserInterfaceListener listener) {
        addObserver(listener, MIN_PRIORITY);
    }

    @Override
    public void addObserver(UserInterfaceListener listener, int priority) {
        MainService.executorService.executeInEDT(() -> {
            synchronized (generalMutex) {
                listeners.put(listener, priority);
                listeners = new MapUtils<UserInterfaceListener, Integer>().sortMapByValue(listeners);
            }
        });
    }

    @Override
    public void addInstrument(UserInterfaceInstrument instrument) {
        MainService.executorService.executeInEDT(() -> {
            synchronized (generalMutex) {
                mainWindow.addInstrument(instrument);
            }
        });
    }

    @Override
    public JFrame getMainFrame() {
        synchronized (generalMutex) {
            return mainWindow.getMainFrame();
        }
    }

    @Override
    public void refresh() {
        MainService.executorService.executeInEDT(() -> {
            synchronized (generalMutex) {
                mainWindow.refresh();
            }
        });
    }

    @Override
    public void copyToClipboard(String text) {
        synchronized (generalMutex) {
            StringSelection stringSelection = new StringSelection(text);
            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
            clpbrd.setContents(stringSelection, null);
        }
    }

    @Override
    public void quit() {
        MainService.executorService.executeInEDT(() -> {
            synchronized (generalMutex) {
                mainWindow.quit();
            }
        });
    }

    @Override
    public void notifyOnChangeDesktopTab(UserInterfaceTab tab) {
        MainService.logger.printDebug("Method 'notifyOnChangeDesktopTab' was called.");

        synchronized (generalMutex) {
            listeners.keySet().forEach(x -> MainService.executorService.executeInEDT(() -> x.onChangeTab(tab)));
        }
    }

    @Override
    public void notifyOnOpenTab(UserInterfaceTab tab) {
        MainService.logger.printDebug("Method 'notifyOnOpenTab' was called.");

        synchronized (generalMutex) {
            listeners.keySet().forEach(x -> MainService.executorService.executeInEDT(() -> x.onOpenTab(tab)));
        }
    }

    @Override
    public void notifyOnCloseTab(UserInterfaceTab tab) {
        synchronized (generalMutex) {
            listeners.keySet().forEach(x -> MainService.executorService.executeInEDT(() -> x.onCloseTab(tab)));
        }
    }

    @Override
    public void notifyOnAddPanel(UserInterfacePanel panel, int place) {
        synchronized (generalMutex) {
            listeners.keySet().forEach(x -> MainService.executorService.executeInEDT(() -> x.onAddPanel(panel, place)));
        }
    }

    @Override
    public void notifyOnRemovePanel(UserInterfacePanel panel, int place) {
        synchronized (generalMutex) {
            listeners.keySet().forEach(x -> MainService.executorService.executeInEDT(() -> x.onRemovePanel(panel, place)));
        }
    }

    @Override
    public void notifyOnQuit() {
        synchronized (generalMutex) {
            listeners.keySet().forEach(x -> MainService.executorService.executeInEDT(x::onQuit));
        }
    }
}
