package vg.service.ui.components;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.service.main.MainService;
import vg.service.ui.UserInterfaceService;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.ToolTipManager;
import javax.swing.plaf.TabbedPaneUI;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DesktopPanel {
    // Main components
    private final JPanel outView, innerView;
    private final JTabbedPane tabs;
    private final JPopupMenu popupMenu;
    private UserInterfaceService.UserInterfaceTab currentTabForPopupMenu;

    // Main data
    private final UserInterfaceService.UserInterfaceServiceCallback userInterfaceServiceCallback;
    private final List<TabComponent> tabComponents = new ArrayList<>();

    // Mutex
    private final Object generalMutex = new Object();

    DesktopPanel(UserInterfaceService.UserInterfaceServiceCallback callback) {
        this.userInterfaceServiceCallback = callback;

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        // w/a for set tooltip text:
        // https://stackoverflow.com/questions/3198037/workaround-for-settooltiptext-consuming-mouse-events
        tabs = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT) {
            @Override
            public String getToolTipText(MouseEvent e) {
                int index = ((TabbedPaneUI)ui).tabForCoordinate(this, e.getX(), e.getY());

                if (index != -1) {
                    JComponent component = (JComponent)getTabComponentAt(index);
                    return component.getToolTipText();
                }

                return super.getToolTipText(e);
            }
        };
        ToolTipManager.sharedInstance().registerComponent(tabs);

        var closeAll = new JMenuItem("Close All");
        closeAll.setPreferredSize(new Dimension(170, 20));
        closeAll.addActionListener(e -> {
            synchronized (generalMutex) {
                Lists.newArrayList(tabComponents).forEach(this::doRemoveTab);
                rebuildView();
            }
        });

        var closeOthers = new JMenuItem("Close Others");
        closeOthers.setPreferredSize(new Dimension(170, 20));
        closeOthers.addActionListener(e -> {
            synchronized (generalMutex) {
                if (currentTabForPopupMenu == null) {
                    log.error("Current tab for popup menu is null.");
                    return;
                }

                Lists.newArrayList(tabComponents).forEach(tabComponent -> {
                    if (tabComponent.userInterfaceTab != currentTabForPopupMenu) {
                        doRemoveTab(tabComponent);
                    }
                });

                rebuildView();
            }
        });

        popupMenu = new JPopupMenu();
        popupMenu.add(closeOthers);
        popupMenu.add(closeAll);

        tabs.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    synchronized (generalMutex) {
                        int index = tabs.getUI().tabForCoordinate(tabs, e.getX(), e.getY());
                        if (index >= 0) {
                            var tabComponent = tabs.getComponentAt(index);
                            if (tabComponent instanceof JComponent) {
                                currentTabForPopupMenu = lookupTabComponent(tabComponent).userInterfaceTab;
                                popupMenu.show(e.getComponent(), e.getX(), e.getY());
                                popupMenu.setEnabled(true);
                            }
                        }
                    }
                }
            }
        });

        // add listeners.
        tabs.addChangeListener(e -> {
            synchronized (generalMutex) {
                doChangeTab();
            }
        });

        // ctrl-w, ctrl-tab and ctrl-shift-tab.
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
            if (e.getID() == KeyEvent.KEY_PRESSED && e.getKeyCode() == KeyEvent.VK_W && (e.isControlDown() || e.isMetaDown())) {
                synchronized (generalMutex) {
                    var selectedComponent = tabs.getSelectedComponent();
                    if (selectedComponent == null) {
                        return true;
                    }
                    removeTab(lookupTabComponent(selectedComponent).userInterfaceTab);
                }
                return true;
            }

            if (e.getID() == KeyEvent.KEY_PRESSED && e.getKeyCode() == KeyEvent.VK_TAB && (e.isControlDown() || e.isMetaDown())) {
                synchronized (generalMutex) {
                    int tabCount = tabs.getTabCount();
                    if (tabCount > 0) {
                        int sel = tabs.getSelectedIndex();
                        if (e.isShiftDown()) {
                            sel = sel - 1;
                            if (sel < 0)
                                sel = tabCount - 1;
                        } else {
                            sel = sel + 1;
                            if (sel >= tabCount)
                                sel = 0;
                        }
                        tabs.setSelectedIndex(sel);
                    }
                }
                return true;
            }
            return false;
        });

        rebuildView();
    }

    public JComponent getView() {
        return outView;
    }

    void addTab(UserInterfaceService.UserInterfaceTab userInterfaceTab,
                String title,
                JComponent view,
                UserInterfaceService.FinishActionCallBack callBack) {
        try {
            MainService.logger.printDebug(String.format("Add new tab with title '%s'.", title));

            synchronized (generalMutex) {
                doAddTab(userInterfaceTab, title, view);
                rebuildView();
            }

            MainService.logger.printDebug(String.format("Tab with title '%s' was added.", title));

            if (callBack != null) {
                callBack.onFinishAction();
            }
        } catch (Throwable ex) {
            MainService.logger.printException(ex);
        }
    }

    public void updateTabTitle(UserInterfaceService.UserInterfaceTab tab, String title) {
        synchronized (generalMutex) {
            var tabComponent = lookupTabComponent(tab);

            Validate.notNull(tabComponent);

            tabComponent.simpleTabWithCloseButton.setTabTitle(title);

            rebuildView();
        }
    }

    private TabComponent lookupTabComponent(Component component) {
        return tabComponents.stream()
                .filter(tabComponent ->
                        tabComponent.simpleTabWithCloseButton == component
                                || tabComponent.userInterfaceTabView == component)
                .findFirst()
                .orElse(null);
    }

    private TabComponent lookupTabComponent(UserInterfaceService.UserInterfaceTab userInterfaceTab) {
        return tabComponents.stream()
                .filter(tabComponent ->
                        tabComponent.userInterfaceTab == userInterfaceTab)
                .findFirst()
                .orElse(null);
    }

    private void removeTab(UserInterfaceService.UserInterfaceTab userInterfaceTab) {
        synchronized (generalMutex) {
            doRemoveTab(lookupTabComponent(userInterfaceTab));
            rebuildView();
        }
    }

    void refresh() {
        synchronized (generalMutex) {
            SwingUtilities.updateComponentTreeUI(tabs);
            SwingUtilities.updateComponentTreeUI(innerView);
            SwingUtilities.updateComponentTreeUI(outView);
            SwingUtilities.updateComponentTreeUI(popupMenu);
        }
    }

    private void rebuildView() {
        // clear panel.
        innerView.removeAll();
        innerView.setBorder(BorderFactory.createLineBorder(Color.BLACK));

        innerView.add(
                tabs,
                new GridBagConstraints(
                        0,
                        0,
                        1,
                        1,
                        1,
                        1,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH,
                        new Insets(0,0,0,0),
                        0,
                        0));

        // update ui
        innerView.updateUI();
    }

    private void doAddTab(
            UserInterfaceService.UserInterfaceTab userInterfaceTab,
            String title,
            JComponent view) {
        Validate.notNull(userInterfaceTab);

        // create title with close button for new tab.
        var simpleTabWithCloseButton = new SimpleTabWithCloseButton(title, e -> removeTab(userInterfaceTab));
        simpleTabWithCloseButton.setToolTipText(title);

        tabComponents.add(new TabComponent(simpleTabWithCloseButton, userInterfaceTab, view));

        userInterfaceServiceCallback.notifyOnOpenTab(userInterfaceTab);

        tabs.addTab(title, view);
        int index = tabs.getTabCount() - 1;

        // w/a for set tooltip text:
        // https://stackoverflow.com/questions/3198037/workaround-for-settooltiptext-consuming-mouse-events
        ToolTipManager.sharedInstance().unregisterComponent(simpleTabWithCloseButton);

        tabs.setTabComponentAt(index, simpleTabWithCloseButton);
        tabs.setSelectedIndex(index);
    }

    private void doRemoveTab(TabComponent tabComponent) {
        tabs.remove(tabComponent.userInterfaceTabView);
        tabComponents.remove(tabComponent);
        userInterfaceServiceCallback.notifyOnCloseTab(tabComponent.userInterfaceTab);
	}

    private void doChangeTab() {
        int index = tabs.getSelectedIndex();
        if(index >= 0 && index < tabs.getTabCount()) {
            JComponent view = (JComponent)tabs.getComponentAt(index);
            var tabComponent = lookupTabComponent(view);
            userInterfaceServiceCallback.notifyOnChangeDesktopTab(tabComponent.userInterfaceTab);
        } else {
            //if all tabs close.
            userInterfaceServiceCallback.notifyOnChangeDesktopTab(null);
        }
    }

    private static class SimpleTabWithCloseButton extends JPanel {
        // Icons
        private static final ImageIcon closeActiveImageIcon;
        private static final ImageIcon closeImageIcon;

        static {
            closeActiveImageIcon = new ImageIcon("./data/resources/textures/closeActive.png");
            closeImageIcon = new ImageIcon("./data/resources/textures/close.png");
        }

        private final JLabel tabTitleLabel;

        SimpleTabWithCloseButton(String title, ActionListener closeAction) {
            super(new GridBagLayout());

            setOpaque(false);

            // handle null title.
            if (title == null) {
                title = "";
            }

            var closeButton = new JLabel(closeImageIcon);
            closeButton.setPreferredSize(new Dimension(20, 20));
            closeButton.setSize(new Dimension(20, 20));
            tabTitleLabel = new JLabel(title);
            tabTitleLabel.setPreferredSize(new Dimension(150 + 25, 20));
            tabTitleLabel.setHorizontalAlignment(JLabel.CENTER);

            // pack ui
            add(tabTitleLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            add(closeButton, new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

            closeButton.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    closeAction.actionPerformed(new ActionEvent(SimpleTabWithCloseButton.this, e.getID(), null));
                }

                @Override
                public void mouseEntered(MouseEvent e) {
                    closeButton.setIcon(closeActiveImageIcon);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    closeButton.setIcon(closeImageIcon);
                }
            });
        }

        void setTabTitle(String tabTitle) {
            tabTitleLabel.setText(tabTitle);
        }
    }

    private static class TabComponent {
        private final SimpleTabWithCloseButton simpleTabWithCloseButton;
        private final UserInterfaceService.UserInterfaceTab userInterfaceTab;
        private final JComponent userInterfaceTabView;

        public TabComponent(
                SimpleTabWithCloseButton simpleTabWithCloseButton,
                UserInterfaceService.UserInterfaceTab userInterfaceTab,
                JComponent userInterfaceTabView) {
            this.simpleTabWithCloseButton = simpleTabWithCloseButton;
            this.userInterfaceTab = userInterfaceTab;
            this.userInterfaceTabView = userInterfaceTabView;
        }
    }
}
