package vg.service.ui.components;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.Validate;
import vg.service.main.MainService;
import vg.service.ui.UserInterfaceService;
import vg.shared.gui.StorageFrame;
import vg.shared.gui.components.SimpleStatusBar;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import java.util.UUID;

/**
 * Main window of the application.
 *
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class MainWindow extends StorageFrame {
    // Constants
    private static final String UI_MAIN_WINDOW = "UI_MAIN_WINDOW_";
    private static final String UI_MAIN_WINDOW_WIDTH = UI_MAIN_WINDOW + "WIDTH";
    private static final String UI_MAIN_WINDOW_HEIGHT = UI_MAIN_WINDOW + "HEIGHT";
    private static final String UI_MAIN_WINDOW_X = UI_MAIN_WINDOW + "X";
    private static final String UI_MAIN_WINDOW_Y = UI_MAIN_WINDOW + "Y";

    private static final String UI_MAIN_WINDOW_WEST_TOP_SELECTED_PANEL = UI_MAIN_WINDOW + "WEST_TOP_SELECTED_PANEL";
    private static final String UI_MAIN_WINDOW_WEST_BOTTOM_SELECTED_PANEL = UI_MAIN_WINDOW + "WEST_BOTTOM_SELECTED_PANEL";

    private static final String UI_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION = UI_MAIN_WINDOW + "WEST_CENTER_DIVIDER_LOCATION";
    private static final String UI_MAIN_WINDOW_WEST_TOP_WEST_BOTTOM_DIVIDER_LOCATION = UI_MAIN_WINDOW + "WEST_TOP_WEST_BOTTOM_DIVIDER_LOCATION";

    private static final int DEFAULT_UI_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION = 150;
    private static final int DEFAULT_UI_MAIN_WINDOW_WEST_TOP_AND_WEST_BOTTOM_DIVIDER_LOCATION = 150;

    private final JPanel innerView;

    // Main Components: menu
    private final JMenu fileMenu, editMenu, analyzeMenu, windowMenu, otherMenu, helpMenu;
    private final JMenuItem quitMenuItem;
    private final JMenuBar menuBar;

    // Main Components: desktop and panels
    private final InstrumentPanel northInstrumentPanel;
    private final InstrumentPanel westInstrumentPanel;

    private final DesktopPanel desktopPanel;
    private final UserInterfacePanelSet westTopPanel, westBottomPanel;

    // Main Components: status bar
    private final JLabel progressName;
    private final SimpleStatusBar statusBar;

    private final JProgressBar progressBar;

    // Main data
    private int westAndCenterDividerLocation;
    private int westTopAndWestBottomDividerLocation;

    private final UserInterfaceService.UserInterfaceServiceCallback callback;

    public MainWindow(UserInterfaceService.UserInterfaceServiceCallback callback) {
        super(UI_MAIN_WINDOW_X, UI_MAIN_WINDOW_Y, UI_MAIN_WINDOW_WIDTH, UI_MAIN_WINDOW_HEIGHT);

        this.callback = callback;

        westAndCenterDividerLocation = MainService.config.getIntegerProperty(UI_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION, DEFAULT_UI_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION);
        westTopAndWestBottomDividerLocation = MainService.config.getIntegerProperty(UI_MAIN_WINDOW_WEST_TOP_WEST_BOTTOM_DIVIDER_LOCATION, DEFAULT_UI_MAIN_WINDOW_WEST_TOP_AND_WEST_BOTTOM_DIVIDER_LOCATION);

        frame.setTitle("Visual Graph");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                quit();
            }
        });

        JPanel outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);
        frame.add(outView);

        // creating menu items
        menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        fileMenu = new JMenu("File");
        editMenu = new JMenu("Edit");
        analyzeMenu = new JMenu("Analyze");
        windowMenu = new JMenu("Window");
        otherMenu = new JMenu("Other");
        helpMenu = new JMenu("Help");

        quitMenuItem  = new JMenuItem("Quit");

        quitMenuItem.setIcon(new ImageIcon("./data/resources/textures/quit.png"));

        // creating components
        desktopPanel = new DesktopPanel(callback);

        northInstrumentPanel = new InstrumentPanel(InstrumentPanel.NORTH_ORIENTATION);
        westInstrumentPanel = new InstrumentPanel(InstrumentPanel.WEST_ORIENTATION);

        westTopPanel = new UserInterfacePanelSet();
        westBottomPanel = new UserInterfacePanelSet();

        // creating status bar components.
        this.statusBar = new SimpleStatusBar();
        this.progressBar = new JProgressBar(0,100);
        this.progressBar.setMaximumSize(new Dimension(100,this.statusBar.getPreferredSize().height-2));
        this.progressName = new JLabel();
        this.statusBar.add(progressName);
        this.statusBar.add(progressBar);
        Timer progressTimer = new Timer(500, e -> SwingUtilities.invokeLater(() -> {
            progressName.setText(String.format("Operation '%s': ", MainService.progressManager.getTaskName()));
            progressBar.setValue((int) MainService.progressManager.updateProgress());
            boolean visible = MainService.progressManager.getTaskCount() > 0;
            progressName.setVisible(visible);
            progressBar.setVisible(visible);
        }));
        progressTimer.start();

        // adding of menu bar
        fileMenu.add(quitMenuItem);

        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        menuBar.add(analyzeMenu);
        menuBar.add(windowMenu);
        menuBar.add(helpMenu);

        // adding mnemonic for menus
        fileMenu.setMnemonic('f');
        editMenu.setMnemonic('e');
        helpMenu.setMnemonic('h');
        quitMenuItem.setMnemonic('q');
        quitMenuItem.addActionListener(e -> quit());
        quitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.ALT_DOWN_MASK));

        rebuildView();
    }

    public void addMenuItem(JMenuItem item, String place) {
        Validate.notNull(item);
        Validate.notNull(place);
        Validate.isTrue(SwingUtilities.isEventDispatchThread());

        MainService.logger.printDebug("Add menu item = " + item.getText() + ", place = " + place);
        switch (place) {
            case UserInterfaceService.FILE_MENU:
                fileMenu.remove(quitMenuItem);
                fileMenu.add(item);
                fileMenu.add(quitMenuItem);
                break;
            case UserInterfaceService.ANALYZE_MENU:
                analyzeMenu.add(item);
                break;
            case UserInterfaceService.EDIT_MENU:
                editMenu.add(item);
                break;
            case UserInterfaceService.WINDOW_MENU:
                windowMenu.add(item);
                break;
            case UserInterfaceService.HELP_MENU:
                helpMenu.add(item);
                break;
            default:
                otherMenu.add(item);
                MainService.logger.printError("Place is not found, place: " + place);
                break;
        }
        rebuildView();
    }

    public void addPanel(UserInterfaceService.UserInterfacePanel panel) {
        MainService.userInterfaceService.addInstrument(panel.getInstrument());

        String key = null;
        switch (panel.getPlace()) {
            case UserInterfaceService.UserInterfacePanel.WEST_TOP_PLACE:
                westTopPanel.addPanel(panel);
                key = UI_MAIN_WINDOW_WEST_TOP_SELECTED_PANEL;
                break;

            case UserInterfaceService.UserInterfacePanel.WEST_BOTTOM_PLACE:
                westBottomPanel.addPanel(panel);
                key = UI_MAIN_WINDOW_WEST_BOTTOM_SELECTED_PANEL;
                break;
        }

        var id = MainService.config.getStringProperty(key);
        if (id != null && UUID.fromString(id).equals(panel.getId())) {
            selectPanel(panel);
        }
    }

    public void selectPanel(UserInterfaceService.UserInterfacePanel panel) {
        // select panel...
        switch (panel.getPlace()) {
            case UserInterfaceService.UserInterfacePanel.WEST_TOP_PLACE:
                westTopPanel.selectPanel(panel);
                MainService.config.setProperty(UI_MAIN_WINDOW_WEST_TOP_SELECTED_PANEL, panel.getId().toString());
                break;

            case UserInterfaceService.UserInterfacePanel.WEST_BOTTOM_PLACE:
                westBottomPanel.selectPanel(panel);
                MainService.config.setProperty(UI_MAIN_WINDOW_WEST_BOTTOM_SELECTED_PANEL, panel.getId().toString());
                break;
        }

        // rebuild view...
        rebuildView();
    }

    public void addTab(UserInterfaceService.UserInterfaceTab userInterfaceTab, String title, JComponent view, UserInterfaceService.FinishActionCallBack callBack) {
        desktopPanel.addTab(userInterfaceTab, title, view, callBack);
    }

    public void updateTabTitles(UserInterfaceService.UserInterfaceTab tab, String title) {
        desktopPanel.updateTabTitle(tab, title);
    }

    public void addInstrument(UserInterfaceService.UserInterfaceInstrument instrument) {
        switch (instrument.getPlace()) {
            case UserInterfaceService.UserInterfaceInstrument.NORTH_PLACE:
                northInstrumentPanel.addInstrument(instrument);
                break;
            case UserInterfaceService.UserInterfaceInstrument.WEST_PLACE:
                westInstrumentPanel.addInstrument(instrument);
                break;
        }
    }

    public JFrame getMainFrame() {
        return frame;
    }

    public void quit() {
        callback.notifyOnQuit();

        // stop service.
        MainService.graphDataBaseService.close();
        MainService.executorService.shutdown();

        System.exit(0);
    }

    private void rebuildView() {
        //clear inner view panel.
        innerView.removeAll();

        innerView.add(
                northInstrumentPanel.getView(),
                new GridBagConstraints(0,0,  3,1,  1, 0,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 0),  0,0));
        innerView.add(
                westInstrumentPanel.getView(),
                new GridBagConstraints(0,1,  1,1,  0, 1,
                        GridBagConstraints.WEST,
                        GridBagConstraints.VERTICAL,
                        new Insets(0, 0, 0, 0),  0,0));

        // build west panel.
        JPanel westPanel = new JPanel(new GridLayout(1, 1));
        if (!westTopPanel.isEmpty() && !westBottomPanel.isEmpty()) {
            var westSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, westTopPanel.getView(), westBottomPanel.getView());
            westSplitPane.setDividerLocation(westTopAndWestBottomDividerLocation);
            westSplitPane.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, evt -> {
                westTopAndWestBottomDividerLocation = Integer.parseInt(evt.getNewValue().toString());
                MainService.config.setIntegerProperty(UI_MAIN_WINDOW_WEST_TOP_WEST_BOTTOM_DIVIDER_LOCATION, westTopAndWestBottomDividerLocation);
                MainService.logger.printDebug(String.format("West Top <-> West Bottom slider: '%s'.", evt.getNewValue()));
            });
            westPanel.add(westSplitPane);
        } else if (!westTopPanel.isEmpty()) {
            westPanel.add(westTopPanel.getView());
        } else if (!westBottomPanel.isEmpty()) {
            westPanel.add(westBottomPanel.getView());
        } else {
            westPanel = null;
        }

        // build center panel.
        JPanel centerPanel = new JPanel(new GridLayout(1, 1));
        centerPanel.add(desktopPanel.getView());

        // build inner view panel.
        if (westPanel != null) {
            var splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, westPanel, centerPanel);
            splitPane.setDividerLocation(westAndCenterDividerLocation);
            splitPane.addPropertyChangeListener(JSplitPane.DIVIDER_LOCATION_PROPERTY, evt -> {
                westAndCenterDividerLocation = Integer.parseInt(evt.getNewValue().toString());
                MainService.config.setIntegerProperty(UI_MAIN_WINDOW_WEST_CENTER_DIVIDER_LOCATION, westAndCenterDividerLocation);
                MainService.logger.printDebug(String.format("West <-> Center slider: '%s'.", evt.getNewValue()));
            });
            innerView.add(
                    splitPane,
                    new GridBagConstraints(1,1,  2,1,  1,1,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.BOTH,
                            new Insets(0,0,0,0),  0,0));
        } else {
            innerView.add(
                    centerPanel,
                    new GridBagConstraints(1,1,  2,1,  1,1,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.BOTH,
                            new Insets(0,0,0,0),  0,0));
        }

        innerView.add(
                statusBar,
                new GridBagConstraints(0,3, 3,1, 1,0,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(0,0,0,0), 0,0));

        // update ui.
        innerView.updateUI();
    }

    public void refresh() {
        SwingUtilities.updateComponentTreeUI(menuBar);
        desktopPanel.refresh();
        westTopPanel.refresh();
        westBottomPanel.refresh();
    }

    private static class UserInterfacePanelSet {
        // Main components.
        private JPanel outView, innerView;

        // Main data.
        private UserInterfaceService.UserInterfacePanel currPanel = null;
        private List<UserInterfaceService.UserInterfacePanel> panels = Lists.newArrayList();

        UserInterfacePanelSet() {
            outView = new JPanel(new GridLayout(1, 1));
            innerView = new JPanel(new GridBagLayout());
            outView.add(innerView);

            rebuildView();
        }

        void addPanel(UserInterfaceService.UserInterfacePanel panel) {
            panels.add(panel);

            rebuildView();
        }

        void selectPanel(UserInterfaceService.UserInterfacePanel panel) {
            if (currPanel == panel) {
                panel = null;
            }

            currPanel = panel;

            for (var p : panels) {
                if (p != panel) {
                    p.getInstrument().setSelected(false);
                } else {
                    p.getInstrument().setSelected(true);
                }
            }

            rebuildView();
        }

        boolean isEmpty() {
            return currPanel == null;
        }

        JPanel getView() {
            return outView;
        }

        private void rebuildView() {
            //clear panel.
            innerView.removeAll();
            innerView.setBorder(BorderFactory.createLineBorder(Color.BLACK));

            if (currPanel != null) {
                innerView.add(currPanel.getView(), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }

            // update ui.
            innerView.updateUI();
        }

        void refresh() {
            SwingUtilities.updateComponentTreeUI(innerView);
            SwingUtilities.updateComponentTreeUI(outView);
        }
    }
}
