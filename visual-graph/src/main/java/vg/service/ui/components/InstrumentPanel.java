package vg.service.ui.components;

import com.google.common.collect.Lists;
import vg.service.ui.UserInterfaceService;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
class InstrumentPanel {
    // Constants
    private static final int SOUTH_ORIENTATION = 1;
    static final int WEST_ORIENTATION = 2;
    private static final int EAST_ORIENTATION = 3;
    static final int NORTH_ORIENTATION = 4;

    // Main components
    private final JPanel outView, innerView;

    // Main data
    private int type;
    private List<UserInterfaceService.UserInterfaceInstrument> userInterfaceInstruments = Lists.newArrayList();

    InstrumentPanel(int type) {
        this.type = type;

        innerView = new JPanel(new GridBagLayout());
        innerView.setBorder(BorderFactory.createLineBorder(Color.black));

        outView = new JPanel(new GridLayout(1, 1));
        outView.add(innerView);
    }

    JPanel getView() {
        return outView;
    }

    void addInstrument(UserInterfaceService.UserInterfaceInstrument userInterfaceInstrument) {
        userInterfaceInstruments.add(userInterfaceInstrument);
        rebuildView();
    }

    private void rebuildView() {
        //clear panel
        innerView.removeAll();

        int index = 0;
        for (UserInterfaceService.UserInterfaceInstrument userInterfaceInstrument : userInterfaceInstruments) {
            JPanel view = userInterfaceInstrument.getView();
            if (type == WEST_ORIENTATION || type == EAST_ORIENTATION) {
                view.setPreferredSize(UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE);
                view.setMaximumSize(UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE);
                view.setMinimumSize(UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE);
            } else if (type == SOUTH_ORIENTATION) {
                view.setPreferredSize(UserInterfaceService.SOUTH_INSTRUMENT_PANEL_SIZE);
                view.setMaximumSize(UserInterfaceService.SOUTH_INSTRUMENT_PANEL_SIZE);
                view.setMinimumSize(UserInterfaceService.SOUTH_INSTRUMENT_PANEL_SIZE);
            }

            int leftInset, rightInset;

            if (type == SOUTH_ORIENTATION && index == 0) {
                leftInset = UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE.width;
            } else {
                leftInset = 0;
            }
            if (type == SOUTH_ORIENTATION && index == userInterfaceInstruments.size() - 1) {
                rightInset = UserInterfaceService.WEST_EAST_INSTRUMENT_PANEL_SIZE.width;
            } else {
                rightInset = 0;
            }


            if (type == NORTH_ORIENTATION || type == SOUTH_ORIENTATION) {
                if (index == userInterfaceInstruments.size() - 1) {
                    innerView.add(view, new GridBagConstraints(index,0, 1,1,  1,0,  GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0,leftInset,0,rightInset),  0,0));
                } else {
                    innerView.add(view, new GridBagConstraints(index,0, 1,1,  0,0,  GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0,leftInset,0,rightInset),  0,0));
                }
            } else {
                if (index == userInterfaceInstruments.size() - 1) {
                    innerView.add(
                            view,
                            new GridBagConstraints(
                                    0,
                                    index,
                                    1,
                                    1,
                                    0,
                                    1,
                                    GridBagConstraints.NORTH,
                                    GridBagConstraints.NONE,
                                    new Insets(0,0,0,0),
                                    0,
                                    0));
                } else {
                    innerView.add(
                            view,
                            new GridBagConstraints(
                                    0,
                                    index,
                                    1,
                                    1,
                                    0,
                                    0,
                                    GridBagConstraints.CENTER,
                                    GridBagConstraints.NONE,
                                    new Insets(0,0,0,0),
                                    0,
                                    0));
                }
            }
            index++;
        }

        // update ui
        innerView.updateUI();
    }
}
