package vg.service.ui;

import vg.service.main.VGMainGlobals;

import javax.swing.*;
import java.awt.*;
import java.util.UUID;

/**
 * User interface service.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface UserInterfaceService {
    // Constants
    Dimension NORTH_INSTRUMENT_PANEL_SIZE = new Dimension(32, 32);
    Dimension WEST_EAST_INSTRUMENT_PANEL_SIZE = new Dimension(25, 90);
    Dimension SOUTH_INSTRUMENT_PANEL_SIZE = new Dimension(110, 25);

    Font INSTRUMENT_PANEL_FONT = new Font(Font.MONOSPACED, Font.PLAIN, 12);

    // DarkKhaki.
    Color GRID_COLOR = new Color(189, 183, 107, 200);
    Point DEFAULT_GRID_SIZE = new Point(20, 20);

    int MAX_PRIORITY = 1;
    int MIN_PRIORITY = 999;

    // Constants: menu
    String FILE_MENU = "File";
    String EDIT_MENU = "Edit";
    String ANALYZE_MENU = "Analyze";
    String WINDOW_MENU = "Window";
    String HELP_MENU = "Help";

    void start();

    /**
     * Adds item to menu
     * <p>
     * Please use constants FILE_MENU, EDIT_MENU, etc.
     */
    void addMenuItem(JMenuItem item, String menu);

    void addInstrument(UserInterfaceInstrument instrument);

    void addPanel(UserInterfacePanel panel);

    void selectPanel(UserInterfacePanel panel);

    void addTab(UserInterfaceTab tab, FinishActionCallBack callBack);

    void updateTabTitle(UserInterfaceTab tab, String title);

    void addObserver(UserInterfaceListener listener);

    /**
     * Max priority is 1, min priority 999
     */
    void addObserver(UserInterfaceListener listener, int priority);

    void quit();

    void refresh();

    void copyToClipboard(String text);

    /**
     * Returns main frame, which may use how parent for different dialogs.
     */
    JFrame getMainFrame();

    interface FinishActionCallBack {
        void onFinishAction();
    }

    interface UserInterfaceTab {
        String getTabTitle();

        void setTabTitle(String tabTitle);

        JComponent getView();
    }

    interface UserInterfacePanel {
        int WEST_TOP_PLACE = 1;
        int WEST_BOTTOM_PLACE = 2;

        UUID getId();

        UserInterfaceInstrument getInstrument();

        int getPlace();

        JPanel getView();
    }

    interface UserInterfaceInstrument {
        int NORTH_PLACE = 1;
        int WEST_PLACE = 2;

        UUID getId();

        int getPlace();

        JPanel getView();

        void setSelected(boolean state);
    }

    /**
     * Determines methods for user interface observer.
     *
     * All methods will called from EDT.
     */
    class UserInterfaceListener {
        public void onChangeTab(UserInterfaceTab tab) {}
        public void onOpenTab(UserInterfaceTab tab) {}
        public void onCloseTab(UserInterfaceTab tab) {}

        public void onAddPanel(UserInterfacePanel panel, int place) {}
        public void onRemovePanel(UserInterfacePanel panel, int place) {}

        public void onQuit() {}
    }

    interface UserInterfaceServiceCallback {
        void notifyOnChangeDesktopTab(UserInterfaceService.UserInterfaceTab tab);
        void notifyOnOpenTab(UserInterfaceService.UserInterfaceTab tab);
        void notifyOnCloseTab(UserInterfaceService.UserInterfaceTab tab);

        void notifyOnRemovePanel(UserInterfaceService.UserInterfacePanel panel, int place);
        void notifyOnAddPanel(UserInterfaceService.UserInterfacePanel panel, int place);

        void notifyOnQuit();
    }
}
