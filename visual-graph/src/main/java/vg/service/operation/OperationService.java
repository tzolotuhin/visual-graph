package vg.service.operation;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.Validate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * This interface manages all algorithms.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class OperationService {
    // Main data.
    private List<OperationFactory> factories = Lists.newArrayList();

    // Mutex.
    private final Object generalMutex = new Object();

    /**
     * This method registers new operation's factory.
     */
    public void registerOperationFactory(OperationFactory operationFactory) {
        Validate.notNull(operationFactory);

        synchronized (generalMutex) {
            factories.add(operationFactory);
        }
    }

    /**
     * This method returns all registered operation's factories.
     */
    public List<OperationFactory> getRegisteredOperationFactories() {
        synchronized (generalMutex) {
            return Lists.newArrayList(factories);
        }
    }

    public OperationFactory getOperationFactoryByName(String name) {
        synchronized (generalMutex) {
            return factories.stream()
                    .filter(x -> x.getName().equalsIgnoreCase(name))
                    .findFirst()
                    .orElse(null);
        }
    }

    public static abstract class OperationFactory {
        public static final String LAYOUT_GROUP = "Layout";
        public static final String OTHER_GROUP = "Other";

        private final Map<String, Object> declaredArgs = Maps.newLinkedHashMap();

        public Map<String, Object> getDeclaredArgs() {
            synchronized (this) {
                return Maps.newLinkedHashMap(declaredArgs);
            }
        }

        public <T> T getDeclaredArg(String argKey, Class<T> clazz) {
            var argValue = declaredArgs.get(argKey);

            if (clazz.isInstance(argValue)) {
                return clazz.cast(argValue);
            }

            return null;
        }

        public void modifyDeclaredArg(String key, Object value) {
            synchronized (this) {
                declaredArgs.put(key, value);
            }
        }

        public abstract BaseProcedure buildOperation(Map<String, Object> args);

        public String getGroup() {
            return OTHER_GROUP;
        }

        public abstract String getName();
    }

    public static abstract class BaseProcedure implements Procedure {
        private static final int STARTED = 1;
        private static final int STOP_IN_PROGRESS = 2;
        private static final int STOPPED = 3;

        protected AtomicInteger state = new AtomicInteger(STARTED);

        protected Map<String, Object> args;

        protected Map<String, Object> getArgs() {
            return args;
        }

        protected <T> T getArg(String argName, Class<T> clazz) {
            var arg = args.get(argName);
            if (arg == null) {
                throw new IllegalArgumentException(String.format("Argument with name %s is not available.", argName));
            }

            return clazz.cast(arg);
        }

        protected <T> List<T> getListArg(String argName, Class<T> clazz) {
            var result = new ArrayList<T>();

            getArg(argName, List.class).forEach(x -> result.add(clazz.cast(x)));

            return result;
        }

        public void stop() {
            synchronized (this) {
                if (state.get() == STARTED) {
                    state.set(STOP_IN_PROGRESS);
                }
            }
        }

        public boolean stopInProgress() {
            return stopInProgress(state);
        }

        protected void finish() {
            synchronized (this) {
                state.set(STOPPED);
            }
        }

        public static boolean stopInProgress(AtomicInteger state) {
            return state.get() == STOP_IN_PROGRESS;
        }
    }

    public interface Procedure {
        void execute();
    }

    public interface ProcedureWithArgs<ArgsType> {
        void execute(ArgsType args);
    }

    public interface ProcedureWith2Args<Arg1Type, Arg2Type> {
        void execute(Arg1Type arg1, Arg2Type arg2);
    }

    public interface ProcedureWith3Args<Arg1Type, Arg2Type, Arg3Type> {
        void execute(Arg1Type arg1, Arg2Type arg2, Arg3Type arg3);
    }

    public interface Operation<ResultType> {
        ResultType execute();
    }

    public interface OperationWithArgs<ArgsType, ResultType> {
        ResultType execute(ArgsType args);
    }

    public interface OperationWith2Args<Arg1Type, Arg2Type, ResultType> {
        ResultType execute(Arg1Type arg1, Arg2Type arg2);
    }
}
