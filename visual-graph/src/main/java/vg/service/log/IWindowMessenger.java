package vg.service.log;

/**
 * Interface for window message.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface IWindowMessenger {
	void errorMessage(String text, String title, WindowMessengerCallBack callBack);

	/**
	 * This method shows dialog window with error.
	 * Note: Additionally, it logs the error.
	 */
	void errorMessage(String text, String title, Throwable ex, WindowMessengerCallBack callBack);

    /**
	 * This method shows window with warning. 
	 * Note: Additionally, it logs the warning.
	 */
	void warningMessage(String text, String title, WindowMessengerCallBack callBack);

    /**
	 * This method shows window with information.
	 * Note: Additionally, it logs the message. 
	 */
	void infoMessage(String text, String title);

    void infoMessage(String text);

	interface WindowMessengerCallBack {
		void execute();
	}
}
