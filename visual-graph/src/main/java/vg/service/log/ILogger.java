package vg.service.log;

/**
 * Interface for logger.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface ILogger {
    /**
     * This method prints debug message.
     */
    void printDebug(String message);

    /**
     * This method prints debug message.
     */
    void printDebug(String message, Throwable ex);

    /**
     * This method prints info message.
     */
    void printInfo(String message);

    /**
     * This method prints error message.
     */
    void printError(String message);

    /**
     * This method prints error message.
     */
    void printError(String message, Throwable ex);

    /**
     * Prints the exception.
     */
    void printException(Throwable ex);

    /**
     * This method returns information about current logger.
     */
    String getLoggerInfo();

    /**
     * TODO: Should be moved to MainService.
     */
    boolean isDebugMode();
}
