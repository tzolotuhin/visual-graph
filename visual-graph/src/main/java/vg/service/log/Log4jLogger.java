package vg.service.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class Log4jLogger implements ILogger {
    private static final Logger LOGGER = LoggerFactory.getLogger(Log4jLogger.class);

    @Override
    public void printDebug(String message) {
        getLogger().debug(message);
    }

    @Override
    public void printDebug(String message, Throwable ex) {
        getLogger().debug(message, ex);
    }

    @Override
    public void printInfo(String message) {
        getLogger().info(message);
    }

    @Override
    public void printError(String message) {
        LOGGER.error(message);
    }

    @Override
    public void printError(String message, Throwable ex) {
        getLogger().error(message, ex);
    }

    @Override
    public void printException(Throwable ex) {
        var message = "Error.";
        if (ex != null) {
            message = ex.getMessage();
        }

        getLogger().error(message, ex);
    }

    @Override
    public String getLoggerInfo() {
        // TODO: необходимо добавить логирование в файл,
        // а так же даавать ссылку на него при вызове данного метода
        return "log4j logger";
    }

    @Override
    public boolean isDebugMode() {
        return true;
    }

    private Logger getLogger() {
        try {
            StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
            Class<?> clazz = Class.forName(stackTraceElements[3].getClassName());
            return LoggerFactory.getLogger(clazz);
        } catch (ClassNotFoundException ex) {
            return LOGGER;
        }
    }
}
