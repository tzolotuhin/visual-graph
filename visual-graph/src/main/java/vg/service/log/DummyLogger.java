package vg.service.log;

public class DummyLogger implements ILogger {
    @Override
    public void printDebug(String message) {

    }

    @Override
    public void printDebug(String message, Throwable ex) {

    }

    @Override
    public void printInfo(String message) {

    }

    @Override
    public void printError(String message) {

    }

    @Override
    public void printError(String message, Throwable ex) {

    }

    @Override
    public void printException(Throwable ex) {

    }

    @Override
    public String getLoggerInfo() {
        return null;
    }

    @Override
    public boolean isDebugMode() {
        return true;
    }
}
