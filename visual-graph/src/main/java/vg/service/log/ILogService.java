package vg.service.log;

/**
 * Interface for log manager.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface ILogService {
	/**
	 * Setup logger.
	 */
	boolean setLogger(ILogger logger);
	
	/**
	 * Setup window messages.
	 */
	boolean setWindowMessenger(IWindowMessenger windowMessenger);

	/**
	 * Returns current logger. 
	 * Can't return <b>null</b>.
	 */
	ILogger getLogger();
	
	/**
	 * Returns current window message.
	 */
	IWindowMessenger getWindowMessenger();
}
