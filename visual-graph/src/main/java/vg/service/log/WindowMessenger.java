package vg.service.log;

import vg.service.main.MainService;

import javax.swing.*;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class WindowMessenger implements IWindowMessenger {
    @Override
    public void errorMessage(String text, String title, WindowMessengerCallBack callBack) {
        errorMessage(text, title, null, callBack);
    }

    @Override
    public void errorMessage(String text, String title, Throwable ex, WindowMessengerCallBack callBack) {
        MainService.logger.printError(String.format("Window messenger, title '%s', text '%s'.", title, text), ex);
        MainService.executorService.executeInEDT(() -> {
            String msg = String.format("%s\n\nAdditional information : \n%s", text, MainService.logger.getLoggerInfo());
            Object[] options = {"Close"};
            JOptionPane.showOptionDialog(
                    null,
                    msg,
                    title,
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
            if (callBack != null) {
                callBack.execute();
            }
        });
    }

    @Override
    public void warningMessage(String text, String title, WindowMessengerCallBack callBack) {
        MainService.logger.printError(text);
        MainService.executorService.executeInEDT(() -> {
            String msg = String.format("%s\n\nAdditional information : \n%s", text, MainService.logger.getLoggerInfo());
            Object[] options = {"Close"};
            JOptionPane.showOptionDialog(
                    null,
                    msg,
                    title,
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.WARNING_MESSAGE,
                    null,
                    options,
                    options[0]);
            if (callBack != null) {
                callBack.execute();
            }
        });
    }

    @Override
    public void infoMessage(String text, String title) {
        MainService.logger.printInfo(text);
        MainService.executorService.executeInEDT(() -> {
            String msg = String.format("%s\n\nAdditional information : \n%s", text, MainService.logger.getLoggerInfo());
            Object[] options = {"Close"};
            JOptionPane.showOptionDialog(
                    null,
                    msg,
                    title,
                    JOptionPane.DEFAULT_OPTION,
                    JOptionPane.INFORMATION_MESSAGE,
                    null,
                    options,
                    options[0]);
        });
    }

    @Override
    public void infoMessage(String text) {
        infoMessage(text, "Info");
    }
}
