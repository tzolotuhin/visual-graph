package vg.service.source;

import vg.service.main.MainService;
import vg.service.ui.UserInterfaceService;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class SourceView implements UserInterfaceService.UserInterfaceTab {
    // Main components
    private JPanel outView, innerView;

    // Mutex
    private final Object generalMutex = new Object();

    // Main data
    private String title;
    private String currentText;
    private int currentLineNumber;

    public SourceView(String title) {
        this.title = title;

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        // rebuild view
        rebuildView();
    }

    @Override
    public String getTabTitle() {
        return title;
    }

    @Override
    public void setTabTitle(String title) {
        this.title = title;
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

    private void rebuildView() {
        innerView.removeAll();

        JTextArea textArea = new JTextArea(currentText);
        JScrollPane scrollPane = new JScrollPane(textArea);

        Highlighter h = textArea.getHighlighter();
        h.removeAllHighlights();
        try {
            int startIndex = textArea.getLineStartOffset(currentLineNumber);
            int endIndex = textArea.getLineEndOffset(currentLineNumber);
            h.addHighlight(startIndex, endIndex, DefaultHighlighter.DefaultPainter);
        } catch (BadLocationException ex) {
            MainService.logger.printException(ex);
        }

        innerView.add(scrollPane, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

        innerView.updateUI();
    }

    private void doShow(String fileName, final int lineNumber) {
        try {
            final byte[] encoded = Files.readAllBytes(Paths.get(fileName));
            final String tmpCurrentText = new String(encoded, StandardCharsets.UTF_8);
            MainService.executorService.executeInEDT(() -> {
                synchronized (generalMutex) {
                    currentText = tmpCurrentText;
                    currentLineNumber = lineNumber;
                    rebuildView();
                }
            });
        } catch (IOException ex) {
            MainService.logger.printException(ex);
        }
    }
}
