package vg.service.gview;

import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;
import vg.service.executor.ExecutorService;
import vg.service.main.MainService;
import vg.shared.gui.components.SearchBarPanel;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import java.awt.*;
import java.util.List;

public class GraphViewStatusPanel {
    // Components
    private final JPanel innerView, outView;

    private final JLabel infoLabel;

    private final JTextArea consoleTextArea;
    private final JScrollPane consoleTextAreaScrollPane;
    private final SearchBarPanel searchBarPanel;

    protected final Object generalMutex = new Object();

    // Data
    @Setter
    private String name = "";
    @Setter
    private String info = "";
    @Setter
    private int vertexAmount = 0;
    @Setter
    private int edgeAmount = 0;
    @Setter
    private int zoomLevel = 100;
    @Setter
    private String consoleText;

    private List<SearchResultInConsoleText> searchResultsInConsoleText;
    private int searchResultsInConsoleTextIndex;

    public GraphViewStatusPanel() {
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        infoLabel = new JLabel();

        consoleTextArea = new JTextArea();
        consoleTextArea.setEditable(false);
        consoleTextAreaScrollPane = new JScrollPane(consoleTextArea);
        searchBarPanel = new SearchBarPanel(false, false, true, false, false, true);
        searchBarPanel.addActionListenerForSearchBarStrField(this::searchStringInTextAction);

        searchBarPanel.addActionListenerForSearchBarNextButton(l -> {
            synchronized (generalMutex) {
                if (searchResultsInConsoleText.isEmpty()) {
                    searchResultsInConsoleTextIndex = -1;
                    return;
                }

                searchResultsInConsoleTextIndex++;
                if (searchResultsInConsoleTextIndex >= searchResultsInConsoleText.size()) {
                    searchResultsInConsoleTextIndex = 0;
                }

                onSelectSubStringInConsoleTextArea();
            }
        });

        searchBarPanel.addActionListenerForSearchBarPrevButton(l -> {
            synchronized (generalMutex) {
                if (searchResultsInConsoleText.isEmpty()) {
                    searchResultsInConsoleTextIndex = -1;
                    return;
                }

                searchResultsInConsoleTextIndex--;
                if (searchResultsInConsoleTextIndex < 0) {
                    searchResultsInConsoleTextIndex = searchResultsInConsoleText.size() - 1;
                }

                onSelectSubStringInConsoleTextArea();
            }
        });

        rebuild();
    }

    private void searchStringInTextAction() {
        synchronized (generalMutex) {
            var localConsoleText = searchBarPanel.isMatchCaseSelected() ? consoleTextArea.getText() : consoleTextArea.getText().toLowerCase();
            var localStr = searchBarPanel.isMatchCaseSelected() ? searchBarPanel.getStr() : searchBarPanel.getStr().toLowerCase();

            MainService.executorService.execute(new ExecutorService.SwingExecutor() {
                @Override
                public void doInBackground() {
                    synchronized (generalMutex) {
                        searchResultsInConsoleText = Lists.newArrayList();
                        searchResultsInConsoleTextIndex = -1;

                        int length = localStr.length();
                        if (length == 0) {
                            return;
                        }

                        int index = 0;
                        while (index >= 0) {
                            index = localConsoleText.indexOf(localStr, index);

                            if (index >= 0) {
                                searchResultsInConsoleText.add(new SearchResultInConsoleText(index, index + length));
                                searchResultsInConsoleTextIndex = 0;
                                index += length;
                            }
                        }

                        if (!searchResultsInConsoleText.isEmpty()) {
                            searchResultsInConsoleTextIndex = 0;
                        }
                    }
                }

                @Override
                public void doInEDT() {
                    synchronized (generalMutex) {
                        onSelectSubStringInConsoleTextArea();
                    }
                }
            });
        }
    }

    public void updateConsoleText(String text) {
        consoleText = text;

        updateUI();
    }

    public void updateUI() {
        consoleTextArea.setText(consoleText);
        infoLabel.setText(String.format("Name: %s | V=%s | E=%s | Zoom = %s%% | %s", name, vertexAmount, edgeAmount, zoomLevel, info));

        searchStringInTextAction();

        innerView.updateUI();
    }

    private void onSelectSubStringInConsoleTextArea() {
        synchronized (generalMutex) {
            // update current index and amount for the search bar panel.
            searchBarPanel.setCurrIndex(searchResultsInConsoleTextIndex);
            searchBarPanel.setAmount(searchResultsInConsoleText.size());
            searchBarPanel.updateUI();

            // select the results for the console text area.
            var highlighter = consoleTextArea.getHighlighter();
            highlighter.removeAllHighlights();
            var painter = new DefaultHighlighter.DefaultHighlightPainter(Color.pink);
            var selectPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.yellow);
            try {
                for(int i = 0; i < searchResultsInConsoleText.size(); i++) {
                    var searchResultInConsoleText = searchResultsInConsoleText.get(i);
                    if (i == searchResultsInConsoleTextIndex) {
                        highlighter.addHighlight(searchResultInConsoleText.start, searchResultInConsoleText.finish, selectPainter);
                        consoleTextArea.setCaretPosition(searchResultInConsoleText.start);
                    } else {
                        highlighter.addHighlight(searchResultInConsoleText.start, searchResultInConsoleText.finish, painter);
                    }
                }
            } catch (BadLocationException ex) {
                MainService.logger.printException(ex);
            }
        }
    }

    private void rebuild() {
        innerView.removeAll();

        innerView.add(
                searchBarPanel.getView(),
                new GridBagConstraints(
                        0,
                        0,
                        1,
                        1,
                        1,
                        0,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 0),
                        0,
                        0));
        innerView.add(
                consoleTextAreaScrollPane,
                new GridBagConstraints(
                        0,
                        1,
                        1,
                        1,
                        1,
                        1,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0),
                        0,
                        0));

        innerView.add(
                infoLabel,
                new GridBagConstraints(
                        0,
                        2,
                        1,
                        1,
                        1,
                        0,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 0),
                        0,
                        0));

        updateConsoleText(consoleText);
    }

    public JPanel getView() {
        return outView;
    }

    private static class SearchResultInConsoleText {
        @Getter
        private final int start;
        @Getter
        private final int finish;

        public SearchResultInConsoleText(int start, int finish) {
            this.start = start;
            this.finish = finish;
        }
    }
}
