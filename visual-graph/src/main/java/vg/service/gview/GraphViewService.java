package vg.service.gview;

import vg.lib.common.CallBack;
import vg.lib.model.graph.Graph;

import java.util.List;

public interface GraphViewService {
    // TODO: Need remove arg 'recursively'.
    void asyncOpenGraphInTab(Graph graph, boolean recursively, CallBack<GraphViewTab> callBack);

    void asyncOpenGraphInTab(List<Graph> graphs, CallBack<GraphViewTab> callBack);

    void asyncOpenGraphViewInTab(GraphViewTab graphViewTab, CallBack<GraphViewTab> callBack);
}
