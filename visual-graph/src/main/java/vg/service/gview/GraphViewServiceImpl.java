package vg.service.gview;

import vg.lib.common.CallBack;
import vg.lib.model.graph.Graph;
import vg.service.executor.ExecutorService;
import vg.service.main.MainService;
import vg.shared.graph.utils.GraphUtils;

import java.util.Collections;
import java.util.List;

public class GraphViewServiceImpl implements GraphViewService {
    @Override
    public void asyncOpenGraphViewInTab(GraphViewTab graphViewTab, CallBack<GraphViewTab> callBack) {
        MainService.executorService.executeInEDT(() ->
                MainService.userInterfaceService.addTab(graphViewTab, () -> MainService.executorService.execute(() -> {
                    try {
                        MainService.userInterfaceService.addTab(graphViewTab, () -> {
                            if (callBack != null) {
                                callBack.callingBack(graphViewTab);
                            }
                        });
                    } catch (Throwable ex) {
                        MainService.logger.printException(ex);
                    }
                })));
    }

    @Override
    public void asyncOpenGraphInTab(Graph graph, boolean recursively, CallBack<GraphViewTab> callBack) {
        var graphName = GraphUtils.getVertexName(graph.getAttributes());
        MainService.executorService.executeInEDT(() -> {
            var graphViewTab = new GraphViewTabImpl(graphName);
            MainService.userInterfaceService.addTab(
                    graphViewTab,
                    () -> MainService.executorService.execute(() -> asyncOpenGraphInGraphView(
                            Collections.singletonList(graph),
                            graphViewTab,
                            recursively,
                            callBack)));
        });
    }

    @Override
    public void asyncOpenGraphInTab(List<Graph> graphs, CallBack<GraphViewTab> callBack) {
        MainService.executorService.executeInEDT(() -> {
            var graphView = new GraphViewTabImpl("Please wait...");
            MainService.userInterfaceService.addTab(
                    graphView,
                    () -> MainService.executorService.execute(() -> asyncOpenGraphInGraphView(
                            graphs,
                            graphView,
                            false,
                            callBack)));
        });
    }

    private void asyncOpenGraphInGraphView(List<Graph> graphs,
                                           GraphViewTab graphViewTab,
                                           boolean recursively,
                                           CallBack<GraphViewTab> callBack) {
        MainService.logger.printInfo(String.format("Try to open graphs (%d) in graph view tab.", graphs.size()));

        for (var graph : graphs) {
            MainService.logger.printDebug(
                    String.format(
                            "The graph contains %s vertices and %s edges.",
                            graph.getAllVertices().size(),
                            graph.getAllEdges().size()));
        }

        MainService.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                if (recursively) {
                    MainService.logger.printDebug("Download children...");
                    graphViewTab.lock("Please wait. Download children recursively...");

                    for (var graph : graphs) {
                        GraphUtils.downloadChildren(graph);
                    }

                    graphViewTab.unlock();
                }

                graphViewTab.clear();

                for (var graph : graphs) {
                    GraphUtils.resetDbIdToVertexIds(graph);

                    graphViewTab.addGraph(graph);
                }

                var graphName = GraphUtils.getVertexName(graphs.get(0).getAttributes());

                graphViewTab.setTabTitle(graphName);
                graphViewTab.setName(graphName);
            }

            @Override
            public void doInEDT() {
                if (callBack != null) {
                    callBack.callingBack(graphViewTab);
                }
            }
        });
    }
}
