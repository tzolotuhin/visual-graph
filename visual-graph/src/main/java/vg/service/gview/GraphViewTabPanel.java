package vg.service.gview;

import org.apache.commons.lang3.Validate;
import vg.lib.config.VGConfigSettings;
import vg.lib.view.GraphViewModel;
import vg.lib.view.GraphViewPanel;
import vg.service.main.MainService;
import vg.shared.gui.components.SearchBarPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class GraphViewTabPanel {
    // Data
    private final GraphViewModel graphViewModel;
    private final GraphViewPanel graphViewPanel;

    private final ChangeStateListener changeStateListener;

    private boolean selectionFrameIsVisible;

    // Components
    private final JScrollPane scrollPane;
    private final JPopupMenu popupMenu;

    private final SearchBarPanel graphViewSearchBarPanel;
    private final GraphViewStatusPanel graphViewStatusPanel;

    private final JLabel infoLabel;

    private final JPanel innerView, outView;

    GraphViewTabPanel(GraphViewModel graphViewModel,
                      GraphViewPanel graphViewPanel,
                      ChangeStateListener changeStateListener) {
        // init parameters.
        this.graphViewModel = graphViewModel;
        this.graphViewPanel = graphViewPanel;
        this.changeStateListener = changeStateListener;

        // init components.
        scrollPane = new JScrollPane(graphViewPanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(18);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(24);

        graphViewSearchBarPanel = new SearchBarPanel(true, true, true, true, true, true);
        graphViewStatusPanel = new GraphViewStatusPanel();

        var graphViewStatusPanelSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollPane, graphViewStatusPanel.getView());
        graphViewStatusPanelSplitPane.setDividerLocation(760);

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        infoLabel = new JLabel("Please wait...");
        infoLabel.setHorizontalAlignment(SwingConstants.CENTER);

        JMenuItem openInNewTabMenuItem = new JMenuItem("Open in new tab");
        JMenuItem copyIdMenuItem = new JMenuItem("Copy id");
        JMenuItem copyTextMenuItem = new JMenuItem("Copy text");
        JMenuItem showNeighborhoodsMenuItem = new JMenuItem("Show neighborhoods");
        JMenuItem exportMenuItem = new JMenuItem("Export");
        JMenuItem collapseMenuItem = new JMenuItem("Collapse");
        JMenuItem uncollapseMenuItem = new JMenuItem("Uncollapse");

        JMenu showGridMenuItem = new JMenu("Show grid");
        JMenuItem showGridUnderTheGraphMenuItem = new JMenuItem("Under the graph");
        JMenuItem showGridAboveTheGraphMenuItem = new JMenuItem("Above the graph");
        JMenuItem showGridDisableMenuItem = new JMenuItem("Disable");
        showGridMenuItem.add(showGridUnderTheGraphMenuItem);
        showGridMenuItem.add(showGridAboveTheGraphMenuItem);
        showGridMenuItem.add(showGridDisableMenuItem);

        JMenu showMinimapMenuItem = new JMenu("Show minimap");
        JMenuItem showMinimapEnableMenuItem = new JMenuItem("Enable");
        JMenuItem showMinimapDisableMenuItem = new JMenuItem("Disable");
        showMinimapMenuItem.add(showMinimapEnableMenuItem);
        showMinimapMenuItem.add(showMinimapDisableMenuItem);

        openInNewTabMenuItem.addActionListener(e -> changeStateListener.onOpenInNewTab());
        copyIdMenuItem.addActionListener(e -> changeStateListener.onCopyId());
        copyTextMenuItem.addActionListener(e -> changeStateListener.onCopyText());
        showNeighborhoodsMenuItem.addActionListener(e -> changeStateListener.onShowNeighborhoods());
        exportMenuItem.addActionListener(e -> changeStateListener.onExport());
        collapseMenuItem.addActionListener(e -> changeStateListener.onCollapse());
        uncollapseMenuItem.addActionListener(e -> changeStateListener.onUncollapse());
        showGridUnderTheGraphMenuItem.addActionListener(e -> changeStateListener.onChangeGridType(VGConfigSettings.UNDER_THE_GRAPH_GRID_TYPE));
        showGridAboveTheGraphMenuItem.addActionListener(e -> changeStateListener.onChangeGridType(VGConfigSettings.ABOVE_THE_GRAPH_GRID_TYPE));
        showGridDisableMenuItem.addActionListener(e -> changeStateListener.onChangeGridType(VGConfigSettings.DISABLE_GRID_TYPE));
        showMinimapEnableMenuItem.addActionListener(e -> changeStateListener.onChangeVisibilityOfMinimap(true));
        showMinimapDisableMenuItem.addActionListener(e -> changeStateListener.onChangeVisibilityOfMinimap(false));

        popupMenu = new JPopupMenu();
        popupMenu.add(openInNewTabMenuItem);
        popupMenu.add(copyIdMenuItem);
        popupMenu.add(copyTextMenuItem);
        popupMenu.add(showNeighborhoodsMenuItem);
        popupMenu.add(showGridMenuItem);
        popupMenu.add(showMinimapMenuItem);
        popupMenu.add(exportMenuItem);
        // TODO: Please implement...
        //popupMenu.add(collapseMenuItem);
        //popupMenu.add(uncollapseMenuItem);

        innerView.add(
                graphViewSearchBarPanel.getView(),
                new GridBagConstraints(
                        0,
                        0,
                        1,
                        1,
                        1,
                        0,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.HORIZONTAL,
                        new Insets(0, 0, 0, 0),
                        0,
                        0));
        innerView.add(
                graphViewStatusPanelSplitPane,
                new GridBagConstraints(
                        0,
                        1,
                        1,
                        1,
                        1,
                        1,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 0),
                        0,
                        0));

        scrollPane.getViewport().addChangeListener(e -> changeStateListener.onChangeViewport((scrollPane.getViewport().getViewRect())));

        graphViewSearchBarPanel.addActionListenerForSearchBarStrField(() -> changeStateListener.onSearch(
                graphViewSearchBarPanel.getStr(),
                graphViewSearchBarPanel.isSearchAmongVerticesSelected(),
                graphViewSearchBarPanel.isSearchAmongEdgesSelected(),
                graphViewSearchBarPanel.isMatchCaseSelected(),
                graphViewSearchBarPanel.isWordsSelected(),
                graphViewSearchBarPanel.isRegexSelected()));

        graphViewSearchBarPanel.addActionListenerForSearchBarNextButton(e -> changeStateListener.onNextSearchResult());

        graphViewSearchBarPanel.addActionListenerForSearchBarPrevButton(e -> changeStateListener.onPrevSearchResult());

        graphViewPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    selectionFrameIsVisible = false;
                    changeStateListener.onSelectFinish(e.getPoint(), e.isControlDown(), e.isShiftDown());
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    popupMenu.show(e.getComponent(), e.getX(), e.getY());
                }

                GraphViewTabPanel.this.graphViewPanel.setFocusable(true);
                GraphViewTabPanel.this.graphViewPanel.requestFocusInWindow();

                if (e.getButton() == MouseEvent.BUTTON1) {
                    selectionFrameIsVisible = true;
                    changeStateListener.onSelectStart(e.getPoint());
                    changeStateListener.onSelect(e.getPoint(), e.isControlDown(), e.isShiftDown());
                }
            }
        });
        graphViewPanel.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (selectionFrameIsVisible) {
                    changeStateListener.onSelect(e.getPoint(), e.isControlDown(), e.isShiftDown());
                }
            }
        });

        graphViewPanel.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_A && (e.isControlDown() || e.isMetaDown())) {
                    changeStateListener.onSelectAll();
                }
            }
        });
    }

    void lock(String text) {
        outView.remove(innerView);
        outView.add(infoLabel);
        infoLabel.setText(text);
        outView.updateUI();
    }

    void unlock() {
        outView.remove(infoLabel);
        outView.add(innerView);
        outView.updateUI();
    }

    void updateConsoleText(String text) {
        graphViewStatusPanel.updateConsoleText(text);
    }

    Rectangle getViewport() {
        return scrollPane.getViewport().getViewRect();
    }

    BufferedImage getImage() {
        return graphViewPanel.getSharedImage();
    }

    public void update() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "Method 'GraphViewComponent.update' shouldn't be called from EDT.");

        graphViewPanel.update();

        graphViewStatusPanel.setName(graphViewModel.getName());
        graphViewStatusPanel.setInfo(graphViewModel.getInfo());
        graphViewStatusPanel.setVertexAmount(graphViewModel.getVertices().size());
        graphViewStatusPanel.setEdgeAmount(graphViewModel.getEdges().size());
        graphViewStatusPanel.setEdgeAmount(graphViewModel.getEdges().size());
        graphViewStatusPanel.setZoomLevel(Math.round(100 * graphViewModel.getZoomLevel()));

        graphViewSearchBarPanel.setCurrIndex(graphViewModel.getSearchBarResultsIndex());
        graphViewSearchBarPanel.setAmount(graphViewModel.getSearchBarResults().size());

        MainService.executorService.executeInEDT(() -> {
            graphViewStatusPanel.updateUI();
            graphViewSearchBarPanel.updateUI();
            scrollPane.updateUI();
        });
    }

    public void moveViewport(Rectangle viewport) {
        Validate.isTrue(SwingUtilities.isEventDispatchThread(), "Method 'GraphViewComponent.update' should be called from EDT.");
        Validate.notNull(viewport, "Viewport shouldn't be null.");

        try {
            scrollPane.getHorizontalScrollBar().setValue(viewport.x);
            scrollPane.getVerticalScrollBar().setValue(viewport.y);
        } catch (Throwable ex) {
            MainService.logger.printException(ex);
        }
        changeStateListener.onChangeViewport(scrollPane.getViewport().getViewRect());
    }

    public JPanel getView() {
        return outView;
    }

    public interface ChangeStateListener {
        void onSearch(
                String searchBarStr,
                boolean searchAmongVertices,
                boolean searchAmongEdges,
                boolean matchCase,
                boolean words,
                boolean isRegex);
        void onNextSearchResult();
        void onPrevSearchResult();

        void onOpenInNewTab();
        void onCopyId();
        void onCopyText();
        void onChangeViewport(Rectangle rectangle);
        void onExport();
        void onSelectStart(Point start);
        void onSelect(Point point, boolean ctrlDown, boolean shiftDown);
        void onSelectFinish(Point finish, boolean ctrlDown, boolean shiftDown);
        void onSelectAll();
        void onShowNeighborhoods();
        void onCollapse();
        void onUncollapse();
        void onChangeGridType(int gridType);
        void onChangeVisibilityOfMinimap(boolean value);
    }
}
