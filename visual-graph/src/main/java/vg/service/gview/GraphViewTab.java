package vg.service.gview;

import vg.lib.view.GraphView;
import vg.service.ui.UserInterfaceService;

public interface GraphViewTab extends GraphView, UserInterfaceService.UserInterfaceTab { }
