package vg.service.gview;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import lombok.NonNull;
import org.apache.commons.lang3.Validate;
import vg.lib.common.CallBack;
import vg.lib.model.graph.Attribute;
import vg.lib.view.GraphView;
import vg.lib.view.GraphViewImpl;
import vg.lib.view.GraphViewModel;
import vg.lib.view.GraphViewPanel;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVObject;
import vg.lib.view.elements.GVVertex;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.ExportWindow;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.GVGraphMLExporter;
import vg.service.executor.ExecutorService;
import vg.service.main.MainService;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.UUID;
import java.util.stream.Collectors;

import static vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.ExportWindow.DEF_LAST_OPEN_GRAPH_DIR;

public class GraphViewTabImpl extends GraphViewImpl implements GraphViewTab {
    private final GraphViewTabPanel graphViewTabPanel;

    public GraphViewTabImpl(@NonNull String tabTitle) {
        this(new GraphViewModel(tabTitle));
    }

    private GraphViewTabImpl(@NonNull GraphViewModel graphViewModel) {
        this(graphViewModel, new GraphViewPanel(graphViewModel));
    }

    private GraphViewTabImpl(@NonNull GraphViewModel graphViewModel, @NonNull GraphViewPanel graphViewPanel) {
        super(graphViewModel, graphViewPanel);

        var changeStateListener = new GraphViewTabPanel.ChangeStateListener() {
            @Override
            public void onShowNeighborhoods() {
                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        var selectedVertices = graphViewModel.getSelectedVertices();
                        var edges = graphViewModel.getEdges();

                        var reVertices = new ArrayList<GVVertex>();
                        var reEdges = new ArrayList<GVEdge>();
                        for (var edge : edges) {
                            for (var selectedVertex : selectedVertices) {
                                GVVertex neighborhood = null;
                                if (edge.getSrcId() == selectedVertex.getId()) {
                                    neighborhood = graphViewModel.getVertexById(edge.getTrgId());
                                }
                                if (edge.getTrgId() == selectedVertex.getId()) {
                                    neighborhood = graphViewModel.getVertexById(edge.getSrcId());
                                }

                                if (neighborhood != null) {
                                    neighborhood.setSelect(true);
                                    edge.setSelect(true);

                                    if (!reVertices.contains(neighborhood)) {
                                        reVertices.add(neighborhood);
                                    }

                                    if (!reEdges.contains(edge)) {
                                        reEdges.add(edge);
                                    }
                                }
                            }
                        }

                        graphViewTabPanel.update();
                        doNotifyOnSelectElements(reVertices, reEdges);
                    }
                });
            }

            @Override
            public void onCopyText() {
                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        List<GVVertex> vertices = graphViewModel.getSelectedVertices();
                        if (vertices.size() > 0) {
                            MainService.userInterfaceService.copyToClipboard(vertices.get(0).getShape().getLabel().getText());
                        }
                    }
                });
            }

            @Override
            public void onCopyId() {
                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        List<GVVertex> vertices = graphViewModel.getSelectedVertices();
                        if (vertices.size() > 0) {
                            Attribute attr = vertices.get(0).getAttribute(graphViewModel.getDefaultIdAttribute());
                            if (attr == null) {
                                return;
                            }

                            MainService.userInterfaceService.copyToClipboard(attr.getStringValue());
                        }
                    }
                });
            }

            @Override
            public void onExport(){
                List<GVVertex> vertices;
                List<GVEdge> edges;
                if(graphViewModel.getSelectedVertices().size() != 0) {
                    vertices = graphViewModel.getSelectedVertices();
                    edges = graphViewModel.getSelectedEdges();
                }else{
                    vertices = graphViewModel.getVertices();
                    edges = graphViewModel.getEdges();
                }

                ExportWindow exportWindow = new ExportWindow();
                JFileChooser fileChooser = exportWindow.getFileChooser();

                // show save dialog
                if(fileChooser.showSaveDialog(MainService.userInterfaceService.getMainFrame()) == JFileChooser.APPROVE_OPTION){
                    File fileToSave = fileChooser.getSelectedFile();
                    File fullName = new File(fileToSave.getPath() + ".graphml");
                    MainService.logger.printDebug("Export to graphml requested");
                    GVGraphMLExporter exporter = new GVGraphMLExporter(vertices, edges, getTabTitle(), fullName);
                    MainService.executorService.execute(exporter);
                    MainService.config.setProperty(DEF_LAST_OPEN_GRAPH_DIR, fileChooser.getCurrentDirectory().getAbsolutePath());
                }
            }

            @Override
            public void onOpenInNewTab() {
                GraphViewTabImpl graphView = new GraphViewTabImpl(getTabTitle());

                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        Map<UUID, UUID> oldToNewVertexId = Maps.newHashMap();

                        // get elements in one parent.
                        var vertices = graphViewModel.getElementsInOneParent(graphViewModel.getSelectedVertices());
                        var edges = graphViewModel.getEdges(vertices);

                        vertices = vertices.stream().map(GVVertex::new).collect(Collectors.toList());
                        edges = edges.stream().map(GVEdge::new).collect(Collectors.toList());

                        // reset parent id for the elements.
                        vertices.forEach(x -> x.setParentId(null));
                        edges.forEach(x -> x.setParentId(null));

                        // downloads children.
                        Queue<GVVertex> queue = Queues.newArrayDeque(vertices);
                        while (!queue.isEmpty()) {
                            var parentWithElements = getElements(queue.poll().getId());

                            // add new vertices to the result.
                            for (var vertex : parentWithElements.getVertices()) {
                               int index = vertices.indexOf(vertex);
                               if (index >= 0) {
                                   vertices.set(index, vertex);
                               } else {
                                   vertices.add(vertex);
                                   queue.add(vertex);
                               }
                            }

                            // add new edges to the result.
                            for (var edge : parentWithElements.getEdges()) {
                                int index = edges.indexOf(edge);
                                if (index >= 0) {
                                    edges.set(index, edge);
                                } else {
                                    edges.add(edge);
                                }
                            }
                        }

                        // new elements.
                        List<GVVertex> newVertices = Lists.newArrayList();
                        List<GVEdge> newEdges = Lists.newArrayList();

                        // build new graph using the elements.
                        vertices.forEach(x -> {
                            UUID oldId = x.getId();
                            if (x.getParentId() != null) {
                                x.setParentId(oldToNewVertexId.get(x.getParentId()));
                            }

                            var newVertex = graphView.addVertex(x);
                            oldToNewVertexId.put(oldId, newVertex.getId());

                            newVertices.add(newVertex);
                        });

                        edges.forEach(x -> {
                            if (x.getParentId() != null) {
                                x.setParentId(oldToNewVertexId.get(x.getParentId()));
                            }

                            x.setSrcId(oldToNewVertexId.get(x.getSrcId()));
                            x.setTrgId(oldToNewVertexId.get(x.getTrgId()));

                            if (x.getSrcPortId() != null) {
                                x.setSrcPortId(oldToNewVertexId.get(x.getSrcPortId()));
                            }
                            if (x.getTrgPortId() != null) {
                                x.setTrgPortId(oldToNewVertexId.get(x.getTrgPortId()));
                            }

                            var newEdge = graphView.addEdge(x);

                            newEdges.add(newEdge);
                        });

                        MainService.graphViewService.asyncOpenGraphViewInTab(
                                graphView,
                                it -> ((GraphViewTabImpl) it).doNotifyOnAddElements(newVertices, newEdges));
                    }
                });
            }

            @Override
            public void onCollapse() {
                MainService.executorService.execute(() -> collapse(graphViewModel.getSelectedVertices()));
            }

            @Override
            public void onUncollapse() {
                MainService.executorService.execute(() -> {
                    var selectedElements = graphViewModel.getSelectedVertices();

                    if (selectedElements.size() != 1) {
                        return;
                    }

                    uncollapse(graphViewModel.getSelectedVertices().iterator().next());
                });
            }

            @Override
            public void onChangeGridType(int gridType) {
                MainService.executorService.execute(() -> {
                    showGrid(gridType);
                    graphViewTabPanel.update();
                });
            }

            @Override
            public void onChangeVisibilityOfMinimap(boolean value) {
                MainService.executorService.execute(() -> {
                    showMinimap(value);
                    graphViewTabPanel.update();
                });
            }

            private void onSelectSearchedGVObject(GVObject gvObject) {
                if (gvObject == null) {
                    // do nothing.
                    return;
                }

                // calculate possible value of viewport.
                var viewport = graphViewModel.getViewport();

                if (gvObject instanceof GVVertex) {
                    var gvVertex = (GVVertex)gvObject;
                    double dx = gvVertex.getShape().getAbsoluteCenterX() - viewport.getCenterX();
                    double dy = gvVertex.getShape().getAbsoluteCenterY() - viewport.getCenterY();

                    viewport.x += dx;
                    viewport.y += dy;
                }

                MainService.executorService.executeInEDT(() -> graphViewTabPanel.moveViewport(viewport));
            }

            @Override
            public void onSearch(String searchBarStr,
                                 boolean searchAmongVertices,
                                 boolean searchAmongEdges,
                                 boolean matchCase,
                                 boolean words,
                                 boolean isRegex) {
                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        var selectedElements = graphViewModel.searchElements(
                                searchBarStr,
                                searchAmongVertices,
                                searchAmongEdges,
                                matchCase,
                                words,
                                isRegex);

                        var selectedElement = graphViewModel.nextSearchBarResult();

                        graphViewTabPanel.update();

                        doNotifyOnSelectElements(
                                selectedElements.getKey(),
                                selectedElements.getValue());

                        onSelectSearchedGVObject(selectedElement);
                    }
                });
            }

            @Override
            public void onNextSearchResult() {
                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        var selectedElement = graphViewModel.nextSearchBarResult();
                        graphViewTabPanel.update();
                        onSelectSearchedGVObject(selectedElement);
                    }
                });
            }

            @Override
            public void onPrevSearchResult() {
                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        var selectedElement = graphViewModel.prevSearchBarResult();
                        graphViewTabPanel.update();
                        onSelectSearchedGVObject(selectedElement);
                    }
                });
            }

            @Override
            public void onChangeViewport(Rectangle rectangle) {
                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        Rectangle oldRectangle = graphViewModel.getViewport();
                        Rectangle newRectangle = graphViewTabPanel.getViewport();
                        if (!oldRectangle.equals(newRectangle)) {
                            graphViewModel.setViewport(newRectangle);
                            graphViewTabPanel.update();
                        }
                    }
                });
            }

            private String generateOnSelectKey() {
                return id + ":onSelect";
            }

            @Override
            public void onSelectStart(Point start) {
                MainService.executorService.executeLastTaskByKey(generateOnSelectKey(), () -> {
                    synchronized (generalMutex) {
                        graphViewModel.onSelectStart(start);
                    }
                });
            }

            @Override
            public void onSelect(Point point, boolean ctrlDown, boolean shiftDown) {
                MainService.executorService.executeLastTaskByKey(generateOnSelectKey(), () -> {
                    synchronized (generalMutex) {
                        graphViewModel.onSelect(point, ctrlDown, shiftDown);
                        graphViewTabPanel.update();
                    }
                });
            }

            @Override
            public void onSelectFinish(Point finish, boolean ctrlDown, boolean shiftDown) {
                MainService.executorService.executeLastTaskByKey(generateOnSelectKey(), () -> {
                    synchronized (generalMutex) {
                        var selectedElements
                                = graphViewModel.onSelect(finish, ctrlDown, shiftDown);
                        graphViewModel.onSelectFinish(finish);
                        graphViewTabPanel.update();

                        doNotifyOnSelectElements(selectedElements.getKey(), selectedElements.getValue());
                    }
                });
            }

            @Override
            public void onSelectAll() {
                MainService.executorService.execute(() -> {
                    synchronized (generalMutex) {
                        var selectedElements = graphViewModel.selectAllElements();
                        graphViewTabPanel.update();
                        doNotifyOnSelectElements(selectedElements.getKey(), selectedElements.getValue());
                    }
                });
            }
        };

        listeners.add(new GraphView.GVListener() {
            @Override
            public void onSelectElements(List<GVVertex> vertices, List<GVEdge> edges) {
                MainService.executorService.execute(new ExecutorService.SwingExecutor()  {
                    private StringBuilder localConsoleText;

                    @Override
                    public void doInBackground() {
                        localConsoleText = new StringBuilder();

                        vertices.forEach(vertex -> {
                            localConsoleText.append(vertex.getName());
                            localConsoleText.append(":\n");
                            vertex.getAttributes().forEach((attribute, mutableBoolean) -> {
                                localConsoleText.append(attribute.getName());
                                localConsoleText.append(":");
                                localConsoleText.append(attribute.getStringValue());
                                localConsoleText.append("\n");
                            });
                            localConsoleText.append("==================================================================\n");
                        });

                        edges.forEach(x -> {
                        });
                    }

                    @Override
                    public void doInEDT() {
                        graphViewTabPanel.updateConsoleText(localConsoleText.toString());
                    }
                });
            }
        });

        graphViewTabPanel = new GraphViewTabPanel(graphViewModel, graphViewPanel, changeStateListener);
    }

    //region: Implementation of GraphView methods.
    @Override
    public void lock(String text) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        MainService.logger.printDebug(String.format("Method 'GraphViewTabImpl.lock' was called, text: '%s'.", text));

        synchronized (generalMutex) {
            graphViewTabPanel.lock(text);
        }
    }

    @Override
    public void unlock() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        MainService.logger.printDebug("Method 'GraphViewTabImpl.unlock' was called.");

        synchronized (generalMutex) {
            graphViewTabPanel.unlock();
        }
    }
    //endregion

    //region: Implementation of UserInterfaceTab interface.
    @Override
    public String getTabTitle() {
        synchronized (generalMutex) {
            return getGraphViewModel().getName();
        }
    }

    @Override
    public void setTabTitle(String tabTitle) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            getGraphViewModel().setName(tabTitle);
        }
    }

    @Override
    public JPanel getView() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return graphViewTabPanel.getView();
        }
    }
    //endregion

    private void doNotifyOnSelectElements(List<GVVertex> vertices, List<GVEdge> edges) {
        // print info about selected elements.
        if (vertices != null && vertices.size() < 10) {
            var message = new StringBuilder();
            vertices.forEach(vertex -> {
                message.append("Vertex: ");
                message.append(vertex.getId());
                message.append("\n");
                for (var attr : vertex.getAttributes().keySet()) {
                    message.append("  ");
                    message.append(attr);
                    message.append("\n");
                }
            });

            MainService.logger.printDebug(message.toString());
        }

        if (edges != null && edges.size() < 10) {
            var message = new StringBuilder();
            edges.forEach(edge -> {
                message.append("Edge: ");
                message.append(edge.getId());
                message.append("\n");
                for (var attr : edge.getAttributes().keySet()) {
                    message.append("  ");
                    message.append(attr);
                    message.append("\n");
                }
            });

            MainService.logger.printDebug(message.toString());
        }

        listeners.forEach(x -> MainService.executorService.executeInEDT(() -> x.onSelectElements(vertices, edges)));
    }

    private void doNotifyOnAddElements(List<GVVertex> vertices, List<GVEdge> edges) {
        listeners.forEach(x -> MainService.executorService.executeInEDT(() -> x.onAddElements(vertices, edges)));
    }
}
