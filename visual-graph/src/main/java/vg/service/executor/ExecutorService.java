package vg.service.executor;

public interface ExecutorService {
    void execute(Runnable runnable);

    void executeLastTaskByKey(String taskKey, Runnable runnable);

    void executeTaskByKey(String taskKey, Runnable runnable);

    void execute(SwingExecutor swingExecutor);

    void executeInEDT(Runnable runnable);

    void shutdown();

    abstract class SwingExecutor {
        protected boolean successfully = true;

        public void doInBackground() {}
        public void doInEDT() {}

        public boolean isSuccessfully() {
            return successfully;
        }
    }
}
