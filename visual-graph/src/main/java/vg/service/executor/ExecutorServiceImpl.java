package vg.service.executor;

import vg.service.main.MainService;

import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExecutorServiceImpl extends ThreadPoolExecutor implements ExecutorService {
    // Constants
    private static final String EDT_PREFIX = "EDT";
    private static final String GENERAL_THREAD_PREFIX = "GENERAL_THREAD";
    private static final String SWING_WORKER_THREAD_PREFIX = "SWING_WORKER_THREAD";

    // Main data
    private int threadIdCounter = 0;
    private int threadCount = 0;

    // Mutex
    private static final Object generalMutex = new Object();

    public ExecutorServiceImpl() {
        super(5, 5, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
    }

    @Override
    public void execute(Runnable runnable) {
        if (runnable == null)
            return;

        final int threadId = nextThreadId();
        super.execute(() -> {
            incThreadCount(threadId, GENERAL_THREAD_PREFIX);
            try {
                runnable.run();
            } catch (Throwable ex) {
                MainService.logger.printException(ex);
            }
            decThreadCount(threadId, GENERAL_THREAD_PREFIX);
        });
    }

    private final Map<String, Runnable> taskKeyToRunnable = new HashMap<>();
    public void executeLastTaskByKey(String taskKey, Runnable newRunnable) {
        synchronized (taskKeyToRunnable) {
            var runnable = taskKeyToRunnable.get(taskKey);
            if (runnable == null) {
                taskKeyToRunnable.put(taskKey, newRunnable);
                CompletableFuture.runAsync(newRunnable).thenRunAsync(() -> {
                    synchronized (taskKeyToRunnable) {
                        var lastRunnable = taskKeyToRunnable.remove(taskKey);
                        if (lastRunnable == null || lastRunnable == newRunnable) {
                            // do nothing...
                            return;
                        }
                        executeLastTaskByKey(taskKey, lastRunnable);
                    }
                });
            } else {
                taskKeyToRunnable.put(taskKey, newRunnable);
            }
        }
    }

    private final Map<String, Queue<Runnable>> taskKeyToQueueRunnable = new HashMap<>();
    public void executeTaskByKey(String taskKey, Runnable newRunnable) {
        synchronized (taskKeyToQueueRunnable) {
            taskKeyToQueueRunnable.putIfAbsent(taskKey, new ArrayDeque<>());
            var queue = taskKeyToQueueRunnable.get(taskKey);

            if (newRunnable != null) {
                queue.add(newRunnable);

                if (queue.size() == 1) {
                    executeTaskByKey(taskKey, null);
                }

                return;
            }

            var runnable = queue.peek();
            if (runnable == null) {
                // do nothing...
                return;
            }

            MainService.logger.printInfo(String.format("Launch task '%s'.", taskKey));
            CompletableFuture.runAsync(runnable).handle((result, ex) -> {
                if (ex != null) {
                    MainService.logger.printError(String.format("executeTaskByKey method (key = %s) was failed with message: '%s'.", taskKey, ex.getMessage()), ex);
                }
                return result;
            }).thenRunAsync(() -> {
                synchronized (taskKeyToQueueRunnable) {
                    MainService.logger.printInfo(String.format("Task '%s' was finished via thread with name '%s'.", taskKey, Thread.currentThread().getName()));

                    // remove the finished task from the queue.
                    queue.poll();
                    if (queue.peek() == null) {
                        // do nothing...
                        return;
                    }

                    executeTaskByKey(taskKey, null);
                }
            });
        }
    }

    public void executeInEDT(Runnable runnable) {
        SwingUtilities.invokeLater(() -> {
            try {
                runnable.run();
            } catch (Throwable ex) {
                MainService.logger.printException(ex);
            }
        });
    }

    @Override
    public void execute(SwingExecutor swingExecutor) {
        if (swingExecutor == null)
            return;

        final int threadId = nextThreadId();
        new SwingWorker() {
            @Override
            protected Object doInBackground() {
                incThreadCount(threadId, SWING_WORKER_THREAD_PREFIX);
                try {
                    swingExecutor.doInBackground();
                } catch (Throwable ex) {
                    MainService.logger.printException(ex);
                }
                decThreadCount(threadId, SWING_WORKER_THREAD_PREFIX);
                return null;
            }

            @Override
            protected void done() {
                incThreadCount(threadId, EDT_PREFIX);
                try {
                    swingExecutor.doInEDT();
                } catch (Throwable ex) {
                    MainService.logger.printException(ex);
                }
                decThreadCount(threadId, EDT_PREFIX);
            }
        }.execute();
    }

    private int nextThreadId() {
        synchronized (generalMutex) {
            return threadIdCounter++;
        }
    }

    private void incThreadCount(int threadId, String prefix) {
        synchronized (generalMutex) {
            threadCount++;
            //MainService.logger.printDebug("Thread: " + prefix + ", with id: " + threadId + " was started, count of threads: " + threadCount);
        }
    }

    private void decThreadCount(int threadId, String prefix) {
        synchronized (generalMutex) {
            threadCount--;
            //MainService.logger.printDebug("Thread: " + prefix + ", with id: " + threadId + " was finished, count of threads: " + threadCount);
        }
    }
}