package vg.service.plugin;

/**
 * All plugins in decoders must implements this interface.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface Plugin {
	/**
	 * This method installs plugin to decoders.
     * It will be called from EDT.
	 */
	void install() throws Exception;
}
