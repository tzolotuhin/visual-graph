package vg.service.db;

import com.google.common.collect.Lists;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.model.record.AttributeOwnerType;
import vg.lib.model.record.AttributeRecord;
import vg.lib.model.record.AttributeRecordType;
import vg.lib.model.record.EdgeRecord;
import vg.lib.model.record.GraphModelRecord;
import vg.lib.model.record.VertexRecord;
import vg.lib.storage.GraphStorage;
import vg.service.main.MainService;
import vg.shared.utils.DateUtils;
import vg.shared.utils.RandomUtils;

@Slf4j
public class SQLiteDataBase implements GraphStorage {
    // Vertex table
    private static final String VERTEX_TABLE = "vertex";
    private static final String VERTEX_TABLE_ID = "id";
    private static final String VERTEX_TABLE_GRAPH_ID = "graph_id";
    private static final String VERTEX_TABLE_OWNER_ID = "owner_id";
    private static final String VERTEX_TABLE_CONTAINS_ELEMENTS = "contains_elements";

    // Edge table
    private static final String EDGE_TABLE = "edge";
    private static final String EDGE_TABLE_ID = "id";
    private static final String EDGE_TABLE_GRAPH_ID = "graph_id";
    private static final String EDGE_TABLE_SRC_VERTEX = "src_id";
    private static final String EDGE_TABLE_TRG_VERTEX = "trg_id";

    // Attribute table
    private static final String ATTRIBUTE_TABLE = "attribute";
    private static final String ATTRIBUTE_TABLE_ID = "id";
    private static final String ATTRIBUTE_TABLE_OWNER_ID = "owner_id";
    private static final String ATTRIBUTE_TABLE_OWNER_TYPE = "owner_type";
    private static final String ATTRIBUTE_TABLE_NAME = "name";
    private static final String ATTRIBUTE_TABLE_VALUE = "value";
    private static final String ATTRIBUTE_TABLE_VISIBLE = "visible";
    private static final String ATTRIBUTE_TABLE_VALUE_TYPE = "value_type";

    // Graph model table
    private static final String GRAPH_MODEL_TABLE = "graph_model";
    private static final String GRAPH_TABLE_ID = "id";
    private static final String GRAPH_TABLE_NAME = "name";

    // Constants: creates
    private static final String CREATE_VERTEX_TABLE = "create table " + VERTEX_TABLE + " (" +
            VERTEX_TABLE_ID + " INT PRIMARY KEY, " +
            VERTEX_TABLE_GRAPH_ID + " INT, " +
            VERTEX_TABLE_OWNER_ID + " INT, " +
            VERTEX_TABLE_CONTAINS_ELEMENTS + " BOOL);";

    private static final String CREATE_EDGE_TABLE = "create table edge (id INT PRIMARY KEY, graph_id INT, src_id INT, trg_id INT);";

    private static final String CREATE_ATTRIBUTE_TABLE = "create table " + ATTRIBUTE_TABLE + " (" +
            ATTRIBUTE_TABLE_ID + " INTEGER PRIMARY KEY, " +
            ATTRIBUTE_TABLE_OWNER_ID + " INT, " +
            ATTRIBUTE_TABLE_OWNER_TYPE + " INT, " +
            ATTRIBUTE_TABLE_NAME + " VARCHAR, " +
            ATTRIBUTE_TABLE_VALUE + " VARCHAR, " +
            ATTRIBUTE_TABLE_VALUE_TYPE + " INT, " +
            ATTRIBUTE_TABLE_VISIBLE + " BOOL);";

    private static final String CREATE_GRAPH_TABLE = "create table " + GRAPH_MODEL_TABLE + " (" +
            GRAPH_TABLE_ID + " INT PRIMARY KEY, " +
            GRAPH_TABLE_NAME + " VARCHAR);";

    private static final String CREATE_VERTEX_INDEX_TABLE = "create index Idx_vertex on " + VERTEX_TABLE + "(" +
            VERTEX_TABLE_OWNER_ID + ");";

    private static final String CREATE_EDGE_INDEX_TABLE = "create index Idx_edge on " + EDGE_TABLE + "(" +
            EDGE_TABLE_SRC_VERTEX + ", " +
            EDGE_TABLE_TRG_VERTEX + ");";

    private static final String CREATE_ATTRIBUTE_INDEX_TABLE = "create index Idx_attribute on " + ATTRIBUTE_TABLE + "(" +
            ATTRIBUTE_TABLE_OWNER_ID + ", " +
            ATTRIBUTE_TABLE_OWNER_TYPE + ", " +
            ATTRIBUTE_TABLE_NAME + ");";

    // Constants: inserts
    private static final String INSERT_ATTRIBUTE_SQL = "insert into attribute values(?, ?, ?, ?, ?, ?, ?)";

    private static final String INSERT_VERTEX_SQL = "insert into vertex values(?, ?, ?, ?)";

    private static final String INSERT_EDGE = "insert into " + EDGE_TABLE + " values(%d, %d, %d, %d);";

    private static final String INSERT_GRAPH_MODEL = "insert into " + GRAPH_MODEL_TABLE + " values(%d, '%s');";

    // Constants: updates
    private static final String UPDATE_GRAPH_HEADER_BY_ID = "UPDATE " + GRAPH_MODEL_TABLE +
            " SET " + GRAPH_TABLE_NAME + "='%s' " +
            " WHERE " + GRAPH_TABLE_ID + "=%d;";

    private static final String UPDATE_VERTEX_HEADER_BY_ID = "UPDATE vertex" +
            " SET " + VERTEX_TABLE_GRAPH_ID + "=%d, " +
            VERTEX_TABLE_OWNER_ID + "=%d " +
            " WHERE " + VERTEX_TABLE_ID + "=%d;";

    private static final String UPDATE_CONTAINS_ELEMENTS_FIELD_FOR_VERTEX_RECORD_BY_ID
        = "update vertex set contains_elements=? where id=?";

    private static final String UPDATE_EDGE_HEADER_BY_ID = "UPDATE " + EDGE_TABLE +
            " SET " + EDGE_TABLE_SRC_VERTEX + "=%d, " +
            EDGE_TABLE_TRG_VERTEX + "=%d " +
            " WHERE " + EDGE_TABLE_ID + "=%d;";

    private static final String DELETE_EDGE_RECORD_BY_ID = "DELETE FROM " + EDGE_TABLE +
            " WHERE " + EDGE_TABLE_ID + "=%d;";

    private static final String UPDATE_ATTRIBUTE_HEADER_BY_ID = "UPDATE " + ATTRIBUTE_TABLE +
            " SET " + ATTRIBUTE_TABLE_VALUE + "='%s'," +
            ATTRIBUTE_TABLE_VALUE_TYPE + "=%d " +
            " WHERE " + ATTRIBUTE_TABLE_ID + "=%d;";

    // Constants: selects
    private static final String SELECT_VERTEX_BY_OWNER_ID = "SELECT * " +
            "FROM " + VERTEX_TABLE + " s1 " +
            "WHERE s1." + VERTEX_TABLE_OWNER_ID + "=%d;";

    private static final String SELECT_EDGE_BY_GRAPH_MODEL_ID = "SELECT * " +
            "FROM " + EDGE_TABLE + " s1 " +
            "WHERE s1." + EDGE_TABLE_GRAPH_ID + "=%d;";

    private static final String SELECT_EDGE_BY_VERTEX_ID = "SELECT * " +
            "FROM " + EDGE_TABLE + " s1 " +
            "WHERE s1." + EDGE_TABLE_SRC_VERTEX + "=%d or s1." + EDGE_TABLE_TRG_VERTEX + "=%d;";

    private static final String SELECT_ROOTS_BY_GRAPH_ID = "SELECT * " +
            "FROM " + VERTEX_TABLE + " s1 " +
            "WHERE s1." + VERTEX_TABLE_GRAPH_ID + "=%d and " + VERTEX_TABLE_OWNER_ID + "=" + VertexRecord.NO_PARENT_ID + ";";

    private static final String SELECT_GRAPH_MODEL_BY_ID = "SELECT * " +
            "FROM " + GRAPH_MODEL_TABLE + " s1 " +
            "WHERE s1." + GRAPH_TABLE_ID + "=%d;";

    private static final String SELECT_GRAPH_MODELS = " SELECT * " +
            " FROM " + GRAPH_MODEL_TABLE + " s1;";

    private static final String SELECT_VERTEX_BY_ID = "SELECT * " +
            "FROM " + VERTEX_TABLE + " s1 " +
            "WHERE s1." + VERTEX_TABLE_ID + "=%d;";

    private static final String SELECT_EDGE_BY_ID = "SELECT * " +
            "FROM " + EDGE_TABLE + " s1 " +
            "WHERE s1." + EDGE_TABLE_ID + "=%d;";

    private static final String SELECT_ATTRIBUTE_BY_ID = "SELECT * " +
            " FROM " + ATTRIBUTE_TABLE + " s1 " +
            " WHERE s1." + ATTRIBUTE_TABLE_ID + "=%d;";

    private static final String SELECT_ATTRIBUTES_BY_OWNER = " SELECT * " +
            " FROM " + ATTRIBUTE_TABLE + " s1 " +
            " WHERE s1." + ATTRIBUTE_TABLE_OWNER_ID + "=%d and s1." + ATTRIBUTE_TABLE_OWNER_TYPE + "=%d;";

    private static final String SELECT_ATTRIBUTES_BY_OWNER_AND_NAME = " SELECT * "
        + " FROM " + ATTRIBUTE_TABLE + " a"
        + " WHERE a." + ATTRIBUTE_TABLE_OWNER_ID + " = %d "
        + " and a." + ATTRIBUTE_TABLE_OWNER_TYPE + " = %d"
        + " and a." + ATTRIBUTE_TABLE_NAME + " = '%s';";

    // TODO: problem with values with quotes.
    private static final String SELECT_ATTRIBUTES_BY_NAME_AND_VALUE = " SELECT * " +
            " FROM " + ATTRIBUTE_TABLE + " a" +
            " WHERE a." + ATTRIBUTE_TABLE_NAME + "='%s' and a."+ ATTRIBUTE_TABLE_VALUE + "='%s';";

    // Main data
    private final Connection connection;
    private int graphModelIdCounter;
    private int vertexIdCounter;
    private int edgeIdCounter;
    private int attributeIdCounter;

    public SQLiteDataBase() {
        // initialize counters for ids.
        graphModelIdCounter = 1;
        vertexIdCounter = 1;
        edgeIdCounter = 1;
        attributeIdCounter = 1;

        // initialize connection to the database.
        try {
            // load the sqlite-JDBC driver using the current class loader
            Class.forName("org.sqlite.JDBC");

            String dataBaseDirectoryName = MainService.getWorkingDirectory() + "data" + File.separator + "db" + File.separator;
            File dataBaseDirectory = new File(dataBaseDirectoryName);
            if (!dataBaseDirectory.exists() && !dataBaseDirectory.mkdirs()) {
                throw new RuntimeException("Couldn't create directory for database");
            }

            // create connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + dataBaseDirectoryName + "database_" + DateUtils.getStartAppTimeInDefaultFormat() + "_" + RandomUtils.generateRandomUUID() + ".db");
            connection.setAutoCommit(false);
        } catch (ClassNotFoundException | SQLException ex) {
            throw new RuntimeException(ex);
        }

        // create tables
        try (var statement = connection.createStatement()) {
            statement.execute(CREATE_VERTEX_TABLE);
            statement.execute(CREATE_EDGE_TABLE);
            statement.execute(CREATE_ATTRIBUTE_TABLE);
            statement.execute(CREATE_GRAPH_TABLE);
            statement.execute(CREATE_VERTEX_INDEX_TABLE);
            statement.execute(CREATE_EDGE_INDEX_TABLE);
            statement.execute(CREATE_ATTRIBUTE_INDEX_TABLE);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    public void close() {
        try {
            connection.commit();
            connection.close();
        } catch (SQLException ex) {
            log.warn("Problem with database disconnection.", ex);
            //throw new RuntimeException(ex);
        }
    }

    @Override
    public int createGraphModel(String name) {
        try (var statement = connection.createStatement()){
            int id = graphModelIdCounter++;
            statement.execute(String.format(INSERT_GRAPH_MODEL, id, name));
            return id;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public int createVertex(int graphModelId, int parentId) {
        int id = vertexIdCounter++;
        try (var preparedStatement = connection.prepareStatement(INSERT_VERTEX_SQL)) {
            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, graphModelId);
            preparedStatement.setInt(3, parentId);
            preparedStatement.setBoolean(4, false);

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        try (var preparedStatement = connection.prepareStatement(UPDATE_CONTAINS_ELEMENTS_FIELD_FOR_VERTEX_RECORD_BY_ID)) {
            preparedStatement.setBoolean(1, true);
            preparedStatement.setInt(2, parentId);

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return id;
    }

    @Override
    public int createEdge(int graphModelId, int sourceVertexId, int targetVertexId) {
        try (var statement = connection.createStatement()) {
            int id = edgeIdCounter++;
            statement.execute(String.format(INSERT_EDGE, id, graphModelId, sourceVertexId, targetVertexId));
            return id;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public int createAttribute(int ownerId,
                               AttributeOwnerType ownerType,
                               String name,
                               String strValue,
                               AttributeRecordType valueType,
                               boolean visible) {
        try (var preparedStatement = connection.prepareStatement(INSERT_ATTRIBUTE_SQL)) {
            int id = attributeIdCounter++;

            preparedStatement.setInt(1, id);
            preparedStatement.setInt(2, ownerId);
            preparedStatement.setInt(3, ownerType.getId());
            preparedStatement.setString(4, name);
            preparedStatement.setString(5, strValue);
            preparedStatement.setInt(6, valueType.getId());
            preparedStatement.setBoolean(7, visible);

            preparedStatement.executeUpdate();

            return id;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void removeEdgeRecord(int edgeId) {
        try (var statement = connection.createStatement()) {
            statement.executeUpdate(String.format(DELETE_EDGE_RECORD_BY_ID, edgeId));
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void editAttributeRecord(AttributeRecord newAttributeRecord) {
        Validate.notNull(newAttributeRecord);
        Validate.notNull(newAttributeRecord.getType());

        try (var statement = connection.createStatement()) {
            statement.executeUpdate(String.format(UPDATE_ATTRIBUTE_HEADER_BY_ID,
                    newAttributeRecord.getStringValue(),
                    newAttributeRecord.getType().getId(),
                    newAttributeRecord.getId()));
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<GraphModelRecord> getGraphRecords() {
        List<GraphModelRecord> result = Lists.newArrayList();

        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(SELECT_GRAPH_MODELS);
            while (resultSet.next()) {
                result.add(new GraphModelRecord(resultSet.getInt(GRAPH_TABLE_ID), resultSet.getString(GRAPH_TABLE_NAME)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    @Override
    public Optional<GraphModelRecord> findGraphModelRecord(int graphModelId) {
        var query = String.format(SELECT_GRAPH_MODEL_BY_ID, graphModelId);

        try (var statement = connection.createStatement()) {
            var resultSet = statement.executeQuery(query);
            if (!resultSet.next()) {
                return Optional.empty();
            }

            var result = new GraphModelRecord(
                resultSet.getInt(GRAPH_TABLE_ID),
                resultSet.getString(GRAPH_TABLE_NAME));

            return Optional.of(result);
        } catch (SQLException ex) {
            handleSQLException(query, ex);
        }

        return Optional.empty();
    }

    @Override
    public VertexRecord getVertexRecord(int vertexId) {
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_VERTEX_BY_ID, vertexId));
            if (resultSet.next()) {
                return new VertexRecord(
                        resultSet.getInt(VERTEX_TABLE_ID),
                        resultSet.getInt(VERTEX_TABLE_GRAPH_ID),
                        resultSet.getInt(VERTEX_TABLE_OWNER_ID),
                        resultSet.getBoolean(VERTEX_TABLE_CONTAINS_ELEMENTS));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return null;
    }

    @Override
    public List<VertexRecord> getRootRecords(int graphId) {
        return doGetVertexRecords(String.format(SELECT_ROOTS_BY_GRAPH_ID, graphId));
    }

    @Override
    public List<VertexRecord> getVertexRecordsByOwnerId(int ownerId) {
        return doGetVertexRecords(String.format(SELECT_VERTEX_BY_OWNER_ID, ownerId));
    }

    private List<VertexRecord> doGetVertexRecords(String query) {
        List<VertexRecord> result = Lists.newArrayList();
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                result.add(new VertexRecord(
                        resultSet.getInt(VERTEX_TABLE_ID),
                        resultSet.getInt(VERTEX_TABLE_GRAPH_ID),
                        resultSet.getInt(VERTEX_TABLE_OWNER_ID),
                        resultSet.getBoolean(VERTEX_TABLE_CONTAINS_ELEMENTS)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    @Override
    public List<EdgeRecord> getEdgeRecordsByGraphModelId(int graphModelId) {
        List<EdgeRecord> result = Lists.newArrayList();
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_EDGE_BY_GRAPH_MODEL_ID, graphModelId));
            while (resultSet.next()) {
                result.add(new EdgeRecord(resultSet.getInt(EDGE_TABLE_ID),
                        resultSet.getInt(EDGE_TABLE_GRAPH_ID),
                        resultSet.getInt(EDGE_TABLE_SRC_VERTEX),
                        resultSet.getInt(EDGE_TABLE_TRG_VERTEX)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    @Override
    public List<EdgeRecord> getEdgeRecordsByVertexId(int vertexId) {
        List<EdgeRecord> result = Lists.newArrayList();
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_EDGE_BY_VERTEX_ID, vertexId, vertexId));
            while (resultSet.next()) {
                result.add(new EdgeRecord(resultSet.getInt(EDGE_TABLE_ID),
                        resultSet.getInt(EDGE_TABLE_GRAPH_ID),
                        resultSet.getInt(EDGE_TABLE_SRC_VERTEX),
                        resultSet.getInt(EDGE_TABLE_TRG_VERTEX)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    @Override
    public EdgeRecord getEdgeRecord(int edgeId) {
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_EDGE_BY_ID, edgeId));
            if (resultSet.next()) {
                return new EdgeRecord(resultSet.getInt(EDGE_TABLE_ID),
                        resultSet.getInt(EDGE_TABLE_GRAPH_ID),
                        resultSet.getInt(EDGE_TABLE_SRC_VERTEX),
                        resultSet.getInt(EDGE_TABLE_TRG_VERTEX));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return null;
    }

    @Override
    public AttributeRecord getAttributeRecord(int attributeId) {
        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_ATTRIBUTE_BY_ID, attributeId));
            if (resultSet.next()) {
                return new AttributeRecord(
                    resultSet.getInt(ATTRIBUTE_TABLE_ID),
                    resultSet.getInt(ATTRIBUTE_TABLE_OWNER_ID),
                    AttributeOwnerType.valueOf(resultSet.getInt(ATTRIBUTE_TABLE_OWNER_TYPE)),
                    resultSet.getString(ATTRIBUTE_TABLE_NAME),
                    resultSet.getString(ATTRIBUTE_TABLE_VALUE),
                    AttributeRecordType.valueOf(resultSet.getInt(ATTRIBUTE_TABLE_VALUE_TYPE)),
                    resultSet.getBoolean(ATTRIBUTE_TABLE_VISIBLE));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return null;
    }

    @Override
    public Optional<AttributeRecord> findAttributeRecord(int ownerId,
                                                         AttributeOwnerType ownerType,
                                                         String name) {
        var query = String.format(SELECT_ATTRIBUTES_BY_OWNER_AND_NAME, ownerId, ownerType.getId(), name);

        try (var statement = connection.createStatement()) {
            var resultSet = statement.executeQuery(query);

            if (!resultSet.next()) {
                return Optional.empty();
            }

            var record = new AttributeRecord(
                resultSet.getInt(ATTRIBUTE_TABLE_ID),
                resultSet.getInt(ATTRIBUTE_TABLE_OWNER_ID),
                AttributeOwnerType.valueOf(resultSet.getInt(ATTRIBUTE_TABLE_OWNER_TYPE)),
                resultSet.getString(ATTRIBUTE_TABLE_NAME),
                resultSet.getString(ATTRIBUTE_TABLE_VALUE),
                AttributeRecordType.valueOf(resultSet.getInt(ATTRIBUTE_TABLE_VALUE_TYPE)),
                resultSet.getBoolean(ATTRIBUTE_TABLE_VISIBLE));

            return Optional.of(record);
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    @Override
    public List<AttributeRecord> getAttributeRecordsByValue(String name, String value) {
        List<AttributeRecord> result = Lists.newArrayList();

        try (var statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(String.format(SELECT_ATTRIBUTES_BY_NAME_AND_VALUE, name, value));
            while (resultSet.next()) {
                result.add(new AttributeRecord(resultSet.getInt(ATTRIBUTE_TABLE_ID),
                    resultSet.getInt(ATTRIBUTE_TABLE_OWNER_ID),
                    AttributeOwnerType.valueOf(resultSet.getInt(ATTRIBUTE_TABLE_OWNER_TYPE)),
                    resultSet.getString(ATTRIBUTE_TABLE_NAME),
                    resultSet.getString(ATTRIBUTE_TABLE_VALUE),
                    AttributeRecordType.valueOf(resultSet.getInt(ATTRIBUTE_TABLE_VALUE_TYPE)),
                    resultSet.getBoolean(ATTRIBUTE_TABLE_VISIBLE)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    @Override
    public List<AttributeRecord> getAttributeRecordsByOwner(int ownerId, AttributeOwnerType ownerType) {
        List<AttributeRecord> result = Lists.newArrayList();

        try (var statement = connection.createStatement()) {
            var resultSet = statement.executeQuery(
                String.format(SELECT_ATTRIBUTES_BY_OWNER, ownerId, ownerType.getId()));

            while (resultSet.next()) {
                result.add(new AttributeRecord(resultSet.getInt(ATTRIBUTE_TABLE_ID),
                    resultSet.getInt(ATTRIBUTE_TABLE_OWNER_ID),
                    AttributeOwnerType.valueOf(resultSet.getInt(ATTRIBUTE_TABLE_OWNER_TYPE)),
                    resultSet.getString(ATTRIBUTE_TABLE_NAME),
                    resultSet.getString(ATTRIBUTE_TABLE_VALUE),
                    AttributeRecordType.valueOf(resultSet.getInt(ATTRIBUTE_TABLE_VALUE_TYPE)),
                    resultSet.getBoolean(ATTRIBUTE_TABLE_VISIBLE)));
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }

        return result;
    }

    private void handleSQLException(String query, SQLException ex) {
        log.debug("Problem with execution of following query '{}', message: '{}'.", query, ex.getMessage());
        throw new RuntimeException(ex);
    }
}
