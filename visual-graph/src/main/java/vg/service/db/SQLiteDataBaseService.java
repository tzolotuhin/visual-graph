package vg.service.db;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.SwingUtilities;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;
import vg.lib.model.graph.Attribute;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.lib.model.record.AttributeOwnerType;
import vg.lib.model.record.AttributeRecord;
import vg.lib.model.record.AttributeRecordType;
import vg.lib.model.record.EdgeRecord;
import vg.lib.model.record.GraphModelRecord;
import vg.lib.model.record.VertexRecord;
import vg.service.main.MainService;

/**
 * This class realizes model, which uses SQL database.
 * This model may cache graph elements in operating memory.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
@Slf4j
public class SQLiteDataBaseService implements IDataBaseService {
    // Main data
    private final SQLiteDataBase sqliteDataBase;
    private final CopyOnWriteArrayList<DataBaseListener> dataBaseListeners;

    // Mutex
    private final Object generalMutex = new Object();

    public SQLiteDataBaseService() {
        sqliteDataBase = new SQLiteDataBase();
        dataBaseListeners = new CopyOnWriteArrayList<>();
    }

    @Override
    public int createGraphModel(String name) {
        int id = doCreateGraphModel(name);

        doNotifyOnNewGraphModel(id);

        return id;
    }

    private int doCreateGraphModel(String name) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createGraphModel(name);
        }
    }

    @Override
    public int createVertex(int graphModelId, int parentId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createVertex(graphModelId, parentId);
        }
    }

    @Override
    public int createEdge(int graphModelId, int sourceVertexId, int targetVertexId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createEdge(graphModelId, sourceVertexId, targetVertexId);
        }
    }

    @Override
    public int createGraphModelAttribute(int graphModelId,
                                         String name,
                                         String strValue,
                                         AttributeRecordType valueType,
                                         boolean visible) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createGraphModelAttribute(graphModelId, name, strValue, valueType, visible);
        }
    }

    @Override
    public int createVertexAttribute(int vertexId, String name, String strValue, AttributeRecordType valueType, boolean visible) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createVertexAttribute(vertexId, name, strValue, valueType, visible);
        }
    }

    @Override
    public int createEdgeAttribute(int edgeId, String name, String strValue, AttributeRecordType valueType, boolean visible) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createEdgeAttribute(edgeId, name, strValue, valueType, visible);
        }
    }

    @Override
    public int createExtendedVertex(int graphId, int vertexId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createExtendedVertex(graphId, vertexId);
        }
    }

    @Override
    public int createAttribute(int ownerId,
                               AttributeOwnerType ownerType,
                               String name,
                               String strValue,
                               AttributeRecordType valueType,
                               boolean visible) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createAttribute(ownerId, ownerType, name, strValue, valueType, visible);
        }
    }

    @Override
    public int createPort(int graphId, int vertexId, boolean isFake, int index) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createPort(graphId, vertexId, isFake, index);
        }
    }

    @Override
    public List<Integer> createCompositeEdge(int graphId, int sourceVertexId, int targetVertexId, int sourcePortId, int targetPortId, boolean directed) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.createCompositeEdge(
                graphId, sourceVertexId, targetVertexId, sourcePortId, targetPortId, directed);
        }
    }

    @Override
    public void editAttributeRecord(AttributeRecord attributeRecord) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            sqliteDataBase.editAttributeRecord(attributeRecord);
        }
    }

    @Override
    public void removeEdgeRecord(int edgeId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            sqliteDataBase.removeEdgeRecord(edgeId);
        }
    }

    @Override
    public List<GraphModelRecord> getGraphRecords() {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getGraphRecords();
        }
    }

    @Override
    public VertexRecord getVertexRecord(int vertexId) {
        //Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getVertexRecord(vertexId);
        }
    }

    @Override
    public List<VertexRecord> getVertexRecordsByOwnerId(int ownerId) {
        //Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getVertexRecordsByOwnerId(ownerId);
        }
    }

    @Override
    public EdgeRecord getEdgeRecord(int edgeId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getEdgeRecord(edgeId);
        }
    }

    @Override
    public List<EdgeRecord> getEdgeRecordsByVertexId(int vertexId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getEdgeRecordsByVertexId(vertexId);
        }
    }

    @Override
    public List<EdgeRecord> getEdgeRecordsByGraphModelId(int graphModelId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getEdgeRecordsByGraphModelId(graphModelId);
        }
    }

    @Override
    public List<VertexRecord> getRootRecords(int graphId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getRootRecords(graphId);
        }
    }

    @Override
    public Optional<GraphModelRecord> findGraphModelRecord(int graphModelId) {
        //Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        return sqliteDataBase.findGraphModelRecord(graphModelId);
    }

    @Override
    public AttributeRecord getAttributeRecord(int attributeId) {
        //Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getAttributeRecord(attributeId);
        }
    }

    @Override
    public Optional<AttributeRecord> findAttributeRecord(int ownerId,
                                                         AttributeOwnerType ownerType,
                                                         String name) {
        //Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        return sqliteDataBase.findAttributeRecord(ownerId, ownerType, name);
    }

    @Override
    public List<AttributeRecord> getAttributeRecordsByOwner(int ownerId, AttributeOwnerType ownerType) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getAttributeRecordsByOwner(ownerId, ownerType);
        }
    }

    @Override
    public List<AttributeRecord> getAttributeRecordsByValue(String name, String value) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getAttributeRecordsByValue(name, value);
        }
    }

    @Override
    public Graph getGraph(int graphModelId, int parentId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getGraph(graphModelId, parentId);
        }
    }

    @Override
    public Vertex getVertex(int vertexId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getVertex(vertexId);
        }
    }

    @Override
    public Edge getEdge(int edgeId) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getEdge(edgeId);
        }
    }

    @Override
    public List<Attribute> getAttributesByOwner(int ownerId, AttributeOwnerType ownerType) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            return sqliteDataBase.getAttributesByOwner(ownerId, ownerType);
        }
    }

    @Override
    public void close() {
        //Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            sqliteDataBase.close();
        }
    }

    @Override
    public void addListener(DataBaseListener dataBaseListener) {
        Validate.isTrue(!SwingUtilities.isEventDispatchThread(), "The method should not be called from EDT.");

        synchronized (generalMutex) {
            dataBaseListeners.add(dataBaseListener);
        }
    }

    private void doNotifyOnNewGraphModel(int graphModelId) {
        var graphModelRecordOptional = findGraphModelRecord(graphModelId);

        if (graphModelRecordOptional.isEmpty()) {
            // do nothing...
            log.warn("Graph model with id {} was not found.", graphModelId);
            return;
        }

        MainService.executorService.executeInEDT(() -> {
            for (DataBaseListener listener : dataBaseListeners) {
                listener.onOpenNewGraph(graphModelRecordOptional.get().clone());
            }
        });
    }
}
