package vg.service.db;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import org.apache.commons.lang3.Validate;
import vg.lib.common.VGUtils;
import vg.lib.model.graph.Attribute;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.lib.model.record.AttributeOwnerType;
import vg.lib.model.record.AttributeRecord;
import vg.lib.model.record.AttributeRecordType;
import vg.lib.model.record.EdgeRecord;
import vg.lib.model.record.GraphModelRecord;
import vg.lib.model.record.VertexRecord;
import vg.lib.storage.GraphStorage;

/**
 * Interface for graph database.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public interface IDataBaseService extends GraphStorage {
    //region create methods.

    /**
     * Creates new graph model record and returns the unique id.
     *
     * @param name - name of the graph model.
     */
    int createGraphModel(String name);

    /**
     * Creates record for the vertex and returns the unique id.
     */
    int createVertex(int graphModelId, int parentId);

    /**
     * Creates record for the edge and returns the unique id.
     *
     * @param graphModelId   - id of graph model.
     * @param sourceVertexId - id of source vertex.
     * @param targetVertexId - id of target vertex.
     */
    int createEdge(int graphModelId, int sourceVertexId, int targetVertexId);

    int createGraphModelAttribute(int graphModelId,
                                  String name,
                                  String strValue,
                                  AttributeRecordType valueType,
                                  boolean visible);

    int createVertexAttribute(int vertexId,
                              String name,
                              String strValue,
                              AttributeRecordType valueType,
                              boolean visible);

    int createEdgeAttribute(int edgeId,
                            String name,
                            String strValue,
                            AttributeRecordType valueType,
                            boolean visible);

    int createExtendedVertex(int graphId, int vertexId);

    int createPort(int graphId, int vertexId, boolean isFake, int index);

    /**
     * Returns parts of composite edge - List[0-master, 1, 2, ..., n].
     */
    List<Integer> createCompositeEdge(int graphId,
                                      int sourceVertexId,
                                      int targetVertexId,
                                      int sourcePortId,
                                      int targetPortId,
                                      boolean directed);

    //endregion

    //region find records methods.

    // TODO: should be deleted with GraphRecord.
    /**
     * Returns list of graph records.
     */
    List<GraphModelRecord> getGraphRecords();

    @Nullable
    VertexRecord getVertexRecord(int vertexId);

    List<VertexRecord> getVertexRecordsByOwnerId(int ownerId);

    EdgeRecord getEdgeRecord(int edgeId);

    List<EdgeRecord> getEdgeRecordsByVertexId(int vertexId);

    List<EdgeRecord> getEdgeRecordsByGraphModelId(int graphModelId);

    List<VertexRecord> getRootRecords(int graphId);

    Optional<GraphModelRecord> findGraphModelRecord(int graphModelId);

    @Nullable
    AttributeRecord getAttributeRecord(int attributeId);

    Optional<AttributeRecord> findAttributeRecord(int ownerId,
                                                  AttributeOwnerType ownerType,
                                                  String name);

    List<AttributeRecord> getAttributeRecordsByOwner(int ownerId, AttributeOwnerType ownerType);

    List<AttributeRecord> getAttributeRecordsByValue(String name, String value);

    Graph getGraph(int graphModelId, int parentId);

    Vertex getVertex(int vertexId);

    Edge getEdge(int edgeId);

    List<Attribute> getAttributesByOwner(int ownerId, AttributeOwnerType ownerType);

    //endregion

    //region edit methods

    void editAttributeRecord(AttributeRecord attributeRecord);

    //endregion

    //region remove methods

    void removeEdgeRecord(int edgeId);

    //endregion

    void close();

    void addListener(DataBaseListener dataBaseListener);

    class DataBaseListener {
        public void onOpenNewGraph(GraphModelRecord graphModelRecord) { }
    }
}
