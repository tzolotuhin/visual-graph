package vg.samples;

import vg.shared.utils.XmlUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class GenerateBigGraph {
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder();

        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<graphml xmlns=\"http://graphml.graphdrawing.org/xmlns\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://graphml.graphdrawing.org/xmlns      http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd\">\n" +
                "<graph edgedefault=\"directed\" id=\"big graph\">");

        for (int i = 0; i < 100; i++) {
            UUID v1 = UUID.randomUUID();
            UUID v21 = UUID.randomUUID();
            UUID v22 = UUID.randomUUID();
            UUID v31 = UUID.randomUUID();
            UUID v32 = UUID.randomUUID();
            UUID v33 = UUID.randomUUID();
            UUID p331 = UUID.randomUUID();
            UUID p332 = UUID.randomUUID();
            UUID v41 = UUID.randomUUID();
            UUID v42 = UUID.randomUUID();
            UUID v5 = UUID.randomUUID();

            sb.append(String.format("<node id=\"%s\"/>", v1));
            sb.append(String.format("<node id=\"%s\"/>", v21));
            sb.append(String.format("<node id=\"%s\"/>", v22));
            sb.append(String.format("<node id=\"%s\"/>", v31));
            sb.append(String.format("<node id=\"%s\"/>", v32));

            sb.append(String.format("<node id=\"%s\">", v33));
            sb.append(String.format("<port name=\"%s\"/>", p331));
            sb.append(String.format("<port name=\"%s\"/>", p332));
            sb.append("</node>");

            sb.append(String.format("<node id=\"%s\"/>", v41));
            sb.append(String.format("<node id=\"%s\"/>", v42));
            sb.append(String.format("<node id=\"%s\"/>", v5));

            sb.append(String.format("<edge id= \"%s\" source=\"%s\" target=\"%s\"/>", UUID.randomUUID(), v1, v21));
            sb.append(String.format("<edge id= \"%s\" source=\"%s\" target=\"%s\"/>", UUID.randomUUID(), v1, v22));
            sb.append(String.format("<edge id= \"%s\" source=\"%s\" target=\"%s\"/>", UUID.randomUUID(), v21, v31));
            sb.append(String.format("<edge id= \"%s\" source=\"%s\" target=\"%s\"/>", UUID.randomUUID(), v21, v32));
            sb.append(String.format("<edge id= \"%s\" source=\"%s\" target=\"%s\"/>", UUID.randomUUID(), v22, v33));

            sb.append(String.format("<edge id= \"%s\" source=\"%s\" sourceport=\"%s\" target=\"%s\" />", UUID.randomUUID(), v33, p331, v41));
            sb.append(String.format("<edge id= \"%s\" source=\"%s\" sourceport=\"%s\" target=\"%s\" />", UUID.randomUUID(), v33, p332, v42));

            sb.append(String.format("<edge id= \"%s\" source=\"%s\" target=\"%s\"/>", UUID.randomUUID(), v41, v5));
            sb.append(String.format("<edge id= \"%s\" source=\"%s\" target=\"%s\"/>", UUID.randomUUID(), v42, v5));
        }

        sb.append("</graph>" + "</graphml>");

        String s = XmlUtils.simpleFormatXml(sb.toString(), 2, 120);

        try (FileOutputStream fos = new FileOutputStream("example/graphml/performance2.graphml")) {
            fos.write(s.getBytes());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
