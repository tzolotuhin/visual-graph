package vg.plugin.gview;

import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.plugin.Plugin;
import vg.service.ui.UserInterfaceService;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

public class SaveToImagePlugin implements Plugin {
    // Main data
    private GraphViewTab currentGraphViewTab;

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void install() {
        currentGraphViewTab = null;

        // initialize find menu item
        final JMenuItem findMenuItem = new JMenuItem("Save to image");
        findMenuItem.setEnabled(false);

        findMenuItem.addActionListener(e -> doSaveToImage());
        findMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));

        MainService.userInterfaceService.addMenuItem(findMenuItem, UserInterfaceService.EDIT_MENU);

        MainService.userInterfaceService.addObserver(new UserInterfaceService.UserInterfaceListener() {
            @Override
            public void onChangeTab(UserInterfaceService.UserInterfaceTab tab) {
                GraphViewTab graphViewTab = null;
                if (tab instanceof GraphViewTab) {
                    graphViewTab = (GraphViewTab) tab;
                    findMenuItem.setEnabled(true);
                } else {
                    findMenuItem.setEnabled(false);
                }

                synchronized (generalMutex) {
                    currentGraphViewTab = graphViewTab;
                }
            }
        });
    }

    private void doSaveToImage() {
        MainService.executorService.execute(() -> {
            GraphViewTab copyGraphView;
            synchronized (generalMutex) {
                copyGraphView = currentGraphViewTab;
            }
            if (copyGraphView != null) {
                try {
                    copyGraphView.saveToImage("save.png");
                } catch (IOException ex) {
                    MainService.logger.printException(ex);
                    MainService.windowMessenger.errorMessage(
                            String.format("Can't save the graph view to image: '%s'.", ex.getMessage()), "Save to image", null);
                }
            }
        });
    }
}