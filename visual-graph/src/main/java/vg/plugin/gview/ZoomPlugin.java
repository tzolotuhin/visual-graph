package vg.plugin.gview;

import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.plugin.Plugin;
import vg.service.ui.UserInterfaceService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.UUID;

/**
 * This plugin installs scaling panel.
 *
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class ZoomPlugin implements UserInterfaceService.UserInterfaceInstrument, Plugin {
    // Main components
    private JPanel toolBar;

    private ZoomIn zoomIn;
    private ZoomOut zoomOut;

    private static final ImageIcon zoomInPluginIcon;
    private static final ImageIcon zoomOutPluginIcon;

    static {
        zoomInPluginIcon = new ImageIcon("./data/resources/textures/graph_view/zoom_in.png");
        zoomOutPluginIcon = new ImageIcon("./data/resources/textures/graph_view/zoom_out.png");
    }

    @Override
    public void install() {
        // initialize components
        zoomIn = new ZoomIn();
        zoomOut = new ZoomOut();

        toolBar = new JPanel(new GridBagLayout());

        toolBar.add(zoomIn.getView(), new GridBagConstraints(0,0, 1,1, 0,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));
        toolBar.add(zoomOut.getView(), new GridBagConstraints(1,0, 1,1, 0,0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0,0,0,0), 0,0));

        MainService.userInterfaceService.addInstrument(this);

        MainService.userInterfaceService.addObserver(new UserInterfaceService.UserInterfaceListener() {
            @Override
            public void onChangeTab(UserInterfaceService.UserInterfaceTab tab) {
                GraphViewTab graphView = null;
                if (tab instanceof GraphViewTab) {
                    graphView =  (GraphViewTab) tab;
                }
                zoomIn.changeView(graphView);
                zoomOut.changeView(graphView);
            }
        });
    }

    @Override
    public UUID getId() {
        return UUID.fromString("d90cef20-34d6-11e9-b56e-0800200c9a66\n");
    }

    @Override
    public int getPlace() {
        return UserInterfaceService.UserInterfaceInstrument.NORTH_PLACE;
    }

    @Override
    public void setSelected(boolean state) { }

    @Override
    public JPanel getView() {
        return toolBar;
    }

    private static class ZoomIn {
        // Main components
        private final JButton button;

        // Data
        private GraphViewTab view;

        // Mutex
        private final Object generalMutex = new Object();

        ZoomIn() {
            // init components
            button = new JButton(zoomInPluginIcon);
            button.setPreferredSize(UserInterfaceService.NORTH_INSTRUMENT_PANEL_SIZE);
            button.setToolTipText("Zoom in");
            button.addActionListener(e -> zoomIn());
            button.setEnabled(false);

            // ctrl + plus
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
                // if use VK_PLUS, that it's will not work, because JDK has bug with it.
                if (e.getID() == KeyEvent.KEY_PRESSED && (e.getKeyChar() == '+' || e.getKeyCode() == KeyEvent.VK_EQUALS) && e.isControlDown()) {
                    zoomIn();
                    return true;
                }
                return false;
            });
        }
        public JComponent getView() {
            return(this.button);
        }

        void changeView(GraphViewTab newView) {
            synchronized (generalMutex) {
                view = newView;
                button.setEnabled(view != null);
            }
        }

        private void zoomIn() {
            MainService.executorService.execute(() -> {
                synchronized (generalMutex) {
                    if (view != null) {
                        view.zoomIn();
                    }
                }
            });
        }
    }

    private static class ZoomOut {
        // Main components
        private final JButton button;

        // Data
        private GraphViewTab view;

        // Mutex
        private final Object generalMutex = new Object();

        ZoomOut() {
            // init components
            button = new JButton(zoomOutPluginIcon);
            button.setPreferredSize(UserInterfaceService.NORTH_INSTRUMENT_PANEL_SIZE);
            button.setToolTipText("Zoom out");
            button.addActionListener(e -> zoomOut());
            button.setEnabled(false);

            // ctrl + minus
            KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
                // if use VK_MINUS, that it's will not work, because JDK has bug with it.
                if (e.getID() == KeyEvent.KEY_PRESSED && (e.getKeyChar() == '-' || e.getKeyCode() == KeyEvent.VK_MINUS) && e.isControlDown()) {
                    zoomOut();
                    return(true);
                }
                return(false);
            });
        }

        public JComponent getView() {
            return button;
        }

        void changeView(GraphViewTab newView) {
            synchronized (generalMutex) {
                view = newView;
                button.setEnabled(view != null);
            }
        }

        private void zoomOut() {
            MainService.executorService.execute(() -> {
                synchronized (generalMutex) {
                    if (view != null) {
                        view.zoomOut();
                    }
                }
            });
        }
    }
}
