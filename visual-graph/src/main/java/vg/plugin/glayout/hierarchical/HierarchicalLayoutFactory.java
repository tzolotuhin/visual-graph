package vg.plugin.glayout.hierarchical;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.Validate;
import vg.lib.layout.hierarchical.HierarchicalLayout;
import vg.lib.layout.hierarchical.HierarchicalLayoutSettings;
import vg.lib.view.elements.GVParentWithElements;
import vg.lib.view.shapes.vshape.GraphViewCircleShape;
import vg.lib.view.shapes.vshape.GraphViewLeftFragmentShape;
import vg.lib.view.shapes.vshape.GraphViewRectangleShape;
import vg.lib.view.shapes.vshape.GraphViewVertexShape;
import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.operation.OperationService;
import vg.shared.gui.data.CheckSettingAttribute;
import vg.shared.gui.data.SelectorSettingAttribute;
import vg.shared.gui.data.ValueSettingAttribute;

import java.awt.*;
import java.util.Arrays;
import java.util.Map;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class HierarchicalLayoutFactory extends OperationService.OperationFactory {
    // Constants
    private static final String GRAPH_VIEW = "GRAPH_VIEW";
    private static final String SPACE_X = "SPACE_X";
    private static final String SPACE_Y = "SPACE_Y";
    private static final String PORT_SPACE_Y = "PORT_SPACE_Y";
    private static final String LAYERING = "LAYERING";
    private static final String ALIGNMENT = "ALIGNMENT";
    private static final String CROSSING_REDUCTION = "CROSSING_REDUCTION";
    private static final String COLLAPSE_EDGE_SRC = "COLLAPSE_EDGE_SRC";
    private static final String COLLAPSE_EDGE_TRG = "COLLAPSE_EDGE_TRG";
    private static final String ROUTING_STYLE = "ROUTING_STYLE";
    private static final String FRAGMENT_SHAPE_STYLE = "FRAGMENT_SHAPE_STYLE";
    private static final String DEFAULT_SHAPE_STYLE = "DEFAULT_SHAPE_STYLE";

    private static final Map<String, Integer> layeringMap;
    private static final Map<String, Integer> alignmentMap;
    private static final Map<String, Integer> crossingReductionMap;
    private static final Map<String, Integer> routingStyleMap;
    private static final Map<String, Integer> fragmentShapeStyleMap;
    private static final Map<String, Integer> defaultShapeStyleMap;

    static {
        layeringMap = Map.of(
            "Legacy", HierarchicalLayoutSettings.LEGACY_LAYERING_ALGORITHM,
            "Custom", HierarchicalLayoutSettings.CUSTOM_LAYERING_ALGORITHM);

        alignmentMap = Maps.newLinkedHashMap();
        alignmentMap.put("Left", HierarchicalLayoutSettings.LEFT_ALIGNMENT_ALGORITHM);
        alignmentMap.put("General", HierarchicalLayoutSettings.GENERAL_ALIGNMENT_ALGORITHM);
        alignmentMap.put("Legacy", HierarchicalLayoutSettings.LEGACY_ALIGNMENT_ALGORITHM);
        alignmentMap.put("Custom", HierarchicalLayoutSettings.CUSTOM_LAYERING_ALGORITHM);

        crossingReductionMap = Maps.newLinkedHashMap();
        crossingReductionMap.put("Default order", HierarchicalLayoutSettings.CROSSING_REDUCTION_WITH_DEFAULT_ORDER_ALGORITHM);
        crossingReductionMap.put("Legacy", HierarchicalLayoutSettings.LEGACY_CROSSING_REDUCTION_ALGORITHM);
        crossingReductionMap.put("General", HierarchicalLayoutSettings.GENERAL_CROSSING_REDUCTION_ALGORITHM);
        crossingReductionMap.put("Custom", HierarchicalLayoutSettings.CUSTOM_CROSSING_REDUCTION_ALGORITHM);

        routingStyleMap = Maps.newLinkedHashMap();
        routingStyleMap.put("Polyline", HierarchicalLayoutSettings.POLYLINE_ROUTING_STYLE);
        routingStyleMap.put("Orthogonal", HierarchicalLayoutSettings.ORTHOGONAL_ROUTING_STYLE);
        routingStyleMap.put("Spline", HierarchicalLayoutSettings.SPLINE_ROUTING_STYLE);

        fragmentShapeStyleMap = Maps.newLinkedHashMap();
        fragmentShapeStyleMap.put("Show fragment info", GraphViewVertexShape.LEFT_FRAGMENT_VERTEX_SHAPE);
        fragmentShapeStyleMap.put("Do not show fragment info", GraphViewVertexShape.RECTANGLE_VERTEX_SHAPE);

        defaultShapeStyleMap = Maps.newLinkedHashMap();
        defaultShapeStyleMap.put("Rectangle", GraphViewVertexShape.RECTANGLE_VERTEX_SHAPE);
        defaultShapeStyleMap.put("Circle", GraphViewVertexShape.CIRCLE_VERTEX_SHAPE);
    }

    public HierarchicalLayoutFactory() {
        modifyDeclaredArg(SPACE_X, new ValueSettingAttribute("Space X", 20));
        modifyDeclaredArg(SPACE_Y, new ValueSettingAttribute("Space Y", 60));
        modifyDeclaredArg(PORT_SPACE_Y, new ValueSettingAttribute("Port space Y", 20));

        modifyDeclaredArg(
            LAYERING,
            new SelectorSettingAttribute(
                "Layering",
                Arrays.asList("Legacy", "Custom"),
                "Custom"));

        modifyDeclaredArg(
            CROSSING_REDUCTION,
            new SelectorSettingAttribute(
                "Crossing Reduction",
                Arrays.asList("Default order", "General", "Legacy", "Custom"),
                "Custom"));

        modifyDeclaredArg(
            ALIGNMENT,
            new SelectorSettingAttribute(
                "Alignment",
                Arrays.asList("Left", "General", "Legacy", "Custom"),
                "Legacy"));

        modifyDeclaredArg(COLLAPSE_EDGE_SRC, new CheckSettingAttribute("Collapse sources", false));
        modifyDeclaredArg(COLLAPSE_EDGE_TRG, new CheckSettingAttribute("Collapse targets", false));
        modifyDeclaredArg(ROUTING_STYLE, new SelectorSettingAttribute("Routing style", Arrays.asList("Polyline", "Spline"), "Polyline"));
        modifyDeclaredArg(FRAGMENT_SHAPE_STYLE, new SelectorSettingAttribute("Fragment style", Arrays.asList("Show fragment info", "Do not show fragment info"), "Show fragment info"));
        modifyDeclaredArg(DEFAULT_SHAPE_STYLE, new SelectorSettingAttribute("Default shape", Arrays.asList("Rectangle", "Circle"), "Rectangle"));
    }

    @Override
    public OperationService.BaseProcedure buildOperation(Map<String, Object> args) {
        return new HierarchicalLayoutBaseProcedure(args);
    }

    @Override
    public String getGroup() {
        return LAYOUT_GROUP;
    }

    @Override
    public String getName() {
        return "Hierarchical layout";
    }

    private static class HierarchicalLayoutBaseProcedure extends OperationService.BaseProcedure {
        HierarchicalLayoutBaseProcedure(Map<String, Object> args) {
            this.args = args;
        }

        @Override
        public void execute() {
            GraphViewTab graphViewArg = (GraphViewTab) getArgs().get(GRAPH_VIEW);
            try {
                // initialize parameters.
                int xSpace = getArg(SPACE_X, ValueSettingAttribute.class).getCurrValue();
                int ySpace = getArg(SPACE_Y, ValueSettingAttribute.class).getCurrValue();
                int yPortSpace = getArg(PORT_SPACE_Y, ValueSettingAttribute.class).getCurrValue();

                int layeringAlgorithm = layeringMap
                    .get(getArg(LAYERING, SelectorSettingAttribute.class).getSelectedItem());

                int crossingReductionAlgorithm = crossingReductionMap
                    .get(getArg(CROSSING_REDUCTION, SelectorSettingAttribute.class).getSelectedItem());

                int alignment = alignmentMap.get(getArg(ALIGNMENT, SelectorSettingAttribute.class).getSelectedItem());

                boolean collapseEdgeSrc = getArg(COLLAPSE_EDGE_SRC, CheckSettingAttribute.class).isCurrValue();
                boolean collapseEdgeTrg = getArg(COLLAPSE_EDGE_TRG, CheckSettingAttribute.class).isCurrValue();
                int routingStyle = routingStyleMap.get(getArg(ROUTING_STYLE, SelectorSettingAttribute.class).getSelectedItem());
                int fragmentShapeStyle = fragmentShapeStyleMap.get(getArg(FRAGMENT_SHAPE_STYLE, SelectorSettingAttribute.class).getSelectedItem());
                int defaultShapeStyle = defaultShapeStyleMap.get(getArg(DEFAULT_SHAPE_STYLE, SelectorSettingAttribute.class).getSelectedItem());
                int arrowSize = 6;
                int baseArrowSize = 10;

                var settings = HierarchicalLayoutSettings.builder()
                        .minHSpaceBetweenVertices(xSpace)
                        .minVSpaceBetweenRows(ySpace)
                        .minVSpaceBetweenPorts(yPortSpace)
                        .layeringAlgorithm(layeringAlgorithm)
                        .crossingReductionAlgorithm(crossingReductionAlgorithm)
                        .alignment(alignment)
                        .collapseEdgeSrc(collapseEdgeSrc)
                        .collapseEdgeTrg(collapseEdgeTrg)
                        .routingStyle(routingStyle)
                        .fragmentShapeStyle(fragmentShapeStyle)
                        .defaultShapeStyle(defaultShapeStyle)
                        .arrowSize(arrowSize)
                        .baseArrow(baseArrowSize)
                        .freeSpaceY(ySpace - 2 * arrowSize - 2 * baseArrowSize)
                        .build();

                Validate.isTrue(ySpace >= 10, "Space Y should be more 40.");

                // execute layout.
                graphViewArg.lock("Please wait. Apply the layout...");
                doPrepareShapes(graphViewArg, settings);
                graphViewArg.updateElementsSize();
                doCustomShapes(graphViewArg, settings);

                int crosses = new HierarchicalLayout(graphViewArg, settings).execute();

                graphViewArg.setInfo("Crosses = " + crosses);

                graphViewArg.refreshView();
                graphViewArg.unlock();

                MainService.logger.printDebug("Finish the layout method.");
            } catch (Throwable ex) {
                MainService.logger.printException(ex);
                MainService.windowMessenger.errorMessage(
                        String.format("Something went wrong...\nDetails: '%s'.", ex.getMessage()),
                        "Hierarchical layout error",
                        null);
                graphViewArg.unlock();
            } finally {
                finish();
            }
        }

        private void doPrepareShapes(GraphViewTab graphView, HierarchicalLayoutSettings settings) {
            graphView.dfs(new GraphViewTab.GraphViewHandler() {
                @Override
                public void onParentWithElements(GVParentWithElements parentWithElements) {
                    // setup new shape.
                    var vertices = parentWithElements.getVertices();
                    vertices.forEach(vertex -> {
                        if (vertex.isFakePort()) {
                            var shape = vertex.getShape();
                            shape.getLabel().setVisible(false);
                            vertex.setShape(new GraphViewCircleShape(shape));
                        } else if (vertex.isVertexWithPorts()) {
                            vertex.setShape(new GraphViewRectangleShape(vertex.getShape()));
                        } else if (vertex.isFragment()) {
                            if (settings.fragmentShapeStyle == GraphViewVertexShape.LEFT_FRAGMENT_VERTEX_SHAPE) {
                                vertex.setShape(new GraphViewLeftFragmentShape(vertex.getShape()));
                            } else {
                                vertex.setShape(new GraphViewRectangleShape(vertex.getShape()));
                            }
                        } else {
                            vertex.setShape(vertex.getInitialShape(settings.defaultShapeStyle));
                        }
                    });

                    graphView.modifyElements(vertices, null);
                }
            });
        }

        private void doCustomShapes(GraphViewTab graphView, HierarchicalLayoutSettings settings) {
            graphView.dfs(new GraphViewTab.GraphViewHandler() {
                @Override
                public void onParentWithElements(GVParentWithElements parentWithElements) {
                    // setup new shape.
                    var vertices = parentWithElements.getVertices();
                    vertices.forEach(vertex -> {
                        if (vertex.isFakePort()) {
                            var shape = vertex.getShape();
                            shape.setSize(new Point(20, 20));
                            vertex.setShape(new GraphViewCircleShape(shape));
                        }
                    });

                    // setup arrow size.
                    parentWithElements.getEdges().forEach(edge -> {
                        edge.getShape().setArrowSize(settings.arrowSize);
                    });

                    graphView.modifyElements(vertices, null);
                }
            });
        }
    }
}
