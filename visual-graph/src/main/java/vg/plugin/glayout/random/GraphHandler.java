package vg.plugin.glayout.random;

import com.google.common.collect.Lists;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVVertex;
import vg.service.gview.GraphViewTab;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
TODO: add components
      handle ports correctly
 */

public class GraphHandler {
    private GraphViewTab graphView;
    private GVVertex parent;
    private List<GVVertex> vertices;
    private List<GVVertex> inputPorts;
    private List<GVVertex> outputPorts;
    private List<GVEdge> edges;


    GraphHandler() {
    }

    void handle(GraphViewTab gv, GVVertex parent) {
        this.graphView = gv;
        this.parent = parent;

        var parentWithElements = gv.getElements(parent);
        this.vertices = parentWithElements.getVertices();
        this.edges = parentWithElements.getEdges();

        getPorts();

        handleVertices();
        if(parent != null) {
            setParentSize();
            handlePorts();
        }

        save();
        handleEdges();
        save();
    }

    private void handleVertices() {
        LayoutRegion layoutRegion = new LayoutRegion();

        for(var v : vertices) {
            v.getShape().setPosition(layoutRegion.getRandom());
        }
    }

    private void handleEdges() {
        GVVertex source, target;

        for (var e : edges) {
            source = graphView.getVertex(e.getSrcId());
            target = graphView.getVertex(e.getTrgId());

            List<Point> points = Lists.newArrayList();

            points.add(source.getShape().getCenterPosition());
            points.add(getInputPosition(source, target));
            e.getShape().setPoints(points);
        }
    }

    //TODO: fix the ports positioning
    private void handlePorts() {
        for(var v : inputPorts) {
            v.getShape().setPosition(getInputPos(parent, new Point(0, 0)));
        }

        for(var v : outputPorts) {
            v.getShape().setPosition(getOutputPos(parent, new Point(0, 0)));
        }
    }

    private void save() {
        if (parent != null) {
            graphView.modifyElements(Collections.singletonList(parent), null);

            graphView.modifyElements(inputPorts, null);
            graphView.modifyElements(outputPorts, null);
        }

        graphView.modifyElements(vertices, edges);
    }

    /**
     * This method returns the exact point on the boundary of a target vertex
     * where an arrow sign of the edge should be placed in.
     *
     * The choose of a place is based on calculating the point of intersecting the target
     * boundary with the line connecting the source and target center points
     * according to the basic properties of similar triangles.
     *
     * It also uses the polar coordinate system to avoid the necessity of considering
     * several cases of vertices relative positions
     *
     * Note: it might be unnecessary to consider the case of infinite and zero tangent
     *       because of the specific way of choosing vertices positions in this layout
     */

    private Point getInputPosition(GVVertex source, GVVertex target) {
        Point sourceCenter = source.getShape().getCenterPosition();
        Point targetCenter = target.getShape().getCenterPosition();

        double oldDistance = Math.sqrt(Math.pow(sourceCenter.x - targetCenter.x, 2) +
                Math.pow((sourceCenter.y - targetCenter.y), 2));
        double sine, cosine, tangent = 0;
        double stretchingFactor;

        //slopes of target's diagonals
        double rightSlope = (double) target.getShape().getSize().y / target.getShape().getSize().x;
        double leftSlope = - rightSlope;

        //Defining the polar angle of a desired point
        if ((targetCenter.x == sourceCenter.x) && (targetCenter.y > sourceCenter.y)) { //infinite derivative
            sine = 1;
            cosine = 0;
        } else if((targetCenter.x == sourceCenter.x) && (targetCenter.y < sourceCenter.y)) {
            sine = -1;
            cosine = 0;
        } else { //finite derivative
            tangent = (double) (targetCenter.y - sourceCenter.y) /
                    (targetCenter.x - sourceCenter.x);
            sine = Math.signum(targetCenter.y - sourceCenter.y) *
                    Math.sqrt(tangent * tangent / (1 + tangent * tangent));
            cosine = Math.signum(targetCenter.x - sourceCenter.x) *
                    Math.sqrt(1 / (1 + tangent * tangent));
        }

        if ((tangent >= 0 && tangent <= rightSlope)
                || (tangent < 0 && Math.abs(tangent) < Math.abs(leftSlope))) {
            stretchingFactor = (double) Math.abs(targetCenter.x - sourceCenter.x) * 2 / target.getShape().getSize().x;
            if (Math.abs(stretchingFactor) > 1e-3) {
                stretchingFactor = 1 - 1 / stretchingFactor;
            }
        } else {
            if (cosine == 0) {
                stretchingFactor = 1 - target.getShape().getSize().y / 2 / oldDistance;
            } else {
                stretchingFactor = (double) Math.abs(targetCenter.y - sourceCenter.y) * 2 / target.getShape().getSize().y;
                if (Math.abs(stretchingFactor) > 1e-3) {
                    stretchingFactor = 1 - 1 / stretchingFactor;
                }
            }
        }

        double newDistance = oldDistance * stretchingFactor;

        return new Point(Math.round(sourceCenter.x + (float)(newDistance * cosine)),
                Math.round(sourceCenter.y + (float)(newDistance * sine)));
    }

    private void getPorts() {
        inputPorts = Lists.newArrayList();
        outputPorts = Lists.newArrayList();

        GVVertex vertex;
        var it = vertices.iterator();

        while(it.hasNext()) {
            vertex = it.next();

            if(vertex.isPort()) {
                if(vertex.isInputPort()) {
                    inputPorts.add(vertex);
                } else {
                    outputPorts.add(vertex);
                }

                it.remove();
            }
        }
    }

    private Point getInputPos(GVVertex v, Point init) {
        int w = v.getShape().getSize().x;

        return new Point(init.x + w / 2, init.y);
    }

    private Point getOutputPos(GVVertex v, Point init) {
        int w = v.getShape().getSize().x;
        int h = v.getShape().getSize().y;

        return new Point(init.x + w / 2, init.y + h - 7);
    }

    private void setParentSize() {
        var maxSize = getMax();

        int width = maxSize.x * vertices.size();
        int height = maxSize.y * vertices.size();

        parent.getShape().setSize(new Point(width, height));
    }

    private Point getMax() {
        int maxWidth = 0;
        int maxHeight = 0;
        int width, height;

        for (var v : vertices) {
            width = v.getShape().getSize().x;
            height = v.getShape().getSize().y;

            if(width > maxWidth) {
                maxWidth = width;
            }

            if(height > maxHeight) {
                maxHeight = height;
            }
        }
        return new Point(maxWidth, maxHeight);
    }

    private class LayoutRegion {
        private ArrayList<Integer> xPositions;
        private ArrayList<Integer> yPositions;

        private LayoutRegion() {
            Point max = getMax();
            xPositions = Lists.newArrayList(vertices.size());
            yPositions = Lists.newArrayList(vertices.size());

            //set coordinates of grid nodes
            for (int i = 0; i < vertices.size(); i++) {
                xPositions.add(i * max.x);
                yPositions.add(i * max.y);
            }
        }

        private Point getRandom() {
            int randomX, randomY;
            int tempX, tempY;

            //choose random coordinates
            randomX = (int) (Math.random() * xPositions.size());
            randomY = (int) (Math.random() * yPositions.size());

            //replace them with last elements in their lists
            tempX = xPositions.get(xPositions.size() - 1);
            tempY = yPositions.get(yPositions.size() - 1);
            xPositions.set(xPositions.size() - 1, xPositions.get(randomX));
            yPositions.set(yPositions.size() - 1, yPositions.get(randomY));
            xPositions.set(randomX, tempX);
            yPositions.set(randomY, tempY);

            //remove elements we've already chosen
            return new Point(xPositions.remove(xPositions.size() - 1),
                             yPositions.remove(yPositions.size() - 1));
        }
    }
}
