package vg.plugin.glayout.random;

import vg.lib.view.elements.GVVertex;
import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.operation.OperationService;

import java.util.Map;

public class RandomLayoutFactory extends OperationService.OperationFactory {
    // Constants
    private static final String GRAPH_VIEW = "GRAPH_VIEW";

    public RandomLayoutFactory() {
    }

    @Override
    public OperationService.BaseProcedure buildOperation(Map<String, Object> args) {
        return new RandomLayout(args);
    }

    @Override
    public String getGroup() {
        return LAYOUT_GROUP;
    }

    @Override
    public String getName() {
        return "Random layout";
    }

    private static class RandomLayout extends OperationService.BaseProcedure {
        private GraphViewTab graphViewTabArg;

        RandomLayout(Map<String, Object> args) {
            this.args = args;
        }

        @Override
        public void execute() {
            try {
                // initialize parameters.
                graphViewTabArg = (GraphViewTab) getArgs().get(GRAPH_VIEW);

                // execute layout
                graphViewTabArg.lock("Please wait. Apply the layout...");
                graphViewTabArg.updateElementsSize();

                //minimize(graphViewArg, (UUID) null, 20);
                doLayout(graphViewTabArg, null);

                graphViewTabArg.refreshView();
                graphViewTabArg.unlock();

                MainService.logger.printDebug("Finish the layout method.");
            } catch (Throwable ex) {
                MainService.logger.printException(ex);
                MainService.windowMessenger.errorMessage(
                        String.format("Something went wrong...\nDetails: '%s'.", ex.getMessage()),
                        "Random layout error",
                        null);
            } finally {
                finish();
            }
        }

/*        private void minimize(GraphView gv, UUID parentId, int size) {
            var elements = gv.getElements(parentId);
            var edges = elements.getValue();
            var vertices = elements.getKey();

            for (GraphView.GVVertex v : vertices) {
                if(!v.isPort()) {
                    v.setSize(new Point(size, size));
                    minimize(gv, v.getId(), size);
                }
            }

            gv.modifyElements(vertices, edges);
        }*/
        private void doLayout(GraphViewTab gv, GVVertex parent) {
            var parentWithElements = gv.getElements(parent);
            for(var v : parentWithElements.getVertices()) {
                if(v.isFragment()) {
                    doLayout(gv, v);
                }
            }
            new GraphHandler().handle(gv, parent);
        }
    }
}
