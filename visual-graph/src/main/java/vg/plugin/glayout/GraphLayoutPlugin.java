package vg.plugin.glayout;

import com.google.common.collect.Maps;
import org.apache.commons.lang3.Validate;
import vg.plugin.glayout.circle.CircleLayoutFactory;
import vg.plugin.glayout.random.RandomLayoutFactory;
import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.operation.OperationService;
import vg.service.main.VGMainGlobals;
import vg.service.plugin.Plugin;
import vg.service.resource.ResourceService;
import vg.service.ui.UserInterfaceService;
import vg.plugin.glayout.hierarchical.HierarchicalLayoutFactory;
import vg.shared.gui.UIUtils;
import vg.shared.gui.data.SettingAttribute;
import vg.shared.utils.CollectionUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

public class GraphLayoutPlugin implements Plugin, UserInterfaceService.UserInterfacePanel {
    // Constants
    private static final int NO_GRAPH_VIEW_STATE = 0;
    private static final int GRAPH_VIEW_STATE = 1;
    private static final int GRAPH_VIEW_LAYOUT_IN_PROGRESS_STATE = 2;

    private static final Dimension valueLabelDimension = new Dimension(100, 20);
    private static final Insets rowInsets = new Insets(2, 5, 2, 5);

    // Main components.
    private JPanel outView, innerView;

    private UserInterfaceService.UserInterfaceInstrument instrument;

    private JLabel infoLabel;
    private JLabel pleaseWaitInfoLabel;
    private JLabel currentLayoutLabel;
    private JLabel currentLayoutSettingsLabel;

    private JComboBox<String> layoutComboBox;
    private JButton runButton;
    private JButton cancelButton;

    // Main data.
    private GraphViewTab currentGraphView;
    private OperationService.OperationFactory currentLayoutFactory;

    private final Map<GraphViewTab, Integer> graphViewToState = Maps.newHashMap();
    private final Map<GraphViewTab, OperationService.BaseProcedure> graphViewToOperation = Maps.newHashMap();

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void install() {
        // install factories.
        MainService.operationService.registerOperationFactory(new GraphLayoutFactory(this));
        MainService.operationService.registerOperationFactory(new HierarchicalLayoutFactory());
        MainService.operationService.registerOperationFactory(new RandomLayoutFactory());
        MainService.operationService.registerOperationFactory(new CircleLayoutFactory());

        // create ui components.
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        infoLabel = new JLabel("If you want to use Layout Manager you need select tab with graph view");
        infoLabel.setHorizontalAlignment(JLabel.CENTER);
        pleaseWaitInfoLabel = new JLabel("Please wait...");
        pleaseWaitInfoLabel.setHorizontalAlignment(JLabel.CENTER);

        currentLayoutLabel = new JLabel("Select layout:");
        currentLayoutSettingsLabel = new JLabel("Settings:");
        currentLayoutLabel.setFont(ResourceService.SUB_TITLE_FONT);
        currentLayoutSettingsLabel.setFont(ResourceService.SUB_TITLE_FONT);

        cancelButton = new JButton("Cancel");
        runButton = new JButton("Run");
        // TODO: need add method in Utils for adding of new shortcuts.
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
            if (e.getID() == KeyEvent.KEY_PRESSED && e.getKeyCode() == KeyEvent.VK_F5) {
                doExecuteLayoutSync(currentGraphView);
                return true;
            }
            return false;
        });

        layoutComboBox = new JComboBox<>();

        layoutComboBox.addActionListener(e -> {
            synchronized (generalMutex) {
                String selectedItem = Objects.requireNonNull(layoutComboBox.getSelectedItem()).toString();
                currentLayoutFactory = MainService.operationService.getOperationFactoryByName(selectedItem);
                rebuildView();
            }
        });

        for (var graphAlgorithmFactory : MainService.operationService.getRegisteredOperationFactories()) {
            if (graphAlgorithmFactory.getGroup().equalsIgnoreCase(OperationService.OperationFactory.LAYOUT_GROUP)) {
                layoutComboBox.addItem(graphAlgorithmFactory.getName());
            }
        }

        instrument = UIUtils.createInstrument(
                UUID.fromString("3b08b470-34d7-11e9-b56e-0800200c9a66"),
                "Layout",
                UserInterfaceService.UserInterfaceInstrument.WEST_PLACE,
                270,
                e -> MainService.userInterfaceService.selectPanel(GraphLayoutPlugin.this)
        );

        MainService.userInterfaceService.addObserver(new UserInterfaceService.UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceService.UserInterfaceTab tab) {
                if (!(tab instanceof GraphViewTab)) {
                    return;
                }

                synchronized (generalMutex) {
                    currentGraphView = (GraphViewTab) tab;

                    graphViewToState.put(currentGraphView, GRAPH_VIEW_STATE);
                }
            }

            @Override
            public void onChangeTab(UserInterfaceService.UserInterfaceTab tab) {
                synchronized (generalMutex) {
                    currentGraphView = null;
                    if (tab instanceof GraphViewTab) {
                        currentGraphView = (GraphViewTab) tab;
                    }
                    rebuildView();
                }
            }
        }, 15);

        cancelButton.addActionListener(e -> {
            synchronized (generalMutex) {
                if (currentGraphView == null) {
                    return;
                }

                var operation = graphViewToOperation.get(currentGraphView);

                if (operation == null) {
                    return;
                }

                operation.stop();

                rebuildView();
            }
        });

        runButton.addActionListener(e -> doExecuteLayoutSync(currentGraphView));

        rebuildView();

        MainService.userInterfaceService.addPanel(this);
    }

    @Override
    public UUID getId() {
        return UUID.fromString("2f33d5d0-34d7-11e9-b56e-0800200c9a66");
    }

    @Override
    public UserInterfaceService.UserInterfaceInstrument getInstrument() {
        return instrument;
    }

    @Override
    public int getPlace() {
        return UserInterfaceService.UserInterfacePanel.WEST_BOTTOM_PLACE;
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

    private void doExecuteLayoutSync(GraphViewTab graphViewTab) {
        synchronized (generalMutex) {
            doExecuteLayout(graphViewTab);
        }
    }

    private void doExecuteLayout(GraphViewTab graphViewTab) {
        if (graphViewTab == null || currentLayoutFactory == null) {
            return;
        }

        var declaredArgs = currentLayoutFactory.getDeclaredArgs();
        declaredArgs.put("GRAPH_VIEW", currentGraphView);

        var operation = currentLayoutFactory.buildOperation(declaredArgs);

        graphViewToState.put(graphViewTab, GRAPH_VIEW_LAYOUT_IN_PROGRESS_STATE);
        graphViewToOperation.put(graphViewTab, operation);

        rebuildView();

        MainService.executorService.executeTaskByKey(graphViewTab.getId().toString(), () -> {
            synchronized (generalMutex) {
                operation.execute();
                MainService.executorService.executeInEDT(() -> {
                    synchronized (generalMutex) {
                        graphViewToState.put(graphViewTab, GRAPH_VIEW_STATE);
                        graphViewToOperation.remove(graphViewTab);
                        rebuildView();
                    }
                });
            }
        });
    }

    private void rebuildView() {
        innerView.removeAll();

        int state = NO_GRAPH_VIEW_STATE;

        if (currentGraphView != null) {
            state = graphViewToState.get(currentGraphView);
        }

        switch (state) {
            case NO_GRAPH_VIEW_STATE:
                innerView.add(infoLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                break;

            case GRAPH_VIEW_STATE:
                layoutComboBox.setEnabled(true);
                int index = 0;

                // add general settings to panel.
                innerView.add(currentLayoutLabel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));

                // add layout value label to panel.
                layoutComboBox.setMinimumSize(valueLabelDimension);
                layoutComboBox.setPreferredSize(layoutComboBox.getMinimumSize());
                layoutComboBox.setMaximumSize(layoutComboBox.getMinimumSize());
                innerView.add(layoutComboBox, new GridBagConstraints(
                        0,
                        index++,
                        1,
                        1,
                        1,
                        0,
                        GridBagConstraints.WEST,
                        GridBagConstraints.HORIZONTAL,
                        rowInsets,
                        0,
                        0));

                // add current layout settings to panel.
                innerView.add(currentLayoutSettingsLabel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(10, 0, 0, 0), 0, 0));

                innerView.add(generateSettingsPanel(), new GridBagConstraints(
                        0,
                        index++,
                        1,
                        1,
                        1,
                        0,
                        GridBagConstraints.NORTHWEST,
                        GridBagConstraints.HORIZONTAL,
                        rowInsets,
                        0,
                        0));

                innerView.add(runButton, new GridBagConstraints(0, index, 1, 1, 1, 1, GridBagConstraints.SOUTH, GridBagConstraints.NONE, new Insets(10, 0, 10, 0), 0, 0));
                break;

            case GRAPH_VIEW_LAYOUT_IN_PROGRESS_STATE:
                layoutComboBox.setEnabled(false);
                innerView.add(layoutComboBox, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
                innerView.add(pleaseWaitInfoLabel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                innerView.add(cancelButton, new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
                break;
        }

        innerView.updateUI();
    }

    private JPanel generateSettingsPanel() {
        List<SettingAttribute> attributes = null;
        if (currentLayoutFactory != null) {
            attributes = CollectionUtils.checkedFilter(SettingAttribute.class, currentLayoutFactory.getDeclaredArgs().values());
        }

        return UIUtils.generateSettingsPanelViaAttributes(attributes);
    }

    private static class GraphLayoutFactory extends OperationService.OperationFactory {
        private final GraphLayoutPlugin graphLayoutPlugin;

        public GraphLayoutFactory(GraphLayoutPlugin graphLayoutPlugin) {
            this.graphLayoutPlugin = graphLayoutPlugin;
        }

        @Override
        public OperationService.BaseProcedure buildOperation(Map<String, Object> args) {
            var graphViewTab = args.getOrDefault(VGMainGlobals.GRAPH_VIEW_ARG, null);

            Validate.notNull(graphViewTab);
            Validate.isInstanceOf(GraphViewTab.class, graphViewTab);

            return new GraphLayoutProcedure(graphLayoutPlugin, (GraphViewTab)graphViewTab);
        }

        @Override
        public String getName() {
            return VGMainGlobals.LAYOUT_OPERATION_FACTORY;
        }
    }

    private static class GraphLayoutProcedure extends OperationService.BaseProcedure {
        private final GraphLayoutPlugin graphLayoutPlugin;
        private final GraphViewTab graphViewTab;

        public GraphLayoutProcedure(GraphLayoutPlugin graphLayoutPlugin, GraphViewTab graphViewTab) {
            this.graphLayoutPlugin = graphLayoutPlugin;
            this.graphViewTab = graphViewTab;
        }

        @Override
        public void execute() {
            graphLayoutPlugin.doExecuteLayoutSync(graphViewTab);
        }
    }
}
