package vg.plugin.glayout.circle;

import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVVertex;
import vg.service.gview.GraphViewTab;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class GraphSorter {
    static final int NONE_CROSSING_REDUCTION = 0;
    static final int BUBBLE_CROSSING_REDUCTION = 1;
    static final int GROUPING_CROSSING_REDUCTION = 2;

    final double eps = 0.01;

    private final CircleLayoutSettings settings;

    private GraphViewTab graphView;
    private GVVertex parent;

    private List<GVEdge> edges;
    private List<GVVertex> vertices;

    private Map<UUID, Point> graphMap;
    private Map<UUID, List<UUID>> graphConnections;

    GraphSorter(Map<UUID, Point> map, CircleLayoutSettings settings) {
        this.settings = new CircleLayoutSettings(settings);
        this.graphConnections = new HashMap<>();
        this.graphMap = map;
    }

    void sort(GraphViewTab gv, GVVertex parent) {
        if(settings.getCrossingReductionAlgorithm() == NONE_CROSSING_REDUCTION) {
            return;
        }

        this.graphView = gv;
        this.parent = parent;

        var parentWithElements = gv.getElements(parent);
        this.vertices = parentWithElements.getVertices();
        this.edges = parentWithElements.getEdges();

        excludePorts();
        if(vertices.size() >= settings.getMinimalVerticesToCrossingReduction()) {
            switch(settings.getCrossingReductionAlgorithm()) {
                case BUBBLE_CROSSING_REDUCTION:
                    verticesBubbleSort();
                    break;
                case GROUPING_CROSSING_REDUCTION:
                    verticesGroupingSort();
                    break;
            }
        }
    }

    private void excludePorts() {
        GVVertex vertice;
        var iter = vertices.iterator();

        while(iter.hasNext()) {
            vertice = iter.next();

            if(vertice.isPort()) {
                iter.remove();
            }
        }
    }

    private void verticesBubbleSort() {
        int counter = 0;

        while(counter < vertices.size()) {
            counter = 0;

            for(var i = 0; i < vertices.size() - 1; i++){
                if(verticesBubbleSort(vertices.get(i), vertices.get(i + 1))) {
                    counter++;
                }
            }

            if(verticesBubbleSort(vertices.get(vertices.size() - 1), vertices.get(0))) {
                counter++;
            }
        }
    }

    private boolean verticesBubbleSort(GVVertex vertex1, GVVertex vertex2) {
        var intersection1 = getIntersections(vertex1);
        var intersection2 = getIntersections(vertex2);

        swap(vertex1, vertex2);

        var intersectionSwap1 = getIntersections(vertex1);
        var intersectionSwap2 = getIntersections(vertex2);

        if (intersectionSwap1 + intersectionSwap2 >= intersection1 + intersection2) {
            swap(vertex1, vertex2);
            return true;
        } else {
            return false;
        }
    }

    /*private void verticesQsort(List<GraphView.GVVertex> vertices) {
        GraphView.GVVertex plot = vertices.get(0);
        var wellplaced = new ArrayList<GraphView.GVVertex>();
        var badplaced = new ArrayList<GraphView.GVVertex>();

        for(var v: vertices) {
            var intersections1 = getIntersections(v);
            swap();
        }
    }*/

    private void mapConnections() {
        for(var vertex: vertices) {
            graphConnections.put(vertex.getId(), new ArrayList<>());
        }

        for(var edge: edges) {
            mapConnection(edge);
        }
    }

    private void mapConnection(GVEdge edge) {
        var srcConnections = graphConnections.get(edge.getSrcId());
        var trgConnections = graphConnections.get(edge.getTrgId());

        if(srcConnections != null &&
                trgConnections != null) {

            srcConnections.add(edge.getTrgId());
            trgConnections.add(edge.getSrcId());
        }

    }

    private void verticesGroupingSort() {
        mapConnections();

        var positions = getPositionsList();
        var availableVertices = new ArrayList<GVVertex>();
        int positionNumber = 0;
        List<GVVertex> contactGroup;
        GVVertex max;

        for(GVVertex vertex : vertices) {
            availableVertices.add(vertex);
        }

        while(positionNumber < positions.size()) {
            max = getMostContactVertex(availableVertices, availableVertices);
            contactGroup = findContactGroup(max, availableVertices);
            checkAvailableVertices(availableVertices, max, contactGroup);

            sortContactGroup(max, contactGroup, positions, positionNumber, availableVertices);
            positionNumber += (contactGroup.size() + 1);
        }

        save();
    }

    private GVVertex getMostContactVertex(List<GVVertex> vertices, List<GVVertex> connections) {
        return findContactVertex(vertices, connections, 0, true);
    }

    private GVVertex getLeastContactVertex(List<GVVertex> vertices, List<GVVertex> connections) {
        return findContactVertex(vertices, connections, connections.size(), false);
    }

    private GVVertex findContactVertex(List<GVVertex> vertices, List<GVVertex> connections, int measureStartValue, boolean signIsGreater) {
        GVVertex result = vertices.get(0);
        int connectionsCounter, measure = measureStartValue;

        for(var vertex : vertices) {
            connectionsCounter = 0;
            var vertexConnections = graphConnections.get(vertex.getId());

            for(var connection: connections) {
                if(vertexConnections.contains(connection.getId())) {
                    connectionsCounter++;
                }
            }

            if((connectionsCounter >= measure && signIsGreater) ||
                connectionsCounter <= measure && !signIsGreater) {

                measure = connectionsCounter;
                result = vertex;
            }
        }

        return result;
    }

    private List<GVVertex> findContactGroup(GVVertex vertex, List<GVVertex> connections) {
        var contactGroup = new ArrayList<GVVertex>();
        var vertexConnections = graphConnections.get(vertex.getId());

        for(var connection : connections) {
            if(vertexConnections.contains(connection.getId())) {
                contactGroup.add(connection);
            }
        }

        return contactGroup;
    }

    private void sortContactGroup(GVVertex mainVertex,
                                  List<GVVertex> contactGroup,
                                  List<Point> positions,
                                  int startPosIndex,
                                  List<GVVertex> connections) {
        int size = 1 + contactGroup.size();
        int mid = (size / 2) + 1;

        sortByConnections(contactGroup, connections);

        int mainIndex = startPosIndex + mid - 1;
        replaceVertex(mainVertex, positions.get(mainIndex));

        int sign = -1;
        int offset = 1;
        int currentOffset = mid;
        int currentIndex;

        for(int i = 0; i < contactGroup.size(); i++) {
            currentOffset += sign * offset;
            currentIndex = startPosIndex + currentOffset - 1;
            replaceVertex(contactGroup.get(i), positions.get(currentIndex));

            sign *= -1;
            offset++;
        }
    }

    private void sortByConnections(List<GVVertex> vertices, List<GVVertex> connections) {
        var connectionsList = new ArrayList<Integer>();

        for(var vertex: vertices) {
            var contactGroup = findContactGroup(vertex, connections);
            connectionsList.add(contactGroup.size());
        }

        var counter = 0;
        while(counter < vertices.size() - 1) {
            counter = 0;

            for(int i = 0; i < vertices.size() - 1; i++) {
                if(connectionsList.get(i) <= connectionsList.get(i + 1)) {
                    counter++;
                } else {
                    var tmpI = connectionsList.get(i);
                    connectionsList.set(i, connectionsList.get(i + 1));
                    connectionsList.set(i + 1, tmpI);

                    var tmpV = vertices.get(i);
                    vertices.set(i, vertices.get(i + 1));
                    vertices.set(i + 1, tmpV);
                }
            }
        }
    }

    private void replaceVertex(GVVertex vertex, Point pos) {
        setCenterPosition(vertex, pos);
        graphMap.replace(vertex.getId(), pos);
    }

    private void checkAvailableVertices(List<GVVertex> available, GVVertex mainVertex, List<GVVertex> contactGroup) {
        available.remove(mainVertex);

        for(var contactVertex: contactGroup) {
            available.remove(contactVertex);
        }
    }

    private List<Point> getPositionsList() {
        var positions = new ArrayList<Point>();

        for (GVVertex v : vertices) {
            positions.add(getCenter(v));
        }

        return positions;
    }

    private void swap(GVVertex vertex1, GVVertex vertex2) {
        Point p1 = graphMap.get(vertex1.getId());
        Point p2 = graphMap.get(vertex2.getId());

        setCenterPosition(vertex1, p2);
        setCenterPosition(vertex2, p1);

        graphMap.replace(vertex1.getId(), p2);
        graphMap.replace(vertex2.getId(), p1);

        var changeList = new ArrayList<GVVertex>();
        changeList.add(vertex1);
        changeList.add(vertex2);

        save(changeList, null);
    }

    private Point getCenter(GVVertex v) {
        Point pos = v.getShape().getPosition();
        Point size = v.getShape().getSize();

        return new Point(pos.x + size.x / 2, pos.y + size.y / 2);
    }

    private void setCenterPosition(GVVertex vertex, Point point) {
        int x = point.x - (int)Math.round(vertex.getShape().getSize().x / 2.0);
        int y = point.y - (int)Math.round(vertex.getShape().getSize().y / 2.0);
        vertex.getShape().setPosition(new Point(x, y));
    }

    private int getIntersections(GVVertex vertex) {
        int intersections = 0;
        UUID id = vertex.getId();

        for(var relatedEdge: edges) {
            var srcId = relatedEdge.getSrcId();
            var trgId = relatedEdge.getTrgId();

            if(srcId == id || trgId == id) {
                for(var e: edges) {
                    if(e.getSrcId() != srcId && e.getTrgId() != srcId
                    && e.getSrcId() != trgId && e.getTrgId() != trgId) {
                        if(checkIntersection(relatedEdge, e)) {
                            intersections++;
                        }
                    }
                }
            }
        }

        return intersections;
    }

    private int area(Point a, Point b, Point c) {
        return (b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x);
    }

    private boolean checkIntersection(int a, int b, int c, int d) {
        int tmp;
        if(a > b) {
            tmp = a;
            a = b;
            b = tmp;
        }

        if(c > d) {
            tmp = c;
            c = d;
            d = tmp;
        }

        int max;
        if(a > c) {
            max = a;
        } else {
            max = c;
        }

        int min;
        if(b < d) {
            min = b;
        } else {
            min = d;
        }

        return max <= min;
    }

    private boolean checkIntersection(GVEdge edge1, GVEdge edge2) {
        var srcPort1 = graphMap.get(edge1.getSrcId());
        var trgPort1 = graphMap.get(edge1.getTrgId());
        var srcPort2 = graphMap.get(edge2.getSrcId());
        var trgPort2 = graphMap.get(edge2.getTrgId());

        if(srcPort1 == null || srcPort2 == null || trgPort1 == null || trgPort2 == null) {
            return false;
        } else {
            if(checkIntersection(srcPort1.x, trgPort1.x, srcPort2.x, trgPort2.x) &&
                    checkIntersection(srcPort1.y, trgPort1.y, srcPort2.y, trgPort2.y) &&
                    (long)area(srcPort1, trgPort1, srcPort2) * (long)area(srcPort1, trgPort1, trgPort2) <= eps &&
                    (long)area(srcPort2, trgPort2, srcPort1) * (long)area(srcPort2, trgPort2, trgPort1) <= eps) {
                return true;
            } else {
                return false;
            }
        }
    }

    private void save() {
        if (parent != null) {
            save(Collections.singletonList(parent), null);
        }

        save(vertices, edges);
    }

    private void save(List<GVVertex> vertices, List<GVEdge> edges) {
        graphView.modifyElements(vertices, edges);
    }
}
