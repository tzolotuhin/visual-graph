package vg.plugin.glayout.circle;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.tuple.Pair;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVVertex;
import vg.service.gview.GraphViewTab;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class EdgesHandler {
    static final int STRAIGHT_EDGE = 0;
    static final int ARC_EDGE = 1;

    final double eps = 1;
    final int nodes = 10;

    private final CircleLayoutSettings settings;

    class DoublePoint {
        double x;
        double y;

        DoublePoint(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    private GraphViewTab graphView;
    private GVVertex parent;

    private List<GVEdge> edges;
    private List<GVVertex> vertices;

    private Map<UUID, Point> graphMap;
    private Map<Pair<UUID, UUID>, Boolean> duplexMap;

    EdgesHandler(Map<UUID, Point> map, CircleLayoutSettings settings) {
        this.settings = new CircleLayoutSettings(settings);
        this.graphMap = map;
    }

    void handle(GraphViewTab gv, GVVertex parent) {
        this.graphView = gv;
        this.parent = parent;

        var parentWithElements = gv.getElements(parent);
        this.vertices = parentWithElements.getVertices();
        this.edges = parentWithElements.getEdges();

        duplexMap = getDuplexEdges();
        handleEdges();
        save();
    }

    class SortByX implements Comparator<Point> {
        public int compare(Point a, Point b) {
            if (a.x < b.x) {
                return -1;
            }
            else if (a.x == b.x) {
                return 0;
            }
            else  {
                return 1;
            }
        }
    }

    private void handleEdges() {
        var edgeType = settings.getEdgeType();

        for (var edge : edges) {
            var srcCenter = graphMap.get(edge.getSrcId());
            var trgCenter = graphMap.get(edge.getTrgId());

            if(srcCenter != null && trgCenter != null) {
                switch(edgeType) {
                    case STRAIGHT_EDGE:
                        setPointsStraight(edge);
                        break;
                    case ARC_EDGE:
                        setPointsWithRounding(edge);
                        break;
                }

                edge.getShape().getLabel().setPosition(edge.getShape().calculateAveragePoint());
            }
        }
    }

    private void mapVertex(GVVertex vertex) {
        graphMap.put(vertex.getId(), getCenter(vertex));
    }

    private Point getCenter(GVVertex v) {
        Point pos = v.getShape().getPosition();
        Point size = v.getShape().getSize();

        return new Point(pos.x + size.x / 2, pos.y + size.y / 2);
    }

    private void setPointsSplain(GVEdge edge) {
        var srcCenter = graphMap.get(edge.getSrcId());
        var trgCenter = graphMap.get(edge.getTrgId());

        var ports = getPorts(edge);
        var edgePoints = new ArrayList<Point>();
        //edgePoints.add(ports[0]);

        var headPoint = graphMap.get(VerticesHandler.nullParentId);
        var splainPoints = getSplain(edge, headPoint);

        for(var point : splainPoints) {
            edgePoints.add(point);
        }

        //edgePoints.add(ports[1]);
        edge.getShape().setPoints(edgePoints);
    }

    private Point[] getSplain(GVEdge edge, Point headPoint) {
        var srcCenter = graphMap.get(edge.getSrcId());
        var trgCenter = graphMap.get(edge.getTrgId());
        var angle = getAngles(edge)[0];

        var rotSrc = rotateCoords(srcCenter, angle);
        var rotTrg = rotateCoords(trgCenter, angle);
        var rotHead = rotateCoords(headPoint, angle);

        var headPoints = new ArrayList<Point>();
        headPoints.add(rotSrc);
        headPoints.add(rotHead);
        headPoints.add(rotTrg);
        headPoints.sort(new SortByX());

        var x = new ArrayList<Float>();
        var y = new ArrayList<Float>();

        for(var point : headPoints) {
            x.add((float)point.x);
            y.add((float)point.y);
        }

        var splineInterpolator = SplineInterpolator.createMonotoneCubicSpline(x, y);
        var step = (rotTrg.x - rotSrc.x) / nodes;
        var points = new Point[nodes];
        int curx = srcCenter.x;

        for(var i = 0; i < nodes; i++) {
            var rotPoint = new Point(curx, Math.round(splineInterpolator.interpolate(curx)));
            System.out.println("H: " + rotPoint.x + " " + rotPoint.y);

            points[i] = rotateCoords(rotPoint, -angle);
            System.out.println(points[i].x + " " + rotPoint.y);

            curx += step;
        }

        return points;
    }

    private Map<Pair<UUID, UUID>, Boolean> getDuplexEdges() {
        var duplexMap = new HashMap<Pair<UUID, UUID>, Boolean>();
        Pair<UUID, UUID> straightPair, reversePair;

        for (var edge: edges) {
            straightPair = Pair.of(edge.getSrcId(), edge.getTrgId());
            reversePair = Pair.of(edge.getTrgId(), edge.getSrcId());

            if(duplexMap.containsKey(reversePair)) {
                duplexMap.replace(reversePair, true);
                duplexMap.put(straightPair, true);
            } else {
                duplexMap.put(straightPair, false);
            }
        }

        return duplexMap;
    }

    private void setPointsStraight(GVEdge edge) {
        List<Point> points = Lists.newArrayList();
        var ports = getPorts(edge);

        points.add(ports[2]);
        points.add(ports[3]);

        edge.getShape().setPoints(points);
        handlePorts(edge, ports[0], ports[1]);
    }

    private void setPointsWithRounding(GVEdge edge) {
        List<Point> points = Lists.newArrayList();

        var ports = getPorts(edge);
        points.add(ports[2]);

        var target = graphView.getVertex(edge.getTrgId());
        var trgRadius = target.getShape().getSize().x;
        var trgCenter = target.getShape().getPosition();
        var doubleTrgCenter = new DoublePoint(trgCenter.x, trgCenter.y);

        var doublePort0 = new DoublePoint(ports[0].x, ports[0].y);
        var doublePort1 = new DoublePoint(ports[1].x, ports[1].y);
        var edgeVector = getVector(doublePort0, doublePort1);
        double x = ports[0].x;
        double y = ports[0].y;

        GVVertex roundingVertex = null;
        double radius, dist;
        Point center;
        DoublePoint doubleCenter, vec;

        while(getDist(new DoublePoint(x, y), doublePort1) > eps) {
            if(roundingVertex == null) {
                for (var v : vertices) {
                    if(graphMap.get(v.getId()) != null) {
                        if(v.getId() != edge.getSrcId() && v.getId() != edge.getTrgId()) {
                            radius = getRoundingRadius(v);
                            center = graphMap.get(v.getId());
                            doubleCenter = new DoublePoint(center.x, center.y);
                            dist = getDist(new DoublePoint(x, y), doubleCenter);

                            if(dist < radius) {
                                roundingVertex = v;
                                break;
                            }
                        }
                    }
                }
            }

            if(roundingVertex != null) {
                radius = getRoundingRadius(roundingVertex);
                center = graphMap.get(roundingVertex.getId());
                doubleCenter = new DoublePoint(center.x, center.y);
                dist = getDist(new DoublePoint(x, y), doubleCenter);

                if(dist > radius) {
                    roundingVertex = null;
                } else {
                    vec = getVector(doubleCenter, new DoublePoint(x, y));
                    vec.x = (vec.x * radius) + doubleCenter.x;
                    vec.y = (vec.y * radius) + doubleCenter.y;

                    if(getDist(vec, doubleTrgCenter) > trgRadius) {
                        points.add(round(vec));
                    }
                }
            }

            x += eps * edgeVector.x;
            y += eps * edgeVector.y;
        }

        points.add(ports[3]);
        edge.getShape().setPoints(points);

        handlePorts(edge, ports[0], ports[1]);
    }

    private double getRoundingRadius(GVVertex vertex) {
        double radius;

        if(vertex.isFragment()) {
            radius = Math.sqrt(Math.pow(vertex.getShape().getSize().x, 2) +
                    Math.pow(vertex.getShape().getSize().y, 2)) / 2.0 + settings.getFragmentRoundingRadius();
        } else {
            radius = vertex.getShape().getSize().x / 2.0 + settings.getVertexRoundingRadius();
        }

        return radius;
    }

    private void handlePorts(GVEdge edge, Point srcPortPos, Point trgPortPos) {
        var srcVertex = graphView.getVertex(edge.getSrcId());
        var trgVertex = graphView.getVertex(edge.getTrgId());

        var srcPortId = edge.getSrcPortId();
        var trgPortId = edge.getTrgPortId();

        var changelistVertices = new ArrayList<GVVertex>();
        var changelistEdges = new ArrayList<GVEdge>();

        if((srcVertex.isFragment() || srcVertex.isVertexWithPorts()) && srcPortId != null) {
            var transformSrcPortPos = transformCoords(srcPortPos, srcVertex);
            var srcPort = graphView.getVertex(srcPortId);
            setCenterPosition(srcPort, transformSrcPortPos);
            changelistVertices.add(srcPort);
            mapVertex(srcPort);
        }

        if((trgVertex.isFragment() || trgVertex.isVertexWithPorts()) && trgPortId != null) {
            var transformTrgPortPos = transformCoords(trgPortPos, trgVertex);
            var trgPort = graphView.getVertex(trgPortId);
            setCenterPosition(trgPort, transformTrgPortPos);
            changelistVertices.add(trgPort);
            mapVertex(trgPort);
        }

        changelistEdges.add(edge);
        save(changelistVertices, changelistEdges);
    }

    private void setCenterPosition(GVVertex vertex, Point point) {
        int x = point.x - (int)Math.round(vertex.getShape().getSize().x / 2.0);
        int y = point.y - (int)Math.round(vertex.getShape().getSize().y / 2.0);
        vertex.getShape().setPosition(new Point(x, y));
    }

    private Point rotateCoords(Point point, double angle) {
        var angleRad = Math.toRadians(angle);
        var rotX = (int)Math.round(point.x * Math.cos(angleRad) + point.y * Math.sin(angleRad));
        var rotY = (int)Math.round(point.y * Math.cos(angleRad) - point.x * Math.sin(angleRad));
        return new Point(rotX, rotY);
    }

    private Point transformCoords(Point point, GVVertex center) {
        return new Point(point.x - center.getShape().getPosition().x, point.y - center.getShape().getPosition().y);
    }

    private DoublePoint getVector(DoublePoint srcPoint, DoublePoint trgPoint) {
        var vectorx = trgPoint.x - srcPoint.x;
        var vectory = trgPoint.y - srcPoint.y;
        var vectorSize = getDist(new DoublePoint(vectorx, vectory));

        vectorx /= vectorSize;
        vectory /= vectorSize;

        return new DoublePoint(vectorx, vectory);
    }

    private double getDist(DoublePoint p) {
        return getDist(p, new DoublePoint(0, 0));
    }

    private double getDist(DoublePoint p1, DoublePoint p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }

    private Point[] getPorts(GVEdge edge) {
        var srcVertex = graphView.getVertex(edge.getSrcId());
        var trgVertex = graphView.getVertex(edge.getTrgId());

        var angles = getAngles(edge);

        var srcAngle = angles[0];
        var trgAngle = angles[1];
        Point srcPortPos, trgPortPos, srcArrowPos, trgArrowPos;

        srcPortPos = getRadialPoint(srcVertex, null, srcAngle);
        trgPortPos = getRadialPoint(trgVertex, null, trgAngle);

        //arrow cant point at fake port, so fragments will be ok
        if(srcVertex.isVertexWithPorts() && edge.getSrcPortId() != null) {
            var srcPort = graphView.getVertex(edge.getSrcPortId());
            srcArrowPos = getRadialPoint(srcVertex, srcPort, srcAngle);
        } else {
            srcArrowPos = srcPortPos;
        }

        if(trgVertex.isVertexWithPorts() && edge.getTrgPortId() != null) {
            var trgPort = graphView.getVertex(edge.getTrgPortId());
            trgArrowPos = getRadialPoint(trgVertex, trgPort, trgAngle);
        } else {
            trgArrowPos = trgPortPos;
        }

        var points = new Point[4];
        points[0] = srcPortPos;
        points[1] = trgPortPos;
        points[2] = srcArrowPos;
        points[3] = trgArrowPos;

        return points;
    }

    private Point getRadialPoint(GVVertex circle, double angle) {
        double radius = circle.getShape().getSize().x / 2.0;
        var angleRad = Math.toRadians(angle);
        var center = graphMap.get(circle.getId());

        return new Point(center.x + (int)Math.round(radius * Math.cos(angleRad)),
                center.y + (int)Math.round(radius * Math.sin(angleRad)));
    }

    private Point getRadialPoint(GVVertex circle, GVVertex port, double angle) {
        double radius = circle.getShape().getSize().x / 2.0;

        if(port != null) {
            double portRadius = port.getShape().getSize().x / 2.0;
            radius += portRadius;
        }

        var angleRad = Math.toRadians(angle);
        var center = graphMap.get(circle.getId());

        return new Point(center.x + (int)Math.round(radius * Math.cos(angleRad)),
                center.y + (int)Math.round(radius * Math.sin(angleRad)));
    }

    private Point getRectPoint(GVVertex rect, double angle) {
        var center = graphMap.get(rect.getId());
        var point = new Point();

        if(angle > 45 && angle <= 135) {
            var angleRad = Math.toRadians(90 - angle);
            point.y = (int)Math.round(center.y + rect.getShape().getSize().y / 2.0);
            point.x = (int)Math.round(center.x - (center.y - point.y) * Math.tan(angleRad));

        } else if(angle > -135 && angle <= -45) {
            var angleRad = Math.toRadians(-90 - angle);
            point.y = (int)Math.round(center.y - rect.getShape().getSize().y / 2.0);
            point.x = (int)Math.round(center.x + (point.y - center.y) * Math.tan(angleRad));

        } else if(angle > -45 && angle <= 45) {
            var angleRad = Math.toRadians(angle);
            point.x = (int)Math.round(center.x + rect.getShape().getSize().x / 2.0);
            point.y = (int)Math.round(center.y + (point.x - center.x) * Math.tan(angleRad));
        } else {
            var angleRad = Math.toRadians(180 - angle);
            point.x = (int)Math.round(center.x - rect.getShape().getSize().x / 2.0);
            point.y = (int)Math.round(center.y + (center.x - point.x) * Math.tan(angleRad));
        }

        return point;
    }

    private double[] getAngles(GVEdge edge) {
        Point srcCenter = graphMap.get(edge.getSrcId());
        Point trgCenter = graphMap.get(edge.getTrgId());

        double angle = Math.toDegrees(Math.atan2(srcCenter.y - trgCenter.y, srcCenter.x - trgCenter.x));
        double srcAngle, trgAngle;

        if(srcCenter.y < trgCenter.y ||
                (Math.abs(srcCenter.y - trgCenter.y) < eps && srcCenter.x < trgCenter.x)) {

            if(angle >= 0 && angle < 180 - eps) {
                srcAngle = angle;
                trgAngle = angle - 180;
            } else {
                srcAngle = angle - 180;
                trgAngle = angle;
            }
        } else {
            if(angle >= 0) {
                srcAngle = angle - 180;
                trgAngle = angle;
            } else {
                srcAngle = angle;
                trgAngle = angle - 180;
            }
        }

        if(duplexMap.get(Pair.of(edge.getSrcId(), edge.getTrgId()))) {
            srcAngle += 20;
            trgAngle -= 20;
        }

        if(srcAngle < -180) {
            srcAngle += 360;
        }

        if(trgAngle < -180) {
            trgAngle += 360;
        }

        var angles = new double[2];
        angles[0] = srcAngle;
        angles[1] = trgAngle;

        return angles;
    }

    private Point round(DoublePoint point) {
        return new Point((int)Math.round(point.x), (int)Math.round(point.y));
    }

    private void save() {
        if (parent != null) {
            save(Collections.singletonList(parent), null);
        }

        save(vertices, edges);
    }

    private void save(List<GVVertex> vertices, List<GVEdge> edges) {
        graphView.modifyElements(vertices, edges);
    }
}
