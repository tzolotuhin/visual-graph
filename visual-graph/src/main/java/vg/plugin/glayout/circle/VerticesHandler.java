package vg.plugin.glayout.circle;

import vg.lib.view.elements.GVVertex;
import vg.service.gview.GraphViewTab;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class VerticesHandler {
    static final UUID nullParentId = UUID.randomUUID();
    private final CircleLayoutSettings settings;

    private GraphViewTab graphView;
    private GVVertex parent;
    private List<GVVertex> vertices;
    private List<GVVertex> realPorts;

    private Point center;
    private Map<UUID, Point> graphMap;

    VerticesHandler(Map<UUID, Point> map, CircleLayoutSettings settings) {
        this.settings = new CircleLayoutSettings(settings);
        this.graphMap = map;
    }

    void handle(GraphViewTab gv, GVVertex parent) {
        this.graphView = gv;
        this.parent = parent;

        var parentWithElements = gv.getElements(parent);
        this.vertices = parentWithElements.getVertices();
        this.realPorts = new ArrayList<>();

        setCenter();
        excludePorts();

        handleVertices();
        if(parent != null) {
            setParentSize();
        }
        handleRealPorts();

        save();
    }

    private void mapVertex(GVVertex vertex) {
        graphMap.put(vertex.getId(), getCenter(vertex));
    }

    private void handleVertices() {
        int curx, cury;

        double step = getStep(vertices);
        double rad = getRadius();
        double ang = 0;

        for (GVVertex v : vertices) {
            curx = center.x + (int)(rad * Math.cos(ang));
            cury = center.y + (int)(rad * Math.sin(ang));
            ang += step;

            setCenterPosition(v, curx, cury);
            mapVertex(v);
        }
    }

    private void handleRealPorts() {
        int curx, cury;

        double step = getStep(realPorts);
        double rad = getParentRadius();
        double ang = 0;

        for (GVVertex v : realPorts) {
            curx = center.x + (int)(rad * Math.cos(ang));
            cury = center.y + (int)(rad * Math.sin(ang));
            ang += step;

            setCenterPosition(v, curx, cury);
            mapVertex(v);
        }
    }

    private void save() {
        if (parent != null) {
            graphView.modifyElements(Collections.singletonList(parent), null);
        }

        graphView.modifyElements(vertices, null);
        graphView.modifyElements(realPorts, null);
    }

    private void setCenterPosition(GVVertex vertex, int x, int y) {
        x -= vertex.getShape().getSize().x / 2.0;
        y -= vertex.getShape().getSize().y / 2.0;
        vertex.getShape().setPosition(new Point(x, y));
    }

    private void excludePorts() {
        GVVertex vertice;
        var iter = vertices.iterator();

        while(iter.hasNext()) {
            vertice = iter.next();

            if(vertice.isPort()) {
                if(vertice.isRealPort()) {
                    realPorts.add(vertice);
                }

                iter.remove();
            }
        }
    }

    private void setParentSize() {
        int parentRadius = center.x * 2;
        parent.getShape().setSize(new Point(parentRadius, parentRadius));
    }

    private int getParentRadius() {
        return center.x;
    }

    private void setCenter() {
        int offset = (int)Math.round(getRadius() + getMaxSide() / 2.0 + settings.getFragmentIndent());
        center = new Point(offset, offset);

        if(parent == null) {
            center.x += settings.getLayoutCenterIndent();
            center.y += settings.getLayoutCenterIndent();
            graphMap.put(nullParentId, center);
        }
    }

    private Point getCenter(GVVertex v) {
        Point pos = v.getShape().getPosition();
        Point size = v.getShape().getSize();

        return new Point(pos.x + size.x / 2, pos.y + size.y / 2);
    }

    private double getStep(List<GVVertex> vertices) {
        return 2 * Math.PI / vertices.size();
    }

    private double getRadius() {
        if(vertices.size() == 1) {
            return 0;
        }

        var max = getMaxSide();
        var diag = Math.sqrt(2) * max;

        return diag / (2 * Math.sin(getStep(vertices) / 2.0)) + settings.getMinimalLayoutRadius();
    }

    private Point getMax() {
        int maxw = 0;
        int maxh = 0;
        int w, h;

        for (GVVertex v : vertices) {
            w = v.getShape().getSize().x;
            h = v.getShape().getSize().y;

            if(w > maxw) {
                maxw = w;
            }

            if(h > maxh) {
                maxh = h;
            }
        }

        return new Point(maxw, maxh);
    }

    private int getMaxSide(Point size) {
        if(size.x > size.y) {
            return size.x;
        } else {
            return size.y;
        }
    }

    private int getMaxSide() {
        return getMaxSide(getMax());
    }
}