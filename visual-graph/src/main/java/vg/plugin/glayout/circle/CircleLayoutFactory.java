package vg.plugin.glayout.circle;

import com.google.common.collect.Maps;
import vg.lib.layout.hierarchical.HierarchicalLayoutSettings;
import vg.lib.view.elements.GVParentWithElements;
import vg.lib.view.elements.GVVertex;
import vg.lib.view.shapes.vshape.GraphViewCircleShape;
import vg.lib.view.shapes.vshape.GraphViewLeftFragmentShape;
import vg.lib.view.shapes.vshape.GraphViewRectangleShape;
import vg.lib.view.shapes.vshape.GraphViewVertexShape;
import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.operation.OperationService;
import vg.shared.gui.data.SelectorSettingAttribute;
import vg.shared.gui.data.ValueSettingAttribute;

import java.awt.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class CircleLayoutFactory extends OperationService.OperationFactory {
    // Constants
    private static final String GRAPH_VIEW = "GRAPH_VIEW";
    private static final String MINIMAL_LAYOUT_RADIUS = "MINIMAL_LAYOUT_RADIUS";
    private static final String LAYOUT_CENTER_INDENT = "LAYOUT_CENTER_INDENT";
    private static final String FRAGMENT_INDENT = "FRAGMENT_INDENT";
    private static final String CROSSING_REDUCTION = "CROSSING_REDUCTION";
    private static final String MINIMAL_VERTICES_TO_CROSSING_REDUCTION = "MINIMAL_VERTICES_TO_CROSSING_REDUCTION";
    private static final String EDGE_TYPE = "EDGE_TYPE";
    private static final String VERTEX_ROUNDING_RADIUS = "VERTEX_ROUNDING_RADIUS";
    private static final String FRAGMENT_ROUNDING_RADIUS = "FRAGMENT_ROUNDING_RADIUS";

    private static final Map<String, Integer> crossingReductionMap;
    private static final Map<String, Integer> edgeTypeMap;

    static {
        crossingReductionMap = Maps.newLinkedHashMap();
        crossingReductionMap.put("None", GraphSorter.NONE_CROSSING_REDUCTION);
        crossingReductionMap.put("Bubble crossing reduction", GraphSorter.BUBBLE_CROSSING_REDUCTION);
        crossingReductionMap.put("Grouping crossing reduction", GraphSorter.GROUPING_CROSSING_REDUCTION);

        edgeTypeMap = Maps.newLinkedHashMap();
        edgeTypeMap.put("Straight edge", EdgesHandler.STRAIGHT_EDGE);
        edgeTypeMap.put("Arc edge", EdgesHandler.ARC_EDGE);
    }

    public CircleLayoutFactory() {
        modifyDeclaredArg(MINIMAL_LAYOUT_RADIUS, new ValueSettingAttribute("Minimal layout radius", 8));
        modifyDeclaredArg(LAYOUT_CENTER_INDENT, new ValueSettingAttribute("Layout center indent", 0));
        modifyDeclaredArg(FRAGMENT_INDENT, new ValueSettingAttribute("Fragment indent", 10));
        modifyDeclaredArg(CROSSING_REDUCTION, new SelectorSettingAttribute("Crossing Reduction", Arrays.asList("None", "Bubble crossing reduction", "Grouping crossing reduction"), "Grouping crossing reduction"));
        modifyDeclaredArg(MINIMAL_VERTICES_TO_CROSSING_REDUCTION, new ValueSettingAttribute("Minimal vertices to crossing reduction", 3));
        modifyDeclaredArg(EDGE_TYPE, new SelectorSettingAttribute("Edge type", Arrays.asList("Straight edge", "Arc edge"), "Straight edge"));
        modifyDeclaredArg(VERTEX_ROUNDING_RADIUS, new ValueSettingAttribute("Vertex rounding radius", 15));
        modifyDeclaredArg(FRAGMENT_ROUNDING_RADIUS, new ValueSettingAttribute("Fragment rounding radius", 30));
    }

    @Override
    public OperationService.BaseProcedure buildOperation(Map<String, Object> args) {
        return new CircleLayout(args);
    }

    @Override
    public String getGroup() {
        return LAYOUT_GROUP;
    }

    @Override
    public String getName() {
        return "Circle layout";
    }

    private static class CircleLayout extends OperationService.BaseProcedure {
        private GraphViewTab graphViewTabArg;

        CircleLayout(Map<String, Object> args) {
            this.args = args;
        }

        @Override
        public void execute() {
            try {
                // initialize parameters.
                graphViewTabArg = (GraphViewTab)getArgs().get(GRAPH_VIEW);
                int minLayoutRad = getArg(MINIMAL_LAYOUT_RADIUS, ValueSettingAttribute.class).getCurrValue();
                int layoutCenterIndent = getArg(LAYOUT_CENTER_INDENT, ValueSettingAttribute.class).getCurrValue();
                int fragmentIndent = getArg(FRAGMENT_INDENT, ValueSettingAttribute.class).getCurrValue();
                int crossingReduction = crossingReductionMap.get(getArg(CROSSING_REDUCTION, SelectorSettingAttribute.class).getSelectedItem());
                int minVerticesToCR = getArg(MINIMAL_VERTICES_TO_CROSSING_REDUCTION, ValueSettingAttribute.class).getCurrValue();
                int edgeType = edgeTypeMap.get(getArg(EDGE_TYPE, SelectorSettingAttribute.class).getSelectedItem());
                int vertexRoundingRad = getArg(VERTEX_ROUNDING_RADIUS, ValueSettingAttribute.class).getCurrValue();
                int fragmentRoundingRad = getArg(FRAGMENT_ROUNDING_RADIUS, ValueSettingAttribute.class).getCurrValue();

                var settings = HierarchicalLayoutSettings.builder()
                        .defaultShapeStyle(GraphViewVertexShape.CIRCLE_VERTEX_SHAPE)
                        .fragmentShapeStyle(GraphViewVertexShape.CIRCLE_VERTEX_SHAPE)
                        .arrowSize(10)
                        .build();

                graphViewTabArg.lock("Please wait. Apply the layout...");
                doPrepareShapes(graphViewTabArg, settings);
                graphViewTabArg.updateElementsSize();
                doCustomShapes(graphViewTabArg, settings);

                var circleLayoutSettings = new CircleLayoutSettings();
                circleLayoutSettings.setMinimalLayoutRadius(minLayoutRad);
                circleLayoutSettings.setLayoutCenterIndent(layoutCenterIndent);
                circleLayoutSettings.setFragmentIndent(fragmentIndent);
                circleLayoutSettings.setCrossingReductionAlgorithm(crossingReduction);
                circleLayoutSettings.setMinimalVerticesToCrossingReduction(minVerticesToCR);
                circleLayoutSettings.setEdgeType(edgeType);
                circleLayoutSettings.setVertexRoundingRadius(vertexRoundingRad);
                circleLayoutSettings.setFragmentRoundingRadius(fragmentRoundingRad);

                // Validation...
                // Validate.isTrue(ySpace >= 40, "Space Y should be more 40.");

                doLayout(graphViewTabArg, null, circleLayoutSettings);

                graphViewTabArg.refreshView();
                graphViewTabArg.unlock();

                MainService.logger.printDebug("Finish the layout method.");
            } catch (Throwable ex) {
                MainService.logger.printException(ex);
                MainService.windowMessenger.errorMessage(
                        String.format("Something went wrong...\nDetails: '%s'.", ex.getMessage()),
                        "Circle layout error",
                        null);
            } finally {
                finish();
            }
        }

        private void doPrepareShapes(GraphViewTab graphViewTab,
                                     HierarchicalLayoutSettings settings) {
            graphViewTab.dfs(new GraphViewTab.GraphViewHandler() {
                @Override
                public void onParentWithElements(GVParentWithElements parentWithElements) {
                    // setup new shape.
                    var vertices = parentWithElements.getVertices();
                    vertices.forEach(vertex -> {
                        if (vertex.isFakePort()) {
                            var shape = vertex.getShape();
                            shape.getLabel().setVisible(false);
                            vertex.setShape(new GraphViewCircleShape(shape));
                        } else if (vertex.isFragment() || vertex.isVertexWithPorts()) {
                            switch (settings.fragmentShapeStyle) {
                                case GraphViewVertexShape.LEFT_FRAGMENT_VERTEX_SHAPE:
                                    vertex.setShape(new GraphViewLeftFragmentShape(vertex.getShape()));
                                    break;
                                case GraphViewVertexShape.CIRCLE_VERTEX_SHAPE:
                                    vertex.setShape(new GraphViewCircleShape(vertex.getShape()));
                                    break;
                                default:
                                    vertex.setShape(new GraphViewRectangleShape(vertex.getShape()));
                                    break;
                            }
                        } else {
                            switch (settings.defaultShapeStyle) {
                                case GraphViewVertexShape.CIRCLE_VERTEX_SHAPE:
                                    vertex.setShape(new GraphViewCircleShape(vertex.getShape()));
                                    break;
                                default:
                                    vertex.setShape(new GraphViewRectangleShape(vertex.getShape()));
                                    break;
                            }
                        }
                    });

                    graphViewTab.modifyElements(vertices, null);
                }
            });
        }

        private void doCustomShapes(GraphViewTab graphView, HierarchicalLayoutSettings settings) {
            graphView.dfs(new GraphViewTab.GraphViewHandler() {
                @Override
                public void onParentWithElements(GVParentWithElements parentWithElements) {
                    // setup new shape.
                    var vertices = parentWithElements.getVertices();
                    vertices.forEach(vertex -> {
                        if (vertex.isFakePort()) {
                            var shape = vertex.getShape();
                            shape.setSize(new Point(20, 20));
                            vertex.setShape(new GraphViewCircleShape(shape));
                        }
                    });

                    // setup arrow size.
                    parentWithElements.getEdges().forEach(edge -> {
                        edge.getShape().setArrowSize(settings.arrowSize);
                    });

                    graphView.modifyElements(vertices, null);
                }
            });
        }

        private void layoutVertices(GraphViewTab gv, GVVertex parent, Map<UUID, Point> map, CircleLayoutSettings settings) {
            var parentWithElements = gv.getElements(parent);
            for(var v : parentWithElements.getVertices()) {
                if(v.isFragment() || v.isVertexWithPorts()) {
                    layoutVertices(gv, v, map, settings);
                }
            }
            new VerticesHandler(map, settings).handle(gv, parent);
        }

        private void layoutEdges(GraphViewTab gv, GVVertex parent, Map<UUID, Point> map, CircleLayoutSettings settings) {
            var parentWithElements = gv.getElements(parent);
            new EdgesHandler(map, settings).handle(gv, parent);

            for(var v : parentWithElements.getVertices()) {
                if(v.isFragment()) {
                    layoutEdges(gv, v, map, settings);
                }
            }
        }

        private void sortGraph(GraphViewTab gv, GVVertex parent, Map<UUID, Point> map, CircleLayoutSettings settings) {
            var parentWithElements = gv.getElements(parent);
            new GraphSorter(map, settings).sort(gv, parent);

            for(var v : parentWithElements.getVertices()) {
                if(v.isFragment()) {
                    sortGraph(gv, v, map, settings);
                }
            }
        }

        private void doLayout(GraphViewTab gv, GVVertex parent, CircleLayoutSettings settings) {
            var graphMap = new HashMap<UUID, Point>();

            layoutVertices(gv, parent, graphMap, settings);
            sortGraph(gv, parent, graphMap, settings);
            layoutEdges(gv, parent, graphMap, settings);
        }
    }
}
