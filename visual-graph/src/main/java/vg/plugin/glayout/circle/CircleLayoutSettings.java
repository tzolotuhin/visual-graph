package vg.plugin.glayout.circle;

import lombok.Getter;
import lombok.Setter;

public class CircleLayoutSettings {
    @Getter @Setter
    private int minimalLayoutRadius = 8;

    @Getter @Setter
    private int layoutCenterIndent = 0;

    @Getter @Setter
    private int fragmentIndent = 10;

    @Getter @Setter
    private int crossingReductionAlgorithm;

    @Getter @Setter
    private int minimalVerticesToCrossingReduction = 3;

    @Getter @Setter
    private int edgeType;

    @Getter @Setter
    private int vertexRoundingRadius = 15;

    @Getter @Setter
    private int fragmentRoundingRadius = 30;

    public CircleLayoutSettings() {}

    public CircleLayoutSettings(CircleLayoutSettings settings) {
        minimalLayoutRadius = settings.minimalLayoutRadius;
        layoutCenterIndent = settings.layoutCenterIndent;
        fragmentIndent = settings.fragmentIndent;
        crossingReductionAlgorithm = settings.crossingReductionAlgorithm;
        minimalVerticesToCrossingReduction = settings.minimalVerticesToCrossingReduction;
        edgeType = settings.edgeType;
        vertexRoundingRadius = settings.vertexRoundingRadius;
        fragmentRoundingRadius = settings.fragmentRoundingRadius;
    }
}
