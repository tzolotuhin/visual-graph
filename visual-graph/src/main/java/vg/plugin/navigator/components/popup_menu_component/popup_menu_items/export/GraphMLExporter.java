package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import vg.plugin.navigator.components.SmartTreeNode;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.arrays.ExportArrays;
import vg.service.main.MainService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class GraphMLExporter implements Runnable {
    private final SmartTreeNode node;
    private final File name;
    private DocumentBuilder docBuilder;
    private final Document doc;

    public GraphMLExporter(SmartTreeNode node, File name) {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        try {
            this.docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            MainService.logger.printError("GraphMLExporter constructor error:",e);
        }
        this.doc = docBuilder.newDocument();
        this.node = node;
        this.name = name;
    }

    @Override
    public void run() {
        MainService.logger.printDebug("Export to graphml started");
        ExportArrays arrays = new ExportArrays(node);
        ExportGraphMLObject exportGraphMLObject = new ExportGraphMLObject(arrays.getGraphs(), arrays.getVertices(), arrays.getEdges(), arrays.getSubgraphs());
        doc.appendChild(exportGraphMLObject.compressToElement(doc));
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(name);
            transformer.transform(source, result);
            MainService.logger.printInfo("Export to graphml successfully finished");
        } catch (TransformerException e) {
            MainService.logger.printError("GraphMLExporter run error:", e);
        }
    }
}
