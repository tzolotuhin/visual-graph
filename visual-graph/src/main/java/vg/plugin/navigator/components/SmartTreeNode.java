package vg.plugin.navigator.components;

import java.util.List;
import javax.swing.tree.DefaultMutableTreeNode;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import vg.lib.model.record.AttributeRecord;
import vg.lib.model.record.EdgeRecord;
import vg.lib.model.record.GraphModelRecord;
import vg.lib.model.record.VertexRecord;

public class SmartTreeNode extends DefaultMutableTreeNode {
    // Constants
    static final int GRAPH_TYPE = 1;
    static final int VERTEX_TYPE = 2;
    static final int VERTEX_WITH_INNER_GRAPH_TYPE = 3;
    static final int PORT_TYPE = 4;
    static final int FAKE_PORT_TYPE = 5;
    static final int EDGE_TYPE = 6;
    static final int ATTRIBUTE_TYPE = 7;

    // Main data
    @Getter
    private boolean loaded = false;
    @Getter @Setter
    private int searchNumber;
    @Getter @Setter
    private boolean satisfiesSearchCondition;
    @Getter @Setter
    private String name;

    private final Object record;
    @Getter
    private final int type;

    public SmartTreeNode(Object record, int type, String name) {
        Validate.notNull(record);

        this.type = type;
        this.record = record;
        this.name = name;
        if (name == null) {
            this.name = "unknown";
        }
        setAllowsChildren(true);

        add(new DefaultMutableTreeNode("Loading...", false));
    }

    public boolean isGraphRecord() {
        return type == GRAPH_TYPE;
    }

    public GraphModelRecord getGraphRecord() {
        return (GraphModelRecord)record;
    }

    public boolean isVertexWithInnerGraphRecord() {
        return type == VERTEX_WITH_INNER_GRAPH_TYPE;
    }

    public VertexRecord getVertexWithInnerGraphRecord() {
        return (VertexRecord)record;
    }

    public boolean isVertexRecord() {
        return type == FAKE_PORT_TYPE || type == PORT_TYPE || type == VERTEX_TYPE || type == VERTEX_WITH_INNER_GRAPH_TYPE;
    }

    public VertexRecord getVertexRecord() {
        return (VertexRecord)record;
    }

    public boolean isPortRecord() {
        return type == FAKE_PORT_TYPE || type == PORT_TYPE;
    }

    public boolean isAttributeRecord() {
        return type == ATTRIBUTE_TYPE;
    }

    public AttributeRecord getAttributeRecord() {
        return (AttributeRecord) record;
    }

    public boolean isEdgeRecord() {
        return type == EDGE_TYPE;
    }

    public EdgeRecord getEdgeRecord() {
        return (EdgeRecord) record;
    }

    public void setChildren(List<SmartTreeNode> children) {
        super.removeAllChildren();
        for (SmartTreeNode node : children) {
            add(node);
        }
        loaded = true;
    }

    @Override
    public void removeAllChildren() {
        super.removeAllChildren();
        add(new DefaultMutableTreeNode("Loading...", false));
        loaded = false;
    }

    @Override
    public boolean isLeaf() {
        return type == ATTRIBUTE_TYPE;
    }

    @Override
    public String toString() {
        String message = "";
        switch (type) {
            case GRAPH_TYPE:
                message += "graph, ";
                break;
            case VERTEX_TYPE:
                message += "vertex, ";
                break;
            case EDGE_TYPE:
                message += "edge, ";
                break;
            case ATTRIBUTE_TYPE:
                message += "attribute, ";
                break;
            default:
                message += "unknown, ";
                break;
        }
        message += "loaded:" + isLoaded() + ",";
        message += "searchNumber:" + getSearchNumber() + ",";
        message += "satisfiesSearchCondition:" + isSatisfiesSearchCondition();
        return message;
    }
}
