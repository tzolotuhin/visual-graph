package vg.plugin.navigator.components.popup_menu_component.popup_menu_items;

import vg.service.main.MainService;
import vg.plugin.navigator.components.SmartTreeNode;
import vg.shared.graph.utils.GraphUtils;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.io.File;
import java.util.Collection;

public class AttachSourceCodePopupMenuItem implements IPopupMenuItem {
    // Constants
    private final static String DEF_LAST_OPEN_GRAPH_DIR = "ATTACH_SRC_CODE_LastDir";

    @Override
    public void action(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();
        if (node != null && node.isGraphRecord()) {
            JFileChooser fileChooser = new JFileChooser(".");

            // set last directory for file chooser
            String lastDir = MainService.config.getStringProperty(DEF_LAST_OPEN_GRAPH_DIR);
            if(lastDir != null) {
                fileChooser.setCurrentDirectory(new File(lastDir));
            }

            // show it
            if (fileChooser.showOpenDialog(MainService.userInterfaceService.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
                MainService.logger.printInfo("Open file = " + fileChooser.getSelectedFile().getPath());
                MainService.config.setProperty(DEF_LAST_OPEN_GRAPH_DIR, fileChooser.getCurrentDirectory().getAbsolutePath());
                GraphUtils.attachSourceCode(node.getGraphRecord().getId(), fileChooser.getSelectedFile().getPath());
            }
        } else {
            MainService.windowMessenger.warningMessage("Selection should be a graph model.", "Attach source code.", null);
        }
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return false;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();

        return node != null && node.isGraphRecord();
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Attach source code";
    }
}
