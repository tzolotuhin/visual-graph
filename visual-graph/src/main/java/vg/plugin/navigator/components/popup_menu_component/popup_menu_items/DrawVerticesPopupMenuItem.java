package vg.plugin.navigator.components.popup_menu_component.popup_menu_items;

import com.google.common.collect.Lists;
import java.util.Collection;
import java.util.List;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.plugin.navigator.components.SmartTreeNode;
import vg.service.main.MainService;

public class DrawVerticesPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, TreePath[] selectionPaths) {
        final List<TreePath> treePaths = Lists.newArrayList();
        for (JTree tree : trees) {
            TreePath[] paths = tree.getSelectionPaths();
            if (paths != null) {
                treePaths.addAll(Lists.newArrayList(paths));
            }
        }

        MainService.executorService.execute(() -> {
            try {
                List<Vertex> vertices = Lists.newArrayList();
                for (TreePath path : treePaths) {
                    SmartTreeNode smartTreeNode = (SmartTreeNode) path.getLastPathComponent();
                    if (smartTreeNode != null && smartTreeNode.isVertexRecord()) {
                        vertices.add(MainService.graphDataBaseService.getVertex(smartTreeNode.getVertexRecord().getId()));
                    }
                }

                MainService.graphViewService.asyncOpenGraphInTab(new Graph(vertices, null),  false, null);
            } catch (Throwable ex) {
                MainService.logger.printError(ex.getMessage(), ex);
            }
        });
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, TreePath[] selectionPaths) {
        List<TreePath> treePaths = Lists.newArrayList();
        for (JTree tree : trees) {
            TreePath[] paths = tree.getSelectionPaths();
            if (paths != null) {
                treePaths.addAll(Lists.newArrayList(paths));
            }
        }

        for (TreePath path : treePaths) {
            SmartTreeNode smartTreeNode = (SmartTreeNode)path.getLastPathComponent();
            if (smartTreeNode != null && smartTreeNode.isVertexRecord()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Draw vertices";
    }
}
