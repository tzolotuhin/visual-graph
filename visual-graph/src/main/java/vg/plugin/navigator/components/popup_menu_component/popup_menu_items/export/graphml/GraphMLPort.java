package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml;

import java.util.ArrayList;
import java.util.Collection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import vg.lib.model.graph.Attribute;
import vg.lib.model.record.AttributeOwnerType;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.AttributeIDGenerator;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.StringNamesGetter;

public class GraphMLPort extends GraphMLObject {
    ArrayList<Attribute> attributeList = new ArrayList<>();
    ArrayList<GraphMLObject> childObjects = new ArrayList<>();

    public GraphMLPort(String id, String ownerid) {
        this.id = id;
        this.ownerid = ownerid;
    }

    @Override
    public Element compressToElement(Document doc,  AttributeIDGenerator attributeIDGenerator) {
        Element portElement = doc.createElement("port");
        portElement.setAttribute("name",id);
        for(Attribute a : attributeList){
            if(a.isVisible()) {
                Element attributeElement = doc.createElement("data");

                var id = attributeIDGenerator.getID(a.getName(), AttributeOwnerType.VERTEX, a.getType());

                attributeElement.setAttribute("key", String.valueOf(id));
                attributeElement.appendChild(doc.createTextNode(a.getStringValue()));
                portElement.appendChild(attributeElement);
            }
        }
        for(GraphMLObject childObject : childObjects){
            portElement.appendChild(childObject.compressToElement(doc, attributeIDGenerator));
        }
        return portElement;
    }

    public void addChildObject(GraphMLObject childObject) {
        childObjects.add(childObject);
    }

    public void addAtributes(Collection<Attribute> attributes){
        attributeList.addAll(attributes);
    }

    public ArrayList<Attribute> getAttributes(){
        return attributeList;
    }

    public void setId(String id) {
        this.id=id;
    }
}
