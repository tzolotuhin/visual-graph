package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.AttributeIDGenerator;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.StringNamesGetter;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

public class GraphMLRoot extends GraphMLObject {
    ArrayList<GraphMLObject> childObjects = new ArrayList<>();

    @Override
    public Element compressToElement(Document doc, AttributeIDGenerator attributeIDGenerator) {
        ArrayList<Element> childElements = new ArrayList<>();
        Element rootElement = doc.createElement("graphml");
        rootElement.setAttribute("xmlns", "http://graphml.graphdrawing.org/xmlns");
        rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        rootElement.setAttribute("xsi:schemaLocation", "http://graphml.graphdrawing.org/xmlns      http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd");
        for(GraphMLObject childObject : childObjects){
            childElements.add(childObject.compressToElement(doc, attributeIDGenerator));
        }
        Set<Map.Entry<String, Integer>> entrySet = attributeIDGenerator.getSet();
        for(Map.Entry<String, Integer> e : entrySet){
            String s = e.getKey();
            int l = s.length();
            Element keyElement = doc.createElement("key");
            keyElement.setAttribute("id",String.valueOf(e.getValue()));
            keyElement.setAttribute("for", StringNamesGetter.getOwnerName(Character.getNumericValue(s.charAt(l-2))));
            keyElement.setAttribute("attr.name", s.substring(0, l-2));
            keyElement.setAttribute("attr.type", StringNamesGetter.getTypeName(Character.getNumericValue(s.charAt(l-1))));
            rootElement.appendChild(keyElement);
        }
        for(Element e:childElements){
            rootElement.appendChild(e);
        }
        return rootElement;
    }

    public void addChildObject(GraphMLObject childObject) {
        childObjects.add(childObject);
    }

    @Override
    public String getId() {
        return "\n";
    }

    @Override
    public String getOwnerid() {
        return "\n\n";
    }
}
