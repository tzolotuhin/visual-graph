package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export;

import vg.plugin.navigator.components.SmartTreeNode;
import vg.service.main.MainService;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;

public class ExportWindow {
    public final static String DEF_LAST_OPEN_GRAPH_DIR = "EXPORT_TO_FILE_LastDir";
    GraphMLExporter graphExporter;


    public JFileChooser getFileChooser(){
        MainService.logger.printDebug("Export menu opened");

        JFileChooser fileChooser = new JFileChooser();
        //adding GraphML file filter
        fileChooser.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File file) {
                if (file.isDirectory()) {
                    return true;
                }
                String s = file.getName();
                return s.endsWith(".graphml");
            }

            @Override
            public String getDescription() {
                return "*.graphml";
            }
        });

        fileChooser.setAcceptAllFileFilterUsed(false);
        String lastDir = MainService.config.getStringProperty(DEF_LAST_OPEN_GRAPH_DIR);
        if (lastDir != null) {
            fileChooser.setCurrentDirectory(new File(lastDir));
        }

        return fileChooser;
    }

}
