package vg.plugin.navigator.components.popup_menu_component.popup_menu_items;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.util.Collection;

public interface IPopupMenuItem {
	void action(Collection<JTree> trees, TreePath[] selectionPaths);

    boolean isVisible(Collection<JTree> trees, TreePath[] selectionPaths);

    Icon getIcon();

    String getActionName();
}
