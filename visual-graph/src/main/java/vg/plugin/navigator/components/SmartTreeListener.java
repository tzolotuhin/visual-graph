package vg.plugin.navigator.components;

/**
 * Note: all methods of the class should be called from EDT.
 */
public class SmartTreeListener {
    public void onStartSearch() {}

    public void onFinishSearch(int allResultsAmount) {}

    public void onSearchProgress(int currentCount, int allResultsAmount) {}

    public void onSelectNodes(boolean addSelectNodes) {}
}
