package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export;

import vg.lib.model.record.AttributeOwnerType;
import vg.lib.model.record.AttributeRecordType;
import vg.service.main.MainService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class AttributeIDGenerator {
    private Integer idCounter = 0;
    private final LinkedHashMap<String,Integer> idMap = new LinkedHashMap<>();

    public int getID(String name, AttributeOwnerType ownerType, AttributeRecordType valueType){
        String string = (name + ownerType) + valueType;
        if(idMap.containsKey(string)){
            return idMap.get(string);
        }else{
            return generateID(string);
        }
    }

    public Set<Map.Entry<String, Integer>> getSet(){
        return idMap.entrySet();
    }

    private int generateID(String s){
        idMap.put(s, ++idCounter);
        return idCounter;
    }
}
