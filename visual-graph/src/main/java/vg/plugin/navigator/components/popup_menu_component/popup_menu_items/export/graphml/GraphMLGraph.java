package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml;

import java.util.ArrayList;
import java.util.Collection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import vg.lib.model.graph.Attribute;
import vg.lib.model.record.AttributeOwnerType;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.AttributeIDGenerator;

public class GraphMLGraph extends GraphMLObject {
    ArrayList<GraphMLObject> childObjects = new ArrayList<>();
    ArrayList<Attribute> attributeList = new ArrayList<>();

    public GraphMLGraph(String id) {
        this.id = id;
    }

    @Override
    public Element compressToElement(Document doc,  AttributeIDGenerator attributeIDGenerator) {
        Element graphElement = doc.createElement("graph");
        graphElement.setAttribute("edgedefault","undirected");
        graphElement.setAttribute("id",id);
        for(Attribute a : attributeList){
            if(a.isVisible()) {
                Element attributeElement = doc.createElement("data");

                var id = attributeIDGenerator.getID(a.getName(), AttributeOwnerType.GRAPH_MODEL, a.getType());

                attributeElement.setAttribute("key", String.valueOf(id));
                attributeElement.appendChild(doc.createTextNode(a.getStringValue()));
                graphElement.appendChild(attributeElement);
            }
        }
        for(GraphMLObject childObject : childObjects){
            graphElement.appendChild(childObject.compressToElement(doc, attributeIDGenerator));
        }
        return graphElement;
    }

    public void addChildObject(GraphMLObject childObject) {
        childObjects.add(childObject);
    }

    public void addAtributes(Collection<Attribute> attributes){
        attributeList.addAll(attributes);
    }

    public ArrayList<Attribute> getAttributes(){
        return attributeList;
    }

    public void setId(String id) {
        this.id=id;
    }
}
