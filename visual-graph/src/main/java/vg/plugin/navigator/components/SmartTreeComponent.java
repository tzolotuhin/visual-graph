package vg.plugin.navigator.components;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.mutable.MutableInt;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Vertex;
import vg.lib.model.record.AttributeOwnerType;
import vg.lib.model.record.GraphModelRecord;
import vg.lib.model.record.VertexRecord;
import vg.service.executor.ExecutorService;
import vg.service.main.MainService;
import vg.service.resource.ResourceService;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.gui.JTreeUtils;

public class SmartTreeComponent {
    // Main components
    private final JPanel outView, innerView;

    private final JTree tree;

    // Main data
    private String searchRequest;
    private boolean isMatchCaseSelected;
    private boolean isRegexSelected;
    private boolean searchInProgress = false;
    private Map<String, SearchInfo> searchInfo = Maps.newHashMap();
    private final int graphId;

    private boolean ctrlKeyPressed = false;

    private final CopyOnWriteArrayList<SmartTreeListener> listeners = new CopyOnWriteArrayList<>();

    // Mutex
    private final Object generalMutex = new Object();

    public SmartTreeComponent(GraphModelRecord graphModelRecord) {
        Validate.notNull(graphModelRecord);

        this.graphId = graphModelRecord.getId();

        var root = new SmartTreeNode(graphModelRecord, SmartTreeNode.GRAPH_TYPE, graphModelRecord.getName());

        innerView = new JPanel(new GridBagLayout());

        outView = new JPanel(new GridLayout(1, 1));
        outView.add(innerView);

        var model = new DefaultTreeModel(root);
        tree = new JTree();
        tree.setModel(model);
        tree.setCellRenderer(new SmartTreeRenderer());
        tree.setShowsRootHandles(true);
        tree.setOpaque(false);
        JTreeUtils.expandAll(tree, false);

        tree.addTreeWillExpandListener(new TreeWillExpandListener() {
            @Override
            public void treeWillExpand(TreeExpansionEvent event) {
                synchronized (generalMutex) {
                    SmartTreeNode smartTreeNode = (SmartTreeNode)event.getPath().getLastPathComponent();
                    doExpandAndSearch(smartTreeNode);
                }
            }

            @Override
            public void treeWillCollapse(TreeExpansionEvent event) {
                synchronized (generalMutex) {
                    SmartTreeNode smartTreeNode = (SmartTreeNode)event.getPath().getLastPathComponent();
                    smartTreeNode.removeAllChildren();
                }
            }
        });
        tree.addTreeSelectionListener(e -> {
            if (!ArrayUtils.isEmpty(tree.getSelectionPaths())) {
                doNotifyOnSelectNodes(ctrlKeyPressed);
            }
        });

        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
            // if use VK_PLUS, that it's will not work, because JDK has bug with it.
            synchronized (generalMutex) {
                ctrlKeyPressed = e.isControlDown();
            }
            return false;
        });

        rebuildView();
    }

    public JPanel getView() {
        return outView;
    }

    public void search(String searchRequest, boolean isMatchCaseSelected, boolean isRegexSelected) {
        synchronized (generalMutex) {
            if (searchInProgress) {
                throw new RuntimeException("Search in progress...");
            }

            this.searchRequest = searchRequest;
            this.isMatchCaseSelected = isMatchCaseSelected;
            this.isRegexSelected = isRegexSelected;

            searchInProgress = true;

            doSearch();
        }
    }

    // TODO: need fix the problem...
    @Deprecated
    public JTree getTree() {
        return tree;
    }

    public void addListener(SmartTreeListener listener) {
        synchronized (generalMutex) {
            listeners.add(listener);
        }
    }

    private void doNotifyOnStartSearch() {
        listeners.forEach(l -> MainService.executorService.executeInEDT(l::onStartSearch));
    }

    private void doNotifyOnFinishSearch(int allResultsAmount) {
        listeners.forEach(l -> MainService.executorService.executeInEDT(() -> l.onFinishSearch(allResultsAmount)));
    }

    private void doNotifyOnSearchProgress(int currentCount, int allCount) {
        listeners.forEach(l -> MainService.executorService.executeInEDT(() -> l.onSearchProgress(currentCount, allCount)));
    }

    private void doNotifyOnSelectNodes(boolean addSelectNodes) {
        listeners.forEach(l -> MainService.executorService.executeInEDT(() -> l.onSelectNodes(addSelectNodes)));
    }

    private void rebuildView() {
        innerView.removeAll();

        innerView.add(tree, new GridBagConstraints(0,0,  1,1,  1,1,  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0),  0,0));

        innerView.updateUI();
    }

    private int recursiveSearch(Pattern pattern, boolean isMatchCaseSelected, boolean isRegexSelected, SmartTreeNode root, Map<String, SearchInfo> searchInfo, MutableInt count) {
        int searchNumber = 0;

        List<SmartTreeNode> children = downloadChildren4Node(root);

        for (SmartTreeNode child : children) {
            searchNumber += recursiveSearch(pattern, isMatchCaseSelected, isRegexSelected, child, searchInfo, count);
        }

        boolean satisfiesSearchCondition = false;

        // searching...
        if (isRegexSelected && pattern.matcher(root.getName()).matches()) {
            satisfiesSearchCondition = true;
            searchNumber++;
        } else if (isMatchCaseSelected && StringUtils.contains(root.getName(), pattern.pattern())) {
            satisfiesSearchCondition = true;
            searchNumber++;
        } else if (!isRegexSelected && !isMatchCaseSelected && StringUtils.containsIgnoreCase(root.getName(), pattern.pattern())) {
            satisfiesSearchCondition = true;
            searchNumber++;
        }

        // counting...
        if (!root.isAttributeRecord()) {
            count.add(1);
            int currentCount = count.intValue();
            if (currentCount % 100 == 0) {
                // TODO: need investigate...
                int allCount = 10_000;
                doNotifyOnSearchProgress(count.intValue(), allCount);
            }
        }

        // setup result values.
        searchInfo.put(root.getName(), new SearchInfo(searchNumber, satisfiesSearchCondition));
        root.setSatisfiesSearchCondition(satisfiesSearchCondition);
        root.setSearchNumber(searchNumber);

        return searchNumber;
    }

    private void doExpandAndSearch(SmartTreeNode root) {
        if (root.isLoaded()) {
            return;
        }

        // download node.
        MainService.executorService.execute(new ExecutorService.SwingExecutor() {
            List<SmartTreeNode> result;

            @Override
            public void doInBackground() {
                result = downloadChildren4Node(root);
                synchronized (generalMutex) {
                    markNodeList(result, searchInfo);
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    var t1 = System.currentTimeMillis();
                    try {
                        root.removeAllChildren();
                        root.setChildren(result);
                        tree.updateUI();
                    } catch (Throwable ex) {
                        MainService.logger.printError("Unexpected error...", ex);
                    }
                    var t2 = System.currentTimeMillis();

                    MainService.logger.printDebug(String.format("Expand node '%s': %sms.", root.getName(), t2 - t1));
                }
            }
        });
    }

    private void doSearch() {
        MainService.executorService.execute(new ExecutorService.SwingExecutor() {
            private int allResultsAmount;

            @Override
            public void doInBackground() {
                SmartTreeNode cloneRoot;
                String cloneSearchRequest;
                boolean cloneIsMatchCaseSelected;
                boolean cloneIsRegexSelected;
                synchronized (generalMutex) {
                    var graphRecord = MainService.graphDataBaseService
                        .findGraphModelRecord(graphId)
                        .orElseThrow(IllegalArgumentException::new);

                    cloneRoot = new SmartTreeNode(graphRecord, SmartTreeNode.GRAPH_TYPE, graphRecord.getName());
                    cloneSearchRequest = searchRequest;
                    cloneIsMatchCaseSelected = isMatchCaseSelected;
                    cloneIsRegexSelected = isRegexSelected;
                }

                doNotifyOnStartSearch();

                if (StringUtils.isEmpty(cloneSearchRequest)) {
                    synchronized (generalMutex) {
                        searchInfo.clear();
                    }
                } else {
                    Pattern pattern = Pattern.compile(cloneSearchRequest);
                    Map<String, SearchInfo> cloneSearchInfo = Maps.newHashMap();
                    allResultsAmount = recursiveSearch(pattern, cloneIsMatchCaseSelected, cloneIsRegexSelected, cloneRoot, cloneSearchInfo, new MutableInt());
                    synchronized (generalMutex) {
                        searchInfo = cloneSearchInfo;
                    }
                }

                // mark current tree
                synchronized (generalMutex) {
                    markNodeRecursive(((SmartTreeNode) tree.getModel().getRoot()), searchInfo);
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    tree.updateUI();
                    searchInProgress = false;
                }
                doNotifyOnFinishSearch(allResultsAmount);
            }
        });
    }

    private static List<SmartTreeNode> downloadChildren4Node(SmartTreeNode smartTreeNode) {
        var t1 = System.currentTimeMillis();

        List<SmartTreeNode> children = Lists.newArrayList();
        switch (smartTreeNode.getType()) {
            case SmartTreeNode.GRAPH_TYPE: {
                var attributeRecords = MainService.graphDataBaseService.getAttributeRecordsByOwner(
                    smartTreeNode.getGraphRecord().getId(),
                    AttributeOwnerType.GRAPH_MODEL);

                for (var attributeRecord : attributeRecords) {
                    if (attributeRecord.isVisible()) {
                        var attributeRecordSmartTreeNode = new SmartTreeNode(
                                attributeRecord,
                                SmartTreeNode.ATTRIBUTE_TYPE,
                                GraphUtils.getAttributeName(attributeRecord.getName(), attributeRecord.getStringValue()));
                        children.add(attributeRecordSmartTreeNode);
                    }
                }

                smartTreeNode.setName(smartTreeNode.getGraphRecord().getName());

                addVertexRecordsToChildren(
                        MainService.graphDataBaseService.getRootRecords(smartTreeNode.getGraphRecord().getId()),
                        children);

                break;
            }
            case SmartTreeNode.EDGE_TYPE:
                var attributeRecords = MainService.graphDataBaseService.getAttributeRecordsByOwner(
                    smartTreeNode.getEdgeRecord().getId(),
                    AttributeOwnerType.EDGE);

                for (var attributeRecord : attributeRecords) {
                    if (attributeRecord.isVisible()) {
                        var attributeRecordSmartTreeNode = new SmartTreeNode(
                                attributeRecord,
                                SmartTreeNode.ATTRIBUTE_TYPE,
                                GraphUtils.getAttributeName(attributeRecord.getName(), attributeRecord.getStringValue()));
                        children.add(attributeRecordSmartTreeNode);
                    }
                }

                break;
            case SmartTreeNode.VERTEX_WITH_INNER_GRAPH_TYPE:
            case SmartTreeNode.VERTEX_TYPE: {
                var graph = MainService.graphDataBaseService.getGraph(
                    smartTreeNode.getVertexRecord().getGraphModelId(), smartTreeNode.getVertexRecord().getId());

                for (var attributeRecord : graph.getAttributes()) {
                    if (attributeRecord.isVisible()) {
                        var attributeRecordSmartTreeNode = new SmartTreeNode(
                                attributeRecord,
                                SmartTreeNode.ATTRIBUTE_TYPE,
                                GraphUtils.getAttributeName(attributeRecord.getName(), attributeRecord.getStringValue()));
                        children.add(attributeRecordSmartTreeNode);
                    }
                }

                // add vertex records to the tree.
                var vertexRecords = graph.getAllVertices().stream()
                        .map(Vertex::getLinkToVertexRecord).collect(Collectors.toList());

                var idToName = addVertexRecordsToChildren(
                        vertexRecords,
                        children);

                // add edge records to the tree.
                var edgeRecords = graph.getAllEdges().stream()
                        .map(Edge::getLinkToEdgeRecord).collect(Collectors.toList());

                for (var edgeRecord : edgeRecords) {
                    var edgeRecordAttributeRecords = MainService.graphDataBaseService.getAttributeRecordsByOwner(
                        edgeRecord.getId(),
                        AttributeOwnerType.EDGE);

                    var edgeName = GraphUtils.getEdgeName(GraphUtils.convertToAttributes(edgeRecordAttributeRecords));
                    var srcName = idToName.get(edgeRecord.getSourceId());
                    var trgName = idToName.get(edgeRecord.getTargetId());
                    var extendedEdgeName = String.format("%s: %s -> %s", edgeName, srcName, trgName);

                    var edgeRecordSmartTreeNode = new SmartTreeNode(
                            edgeRecord,
                            SmartTreeNode.EDGE_TYPE,
                            extendedEdgeName);

                    children.add(edgeRecordSmartTreeNode);
                }

                break;
            }
        }

        var t2 = System.currentTimeMillis();

        MainService.logger.printDebug(String.format("Download children for node '%s': %sms.", smartTreeNode.getName(), t2 - t1));

        return children;
    }

    private static Map<Integer, String> addVertexRecordsToChildren(List<VertexRecord> vertexRecords, List<SmartTreeNode> children) {
        var idToName = new HashMap<Integer, String>();
        for (VertexRecord vertexRecord : vertexRecords) {
            var vertexRecordAttributeRecords = MainService.graphDataBaseService.getAttributeRecordsByOwner(
                vertexRecord.getId(),
                AttributeOwnerType.VERTEX);

            var vertexRecordAttributes = GraphUtils.convertToAttributes(vertexRecordAttributeRecords);

            var vertexName = GraphUtils.getVertexName(vertexRecordAttributes);

            var type = SmartTreeNode.VERTEX_TYPE;
            if (vertexRecord.isVertexWithInnerGraph()) {
                type = SmartTreeNode.VERTEX_WITH_INNER_GRAPH_TYPE;
            } else if (GraphUtils.isPort(vertexRecordAttributes)) {
                type = SmartTreeNode.PORT_TYPE;
            }

            var vertexRecordSmartTreeNode = new SmartTreeNode(
                    vertexRecord,
                    type,
                    vertexName);

            children.add(vertexRecordSmartTreeNode);
            idToName.put(vertexRecord.getId(), vertexName);
        }

        return idToName;
    }

    private static void markNodeRecursive(SmartTreeNode root, Map<String, SearchInfo> searchInfo) {
        if (root.isLoaded()) {
            for (int index = 0; index < root.getChildCount(); index++) {
                if (root.getChildAt(index) instanceof SmartTreeNode) {
                    SmartTreeNode child = (SmartTreeNode)root.getChildAt(index);
                    markNodeRecursive(child, searchInfo);
                }
            }
        }

        // setup result values
        SearchInfo searchInfoValue = searchInfo.get(root.getName());
        if (searchInfoValue == null) {
            root.setSatisfiesSearchCondition(false);
            root.setSearchNumber(0);
        } else {
            root.setSatisfiesSearchCondition(searchInfoValue.isSatisfiesSearchCondition());
            root.setSearchNumber(searchInfoValue.getSearchNumber());
        }
    }

    private static void markNodeList(List<SmartTreeNode> nodes, Map<String, SearchInfo> searchInfo) {
        for (SmartTreeNode node : nodes) {
            SearchInfo searchInfoValue = searchInfo.get(node.getName());
            if (searchInfoValue == null) {
                node.setSatisfiesSearchCondition(false);
                node.setSearchNumber(0);
            } else {
                node.setSatisfiesSearchCondition(searchInfoValue.isSatisfiesSearchCondition());
                node.setSearchNumber(searchInfoValue.getSearchNumber());
            }
        }
    }

    private static class SmartTreeRenderer extends DefaultTreeCellRenderer {
        private static final Icon graphModelIcon;
        private static final Icon graphIcon;
        private static final Icon vertexIcon;
        private static final Icon edgeIcon;
        private static final Icon portIcon;
        private static final Icon attributeIcon;

        private static final Color selectedBackgroundColor;
        private static final Color noSelectedBackgroundColor;
        private static final Color selectedForegroundColor;
        private static final Color satisfiesSearchConditionColor;

        static {
            graphModelIcon = new ImageIcon(MainService.getWorkingDirectory() + "data/resources/textures/navigator/graph_model.png");
            graphIcon = new ImageIcon(MainService.getWorkingDirectory() + "data/resources/textures/navigator/graph.png");
            vertexIcon = new ImageIcon(MainService.getWorkingDirectory() + "data/resources/textures/navigator/vertex.png");
            edgeIcon = new ImageIcon(MainService.getWorkingDirectory() + "data/resources/textures/navigator/edge.png");
            portIcon = new ImageIcon(MainService.getWorkingDirectory() + "data/resources/textures/navigator/port.png");
            attributeIcon = new ImageIcon(MainService.getWorkingDirectory() + "data/resources/textures/navigator/attribute.png");

            noSelectedBackgroundColor = ResourceService.DEFAULT_PANEL_BACKGROUND_COLOR;
            selectedBackgroundColor = UIManager.getDefaults().getColor("List.selectionBackground");
            selectedForegroundColor = UIManager.getDefaults().getColor("List.selectionForeground");
            satisfiesSearchConditionColor = Color.decode("0xFFCC00");
        }

        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            if (!(value instanceof SmartTreeNode)) {
                TreeCellRenderer delegate = new JTree().getCellRenderer();
                return delegate.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
            }

            SmartTreeNode smartTreeNode = (SmartTreeNode)value;

            JLabel label = new JLabel();
            label.setOpaque(true);

            if (selected) {
                label.setBackground(selectedBackgroundColor);
                label.setForeground(selectedForegroundColor);
            } else {
                label.setBackground(noSelectedBackgroundColor);
            }

            label.setText(StringUtils.abbreviate(smartTreeNode.getName(), 128));
            if (smartTreeNode.isGraphRecord()) {
                label.setIcon(graphModelIcon);
            }

            if (smartTreeNode.isVertexRecord()) {
                if (smartTreeNode.isVertexWithInnerGraphRecord()) {
                    label.setIcon(graphIcon);
                } else if (smartTreeNode.isPortRecord()) {
                    label.setIcon(portIcon);
                } else {
                    label.setIcon(vertexIcon);
                }
            }

            if (smartTreeNode.isEdgeRecord()) {
                label.setIcon(edgeIcon);
            }

            if (smartTreeNode.isAttributeRecord()) {
                label.setIcon(attributeIcon);
            }

            if (smartTreeNode.getSearchNumber() > 0) {
                label.setText(label.getText() + " (" + smartTreeNode.getSearchNumber() + ")");
            }

            if (smartTreeNode.isSatisfiesSearchCondition()) {
                label.setBackground(satisfiesSearchConditionColor);
            }

            return label;
        }
    }

    private static class SearchInfo {
        private final int searchNumber;
        private final boolean satisfiesSearchCondition;

        private SearchInfo(int searchNumber, boolean satisfiesSearchCondition) {
            this.searchNumber = searchNumber;
            this.satisfiesSearchCondition = satisfiesSearchCondition;
        }

        public int getSearchNumber() {
            return searchNumber;
        }

        public boolean isSatisfiesSearchCondition() {
            return satisfiesSearchCondition;
        }
    }
}
