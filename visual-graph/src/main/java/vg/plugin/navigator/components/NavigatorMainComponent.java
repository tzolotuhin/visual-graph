package vg.plugin.navigator.components;

import com.google.common.collect.Maps;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.Map;
import java.util.UUID;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import vg.lib.model.record.GraphModelRecord;
import vg.plugin.navigator.components.popup_menu_component.PopupMenuComponent;
import vg.service.main.MainService;
import vg.service.ui.UserInterfaceService;
import vg.shared.gui.UIUtils;
import vg.shared.gui.components.SearchBarPanel;

public class NavigatorMainComponent implements UserInterfaceService.UserInterfacePanel {
    // Main components
    private final JPanel outView, innerView;

    private final UserInterfaceService.UserInterfaceInstrument instrument;

    private final SearchBarPanel searchBarPanel;

    private final PopupMenuComponent popupMenuComponent;
    private final JProgressBar progressBar;
    private final Map<SmartTreeComponent, SmartTreeComponentState> smartTreeComponents = Maps.newLinkedHashMap();

    // Main data
    private boolean searchInProgress;

    // Mutex
    private final Object generalMutex = new Object();

    public NavigatorMainComponent() {
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        popupMenuComponent = new PopupMenuComponent();
        searchBarPanel = new SearchBarPanel(false, false, true, false, true, false);

        searchBarPanel.addActionListenerForSearchBarStrField(() -> {
            synchronized (generalMutex) {
                search();
            }
        });

        progressBar = new JProgressBar();
        progressBar.setVisible(false);

        instrument = UIUtils.createInstrument(
                UUID.fromString("794870e0-3441-11e9-b56e-0800200c9a66"),
                "Navigator",
                UserInterfaceService.UserInterfaceInstrument.WEST_PLACE,
                270,
                e -> MainService.userInterfaceService.selectPanel(NavigatorMainComponent.this)
        );

        rebuildView();
    }

    @Override
    public UUID getId() {
        return UUID.fromString("951e7a10-343e-11e9-b56e-0800200c9a66");
    }

    @Override
    public UserInterfaceService.UserInterfaceInstrument getInstrument() {
        return instrument;
    }

    @Override
    public int getPlace() {
        return UserInterfaceService.UserInterfacePanel.WEST_TOP_PLACE;
    }

    public void addGraph(GraphModelRecord graphModelRecord) {
        synchronized (generalMutex) {
            var smartTreeComponent = new SmartTreeComponent(graphModelRecord);
            smartTreeComponents.put(smartTreeComponent, new SmartTreeComponentState());
            popupMenuComponent.addTree(smartTreeComponent.getTree());
            smartTreeComponent.addListener(new SmartTreeListener() {
                @Override
                public void onStartSearch() {
                    synchronized (generalMutex) {
                        smartTreeComponents.get(smartTreeComponent).setSearchInProgress(true);
                        doOnStartSearch();
                    }
                }

                @Override
                public void onFinishSearch(int allResultsAmount) {
                    synchronized (generalMutex) {
                        smartTreeComponents.get(smartTreeComponent).setSearchInProgress(false);
                        doOnFinishSearch(allResultsAmount);
                    }
                }

                @Override
                public void onSearchProgress(int currentCount, int allCount) {
                    synchronized (generalMutex) {
                        var state = smartTreeComponents.get(smartTreeComponent);
                        state.setAllCount(allCount);
                        state.setCurrentCount(currentCount);
                        doOnSearchProgress();
                    }
                }

                @Override
                public void onSelectNodes(boolean addSelectNodes) {
                    synchronized (generalMutex) {
                        doOnSelectNodes(smartTreeComponent, addSelectNodes);
                    }
                }
            });
            rebuildView();
        }
    }

    @Override
    public JPanel getView() {
        return outView;
    }

    private void doOnStartSearch() {
        progressBar.setVisible(true);
        innerView.updateUI();
    }

    private void doOnFinishSearch(int allResultsAmount) {
        boolean check = false;
        for (var state : smartTreeComponents.values()) {
            if (state.isSearchInProgress()) {
                check = true;
                break;
            }
        }

        if (!check) {
            searchBarPanel.setCurrIndex(-1);
            searchBarPanel.setAmount(allResultsAmount);

            searchInProgress = false;
            progressBar.setVisible(false);

            searchBarPanel.updateUI();
            innerView.updateUI();
        }
    }

    private void doOnSearchProgress() {
        int currentCount = 0, allCount = 0;
        for (var state : smartTreeComponents.values()) {
            if (state.isSearchInProgress()) {
                currentCount += state.getCurrentCount();
                allCount += state.getAllCount();
            }
        }
        progressBar.setValue(currentCount);
        progressBar.setMinimum(0);
        progressBar.setMaximum(allCount);

        searchBarPanel.setCurrIndex(-1);
        searchBarPanel.setAmount(allCount);

        searchBarPanel.updateUI();
        innerView.updateUI();
    }

    public void doOnSelectNodes(SmartTreeComponent currentSmartTreeComponent, boolean addSelectNodes) {
        for (var smartTreeComponent : smartTreeComponents.keySet()) {
            if (!addSelectNodes && smartTreeComponent != currentSmartTreeComponent) {
                smartTreeComponent.getTree().setSelectionPaths(null);
            }
        }
        innerView.updateUI();
    }

    private void rebuildView() {
        innerView.removeAll();

        var treePanel = new JPanel(new GridBagLayout());

        int verticalIndex = 0;
        for (var smartTreeComponent : smartTreeComponents.keySet()) {
            treePanel.add(smartTreeComponent.getView(), new GridBagConstraints(0, verticalIndex++, 1, 1, 1, 0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        treePanel.add(new JPanel(), new GridBagConstraints(0, verticalIndex, 1, 1, 1, 1, GridBagConstraints.NORTH, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

        var scrollPane = new JScrollPane(treePanel);
        scrollPane.getVerticalScrollBar().setUnitIncrement(18);
        scrollPane.getHorizontalScrollBar().setUnitIncrement(18);

        innerView.add(searchBarPanel.getView(),
                new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

        innerView.add(scrollPane,
                new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));

        innerView.add(progressBar,
                new GridBagConstraints(0, 2, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 5, 5, 5), 0, 0));

        searchBarPanel.updateUI();
        innerView.updateUI();
    }

    private void search() {
        if (searchInProgress) {
            return;
        }

        searchInProgress = true;
        for (var smartTreeComponent : smartTreeComponents.keySet()) {
            smartTreeComponent.search(searchBarPanel.getStr(), searchBarPanel.isMatchCaseSelected(), searchBarPanel.isRegexSelected());
        }
    }

    private static class SmartTreeComponentState {
        private boolean searchInProgress;
        private int currentCount;
        private int allCount;

        boolean isSearchInProgress() {
            return searchInProgress;
        }

        void setSearchInProgress(boolean searchInProgress) {
            this.searchInProgress = searchInProgress;
        }

        int getCurrentCount() {
            return currentCount;
        }

        void setCurrentCount(int currentCount) {
            this.currentCount = currentCount;
        }

        int getAllCount() {
            return allCount;
        }

        void setAllCount(int allCount) {
            this.allCount = allCount;
        }
    }
}