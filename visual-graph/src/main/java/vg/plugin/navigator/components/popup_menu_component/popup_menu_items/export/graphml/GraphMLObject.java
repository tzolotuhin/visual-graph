package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.AttributeIDGenerator;

public abstract class GraphMLObject {
    protected String id;
    protected String ownerid;

    Element compressToElement(Document doc, AttributeIDGenerator generator){
        return null;
    }

    public String getId(){
        return id;
    }

    public String getOwnerid(){
        return ownerid;
    }
}
