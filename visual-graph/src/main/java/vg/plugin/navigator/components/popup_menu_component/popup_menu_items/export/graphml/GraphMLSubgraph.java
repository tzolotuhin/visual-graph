package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml;

import java.util.ArrayList;
import java.util.Collection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import vg.lib.model.graph.Attribute;
import vg.lib.model.record.AttributeOwnerType;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.AttributeIDGenerator;

public class GraphMLSubgraph extends GraphMLObject {
    ArrayList<Attribute> attributeList = new ArrayList<>();
    ArrayList<GraphMLObject> childObjects = new ArrayList<>();

    public GraphMLSubgraph(String id, String ownerid) {
        this.id = id;
        this.ownerid = ownerid;
    }

    @Override
    public Element compressToElement(Document doc,  AttributeIDGenerator attributeIDGenerator) {
        for (Attribute a : attributeList) {
            if (a.getName().equals("inner_graph_name")) {
                return compressAsSubgraph(doc, attributeIDGenerator);
            }
        }
        return compressAsVertex(doc, attributeIDGenerator);
    }

    private Element compressAsSubgraph(Document doc,  AttributeIDGenerator attributeIDGenerator){
        Element subgraphNodeElement = doc.createElement("node");
        subgraphNodeElement.setAttribute("id", id);
        Element graphElement = doc.createElement("graph");
        graphElement.setAttribute("edgedefault", "directed");
        graphElement.setAttribute("id", id);
        for (Attribute a : attributeList) {
            if (a.isVisible()) {
                Element attributeElement = doc.createElement("data");

                var id = attributeIDGenerator.getID(a.getName(), AttributeOwnerType.GRAPH_MODEL, a.getType());

                attributeElement.setAttribute("key", String.valueOf(id));
                attributeElement.appendChild(doc.createTextNode(a.getStringValue()));
                graphElement.appendChild(attributeElement);
            } else if (a.getName().equals("inner_graph_name")) {
                graphElement.setAttribute("id", a.getStringValue());
            }
        }
        for (GraphMLObject childObject : childObjects) {
            if (childObject.getClass().equals(GraphMLPort.class)) {
                subgraphNodeElement.appendChild(childObject.compressToElement(doc, attributeIDGenerator));
            } else {
                graphElement.appendChild(childObject.compressToElement(doc, attributeIDGenerator));
            }
        }
        subgraphNodeElement.appendChild(graphElement);
        return subgraphNodeElement;
    }

    private Element compressAsVertex(Document doc, AttributeIDGenerator attributeIDGenerator){
        Element vertexElement;
        vertexElement = doc.createElement("node");
        vertexElement.setAttribute("id",id);
        for(Attribute a : attributeList){
            if(a.isVisible()) {
                Element attributeElement = doc.createElement("data");

                var id = attributeIDGenerator.getID(a.getName(), AttributeOwnerType.VERTEX, a.getType());

                attributeElement.setAttribute("key", String.valueOf(id));
                attributeElement.appendChild(doc.createTextNode(a.getStringValue()));
                vertexElement.appendChild(attributeElement);
            }
        }
        for(GraphMLObject childObject : childObjects){
            vertexElement.appendChild(childObject.compressToElement(doc, attributeIDGenerator));
        }
        return vertexElement;
    }

    public void addChildObject(GraphMLObject childObject) {
        childObjects.add(childObject);
    }

    public void addAtributes(Collection<Attribute> attributes){
        attributeList.addAll(attributes);
    }

    public ArrayList<Attribute> getAttributes(){
        return attributeList;
    }

    public void setId(String id) {
        this.id=id;
    }
}
