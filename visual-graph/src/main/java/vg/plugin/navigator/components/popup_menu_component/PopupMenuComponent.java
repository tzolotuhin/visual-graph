package vg.plugin.navigator.components.popup_menu_component;

import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.DrawAsGraphPopupMenuItem;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.DrawAsHierarchicalGraphPopupMenuItem;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.DrawVertexAsHierarchicalGraphPopupMenuItem;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.DrawVerticesPopupMenuItem;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.ExportGraphToFilePopupMenuItem;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.GraphComparisonPopupMenuItem;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.IPopupMenuItem;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import com.google.common.collect.Lists;

public class PopupMenuComponent {
    // Components
    private final JPopupMenu popup;
    private final MouseAdapter mouseAdapter;

    // Main data
    private final List<IPopupMenuItem> menuItems;
    private final List<JTree> trees;
    private TreePath[] selectionPaths;

    public PopupMenuComponent() {
        trees = Lists.newArrayList();
        menuItems = Lists.newArrayList();

        popup = new JPopupMenu();

        // create items
        addMenuItem(new DrawAsHierarchicalGraphPopupMenuItem());
        addMenuItem(new DrawAsGraphPopupMenuItem());
        addMenuItem(new DrawVerticesPopupMenuItem());
        addMenuItem(new DrawVertexAsHierarchicalGraphPopupMenuItem());
        addMenuItem(null);
        addMenuItem(new GraphComparisonPopupMenuItem());
        addMenuItem(null);
        //addMenuItem(new AttachSourceCodePopupMenuItem());
        addMenuItem(new ExportGraphToFilePopupMenuItem());

        // add listeners
        mouseAdapter = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                mouseReleased(e);
                if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() >= 2) {
                    doSomething();
                }
            }

            public void mouseReleased(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    buildPopupMenu(e.getComponent(), e.getX(), e.getY());
                }
            }
        };
    }

    public void addMenuItem(IPopupMenuItem menuItem) {
        menuItems.add(menuItem);
    }

    public void addTree(JTree tree) {
        trees.add(tree);
        tree.addTreeSelectionListener(e -> {
            var lastSelectedPathComponent = tree.getLastSelectedPathComponent();
            // update selection paths if lastSelectedPathComponent != null otherwise do nothing...
            if (lastSelectedPathComponent != null) {
                selectionPaths = e.getPaths();
            }
        });
        tree.addMouseListener(mouseAdapter);
    }

    private void buildPopupMenu(Component invoker, int x, int y) {
        popup.removeAll();

        for(var menuItem : menuItems) {
            if (menuItem == null) {
                popup.addSeparator();
                continue;
            }

            JMenuItem jMenuItem = new JMenuItem(menuItem.getActionName());
            jMenuItem.setIcon(menuItem.getIcon());
            jMenuItem.setEnabled(menuItem.isVisible(trees, selectionPaths));
            jMenuItem.addActionListener(e -> menuItem.action(trees, selectionPaths));
            popup.add(jMenuItem);
        }

        if(popup.getComponentCount() > 0) {
            popup.show(invoker, x, y);
        }
    }

    private void doSomething() {}
}
