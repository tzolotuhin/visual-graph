package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export;

import vg.lib.model.record.AttributeOwnerType;
import vg.lib.model.record.AttributeRecordType;

public class StringNamesGetter {

    public static final int PORT_OWNER_TYPE = 4;

    public static String getOwnerName(Integer i){
        if (AttributeOwnerType.VERTEX.getId() == i) {
            return "node";
        }

        if (AttributeOwnerType.EDGE.getId() == i) {
            return "edge";
        }

        if (AttributeOwnerType.GRAPH_MODEL.getId() == i) {
            return "graph";
        }

        if (StringNamesGetter.PORT_OWNER_TYPE == i) {
            return "port";
        }

        return null;
    }

    public static String getTypeName(Integer i){
        switch (AttributeRecordType.valueOf(i)) {
            case INTEGER:
                return "int";
            case STRING:
                return "string";
            case BOOLEAN:
                return "boolean";
            case DOUBLE:
                return "double";
            default:
                return null;
        }
    }

}
