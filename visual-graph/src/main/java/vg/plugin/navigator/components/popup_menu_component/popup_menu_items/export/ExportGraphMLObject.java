package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml.GraphMLEdge;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml.GraphMLGraph;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml.GraphMLObject;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml.GraphMLPort;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml.GraphMLRoot;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml.GraphMLSubgraph;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml.GraphMLVertex;
import vg.service.main.MainService;
import vg.shared.graph.utils.GraphUtils;

public class ExportGraphMLObject {
    private final LinkedHashMap<String, String> nodeIdMap = new LinkedHashMap<>();
    private final GraphMLRoot root = new GraphMLRoot();
    private final ArrayList<GraphMLEdge> edges = new ArrayList<>();
    private final ArrayList<GraphMLGraph> graphs = new ArrayList<>();
    private final ArrayList<GraphMLVertex> vertices = new ArrayList<>();
    private final ArrayList<GraphMLPort> ports = new ArrayList<>();
    private final ArrayList<GraphMLSubgraph> subgraphs = new ArrayList<>();

    private static final int COMPOSITE_EDGE_START_TYPE = 1;
    private static final int COMPOSITE_EDGE_FINISH_TYPE = 2;

    public ExportGraphMLObject(Collection<Graph> graphCollection,
                               Collection<Vertex> vertexCollection,
                               Collection<Edge> edgeCollection,
                               Collection<Graph> subgraphCollection) {
        int i = 0;
        for(Graph g: graphCollection){
            graphs.add(new GraphMLGraph(String.valueOf(g.getLinkToGraphRecord().getId())));
            graphs.get(i++).addAtributes(g.getAttributes());
        }
        for(Vertex v: vertexCollection){
            vertices.add(new GraphMLVertex(String.valueOf(v.getLinkToVertexRecord().getId()), String.valueOf(v.getLinkToVertexRecord().getParentId())));
            vertices.get(vertices.size()-1).addAtributes(v.getAttributes());
        }
        for(var e: edgeCollection){
            edges.add(new GraphMLEdge(
                String.valueOf(e.getLinkToEdgeRecord().getId()),
                String.valueOf(e.getLinkToEdgeRecord().getGraphModelId()),
                String.valueOf(e.getTarget().getLinkToVertexRecord().getId()),
                String.valueOf(e.getSource().getLinkToVertexRecord().getId())));
            edges.get(edges.size()-1).addAtributes(e.getAttributes());
        }
        for(Graph g: subgraphCollection){
            subgraphs.add(new GraphMLSubgraph(String.valueOf(g.getLinkToGraphRecord().getId()),String.valueOf(g.getLinkToVertexRecord().getParentId())));
            subgraphs.get(subgraphs.size()-1).addAtributes(g.getAttributes());
        }
        MainService.logger.printInfo("ports processing...");
        getPorts();
        MainService.logger.printInfo("composite edges processing...");
        getCompositeEdges();
        MainService.logger.printInfo("removing fake ports...");
        removeFakePorts();
        MainService.logger.printInfo("child objects addition...");
        fillWithChildObjects();
        MainService.logger.printInfo("changing Ids...");
        changeIds();
    }

    Element compressToElement(Document doc){
        MainService.logger.printInfo("Root compression requested.");
        return root.compressToElement(doc, new AttributeIDGenerator());
    }

    private void getPorts(){
        ArrayList<GraphMLVertex> toRemove = new ArrayList<>();
        for(GraphMLVertex v:vertices){
            if(v.isPort()){
                ports.add(new GraphMLPort(v.getId(), v.getOwnerid()));
                ports.get(ports.size()-1).addAtributes(v.getAttributes());
                toRemove.add(v);
            }
        }
        vertices.removeAll(toRemove);
    }

    private void fillWithChildObjects(){
        for(GraphMLSubgraph sub: subgraphs){
            for(GraphMLPort port: ports){
                if(port.getOwnerid().equals(sub.getId())){
                    sub.addChildObject(port);
                }
            }
            for(GraphMLSubgraph subgraph: subgraphs){
                if(subgraph.getOwnerid().equals(sub.getId())){
                    sub.addChildObject(subgraph);
                }
            }
            for(GraphMLVertex vertex: vertices){
                if(vertex.getOwnerid().equals(sub.getId())){
                    sub.addChildObject(vertex);
                }
            }
            for(GraphMLEdge edge: edges){
                if(edge.getOwnerid().equals(sub.getId())){
                    sub.addChildObject(edge);
                }
            }
        }
        for(GraphMLGraph graph: graphs){
            for(GraphMLPort port: ports){
                if(port.getOwnerid().equals(graph.getId())){
                    graph.addChildObject(port);
                }
            }
            for(GraphMLSubgraph subgraph: subgraphs){
                if(subgraph.getOwnerid().equals(graph.getId())){
                    graph.addChildObject(subgraph);
                }
            }
            for(GraphMLVertex vertex: vertices){
                if(vertex.getOwnerid().equals(graph.getId())){
                    graph.addChildObject(vertex);
                }
            }
            for(GraphMLEdge edge: edges){
                if(edge.getOwnerid().equals(graph.getId())){
                    graph.addChildObject(edge);
                }
            }
        }
        for (GraphMLVertex vertex: vertices){
            for(GraphMLPort port: ports){
                if(port.getOwnerid().equals(vertex.getId())){
                    vertex.addChildObject(port);
                }
            }
        }
        for(GraphMLPort port: ports){
            for(GraphMLPort innerPort: ports){
                if(innerPort.getOwnerid().equals(port.getId())){
                    port.addChildObject(port);
                }
            }
        }
        for (GraphMLGraph graph: graphs){
            root.addChildObject(graph);
        }
    }

    private void changeIds(){
        for(GraphMLPort port:ports){
            for(var a: port.getAttributes()){
                if(a.getName().equals("node_id")) {
                    nodeIdMap.put(port.getId(), a.getStringValue());
                    port.setId(a.getStringValue());
                    break;
                }
            }
        }
        for(GraphMLVertex vertex:vertices){
            for(var a: vertex.getAttributes()){
                if(a.getName().equals("node_id")) {
                    nodeIdMap.put(vertex.getId(), a.getStringValue());
                    vertex.setId(a.getStringValue());
                    break;
                }
            }
        }
        for(GraphMLGraph graph:graphs){
            for(var a: graph.getAttributes()){
                if(a.getName().equals("inner_graph_name")) {
                    nodeIdMap.put(graph.getId(), a.getStringValue());
                    graph.setId(a.getStringValue());
                    break;
                }
            }
        }
        for(GraphMLSubgraph sub:subgraphs){
            for(var a: sub.getAttributes()){
                if(a.getName().equals("node_id")) {
                    nodeIdMap.put(sub.getId(), a.getStringValue());
                    sub.setId(a.getStringValue());
                    break;
                }
            }
        }
        for(GraphMLEdge edge:edges){
            for(var a: edge.getAttributes()){
                switch (a.getName()){
                    case "edge_id":
                        edge.setGraphMLId(a.getStringValue());
                        break;
                    case "trg_port_id":
                        if(nodeIdMap.containsKey(a.getStringValue())) {
                            a.setStringValue(nodeIdMap.get(a.getStringValue()));
                            break;
                        }
                    case "src_port_id":
                        if(nodeIdMap.containsKey(a.getStringValue())){
                            a.setStringValue(nodeIdMap.get(a.getStringValue()));
                            break;
                        }
                }
            }
            String targetId = edge.getTargetId();
            String sourceId = edge.getSourceId();
            if(nodeIdMap.containsKey(sourceId))
                edge.setSourceId(nodeIdMap.get(sourceId));
            if(nodeIdMap.containsKey(targetId))
                edge.setTargetId(nodeIdMap.get(targetId));
        }

    }

    private void getCompositeEdges(){
        ArrayList<GraphMLEdge> toRemove = new ArrayList<>();
        LinkedHashMap<Integer, ArrayList<GraphMLEdge>> compositeEdgesMap = new LinkedHashMap<>();
        for(GraphMLEdge edge: edges){
            for(var a: edge.getAttributes()){
                if(a.getName().equals("comp_edge_master_id")){
                    toRemove.add(edge);
                    ArrayList<GraphMLEdge> edgeArrayList = compositeEdgesMap.get(a.getIntegerValue());
                    if(edgeArrayList == null){
                        edgeArrayList = new ArrayList<>();
                        edgeArrayList.add(edge);
                        compositeEdgesMap.put(a.getIntegerValue(), edgeArrayList);
                    }else{
                        edgeArrayList.add(edge);
                        compositeEdgesMap.replace(a.getIntegerValue(), edgeArrayList);
                    }
                    break;
                }
            }
        }
        compositeEdgesMap.forEach((k, v) -> {
            boolean isValid1 = false;
            boolean isValid2 = false;
            for(GraphMLEdge edge:v){
                for(var a:edge.getAttributes()){
                    if(a.getName().equals("comp_edge_type")){
                        if(a.getIntegerValue().equals(COMPOSITE_EDGE_START_TYPE)){
                            isValid1 = true;
                            break;
                        }
                        if(a.getIntegerValue().equals(COMPOSITE_EDGE_FINISH_TYPE)){
                            isValid2 = true;
                            break;
                        }
                    }
                }
            }
            if(isValid1 && isValid2) {
                boolean ownerIsNotFound = true;
                String ownerID = v.get(v.size() - 1).getOwnerid();
                ownerFoundingLoop:
                while (ownerIsNotFound) {
                    for (GraphMLEdge e : v) {
                        if (e.getOwnerid().equals(getNode(ownerID).getOwnerid())) {
                            ownerID = e.getOwnerid();
                            continue ownerFoundingLoop;
                        }
                    }
                    ownerIsNotFound = false;
                }
                Edge compositeEdge = GraphUtils.getCompositeEdge(k);
                edges.add(new GraphMLEdge(String.valueOf(k), ownerID, String.valueOf(compositeEdge.getTarget().getLinkToVertexRecord().getId()), String.valueOf(compositeEdge.getSource().getLinkToVertexRecord().getId())));
            }
        });
        edges.removeAll(toRemove);
    }

    private GraphMLObject getNode(String id){
        for(GraphMLPort p : ports)
            if(p.getId().equals(id))
                return p;
        for(GraphMLVertex v: vertices)
            if(v.getId().equals(id))
                return v;
        for(GraphMLGraph g: graphs)
            if(g.getId().equals(id))
                return g;
        for(GraphMLSubgraph sg: subgraphs)
            if(sg.getId().equals(id))
                return sg;
        if(root.getId().equals(id))
            return root;
        return null;
    }

    private void removeFakePorts(){
        ArrayList<GraphMLPort> toRemove = new ArrayList<>();
        for(GraphMLPort port: ports){
            var attributes = port.getAttributes();
            for (var a: attributes){
                if(a.getName().equals("is_fake_port")){
                    if(a.getBooleanValue()){
                        toRemove.add(port);
                        break;
                    }
                }
            }
        }
        ports.removeAll(toRemove);
    }
}
