package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.arrays;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVVertex;
import vg.lib.model.graph.Attribute;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.lib.model.record.EdgeRecord;
import vg.lib.model.record.GraphModelRecord;
import vg.lib.model.record.VertexRecord;
import vg.plugin.navigator.components.SmartTreeNode;
import vg.service.main.MainService;

public class ExportArrays {
    private final ArrayList<Graph> graphs = new ArrayList<>();
    private final ArrayList<Vertex> vertices = new ArrayList<>();
    private final ArrayList<Edge> edges = new ArrayList<>();
    private final ArrayList<Graph> subgraphs = new ArrayList<>();

    public ExportArrays (SmartTreeNode node){
        if (node.isVertexWithInnerGraphRecord()) {
            MainService.logger.printDebug("Node is a single graph.");

            var record = node.getVertexWithInnerGraphRecord();

            var graph = MainService.graphDataBaseService.getGraph(record.getGraphModelId(), record.getId());
            graph.setLinkToGraphRecord(new GraphModelRecord(node.getVertexWithInnerGraphRecord().getId()));

            graphs.add(graph);
            getInnerObjects(graph);
        }
        if (node.isGraphRecord()) {
            MainService.logger.printDebug("node is a collection of graphs");
            List<VertexRecord> rootRecords = MainService.graphDataBaseService.getRootRecords(node.getGraphRecord().getId());
            for (VertexRecord vr : rootRecords) {
                var graph = MainService.graphDataBaseService.getGraph(vr.getGraphModelId(), vr.getId());
                graph.setLinkToGraphRecord(new GraphModelRecord(vr.getId()));
                getInnerObjects(graph);
            }
        }

    }

    public ExportArrays(Collection<GVVertex> gvVertices, Collection<GVEdge> gvEdges, String tabName){
        int root_id = -1;
        Graph rootGraph = new Graph();
        rootGraph.setLinkToGraphRecord(new GraphModelRecord(root_id, tabName));
        rootGraph.addAttribute("inner_graph_name", tabName);
        graphs.add(rootGraph);

        for (var vertex: gvVertices){
            var attributeMap = vertex.getAttributes();
            AtomicInteger db_id = new AtomicInteger();
            attributeMap.forEach((k,v) -> {
                if(k.getName().equals("system_db_id")) {
                    db_id.set(k.getIntegerValue());
                }
            });
            var v = MainService.graphDataBaseService.getVertex(db_id.intValue());
            vertices.add(v);
        }

        for (Vertex v :vertices){
            boolean parentVertexExists = false;
            for(Vertex parVertex : vertices){
                if(v.getLinkToVertexRecord().getParentId() == parVertex.getLinkToVertexRecord().getId()) {
                    parentVertexExists = true;
                    break;
                }
            }
            if(!parentVertexExists)
                v.getLinkToVertexRecord().setParentId(root_id);
        }

        for(GVEdge edge: gvEdges){
            Map<Attribute, MutableBoolean> attributeMap = edge.getAttributes();
            AtomicInteger db_id = new AtomicInteger();
            attributeMap.forEach((k,v) -> {
                if(k.getName().equals("system_db_id")){
                    db_id.set(k.getIntegerValue());
                }
            });
            Edge e = MainService.graphDataBaseService.getEdge(db_id.intValue());
            boolean parentVertexExists = false;
            boolean sourceExists = false;
            boolean targetExists = false;
            for(Vertex vertex : vertices){
                EdgeRecord eRecord = e.getLinkToEdgeRecord();
                VertexRecord vRecord = vertex.getLinkToVertexRecord();
                if(eRecord.getGraphModelId() == vRecord.getId()) {
                    parentVertexExists = true;
                }
                if(eRecord.getSourceId() == vRecord.getId()){
                    sourceExists = true;
                }
                if(eRecord.getTargetId() == vRecord.getId()){
                    targetExists = true;
                }
            }
            if(sourceExists && targetExists) {
                if (!parentVertexExists)
                    e.getLinkToEdgeRecord().setGraphModelId(root_id);
                edges.add(e);
            }
        }

        ArrayList<Vertex> toRemove = new ArrayList<>();
        for(Vertex v : vertices){
            var vRecord = v.getLinkToVertexRecord();
            var graph = MainService.graphDataBaseService.getGraph(vRecord.getGraphModelId(), vRecord.getId());
            if(!graph.getAllVertices().isEmpty()) {
                graph.setLinkToGraphRecord(new GraphModelRecord(v.getLinkToVertexRecord().getId()));
                graph.setLinkToVertexRecord(v.getLinkToVertexRecord());
                toRemove.add(v);
                subgraphs.add(graph);
            }
        }
        vertices.removeAll(toRemove);
    }

    private void getInnerObjects(Graph g){
        Collection<Vertex> vertexCollection = g.getAllVertices();
        Collection<Edge> edgeCollection = g.getAllEdges();
        ArrayList<Vertex> toRemove = new ArrayList<>();
        for(Vertex v : vertexCollection){
            var vRecord = v.getLinkToVertexRecord();
            var graph = MainService.graphDataBaseService.getGraph(vRecord.getGraphModelId(), vRecord.getId());
            if(!graph.getAllVertices().isEmpty()) {
                graph.setLinkToGraphRecord(new GraphModelRecord(v.getLinkToVertexRecord().getId()));
                toRemove.add(v);
                subgraphs.add(graph);
                getInnerObjects(graph);
            }
        }
        vertexCollection.removeAll(toRemove);
        vertices.addAll(vertexCollection);
        edges.addAll(edgeCollection);
    }

    public ArrayList<Graph> getGraphs() {
        return graphs;
    }

    public ArrayList<Vertex> getVertices() {
        return vertices;
    }

    public ArrayList<Edge> getEdges() {
        return edges;
    }

    public ArrayList<Graph> getSubgraphs() {
        return subgraphs;
    }
}
