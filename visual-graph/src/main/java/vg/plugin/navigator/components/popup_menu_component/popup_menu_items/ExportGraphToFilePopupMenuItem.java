package vg.plugin.navigator.components.popup_menu_component.popup_menu_items;

import vg.plugin.navigator.components.SmartTreeNode;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.ExportWindow;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.GraphMLExporter;
import vg.service.main.MainService;

import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.io.File;
import java.util.Collection;

import static vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.ExportWindow.DEF_LAST_OPEN_GRAPH_DIR;

public class ExportGraphToFilePopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();
        if (node != null && (node.isGraphRecord() || node.isVertexWithInnerGraphRecord())) {
            ExportWindow exportWindow = new ExportWindow();
            JFileChooser fileChooser = exportWindow.getFileChooser();

            // show save dialog
            if(fileChooser.showSaveDialog(MainService.userInterfaceService.getMainFrame()) == JFileChooser.APPROVE_OPTION){
                File fileToSave = fileChooser.getSelectedFile();
                File fullName = new File(fileToSave.getPath() + ".graphml");
                MainService.logger.printDebug("Export to graphml requested");
                GraphMLExporter exporter = new GraphMLExporter(node, fullName);
                MainService.executorService.execute(exporter);
                MainService.config.setProperty(DEF_LAST_OPEN_GRAPH_DIR, fileChooser.getCurrentDirectory().getAbsolutePath());
            }
        }
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return false;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();

        return node != null && (node.isGraphRecord() || node.isVertexWithInnerGraphRecord());
    }

    @Override
    public Icon getIcon() { return null; }

    @Override
    public String getActionName() { return "Export to file"; }
}
