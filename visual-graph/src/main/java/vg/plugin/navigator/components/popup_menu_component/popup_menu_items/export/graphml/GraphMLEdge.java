package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.graphml;

import java.util.ArrayList;
import java.util.Collection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import vg.lib.model.graph.Attribute;
import vg.lib.model.record.AttributeOwnerType;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.AttributeIDGenerator;

public class GraphMLEdge extends GraphMLObject {
    String graphMLID;
    String targetId;
    String sourceId;
    ArrayList<Attribute> attributeList = new ArrayList<>();

    public GraphMLEdge(String id, String ownerid, String targetId, String sourceId) {
        this.id = id;
        this.ownerid = ownerid;
        this.targetId = targetId;
        this.sourceId = sourceId;
    }

    @Override
    public Element compressToElement(Document doc,  AttributeIDGenerator attributeIDGenerator) {
        Element edgeElement = doc.createElement("edge");
        if(graphMLID!=null) {
            edgeElement.setAttribute("id", graphMLID);
        }
        edgeElement.setAttribute("source",sourceId);
        edgeElement.setAttribute("target",targetId);
        for(Attribute a : attributeList){
            switch (a.getName()) {
                case "directed_edge":
                    edgeElement.setAttribute("directed", a.getStringValue());
                    break;
                case "trg_port_id":
                    edgeElement.setAttribute("targetport", a.getStringValue());
                    break;
                case "src_port_id":
                    edgeElement.setAttribute("sourceport", a.getStringValue());
                    break;
                default:
                    if (a.isVisible()) {
                        Element attributeElement = doc.createElement("data");

                        var id = attributeIDGenerator.getID(a.getName(), AttributeOwnerType.EDGE, a.getType());

                        attributeElement.setAttribute("key", String.valueOf(id));
                        attributeElement.appendChild(doc.createTextNode(a.getStringValue()));
                        edgeElement.appendChild(attributeElement);
                    }
            }
        }
        return edgeElement;
    }

    public void addAtributes(Collection<Attribute> attributes){
        attributeList.addAll(attributes);
    }

    public ArrayList<Attribute> getAttributes(){
        return attributeList;
    }

    public void setGraphMLId(String id) {
        this.graphMLID=id;
    }

    public String getTargetId(){
        return targetId;
    }

    public String getSourceId(){
        return sourceId;
    }

    public void setSourceId(String sourceId){
        this.sourceId = sourceId;
    }

    public void setTargetId(String targetId){
        this.targetId = targetId;
    }
}
