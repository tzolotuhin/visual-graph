package vg.plugin.navigator.components.popup_menu_component.popup_menu_items;

import java.util.Collection;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import vg.lib.model.graph.Graph;
import vg.plugin.navigator.components.SmartTreeNode;
import vg.service.main.MainService;
import vg.shared.graph.utils.GraphUtils;

public class DrawVertexAsHierarchicalGraphPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();
        if (node != null && node.isVertexRecord()) {
            MainService.executorService.execute(() -> {
                try {
                    var graph = new Graph();
                    graph.insertVertex(MainService.graphDataBaseService.getVertex(node.getVertexRecord().getId()), null);
                    GraphUtils.downloadChildren(graph);
                    MainService.graphViewService.asyncOpenGraphInTab(graph, true, null);
                } catch (Throwable ex) {
                    MainService.logger.printError(ex.getMessage(), ex);
                }
            });
        } else {
            MainService.windowMessenger.warningMessage("Selection should be a vertex", "Opening of a vertex as a graph", null);
        }
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return false;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();

        return node != null && node.isVertexRecord();
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Draw vertex as hierarchical graph";
    }
}
