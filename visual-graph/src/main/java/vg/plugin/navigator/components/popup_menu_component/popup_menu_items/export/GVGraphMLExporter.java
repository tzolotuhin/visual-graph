package vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export;

import org.w3c.dom.Document;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVVertex;
import vg.plugin.navigator.components.popup_menu_component.popup_menu_items.export.arrays.ExportArrays;
import vg.service.main.MainService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.Collection;

public class GVGraphMLExporter implements Runnable{
    private final Collection<GVVertex> vertices;
    private final Collection<GVEdge> edges;
    private final File name;
    private DocumentBuilder docBuilder;
    private final Document doc;
    private final String tabName;

    public GVGraphMLExporter(Collection<GVVertex> vertices, Collection<GVEdge> edges, String tabName, File name) {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        try {
            this.docBuilder = docFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            MainService.logger.printError("GraphMLExporter constructor error:",e);
        }
        this.doc = docBuilder.newDocument();
        this.vertices = vertices;
        this.edges = edges;
        this.name = name;
        this.tabName = tabName;
    }

    @Override
    public void run() {
        MainService.logger.printDebug("Export to graphml started");
        ExportArrays arrays = new ExportArrays(vertices, edges, tabName);
        ExportGraphMLObject exportGraphMLObject = new ExportGraphMLObject(arrays.getGraphs(), arrays.getVertices(), arrays.getEdges(), arrays.getSubgraphs());
        doc.appendChild(exportGraphMLObject.compressToElement(doc));
        try {
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(name);
            transformer.transform(source, result);
            MainService.logger.printInfo("Export to graphml successfully finished");
        } catch (TransformerException e) {
            MainService.logger.printError("GraphMLExporter run error:", e);
        }
    }
}
