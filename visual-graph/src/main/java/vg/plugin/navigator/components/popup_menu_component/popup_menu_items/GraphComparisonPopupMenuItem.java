package vg.plugin.navigator.components.popup_menu_component.popup_menu_items;

import com.google.common.collect.Lists;
import vg.service.main.MainService;
import vg.plugin.navigator.components.SmartTreeNode;

import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import java.util.Collection;
import java.util.List;

public class GraphComparisonPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, TreePath[] selectionPaths) {
        List<TreePath> treePaths = Lists.newArrayList();
        for (JTree tree : trees) {
            TreePath[] paths = tree.getSelectionPaths();
            if (paths != null) {
                treePaths.addAll(Lists.newArrayList(paths));
            }
        }

        if (treePaths.size() != 2) {
            MainService.windowMessenger.errorMessage("Please, select two graphs for comparison", "Error", null);
            return;
        }

        var smartTreeNode1 = (SmartTreeNode)treePaths.get(0).getLastPathComponent();
        var smartTreeNode2 = (SmartTreeNode)treePaths.get(1).getLastPathComponent();

        if (smartTreeNode1 == null || smartTreeNode2 == null || !smartTreeNode1.isVertexWithInnerGraphRecord() || !smartTreeNode2.isVertexWithInnerGraphRecord()) {
            MainService.windowMessenger.errorMessage("Please, select two graphs for comparison", "Error", null);
            return;
        }

        MainService.executorService.execute(() -> {
            try {
                var graph1 = MainService.graphDataBaseService.getGraph(
                    smartTreeNode1.getVertexWithInnerGraphRecord().getGraphModelId(),
                    smartTreeNode1.getVertexWithInnerGraphRecord().getId());

                var graph2 = MainService.graphDataBaseService.getGraph(
                    smartTreeNode2.getVertexWithInnerGraphRecord().getGraphModelId(),
                    smartTreeNode2.getVertexWithInnerGraphRecord().getId());

                var factory = MainService.operationService.getOperationFactoryByName("Graph Comparator");

                var declaredArgs = factory.getDeclaredArgs();
                declaredArgs.put("GRAPH1", graph1);
                declaredArgs.put("GRAPH2", graph2);

                var operation = factory.buildOperation(declaredArgs);
                operation.execute();
            } catch (Throwable ex) {
                MainService.logger.printError(ex.getMessage(), ex);
            }
        });
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, TreePath[] selectionPaths) {
        List<TreePath> treePaths = Lists.newArrayList();
        for (JTree tree : trees) {
            TreePath[] paths = tree.getSelectionPaths();
            if (paths != null) {
                treePaths.addAll(Lists.newArrayList(paths));
            }
        }
        if (treePaths.size() != 2) {
            return false;
        }

        SmartTreeNode smartTreeNode1 = (SmartTreeNode)treePaths.get(0).getLastPathComponent();
        SmartTreeNode smartTreeNode2 = (SmartTreeNode)treePaths.get(1).getLastPathComponent();

        return smartTreeNode1 != null && smartTreeNode2 != null && smartTreeNode1.isVertexWithInnerGraphRecord() && smartTreeNode2.isVertexWithInnerGraphRecord();
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Compare two graphs";
    }
}

