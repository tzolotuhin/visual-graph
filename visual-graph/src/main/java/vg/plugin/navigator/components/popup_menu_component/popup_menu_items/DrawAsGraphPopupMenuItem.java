package vg.plugin.navigator.components.popup_menu_component.popup_menu_items;

import vg.service.main.MainService;
import vg.plugin.navigator.components.SmartTreeNode;

import javax.swing.*;
import javax.swing.tree.TreePath;
import java.util.Collection;

public class DrawAsGraphPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();
        if (node != null && node.isVertexWithInnerGraphRecord()) {
            var vertexWithInnerGraphRecord = node.getVertexWithInnerGraphRecord();
            MainService.executorService.execute(() -> {
                try {
                    var graph = MainService.graphDataBaseService.getGraph(
                        vertexWithInnerGraphRecord.getGraphModelId(),
                        vertexWithInnerGraphRecord.getId());

                    MainService.graphViewService.asyncOpenGraphInTab(graph, false,null);
                } catch (Throwable ex) {
                    MainService.logger.printError(ex.getMessage(), ex);
                }
            });
        } else {
            MainService.windowMessenger.warningMessage("Selection should be a graph", "Opening of a graph", null);
        }
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return false;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();

        return node != null && node.isVertexWithInnerGraphRecord();
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Draw as graph";
    }
}
