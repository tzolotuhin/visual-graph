package vg.plugin.navigator.components.popup_menu_component.popup_menu_items;

import java.util.Collection;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.TreePath;
import vg.lib.model.record.VertexRecord;
import vg.plugin.navigator.components.SmartTreeNode;
import vg.service.main.MainService;

public class DrawAsHierarchicalGraphPopupMenuItem implements IPopupMenuItem {
    @Override
    public void action(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();
        if (node != null && (node.isVertexWithInnerGraphRecord() || node.isGraphRecord())) {
            MainService.executorService.execute(() -> {
                try {
                    VertexRecord rootRecord;
                    if (node.isGraphRecord()) {
                        var rootRecords = MainService.graphDataBaseService.getRootRecords(node.getGraphRecord().getId());
                        rootRecord = rootRecords.get(0);
                    } else {
                        rootRecord = node.getVertexWithInnerGraphRecord();
                    }

                    MainService.logger.printDebug("Get owner elements.");

                    var graph = MainService.graphDataBaseService
                        .getGraph(rootRecord.getGraphModelId(), rootRecord.getId());

                    MainService.logger.printDebug("Open graph in new tab (async).");
                    MainService.graphViewService.asyncOpenGraphInTab(graph, true, null);
                } catch (Throwable ex) {
                    MainService.logger.printError(ex.getMessage(), ex);
                }
            });
        } else {
            MainService.windowMessenger.warningMessage("Selection should be a graph", "Opening of a graph", null);
        }
    }

    @Override
    public boolean isVisible(Collection<JTree> trees, TreePath[] selectionPaths) {
        if (selectionPaths == null || selectionPaths.length == 0) {
            return false;
        }

        var node = (SmartTreeNode)selectionPaths[0].getLastPathComponent();

        return node != null && (node.isVertexWithInnerGraphRecord() || node.isGraphRecord());
    }

    @Override
    public Icon getIcon() {
        return null;
    }

    @Override
    public String getActionName() {
        return "Draw as hierarchical graph";
    }
}
