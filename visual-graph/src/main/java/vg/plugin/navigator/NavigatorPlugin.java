package vg.plugin.navigator;

import vg.lib.model.record.GraphModelRecord;
import vg.plugin.navigator.components.NavigatorMainComponent;
import vg.service.db.IDataBaseService;
import vg.service.main.MainService;
import vg.service.plugin.Plugin;

/**
 * The navigator plugin.
 */
public class NavigatorPlugin implements Plugin {
    // Main data.
    private NavigatorMainComponent navigatorMainComponent;

    @Override
    public void install() {
        navigatorMainComponent = new NavigatorMainComponent();

        MainService.executorService.execute(() -> MainService.graphDataBaseService.addListener(new IDataBaseService.DataBaseListener() {
            @Override
            public void onOpenNewGraph(GraphModelRecord graphModelRecord) {
                navigatorMainComponent.addGraph(graphModelRecord);
            }
        }));

        MainService.userInterfaceService.addPanel(navigatorMainComponent);
    }
}
