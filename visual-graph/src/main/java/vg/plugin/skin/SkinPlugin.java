package vg.plugin.skin;

import com.jtattoo.plaf.acryl.AcrylLookAndFeel;
import com.jtattoo.plaf.aero.AeroLookAndFeel;
import com.jtattoo.plaf.fast.FastLookAndFeel;
import com.jtattoo.plaf.luna.LunaLookAndFeel;
import com.jtattoo.plaf.texture.TextureLookAndFeel;
import vg.service.main.MainService;
import vg.service.plugin.Plugin;
import vg.service.ui.UserInterfaceService;

import javax.swing.*;
import java.util.Properties;

public class SkinPlugin implements Plugin {
    // Constants
    private static final String CONF_STYLE = "skin_plugin_skin_name";

    private static final String STANDARD_JAVA_LOOK_AND_FEEL = "Java Skin";
    private static final String STANDARD_WINDOWS_LOOK_AND_FEEL = "Windows Skin";
    private static final String STANDARD_SOLARIS_LOOK_AND_FEEL = "Solaris Skin";

    private static final String JTATOO_AERO_LOOK_AND_FEEL = "Aero Skin";
    private static final String JTATOO_ACRY_LOOK_AND_FEEL = "Acry Skin";
    private static final String JTATOO_FAST_LOOK_AND_FEEL = "Fast Skin";
    private static final String JTATOO_LUNA_LOOK_AND_FEEL = "Luna Skin";
    private static final String JTATOO_TEXTURE_LOOK_AND_FEEL = "Texture Skin";

	@Override
	public void install() {
        // set current skin.
        doSetSkin();

        // add skins to the menu.
        JMenu LnFMenuItem = new JMenu("Skins");

        LnFMenuItem.addSeparator();

        LnFMenuItem.add(doCreateJMenuItem(STANDARD_JAVA_LOOK_AND_FEEL));
        LnFMenuItem.add(doCreateJMenuItem(STANDARD_WINDOWS_LOOK_AND_FEEL));
        LnFMenuItem.add(doCreateJMenuItem(STANDARD_SOLARIS_LOOK_AND_FEEL));

        LnFMenuItem.addSeparator();

        LnFMenuItem.add(doCreateJMenuItem(JTATOO_AERO_LOOK_AND_FEEL));
        LnFMenuItem.add(doCreateJMenuItem(JTATOO_ACRY_LOOK_AND_FEEL));
        LnFMenuItem.add(doCreateJMenuItem(JTATOO_FAST_LOOK_AND_FEEL));
        LnFMenuItem.add(doCreateJMenuItem(JTATOO_LUNA_LOOK_AND_FEEL));
        LnFMenuItem.add(doCreateJMenuItem(JTATOO_TEXTURE_LOOK_AND_FEEL));

        MainService.userInterfaceService.addMenuItem(LnFMenuItem, UserInterfaceService.WINDOW_MENU);
        MainService.userInterfaceService.refresh();
	}

	private JMenuItem doCreateJMenuItem(String name) {
        var skinMenu = new JMenuItem(name);
        skinMenu.addActionListener(e -> {
            MainService.config.setProperty(CONF_STYLE, name);
            doSetSkin();
            MainService.windowMessenger.infoMessage("Please, restart the program for correct applying new skin.");
        });

        return skinMenu;
    }

    private void doSetSkin() {
        // set current skin.
        try {
            Properties props = new Properties();
            props.put("logoString", "");

            String lnf = MainService.config.getStringProperty(CONF_STYLE);
            switch (lnf) {
                case STANDARD_WINDOWS_LOOK_AND_FEEL:
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                    break;
                case STANDARD_SOLARIS_LOOK_AND_FEEL:
                    UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
                    break;

                case JTATOO_AERO_LOOK_AND_FEEL:
                    AeroLookAndFeel.setCurrentTheme(props);
                    UIManager.setLookAndFeel("com.jtattoo.plaf.aero.AeroLookAndFeel");
                    break;
                case JTATOO_ACRY_LOOK_AND_FEEL:
                    AcrylLookAndFeel.setCurrentTheme(props);
                    UIManager.setLookAndFeel("com.jtattoo.plaf.acryl.AcrylLookAndFeel");
                    break;
                case JTATOO_FAST_LOOK_AND_FEEL:
                    FastLookAndFeel.setCurrentTheme(props);
                    UIManager.setLookAndFeel("com.jtattoo.plaf.fast.FastLookAndFeel");
                    break;
                case JTATOO_LUNA_LOOK_AND_FEEL:
                    LunaLookAndFeel.setCurrentTheme(props);
                    UIManager.setLookAndFeel("com.jtattoo.plaf.luna.LunaLookAndFeel");
                    break;
                case JTATOO_TEXTURE_LOOK_AND_FEEL:
                    TextureLookAndFeel.setCurrentTheme(props);
                    UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
                    break;

                case STANDARD_JAVA_LOOK_AND_FEEL:
                default:
                    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
            }

            if (MainService.userInterfaceService.getMainFrame() != null) {
                SwingUtilities.updateComponentTreeUI(MainService.userInterfaceService.getMainFrame());
            }
        } catch (Exception ex) {
            MainService.logger.printException(ex);
        }
    }
}
