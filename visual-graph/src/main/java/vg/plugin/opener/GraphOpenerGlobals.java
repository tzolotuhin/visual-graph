package vg.plugin.opener;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class GraphOpenerGlobals {
    public static final String GRAPHML_FORMAT = "graphml";
    public static final String GML_FORMAT = "gml";
    public static final String ZIP_FORMAT = "zip";
    public static final String DOT_FORMAT = "dot";
    public static final String GV_FORMAT = "gv";

    public static final String ZIP_GRAPHML_FORMAT = "graphml.zip";
    public static final String ZIP_GML_FORMAT = "gml.zip";
    public static final String ZIP_DOT_FORMAT = "dot.zip";
    public static final String ZIP_GV_FORMAT = "gv.zip";
}
