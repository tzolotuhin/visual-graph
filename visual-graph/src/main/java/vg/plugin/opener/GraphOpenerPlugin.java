package vg.plugin.opener;

import java.awt.event.KeyEvent;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;
import lombok.extern.slf4j.Slf4j;
import vg.service.main.MainService;
import vg.service.plugin.Plugin;
import vg.service.ui.UserInterfaceService;
import vg.shared.utils.IOUtils;

@Slf4j
public class GraphOpenerPlugin implements Plugin {
    // Constants
    private final static String DEF_LAST_OPEN_GRAPH_DIR = "GRAPH_OPENER_LastDir";

	// Main components
    private JFileChooser fileChooser;

	public void install() {
        final JMenuItem jMenuItem = new JMenuItem("Open file");
		jMenuItem.setIcon(new ImageIcon("./data/resources/textures/openFile.png"));

		jMenuItem.addActionListener(e -> openGraph());
		jMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));

        MainService.userInterfaceService.addMenuItem(jMenuItem, UserInterfaceService.FILE_MENU);

        fileChooser = new JFileChooser(".");

        for (final String ext : MainService.graphDecoderService.getAvailableExtensions()) {
        	fileChooser.addChoosableFileFilter(new FileFilter() {
				@Override
                public String getDescription() {
					return ext + " file (*." + ext +")";
				}

                @Override
                public boolean accept(File f) {
					return f != null && (f.isDirectory() || f.getName().toLowerCase().endsWith(ext));
				}
			});
        }
	}

	private void openGraph() {
        // set last directory for file chooser
        String lastDir = MainService.config.getStringProperty(DEF_LAST_OPEN_GRAPH_DIR);
        if(lastDir != null) {
            fileChooser.setCurrentDirectory(new File(lastDir));
        }

        // show it
		if (fileChooser.showOpenDialog(MainService.userInterfaceService.getMainFrame()) == JFileChooser.APPROVE_OPTION) {
            log.info("Open file = {}.", fileChooser.getSelectedFile().getPath());
            MainService.config.setProperty(DEF_LAST_OPEN_GRAPH_DIR, fileChooser.getCurrentDirectory().getAbsolutePath());
            doOpenGraph(fileChooser.getSelectedFile());
		}
	}

    private void doOpenGraph(final File file) {
        MainService.executorService.execute(() -> {
            int oldPriority = Thread.currentThread().getPriority();
            try {
                Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

                MainService.graphDecoderService.openFile(file);

                log.info("File was opened. File name: {}.", file.getAbsolutePath());
            } catch (Throwable ex) {
                MainService.logger.printException(ex);
                MainService.windowMessenger.errorMessage("Can't open file: " + file.getAbsolutePath() + IOUtils.getNewLineSeparator() + ex.getMessage(), "Error", null);
            } finally {
                Thread.currentThread().setPriority(oldPriority);
            }
        });
    }
}
