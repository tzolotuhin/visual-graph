package vg.plugin.attribute;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import vg.lib.view.elements.GVEdge;
import vg.lib.view.elements.GVVertex;
import vg.lib.view.shapes.vshape.GraphViewVertexShape;
import vg.lib.model.graph.Attribute;
import vg.plugin.attribute.data.AttributeComponent;
import vg.service.executor.ExecutorService;
import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.plugin.Plugin;
import vg.service.resource.ResourceService;
import vg.service.ui.UserInterfaceService;
import vg.shared.gui.ThreeStateCheckBox;
import vg.shared.gui.UIUtils;

/**
 * Attribute manager plugin.
 */
public class AttributeManagerPlugin implements Plugin, UserInterfaceService.UserInterfacePanel {
    private static final String ATTRIBUTE_MANAGER_PREFIX = "ATTRIBUTE_MANAGER_";

    private static final String SHOW_VERTEX_ATTRIBUTES_BY_DEFAULT_KEY = ATTRIBUTE_MANAGER_PREFIX + "SHOW_VERTEX_ATTRIBUTES_BY_DEFAULT";
    private static final String SHOW_EDGE_ATTRIBUTES_BY_DEFAULT_KEY = ATTRIBUTE_MANAGER_PREFIX + "SHOW_EDGE_ATTRIBUTES_BY_DEFAULT";
    private static final String VERTEX_FONT_SIZE_BY_DEFAULT_KEY = ATTRIBUTE_MANAGER_PREFIX + "VERTEX_FONT_SIZE_BY_DEFAULT";

    // Main components
    private JPanel outView, innerView;

    private UserInterfaceService.UserInterfaceInstrument instrument;

    private JButton applyButton;
    private JCheckBox selectAllVertexAttributesCheckBox, selectAllEdgeAttributesCheckBox;

    private JCheckBox showAttributeNamesCheckBox;

    private JTextField
            showVertexAttributesByDefaultTextField,
            showEdgeAttributesByDefaultTextField,
            defaultFontSizeTextField;

    private JButton showVertexAttributesByDefaultSaveButton, showEdgeAttributesByDefaultSaveButton, defaultFontSizeSaveButton;
    private JLabel infoLabel;

    // Main data
    private List<AttributeComponent> vertexAttributeComponents = Lists.newArrayList();
    private List<AttributeComponent> edgeAttributeComponents = Lists.newArrayList();

    private GraphViewTab currentGraphViewTab;
    private List<GVVertex> currVertices;
    private List<GVEdge> currEdges;

    private String regexpVertexAttributes;
    private String regexpEdgeAttributes;

    private boolean showAttributeNames = true;

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void install() {
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        applyButton = new JButton("Apply");

        showAttributeNamesCheckBox = new JCheckBox("Show name of attribute");
        showAttributeNamesCheckBox.addActionListener(e -> {
            synchronized (generalMutex) {
                showAttributeNames = showAttributeNamesCheckBox.isSelected();
            }
        });

        selectAllVertexAttributesCheckBox = new JCheckBox("Vertex attributes:");
        selectAllEdgeAttributesCheckBox = new JCheckBox("Edge attributes:");
        selectAllVertexAttributesCheckBox.setFont(ResourceService.SUB_TITLE_FONT);
        selectAllEdgeAttributesCheckBox.setFont(ResourceService.SUB_TITLE_FONT);

        showVertexAttributesByDefaultTextField = new JTextField();
        showEdgeAttributesByDefaultTextField = new JTextField();
        defaultFontSizeTextField = new JTextField();

        showVertexAttributesByDefaultSaveButton = new JButton("Save");
        showEdgeAttributesByDefaultSaveButton = new JButton("Save");
        defaultFontSizeSaveButton = new JButton("Save");
        defaultFontSizeSaveButton.addActionListener(e -> {
            synchronized (generalMutex) {
                var text = defaultFontSizeTextField.getText();
                if (NumberUtils.isNumber(text)) {
                    MainService.config.setProperty(VERTEX_FONT_SIZE_BY_DEFAULT_KEY, text);
                } else {
                    defaultFontSizeTextField.setText("");
                }
            }
        });

        infoLabel = new JLabel("If you want to use Attribute Manager you need open some graph");
        infoLabel.setHorizontalAlignment(JLabel.CENTER);

        showVertexAttributesByDefaultSaveButton.addActionListener(e -> {
            synchronized (generalMutex) {
                MainService.config.setProperty(SHOW_VERTEX_ATTRIBUTES_BY_DEFAULT_KEY, showVertexAttributesByDefaultTextField.getText());
                initDefaultAttributes();
            }
        });

        showEdgeAttributesByDefaultSaveButton.addActionListener(e -> {
            synchronized (generalMutex) {
                MainService.config.setProperty(SHOW_EDGE_ATTRIBUTES_BY_DEFAULT_KEY, showVertexAttributesByDefaultTextField.getText());
                initDefaultAttributes();
            }
        });

        applyButton.addActionListener(e -> MainService.executorService.execute(() -> {
            synchronized (generalMutex) {
                doApply();
            }
        }));

        selectAllVertexAttributesCheckBox.addActionListener(e -> {
            synchronized (generalMutex) {
                vertexAttributeComponents.forEach(x -> x.setShowState(true));
                rebuildView();
            }
        });

        selectAllEdgeAttributesCheckBox.addActionListener(e -> {
            synchronized (generalMutex) {
                edgeAttributeComponents.forEach(x -> x.setShowState(true));
                rebuildView();
            }
        });

        instrument = UIUtils.createInstrument(
                UUID.fromString("29904f40-3442-11e9-b56e-0800200c9a66"),
                "Attribute",
                UserInterfaceService.UserInterfaceInstrument.WEST_PLACE,
                270,
                e -> MainService.userInterfaceService.selectPanel(AttributeManagerPlugin.this)
        );

        MainService.userInterfaceService.addObserver(new UserInterfaceService.UserInterfaceListener() {
            @Override
            public void onOpenTab(UserInterfaceService.UserInterfaceTab tab) {
                if (tab instanceof GraphViewTab graphViewTab) {
                    MainService.executorService.execute(() -> graphViewTab.addListener(new GraphViewTab.GVListener() {
                        @Override
                        public void onAddElements(List<GVVertex> vertices, List<GVEdge> edges) {
                            MainService.executorService.executeTaskByKey(
                                    graphViewTab.getId().toString(),
                                    () -> doOnAddElements(vertices, edges, graphViewTab, regexpVertexAttributes, regexpEdgeAttributes));
                        }

                        @Override
                        public void onSelectElements(List<GVVertex> vertices, List<GVEdge> edges) {
                            MainService.logger.printDebug("AttributeManagerPlugin:onSelectElements action.");

                            doOnSelectElements(vertices, edges, graphViewTab);
                        }
                    }));
                }
            }

            @Override
            public void onChangeTab(UserInterfaceService.UserInterfaceTab tab) {
                MainService.logger.printDebug("AttributeManagerPlugin:onChangeTab action.");

                synchronized (generalMutex) {
                    if (tab instanceof GraphViewTab) {
                        currentGraphViewTab = (GraphViewTab) tab;
                    } else {
                        currentGraphViewTab = null;
                    }

                    MainService.executorService.execute(new ExecutorService.SwingExecutor() {
                        @Override
                        public void doInBackground() {
                            synchronized (generalMutex) {
                                if (currentGraphViewTab == null) {
                                    return;
                                }

                                showAttributeNames = currentGraphViewTab.isShowAttributeNames();
                            }
                        }

                        @Override
                        public void doInEDT() {
                            synchronized (generalMutex) {
                                rebuildView();
                            }
                        }
                    });
                }
            }
        }, 12);

        initDefaultAttributes();

        rebuildView();

        MainService.userInterfaceService.addPanel(this);
    }

    @Override
    public UUID getId() {
        return UUID.fromString("f88c8cb0-3441-11e9-b56e-0800200c9a66");
    }

    @Override
    public UserInterfaceService.UserInterfaceInstrument getInstrument() {
        return instrument;
    }

    @Override
    public int getPlace() {
        return UserInterfaceService.UserInterfacePanel.WEST_BOTTOM_PLACE;
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

    private void doOnAddElements(
            List<GVVertex> vertices,
            List<GVEdge> edges,
            GraphViewTab graphViewTab,
            String regexpVertexAttributes,
            String regexpEdgeAttributes) {
        MainService.logger.printDebug(String.format(
                "AttributeManagerPlugin:onAddElements action, vertices: %d, edges %d.",
                vertices.size(),
                edges.size()));

        // handle attributes for vertices.
        vertices.forEach(x -> x.getAttributes().forEach((attribute, mutableBoolean) -> {
            mutableBoolean.setValue(attribute.getName().matches(regexpVertexAttributes));

            if (x.isVertex()) {
                x.setShape(x.getInitialShape(GraphViewVertexShape.RECTANGLE_VERTEX_SHAPE));
            }

            x.getShape().setColor(x.getInitialColor());
        }));

        // handle attributes for edges.
        edges.forEach(x -> x.getAttributes().forEach((attribute, mutableBoolean) -> {
            mutableBoolean.setValue(attribute.getName().matches(regexpEdgeAttributes));

            x.getShape().setColor(x.getInitialColor());
        }));

        graphViewTab.modifyElements(vertices, edges);
        graphViewTab.setFont(Integer.parseInt(defaultFontSizeTextField.getText()));
        graphViewTab.refreshView();
    }

    private void initDefaultAttributes() {
        regexpVertexAttributes = MainService.config.getStringProperty(SHOW_VERTEX_ATTRIBUTES_BY_DEFAULT_KEY, "");
        regexpEdgeAttributes = MainService.config.getStringProperty(SHOW_EDGE_ATTRIBUTES_BY_DEFAULT_KEY, "");
        showVertexAttributesByDefaultTextField.setText(regexpVertexAttributes);
        showEdgeAttributesByDefaultTextField.setText(regexpEdgeAttributes);
        defaultFontSizeTextField.setText(MainService.config.getStringProperty(VERTEX_FONT_SIZE_BY_DEFAULT_KEY, "16"));
    }

    private void doOnSelectElements(List<GVVertex> vertices, List<GVEdge> edges, GraphViewTab graphViewTab) {
        MainService.executorService.execute(new ExecutorService.SwingExecutor() {
            private final List<AttributeComponent> localVertexAttributeComponents = Lists.newArrayList();
            private final List<AttributeComponent> localEdgeAttributeComponents = Lists.newArrayList();

            @Override
            public void doInBackground() {
                // build local attribute components.
                vertices.forEach(x -> {
                    MainService.logger.printDebug(String.format("Selected vertex: %s.", x.getId()));
                    doAddAttributes(x.getAttributes().keySet(), localVertexAttributeComponents);
                });

                edges.forEach(x -> {
                    MainService.logger.printDebug(String.format("Selected edge: %s.", x.getId()));
                    doAddAttributes(x.getAttributes().keySet(), localEdgeAttributeComponents);
                });

                // set states.
                vertices.forEach(x -> doMergeAttributes(x.getAttributes(), localVertexAttributeComponents));

                edges.forEach(x -> doMergeAttributes(x.getAttributes(), localEdgeAttributeComponents));
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    if (currentGraphViewTab == graphViewTab) {
                        currVertices = vertices;
                        currEdges = edges;

                        vertexAttributeComponents = localVertexAttributeComponents;
                        edgeAttributeComponents = localEdgeAttributeComponents;
                        boolean vertexSelect = true;
                        for (AttributeComponent attributeComponent : vertexAttributeComponents) {
                            vertexSelect &= attributeComponent.isShowState();
                        }

                        boolean edgeSelect = true;
                        for (AttributeComponent attributeComponent : edgeAttributeComponents) {
                            edgeSelect &= attributeComponent.isShowState();
                        }

                        selectAllVertexAttributesCheckBox.setSelected(vertexSelect);
                        selectAllEdgeAttributesCheckBox.setSelected(edgeSelect);

                        rebuildView();
                    }
                }
            }
        });
    }

    private void doAddAttributes(Collection<Attribute> attributes, List<AttributeComponent> attributeComponents) {
        for (Attribute attribute : attributes) {
            if (!attribute.isVisible())
                continue;

            boolean check = false;
            for (AttributeComponent attributeComponent : attributeComponents) {
                if (attribute.getName().equals(attributeComponent.getAttributeName())) {
                    check = true;
                    break;
                }
            }
            if (!check) {
                AttributeComponent newAttrComp = new AttributeComponent(AttributeComponent.NOT_STATE, attribute.getName());
                attributeComponents.add(newAttrComp);
            }
        }
    }

    private void doMergeAttributes(Map<Attribute, MutableBoolean> attributes, List<AttributeComponent> attributeComponents) {
        for (Attribute attribute : attributes.keySet()) {
            for (AttributeComponent localVertexAttributeComponent : attributeComponents) {
                if (localVertexAttributeComponent.getAttributeName().equals(attribute.getName())) {
                    localVertexAttributeComponent.mergeShow(attributes.get(attribute).booleanValue());
                }
            }
        }
    }

    private void doApply() {
        final Map<String, Boolean> vertexAttributes = Maps.newHashMap();
        final Map<String, Boolean> edgeAttributes = Maps.newHashMap();

        for (AttributeComponent attributeComponent : vertexAttributeComponents) {
            if (attributeComponent.getShowState() == AttributeComponent.UNKNOWN_SHOW_ATTRIBUTE_STATE || attributeComponent.getShowState() == AttributeComponent.NOT_STATE)
                continue;
            vertexAttributes.put(attributeComponent.getAttributeName(), attributeComponent.isShowState());
        }

        for (AttributeComponent attributeComponent : edgeAttributeComponents) {
            if (attributeComponent.getShowState() == AttributeComponent.UNKNOWN_SHOW_ATTRIBUTE_STATE || attributeComponent.getShowState() == AttributeComponent.NOT_STATE)
                continue;
            edgeAttributes.put(attributeComponent.getAttributeName(), attributeComponent.isShowState());
        }

        if (currentGraphViewTab != null) {
            if (currVertices != null) {
                currVertices.forEach(x -> x.getAttributes().forEach((attribute, mutableBoolean) -> {
                    Boolean value = vertexAttributes.get(attribute.getName());
                    if (value != null) {
                        mutableBoolean.setValue(value);
                    }
                }));
            }

            if (currEdges != null) {
                currEdges.forEach(x -> x.getAttributes().forEach((attribute, mutableBoolean) -> {
                    Boolean value = edgeAttributes.get(attribute.getName());
                    if (value != null) {
                        mutableBoolean.setValue(value);
                    }
                }));
            }

            currentGraphViewTab.modifyElements(currVertices, currEdges);
            currentGraphViewTab.setFont(Integer.parseInt(defaultFontSizeTextField.getText()));
            currentGraphViewTab.setShowAttributeNames(showAttributeNames);
            currentGraphViewTab.refreshView();
        }
    }

    private void rebuildView() {
        innerView.removeAll();

        if (currentGraphViewTab != null) {
            // build show attributes panel.
            JPanel showAttributesPanel = new JPanel(new GridBagLayout());

            int index = 0;

            showAttributesPanel.add(showAttributeNamesCheckBox, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
            showAttributeNamesCheckBox.setSelected(showAttributeNames);

            // build vertex attributes.
            showAttributesPanel.add(selectAllVertexAttributesCheckBox, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
            index = doAddAttributeComponentsOnShowAttributesPanel(showAttributesPanel, vertexAttributeComponents, index);

            // build edge attributes.
            showAttributesPanel.add(selectAllEdgeAttributesCheckBox, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 0, 0), 0, 0));
            index = doAddAttributeComponentsOnShowAttributesPanel(showAttributesPanel, edgeAttributeComponents, index);

            {
                showAttributesPanel.add(new JLabel("Default font size:"), new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 5, 0), 0, 0));
                JPanel panel = new JPanel(new GridBagLayout());
                panel.add(defaultFontSizeTextField, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                panel.add(defaultFontSizeSaveButton, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
                showAttributesPanel.add(panel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }

            {
                showAttributesPanel.add(new JLabel("Default vertex attributes:"), new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 5, 0), 0, 0));
                JPanel panel = new JPanel(new GridBagLayout());
                panel.add(showVertexAttributesByDefaultTextField, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                panel.add(showVertexAttributesByDefaultSaveButton, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
                showAttributesPanel.add(panel, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }

            {
                showAttributesPanel.add(new JLabel("Default edge attributes:"), new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(10, 0, 5, 0), 0, 0));
                JPanel panel = new JPanel(new GridBagLayout());
                panel.add(showEdgeAttributesByDefaultTextField, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                panel.add(showEdgeAttributesByDefaultSaveButton, new GridBagConstraints(1, 0, 1, 1, 0, 1, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
                showAttributesPanel.add(panel, new GridBagConstraints(0, index, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }

            // build attributes info panel.
            JPanel attributesInfoPanel = new JPanel(new GridBagLayout());

            index = 0;

            attributesInfoPanel.add(new JPanel(), new GridBagConstraints(0, index, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

            // build panel for scroll.
            var panelForScroll = new JPanel(new GridBagLayout());
            panelForScroll.add(showAttributesPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            panelForScroll.add(attributesInfoPanel, new GridBagConstraints(0, 1, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

            var scrollPane = new JScrollPane(panelForScroll);

            // build result panel with apply button.
            var resultPanel = new JPanel(new GridBagLayout());
            resultPanel.add(scrollPane, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.NORTHWEST, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            resultPanel.add(applyButton, new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(5, 0, 5, 0), 0, 0));

            innerView.add(resultPanel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        } else {
            innerView.add(infoLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }

        innerView.updateUI();
    }

    private int doAddAttributeComponentsOnShowAttributesPanel(JPanel showAttributesPanel, List<AttributeComponent> attributeComponents, int index) {
        for (AttributeComponent attributeComponent : attributeComponents) {
            final ThreeStateCheckBox checkBox = new ThreeStateCheckBox(attributeComponent.getAttributeName(), AttributeComponent.toThreeState(attributeComponent.getShowState()));
            showAttributesPanel.add(checkBox, new GridBagConstraints(0, index++, 1, 1, 1, 0, GridBagConstraints.NORTHWEST, GridBagConstraints.HORIZONTAL, new Insets(0, 3, 0, 0), 0, 0));
            checkBox.addChangeListener(e -> {
                synchronized (generalMutex) {
                    attributeComponent.setShowState(AttributeComponent.toAttributeState(checkBox.getState()));
                }
            });
        }
        return index;
    }
}
