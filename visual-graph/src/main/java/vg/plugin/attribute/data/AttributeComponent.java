package vg.plugin.attribute.data;

import vg.shared.gui.ThreeStateCheckBox;

public class AttributeComponent {
    // Constants
    public static final int NOT_STATE = -1;
    public static final int NOT_SHOW_ATTRIBUTE_STATE = 0;
    public static final int SHOW_ATTRIBUTE_STATE = 1;
    public static final int UNKNOWN_SHOW_ATTRIBUTE_STATE = 2;

    private int showState;
    private final String attributeName;

    public AttributeComponent(int showState, String attributeName) {
        this.showState = showState;
        this.attributeName = attributeName;
    }

    public int getShowState() {
        return showState;
    }

    public void setShowState(int showState) {
        this.showState = showState;
    }

    public void setShowState(boolean showState) {
        this.showState = showState ? SHOW_ATTRIBUTE_STATE : NOT_SHOW_ATTRIBUTE_STATE;
    }

    public boolean isShowState() {
        return showState == SHOW_ATTRIBUTE_STATE;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void mergeShow(boolean show) {
        if (showState == NOT_STATE)
            setShowState(show);
        else if (showState == SHOW_ATTRIBUTE_STATE && !show)
            showState = UNKNOWN_SHOW_ATTRIBUTE_STATE;
        else if (showState == NOT_SHOW_ATTRIBUTE_STATE && show)
            showState = UNKNOWN_SHOW_ATTRIBUTE_STATE;
    }

    public static int toAttributeState(ThreeStateCheckBox.State state) {
        if (state == ThreeStateCheckBox.DONT_CARE) return UNKNOWN_SHOW_ATTRIBUTE_STATE;
        if (state == ThreeStateCheckBox.NOT_SELECTED) return NOT_SHOW_ATTRIBUTE_STATE;
        if (state == ThreeStateCheckBox.SELECTED) return SHOW_ATTRIBUTE_STATE;
        return NOT_STATE;
    }

    public static ThreeStateCheckBox.State toThreeState(int state) {
        if (state == NOT_SHOW_ATTRIBUTE_STATE) return ThreeStateCheckBox.NOT_SELECTED;
        if (state == SHOW_ATTRIBUTE_STATE) return ThreeStateCheckBox.SELECTED;
        return ThreeStateCheckBox.DONT_CARE;
    }
}