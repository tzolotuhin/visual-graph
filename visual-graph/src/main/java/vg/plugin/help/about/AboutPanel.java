package vg.plugin.help.about;

import vg.service.main.MainService;
import vg.service.resource.ResourceService;
import vg.shared.gui.StorageFrame;

import javax.swing.*;
import java.awt.*;

public class AboutPanel extends StorageFrame {
	private final static String WINDOWS_SIZE_X = "about_plugin_width";
	private final static String WINDOWS_SIZE_Y = "about_plugin_height";
	private final static String WINDOWS_POS_X = "about_plugin_pos_x";
	private final static String WINDOWS_POS_Y = "about_plugin_pos_y";

    public AboutPanel() {
        super(WINDOWS_POS_X, WINDOWS_POS_Y, WINDOWS_SIZE_X, WINDOWS_SIZE_Y, false);

        String text = "\nThanks for using Visual Graph\n\n" +
                "Home Page: https://bitbucket.org/tzolotuhin/visual-graph\n\n" +
                "e-mail: tzolotuhin@gmail.com\n\n";

        JTextArea aboutTextArea = new JTextArea(text);
        aboutTextArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
        aboutTextArea.setEditable(false);
        JLabel vgLabel = new JLabel(new ImageIcon(MainService.resourceService.getImageResource(ResourceService.LOGO_IMAGE)));

        frame.setResizable(false);
        frame.setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        frame.add(vgLabel, BorderLayout.WEST);
        frame.add(aboutTextArea, BorderLayout.EAST);
        frame.pack();
    }

    public void setVisible(boolean visible) {
        frame.setVisible(visible);
    }
}
