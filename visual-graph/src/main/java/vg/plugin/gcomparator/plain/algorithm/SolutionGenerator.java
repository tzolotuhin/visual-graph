package vg.plugin.gcomparator.plain.algorithm;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.Validate;
import vg.service.main.MainService;
import vg.shared.utils.MathUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class generates solutions.
 */
public class SolutionGenerator {
    // column index -> row index.
    private final int[] savedSolution;
	private final int[] currentSolution;
	private final int[] forDuplicatesValidation;
	private float currentSolutionWeight;
    private float savedSolutionWeight;

	private final List<RowPossibleValuesImpl> columns;
	private final RowPossibleValues firstColumn;

	private long estimateAmount;

	private boolean stopProcess;

    /**
     * @param table - not null.
     * @param barrier - in range from 0 to 1.
     * @throws IllegalArgumentException - if relationshipTable is null or barrier isn't in range from 0 to 1.
     */
	public SolutionGenerator(float[][] table, float barrier) {
		// check arguments.
        if (table == null || barrier > 1 || barrier < 0) {
            throw new IllegalArgumentException("table == null || barrier > 1 || barrier < 0");
        }

        // initialize arguments.
        this.columns = new ArrayList<>();
        this.currentSolutionWeight = 0;
        this.savedSolutionWeight = 0;
        this.stopProcess = false;
		this.currentSolution = table.length > 0 ? new int[table[0].length] : new int[0];
        Arrays.fill(currentSolution, -1);
		this.savedSolution = new int[this.currentSolution.length];
        Arrays.fill(savedSolution, -1);

        // need for stop process.
        RowPossibleValues lastFakeColumn = () -> stopProcess = true;

        // additional array for searching duplicates of row indices in the solution.
        forDuplicatesValidation = new int[table.length];
        Arrays.fill(forDuplicatesValidation, 0);

        // fill columns.
        List<Integer> possibleRowIndices = new ArrayList<>();
        possibleRowIndices.add(-1);
        List<Float> possibleRowValues = new ArrayList<>();
        possibleRowValues.add(0.0f);

        estimateAmount = 1;
        RowPossibleValues lastColumn = lastFakeColumn;
        for (int columnIndex = 0; columnIndex < currentSolution.length; columnIndex++) {
            for (int rowIndex = 0; rowIndex < table.length; rowIndex++) {
                if (table[rowIndex][columnIndex] >= barrier) {
                    possibleRowIndices.add(rowIndex);
                    possibleRowValues.add(table[rowIndex][columnIndex]);
                }
            }

            if (possibleRowIndices.size() > 1) {
                var column = new RowPossibleValuesImpl(
                        lastColumn,
                        columnIndex,
                        currentSolution,
                        ArrayUtils.toPrimitive(possibleRowIndices.toArray(new Integer[0]), -1),
                        ArrayUtils.toPrimitive(possibleRowValues.toArray(new Float[0]), 0.0F));

                columns.add(column);

                estimateAmount = MathUtils.multiplyExactOrReturnDefaultValue(
                        estimateAmount,
                        possibleRowValues.size(),
                        Long.MAX_VALUE);

                lastColumn = column;

                possibleRowIndices.clear();
                possibleRowIndices.add(-1);

                possibleRowValues.clear();
                possibleRowValues.add(0.0f);
            }
        }

        firstColumn = lastColumn;

        if (firstColumn == lastFakeColumn) {
            stopProcess = true;
        }

        // print columns.
        var printColumns = new StringBuilder();
        printColumns.append("Columns:\n");
        columns.forEach(column -> {
            printColumns.append("|");
            printColumns.append(column.getPossibleRowValuesAmount());
            printColumns.append("|");
        });
        MainService.logger.printDebug(printColumns.toString());
	}

	public long estimateAmount() {
	    return estimateAmount;
    }

    /**
     * Returns true if the next solution was found, and false otherwise.
     */
	public boolean tryNext() {
	    while (!stopProcess) {
	        currentSolutionWeight = 0;

            firstColumn.next();

            // check that solution is valid.
            boolean valid = true;

            Arrays.fill(forDuplicatesValidation, 0);
            for(int index = 0; index < currentSolution.length; index++) {
                if (currentSolution[index] < 0) {
                    continue;
                }
                forDuplicatesValidation[currentSolution[index]]++;

                if (forDuplicatesValidation[currentSolution[index]] > 1) {
                    valid = false;
                    break;
                }
            }

            if (valid) {
                for(var column : columns) {
                    currentSolutionWeight += column.getCurrentRowValue();
                }
                break;
            }
        }

        return !stopProcess;
    }

    public void saveCurrentSolution() {
        System.arraycopy(currentSolution, 0, savedSolution, 0, currentSolution.length);
        savedSolutionWeight = currentSolutionWeight;
    }

    public float getCurrentSolutionWeight() {
	    return currentSolutionWeight;
    }

    public float getSavedSolutionWeight() {
        return savedSolutionWeight;
    }

    /**
     * Note: only for reading.
     */
    public int[] getCurrentSolution() {
        return currentSolution;
    }

    /**
     * Note: only for reading.
     */
    public int[] getSavedSolution() {
        return savedSolution;
    }

    private interface RowPossibleValues {
        void next();
    }

    private static class RowPossibleValuesImpl implements RowPossibleValues {
	    private final RowPossibleValues next;
	    private final int currColumnIndex;
	    private final int[] currentSolution;
	    private final int[] possibleRowIndices;
        private final float[] possibleRowValues;
	    private int currentRowIndex;

        public RowPossibleValuesImpl(
                RowPossibleValues next,
                int currColumnIndex,
                int[] currentSolution,
                int[] possibleRowIndices,
                float[] possibleRowValues) {
            Validate.notNull(next);
            Validate.notNull(possibleRowIndices);
            Validate.isTrue(possibleRowIndices.length >= 1);
            Validate.isTrue(possibleRowIndices[0] == -1);

            Validate.notNull(possibleRowValues);
            Validate.isTrue(possibleRowValues.length >= 1);
            Validate.isTrue(possibleRowValues[0] == 0);

            Validate.isTrue(possibleRowValues.length == possibleRowIndices.length);

            this.next = next;
            this.currColumnIndex = currColumnIndex;
            this.currentSolution = currentSolution;
            this.possibleRowIndices = possibleRowIndices;
            this.possibleRowValues = possibleRowValues;
            this.currentRowIndex = 0;
        }

        @Override
        public void next() {
            currentRowIndex++;

            if (currentRowIndex >= possibleRowIndices.length) {
                currentRowIndex = 0;
                if (next != null) {
                    next.next();
                }
            }

            currentSolution[currColumnIndex] = possibleRowIndices[currentRowIndex];
        }

        public float getCurrentRowValue() {
            return possibleRowValues[currentRowIndex];
        }

        public int getPossibleRowValuesAmount() {
            return possibleRowValues.length;
        }
    }
}
