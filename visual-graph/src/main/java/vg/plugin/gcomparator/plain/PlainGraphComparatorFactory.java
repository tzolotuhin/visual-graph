package vg.plugin.gcomparator.plain;

import com.google.common.collect.Maps;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.plugin.gcomparator.plain.algorithm.GraphComparator;
import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.main.VGMainGlobals;
import vg.service.operation.OperationService;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.gui.data.SelectorSettingAttribute;
import vg.shared.gui.data.SliderSettingAttribute;

public class PlainGraphComparatorFactory extends OperationService.OperationFactory {
    public static final String GRAPH1 = "GRAPH1";
    public static final String GRAPH2 = "GRAPH2";
    public static final String SHOW_MODE = "SHOW_MODE";
    public static final String VERTEX_BARRIER = "VERTEX_BARRIER";
    public static final String EDGE_BARRIER = "EDGE_BARRIER";
    public static final String ATTRIBUTE_WEIGHTS_FOR_VERTICES = "ATTRIBUTE_WEIGHTS_FOR_VERTICES";
    public static final String ATTRIBUTE_WEIGHTS_FOR_EDGES = "ATTRIBUTE_WEIGHTS_FOR_EDGES";

    private static final Map<String, Integer> showModeMap;

    static {
        showModeMap = Maps.newLinkedHashMap();
        showModeMap.put(GraphComparator.SHOW_ALL_MODE_STR, GraphComparator.SHOW_ALL_MODE);
        showModeMap.put(GraphComparator.SHOW_DIFFERENCE_MODE_STR, GraphComparator.SHOW_DIFFERENCE_MODE);
    }

    @Override
    public OperationService.BaseProcedure buildOperation(Map<String, Object> args) {
        return new PlainGraphComparatorProcedure(args);
    }

    @Override
    public String getName() {
        return "Plain Graph Comparator";
    }

    private static class PlainGraphComparatorProcedure extends OperationService.BaseProcedure {
        // Input arguments.
        private Graph graph1, graph2;

        // Auxiliary fields.
        private Graph commonGraph;
        private int showMode;

        private PlainGraphComparatorProcedure(Map<String, Object> args) {
            this.args = args;
        }

        @Override
        public void execute() {
            var graphView = (GraphViewTab)getArgs().get(VGMainGlobals.GRAPH_VIEW_ARG);
            graph1 = (Graph)getArgs().get(GRAPH1);
            graph2 = (Graph)getArgs().get(GRAPH2);
            commonGraph = new Graph();

            showMode = showModeMap.get(getArg(PlainGraphComparatorFactory.SHOW_MODE, SelectorSettingAttribute.class).getSelectedItem());

            GraphUtils.resetDbIdToVertexIds(graph1);
            GraphUtils.resetDbIdToVertexIds(graph2);

            graphView.lock("Compare the graphs...");

            doExecute(graph1, graph2, null);

            graphView.unlock();

            graphView.clear();
            if (showMode == GraphComparator.SHOW_ALL_MODE || stopInProgress()) {
                graphView.addGraph(graph1);
                graphView.addGraph(graph2);
            }

            if (!stopInProgress()) {
                GraphUtils.resetDbIdToVertexIds(commonGraph);

                var g1VertexIdToVertexId = new HashMap<UUID, UUID>();
                var g2VertexIdToVertexId = new HashMap<UUID, UUID>();
                var vertexIdToIsPort = new HashMap<UUID, Boolean>();

                commonGraph.getAllVertices().forEach(vertex -> {
                    var vertexId = GraphUtils.getSystemVertexId(vertex);
                    var g1VertexId = vertex.getAttribute(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G1_VERTEX_ID);
                    var g2VertexId = vertex.getAttribute(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G2_VERTEX_ID);

                    if (g1VertexId != null) {
                        g1VertexIdToVertexId.put(g1VertexId.getUUIDValue(), vertexId);
                    }
                    if (g2VertexId != null) {
                        g2VertexIdToVertexId.put(g2VertexId.getUUIDValue(), vertexId);
                    }

                    vertexIdToIsPort.put(vertexId, GraphUtils.isPort(vertex));

                    // remove the unnecessary attributes.
                    vertex.removeAllAttributes(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G1_VERTEX_ID);
                    vertex.removeAllAttributes(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G2_VERTEX_ID);
                });

                commonGraph.getAllEdges().forEach(edge -> {
                    var systemG1SrcPortId = edge.getAttribute(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G1_SRC_PORT_ID);
                    var systemG2SrcPortId = edge.getAttribute(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G2_SRC_PORT_ID);
                    var systemG1TrgPortId = edge.getAttribute(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G1_TRG_PORT_ID);
                    var systemG2TrgPortId = edge.getAttribute(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G2_TRG_PORT_ID);

                    if (systemG1SrcPortId != null && systemG2SrcPortId != null) {
                        var vertexId1 = g1VertexIdToVertexId.get(systemG1SrcPortId.getUUIDValue());
                        var vertexId2 = g2VertexIdToVertexId.get(systemG2SrcPortId.getUUIDValue());

                        if (vertexId1 != null
                                && vertexId2 != null
                                && Objects.equals(vertexId1, vertexId2)
                                && vertexIdToIsPort.getOrDefault(vertexId1, false)) {
                            GraphUtils.resetSystemSrcPortIdAttribute(edge, vertexId1);
                        }
                    }

                    if (systemG1TrgPortId != null && systemG2TrgPortId != null) {
                        var vertexId1 = g1VertexIdToVertexId.get(systemG1TrgPortId.getUUIDValue());
                        var vertexId2 = g2VertexIdToVertexId.get(systemG2TrgPortId.getUUIDValue());

                        if (vertexId1 != null
                                && vertexId2 != null
                                && Objects.equals(vertexId1, vertexId2)
                                && vertexIdToIsPort.getOrDefault(vertexId1, false)) {
                            GraphUtils.resetSystemTrgPortIdAttribute(edge, vertexId1);
                        }
                    }

                    // remove the unnecessary attributes.
                    edge.removeAllAttributes(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G1_SRC_PORT_ID);
                    edge.removeAllAttributes(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G2_SRC_PORT_ID);
                    edge.removeAllAttributes(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G1_TRG_PORT_ID);
                    edge.removeAllAttributes(GraphComparator.ATTRIBUTE_NAME_FOR_SYSTEM_G2_TRG_PORT_ID);
                });
            }

            graphView.addGraph(commonGraph, value -> MainService.executorService.executeTaskByKey(value.getId().toString(), () -> {
                Map<String, Object> args = Maps.newLinkedHashMap();
                args.put(VGMainGlobals.GRAPH_VIEW_ARG, value);

                MainService.operationService
                        .getOperationFactoryByName(VGMainGlobals.LAYOUT_OPERATION_FACTORY)
                        .buildOperation(args).execute();
            }));
        }

        private void doExecute(Vertex parent1, Vertex parent2, Vertex parent) {
            if (stopInProgress()) {
                return;
            }

            var subGraph1 = new Graph();
            if (parent1 != null) {
                subGraph1 = graph1.getSubGraph(parent1.getChildVertices());
            }

            var subGraph2 = new Graph();
            if (parent2 != null) {
                subGraph2 = graph2.getSubGraph(parent2.getChildVertices());
            }

            var comparator = new GraphComparator(subGraph1, subGraph2, state);

            comparator.setShowMode(showMode);

            var vertexBarrier = getArg(PlainGraphComparatorFactory.VERTEX_BARRIER, SliderSettingAttribute.class);
            comparator.setVertexBarrier(vertexBarrier.normalizeCurrValue());

            var edgeBarrier = getArg(PlainGraphComparatorFactory.EDGE_BARRIER, SliderSettingAttribute.class);
            comparator.setEdgeBarrier(edgeBarrier.normalizeCurrValue());

            getListArg(PlainGraphComparatorFactory.ATTRIBUTE_WEIGHTS_FOR_VERTICES, SliderSettingAttribute.class)
                    .forEach(x -> comparator.getVertexWeights().put(x.getName(), x.normalizeCurrValue()));

            getListArg(PlainGraphComparatorFactory.ATTRIBUTE_WEIGHTS_FOR_EDGES, SliderSettingAttribute.class)
                    .forEach(x -> comparator.getEdgeWeights().put(x.getName(), x.normalizeCurrValue()));

            comparator.compare();
            if (stopInProgress()) {
                return;
            }

            for (var vertex : comparator.getCommonGraph().getAllVertices()) {
                commonGraph.insertVertex(vertex, parent);
            }
            commonGraph.addEdges(comparator.getCommonGraph().getAllEdges());

            // handle common vertices.
            for (var entry : comparator.getG1CommonVertices().entrySet()) {
                var tmpParent = entry.getKey();
                var tmpParent1 = entry.getValue();
                var tmpParent2 = comparator.getG2CommonVertices().get(tmpParent);

                if (tmpParent1.hasChildren() || tmpParent2.hasChildren()) {
                    doExecute(tmpParent1, tmpParent2, tmpParent);
                }
            }

            // handle outside vertices of first graph.
            for (var entry : comparator.getG1OutsideVertices().entrySet()) {
                var tmpParent = entry.getKey();
                var tmpParent1 = entry.getValue();

                if (tmpParent1.hasChildren()) {
                    doExecute(tmpParent1, null, tmpParent);
                }
            }

            // handle outside vertices of second graph.
            for (var entry : comparator.getG2OutsideVertices().entrySet()) {
                var tmpParent = entry.getKey();
                var tmpParent2 = entry.getValue();

                if (tmpParent2.hasChildren()) {
                    doExecute(null, tmpParent2, tmpParent);
                }
            }
        }
    }
}
