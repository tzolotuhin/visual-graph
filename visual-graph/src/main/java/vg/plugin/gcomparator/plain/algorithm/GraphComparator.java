package vg.plugin.gcomparator.plain.algorithm;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.Validate;
import vg.lib.model.graph.Attribute;
import vg.lib.model.graph.AttributedItem;
import vg.lib.model.graph.Edge;
import vg.lib.model.graph.Graph;
import vg.lib.model.graph.Vertex;
import vg.lib.model.record.AttributeRecordType;
import vg.service.main.MainService;
import vg.service.operation.OperationService;
import vg.shared.graph.utils.GraphUtils;

/**
 * Algorithm for matching of two graphs.
 */
public class GraphComparator {
    // Constants
    public static final String INCOMING_COUNT_EDGE = "Incoming edges";
    public static final String OUTGOING_COUNT_EDGE = "Outgoing edges";

    public static final String ATTRIBUTE_NAME_FOR_SYSTEM_G1_VERTEX_ID = "system_g1_vertex_id";
    public static final String ATTRIBUTE_NAME_FOR_SYSTEM_G2_VERTEX_ID = "system_g2_vertex_id";

    public static final String ATTRIBUTE_NAME_FOR_SYSTEM_G1_SRC_PORT_ID = "system_g1_src_port_id";
    public static final String ATTRIBUTE_NAME_FOR_SYSTEM_G2_SRC_PORT_ID = "system_g2_src_port_id";

    public static final String ATTRIBUTE_NAME_FOR_SYSTEM_G1_TRG_PORT_ID = "system_g1_trg_port_id";
    public static final String ATTRIBUTE_NAME_FOR_SYSTEM_G2_TRG_PORT_ID = "system_g2_trg_port_id";

    public static final String SHOW_ALL_MODE_STR = "Show all";
    public static final String SHOW_DIFFERENCE_MODE_STR = "Show difference";

    public static final int SHOW_ALL_MODE = 1;
    public static final int SHOW_DIFFERENCE_MODE = 2;

    private static final float MAX_PERCENT_VALUE = 1.0f;
    private static final float MIN_PERCENT_VALUE = 0.0f;
    private static final float MIDDLE_PERCENT_VALUE = 0.0f;

    private static final String G1_COLOR = "0xE06666";
    private static final String G2_COLOR = "0x93C47D";
    private static final String C_COLOR = "0xFFD966";

    // Main data
    private final VertexWithIndex[] g1Vertices, g2Vertices;
    private final EdgeWithIndex[] g1Edges, g2Edges;

    @Getter @Setter
    private float vertexBarrier, edgeBarrier;

    @Getter @Setter
    private int showMode;

    private boolean enableStrongComparison;

    @Getter
    private final Map<String, Float> vertexWeights, edgeWeights;

    private Map<String, Float> normalizeVertexWeightsForAttributes;
    private Map<String, Float> normalizeEdgeWeightsForAttributes;

    private float[][] vertexMatchingTable = new float[0][];
    private float[][] edgeMatchingTable = new float[0][];

    private int[][] g1AdjacencyMatrix, g2AdjacencyMatrix;
    private Map<Integer, List<Integer>> g1MultiEdges, g2MultiEdges;

    private int[] vertexMatchingResult, edgeMatchingResult;

    private final AtomicInteger state;

    @Getter
    private Graph commonGraph;

    // common graph vertex -> original graph vertex.
    @Getter
    private BiMap<Vertex, Vertex> g1CommonVertices, g2CommonVertices;

    // common graph vertex -> original graph vertex.
    @Getter
    private BiMap<Vertex, Vertex> g1OutsideVertices, g2OutsideVertices;

    public GraphComparator(Graph g1, Graph g2, AtomicInteger state) {
        Validate.notNull(g1);
        Validate.notNull(g2);
        Validate.notNull(state);

        this.state = state;

        vertexBarrier = 1.0f;
        edgeBarrier = 1.0f;

        g1Vertices = initVertices(g1);
        g2Vertices = initVertices(g2);

        g1Edges = initEdges(g1, g1Vertices);
        g2Edges = initEdges(g2, g2Vertices);

        buildAdjacencyMatrices();

        // initialize vertex & edge attribute to weight map
        vertexWeights = Maps.newLinkedHashMap();
        edgeWeights = Maps.newLinkedHashMap();

        collectVertexAttributes(g1, vertexWeights);
        collectVertexAttributes(g2, vertexWeights);

        collectEdgeAttributes(g1, edgeWeights);
        collectEdgeAttributes(g2, edgeWeights);
    }

    public void compare() {
        MainService.logger.printDebug("Method 'GraphComparator.compare' was called.");

        // run phases...
        normalizeWeights();
        buildMatchingTables();
        match();
        buildCommonGraph();
    }

    private void buildAdjacencyMatrices() {
        g1MultiEdges = new HashMap<>();
        g2MultiEdges = new HashMap<>();

        g1AdjacencyMatrix = buildAdjacencyMatrix(g1Vertices, g1Edges, g1MultiEdges);
        g2AdjacencyMatrix = buildAdjacencyMatrix(g2Vertices, g2Edges, g2MultiEdges);
    }

    private int[][] buildAdjacencyMatrix(VertexWithIndex[] vertices, EdgeWithIndex[] edges, Map<Integer, List<Integer>> multiEdges) {
        int[][] adjacencyMatrix = new int[vertices.length][vertices.length];
        for (int i = 0; i < vertices.length; i++) {
            Arrays.fill(adjacencyMatrix[i], Integer.MIN_VALUE);
        }

        int multiEdgesIndex = -1;
        for (var edge : edges) {
            int currEdgeIndex = adjacencyMatrix[edge.getSrcIndex()][edge.getTrgIndex()];

            if (currEdgeIndex == Integer.MIN_VALUE) {
                adjacencyMatrix[edge.getSrcIndex()][edge.getTrgIndex()] = edge.getIndex();
                continue;
            }

            if (currEdgeIndex < 0) {
                multiEdges.get(currEdgeIndex).add(edge.getIndex());
                continue;
            }

            // if current edge index more zero.
            var multiEdgIndices = new ArrayList<Integer>();
            multiEdgIndices.add(currEdgeIndex);
            multiEdgIndices.add(edge.getIndex());
            multiEdges.put(multiEdgesIndex, multiEdgIndices);

            adjacencyMatrix[edge.getSrcIndex()][edge.getTrgIndex()] = multiEdgesIndex;

            multiEdgesIndex--;
        }

        return adjacencyMatrix;
    }

    private void buildMatchingTables() {
        vertexMatchingTable = buildMatchingTable(
                g1Vertices,
                g2Vertices,
                normalizeVertexWeightsForAttributes,
                enableStrongComparison);

        edgeMatchingTable = buildMatchingTable(
                g1Edges,
                g2Edges,
                normalizeEdgeWeightsForAttributes,
                enableStrongComparison
        );

        // apply Hungarian optimization.
//        float[][] cost = new float[g1.getAllVertices().size()][g2.getAllVertices().size()];
//        heuristicVertexMatchingTable = new float[g1.getAllVertices().size()][g2.getAllVertices().size()];
//        heuristicEdgeMatchingTable = new float[g1.getAllEdges().size()][g2.getAllEdges().size()];
//
//        HungarianAlgorithmOutput hungarianAlgorithmOutput = new HungarianAlgorithm(vertexMatchingTable, HungarianAlgorithm.MAXIMIZE).execute();
//        if (hungarianAlgorithmOutput != null) {
//            cost = hungarianAlgorithmOutput.getCost();
//        }
//
//        for (int i = 0; i < cost.length; i++) {
//            for (int j = 0; j < cost[i].length; j++) {
//                if (cost[i][j] == 0 && vertexMatchingTable[i][j] >= vertexBarrier) {
//                    heuristicVertexMatchingTable[i][j] = vertexMatchingTable[i][j];
//                } else {
//                    heuristicVertexMatchingTable[i][j] = 0;
//                }
//            }
//        }
//
//        for (int i = 0; i < g1.getAllEdges().size(); i++) {
//            System.arraycopy(edgeMatchingTable[i], 0, heuristicEdgeMatchingTable[i], 0, g2.getAllEdges().size());
//        }
    }

    /**
     * Build matching table for the graph1 and graph2.
     */
    private static float[][] buildMatchingTable(
            ElementWithIndex[] g1Elements,
            ElementWithIndex[] g2Elements,
            Map<String, Float> weights,
            boolean enableStrongComparison) {
        float[][] matchingTable = new float[g1Elements.length][g2Elements.length];

        for (int index1 = 0; index1 < g1Elements.length; index1++) {
            for (int index2 = 0; index2 < g2Elements.length; index2++) {
                matchingTable[index1][index2] = AttributedItem.getSimilarityPercentage(
                        g1Elements[index1].getAttributedItem(),
                        g2Elements[index2].getAttributedItem(),
                        weights,
                        enableStrongComparison);
            }
        }

        return matchingTable;
    }

    private void match() {
        MainService.logger.printDebug("Method 'GraphComparator.match' was called.");

        vertexMatchingResult = doMatch(new SolutionGenerator(vertexMatchingTable, vertexBarrier));

        // print matching result for vertices.
        MainService.logger.printDebug("Print matching result for vertices.");
        for (int g2VertexIndex = 0; g2VertexIndex < vertexMatchingResult.length; g2VertexIndex++) {
            if (vertexMatchingResult[g2VertexIndex] < 0) {
                continue;
            }

            MainService.logger.printDebug(String.format(
                    "%s (%s) -> %s (%s)",
                    GraphUtils.getVertexIdAttribute(g2Vertices[g2VertexIndex].getVertex()),
                    g2VertexIndex,
                    GraphUtils.getVertexIdAttribute(g1Vertices[vertexMatchingResult[g2VertexIndex]].getVertex()),
                    vertexMatchingResult[g2VertexIndex]));
        }

        var localEdgeMatchingTable = new float[g1Edges.length][g2Edges.length];
        for (int g2SrcIndex = 0; g2SrcIndex < vertexMatchingResult.length; g2SrcIndex++) {
            for (int g2TrgIndex = 0; g2TrgIndex < vertexMatchingResult.length; g2TrgIndex++) {
                int g1SrcIndex = vertexMatchingResult[g2SrcIndex];
                int g1TrgIndex = vertexMatchingResult[g2TrgIndex];

                if (g1SrcIndex >= 0 && g1TrgIndex >= 0) {
                    int g1EdgeIndex = g1AdjacencyMatrix[g1SrcIndex][g1TrgIndex];
                    int g2EdgeIndex = g2AdjacencyMatrix[g2SrcIndex][g2TrgIndex];
                    if (g1EdgeIndex == Integer.MIN_VALUE || g2EdgeIndex == Integer.MIN_VALUE) {
                        // do nothing...
                        continue;
                    }

                    if (g1EdgeIndex >=0 && g2EdgeIndex >= 0) {
                        // the most common case...
                        localEdgeMatchingTable[g1EdgeIndex][g2EdgeIndex] = edgeMatchingTable[g1EdgeIndex][g2EdgeIndex];
                        continue;
                    }

                    List<Integer> g1EdgeIndices;
                    if (g1EdgeIndex >= 0) {
                        g1EdgeIndices = Collections.singletonList(g1EdgeIndex);
                    } else {
                        g1EdgeIndices = g1MultiEdges.get(g1EdgeIndex);
                    }

                    List<Integer> g2EdgeIndices;
                    if (g2EdgeIndex >= 0) {
                        g2EdgeIndices = Collections.singletonList(g2EdgeIndex);
                    } else {
                        g2EdgeIndices = g2MultiEdges.get(g2EdgeIndex);
                    }

                    for (var g1TmpEdgeIndex : g1EdgeIndices) {
                        for (var g2TmpEdgeIndex : g2EdgeIndices) {
                            localEdgeMatchingTable[g1TmpEdgeIndex][g2TmpEdgeIndex] = edgeMatchingTable[g1TmpEdgeIndex][g2TmpEdgeIndex];
                        }
                    }
                }
            }
        }

        edgeMatchingResult = doMatch(new SolutionGenerator(localEdgeMatchingTable, edgeBarrier));

        // print matching result for edges.
        MainService.logger.printDebug("Print matching result for edges.");
        for (int g2EdgeIndex = 0; g2EdgeIndex < edgeMatchingResult.length; g2EdgeIndex++) {
            if (edgeMatchingResult[g2EdgeIndex] < 0) {
                continue;
            }

            MainService.logger.printDebug(String.format(
                    "%s (%s) -> %s (%s)",
                    GraphUtils.getEdgeIdAttribute(g2Edges[g2EdgeIndex].getEdge()),
                    g2EdgeIndex,
                    GraphUtils.getEdgeIdAttribute(g1Edges[edgeMatchingResult[g2EdgeIndex]].getEdge()),
                    edgeMatchingResult[g2EdgeIndex]));
        }
    }

    private int[] doMatch(SolutionGenerator solutionGenerator) {
        MainService.logger.printDebug(String.format("Method 'GraphComparator.doMatch' was called, amount of solutions ~ %s.", solutionGenerator.estimateAmount()));

        long count = 0;
        while (!OperationService.BaseProcedure.stopInProgress(state) && solutionGenerator.tryNext()) {
            int re = Float.compare(solutionGenerator.getCurrentSolutionWeight(), solutionGenerator.getSavedSolutionWeight());
            if (re > 0) {
                solutionGenerator.saveCurrentSolution();
            }
            count++;

            if (count % 100000 == 0) {
                MainService.logger.printDebug(String.format("Count of checked solutions ~ %s.", count));
            }
        }

        return solutionGenerator.getSavedSolution();
    }

    private void buildCommonGraph() {
        g1CommonVertices = HashBiMap.create();
        g2CommonVertices = HashBiMap.create();
        g1OutsideVertices = HashBiMap.create();
        g2OutsideVertices = HashBiMap.create();

        // build common vertices for g1 and g2.
        int[] reverseVertexMatchingResult = new int[g1Vertices.length];
        Arrays.fill(reverseVertexMatchingResult, -1);

        List<Vertex> commonVertices = new ArrayList<>();
        for (int g2VertexIndex = 0; g2VertexIndex < vertexMatchingResult.length; g2VertexIndex++) {
            int g1VertexIndex = vertexMatchingResult[g2VertexIndex];

            if (g1VertexIndex < 0) {
                continue;
            }

            reverseVertexMatchingResult[g1VertexIndex] = g2VertexIndex;

            var g1Vertex = g1Vertices[g1VertexIndex].getVertex();
            var g2Vertex = g2Vertices[g2VertexIndex].getVertex();

            var commonVertex = new Vertex(mergingAttributeItems(g1Vertex, g2Vertex).getAttributes());
            commonVertices.add(commonVertex);

            g1CommonVertices.put(commonVertex, g1Vertex);
            g2CommonVertices.put(commonVertex, g2Vertex);

            commonVertex.addInvisibleAttribute(
                ATTRIBUTE_NAME_FOR_SYSTEM_G1_VERTEX_ID,
                GraphUtils.getSystemVertexId(g1Vertex),
                AttributeRecordType.UUID);
            commonVertex.addInvisibleAttribute(
                ATTRIBUTE_NAME_FOR_SYSTEM_G2_VERTEX_ID,
                GraphUtils.getSystemVertexId(g2Vertex),
                AttributeRecordType.UUID);

            var isInputPort = GraphUtils.isInputPort(g1Vertex) & GraphUtils.isInputPort(g2Vertex);
            var isOutputPort = GraphUtils.isOutputPort(g1Vertex) & GraphUtils.isOutputPort(g2Vertex);
            var isFakePort = GraphUtils.isFakePort(g1Vertex) & GraphUtils.isFakePort(g2Vertex);

            if (isInputPort) {
                GraphUtils.resetSystemPortAttribute(commonVertex, GraphUtils.INPUT_PORT_TYPE, isFakePort);
            }
            if (isOutputPort) {
                GraphUtils.resetSystemPortAttribute(commonVertex, GraphUtils.OUTPUT_PORT_TYPE, isFakePort);
            }
        }

        // build outside vertices for g1.
        for (int g1VertexIndex = 0; g1VertexIndex < reverseVertexMatchingResult.length; g1VertexIndex++) {
            if (reverseVertexMatchingResult[g1VertexIndex] < 0) {
                var g1Vertex = g1Vertices[g1VertexIndex];

                var outsizeVertex = new Vertex(g1Vertex.getVertex());
                GraphUtils.resetSystemColorAttribute(outsizeVertex, G1_COLOR);
                outsizeVertex.addInvisibleAttribute(
                    ATTRIBUTE_NAME_FOR_SYSTEM_G1_VERTEX_ID,
                    GraphUtils.getSystemVertexId(g1Vertex.getVertex()),
                    AttributeRecordType.UUID);

                g1OutsideVertices.put(outsizeVertex, g1Vertex.getVertex());
            }
        }

        // build outside vertices for g2.
        for (int g2VertexIndex = 0; g2VertexIndex < vertexMatchingResult.length; g2VertexIndex++) {
            if (vertexMatchingResult[g2VertexIndex] < 0) {
                var g2Vertex = g2Vertices[g2VertexIndex];

                var outsizeVertex = new Vertex(g2Vertex.getVertex());
                GraphUtils.resetSystemColorAttribute(outsizeVertex, G2_COLOR);
                outsizeVertex.addInvisibleAttribute(
                    ATTRIBUTE_NAME_FOR_SYSTEM_G2_VERTEX_ID,
                    GraphUtils.getSystemVertexId(g2Vertex.getVertex()),
                    AttributeRecordType.UUID);

                g2OutsideVertices.put(outsizeVertex, g2Vertex.getVertex());
            }
        }

        // build common edges for g1 and g2.
        int[] reverseEdgeMatchingResult = new int[g1Edges.length];
        Arrays.fill(reverseEdgeMatchingResult, -1);

        List<Edge> commonEdges = new ArrayList<>();
        for (int g2EdgeIndex = 0; g2EdgeIndex < edgeMatchingResult.length; g2EdgeIndex++) {
            int g1EdgeIndex = edgeMatchingResult[g2EdgeIndex];

            if (g1EdgeIndex < 0) {
                continue;
            }

            reverseEdgeMatchingResult[g1EdgeIndex] = g2EdgeIndex;

            var g1Edge = g1Edges[g1EdgeIndex].getEdge();
            var g2Edge = g2Edges[g2EdgeIndex].getEdge();

            var g1CommonEdgeSource = g1CommonVertices.inverse().get(g1Edge.getSource());
            var g2CommonEdgeSource = g2CommonVertices.inverse().get(g2Edge.getSource());
            Validate.isTrue(g1CommonEdgeSource == g2CommonEdgeSource);

            var g1CommonEdgeTarget = g1CommonVertices.inverse().get(g1Edge.getTarget());
            var g2CommonEdgeTarget = g2CommonVertices.inverse().get(g2Edge.getTarget());
            Validate.isTrue(g1CommonEdgeTarget == g2CommonEdgeTarget);

            var commonEdge = new Edge(g1CommonEdgeSource, g1CommonEdgeTarget, mergingAttributeItems(g1Edge, g2Edge).getAttributes());
            commonEdges.add(commonEdge);

            var isDirectedEdge = GraphUtils.isDirectedEdge(g1Edge) & GraphUtils.isDirectedEdge(g2Edge);
            GraphUtils.resetDirectedEdgeAttribute(commonEdge, isDirectedEdge);

            // handle the case for attribute related to ports.
            var g1EdgeSrcPortId = GraphUtils.getSrcPortId(g1Edge);
            var g1EdgeTrgPortId = GraphUtils.getTrgPortId(g1Edge);

            var g2EdgeSrcPortId = GraphUtils.getSrcPortId(g2Edge);
            var g2EdgeTrgPortId = GraphUtils.getTrgPortId(g2Edge);

            if (g1EdgeSrcPortId != null) {
                commonEdge.addInvisibleAttribute(
                    ATTRIBUTE_NAME_FOR_SYSTEM_G1_SRC_PORT_ID,
                    g1EdgeSrcPortId,
                    AttributeRecordType.UUID);
            }
            if (g2EdgeSrcPortId != null) {
                commonEdge.addInvisibleAttribute(
                    ATTRIBUTE_NAME_FOR_SYSTEM_G2_SRC_PORT_ID,
                    g2EdgeSrcPortId,
                    AttributeRecordType.UUID);
            }
            if (g1EdgeTrgPortId != null) {
                commonEdge.addInvisibleAttribute(
                    ATTRIBUTE_NAME_FOR_SYSTEM_G1_TRG_PORT_ID,
                    g1EdgeTrgPortId,
                    AttributeRecordType.UUID);
            }
            if (g2EdgeTrgPortId != null) {
                commonEdge.addInvisibleAttribute(
                    ATTRIBUTE_NAME_FOR_SYSTEM_G2_TRG_PORT_ID,
                    g2EdgeTrgPortId,
                    AttributeRecordType.UUID);
            }
        }

        // build outside edges for g1.
        var g1OutsideEdges = buildOutsideEdges(g1Edges, reverseEdgeMatchingResult, g1CommonVertices, g1OutsideVertices, G1_COLOR);

        // build outside edges for g2.
        var g2OutsideEdges = buildOutsideEdges(g2Edges, edgeMatchingResult, g2CommonVertices, g2OutsideVertices, G2_COLOR);

        // build common graph.
        commonVertices.addAll(g1OutsideVertices.keySet());
        commonVertices.addAll(g2OutsideVertices.keySet());

        commonEdges.addAll(g1OutsideEdges);
        commonEdges.addAll(g2OutsideEdges);

        commonGraph = new Graph(commonVertices, commonEdges);
    }

    private static List<Edge> buildOutsideEdges(EdgeWithIndex[] edges, int[] edgeMatchingResult, BiMap<Vertex, Vertex> commonVertices, BiMap<Vertex, Vertex> outsideVertices, String color) {
        List<Edge> g1OutsideEdges = new ArrayList<>();
        for (int g1EdgeIndex = 0; g1EdgeIndex < edgeMatchingResult.length; g1EdgeIndex++) {
            if (edgeMatchingResult[g1EdgeIndex] < 0) {
                var g1Edge = edges[g1EdgeIndex].getEdge();

                var outsizeEdgeSource = commonVertices.inverse().get(g1Edge.getSource());
                if (outsizeEdgeSource == null) {
                    outsizeEdgeSource = outsideVertices.inverse().get(g1Edge.getSource());
                }

                var outsizeEdgeTarget = commonVertices.inverse().get(g1Edge.getTarget());
                if (outsizeEdgeTarget == null) {
                    outsizeEdgeTarget = outsideVertices.inverse().get(g1Edge.getTarget());
                }

                var g1OutsideEdge = new Edge(outsizeEdgeSource, outsizeEdgeTarget, g1Edge.getAttributes());
                GraphUtils.resetSystemColorAttribute(g1OutsideEdge, color);
                g1OutsideEdges.add(g1OutsideEdge);
            }
        }
        return g1OutsideEdges;
    }

    private void normalizeWeights() {
        normalizeVertexWeightsForAttributes = Maps.newLinkedHashMap();
        normalizeEdgeWeightsForAttributes = Maps.newLinkedHashMap();

        float vertexWeight = ((Double) vertexWeights.values().stream().mapToDouble(Float::doubleValue).sum()).floatValue();
        float edgeWeight = ((Double) edgeWeights.values().stream().mapToDouble(Float::doubleValue).sum()).floatValue();

        vertexWeights.forEach((x, y) -> {
            if (Float.compare(vertexWeight, 0.0f) == 0)
                normalizeVertexWeightsForAttributes.put(x, 0.0f);
            else
                normalizeVertexWeightsForAttributes.put(x, y / vertexWeight);
        });

        edgeWeights.forEach((x, y) -> {
            if (Float.compare(edgeWeight, 0.0f) == 0)
                normalizeEdgeWeightsForAttributes.put(x, 0.0f);
            else
                normalizeEdgeWeightsForAttributes.put(x, y / edgeWeight);
        });
    }

    private AttributedItem mergingAttributeItems(AttributedItem item1, AttributedItem item2) {
        List<Attribute> attributes = Lists.newArrayList();
        for (Attribute attr1 : item1.getAttributes()) {
            Attribute attr2 = item2.getAttribute(attr1.getName());

            // skip invisible attributes.
            if (!attr1.isVisible()) {
                continue;
            }

            String value1 = attr1.getStringValue();
            String value2 = null;

            if (attr2 != null && attr2.isVisible()) {
                value2 = attr2.getStringValue();
            }

            String diffValue;
            if (value1.equals(value2)) {
                diffValue = value1;
            } else {
                diffValue = value1 + " | " + value2;
            }

            attributes.add(new Attribute(
                attr1.getName(), diffValue, AttributeRecordType.STRING, true));
        }
        return new AttributedItem(attributes);
    }

    private static VertexWithIndex[] initVertices(Graph graph) {
        var vertices = new VertexWithIndex[graph.getAllVertices().size()];
        int index = 0;
        for (Vertex vertex : graph.getAllVertices()) {
            var attributedItem = new AttributedItem(vertex);

            attributedItem.addAttribute(new Attribute(
                    INCOMING_COUNT_EDGE,
                    graph.getIncomingEdgeCount(vertex),
                    AttributeRecordType.INTEGER,
                    false));

            attributedItem.addAttribute(new Attribute(
                    OUTGOING_COUNT_EDGE,
                    graph.getOutgoingEdgeCount(vertex),
                    AttributeRecordType.INTEGER,
                    false));

            vertices[index] = new VertexWithIndex(vertex, index, attributedItem);

            index++;
        }
        return vertices;
    }

    private static EdgeWithIndex[] initEdges(Graph graph, VertexWithIndex[] vertices) {
        // build map for performance boost.
        var vertexToIndex = new HashMap<Vertex, Integer>();
        for (var vertex : vertices) {
            vertexToIndex.put(vertex.getVertex(), vertex.getIndex());
        }

        // build result...
        var edges = new EdgeWithIndex[graph.getAllEdges().size()];
        int index = 0;
        for (Edge edge : graph.getAllEdges()) {
            var attributedItem = new AttributedItem(edge);

            int srcIndex = vertexToIndex.get(edge.getSource());
            int trgIndex = vertexToIndex.get(edge.getTarget());

            edges[index] = new EdgeWithIndex(edge, index, srcIndex, trgIndex, attributedItem);

            index++;
        }

        return edges;
    }

    /**
     * Adds vertex attributes to map with weight, if this map doesn't contains it.
     */
    private static void collectVertexAttributes(Graph graph, Map<String, Float> vertexAttribute2Weight) {
        graph.getAllVertexAttributeNames(true).forEach(attrName -> vertexAttribute2Weight.put(attrName, MIDDLE_PERCENT_VALUE));
    }

    /**
     * Adds edge attributes to map with weight, if this map doesn't contains it.
     */
    private static void collectEdgeAttributes(Graph graph, Map<String, Float> edgeAttribute2Weight) {
        graph.getAllEdgeAttributeNames(true).forEach(attrName -> edgeAttribute2Weight.put(attrName, MIDDLE_PERCENT_VALUE));
    }

    private static class ElementWithIndex {
        @Getter
        private final AttributedItem attributedItem;

        @Getter
        private final int index;

        public ElementWithIndex(AttributedItem attributedItem, int index) {
            this.attributedItem = attributedItem;
            this.index = index;
        }
    }

    private static class VertexWithIndex extends ElementWithIndex {
        @Getter
        private final Vertex vertex;

        public VertexWithIndex(Vertex vertex, int index, AttributedItem attributedItem) {
            super(attributedItem, index);

            this.vertex = vertex;
        }
    }

    private static class EdgeWithIndex extends ElementWithIndex {
        @Getter
        private final Edge edge;

        @Getter
        private final int srcIndex, trgIndex;

        public EdgeWithIndex(Edge edge, int index, int srcIndex, int trgIndex, AttributedItem attributedItem) {
            super(attributedItem, index);

            this.edge = edge;
            this.srcIndex = srcIndex;
            this.trgIndex = trgIndex;
        }
    }
}
