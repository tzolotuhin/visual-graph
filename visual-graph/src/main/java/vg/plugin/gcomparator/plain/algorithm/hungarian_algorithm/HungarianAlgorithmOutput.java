package vg.plugin.gcomparator.plain.algorithm.hungarian_algorithm;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class HungarianAlgorithmOutput {
    private int[] assigned;
    private float[][] cost;
    private float sum;

    public HungarianAlgorithmOutput(int[] assigned, float[][] cost, float sum) {
        this.assigned = assigned;
        this.cost = cost;
        this.sum = sum;
    }

    public int[] getAssigned() {
        return assigned;
    }

    public float[][] getCost() {
        return cost;
    }

    public float getSum() {
        return sum;
    }
}
