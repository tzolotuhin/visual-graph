package vg.plugin.gcomparator;

import vg.lib.model.graph.Graph;
import vg.service.gview.GraphViewTab;

public interface GraphComparatorPluginCallback {
    void registerNewGraphViewForComparison(GraphViewTab graphViewTab, Graph graph1, Graph graph2);
}
