package vg.plugin.gcomparator;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import vg.lib.model.graph.Graph;
import vg.plugin.gcomparator.plain.PlainGraphComparatorFactory;
import vg.plugin.gcomparator.plain.algorithm.GraphComparator;
import vg.service.executor.ExecutorService;
import vg.service.gview.GraphViewTab;
import vg.service.main.MainService;
import vg.service.main.VGMainGlobals;
import vg.service.operation.OperationService;
import vg.service.plugin.Plugin;
import vg.service.resource.ResourceService;
import vg.service.ui.UserInterfaceService;
import vg.shared.graph.utils.GraphUtils;
import vg.shared.gui.UIUtils;
import vg.shared.gui.data.SelectorSettingAttribute;
import vg.shared.gui.data.SettingAttribute;
import vg.shared.gui.data.SliderSettingAttribute;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class GraphComparatorPlugin implements Plugin, UserInterfaceService.UserInterfacePanel, GraphComparatorPluginCallback {
    // Constants
    private static final int NO_COMPARISON_VIEW_STATE = 0;
    private static final int COMPARISON_VIEW_STATE = 1;
    private static final int COMPARISON_VIEW_IN_PROGRESS_STATE = 2;

    private static final Insets rowInsets = new Insets(2, 5, 2, 5);

    // Main components.
    private JPanel innerView, outView;

    private UserInterfaceService.UserInterfaceInstrument instrument;

    private JLabel infoLabel;
    private JLabel pleaseWaitInfoLabel;
    private JLabel settingsLabel;
    private JLabel vertexAttributesLabel;
    private JLabel edgeAttributesLabel;

    private JButton runButton;
    private JButton cancelButton;

    // Main data.
    private GraphViewTab currentGraphComparator;

    private final Map<GraphViewTab, GraphComparatorEntry> graphViewToEntry = Maps.newHashMap();

    // Mutex
    private final Object generalMutex = new Object();

    @Override
    public void install() {
        // install factories.
        MainService.operationService.registerOperationFactory(new GraphComparatorFactory(this));
        MainService.operationService.registerOperationFactory(new PlainGraphComparatorFactory());

        // create ui components.
        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        infoLabel = new JLabel("If you want to use Comparator you need select tab with matching of two graphs");
        infoLabel.setHorizontalAlignment(JLabel.CENTER);
        pleaseWaitInfoLabel = new JLabel("Please wait...");
        pleaseWaitInfoLabel.setHorizontalAlignment(JLabel.CENTER);

        settingsLabel = new JLabel("Settings:");
        settingsLabel.setFont(ResourceService.SUB_TITLE_FONT);

        vertexAttributesLabel = new JLabel("Vertex attributes:");
        vertexAttributesLabel.setFont(ResourceService.SUB_TITLE_FONT);

        edgeAttributesLabel = new JLabel("Edge attributes:");
        edgeAttributesLabel.setFont(ResourceService.SUB_TITLE_FONT);

        runButton = new JButton("Run");
        cancelButton = new JButton("Cancel");

        instrument = UIUtils.createInstrument(
                UUID.fromString("1fc275e0-34df-11e9-b56e-0800200c9a66"),
                "Comparator",
                UserInterfaceService.UserInterfaceInstrument.WEST_PLACE,
                270,
                e -> MainService.userInterfaceService.selectPanel(GraphComparatorPlugin.this)
        );

        MainService.userInterfaceService.addObserver(new UserInterfaceService.UserInterfaceListener() {
            @Override
            public void onChangeTab(UserInterfaceService.UserInterfaceTab tab) {
                // TODO: need handle duplicates...
                synchronized (generalMutex) {
                    currentGraphComparator = null;
                    if (tab instanceof GraphViewTab graphViewTab) {
                        currentGraphComparator = graphViewTab;
                    }
                    rebuildView();
                }
            }
        }, 20);

        runButton.addActionListener(e -> {
            synchronized (generalMutex) {
                doExecuteComparator(currentGraphComparator);
            }
        });

        cancelButton.addActionListener(e -> {
            synchronized (generalMutex) {
                // TODO: need handle duplicates...
                if (currentGraphComparator == null) {
                    return;
                }

                var entry = graphViewToEntry.get(currentGraphComparator);
                if (entry == null) {
                    return;
                }

                if (entry.getOperation() != null) {
                    entry.getOperation().stop();
                }
            }
        });

        MainService.userInterfaceService.addPanel(this);

        rebuildView();
    }

    @Override
    public UUID getId() {
        return UUID.fromString("5f7c4115-0bcb-42b8-a20e-3d1b9f6e12ab");
    }

    @Override
    public UserInterfaceService.UserInterfaceInstrument getInstrument() {
        return instrument;
    }

    @Override
    public int getPlace() {
        return UserInterfaceService.UserInterfacePanel.WEST_BOTTOM_PLACE;
    }

    @Override
    public JPanel getView() {
        synchronized (generalMutex) {
            return outView;
        }
    }

    @Override
    public void registerNewGraphViewForComparison(GraphViewTab graphViewTab, Graph graph1, Graph graph2) {
        MainService.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                synchronized (generalMutex) {
                    var entry = new GraphComparatorEntry(graphViewTab, graph1, graph2, COMPARISON_VIEW_STATE);

                    Set<String> vertexAttrNames = Sets.newLinkedHashSet();
                    vertexAttrNames.add(GraphComparator.INCOMING_COUNT_EDGE);
                    vertexAttrNames.add(GraphComparator.OUTGOING_COUNT_EDGE);

                    vertexAttrNames.addAll(graph1.getAllVertexAttributeNames(true));
                    vertexAttrNames.addAll(graph2.getAllVertexAttributeNames(true));
                    vertexAttrNames.forEach(vertexAttrName -> {
                        int currValue = 10;
                        if (vertexAttrName.equalsIgnoreCase(GraphComparator.INCOMING_COUNT_EDGE) || vertexAttrName.equalsIgnoreCase(GraphComparator.OUTGOING_COUNT_EDGE)) {
                            currValue = 100;
                        }
                        entry.vertexWeights.add(new SliderSettingAttribute(vertexAttrName, 0, 100, currValue));
                    });

                    var edgeAttrNames = graph1.getAllEdgeAttributeNames(true);
                    edgeAttrNames.addAll(graph2.getAllEdgeAttributeNames(true));
                    edgeAttrNames.forEach(edgeAttrName -> entry.edgeWeights.add(new SliderSettingAttribute(edgeAttrName, 0, 100, 10)));

                    var g1Title = GraphUtils.getGraphName(graph1.getAttributes());
                    var g2Title = GraphUtils.getGraphName(graph2.getAttributes());

                    MainService.userInterfaceService.updateTabTitle(graphViewTab, g1Title + "<->" + g2Title);
                    graphViewTab.setName(g1Title + "<->" + g2Title);

                    graphViewToEntry.put(graphViewTab, entry);
                }
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    rebuildView();
                }
            }
        });
    }

    private void doExecuteComparator(GraphViewTab graphViewTab) {
        if (graphViewTab == null || !graphViewToEntry.containsKey(graphViewTab)) {
            return;
        }

        var entry = graphViewToEntry.get(graphViewTab);

        String selectedItem = "Plain Graph Comparator";

        var operationFactory = MainService.operationService.getOperationFactoryByName(selectedItem);

        var declaredArgs = operationFactory.getDeclaredArgs();

        declaredArgs.put(VGMainGlobals.GRAPH_VIEW_ARG, graphViewTab);
        declaredArgs.put(PlainGraphComparatorFactory.GRAPH1, entry.getGraph1());
        declaredArgs.put(PlainGraphComparatorFactory.GRAPH2, entry.getGraph2());
        declaredArgs.put(PlainGraphComparatorFactory.SHOW_MODE, entry.getShowMode());
        declaredArgs.put(PlainGraphComparatorFactory.VERTEX_BARRIER, entry.getVertexBarrier());
        declaredArgs.put(PlainGraphComparatorFactory.EDGE_BARRIER, entry.getEdgeBarrier());
        declaredArgs.put(PlainGraphComparatorFactory.ATTRIBUTE_WEIGHTS_FOR_VERTICES, entry.getVertexWeights());
        declaredArgs.put(PlainGraphComparatorFactory.ATTRIBUTE_WEIGHTS_FOR_EDGES, entry.getEdgeWeights());

        var operation = operationFactory.buildOperation(declaredArgs);

        entry.setState(COMPARISON_VIEW_IN_PROGRESS_STATE);
        entry.setOperation(operation);

        rebuildView();

        MainService.executorService.execute(new ExecutorService.SwingExecutor() {
            @Override
            public void doInBackground() {
                operation.execute();
            }

            @Override
            public void doInEDT() {
                synchronized (generalMutex) {
                    entry.setState(COMPARISON_VIEW_STATE);
                    entry.setOperation(null);
                    rebuildView();
                }
            }
        });
    }

    private void rebuildView() {
        innerView.removeAll();

        int state = NO_COMPARISON_VIEW_STATE;
        GraphComparatorEntry graphComparatorEntry = null;

        if (currentGraphComparator != null) {
            graphComparatorEntry = graphViewToEntry.get(currentGraphComparator);
            if (graphComparatorEntry != null) {
                state = graphComparatorEntry.getState();
            }
        }

        switch (state) {
            case NO_COMPARISON_VIEW_STATE: {
                innerView.add(infoLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                break;
            }
            case COMPARISON_VIEW_IN_PROGRESS_STATE: {
                innerView.add(pleaseWaitInfoLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                innerView.add(cancelButton, new GridBagConstraints(0, 1, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
                break;
            }
            case COMPARISON_VIEW_STATE: {
                int index = 0;

                JPanel inputPanel = new JPanel(new GridBagLayout());

                // building sliders.
                JPanel inputSlidersPanel = new JPanel(new GridBagLayout());
                JScrollPane inputSlidersScrollPane = new JScrollPane(inputSlidersPanel);

                inputSlidersPanel.add(settingsLabel, new GridBagConstraints(
                        0,
                        index++,
                        1,
                        1,
                        1,
                        0,
                        GridBagConstraints.NORTHWEST,
                        GridBagConstraints.NONE,
                        new Insets(5, 0, 5, 0),
                        0,
                        0));

                inputSlidersPanel.add(
                        UIUtils.generateSettingsPanelViaAttributes(Collections.singletonList(graphComparatorEntry.showMode)),
                        new GridBagConstraints(
                                0,
                                index++,
                                1,
                                1,
                                1,
                                0,
                                GridBagConstraints.NORTH,
                                GridBagConstraints.HORIZONTAL,
                                rowInsets,
                                0,
                                0));

                inputSlidersPanel.add(vertexAttributesLabel, new GridBagConstraints(
                        0,
                        index++,
                        1,
                        1,
                        1,
                        0,
                        GridBagConstraints.WEST,
                        GridBagConstraints.NONE,
                        new Insets(0, 0, 0, 0),
                        0,
                        0));

                var vertexWeightsWithBarrier = new ArrayList<SettingAttribute>();
                vertexWeightsWithBarrier.add(graphComparatorEntry.vertexBarrier);
                vertexWeightsWithBarrier.addAll(graphComparatorEntry.vertexWeights);

                inputSlidersPanel.add(
                        UIUtils.generateSettingsPanelViaAttributes(vertexWeightsWithBarrier),
                        new GridBagConstraints(
                                0,
                                index++,
                                1,
                                1,
                                1,
                                0,
                                GridBagConstraints.NORTH,
                                GridBagConstraints.HORIZONTAL,
                                rowInsets,
                                0,
                                0));

                inputSlidersPanel.add(edgeAttributesLabel, new GridBagConstraints(
                        0,
                        index++,
                        1,
                        1,
                        1,
                        0,
                        GridBagConstraints.NORTHWEST,
                        GridBagConstraints.NONE,
                        new Insets(5, 0, 5, 0),
                        0,
                        0));

                var edgeWeightsWithBarrier = new ArrayList<SettingAttribute>();
                edgeWeightsWithBarrier.add(graphComparatorEntry.edgeBarrier);
                edgeWeightsWithBarrier.addAll(graphComparatorEntry.edgeWeights);

                inputSlidersPanel.add(
                        UIUtils.generateSettingsPanelViaAttributes(edgeWeightsWithBarrier), new GridBagConstraints(
                                0,
                                index++,
                                1,
                                1,
                                1,
                                0,
                                GridBagConstraints.NORTH,
                                GridBagConstraints.HORIZONTAL,
                                rowInsets,
                                0,
                                0));

                inputSlidersPanel.add(runButton, new GridBagConstraints(
                        0,
                        index,
                        1,
                        1,
                        1,
                        1,
                        GridBagConstraints.SOUTH,
                        GridBagConstraints.NONE,
                        new Insets(0, 0, 0, 0),
                        0,
                        0));

                // building input panel.
                inputPanel.add(inputSlidersScrollPane, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

                // building inner view.
                innerView.add(inputPanel, new GridBagConstraints(0, 1, 1, 1, 0.3, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

                break;
            }
        }

        innerView.updateUI();
    }

    private static class GraphComparatorEntry {
        @Getter
        private final GraphViewTab graphViewTab;

        @Getter
        private final Graph graph1;

        @Getter
        private final Graph graph2;

        @Getter @Setter
        private int state;

        @Getter
        private final SettingAttribute showMode = new SelectorSettingAttribute("Show mode", Arrays.asList(GraphComparator.SHOW_ALL_MODE_STR, GraphComparator.SHOW_DIFFERENCE_MODE_STR), "Show all");

        @Getter
        private final SettingAttribute vertexBarrier = new SliderSettingAttribute("Vertex barrier", 0, 100, 80);

        @Getter
        private final SettingAttribute edgeBarrier = new SliderSettingAttribute("Edge barrier", 0, 100, 80);

        @Getter
        private final List<SettingAttribute> vertexWeights = new ArrayList<>();

        @Getter
        private final List<SettingAttribute> edgeWeights = new ArrayList<>();

        @Getter @Setter
        private OperationService.BaseProcedure operation;

        public GraphComparatorEntry(GraphViewTab graphViewTab, Graph graph1, Graph graph2, int state) {
            this.graphViewTab = graphViewTab;
            this.graph1 = graph1;
            this.graph2 = graph2;
            this.state = state;
        }
    }
}
