package vg.plugin.gcomparator;

import vg.lib.model.graph.Graph;
import vg.service.main.MainService;
import vg.service.operation.OperationService;
import vg.shared.graph.utils.GraphUtils;

import java.util.Arrays;
import java.util.Map;

public class GraphComparatorFactory extends OperationService.OperationFactory {
    private static final String FIRST_GRAPH = "GRAPH1";
    private static final String SECOND_GRAPH = "GRAPH2";

    private final GraphComparatorPluginCallback callback;

    public GraphComparatorFactory(GraphComparatorPluginCallback callback) {
        this.callback = callback;
        modifyDeclaredArg(FIRST_GRAPH, null);
        modifyDeclaredArg(SECOND_GRAPH, null);
    }

    @Override
    public OperationService.BaseProcedure buildOperation(Map<String, Object> args) {
        return new GraphComparatorProcedure(callback, args);
    }

    @Override
    public String getName() {
        return "Graph Comparator";
    }

    private static class GraphComparatorProcedure extends OperationService.BaseProcedure {
        private final GraphComparatorPluginCallback callback;

        private GraphComparatorProcedure(GraphComparatorPluginCallback callback, Map<String, Object> args) {
            this.callback = callback;
            this.args = args;
        }

        @Override
        public void execute() {
            var graph1 = (Graph) args.get(FIRST_GRAPH);
            var graph2 = (Graph) args.get(SECOND_GRAPH);

            GraphUtils.downloadChildren(graph1);
            GraphUtils.downloadChildren(graph2);

            MainService.graphViewService.asyncOpenGraphInTab(
                    Arrays.asList(graph1, graph2),
                    graphView -> callback.registerNewGraphViewForComparison(graphView, graph1, graph2));
        }
    }
}
