package vg.shared.utils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CollectionUtils {
    public static <T> List<T> checkedFilter(Class<T> clazz, Collection<?> items) {
        return items.stream()
                .filter(clazz::isInstance)
                .map(clazz::cast)
                .collect(Collectors.toList());
    }
}
