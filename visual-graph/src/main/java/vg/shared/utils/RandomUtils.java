package vg.shared.utils;

import java.util.Random;
import java.util.UUID;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class RandomUtils {
    // Constants
    private static final char[] englishAlphabet =  "abcdefghijklmnopqrstuvwxyz".toCharArray();

    // Main data
    private static final Random random = new Random();

    /**
     * Returns a psuedo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimim value
     * @param max Maximim value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public static int randInt(int min, int max) {
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = random.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static String generateEnglishRandomWord(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char c = englishAlphabet[random.nextInt(englishAlphabet.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static UUID generateRandomUUID() {
        return UUID.randomUUID();
    }
}
