package vg.shared.utils;

public class ArrayUtils {
    public static Float[][] toObject(float[][] array) {
        Float[][] result = new Float[array.length][];

        for (int i = 0; i < array.length; i++) {
            result[i] = org.apache.commons.lang3.ArrayUtils.toObject(array[i]);
        }

        return result;
    }

    public static float mul(float[] row1, float[] row2) {
        float count = 0;
        for (int i = 0; i < row1.length; i++) {
            count += row2[i] * row1[i];
        }
        return count;
    }

    public static float mul(float row1K, float[] row1, float row2K, float[] row2) {
        float count = 0;
        for (int i = 0; i < row1.length; i++) {
            count += row1K * row2K * row2[i] * row1[i];
        }
        return count;
    }

    /**
     * Adds row2 to row1.
     */
    public static void add(int[] row1, int[] row2) {
        for (int i = 0; i < row1.length; i++) {
            row1[i] = row1[i] + row2[i];
        }
    }

    /**
     * Adds row2 to row1.
     */
    public static void add(long[] row1, long[] row2) {
        for (int i = 0; i < row1.length; i++) {
            row1[i] = row1[i] + row2[i];
        }
    }

    // TODO: возможно необходимо более аккуратно составить данную функцию.
    public static void calcRow(float[] row, int rowLevel, float[] from, int[] indexToLevel, boolean useK) {
        for (int i = 0; i < from.length; i++) {
            float k = 1.0f;

            if (useK) {
                k = 1.0f / (Math.abs(indexToLevel[i] - rowLevel) + 1);
                if (Math.abs(indexToLevel[i] - rowLevel) > 100) {
                    k = 0;
                }
            }

            row[i] = row[i] + from[i] * k;
        }
    }

    public static float count(float[] row) {
        float count = 0;
        for (int i = 0; i < row.length; i++) {
            count += row[i];
        }
        return count;
    }

    public static float[][] mergeMatrices(float[][] matrix1, float k1, float[][] matrix2, float k2) {
        float[][] matrix = new float[matrix1.length][matrix1.length];
        for (int i = 0; i < matrix1.length; i++) {
            for (int j = 0; j < matrix1.length; j++) {
                matrix[i][j] = k1 * matrix1[i][j] + k2 * matrix2[i][j];
            }
        }
        return matrix;
    }
}
