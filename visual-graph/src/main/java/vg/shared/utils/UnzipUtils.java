package vg.shared.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipUtils {
	public static void unzip(InputStream zippedInputStream, File outputFolder) throws IOException {
		byte[] buffer = new byte[1024];

        // create output directory is not exists
        if (!outputFolder.exists() && !outputFolder.mkdir()) {
            throw new IOException("Can't create directory: " + outputFolder);
        }

        try (ZipInputStream zis = new ZipInputStream(zippedInputStream)) {
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {

                String fileName = ze.getName();
                File newFile = new File(outputFolder + File.separator + fileName);

                System.out.println("file unzip : " + newFile.getAbsoluteFile());

                File parentDir = new File(newFile.getParent());
                if (!parentDir.exists() && !parentDir.mkdirs()) {
                    throw new IOException("Can't create directory: " + newFile.getParent());
                }

                if (!ze.isDirectory()) {
                    try (FileOutputStream fos = new FileOutputStream(newFile)) {
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                    }
                }

                ze = zis.getNextEntry();
            }
        }
	}

    public static String getRootDir(File zipFile) throws IOException {
        try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile))) {
            ZipEntry ze = zis.getNextEntry();

            while (ze != null) {
                if (ze.isDirectory())
                    return ze.getName();

                ze = zis.getNextEntry();
            }
        }
        return null;
    }
}
