package vg.shared.utils;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import vg.service.main.MainService;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class XmlUtils {
    public static int getIntContentFromElementByTag(Element eElement, String tag) {
        return getIntContentFromElementByTag(eElement, tag, 0);
    }

    public static int getIntContentFromElementByTag(Element eElement, String tag, int defaultIntegerContent) {
        try {
            return Integer.parseInt(getTextContentFromElementByTag(eElement, tag, "0"));
        } catch (Exception ex) {
            MainService.logger.printDebug(ex.getMessage(), ex);
        }

        return defaultIntegerContent;
    }

    public static String getTextContentFromElementByTag(Element eElement, String tag) {
        return getTextContentFromElementByTag(eElement, tag, "");
    }

    public static String getTextContentFromElementByTag(Element eElement, String tag, String defaultTextContent) {
        NodeList nodeList = eElement.getElementsByTagName(tag);

        if (nodeList != null && nodeList.getLength() > 0)
            return nodeList.item(0).getTextContent();

        return defaultTextContent;
    }

    public static String simpleFormatXml(String unformattedXml, int indent, int lineWidth) {
        try {
            final Document document = parseXml(unformattedXml);

            OutputFormat format = new OutputFormat(document);
            format.setLineWidth(lineWidth);
            format.setIndenting(true);
            format.setIndent(indent);
            Writer out = new StringWriter();
            XMLSerializer serializer = new XMLSerializer(out, format);
            serializer.serialize(document);
            return out.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Document parseXml(String xml) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(xml));
            return db.parse(is);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
