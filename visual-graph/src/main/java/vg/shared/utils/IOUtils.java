package vg.shared.utils;

import java.util.Arrays;

/**
 * @author Timur Zolotuhin (e-mail: tzolotuhin@gmail.com)
 */
public class IOUtils {
    private static final String NEW_LINE_SEPARATOR = System.getProperty("line.separator");

    public static String getNewLineSeparator() {
        return NEW_LINE_SEPARATOR;
    }

    public static String tableToString(int[][] table, String template, String replacement) {
        return "\n" + Arrays.deepToString(table).replace("],", "\n").replace(",", "  ")
                .replaceAll("[\\[\\]]", " ").replaceAll(template, replacement);
    }
}
