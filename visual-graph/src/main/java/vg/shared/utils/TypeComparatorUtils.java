package vg.shared.utils;

import org.apache.commons.lang3.StringUtils;

public class TypeComparatorUtils {
    // Returns result in range from 0 to 1
    public static float flexibleDoubleComparison(double value1, double value2) {
        if (value1 < 0 && value2 < 0) {
            value1 = -value1;
            value2 = -value2;
        }

        if (value1 < 0 && value2 > 0) {
            value1 = -value1;
            value2 = value1 + value2;
        }

        if (value2 < 0 && value1 > 0) {
            value2 = -value2;
            value1 = value1 + value2;
        }

        // not zero
        value1++;
        value2++;


        float diff;
        if (value1 > value2)
            diff = (float) (value2 / value1);
        else
            diff = (float) (value1 / value2);

        if (diff > 1) diff = 1;
        if (diff < 0) diff = 0;

        return diff;
    }

    // Returns result in range from 0 to 1.
    public static float flexibleStringComparison(String value1, String value2) {
        if (value1 == null && value2 == null) {
            return 1.0f;
        }

        if (value1 == null || value2 == null) {
            return 0.0f;
        }

        if (value1.length() == 0 || value2.length() == 0) {
            return 1.0f;
        }

        byte[] bytes1 = value1.getBytes();
        byte[] bytes2 = value2.getBytes();

        if (bytes1.length > bytes2.length) {
            var tmp = bytes2;
            bytes2 = bytes1;
            bytes1 = tmp;
        }

        int i;
        for (i = 0; i < bytes1.length; i++) {
            if (bytes1[i] != bytes2[i]) {
                break;
            }
        }

        return (2 * i) / (float) (bytes1.length + bytes2.length);
    }

    // Returns result which equals 0 or 1.
    public static float strongDoubleComparison(double value1, double value2) {
        if (Double.compare(value1, value2) == 0)
            return 1.0f;
        return 0.0f;
    }

    // Returns result which equals 0 or 1.
    public static float strongStringComparison(String value1, String value2) {
        return StringUtils.equals(value1, value2) ? 1.0f : 0.0f;
    }
}
