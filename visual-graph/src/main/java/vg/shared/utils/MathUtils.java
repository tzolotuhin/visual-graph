package vg.shared.utils;

public class MathUtils {
    public static long multiplyExactOrReturnDefaultValue(long a, long b, long defaultValue) {
        try {
            return Math.multiplyExact(a, b);
        } catch (ArithmeticException ex) {
            return defaultValue;
        }
    }
}
