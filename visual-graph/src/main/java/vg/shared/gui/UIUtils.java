package vg.shared.gui;

import vg.service.ui.UserInterfaceService;
import vg.shared.gui.components.CompoundIcon;
import vg.shared.gui.components.RotatedIcon;
import vg.shared.gui.components.TextFieldDocumentAdapter;
import vg.shared.gui.components.TextFieldIntFilter;
import vg.shared.gui.components.TextIcon;
import vg.shared.gui.data.CheckSettingAttribute;
import vg.shared.gui.data.SelectorSettingAttribute;
import vg.shared.gui.data.SettingAttribute;
import vg.shared.gui.data.SliderSettingAttribute;
import vg.shared.gui.data.ValueSettingAttribute;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.text.PlainDocument;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UIUtils {
    public static UserInterfaceService.UserInterfaceInstrument createInstrument(UUID id, String title, int place, int angle, ActionListener actionListener) {
        JToggleButton componentButton = new JToggleButton();
        componentButton.setFocusPainted(false);

        TextIcon ti = new TextIcon(componentButton, title);
        ti.setFont(UserInterfaceService.INSTRUMENT_PANEL_FONT);
        ImageIcon ii = new ImageIcon();
        CompoundIcon ci = new CompoundIcon(ii, ti);
        RotatedIcon ri = new RotatedIcon(ci, angle);
        componentButton.setIcon(ri);

        componentButton.addActionListener(actionListener);

        JPanel componentButtonOutView = new JPanel(new GridBagLayout());
        componentButtonOutView.add(
                componentButton,
                new GridBagConstraints(0,0, 1,1, 1,1,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH,
                        new Insets(0,0,0,0), 0,0));

        return new UserInterfaceService.UserInterfaceInstrument() {
            @Override
            public UUID getId() {
                return id;
            }

            @Override
            public int getPlace() {
                return place;
            }

            @Override
            public JPanel getView() {
                return componentButtonOutView;
            }

            @Override
            public void setSelected(boolean state) {
                componentButton.setSelected(state);
            }
        };
    }

    public static JPanel generateSettingsPanelViaAttributes(List<SettingAttribute> attributes) {
        var cellInsets = new Insets(1, 0, 2, 0);

        var generalSettingsPanel = new JPanel(new GridBagLayout());

        if (attributes == null || attributes.isEmpty()) {
            return generalSettingsPanel;
        }

        int index = 0;
        for (var attribute : attributes) {
            var nameLabel = new JLabel(attribute.getName());
            generalSettingsPanel.add(nameLabel, new GridBagConstraints(
                    0,
                    index,
                    1,
                    1,
                    1,
                    0,
                    GridBagConstraints.NORTHWEST,
                    GridBagConstraints.HORIZONTAL,
                    cellInsets,
                    0,
                    0));

            JComponent valueField;
            if (attribute instanceof SelectorSettingAttribute) {
                var selectorSettingAttribute = (SelectorSettingAttribute)attribute;
                JComboBox<String> valueComboBox = new JComboBox<>(selectorSettingAttribute.getItems().toArray(new String[0]));
                valueComboBox.setSelectedItem(selectorSettingAttribute.getSelectedItem());

                valueComboBox.addActionListener(e -> {
                    var selectedItem = Objects.requireNonNull(valueComboBox.getSelectedItem()).toString();
                    selectorSettingAttribute.setSelectedItem(selectedItem);
                });

                valueField = valueComboBox;
            } else if (attribute instanceof CheckSettingAttribute) {
                var checkSettingAttribute = (CheckSettingAttribute)attribute;
                var checkBox = new JCheckBox("", checkSettingAttribute.isCurrValue());

                checkBox.addActionListener(e -> {
                    checkSettingAttribute.setCurrValue(checkBox.isSelected());
                });

                valueField = checkBox;
            } else if (attribute instanceof SliderSettingAttribute) {
                var sliderSettingAttribute = (SliderSettingAttribute)attribute;

                var valueSlider = new JSlider(JSlider.HORIZONTAL, sliderSettingAttribute.getMin(), sliderSettingAttribute.getMax(), sliderSettingAttribute.getCurrValue());
                valueSlider.setMajorTickSpacing(25);
                valueSlider.setMinorTickSpacing(5);

                valueSlider.addChangeListener(e -> sliderSettingAttribute.setCurrValue(valueSlider.getValue()));

                valueField = valueSlider;
            } else if (attribute instanceof ValueSettingAttribute) {
                var valueSettingAttribute = (ValueSettingAttribute)attribute;
                JTextField textField = new JTextField(Integer.toString(valueSettingAttribute.getCurrValue()));
                ((PlainDocument)textField.getDocument()).setDocumentFilter(new TextFieldIntFilter());

                textField.getDocument().addDocumentListener((TextFieldDocumentAdapter) e -> {
                    String text = textField.getText();
                    try {
                        int value = Integer.parseInt(text);
                        valueSettingAttribute.setCurrValue(value);
                    } catch (NumberFormatException ex) {
                        // do nothing...
                    }
                });

                valueField = textField;
            } else {
                throw new IllegalArgumentException(String.format("Class '%s' is not support.", attribute.getClass()));
            }

            generalSettingsPanel.add(valueField, new GridBagConstraints(
                    1,
                    index,
                    1,
                    1,
                    1,
                    0,
                    GridBagConstraints.NORTHWEST,
                    GridBagConstraints.HORIZONTAL,
                    cellInsets,
                    0,
                    0));

            index++;
        }

        return generalSettingsPanel;
    }
}
