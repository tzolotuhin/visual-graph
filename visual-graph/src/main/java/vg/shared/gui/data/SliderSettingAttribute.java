package vg.shared.gui.data;

import lombok.Getter;
import lombok.Setter;

public class SliderSettingAttribute extends ValueSettingAttribute {
    @Getter
    private final int min;
    @Getter
    private final int max;
    @Getter @Setter
    private int currValue;

    public SliderSettingAttribute(String name, int min, int max, int currValue) {
        super(name, currValue);
        this.min = min;
        this.max = max;
        this.currValue = currValue;
    }

    public float normalizeCurrValue() {
        return currValue / (float)max;
    }
}
