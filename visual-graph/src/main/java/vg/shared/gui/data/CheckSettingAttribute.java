package vg.shared.gui.data;

import lombok.Getter;
import lombok.Setter;

public class CheckSettingAttribute extends SettingAttribute {
    @Getter @Setter
    private boolean currValue;

    public CheckSettingAttribute(String name, boolean currValue) {
        super(name);
        this.currValue = currValue;
    }
}
