package vg.shared.gui.data;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SelectorSettingAttribute extends SettingAttribute {
    @Getter
    private final List<String> items;

    @Getter @Setter
    private String selectedItem;

    public SelectorSettingAttribute(String name, Collection<String> items, String selectedItem) {
        super(name);
        this.selectedItem = selectedItem;
        this.items = new ArrayList<>(items);
    }
}
