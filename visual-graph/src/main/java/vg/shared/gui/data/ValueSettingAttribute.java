package vg.shared.gui.data;

import lombok.Getter;
import lombok.Setter;

public class ValueSettingAttribute extends SettingAttribute {
    @Getter @Setter
    private int currValue;

    public ValueSettingAttribute(String name, int currValue) {
        super(name);
        this.currValue = currValue;
    }
}
