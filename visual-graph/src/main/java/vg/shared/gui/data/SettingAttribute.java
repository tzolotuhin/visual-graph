package vg.shared.gui.data;

import lombok.Getter;

public class SettingAttribute {
    @Getter
    private final String name;

    public SettingAttribute(String name) {
        this.name = name;
    }
}
