package vg.shared.gui.components;

import javax.swing.*;
import java.awt.*;

public class SimpleStatusBar extends JPanel {
    // Panels.
    private final JPanel leftPanel;
    private final JLabel tip;

    // Defines.
    private final static Dimension DEF_PREFERRED_SIZE = new Dimension(10, 20);

    public SimpleStatusBar() {
        setLayout(new BorderLayout());
        setPreferredSize(DEF_PREFERRED_SIZE);

        leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.X_AXIS));
        tip = new JLabel();
        leftPanel.add(tip);
        leftPanel.add(Box.createHorizontalGlue());

        JPanel rightPanel = new JPanel(new BorderLayout());
        rightPanel.add(new JLabel(new AngledLinesWindowsCornerIcon()),
                BorderLayout.SOUTH);
        rightPanel.setOpaque(false);
        leftPanel.setOpaque(false);
        add(leftPanel, BorderLayout.CENTER);

        add(rightPanel, BorderLayout.EAST);
        setBackground(SystemColor.control);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        int y = 0;
        g.setColor(new Color(156, 154, 140));
        g.drawLine(0, y, getWidth(), y);
        y++;
        g.setColor(new Color(196, 194, 183));
        g.drawLine(0, y, getWidth(), y);
        y++;
        g.setColor(new Color(218, 215, 201));
        g.drawLine(0, y, getWidth(), y);
        y++;
        g.setColor(new Color(233, 231, 217));
        g.drawLine(0, y, getWidth(), y);

        y = getHeight() - 3;
        g.setColor(new Color(233, 232, 218));
        g.drawLine(0, y, getWidth(), y);
        y++;
        g.setColor(new Color(233, 231, 216));
        g.drawLine(0, y, getWidth(), y);
        y = getHeight() - 1;
        g.setColor(new Color(221, 221, 220));
        g.drawLine(0, y, getWidth(), y);
    }

    public Component add(Component component) {
        return leftPanel.add(component);
    }
}
