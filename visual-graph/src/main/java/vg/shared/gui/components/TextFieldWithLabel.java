package vg.shared.gui.components;

import javax.swing.*;
import java.awt.*;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class TextFieldWithLabel {
    // Constants
    public static final int WIDE_ALIGNMENT = 1;
    public static final int LEFT_ALIGNMENT = 2;

    // Main components
    private JPanel outView, innerView;

    private JLabel label;
    private JTextField textField;

    private int alignment;

    public TextFieldWithLabel(String labelText) {
        this(labelText, "");
    }

    public TextFieldWithLabel(String labelText, int alignment) {
        this(labelText, "", alignment);
    }

    public TextFieldWithLabel(String labelText, String text) {
        this(labelText, text, LEFT_ALIGNMENT);
    }

    public TextFieldWithLabel(String labelText, String text, int alignment) {
        this.alignment = alignment;

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        label = new JLabel(labelText);
        textField = new JTextField(text);

        rebuildView();
    }

    public JLabel getLabel() {
        return label;
    }

    public void setLabel(JLabel label) {
        this.label = label;
    }

    public JTextField getTextField() {
        return textField;
    }

    public void setTextField(JTextField textField) {
        this.textField = textField;
    }

    public JPanel getView() {
        return outView;
    }

    public void setEnabled(boolean enabled) {
        label.setEnabled(enabled);
        textField.setEnabled(enabled);
    }

//==============================================================================
//---------------PRIVATE METHODS------------------------------------------------
    protected void rebuildView() {
        innerView.removeAll();

        innerView.add(label, new GridBagConstraints(0,0,  1,1,  0,0,  GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0),  0,0));

        switch (alignment) {
            case LEFT_ALIGNMENT:
                innerView.add(textField, new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
                break;
            case WIDE_ALIGNMENT:
                innerView.add(textField, new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
                textField.setPreferredSize(new Dimension(120, textField.getPreferredSize().height));
                break;
        }

        innerView.updateUI();
    }
}
