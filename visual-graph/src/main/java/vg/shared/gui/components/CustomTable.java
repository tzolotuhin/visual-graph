package vg.shared.gui.components;

import org.apache.commons.lang3.ArrayUtils;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.util.AbstractMap;
import java.util.Map;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CustomTable {
    public static Map.Entry<JTable, JScrollPane> buildCustomTable(Object[][] data, String[] columnHeaders, String[] rowHeaders) {
        columnHeaders = (String[])ArrayUtils.add(columnHeaders, 0, "");

        TableModel tm = new CustomTableModel(data, columnHeaders, rowHeaders);
        TableColumnModel cm = new CustomTableColumnModel();
        TableColumnModel rm = new CustomTableRowModel();

        JTable jt = new JTable(tm, cm);
        JTable headerColumn = new JTable(tm, rm);
        jt.createDefaultColumnsFromModel();
        headerColumn.createDefaultColumnsFromModel();

        jt.setSelectionModel(headerColumn.getSelectionModel());

        headerColumn.setBackground(Color.lightGray);
        headerColumn.setColumnSelectionAllowed(false);
        headerColumn.setCellSelectionEnabled(false);

        JViewport jv = new JViewport();
        jv.setView(headerColumn);
        jv.setPreferredSize(headerColumn.getMaximumSize());

        jt.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        JScrollPane jsp = new JScrollPane(jt);
        jsp.setRowHeader(jv);
        jsp.setCorner(ScrollPaneConstants.UPPER_LEFT_CORNER, headerColumn.getTableHeader());

        return new AbstractMap.SimpleEntry<>(jt, jsp);
    }

    public static class CustomTableModel extends AbstractTableModel {
        private Object[][] data;
        private String[] columnHeaders;
        private String[] rowHeaders;

        public CustomTableModel(Object[][] data, String[] columnHeaders, String[] rowHeaders) {
            this.data = data;
            this.columnHeaders = columnHeaders;
            this.rowHeaders = rowHeaders;
        }

        public int getColumnCount() {
            return data.length + 1;
        }

        public int getRowCount() {
            return rowHeaders.length;
        }

        public String getColumnName(int col) {
            return columnHeaders[col];
        }

        public Object getValueAt(int row, int col) {
            if (col == 0)
                return rowHeaders[row];
            return data[col - 1][row];
        }
    }

    public static class CustomTableColumnModel extends DefaultTableColumnModel {
        private boolean first = true;

        public void addColumn(TableColumn tc) {
            if (first) {
                first = false;
                return;
            }
            tc.setMinWidth(25);
            super.addColumn(tc);
        }
    }

    public static class CustomTableRowModel extends DefaultTableColumnModel {
        private boolean first = true;

        public void addColumn(TableColumn tc) {
            if (first) {
                tc.setMaxWidth(tc.getPreferredWidth());
                super.addColumn(tc);
                first = false;
            }
        }
    }

}
