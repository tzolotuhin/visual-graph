package vg.shared.gui.components;

import lombok.Setter;
import vg.service.main.MainService;
import vg.service.resource.ResourceService;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;

public class SearchBarPanel {
    // Components
    private final JPanel innerView, outView;

    private final JTextField strField;
    private final JButton prevButton;
    private final JButton nextButton;

    private final JToggleButton searchAmongVerticesButton;
    private final JToggleButton searchAmongEdgesButton;
    private final JToggleButton matchCaseButton;
    private final JToggleButton wordsButton;
    private final JToggleButton regexButton;

    private final JLabel resultLabel;

    // Data
    @Setter
    private int currIndex = -1;
    @Setter
    private int amount = 0;
    private final boolean showSearchAmongVerticesButton;
    private final boolean showSearchAmongEdgesButton;
    private final boolean showMatchCaseButton;
    private final boolean showWordsButton;
    private final boolean showRegexButton;
    private final boolean showPrevAndNextButtons;

    public SearchBarPanel(
            boolean showSearchAmongVerticesButton,
            boolean showSearchAmongEdgesButton,
            boolean showMatchCaseButton,
            boolean showWordsButton,
            boolean showRegexButton,
            boolean showPrevAndNextButtons) {
        this.showSearchAmongVerticesButton = showSearchAmongVerticesButton;
        this.showSearchAmongEdgesButton = showSearchAmongEdgesButton;
        this.showMatchCaseButton = showMatchCaseButton;
        this.showWordsButton = showWordsButton;
        this.showRegexButton = showRegexButton;
        this.showPrevAndNextButtons = showPrevAndNextButtons;

        outView = new JPanel(new GridLayout(1, 1));
        innerView = new JPanel(new GridBagLayout());
        outView.add(innerView);

        strField = new JTextField();
        prevButton = new JButton(MainService.resourceService.getImageIcon(ResourceService.ARROW_TOP_IMAGE));
        nextButton = new JButton(MainService.resourceService.getImageIcon(ResourceService.ARROW_BOTTOM_IMAGE));

        searchAmongVerticesButton = new JToggleButton(MainService.resourceService.getImageIcon(ResourceService.VERTEX_BOTTOM_IMAGE));
        searchAmongVerticesButton.setFocusPainted(false);
        searchAmongVerticesButton.setToolTipText("Search among the vertices");
        searchAmongVerticesButton.setSelected(true);
        searchAmongEdgesButton = new JToggleButton(MainService.resourceService.getImageIcon(ResourceService.EDGE_BOTTOM_IMAGE));
        searchAmongEdgesButton.setFocusPainted(false);
        searchAmongEdgesButton.setToolTipText("Search among the edges");
        searchAmongEdgesButton.setSelected(true);
        matchCaseButton = new JToggleButton(MainService.resourceService.getImageIcon(ResourceService.MATCH_CASE_BOTTOM_IMAGE));
        matchCaseButton.setFocusPainted(false);
        matchCaseButton.setToolTipText("Match Case");
        wordsButton = new JToggleButton(MainService.resourceService.getImageIcon(ResourceService.WORDS_BOTTOM_IMAGE));
        wordsButton.setFocusPainted(false);
        wordsButton.setToolTipText("Words");
        regexButton = new JToggleButton(MainService.resourceService.getImageIcon(ResourceService.REGEX_BOTTOM_IMAGE));
        regexButton.setFocusPainted(false);
        regexButton.setToolTipText("Regex");

        resultLabel = new JLabel();

        rebuild();
    }

    public String getStr() {
        return strField.getText();
    }

    public boolean isSearchAmongVerticesSelected() {
        return searchAmongVerticesButton.isSelected();
    }

    public boolean isSearchAmongEdgesSelected() {
        return searchAmongEdgesButton.isSelected();
    }

    public boolean isMatchCaseSelected() {
        return matchCaseButton.isSelected();
    }

    public boolean isWordsSelected() {
        return wordsButton.isSelected();
    }

    public boolean isRegexSelected() {
        return regexButton.isSelected();
    }

    public void addActionListenerForSearchBarStrField(GraphViewSearchBarPanelListener l) {
        strField.addActionListener(e -> l.update());
        strField.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent e) {
                l.update();
            }
            public void removeUpdate(DocumentEvent e) {
                l.update();
            }
            public void insertUpdate(DocumentEvent e) {
                l.update();
            }
        });
        matchCaseButton.addActionListener(e -> l.update());
        wordsButton.addActionListener(e -> l.update());
        regexButton.addActionListener(e -> l.update());
        searchAmongVerticesButton.addActionListener(e -> l.update());
        searchAmongEdgesButton.addActionListener(e -> l.update());
    }

    public void addActionListenerForSearchBarNextButton(ActionListener l) {
        nextButton.addActionListener(l);
    }

    public void addActionListenerForSearchBarPrevButton(ActionListener l) {
        prevButton.addActionListener(l);
    }

    public void updateUI() {
        if (amount == 0) {
            resultLabel.setText("0 results");
        } else if (currIndex < 0){
            resultLabel.setText(String.format("%s results", amount));
        } else {
            resultLabel.setText(String.format("%s/%s", currIndex + 1, amount));
        }

        innerView.updateUI();
    }

    public void rebuild() {
        innerView.removeAll();

        int index = 0;

        var defaultInsets = new Insets(2, 2, 2, 0);

        innerView.add(
                strField,
                new GridBagConstraints(
                        index++,
                        0,
                        1,
                        1,
                        1.4f,
                        0f,
                        GridBagConstraints.CENTER,
                        GridBagConstraints.HORIZONTAL,
                        defaultInsets,
                        0,
                        0));

        if (showSearchAmongVerticesButton) {
            innerView.add(
                    searchAmongVerticesButton,
                    new GridBagConstraints(
                            index++,
                            0,
                            1,
                            1,
                            0,
                            0,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.NONE,
                            defaultInsets,
                            0,
                            0));
        }

        if (showSearchAmongEdgesButton) {
            innerView.add(
                    searchAmongEdgesButton,
                    new GridBagConstraints(
                            index++,
                            0,
                            1,
                            1,
                            0,
                            0,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.NONE,
                            defaultInsets,
                            0,
                            0));
        }

        if (showMatchCaseButton) {
            innerView.add(
                    matchCaseButton,
                    new GridBagConstraints(
                            index++,
                            0,
                            1,
                            1,
                            0,
                            0,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.NONE,
                            defaultInsets,
                            0,
                            0));
        }

        if (showWordsButton) {
            innerView.add(
                    wordsButton,
                    new GridBagConstraints(
                            index++,
                            0,
                            1,
                            1,
                            0,
                            0,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.NONE,
                            defaultInsets,
                            0,
                            0));
        }

        if (showRegexButton) {
            innerView.add(
                    regexButton,
                    new GridBagConstraints(
                            index++,
                            0,
                            1,
                            1,
                            0,
                            0,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.NONE,
                            defaultInsets,
                            0,
                            0));
        }

        if (showPrevAndNextButtons) {
            innerView.add(
                    prevButton,
                    new GridBagConstraints(
                            index++,
                            0,
                            1,
                            1,
                            0,
                            0,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.NONE,
                            defaultInsets,
                            0,
                            0));

            innerView.add(
                    nextButton,
                    new GridBagConstraints(
                            index++,
                            0,
                            1,
                            1,
                            0,
                            0,
                            GridBagConstraints.CENTER,
                            GridBagConstraints.NONE,
                            defaultInsets,
                            0,
                            0));
        }

        innerView.add(
                resultLabel,
                new GridBagConstraints(
                        index,
                        0,
                        1,
                        1,
                        0.6f,
                        0,
                        GridBagConstraints.WEST,
                        GridBagConstraints.HORIZONTAL,
                        defaultInsets,
                        0,
                        0));

        updateUI();
    }

    public JPanel getView() {
        return outView;
    }

    public interface GraphViewSearchBarPanelListener {
        void update();
    }
}
