package vg.shared.file_utils;

import vg.service.main.MainService;

import java.io.*;

public class CopyFile {
	public static boolean copyFile(String srcFile, String dstFile) {
		InputStream in = null;
		OutputStream out = null;
		try {
			File f1 = new File(srcFile);
			File f2 = new File(dstFile);
			in = new FileInputStream(f1);

			out = new FileOutputStream(f2);

			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			return true;
		} catch (Exception ex) {
			MainService.logger.printException(ex);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception ex) {}
			}
			if (out != null) {
				try {
					out.close();
				} catch (Exception ex) {}
			}				
		}
		return false;
	}
}
