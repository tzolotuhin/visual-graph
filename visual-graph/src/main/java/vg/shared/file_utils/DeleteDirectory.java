package vg.shared.file_utils;

import vg.service.main.MainService;

import java.io.File;
import java.io.IOException;

public class DeleteDirectory {
	public static boolean deleteDirectory(String srcFolder) {
		File directory = new File(srcFolder);

		// make sure directory exists
		if (!directory.exists()) {
			return false;
		} else {
			try {
				delete(directory);
			} catch (IOException ex) {
				MainService.logger.printException(ex);
				return false;
			}
		}
		return true;
	}

	private static void delete(File file) throws IOException {

		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

				file.delete();
				System.out.println("Directory is deleted : "
						+ file.getAbsolutePath());

			} else {

				// list all the directory contents
				String files[] = file.list();

				for (String temp : files) {
					// construct the file structure
					File fileDelete = new File(file, temp);

					// recursive delete
					delete(fileDelete);
				}

				// check the directory again, if empty then delete it
				if (file.list().length == 0) {
					file.delete();
					System.out.println("Directory is deleted : "
							+ file.getAbsolutePath());
				}
			}

		} else {
			// if file, then delete it
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}
}
