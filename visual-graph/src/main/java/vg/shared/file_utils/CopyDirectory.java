package vg.shared.file_utils;

import vg.service.main.MainService;

import java.io.*;

public class CopyDirectory {
	public static boolean copyDirectory(String srcDir, String dstDir) {
		if (srcDir == null || dstDir == null) return false;
		
		File srcFolder = new File(srcDir);
		File destFolder = new File(dstDir);

		// make sure source exists
		if (!srcFolder.exists()) {
			return false;
		} else {
			try {
				copyFolder(srcFolder, destFolder);
			} catch (IOException ex) {
				MainService.logger.printException(ex);
				return false;
			}
		}
		return true;
	}
	
	private static void copyFolder(File src, File dest) throws IOException {
		if (src.isDirectory()) {

			// if directory not exists, create it
			if (!dest.exists()) {
				dest.mkdir();
				System.out.println("Directory copied from " + src + "  to "
						+ dest);
			}

			// list all the directory contents
			String files[] = src.list();

			for (String file : files) {
				// construct the src and dest file structure
				File srcFile = new File(src, file);
				File destFile = new File(dest, file);
				// recursive copy
				copyFolder(srcFile, destFile);
			}

		} else {
			// if file, then copy it
			// Use bytes stream to support all file types
			InputStream in = new FileInputStream(src);
			OutputStream out = new FileOutputStream(dest);

			byte[] buffer = new byte[1024];

			int length;
			// copy the file content in bytes
			while ((length = in.read(buffer)) > 0) {
				out.write(buffer, 0, length);
			}

			in.close();
			out.close();
		}
	}
}
