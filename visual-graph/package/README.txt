Thanks for using Visual Graph ;)

It's short description of the system, if you want more information about the system, please read manual.
================================================================================

Visual Graph is system for drawing hierarchical attributed graphs on the plane and searching different information in it.
For that user may use wide set of tools: navigator, attribute manager, mini map, graph viewer, graph matching, path searcher,
cycle searcher and other.

Visual Graph has wide set of layout for graphs: hierarchical layout (main layout for the program), circle layout, organic layout,
fast organic layout, layered layout and other.

Key application area: graphs in the compiler (control flow graph, syntax trees, call graphs).

The program supports the following popular graph data formats: dot (gz), graphml, gml.

Analogs: yEd, aiSee, Cytoscape, Gephi

================================================================================