#include <stdio.h>
#include <stdlib.h>
#include <string.h>
 
typedef struct record
{
    char *data;
    int hash;
    int hitcount;
} record;
 
typedef struct cache
{
    record  *record;
    int size;
} cache;
 
int min(cache *cch)
{
    int hit = cch->record[0].hitcount;
    int minimum = 0;
    for (int i = 0; i < cch->size; i++)
    {
        if (cch->record[i].hitcount < hit)
        {
            hit = cch->record[i].hitcount;
            minimum = i;
        }
    }
    return minimum;
}
 
int hashCounter(char *newdata)
{
    int count = 0;
    for (int i = 0; ; i++) {
        if (newdata[i] == '\0')
        {
            break;
        }
        else
        {
            count++;
        }
    }
    return count;
}
 
int addRec(cache *cch, char *newData)
{
    int newHash = hashCounter(newData);
    for (int j = 0; j < cch->size; j++)
    {
        if ((newHash == cch->record[j].hash) && (strcmp(cch->record[j].data, newData) == 0))
        {
            cch->record[j].hitcount++;
            return 0;
        }
        if (cch->record[j].hitcount == 0)
        {
            cch->record[j].data = (char*)malloc((newHash + 1) * sizeof(char));
            for (int i = 0; i <= newHash; i++)
            {
                cch->record[j].data[i] = newData[i];
            }
            cch->record[j].hitcount = 1;
            cch->record[j].hash = newHash;
            return 1;
        }
    }
    int n = min(cch);
 
    free(cch->record[n].data);
    cch->record[n].data = (char*)malloc((newHash + 1) * sizeof(char));
 
    for (int i = 0; i <= newHash; i++)
    {
        cch->record[n].data[i] = newData[i];
    }
    cch->record[n].hitcount = 1;
    cch->record[n].hash = newHash;
    return 2;
}
 
record* getRecord(cache *cch, char *search)
{
    int searchHash = hashCounter(search);
    for (int i = 0; i < cch->size; i++)
    {
        if ((searchHash == cch->record[i].hash)&&(strcmp(search, cch->record[i].data) == 0))
        {
            cch->record[i].hitcount++;
            printf("String: %s \n Hits: %i \n Hash: %i \n", cch->record[i].data, cch->record[i].hitcount, cch->record[i].hash);
            return &(cch->record[i]);
        }
    }
    printf("NULL\n");
    return NULL;
}
 
void clean(cache *cch)
{
    for (int i = 0; i < cch->size; i++)
    {
        char x[] = " ";
        cch->record[i].hitcount = 0;
        cch->record[i].data = x;
    }
}
 
int console(cache *cch)
{
    char str[80], strdo[80];
    while (1)
    {
        printf("Command: ");
        scanf("%s", str, (unsigned)sizeof(str));
        if (strcmp(str, "add") == 0)
        {
            scanf("%s", strdo, (unsigned)sizeof(strdo));
            addRec(cch, strdo);
        }
        if (strcmp(str, "out") == 0)
        {
            scanf("%s", strdo, (unsigned)sizeof(strdo));
            getRecord(cch, strdo);
        }
        if (strcmp(str, "help") == 0)
        {
            printf("add 'string'\nout 'string'\nexit\n");
        }
        if (strcmp(str, "exit") == 0)
        {
            free(cch->record);
            free(cch);
            return 0;
        }
    }
}
 
int main()
{
    cache* cch = (cache*)malloc(sizeof(cache));
    printf("Amount of memory: ");
    scanf("%i", &cch->size);
    cch->record = (record*)malloc(cch->size * sizeof(record));
    clean(cch);
    console(cch);
}