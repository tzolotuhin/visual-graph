#include <stdio.h>

int fib(int n) {
    if (n <= 0)
        return 0;
    if (n == 1 || n == 2)
        return 1;
    return fib(n - 1) + fib(n - 2);
}

int main() {
	printf("Fib: ");
	for (int i = 0; i < 10; i++) {
		printf("%d ", fib(i));
	}

	return 0;
}
