#include <stdio.h>

int size = 6;
int arr[] = { 14, 10, 11, 19, 2, 25 };

void swap(int *a, int *b) {
    int c = *a;
    *a = *b;
    *b = c;
}

void qsort (int b, int e) {
  int l = b, r = e;
  int piv = arr[(l + r) / 2]; // Опорным элементом для примера возьмём средний
  while (l <= r) {
    while (arr[l] < piv)
      l++;
    while (arr[r] > piv)
      r--;
    if (l <= r)
      swap (&arr[l++], &arr[r--]);
  }
  if (b < r)
    qsort (b, r);
  if (e > l)
    qsort (l, e);
}

int main() {
	printf("The array before sort: ");
	for (int i = 0; i < size; i++) {
		printf("%d ", arr[i]);
	}

    qsort(0, size - 1);

	printf("\nThe array after sort: ");
	for (int i = 0; i < size; i++) {
		printf("%d ", arr[i]);
	}

	return 0;
}
