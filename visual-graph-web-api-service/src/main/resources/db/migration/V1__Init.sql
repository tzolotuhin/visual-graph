-- Init script
CREATE TABLE IF NOT EXISTS public.graph_file
(
    id         SERIAL       NOT NULL PRIMARY KEY,
    user_id    INT          NOT NULL,
    format     VARCHAR(255) NOT NULL,
    file       BYTEA        NOT NULL
)
