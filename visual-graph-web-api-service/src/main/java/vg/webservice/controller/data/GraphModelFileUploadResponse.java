package vg.webservice.controller.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class GraphModelFileUploadResponse {
    private Integer graphModelFileId;
}
