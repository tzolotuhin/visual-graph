package vg.webservice.controller.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CreateTokenRequest {
    private String login;
    private String password;
}
