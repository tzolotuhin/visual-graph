package vg.webservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vg.webservice.controller.data.HealthCheckResponse;

@Slf4j
@RestController
@RequestMapping(path = "v1")
public class PublicHealthController {
    @GetMapping("health-check")
    public ResponseEntity<HealthCheckResponse> healthCheck() {
        log.info("Health check was called.");

        var response = new HealthCheckResponse();
        response.setStatus("OK");

        return ResponseEntity.ok(response);
    }
}
