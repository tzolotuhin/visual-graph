package vg.webservice.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vg.webservice.controller.data.GraphModelFileUploadResponse;

@RestController
@RequestMapping(path = "/v1/graph-model")
public class GraphModelFileController {
    @PostMapping("upload")
    public ResponseEntity<GraphModelFileUploadResponse> upload(
        @RequestParam("graph_model_file") MultipartFile graphModelFile,
        @RequestParam("graph_model_name") String graphModelName) {
        var principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return ResponseEntity.ok().build();
    }
}
